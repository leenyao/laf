#!//bin/bash
echo -e "\n"
echo 'DROP VIEW IF EXISTS `system_matadata_'$1'` ;
CREATE VIEW `system_matadata_'$1'` AS select `system_metadata`.`id` AS `id`,`system_metadata`.`md5id` AS `md5id`,`system_metadata`.`status` AS `status`,`system_metadata`.`type` AS `type`,`system_metadata`.`p_type` AS `p_type`,`system_metadata`.`p_resid` AS `p_resid`,`system_metadata`.`resid` AS `resid`,`system_metadata`.`label` AS `label`,`system_metadata`.`modified` AS `modified` from `system_metadata` where (`system_metadata`.`type` = '$1');'
echo -e "\n"
