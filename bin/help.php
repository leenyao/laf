<?php
include_once(__DIR__."/script/config_inc.php");

$sm = $oGlobalFramework->sm;
$aConfig = $sm->get('config');
$aDsn = \YcheukfCommon\Lib\Functions::getParamFromDSN($aConfig['db_master']['dsn']);
$aDsnSlave = \YcheukfCommon\Lib\Functions::getParamFromDSN($aConfig['db_slave']['dsn']);

// print_r($aConfig['cache_config']['params']['servers']);
// system("mysql -u".$this->config['db_master']['username']." -p".$this->config['db_master']['password']." --port=".$aDsn['port']." -h".$aDsn['host']."  -e \"create database if not exists ".$aDsn['dbname']."\"");

// self::$PDOObejct =  new \PDO($aConfig['db_master']['dsn'], $aConfig['db_master']['username'], $aConfig['db_master']['password'], array(
        // \PDO::ATTR_PERSISTENT => true,
        // \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\';SET time_zone = \'+8:00\''
// ));
$sToday = date('Ymd');


$sAddtionnal = "";
if (LAF_NAMESPACE == "Lur") {

	$sAliveAdrSet = LAF_CACHE_ADRSERVERS_ALIVE_106;
	$sAliveAdrSet2 = LAF_CACHE_ADDTIONAL_ADRALIVE_PRE_310;

	$sAddtionnal = <<<_EOF

remove adr from alive-list
	redis-cli -h {$aConfig['cache_config']['params']['servers'][0][0]} -p {$aConfig['cache_config']['params']['servers'][0][1]} del {$sAliveAdrSet} 1
	redis-cli -h {$aConfig['cache_config']['params']['servers'][0][0]} -p {$aConfig['cache_config']['params']['servers'][0][1]} del {$sAliveAdrSet2}1


rebuild adrid-vpn config
	redis-cli -h {$aConfig['addtionalredis']['host']} -p {$aConfig['addtionalredis']['port']} DEL LC_111

clear vpn-ignore account
	redis-cli -h {$aConfig['addtionalredis']['host']} -p {$aConfig['addtionalredis']['port']} keys LC_110* | xargs redis-cli  -h {$aConfig['addtionalredis']['host']} -p {$aConfig['addtionalredis']['port']} del

connect addtional redis
	redis-cli -h {$aConfig['addtionalredis']['host']} -p {$aConfig['addtionalredis']['port']}

rebuild active-truck-adr
	redis-cli -h {$aConfig['cache_config']['params']['servers'][0][0]} -p {$aConfig['cache_config']['params']['servers'][0][1]} del LC_317 && php /app/bin/script/crondjob/run.php firealarm
	

_EOF;

}



echo <<<_EOF

adjust addtional db
	php /app/module/Lur/src/Lur/Script/addtional_db/justic.php

switch adr runmodel
	php /app/module/Lur/src/Lur/Script/switch_adrtype/run.php
 
import iacip
	cd /app/module/Lur/src/Lur/Script/iacipload/ && cat readme

import vpnaccount
	cd /app/module/Lur/src/Lur/Script/importvpnacc/ && cat readme

generate sdkconfig package
	php /app/module/Lur/src/Lur/Script/gensdkconfig/go.php

generate laf-module
	php /app/bin/script/generate_module.php

generate permission
	php /app/bin/script/generate_permission.php

lur stats
php /app/module/Lur/src/Lur/Script/stats/run.php

lur addtionnal history
	php /app/module/Lur/src/Lur/Script/addtionnalhist/history.php {$sToday} [codeid] [adrid]

lur addtionnal history hour-trend
	php /app/module/Lur/src/Lur/Script/addtionnalhist/history.hour.php {$sToday} [codeid] [adrid]

recall vpn
	sh /app/bin/run_service.sh 1 && lur-recallvpn

{$sAddtionnal}

connect mysql
	mysql -u{$aConfig['db_master']['username']} -p{$aConfig['db_master']['password']} --port={$aDsn['port']} -h{$aDsn['host']} 
	mysql -u{$aConfig['db_slave']['username']} -p{$aConfig['db_slave']['password']} --port={$aDsnSlave['port']} -h{$aDsnSlave['host']} 

connect redis
	redis-cli -h {$aConfig['cache_config']['params']['servers'][0][0]} -p {$aConfig['cache_config']['params']['servers'][0][1]}

iac ip-city cache
redis-cli -h {$aConfig['cache_config']['params']['servers'][0][0]} -p {$aConfig['cache_config']['params']['servers'][0][1]} keys LC_102/* |  xargs redis-cli -h {$aConfig['cache_config']['params']['servers'][0][0]} -p {$aConfig['cache_config']['params']['servers'][0][1]} del
	
adr batch script
	php /app/module/Lur/public/adrscript/batch/help.php
	
---proxy---
查看当天被禁止的账号
redis-cli -h lafredislur -p 9003 smembers LC_839/{$sToday}
重新分配账号
php /app/bin/script/crondjob/run.php UpdateProxy 1



cronjobs restart|stop|start
	sh /app/bin/run_service.sh [1|off|on]


_EOF;

