#!//bin/bash
echo "=============================="
echo "[LAF]setting config..."
echo "=============================="
sh setup_config.sh >> /app/data/install.log

echo "=============================="
echo "[LAF]setting composer..."
echo "=============================="
sh setup_composer.sh >> /app/data/install.log

echo "=============================="
echo "[LAF]setting db-migration..."
echo "=============================="
sh run_migration.sh >> /app/data/install.log


echo "=============================="
echo "[LAF]setting js&css..."
echo "=============================="
sh setup_jscss.sh >> /app/data/install.log