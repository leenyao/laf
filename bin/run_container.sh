#!/bin/bash
set -e
sh /app/bin/setup_config.sh
# sh /app/bin/run_translate.sh

ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime 
echo "Asia/Shanghai" > /etc/timezone

(echo "ly0509LUR";sleep 1;echo "ly0509LUR") | passwd
if [[ -f /root/.ssh/id_rsa ]]; then
    chmod -c 600 /root/.ssh/id_rsa
fi

chmod -cR 777 /app/data
mkdir -p /app/data/ycfreport/csv/

############check service running
process=`ps aux | grep run_container.sh | grep -v grep`
process_num=${#process[@]} 
if [ "$process_num" -gt 1 ]; then
    echo "[LAF]another run_container.sh is running. "
    exit 1
else
    #### last setup failed, restart###
    if [ -f "/app/data/laf_installing" ]; then 
        rm -f /app/data/laf_installing
    fi
fi


############config apache
sed -ri -e "s/^#ServerName.*/ServerName laf_web:80/" /etc/httpd/conf/httpd.conf
sed -ri -e "s/AllowOverride None/AllowOverride FileInfo/" /etc/httpd/conf/httpd.conf
sed -ri -e "s/logs\/error_log/\/app\/php_errors.log/" /etc/httpd/conf/httpd.conf

sed -ri -e "s/CustomLog \"logs\/access_log\" combined/\#CustomLog \"logs\/access_log\" combined/" /etc/httpd/conf/httpd.conf
sed -ri -e "s/^upload_max_filesize.*/upload_max_filesize = 10M/"  /etc/php.ini
sed -ri -e "s/^memory_limit.*/memory_limit = 5516M/"  /etc/php.ini
sed -ri -e "s/^post_max_size.*/post_max_size = 10M/"  /etc/php.ini
sed -ri -e "s/^display_errors =.*/display_errors = Off/"  /etc/php.ini
sed -ri -e "s/^;date\.timezone =.*/date.timezone = Asia\/Chongqing/"  /etc/php.ini
sed -ri -e "s/^;error_log = php_errors.log/error_log = \/app\/php_errors.log/"  /etc/php.ini


echo "alias lur-help=\"cd /app && php /app/bin/help.php\"" >> ~/.bashrc
echo "alias lur-recallvpn=\"sh /app/bin/run_service.sh 1 && redis-cli -h lafredislur -p 9001 DEL LC_111 && redis-cli -h lafredislur -p 9001 keys LC_110/* |  xargs redis-cli  -h lafredislur -p 9001 del\"" >> ~/.bashrc
echo "alias lur-cronjob=\"php /app/bin/script/crondjob/run.php -h && echo 'php /app/bin/script/crondjob/run.php -h'\"" >> ~/.bashrc
echo "export LESSCHARSET=utf-8" >> ~/.bashrc
source ~/.bashrc



export LESSCHARSET=utf-8

############


if [ ! -f "/app/data/laf_installed" ]; then 

    if [ ! -f "/app/data/laf_installing" ]; then 
        touch /app/data/laf_installing
        cd /app/bin
        sh install.sh >/app/data/install.log
        rm -f /app/data/laf_installing
        touch /app/data/laf_installed
    else
        echo "[LAF]another laf_install-script is running.  wait or remove data/laf_install* to restart"
        exit 1
    fi
fi
sh /app/bin/run_migration.sh


while true
do 
 
    # need to watch service
    if [ -f "/app/data/runservice.lock" ]; then 
        count=`ps -ef |grep "crondjob/run.php" |grep -v "grep" |wc -l`
        if [ $count -eq 0 ]; then
            sh /app/bin/run_service.sh on &
        fi
    # else
    #     sh /app/bin/run_service.sh off &
    fi

    count=`ps -ef |grep "/usr/sbin/sshd" |grep -v "grep" |wc -l`
    if [ $count -eq 0 ]; then
       /usr/sbin/sshd > /dev/null &
    fi


    count=`ps -ef |grep httpd |grep -v "grep" |wc -l`
    if [ $count -eq 0 ]; then
        apachectl -k start
    fi
    # translate
    # sh /app/bin/run_translate.sh
    sleep 20

done