#!//bin/bash
currentdir=$(cd "$(dirname "$0")"; pwd)
cd $currentdir
sh run_service.sh off
sh run_cache_flush.sh
php script/gen_migration.php 0
sh ../data/migration.run.sh
php /app/bin/script/generate_permission.php 1
sh run_service.sh 1

