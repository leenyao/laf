#!/usr/bin/env bash

# This script prints out all of your Redis keys and their size in a human readable format
# Copyright 2013 Brent O'Connor
# License: http://www.apache.org/licenses/LICENSE-2.0

human_size() {
        awk -v sum="$1" ' BEGIN {hum[1024^3]="Gb"; hum[1024^2]="Mb"; hum[1024]="Kb"; for (x=1024^3; x>=1024; x/=1024) { if (sum>=x) { printf "%.2f%s\n",sum/x,hum[x]; break; } } if (sum<1024) print "1kb"; } '
}

redis_cmd='redis-cli -h lafredislur -p 9001'
# redis_cmd='redis-cli  -h 10.30.200.108 -p 6377'

# get keys and sizes
for k in `$redis_cmd keys "*"`; do 
    key_size_bytes=`$redis_cmd debug object $k | perl -wpe 's/^.+serializedlength:([\d]+).+$/$1/g'`; 
    key_ttl=`$redis_cmd ttl $k`; 
    size_key_list="$size_key_list $key_size_bytes\t$key_ttl\t$k\n"; 
done

# sort the list
sorted_key_list=`echo -e "$size_key_list" | sort -rn`



nTotlSize=0
# print out the list with human readable sizes
echo -e "$sorted_key_list" | while read l; do
    if [[ -n "$l" ]]; then

        size=`echo $l | awk  '{print $1}'`
        let "nTotlSize=nTotlSize+$size"
        hTotalSize=`human_size $nTotlSize`

        hsize=`human_size "$size"`; 
        ttl=`echo $l | awk  '{print $2}'`
        key=`echo $l | awk  '{print $3}'`
        echo -e "$hsize\t$ttl\t$key\t$hTotalSize" 
        # printf "%-10s%-10s%s\n" "$hsize" "$ttl" "$key";
    fi
done 
