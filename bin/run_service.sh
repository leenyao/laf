#!/bin/sh

if [[ -z $1 ]]; then
	echo '
	sh run_service.sh [on/off/1]

	on => start service	
	off => stop service	
	1 => force start service	
'
	exit 1
fi

count=`ps -ef |grep crondjob/run.php |grep -v "grep" |wc -l`

if [[ $1 = 1 ]]; then
	# 常驻锁文件
	touch /app/data/runservice.lock

	if [ $count -ne 0 ]; then
		pid=`ps -ef |grep crondjob/run.php |grep -v "grep" | awk '{print $2}'`
		kill -9 $pid
	fi
	php /app/bin/script/crondjob/run.php >> /app/data/crondjob.log &
	echo "restart crondjob"
elif [[ $1 = "on" ]]; then

	# 常驻锁文件
	touch /app/data/runservice.lock

	if [ $count -eq 0 ]; then
	    php /app/bin/script/crondjob/run.php >> /app/data/crondjob.log &
		echo "start crondjob"
	    exit 0
	else
		echo "crondjob is running"
	fi
elif [[ $1 = "off" ]]; then
	# 常驻锁文件
	rm -f /app/data/runservice.lock
	if [ $count -ne 0 ]; then
		pid=`ps -ef |grep crondjob/run.php |grep -v "grep" | awk '{print $2}'`
		kill -9 $pid
		
		pid=`ps -ef |grep daemon_lur_gen_ips.php |grep -v "grep" | awk '{print $2}'`
		kill -9 $pid
	fi
	echo "stop crondjob"
else
	echo "no such param:"$1
fi

# if [ $count -eq 0 ]; then
#     php /app/bin/script/crondjob/run.php >> /app/data/crondjob.log &
#     exit 0
# else
	
#     exit 1
# fi