#!/bin/bash
## this is a mfn vendor setup script
currentdir=$(cd "$(dirname "$0")"; pwd)
sProjectName=`echo $LAF_project_namespace | sed 's/^\w\|\s\w/\U&/g'`


# echo ${currentdir}
# echo ${sProjectName}

msgfmt ${currentdir}/../module/${sProjectName}/language/zh_CN.po -o ${currentdir}/../module/${sProjectName}/language/zh_CN.mo
msgfmt ${currentdir}/../module/Application/language/zh_CN.po -o ${currentdir}/../module/Application/language/zh_CN.mo

# cd $currentdir/../