<?php
/***********配置区****************/
include(dirname(__FILE__)."/../config_inc.php");
\YcheukfDebug\Model\Debug::setDumpFlag(false);//关掉debug
$sLocationBase = $oGlobalFramework->config['apiservice']['base_location'];
//获取oauth token
//$sToken = $oGlobalFramework->oauthlogin();
$sToken = "whatever";
echo "running demodata: ".__FILE__."\n";
/***********配置区****************/

_demo_user_data();//生成post数据
_demo_post_data();//生成post数据
_demo_ads_data();//生成category数据
_demo_meta_data();//生成meta 标签 匿名数据

echo "__DONE__ at ".date("Y-m-d H:i:s")."\n\n";

function _demo_ads_data(){    
    global $sLocationBase, $sToken, $oGlobalFramework;

    $aM122 = _getMetadataBy(122);
    $aM123 = _getMetadataBy(123);
    $aM131 = _getMetadataBy(131);

    $aTmp = array(
        "/../../public/img/demo/ad1.jpg", 
        "/../../public/img/demo/ad2.jpg", 
        "/../../public/img/demo/ad3.jpg", 
        "/../../public/img/demo/ad4.jpg", 
        "/../../public/img/demo/web-banner-01.jpg", 
        "/../../public/img/demo/web-banner-02.jpg", 
        "/../../public/img/demo/web-banner-03.jpg", 
        "/../../public/img/demo/web-banner-04.jpg"
    );
    $aThumbnails = $oGlobalFramework->uploadFile($aTmp);

    $aTitles = array("牵牛网汇集了500000人以上的业界精英，赶紧来瞧瞧你是牦牛？黄牛？水牛？还是传说中的神牛？", "跨平台系统，打通移动端和Web端，并同步云服务器", "国内最垂直的IT/互联网、广告人、媒体人的聚集地", "全国首家总包平台系统提供商、技术解决方、一站式服务提供商" , "牦牛？黄牛？水牛？");

    $aRequest = array();
    $aP4Pics = array(
        "/images/v3/PC_01.jpg",
        "/images/v3/PC_02.jpg",
        "/images/v3/PC_03.jpg",
        "/images/v3/PC_04.jpg",
    );
    $nTime = date("Y-m-d H:i:s");
    for($i=1; $i<40; $i++){
        $uid = uniqid();
        $nPositionId = $aM122[array_rand($aM122)]['resid'];
        $sPicUrl = ($nPositionId==4) ? $aThumbnails[rand(4, 7)] : $aThumbnails[rand(0, 3)];
        $type_id = $aM123[array_rand($aM123)]['resid'];
        if($nPositionId == 5)
            $type_id = 3;
        if($nPositionId == 6){
            $type_id = 1;
            $sPicUrl = "img/demo/app-loading.jpg";
        }
        if($nPositionId == 7 || $nPositionId == 8){
            $type_id = 1;
            $sPicUrl = "img/demo/web-banner-05.jpg";
        }
        if($nPositionId == 4){
            $type_id = 1;
            $sPicUrl = $aP4Pics[array_rand($aP4Pics)];
        }
        $aRequest[] = array(
            'position_id' => $nPositionId,
            'type_id' => $type_id,
            'action_id' => $aM131[array_rand($aM131)]['resid'],
            'status' => 1,
            'title' => $aTitles[array_rand($aTitles)],
            'pic_url' => $sPicUrl,
            'click_url' => "http://www.baidu.com",
            'modified' => $nTime,
        );
    }
    _demo_save($aRequest, $sLocationBase, $sToken, 'ads');//发送category数据

    return $aRequest;
}

function _demo_post_data(){
    global $sLocationBase, $sToken,$oGlobalFramework;

    _insert_post_upload_demo();

//    include_once(__DIR__."/rss-php-master/feed.class.php");
//    $url = "http://cnbeta.feedsportal.com/c/34306/f/624776/index.rss";
//    $url = "http://news.163.com/special/00011K6L/rss_newstop.xml";
//    $url = "http://feeds2.feedburner.com/jandan";
//    $rss = Feed::loadRss($url);


    $aThumbnails = array(
        "http://www.1000new.com.cn/img/tmpupload/2014/01/06/52ca2574bee71.jpg", 
        "http://www.1000new.com.cn/img/tmpupload/2014/01/08/52ccceff21844.jpg", 
        "http://www.1000new.com.cn/img/tmpupload/2014/01/07/52cb7b8f97fb1.jpg", 
        "http://www.1000new.com.cn/img/tmpupload/2014/01/03/52c63983abcb5.jpg", 
        "http://www.1000new.com.cn/img/tmpupload/2013/12/23/52b7ca6f4d926.jpg", 
    );

    
    //更新post 表
    $aRssData = array();
    $nTime = date("Y-m-d H:i:s");
    $aUser = _getResourceList('user');
    $aCategory = _getResourceList('category', array('id'=>array('$gt'=>9, '$lt'=>20)));


    for($i=0; $i<30; $i++){
        $aRssData[] = array(
            'user_id' => $aUser[array_rand($aUser)]['id'],
            'view_type' => 3,
            'category_id' => $aCategory[array_rand($aCategory)]['id'],
            'touser_id' => $aUser[array_rand($aUser)]['id'],
            'fid' => "",
            'status' => 1,
//            'link' => (string)$item->link,
            'created' => date("Y-m-d", time()),
            'modified' => $nTime,
            'title' => $i."_这是号外title",
            'postname' => $i."_这是号外postname",
            'thumbnails' => $aThumbnails[array_rand($aThumbnails)],
            'description' => $i."_这是号外description",
            'content' => $i."_这是号外内容",
        );
    }
//    $aRssData = array_merge($aRssData, $aRssData, $aRssData);

    //增加4,5关系的数据
    $aRssData[0]['view_type'] = $aRssData[1]['view_type'] = 4;
    $aRssData[0]['postname'] = "[测试内容]多图文帖子1";
    $aRssData[1]['postname'] = "[测试内容]多图文帖子2";
    $aRssData[0]['category_id'] = $aRssData[1]['category_id'] = 11;
    $aRssData[2]['view_type'] = $aRssData[3]['view_type'] = $aRssData[4]['view_type'] = $aRssData[5]['view_type'] = $aRssData[6]['view_type'] = $aRssData[7]['view_type']  = 5;
    $result = $oGlobalFramework->callApiByCurl($sLocationBase."/common/post/save", array(
        "access_token" => $sToken,
        "params" => array("dataset" => array($aRssData[0])),
    ));
    $aRssData[2]['fid'] = $aRssData[3]['fid'] = $aRssData[4]['fid'] = $aRssData[5]['fid'] = $result['data'];
    $result = $oGlobalFramework->callApiByCurl($sLocationBase."/common/post/save", array(
        "access_token" => $sToken,
        "params" => array("dataset" => array($aRssData[1])),
    ));
    $aRssData[6]['fid'] = $aRssData[7]['fid'] = $result['data'];
    unset($aRssData[0]);
    unset($aRssData[1]);
//    var_dump($aRssData);
    _demo_save($aRssData, $sLocationBase, $sToken, 'post');//发送category数据
    return $aRssData;

}

function _demo_user_data($sMikiFlag=false){
    global $sLocationBase, $sToken, $oGlobalFramework;
    if(!$sMikiFlag){
        $aUsers = array(
            array("fengruzhuo", "18621110304", "ruzhuo_feng@1000new.com"),
//            array("chenxiaomi", "13524681009", "miki_chen@1000new.com"),
//            array("zhanglei", "15618688068", "1_lei_zhang@1000new.com"),
//            array("jilifeng", "15800781124", "1_eric_ji@1000new.com"),
//            array("wuzhenyou", "18621052569", "1_zhenyou_wu@1000new.com"),
//            array("wangyuan", "13916890460", "1_crystal_wang@1000new.com"),
//            array("shenliang", "13512198910", "1_liang_shen@1000new.com"),
//            array("guzhijun", "13816324701", "1_zhijun_gu@1000new.com"),
//            array("kangda", "13818084170", "1_seven_kang@1000new.com"),
//            array("kanhaiying", "15601822755", "1_seagle@1000new.com"),
//            array("daiwenjing", "13701905389", "1_anita_dai@1000new.com"),
//            array("yuhongjie", "13564915135", "1_hongjie_yu@1000new.com"),
//            array("liufujuan", "15901956031", "1_amy_liu@1000new.com"),
//            array("lumingmei", "18930768351", "1_koko_lu@1000new.com"),
//            array("wangzhengqiang", "15821023013", "1_koko_lu@1000new.com"),
//            array("zhuzhilong", "18867112730", "1_koko_lu@1000new.com"),
//            array("zhouweifeng", "13917613063", "1_koko_lu@1000new.com"),
//            array("fengpeng", "18671219795", "1_koko_lu@1000new.com"),
//            array("liutao", "18817262082", "1_tao_liu@1000new.com"),
//            array("lifengrong", "18817262082", "fengrong_li@1000new.com"),
//            array("hexiaoying", "18817262082", "may_he@1000new.com"),
        );
    }else{
        $aUsers = array(
        );
    }

    $aM105 = _getMetadataBy(105);

    $aThumbnails = $oGlobalFramework->uploadFile(array("/../../public/img/demo/mahuateng.jpg", "/../../public/img/demo/mayun1.jpg"));
    $nTime = date("Y-m-d H:i:s");
    $aSysUser = $aOauthUser = array();
    foreach($aUsers as $i=>$row){
        $sUser = $row[0];
        $sPhone = $row[1];
        $sMail = $row[2];
//        $aOauthUser[] = array(
        $aTmp = array(
            'username' => $sUser,
            'from_type' => 1,
            'password' => md5($sUser),
            'email' => $sMail,
            'phone' => $sPhone,
            'modified' => $nTime,
        );

        $result = $oGlobalFramework->callApiByCurl($sLocationBase."/common/oauthuser/save", array(
            "access_token" => $sToken,
            "params" => array("dataset" => array($aTmp)),
        ));
        $aSysUser[] = array(
            'id' => $result['data'],
            'user_id' => $result['data'],
            'username' => $sUser,
            'status' => 1,
            'password' => md5($sUser),
            'email' => $sMail,
            'email_verified' => 1,
            'phone_verified' => 1,
            'user_registered' => "2014-".rand(1,12)."-".rand(1,20),
            'phone' => 1,
            'display_name' => $sUser,
//            'credit' => rand(10,20),
            'phone' => $sPhone,
            'avatar' => $aThumbnails[array_rand($aThumbnails)],
            'modified' => $nTime,
            'birthday' => rand(1980,2000)."-".rand(1,12)."-".rand(1,20),
            'joinworkdate' => rand(2005,2010)."-".rand(1,12)."-".rand(1,20),
            'location_city_id' => rand(1,20),
            'm105_id' => $aM105[array_rand($aM105)]['resid'],
            'memo' => "memo".$i,
        );
    }

    $aMetadata = array();
    foreach($aSysUser as $row){
        $aMetadata[] = array(
                'type' => 6,
                'resid' => $row['id'],
                'label' => 8,
                'modified' => $nTime,
        );
    }
    _demo_save($aMetadata, $sLocationBase, $sToken, 'metadata');//发送category数据
//    var_dump($aSysUser);
    _demo_save($aSysUser, $sLocationBase, $sToken, 'user');//发送category数据
//    _demo_save($aOauthUser, $sLocationBase, $sToken, 'oauthuser');//发送category数据

}



function _insert_post_upload_demo(){
    global $sLocationBase, $sToken, $oGlobalFramework;
    $aRssData = array();
    $nTime = date("Y-m-d H:i:s");

    $aTmp = array("/../../public/img/demo/864X480.jpg","/../../public/img/demo/148X148.jpg","/../../public/img/demo/148X148_2.jpg",);
    $aThumbnails = $oGlobalFramework->uploadFile($aTmp);


    //多图文-上传图片测试帖子
    $aRssData[] = array(
        'user_id' => 1,
        'view_type' => 4,
        'category_id' => 12,
        'touser_id' => rand(10,15),
        'fid' => "",
        'status' => 1,
//            'link' => (string)$item->link,
        'created' => "2013-11-22",
        'modified' => $nTime,
        'title' => "多图文-上传图片测试帖子",
        'postname' => "多图文-上传图片测试帖子",
        'thumbnails' => "",
        'description' => "",
        'content' => "",
    );
    $result = $oGlobalFramework->callApiByCurl($sLocationBase."/common/post/save", array(
        "access_token" => $sToken,
        "params" => array("dataset" => array($aRssData[0])),
    ));
    $nFid = $result['data'];

    $aRssData = array();
    $aRssData[] = array(
        'user_id' => 1,
        'view_type' => 5,
        'category_id' => 12,
        'touser_id' => rand(10,15),
        'fid' => $nFid,
        'status' => 1,
//            'link' => (string)$item->link,
        'created' => "2013-11-22",
        'modified' => $nTime,
        'title' => "美陪审团复审判三星向苹果支付2.9亿美元赔偿金",
        'postname' => "多图文-上传图片测试帖子",
        'thumbnails' => $aThumbnails[0],
        'description' => "据华尔街日报报道，周四苹果最终在与三星电子有限公司的专利侵权诉讼复审判决中，获得2.9亿美元的赔偿。陪审团去年宣判三星侵犯了苹果的专利，并要求三星赔偿苹果10.5亿美元。但美国地方法院法官露西苏梅说，陪审团错误的计算了13款三星设备的专利侵权，约合4.5亿美元的赔偿。并在这个问题上下令复审。而另外6亿美元不受影响。",
        'content' => '<p style="text-align: center;"><span style="line-height: 0px;">﻿</span><img alt="http://static.cnbetacdn.com/newsimg/2013/1122/01385072861.jpg" src="http://static.cnbetacdn.com/newsimg/2013/1122/01385072861.jpg" original="http://static.cnbetacdn.com/newsimg/2013/1122/01385072861.jpg" data-pinit="registered"><br></p><p>重审关系到韩国电子巨头到底赔偿多少钱给苹果。结合周四的判决，<a data-type="2" data-keyword="三星" data-rd="1" data-style="1" data-tmpl="290x380" target="_blank">三星</a>现在共拖欠苹果8.9亿美元的赔偿。此前，有消息称苹果希望获得3.8亿美元赔偿，但三星表示，它只应该<a data-type="2" data-keyword="支付" data-rd="1" data-style="1" data-tmpl="290x380" target="_blank">支付</a>5200万美元。</p>',
    );
    $aRssData[] = array(
        'user_id' => 1,
        'view_type' => 5,
        'category_id' => 12,
        'touser_id' => rand(10,15),
        'fid' => $nFid,
        'status' => 1,
//            'link' => (string)$item->link,
        'created' => "2013-11-22",
        'modified' => $nTime,
        'title' => "传微软考虑接手Winamp 正与AOL就价格谈判",
        'postname' => "多图文-上传图片测试帖子",
        'thumbnails' => $aThumbnails[1],
        'description' => "AOL（美国在线）昨天宣布，该公司将关闭旗下音乐服务Winamp。Winamp是一款针对Windows和Android等设备的媒体播放软件，由 AOL在1999年以8000万美元的价格从Nullsoft之手并购而来。但与此同时，媒体又有消息称，AOL正在与微软谈判，以商定将Winamp及 旗下另一款音乐服务Shoutcast出售给微软的相关事宜。Shoutcast也是一款由Nullsoft开发的媒体流服务。",
        'content' => '<p class="f_center">﻿<img alt="http://static.cnbetacdn.com/newsimg/2013/1122/01385073332.jpg" data-pinit="registered" original="http://static.cnbetacdn.com/newsimg/2013/1122/01385073332.jpg" src="http://static.cnbetacdn.com/newsimg/2013/1122/01385073332.jpg" style="display: block;"><br></p><p>消息人士透露称，AOL计划宣布在下周某一时间关闭Shoutcast服务。</p><p>对于上述传闻，AOL方面一直拒绝置评，而微软也未就此发表评论。据知情人士透露的消息称，目前AOL与微软还未就交易事宜达成最终协议，双方仍在就交易价格问题进行谈判。</p><p>AOL昨天在宣布将关闭Winamp时，并未提及Shoutcast相关的事宜。</p><p>从AOL这方面来看，AOL为何关闭Winamp和Shoutcast，原因非常清楚，出售这两个服务对AOL而言也有着重要意义。此前，AOL在这两大服务方面倾注了大量的精力和心血，特别是在各大数字音乐公司都在拉拢听众和不断创新技术之际。不过，AOL目前在此方面却遇到了困难。除这两大服务之外，AOL还关闭或抛售了其它一些音乐资产，以此将更多的资源整合到网络发行方面（AOL目前拥有TechCrunch、Engadget、《赫芬顿邮报》以及其它多数博客网站）和媒体广告网络运营方面，并将更多的重点放在广告技术方面，以此提高广告发行能力。当然，对AOL而言，音乐服务可以成为综合业务的一部分，不过，这些音乐服务却缺乏大量的资金支持，也缺乏足够的外来战略投资。</p><p>从微软的角度来看，微软公司在音乐产品方面有自己的困难，不过，微软最近已经大力投资<a data-type="2" data-keyword="Xbox" data-rd="1" data-style="1" data-tmpl="290x380" target="_blank">Xbox</a> Music业务，此服务主要在Xbox 360、Windows 8、Windows RT、Windows Phone 8、iOS和Android等多种设备上运行，而且提供免费的广告支持的流音乐、音乐订阅和音乐<a class="a-tips-Article-QQ" href="http://download.tech.qq.com/" target="_blank">下载</a>等服务。</p><p>Winamp和Shoutcast能否融入微软的这些服务，目前还需拭目以待。目前Winamp和Shoutcast已经成为了一个拥有5万多电台的门户平台，微软可以利用好这一点，将这些优势整合到Xbox Music平台上，毕竟这也是微软目前的欠缺，这样微软也可以增加自己类似于Pandora风格的个人电台功能。</p>',
    );
    $result = $oGlobalFramework->callApiByCurl($sLocationBase."/common/post/save", array(
        "access_token" => $sToken,
        "params" => array("dataset" => $aRssData),
    ));
}


//个人标签 匿名
function _demo_meta_data($sMikiFlag=false){
	global $sLocationBase, $sToken, $oGlobalFramework;

    $nTime = date("Y-m-d H:i:s");
    for($i=1; $i<10; $i++){
        $aRequest[] = array(
            'md5id' => md5($i),
            'type' => 161,
            'resid' => $i,
            'label' => "标签".$i,
            'modified' => $nTime,
        );

		$bRequest[] = array(
            'md5id' => md5($i),
            'type' => 162,
            'resid' => $i,
            'label' => "匿名称呼".$i,
            'modified' => $nTime,
        );
    }

    _demo_save($aRequest, $sLocationBase, $sToken, 'metadata');

	_demo_save($bRequest, $sLocationBase, $sToken, 'metadata');

    return $aRequest;
}