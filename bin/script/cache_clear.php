<?php
session_start();
include_once(__DIR__."/config_inc.php");

if($oGlobalFramework->config['cache_config']['type'] == 'memcache'){
	$memcache = new \Memcache;
	foreach($oGlobalFramework->config['cache_config']['params']['servers'] as $aRow){
		$bFlag = $memcache->addServer($aRow[0], $aRow[1], $aRow[2]);
	}
	if($memcache->flush())
		echo "\n\n flsh memcache done \n\n";
	else
		echo "\n\n flsh memcache error \n\n";
}
if($oGlobalFramework->config['cache_config']['type'] == 'redis'){
	\YcheukfCommon\Lib\Functions::flushCache($oGlobalFramework->sm);
	echo "\nflash redis done\n";
}
session_destroy();