<?php
//框架文件
set_time_limit(0);
ini_set("auto_detect_line_endings", "1");

chdir(dirname(dirname(__DIR__)));
defined('BASE_INDEX_PATH') || define('BASE_INDEX_PATH', dirname(dirname(dirname(__FILE__)))."/public");
defined('LAF_NAMESPACE') || define('LAF_NAMESPACE', ucfirst(getenv("LAF_project_namespace")));

include("init_autoloader.php");
defined('APPLICATION_MODEL') || define('APPLICATION_MODEL', "CONSOLE");
//var_dump(require 'config/application.config.php');

$oGlobalFramework = new GlobalFramework();
// var_dump($oGlobalFramework->pdo);

if (class_exists('\YcheukfDebug\Model\Debug')) {
	\YcheukfDebug\Model\Debug::setDumpFlag(false);//关掉debug
}

class GlobalFramework{
	var $app;
	var $sm;
	var $config;
	// var $pdo=null;
	var $dbname;
	var $dbhost;
	public function __construct(){
		// 性能杀手
		$this->app = \Zend\Mvc\Application::init(require 'config/console.config.php');

		$this->sm = $this->app->getServiceManager();
		$this->config = $this->app->getServiceManager()->get('config');
		// var_dump($this->config['3rdschedule']);
		// $this->initPdo();


//	$oGlobalApp = \Zend\Mvc\Application::init(require 'config/console.config.php');
//	$aGlobalAppConfig = $oGlobalApp->getServiceManager()->get('config');
//	$oGlobalPDO =  @new PDO($aGlobalAppConfig['db_master']['dsn'], $aGlobalAppConfig['db_master']['username'], $aGlobalAppConfig['db_master']['password'], array(PDO::ATTR_PERSISTENT => true));
//	@$oGlobalPDO->query("set names UTF8");
//here
//	
	}
	function queryPdo($sSql){
		$rs = @$this->pdo->query($sSql, \PDO::FETCH_ASSOC);
		if("00000" != $this->pdo->errorCode()){//mysql出错, 重新初始化对象, 避免长时间链接导致断开问题
	//		var_dump(__FUNCTION__.": error");
			$this->pdo =  $this->initPdo();
			$rs = @$this->pdo->query($sSql, \PDO::FETCH_ASSOC);
			if("00000" != $this->pdo->errorCode()){//依然出错, 跳出
				throw new Exception("\n".join("\n", $this->pdo->errorInfo()));
			}
		}
		return $rs;
	}
	function __get($name='')
	{
		switch ($name) {
			case 'pdo':
				$this->initPdo();
				return $this->pdo;
				break;
			default:
				break;
		}
	}

	function initPdo(){
        $aDsn = \YcheukfCommon\Lib\Functions::getParamFromDSN($this->config['db_master']['dsn']);
        system("mysql -u".$this->config['db_master']['username']." -p".$this->config['db_master']['password']." --port=".$aDsn['port']." -h".$aDsn['host']."  -e \"create database if not exists ".$aDsn['dbname']."\"");
		$this->dbname = $aDsn['dbname']; 
		$this->dbhost = $aDsn['host'];
		$this->dbport = $aDsn['port'];
		$oGlobalPDO =  @new PDO($this->config['db_master']['dsn'], $this->config['db_master']['username'], $this->config['db_master']['password'], array(PDO::ATTR_PERSISTENT => true));
		@$oGlobalPDO->query($this->config['db_master']['driver_options'][\PDO::MYSQL_ATTR_INIT_COMMAND]);

		$this->pdo = $oGlobalPDO;
	}


}

function myFileGetContents($sUrl, $sProxyIp=""){
	if (filter_var($sUrl, FILTER_VALIDATE_URL) === false){
		$sReturn = file_get_contents($sUrl);
		return trim($sReturn);
	}else{
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $sUrl);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		//设置curl默认访问为IPv4
		if(defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')){
		    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		}

        curl_setopt($ch, CURLOPT_HEADER, 0);

		if(!empty($sProxyIp)){
		    curl_setopt($ch, CURLOPT_PROXY, $sProxyIp);
		}
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        $sUa = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36';
        curl_setopt($ch, CURLOPT_USERAGENT, $sUa); 


		//设置curl请求连接时的最长秒数，如果设置为0，则无限
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 30);
		//设置curl总执行动作的最长秒数，如果设置为0，则无限
		curl_setopt ($ch, CURLOPT_TIMEOUT, 30);
		$file_contents = curl_exec($ch);
        $info = curl_getinfo($ch); 
		// print_r($info);
		curl_close($ch);
		return trim($file_contents);
	}
}

function echoMsg($sMsg, $sDesc=''){
    echo "[".date('Y-m-d H:i:s')."] {$sDesc} ".(is_array($sMsg) ? "\n".print_r($sMsg, 1): $sMsg)."\n";
}

function cronjobMsg($sMsg, $title=''){
    if(is_array($sMsg)){
        $mutilevel = false;
        $msg2 = [];
        foreach($sMsg as $i => $v){
            if (is_array($v)) {
                $mutilevel = true;
                break;
            }
            $msg2[] = $i."=>".$v;

        }
        if ($mutilevel) {
            $sMsg = print_r($sMsg, 1);
        }else{
            $sMsg = join(", ", $msg2);
        }

    } 
    echo "[".date('Y-m-d H:i:s')."] [{$title}] ".$sMsg."\n";
}




/*

	//oauth 登陆 函数
	function oauthlogin(){
		$post_data = array();
		$post_data['grant_type'] = $this->config['api_oauth']["grant_type"];
		$post_data['client_id'] = $this->config['api_oauth']["client_id"];
		$post_data['client_secret'] = $this->config['api_oauth']["client_secret"];
		$post_data['username'] = $this->config['api_oauth']["username"];
		$post_data['password'] = $this->config['api_oauth']["password"];
		$login_url = $this->config['api_oauth']["login_url"];
		$o="";
		foreach ($post_data as $k=>$v)
		{
			$o.= "$k=".urlencode($v)."&";
		}
		$post_data=substr($o,0,-1);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
		curl_setopt($ch, CURLOPT_POST, TRUE); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); 
		curl_setopt($ch, CURLOPT_URL, $login_url);
		$result =curl_exec($ch);
		curl_close($ch);
		$aJson = json_decode($result, 1);
		if($aJson['status'] !=1){
			echo  "login error!\n";
			var_export($login_url);
			var_export($result);
			exit;
		}
		return $aJson['data'];
	}
	//上传图片
	function uploadFile($aTmp){
		$aReturn = array();
        $aM151 = \Application\Model\Common::getResourceMetaData($this->sm, 151, 1);
		foreach($aTmp as $sSrc){
			 $post = array(
				"uploadfile"=>"@".__DIR__ .$sSrc,
				"sp"=>$aM151['dataset'][0]['label'],
			);
			$aReTmp = $this->callApiByCurl($this->config['apiservice']['base_location']."/imgupload", $post , 'curl');
			if($aReTmp['status']!=1){
				var_dump($aTmp);
				var_dump($aReTmp);
				echo "upload error";
				exit;
			}
			$aReturn[] = $aReTmp['data'];
		}
		return $aReturn;
	}

	//通过curl请求api函数
	function callApiByCurl($sUrl, $aParams, $type="function"){
		if($type == "curl"){
			$aPost = array();
			foreach ($aParams as $k=>$v)
			{
				$sTmp = (is_array($v)?json_encode($v) : $v);
				$aPost[$k] = $sTmp;
			}
			$ch = curl_init();
			$header = array('Content-Type: multipart/form-data');
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
			curl_setopt($ch, CURLOPT_POST, TRUE); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $aPost); 
			curl_setopt($ch, CURLOPT_URL, $sUrl);
			$result =curl_exec($ch);
			curl_close($ch);
			$aJson = json_decode($result, 1);
			if(is_null($aJson)){
				echo "error happend: \n\n";
				var_dump($sUrl);
				var_dump($aPost);
				var_export($result);
				exit;
			}
			return $aJson;
		}else{
			$sOpurl = str_replace($this->config['apiservice']['base_location']."/", '', $sUrl);
	//		var_dump($aParams);
	//		var_dump($sOpurl);
			list($sType, $sResource, $sOp) = explode("/", $sOpurl);
			$aParams['op'] = $sOp;
			$aParams['resource'] = $sResource;
			$aParams['resource_type'] = $sType;
	//		exit;
			return \YcheukfCommon\Lib\Functions::getDataFromApi($this->app->getServiceManager(), $aParams, 'function');	
		}
	}

	function _getResourceList($resource, $sWhere=null){
	    global $sLocationBase, $sToken,$oGlobalFramework;
	    $aParam = array(
	        "access_token" => $sToken,
	    //        "params" => array(),
	        "params" => array(
	            "select" => array(
	            ),
	        ),
	    );
	    if(!is_null($sWhere))
	        $aParam['params']['select']['where'] = $sWhere;
	    $aResultTmp = $oGlobalFramework->callApiByCurl($sLocationBase."/common/".$resource."/get", $aParam);
	    return $aResultTmp['data']['dataset'];
	}


	function _getMetadataBy($type){
	    global $sLocationBase, $sToken,$oGlobalFramework;
	    $aResultTmp = $oGlobalFramework->callApiByCurl($sLocationBase."/common/metadata/get", array(
	        "access_token" => $sToken,
	    //        "params" => array(),
	        "params" => array(
	            "select" => array(
	                'where' => array('type'=>$type)
	            ),
	        ),
	    ));
	    if($aResultTmp['status'] != 1){
	        var_dump($aResultTmp);
	        exit;
	    }
	    return $aResultTmp['data']['dataset'];
	}

	function _demo_save($aRssData, $sLocationBase, $sToken, $sResource=''){
	    global $oGlobalFramework;
	    //更新catalog表
	    $nRepeartTime = 1;
	    $nOffset = 10;
	    echo("".$sResource."\n");

	    for($n=0; $n<$nRepeartTime; $n++){
	        $aTmp = array();
	        $nIndex = 0;
	        foreach($aRssData as $i=>$aTmp2){
	//        for($i=0; $i<count($aRssData); $i++){
	            $nIndex++;
	            unset($aRssData[$i]['link']);
	            $aTmp[] = $aRssData[$i];
	            if($nIndex>=$nOffset || $i>=count($aRssData)-1){
	                try{
	                    $result = $oGlobalFramework->callApiByCurl($sLocationBase."/common/".$sResource."/save", array(
	                        "access_token" => $sToken,
	                        "params" => array("dataset" => $aTmp),
	                    ));
	                    $nIndex = 0;
	                    $aTmp = array();
	                }catch(\YcheukfCommon\Lib\Exception $e){
	                    echo ("\nerror\n:".$e->getMessage()."\n");
	                }
	            }
	        }
	    }
	}	
 */