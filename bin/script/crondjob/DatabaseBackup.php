<?php
/*
数据库备份  

**/
class DatabaseBackup extends \YcheukfCommon\Lib\Crondjob{



    function go(){
        

        $sCacheKey = date("Ymd");
        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_DATABACKUP_201, $sCacheKey);
        if (count($aMd5KeyData)) {//今天已经
            echoMsg('['.__CLASS__.']already done');
            return true;
        }

        
        //记录发送记录
        \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_DATABACKUP_201, $sCacheKey, "");



        $sExpiredDate = date("YmdHis");
        $sBackupDir = BASE_INDEX_PATH."/../data/databasebackup";
        exec("mkdir -p ".$sBackupDir);
        $sBackupFile = $sBackupDir."/".$sExpiredDate;
        $nBackupDay = 60;//历史备份文件保存时间
        $nTriggerDate = date("Ymd", strtotime("-".$nBackupDay." day"));


        $sDirPath = $sBackupDir;
        if ($dHandle = opendir($sDirPath)) 
        {
          while (($file = readdir($dHandle)) !== false)
          {
        
            if(preg_match("/^\d{14}.tar.gz$/i", $file))
            {
                $sDateString = str_replace(".tar.gz", "", $file);
                $sDateString = substr($sDateString, 0,8);
                if ($nTriggerDate > $sDateString) {
                    echoMsg('['.__CLASS__.']delete old backupfile:'.$file);
                    exec("rm -f ".$sBackupDir."/".$file);
                }
            }
          }
        }
        closedir($dHandle);
        
        
        



        $aDbConfig = $this->serviceManager->get('config');
        $aDsn = \YcheukfCommon\Lib\Functions::getParamFromDSN($aDbConfig['db_master']['dsn']);
        $sDbName = $aDsn['dbname']; 
        $sDbHost = $aDsn['host']; 
        $sDbPort = $aDsn['port']; 
        $sDbUsername = $aDbConfig['db_master']['username'];
        $sDbPassword = $aDbConfig['db_master']['password'];



        $sScript = "mysqldump  --add-drop-table -h$sDbHost -u$sDbUsername -p$sDbPassword --port=$sDbPort $sDbName --ignore-table=$sDbName.system_trigger_log > $sBackupFile";
        exec($sScript);
        echoMsg('['.__CLASS__.']:'.$sScript);
        if (file_exists($sBackupFile)) {
            $sScript = "tar -czf $sBackupFile.tar.gz $sBackupFile; rm -f $sBackupFile";
            exec($sScript);
        }


        return true;
    }

}

