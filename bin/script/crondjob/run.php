<?php
include_once(__DIR__."/../config_inc.php");

//关闭debug
if(class_exists('\YcheukfDebug\Model\Debug'))
    \YcheukfDebug\Model\Debug::setDumpFlag(false);

$sScriptIndex = isset($_SERVER['argv'][1]) ? strval($_SERVER['argv'][1]) : null;
if (!is_null($sScriptIndex) && !isset($oGlobalFramework->config['crondjob']['aGlobalScriptIndex'][$sScriptIndex])) {
    print_r($oGlobalFramework->config['crondjob']['aGlobalScriptIndex']);
    print_r(array_keys($oGlobalFramework->config['crondjob']['aGlobalScriptIndex']));
    exit;
}

try{

    // 如果不存在此触发器表则报错
    $sSql = "SHOW TABLES LIKE '" . $oGlobalFramework->config['crondjob']['sGlobalTriggerTalbe'] . "'";
    $oPDOStatement = $oGlobalFramework->queryPdo($sSql);
    // var_dump($oPDOStatement->rowCount());
    if ($oPDOStatement && $oPDOStatement->rowCount() < 1) {
        throw new exception("ERROR: table ".$oGlobalFramework->config['crondjob']['sGlobalTriggerTalbe']." dose not exist!");
    }


    echoMsg( "laf crondjob is running now!");
    $oInstants = array();
    $nLastInitDbTime = time();

    while (true){//死循环, 监听trigger信息以同步
        $nRuntime = time();
        if ($nRuntime - $nLastInitDbTime > 3600*3) {
            echoMsg("restart job Interval");
            exit;

            // $oGlobalFramework->initPdo();
            // $nLastInitDbTime = $nRuntime;
            // echoMsg("reconnect db");
        }

        $bSleepFlag = true;//睡眠标识, 在所有脚本都没有操作记录时触发睡眠
        foreach($oGlobalFramework->config['crondjob']['aGlobalScriptIndex'] as $nIndex => $aTmp){
            if (!is_null($sScriptIndex) && $sScriptIndex!==strval($nIndex)) {//debug model
                continue;
            }
            include_once($aTmp[0]);
            $aScriptPath = pathinfo($aTmp[0]);
            $sClassName = $aScriptPath['filename'];
            $nOffsetTime =  0;
            $bCronFalg = false; //是否指定时间执行

            if (is_null($sScriptIndex)) {
               if (isset($aTmp[1])) {
                    if (is_int($aTmp[1])) {
                        $nOffsetTime = $aTmp[1];
                    }elseif (preg_match("/H\d+/i", $aTmp[1])) {//指定小时执行


                        $nRunHour = str_replace("H", "", $aTmp[1]);
                        if ($nRunHour != date("G")) {
                            continue;
                        }else{

                            $sCacheKey = $nIndex."-".date("Ymd", $nRuntime)."-".$aTmp[1];
                            $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($oGlobalFramework->sm, 104, $sCacheKey);

                            if (count($aMd5KeyData)) {//当天已经被执行
                                continue;
                            }else{//到点执行

                                $nOffsetTime = 7200;
                                $bCronFalg = true;
                            }
                        }
                    }
                }
            }

            if (!isset($oInstants[$sClassName])) {
                $oInstants[$sClassName] = new $sClassName($oGlobalFramework);
            }
            // 获取下一次执行时间
            $nLastRunTime = $oInstants[$sClassName]->getLastRunTime();

            //检查是否到达触发事件  非定时事件 && 尚未到达启动时间
            if($bCronFalg==false && $nLastRunTime > $nRuntime){
                continue;
            }

            if(@$oGlobalFramework->pdo->getAttribute(PDO::ATTR_SERVER_INFO) == false)
                throw new exception("connect Mysql faild");


            
            // echoMsg("[$sClassName] starting");
            $bScript = $oInstants[$sClassName]->go();//返回false时没有操作记录

            if ($bCronFalg) {
                \YcheukfCommon\Lib\Functions::saveLafCacheData($oGlobalFramework->sm, LAF_MD5KV_SCRIPTHOUR, $sCacheKey, "");
            }

            if (!is_null($sScriptIndex)) {//指定执行脚本, 则退出
                exit;
            }
            $oInstants[$sClassName]->updateTimer($nOffsetTime);
            $bSleepFlag = $bSleepFlag && !$bScript;//所有的脚本都不许要执行, 进行休眠
        }
        if($bSleepFlag){//定时休眠
//            echoMsg( "sleep:".$oGlobalFramework->config['crondjob']['nGlobalFrequency']);
            sleep($oGlobalFramework->config['crondjob']['nGlobalFrequency']);
        }

    }
}catch (Exception $e) {
    $sError = date("Y-m-d H:i:s")."\n============Error ! ============\n----file line----\n".$e->getFile()." @ line ".$e->getLine()."\n----message----\n".$e->getMessage()."\n----trace----\n".$e->getTraceAsString ()."\n\n";
    echoMsg("Exception: \n\n" .$sError);
}catch (PDOException $e) {
    $sError = date("Y-m-d H:i:s")."\n============Error ! ============\n----file line----\n".$e->getFile()." @ line ".$e->getLine()."\n----message----\n".$e->getMessage()."\n----trace----\n".$e->getTraceAsString ()."\n\n";
    echoMsg( "pdoexception: \n\n" . $e->getMessage());
}