<?php
/**
 * 根据 id,url 将排期数据导出成 [批量导入排期文件]
 */
include_once(__DIR__."/config_inc.php");


$aSearchData = array(
	// 3,
	3800,
	107,
	108,
	'http://vletv.admaster.com.cn/i/a63302,b902341,c2751,i0,m202,h',
	'http://baidu.com'
);


$aCsvHeader = array(
	0 => 'userid',
	1 => 'codelabel',
	2 => 'show_code',
	3 => 'click_code',
	4 => 'servers',
	5 => 'UA',
	6 => 'cities',//
	7 => 'refferer',
	8 => 'ip frequency',
	9 => 'cookie frequency',
	10 => 'show_activity',
	11 => 'click_activity',
	12 => 'date-start-column',
);


function fmtCsvRow($aEntity, $oGlobalFramework, $sStartDate, $sEndDate)
{
	$aReturn = array();
	if (empty($aEntity) || is_null($aEntity)) {
		$aReturn = array();
	}else{
		$aRelation1007 = \Application\Model\Common::getRelationList($oGlobalFramework->sm,1007, $aEntity['id']);//city
		$aRelation1008 = \Application\Model\Common::getRelationList($oGlobalFramework->sm,1008, $aEntity['id']);//ua
		$aRelation1009 = \Application\Model\Common::getRelationList($oGlobalFramework->sm,1009, $aEntity['id']);//server
        // var_dump($aRelation1008);
// exit;
		$aReturn = array(
			0 => $aEntity['user_id'],
			1 => empty($aEntity['code_label']) ? "" : iconv("utf8", "GBK", $aEntity['code_label']),
			2 => $aEntity['code'],
			3 => '',
			4 => json_encode($aRelation1009),
			5 => json_encode($aRelation1008),
			6 => json_encode($aRelation1007),//
			7 => $aEntity['code'],
			8 => 1000,
			9 => 1,
			10 => $aEntity['m102_id'],
			11 => '',
			12 => 'date-start-column',
		);
		$aJson = json_decode($aEntity['schedule']);
		$aDate2Count = array();
		if (!is_null($aJson) && count($aJson)) {
			foreach ($aJson as $k2 => $v2) {
				$aReturn[8] = $v2[2];
				$aReturn[9] = $v2[1];
                $aTmp4 = explode(" ", $v2[0]);
                if(preg_match("/\,/", $aTmp4[0])){
                    list($sStartDateTmp,$sEndDateTmp) = explode(",", $aTmp4[0]);
                }else{
                    $sStartDateTmp = $sEndDateTmp = $aTmp4[0];
                }
		        while (strtotime($sStartDateTmp) <= strtotime($sEndDateTmp)) {
		        	if (!empty($v2[3])) {
			        	$aDate2Count[$sStartDateTmp] = $v2[3];
		        	}
					$sStartDateTmp = date("Y-m-d", strtotime("+1 day", strtotime($sStartDateTmp)));
		        }		
			}
		}
		
		// var_dump($sStartDate, $sEndDate);
  //       var_dump($aDate2Count);
        while (strtotime($sStartDate) <= strtotime($sEndDate)) {
        	if (isset($aDate2Count[$sStartDate])) {
	        	$aReturn[] = $aDate2Count[$sStartDate];
        	}else{
	        	$aReturn[] = "";
        	}
        	// $aReturn[] = "";//click

			$sStartDate = date("Y-m-d", strtotime("+1 day", strtotime($sStartDate)));
        }

	}
	return $aReturn;
		# code...
}
function getStartEndDate($sStartDate, $sEndDate, $aEntity){
	if (!isset($aEntity['schedule'])) {
		return array($sStartDate,$sEndDate);
	}
	$aJson = json_decode($aEntity['schedule']);
	$aDate2Count = array();
	if (!is_null($aJson) && count($aJson)) {
		foreach ($aJson as $k2 => $v2) {
            $aTmp4 = explode(" ", $v2[0]);
            if(preg_match("/\,/", $aTmp4[0])){
                list($sStartDateTmp,$sEndDateTmp) = explode(",", $aTmp4[0]);
            }else{
                $sStartDateTmp = $sEndDateTmp = $aTmp4[0];
            }
            $sStartDate = strtotime($sStartDateTmp)<=strtotime($sStartDate) ? $sStartDateTmp : $sStartDate;
            $sEndDate = strtotime($sEndDateTmp)>=strtotime($sEndDate) ? $sEndDateTmp : $sEndDate;
		}
	}
	return array($sStartDate, $sEndDate);
}
$aSearchData2 = array();
$sStartDate = $sEndDate = date("Y-m-d");
foreach ($aSearchData as $k1 => $v1) {
	if (preg_match('/^\d+$/i', $v1)) {
		$aSearchData2[$v1] = $aEntity = \YcheukfCommon\Lib\Functions::getResourceById($oGlobalFramework->sm, 'adschedule', trim($v1), 'id');
		list($sStartDate, $sEndDate) = getStartEndDate($sStartDate, $sEndDate, $aEntity);

	}else if(filter_var($v1, FILTER_VALIDATE_URL) !== false) {
		$aSearchData2[$v1] = $aEntity = \YcheukfCommon\Lib\Functions::getResourceById($oGlobalFramework->sm, 'adschedule', trim($v1), 'code');
		list($sStartDate, $sEndDate) = getStartEndDate($sStartDate, $sEndDate, $aEntity);
	}else{
		$aSearchData2[$v1] = array();
	}
}

//补全头的日期
$sStartDateTmp = $sStartDate;
while (strtotime($sStartDateTmp) <= strtotime($sEndDate)) {
	$aCsvHeader[] = $sStartDateTmp."|show";
	// $aCsvHeader[] = $sStartDateTmp."|click";
	$sStartDateTmp = date("Y-m-d", strtotime("+1 day", strtotime($sStartDateTmp)));
}

// var_dump($aCsvHeader);
$aRowData = array();
foreach ($aSearchData2 as $k2 => $v2) {
	if (is_null($v2) || empty($v2)) {
		$aRowData[] = array($k2);
	}else{
		$aRowData[] = fmtCsvRow($v2, $oGlobalFramework, $sStartDate, $sEndDate);
	}
}

exec("mkdir -p ".BASE_INDEX_PATH."/../data/export_adschedule/");
$sFile = BASE_INDEX_PATH."/../data/export_adschedule/".uniqid().".csv";
$hFile = fopen($sFile, "w");
foreach (array_merge(array($aCsvHeader), $aRowData) as $k3 => $v3) {
	// var_dump($v3);
	fwrite($hFile, '"'.join('","', $v3).'"'."\n");
}
fclose($hFile);
echo($sFile)."\n";

echo "DONE\n\n";


