<?php
session_start();
include_once(__DIR__."/config_inc.php");
$sContentSh = "";
$sContentSh = "#!//bin/bash\n";
$sTmp = <<<OUTPUT
php ./script/apiclientdemo/index.php

OUTPUT;
$sContentSh .= $sTmp;


$aApplicationConfig = require(__DIR__."/../../config/application.config.php");
foreach($aApplicationConfig['modules'] as $s){
    $sPath = __DIR__."/../../module/".$s."/migrations";
    if(file_exists($sPath)){
        $sTmp = <<<OUTPUT
php ../module/{$s}/bin/apiclientdemo/index.php

OUTPUT;
        $sContentSh .= $sTmp;
    }
}


$sContentSh .= "\n";
$sFileName = __DIR__."/../../data/setup_db_demodata.run.sh";
$fp = fopen($sFileName, 'w');
fwrite($fp, $sContentSh);
fclose($fp);


echo " generate demodata script done !\n\n";
