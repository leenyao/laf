<?php
session_start();
include_once(__DIR__."/config_inc.php");
$sContentSh = "";
$sContentSh = "#!//bin/bash\n";

// 是否需要重置
$bResetFlag = isset($argv[1]) ? $argv[1] : false;


$sResetScript = $bResetFlag ? "php public/index.php migration down db_master --dir=../migrations/default" : "";
$sTmp = <<<OUTPUT
cd ..
{$sResetScript}
php public/index.php migration up db_master --dir=../migrations/default

OUTPUT;
$sContentSh .= $sTmp;


$aApplicationConfig = require(__DIR__."/../../config/application.config.php");
foreach($aApplicationConfig['modules'] as $s){
    $sPath = __DIR__."/../../module/".$s."/migrations";
    if(file_exists($sPath)){

    	$sResetScript = $bResetFlag ? "php public/index.php migration down db_master --dir=../module/{$s}/migrations" : "";

        $sTmp = <<<OUTPUT
{$sResetScript}
php public/index.php migration up db_master --dir=../module/{$s}/migrations

OUTPUT;
        $sContentSh .= $sTmp;
    }
}


$sContentSh .= "cd bin\n";
$sFileName = __DIR__."/../../data/migration.run.sh";

// var_dump($sContentSh);
// exit;
$fp = fopen($sFileName, 'w');
fwrite($fp, $sContentSh);
fclose($fp);


echo " generate migration script done !\n\n";
