<?php

$sBaseDir = __DIR__."/../../";
$sModuleDir = __DIR__."/../../module";
$nParamIndex = 1;
$sModuleName = null;
if (isset($_SERVER['argv'][$nParamIndex])) {
    $sModuleName = $_SERVER['argv'][$nParamIndex];
    if (!preg_match("/^\w+$/i", $sModuleName)) {
        echo ("wrong param[".$nParamIndex."]:".$sModuleName."\n");
        exit;
    }
}else{

    echo ("need param[".$nParamIndex."]\n");
    exit;
}

function echoMsg($sMsg){
    echo "[".date('Y-m-d H:i:s')."]  ".(is_array($sMsg) ? json_encode($sMsg): $sMsg)."\n";
}


$sTargetModuleDir = $sModuleDir."/".ucfirst(strtolower($sModuleName));
if (!file_exists($sTargetModuleDir)) {
    exec("mkdir -p ".$sTargetModuleDir);
    // exec("rm -fR ".$sTargetModuleDir."/*");
    exec("cp -R ".$sModuleDir."/Demo/* ".$sTargetModuleDir."/");
    exec("mv ".$sModuleDir."/Demo/src/Demo ".$sTargetModuleDir."/src/".ucfirst(strtolower($sModuleName)));
    // exec("rm -fR ".$sModuleDir."/Demo/src/Demo");

    echoMsg("sModuleName=".$sModuleName);
    echoMsg("Demo=".$sModuleDir."/Demo/src/Demo");
    echoMsg("sTargetModuleDir=".$sTargetModuleDir);

    // 替换命名空间
    renameModuleSpace($sTargetModuleDir);

    echoMsg("DONE");
}else{
    echoMsg($sTargetModuleDir." is already exist");
}

function renameModuleSpace($dir)
{
    global $sModuleName;
    if(!is_dir($dir))
      return false;
    $handle=opendir($dir);          //打开目录
    while(($file=readdir($handle))!==false)
    {
          
        if($file=='.'||$file=='..')
        {
           continue;
        }
        $file=$dir.DIRECTORY_SEPARATOR.$file;
        if(is_file($file))                 //是文件就输出
        {
            $sContent = file_get_contents($file);
            $sContent = preg_replace("/namespace\s+Demo/", "namespace ".ucfirst(strtolower($sModuleName)), $sContent);
            file_put_contents($file, $sContent);

           print $file."\n";
        }
        elseif(is_dir($file))
        {
         
          renameModuleSpace($file);          //递归查询
        }
    }
    closedir($handle);                 //关闭目录
}
