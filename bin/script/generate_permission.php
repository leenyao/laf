<?php
include_once(__DIR__."/config_inc.php");


$nParamIndex = 1;
$sVarName = null;
if (isset($_SERVER['argv'][$nParamIndex])) {
    $sVarName = $_SERVER['argv'][$nParamIndex];
    if (!preg_match("/^\d$/i", $sVarName)) {
        echo ("wrong param[".$nParamIndex."]:".$sVarName."\n");
        exit;
    }
}else{
    $sScriptFile = __FILE__;
    echo <<<OUTPUT
process 
    php {$sScriptFile} 1

OUTPUT;
    exit;
}


$aConfig = $oGlobalFramework->sm->get("config");
$aSql4Insert = array();
if (isset($aConfig['module_permissions'])) {

    if (is_array($aConfig['module_permissions']['roles']) && count($aConfig['module_permissions']['roles'])) {
        $aSql4Insert[] = "delete from  `rbac_role` where id>100";
        foreach ($aConfig['module_permissions']['roles'] as $sKtmp1 => $aRowTmp1) {
            $aSql4Insert[] = "insert into `rbac_role` (`id`, `role_name`) values (".$sKtmp1.", '".$aRowTmp1."')";
        }
    }

    if (is_array($aConfig['module_permissions']['resource']) && count($aConfig['module_permissions']['resource'])) {
        $aSql4Insert[] = "delete from  `rbac_permission` where id>0";
        foreach ($aConfig['module_permissions']['resource'] as $sKtmp1 => $aRowTmp1) {
            if (preg_match("/\//i", $aRowTmp1)) {//直接写的resoucee
                $aSql4Insert[] = "INSERT INTO `rbac_permission` (perm_name, status, perm_label) VALUES ( '".$aRowTmp1."', '1', '".$aRowTmp1."')";
            }else{
                $aSql4Insert[] = "INSERT INTO `rbac_permission` (perm_name, status, perm_label) VALUES ( 'zfcadmin/contentedit/".$aRowTmp1."', '1', 'edit')";
                $aSql4Insert[] = "INSERT INTO `rbac_permission` (perm_name, status, perm_label) VALUES ('zfcadmin/contentedit/".$aRowTmp1."', '1', 'view')";
                $aSql4Insert[] = "INSERT INTO `rbac_permission` (perm_name, status, perm_label) VALUES ('zfcadmin/contentlist/".$aRowTmp1."', '1', 'view')";

            }
        }
        $aSql4Insert[] = "INSERT INTO `rbac_permission` (perm_name, status, perm_label) VALUES ('zfcadmin/userprofile', '1', 'view')";

    }

    $oPdo = \Application\Model\Common::getPDOObejct($oGlobalFramework->sm, "db_master"); //db_master|db_slave

    if (is_array($aSql4Insert) && count($aSql4Insert)) {
        foreach ($aSql4Insert as $sKtmp1 => $sSql) {
            $oSth = $oPdo->prepare($sSql);
            $oSth->execute(array(
            ));
        }
    }


    $aSql4Insert2 = array();
    if (is_array($aConfig['module_permissions']['relations']) && count($aConfig['module_permissions']['relations'])) {
        $aSql4Insert2[] = "delete from  `rbac_role_permission` where id>0";
        foreach ($aConfig['module_permissions']['relations'] as $sKtmp1 => $aRowTmp1) {
            foreach ($aRowTmp1 as $sResourceTmp) {
                if (preg_match("/\//i", $sResourceTmp)) {//直接写的resoucee

                    $sSql = "select id from rbac_permission where perm_name='".$sResourceTmp."'";
                    $oSth = $oPdo->prepare($sSql);
                    $oSth->execute(array(
                    ));
                    $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
                    $aCounterRanking = array();
                    foreach ($aResultTmp as $aRowTmp) {
                        $aSql4Insert2[] = "INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (".$sKtmp1.", ".$aRowTmp['id'].")";
                    }
                }else{

                    $sSql = "select id from rbac_permission where perm_name='zfcadmin/contentedit/".$sResourceTmp."'";
                    $oSth = $oPdo->prepare($sSql);
                    $oSth->execute(array(
                    ));
                    $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
                    $aCounterRanking = array();
                    foreach ($aResultTmp as $aRowTmp) {
                        $aSql4Insert2[] = "INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (".$sKtmp1.", ".$aRowTmp['id'].")";
                    }

                    $sSql = "select id from rbac_permission where perm_name='zfcadmin/contentlist/".$sResourceTmp."'";
                    $oSth = $oPdo->prepare($sSql);
                    $oSth->execute(array(
                    ));
                    $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
                    $aCounterRanking = array();
                    foreach ($aResultTmp as $aRowTmp) {
                        $aSql4Insert2[] = "INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (".$sKtmp1.", ".$aRowTmp['id'].")";
                    }

                    $sSql = "select id from rbac_permission where perm_name='zfcadmin/userprofile'";
                    $oSth = $oPdo->prepare($sSql);
                    $oSth->execute(array(
                    ));
                    $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
                    $aCounterRanking = array();
                    foreach ($aResultTmp as $aRowTmp) {
                        $aSql4Insert2[] = "INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (".$sKtmp1.", ".$aRowTmp['id'].")";
                    }
                    
                }


            }


        }

        if (is_array($aSql4Insert2) && count($aSql4Insert2)) {
            foreach ($aSql4Insert2 as $sKtmp1 => $sSql) {
                $oSth = $oPdo->prepare($sSql);
                $oSth->execute(array(
                ));
            }
        }

    }
}
    print_r($aSql4Insert);
    print_r($aSql4Insert2);


echoMsg("DONE");