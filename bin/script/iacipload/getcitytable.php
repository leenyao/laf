<?php
/**
 * 生成iac地域编码表
 * 
 */

include_once(__DIR__."/../config_inc.php");



$datacity1 = file('/app/bin/script/iacipload/city.csv');
$nIndex = 0;
$datacity = array();
foreach((array)$datacity1 as $value){
	if ($nIndex++ === 0) {
		continue;
	}
	$string = mb_convert_encoding(trim(strip_tags($value)), 'utf-8', 'gbk');
	$v = explode(',', trim($value));
    if (preg_match("/^.*市$/i", $v[1])) {
        $v[1] = preg_replace("/^(.*)市$/i", '\1', $v[1]);
    }

    if (preg_match("/^.*市$/i", $v[2])) {
        $v[2] = preg_replace("/^(.*)市$/i", '\1', $v[2]);
    }

	$a["citycode"] = $v[0];
	$a["citylabel"] = $v[1];
	$a["fcitylabel"] = $v[2];
	$datacity[] = $a;
}

$table = "location_iaccode";
$sql = "
SET NAMES 'UTF8';
drop table if EXISTS location_iaccode;

CREATE TABLE  IF NOT EXISTS {$table} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` bigint(11) NOT NULL,
  `label` varchar(100) NOT NULL DEFAULT '',
  `fcode` bigint(11) NOT NULL DEFAULT 0,
  `flabel` varchar(100) NOT NULL DEFAULT '',
  `mapresid` varchar(100) NOT NULL DEFAULT '' COMMENT '对应的resid',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `mapresid` (`mapresid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'iac提供的地域编码表';

";
// $oGlobalFramework->pdo->query($sql);  
echo $sql."\n";
// exit;
$sSqlConntent = "truncate table $table;\n";
for ($i = 0; $i < count($datacity); $i++){
    $keys = "code,label,flabel";  
    $vals = "'".join("','",array_values($datacity[$i]))."'"; 
	$sql = "insert {$table}({$keys})values({$vals})";  
	// echo $sql."\n";
	$sSqlConntent .= $sql.";\n";
	// $oGlobalFramework->pdo->query($sql);  
}
echo $sSqlConntent."\n";
