<?php
include_once(__DIR__."/../config_inc.php");



$datacity1 = file('/app/bin/script/iacipload/city.csv');
foreach((array)$datacity1 as $value){
	$string = mb_convert_encoding(trim(strip_tags($value)), 'utf-8', 'gbk');
	$v = explode(',', trim($value));
	$a["citycode"] = $v[0];
	$a["citylabel"] = $v[1];
	$datacity[] = $a;

}
$dataip1 = file('/app/bin/script/iacipload/ip.csv');
foreach((array)$dataip1 as $value){
	$string = mb_convert_encoding(trim(strip_tags($value)), 'utf-8', 'gbk');
	$v = explode(',', trim($value));
	$s = explode('.', trim($v[0]));
	$e = explode('.', trim($v[1]));
	$b["startip"] = $v[0];
	$b["endip"] = $v[1];
	$b["start"] = $s[0]*0x1000000+$s[1]*0x10000+$s[2]*0x100+$s[3];
	$b["end"] = $e[0]*0x1000000+$e[1]*0x10000+$e[2]*0x100+$e[3];
	$b["citycode"] = $v[2];
	$dataip[] = $b;

}

$all=_mergerArray($dataip,$datacity,"citycode","citycode");
$table="b_iacip_list";
$sSqlConntent = "SET NAMES 'UTF8';\n";
$sSqlConntent .= "truncate table $table;\n";
for ($i = 0; $i < count($all); $i++){
    $keys = join(",",array_keys($all[$i]));  
    $vals = "'".join("','",array_values($all[$i]))."'"; 
	$sql = "insert {$table}({$keys})values({$vals})";  
	$sSqlConntent .= $sql.";\n";
	// $oGlobalFramework->pdo -> query($sql);  
}

file_put_contents("/app/module/Lur/migrations/iacip.sql", $sSqlConntent);
echo $sSqlConntent;


function _mergerArray($array1, $array2, $field1, $field2 = '') { 
	$ret = array(); 
	//使用数组下标的办法 
	foreach ($array2 as $key => $value) { 
		$array3[$value[$field1]] = $value; 
	} 
	foreach ($array1 as $key => $value) { 
		$ret[] = array_merge($array3[$value[$field1]], $value); 
	} 
	return $ret; 
}
