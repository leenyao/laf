
## 目录说明

 - example 样例目录
 - lib SDK库


## 使用注意(重要)

example 中的 LURSDK_APPID, LURSDK_APPSCRECT 为DEMO账号, 供对接调试使用, 该账号不会真正的产生投放.
上线后请将两者替换成正式账号(正式账号我们会另外邮件通知)


## sdk下载地址

http://lur.rayeead.com/sdkdownload