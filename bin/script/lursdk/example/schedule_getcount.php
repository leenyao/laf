<?php
/**
 * 获取排期的执行数量, 实时数据5分钟更新一次
 */


/**
 * 配置, id 1000是用来测试的, 不会真正产生投放数据
 */
require_once(__DIR__."/../lib/LurSchedule.php");
define("LURSDK_APPID", 1000);		//appid
define("LURSDK_APPSCRECT", "ae18a28906ddd7852a429ce223d49d2b");	//appscrect


/**
 * DEMO数据区域
 */
// 需要查询的代码
$aDemoUrls = array(
	"http://www.baidu.com?w=show",
	"http://www.baidu.com?w=click",
);

$aDemoPostDates = array(
	"2017-05-18",
	"2017-05-19",
	"2017-05-20",
);



/**
 * 程序区域
 */
$oLurSchedule = new LurSchedule();

// 第二个参数可不传, 默认为取最近3天数据
$aResponce = $oLurSchedule->getScheduleCounter($aDemoUrls);
$aJsonData = json_decode($aResponce, 1);
if (!is_null($aJsonData) && $aJsonData["code"] && $aJsonData["code"]===1) {
	echo "获取排期的执行数量 成功!\n\n";
	print_r($aJsonData["data"]);
}else{
	echo "获取排期的执行数量 失败. 请检查原因\n\n";
	print_r($aResponce);
}
