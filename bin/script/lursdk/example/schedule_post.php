<?php
/**
 * 提交排期DEMO
 */



/**
 * 配置, id 1000是用来测试的, 不会真正产生投放数据
 */
require_once(__DIR__."/../lib/LurSchedule.php");
define("LURSDK_APPID", 1000);		//appid
define("LURSDK_APPSCRECT", "ae18a28906ddd7852a429ce223d49d2b");	//appscrect




$sDateTomorrow = date("Y-m-d", strtotime("+1 day"));
/**
 * DEMO数据区域
 */
$aDemoPost = array(

	"showcode" => array(//showcode与clickcode 至少需要传一个, show代码
		"url" => "http://www.baidu.com?w=show", // 必传
		"label" => "代码别名",	//非必传, 代码的label, 可为空
		"enable" => 1, //非必传, show代码开关. 1=>正常, 0=>停止. 默认1
		"cookefreq" => 2, //非必传, cookie频次, 默认1
		"data" => array(//排期, 支持一下几种格式
			"2017-05-18" => 100,			//投 2017-05-18, 100次访问
			"2017-05-19 11-23" => 200,		//投 2017-05-19 的 11点至23点, 200次访问
			"2017-05-20 11,13,15" => 300,	//投 2017-05-20 的 11,13,15点, 300次访问
			"2017-06-01,$sDateTomorrow 12,13,14" => 300,	//投 2017-04-10至2017-05-11 的每天 11,13,15点, 300次访问
		),
	),
	"clickcode" => array(//showcode与clickcode 至少需要传一个, click代码, 所有配置与前面的show代码一致
		"url" => "http://www.baidu.com?w=click", 
		"label" => "代码别名",	
		"enable" => 1, 
		// "cookefreq" => 1, //无效, 点击代码的cookefreq会被系统强制转换为1
		"data" => array(
			"2017-05-18" => 100,			
			"2017-05-19 11-23" => 200,		
			"2017-05-20 11,13,15" => 300,	
			"2017-06-01,$sDateTomorrow 12,13,14" => 30,	//投 2017-04-10至2017-05-11 的每天 11,13,15点, 300次访问
			
		),
	),

	"referrer" => array("http://www.sohu.com", "http://www.sina.com"), //可为空
	"cities" => array(1156440300, 1156610100, 1156211100), //非必传, 定向城市, 传值见(http://www.iac-i.org/ 提供的地域编码表), 不传默认全国
	"adtype" => LURSDK_ADTYPE_PC, //非必传,  投放方式, 枚举值范围如下, 默认 LURSDK_ADTYPE_PC
								/**
								 		LURSDK_ADTYPE_PC 	=> PC投放方式
										LURSDK_ADTYPE_SDK 	=> SDK投放方式
								 */
	"uatype" => LURSDK_UA_PC, //非必传, UA, 枚举值范围如下, 默认LURSDK_UA_PC, 其它的选项参考 Defined.php 的UA部分
	"stb_percent" => 10, //非必传, 默认0, stable cookie percent, 整型, 范围0-100, 

);



/**
 * 程序区域
 */
$oLurSchedule = new LurSchedule();
$aResponce = $oLurSchedule->sentSchedule($aDemoPost);
$aJsonData = json_decode($aResponce, 1);
if (!is_null($aJsonData) && isset($aJsonData["code"]) && $aJsonData["code"]===1) {
	echo "排期上传成功!\n\n";
}else{
	echo "排期上传失败. 请检查原因\n\n";
	print_r($aResponce);
}





