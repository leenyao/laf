<?php

// 投放方式
define("LURSDK_ADTYPE_PC", 1);		// pc投放方式
define("LURSDK_ADTYPE_SDK", 2);		// SDK投放方式


/**
 * UA 列表部分 开始
 */
// UA列表-常规
define("LURSDK_UA_PC", 1);		// pc
define("LURSDK_UA_ANDROID", 2);		// android
define("LURSDK_UA_IOS", 3);		// ios
define("LURSDK_UA_PAD", 4);		// pad
// UA列表-小米系列
define("LURSDK_UA_PHONE_XIAOMI", 5);		// 小米手机
define("LURSDK_UA_TV_XIAOMI", 12);		// 小米电视
define("LURSDK_UA_BOX_XIAOMI", 13);		// 小米盒子
// UA列表-其它系列
define("LURSDK_UA_PHONE_COOLPAD", 6);		// 酷派
define("LURSDK_UA_TV_HISENSE", 7);		// 海信
define("LURSDK_UA_TV_LETV", 8);		// 乐视
define("LURSDK_UA_BOX_TMALL", 9);		// 天猫盒子
define("LURSDK_UA_TV_SKYWORLD", 10);		// 创维
define("LURSDK_UA_BOX_PPTV", 11);		// pptv
define("LURSDK_UA_BOX_IQIYI", 14);		// 爱奇艺盒子
/**
 * UA 列表部分 结束
 */


define("LURSDK_STBPRECENT_DEFAULT", 0);		// 默认的stb percent



// schedule API
// define("LURSDK_API_SCHEDULE_POST", "http://127.0.0.1/module/lur/schedule.php");		
define("LURSDK_API_SCHEDULE_POST", "http://lur.rayeead.com/schedule");		
