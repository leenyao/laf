<?php
require_once(__DIR__."/Defined.php");
class LurFunctions{

	/**
	 * 生成API的URL
	 */
	static function genScheduleApiUrl($sAction=null)
	{
		return LURSDK_API_SCHEDULE_POST."?action=".$sAction."&".self::genDynamicToken4LurApiParams();
	} 

	/**
	 * 为访问API生成token参数
	 */
	static function genDynamicToken4LurApiParams()
	{
		list($sTokenT, $sTokenV) = self::getLyDynamicToken();
		return "token_t=".$sTokenT."&token_v=".$sTokenV."&token_i=".LURSDK_APPID;
	} 

	 /**
	  * 动态token生成
	  */
	static function getLyDynamicToken()
	{
		$sSlat = LURSDK_APPSCRECT;
		$sTimeSpan = time();//基数
		$sToken = md5(strrev($sTimeSpan.$sSlat));
		return array($sTimeSpan,  $sToken);
	} 
}