<?php

require_once(__DIR__."/Defined.php");
require_once(__DIR__."/LurFunctions.php");

class LurSchedule
{
	
	function __construct()
	{
		if (
			!defined("LURSDK_APPID")
			|| !defined("LURSDK_APPSCRECT")
		) {
			echo "错误: 缺少宏变量 LURSDK_APPID, LURSDK_APPSCRECT\n\n";
			exit(2);
		}
	}

	
	public function getScheduleInfo($aSchedule)
	{
		$sUrl = \LurFunctions::genScheduleApiUrl("getinfo");
		$aCurlPostData = array(
			"urls" => json_encode($aSchedule),
		);
		return $this->_curl($sUrl, $aCurlPostData);
	}
	public function sentSchedule($aSchedule)
	{
		$sUrl = \LurFunctions::genScheduleApiUrl("postschedule");
		$aCurlPostData = array(
			"data" => json_encode($aSchedule),
		);
		return $this->_curl($sUrl, $aCurlPostData);
	}

	public function getScheduleCounter($aUrls, $aDates=array())
	{
		$sUrl = \LurFunctions::genScheduleApiUrl("getcounter");
		$aCurlPostData = array(
			"urls" => json_encode($aUrls),
			"dates" => json_encode($aDates),
		);
		return $this->_curl($sUrl, $aCurlPostData);
	}

	private function _curl($sUrl, $aPostData)
	{
	    $curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => $sUrl,
	      CURLOPT_TIMEOUT => 10,
	      CURLOPT_RETURNTRANSFER => 1,
	      // CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => ($aPostData),
	    ));

	    // echo $sUrl."\n\n";

	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);
	    if ($err) {
	        return $err;
	    }
	    return $response;
	}
	
}