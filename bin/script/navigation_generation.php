<?php
include_once(__DIR__."/config_inc.php");

$sLabelPre = "menu_";
$aNavigations = array(
//        'containers' => array(
    'default' => array(
    //            array(
    //                'name' => 'admin',
    //                'label' => $sLabelPre . 'admin',
    //                'route' => 'zfcadmin',
    //            ),
//        array(
//            'name' => 'contentblock',
//            'debugFlag' => true,
//            'label' => $sLabelPre . 'contentblock',
//            'route' => 'zfcadmin/contentlist',
//            'params' => array('type' => 'contentblock', 'cid' => 1),
//        ),


        array(

            'name' => 'campaign',
            'label' => $sLabelPre . 'campaign',
            'route' => 'zfcadmin/contentlist',
            'params' => array('type' => 'viewcampaignuser', 'cid' => 0),

           'pages' => array(
                array(
                   'name' => 'campaign',
                    'label' => $sLabelPre.'campaign',
                    'route' => 'zfcadmin/contentlist',
                    'iconCls' => 'licon-campaign',
                    'params' => array('type'=>'campaign', 'cid'=>0, ),
                    'permission' => array(
                        'zfcadmin/contentlist/campaign/0',
                    ),
                   'pages' => array(
                        array(
                            'visible' => false,
                            'label' => 'edit',
                            'route' => 'zfcadmin/contentedit',
                            'params' => array('type'=>'campaign', 'cid'=>0, ),
                        ),
                        array(
                            'visible' => false,
                            'label' => 'sendmsg',
                            'route' => 'zfcadmin/sendmsg',
                        ),
                        array(
                            'visible' => false,
                            'label' => 'add',
                            'route' => 'zfcadmin/contentadd',
                            'params' => array( 'type'=>'campaign', 'cid'=>0,),
                        ),
                    ),
                ),
            )
        ),

        array(
            'name' => 'content',
            'label' => $sLabelPre . 'content',
            'route' => 'zfcadmin/contentlist',
            'params' => array('type' => 'content', 'cid' => 0),
           'pages' => array(
                
                array(
                    'name' => 'content',
                    'label' => $sLabelPre.'content',
                    'route' => 'zfcadmin/contentlist',
                    'iconCls' => 'licon-ads1',
                    'params' => array( 'type'=>'content', 'cid'=>0,),
                    'permission' => array(
                        'zfcadmin/contentlist/content/0',
                    ),
                   'pages' => array(
                        array(
                            'visible' => false,
                            'label' => 'edit',
                            'route' => 'zfcadmin/contentedit',
                            'params' => array('type'=>'content', 'cid'=>0, ),
                        ),
                        array(
                            'visible' => false,
                            'label' => 'add',
                            'route' => 'zfcadmin/contentadd',
                            'params' => array( 'type'=>'content', 'cid'=>0,),
                        ),
                       array(
                            'visible' => false,
                            'label' => 'edit',
                            'route' => 'zfcadmin/imagecropping',
                            'params' => array(),
                        ),
                    ),
                ),
           ),
        ),

        array(
            'name' => 'user',
            'label' => $sLabelPre . 'user',
            'route' => 'zfcadmin/contentlist',
            'params' => array('type' => 'user', 'cid' => 0),
            'permission' => array(
                'zfcadmin/contentlist/user/0',
            ),
           'pages' => array(
                array(
                    'name' => 'user',
                    'label' => $sLabelPre . 'user',
                    'route' => 'zfcadmin/contentlist',
                    'params' => array('type' => 'user', 'cid' => 0),
                    'permission' => array(
                        'zfcadmin/contentlist/user/0',
                    ),
                   'pages' => array(
                        array(
                            'visible' => false,
                            'label' => 'edit',
                            'route' => 'zfcadmin/contentedit',
                            'params' => array('type'=>'user', 'cid' => 0),
                        ),
                        array(
                            'visible' => false,
                            'label' => 'add',
                            'route' => 'zfcadmin/contentadd',
                            'params' => array( 'type'=>'user'),
                        ),
                       array(
                            'visible' => false,
                            'label' => 'add',
                            'route' => 'zfcadmin/contentadd',
                            'params' => array( 'type'=>'viewtradecredit'),
                        ),
                       array(
                            'visible' => false,
                            'label' => 'tradelist',
                            'route' => 'zfcadmin/contentlist',
                            'params' => array( 'type'=>'viewtradecredit'),
                        ),
                    ),
                ),
            ),
        ),
        array(
            'name' => 'system',
            'label' => $sLabelPre . 'system',
            'route' => 'zfcadmin/contentlist',
             'params' => array('type' => 'rbacrole', 'cid' => 0),
            'permission' => array(
                'superadmin',//for super admin
            ),
           'pages' => array(
    //                    array(
    //                        'name' => 'clearcache',
    //                        'label' => $sLabelPre . 'clearcache',
    //                        'route' => 'zfcadmin/system/clearcache',
    //                    ),
    //                    array(
    //                        'name' => 'accprofile',
    //                        'label' => $sLabelPre . 'accprofile',
    //                        'route' => 'zfcadmin/system/accprofile',
    //                    ),
                array(
                    'name' => 'rbacrole',
                    'label' => $sLabelPre . 'rbacrole',
                    'route' => 'zfcadmin/contentlist',
                    'params' => array('type' => 'rbacrole', 'cid' => 0),
                   'pages' => array(
                        array(
                            'visible' => false,
                            'label' => 'edit',
                            'route' => 'zfcadmin/contentedit',
                            'params' => array( 'type'=>'rbacrole', 'cid' => 0),
                        ),
                        array(
                            'visible' => false,
                            'label' => 'add',
                            'route' => 'zfcadmin/contentadd',
                            'params' => array( 'type'=>'rbacrole', 'cid' => 0),
                        ),
                    ),
                ),
                array(
                    'name' => 'cacheadmin',
                    'label' => $sLabelPre . 'cacheadmin',
                    'route' => 'zfcadmin/contentlist',
                    'params' => array('type' => 'cacheadmin', 'cid' => 0),
                ),
            ),
        ),
    ),
    'listeners' => array()
);
//递归处理nav, 补全 permission
foreach($aNavigations as $sNavKey => $aNav){
    $aNavigations[$sNavKey] = Application\Model\Common::_nav_recursion($aNav);
}
//echo "<pre>";
$a = var_export($aNavigations, 1);

$sFileName = __DIR__."/../../config/autoload/nav.autogeneration.php";
$fp = fopen($sFileName, 'w');
fwrite($fp, "<?php\n //自动生成文件, 修改无用 \n return ". $a.";");
fclose($fp);
echo " generate navigation done !\n\n";
