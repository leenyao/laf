<?php
include_once(__DIR__."/config_inc.php");

// var_dump($oGlobalFramework);
class SimpleRestApi 
{
    function __construct()
    {
    }

    public function chkToken()
    {
        if (
            !isset($_GET['token_i'])
            || !isset($_GET['token_v'])
            || !isset($_GET['token_t'])
        ) {
            return array(false, "miss token params");
        }

        $sEncrytId = \YcheukfCommon\Lib\Functions::encryptBySlat($_GET['token_i']);
        $bFlag = \YcheukfCommon\Lib\Functions::chkLyDynamicToken($_GET['token_t'],  $_GET['token_v'], $sEncrytId);
        if (!$bFlag) {
            return array(false, "token verification failed");
        }
        $nCurrentTime = time();
        if (($_GET['token_t']>($nCurrentTime+600) || $_GET['token_t']<($nCurrentTime-600))) {//时间和服务器对不上报错
            return array(false, "timespan check faild; token_t(".$_GET['token_t'].") - servertimespan(".$nCurrentTime.") = ".($nCurrentTime-$_GET['token_t']));
        }
        return array(true, "");
    }

    function httpjsonresponse($sMsg="", $nCode=1, $xData=array()){
        $this->httpresponse(array(
            "code" => $nCode,
            "msg" => $sMsg,
            "data" => $xData,
        ));
    }


    function httpresponse($sMsg='', $nStatus=\Zend\Http\PhpEnvironment\Response::STATUS_CODE_200, $bJsonFlag=true)
    {

        $response = new \Zend\Http\PhpEnvironment\Response();
        $response->setStatusCode($nStatus);
        $response->getHeaders()->addHeaders(array(
                'Access-Control-Allow-Origin' => '*'
            )
        );

        if ($bJsonFlag) {
            $response->getHeaders()->addHeaders(array(
                    'Content-type' => 'application/json'
                )
            );
            $sMsg = json_encode($sMsg);

        }
        $response->setContent($sMsg);

        $response->send();
        exit;
    }

    function __get($name='')
    {
        global $oGlobalFramework;
        switch ($name) {
            case 'thirdScheduleRedis':
                // $this->iniThirdScheduleRedis();
                return $oGlobalFramework->sm->get('\Lur\Service\Common')->getThirdScheduleRedis();
                break;
            default:
                break;
        }
    }



}




