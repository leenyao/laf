<?php

set_time_limit(0);
chdir(('/app'));

spl_autoload_register(function($class)
{
    if ($class == 'YcheukfCommon\Lib\Functions') {
        $path = '/app/module/ycheukf/Common/src/Common/Lib/Functions.php';
        include_once $path;
    }
    if ($class == 'YcheukfCommon\Lib\RedisMq') {
        $path = '/app/module/ycheukf/Common/src/Common/Lib/RedisMq.php';
        include_once $path;
    }
    if ($class == 'Lur\Service\Uuid') {
        $path = '/app/module/Lur/src/Lur/Service/Uuid.php';
        include_once $path;
    }
});

require '/app/init_autoloader.php';

class SimpleRestApi2 
{
    function __construct()
    {
    }

    public function chkToken()
    {
        if (
            !isset($_GET['token_i'])
            || !isset($_GET['token_v'])
            || !isset($_GET['token_t'])
        ) {
            return array(false, "miss token params");
        }

        $sEncrytId = \YcheukfCommon\Lib\Functions::encryptBySlat($_GET['token_i']);
        $bFlag = \YcheukfCommon\Lib\Functions::chkLyDynamicToken($_GET['token_t'],  $_GET['token_v'], $sEncrytId);
        if (!$bFlag) {
            return array(false, "token verification failed");
        }
        $nCurrentTime = time();
        if (($_GET['token_t']>($nCurrentTime+600) || $_GET['token_t']<($nCurrentTime-600))) {//时间和服务器对不上报错
            return array(false, "timespan check faild; token_t(".$_GET['token_t'].") - servertimespan(".$nCurrentTime.") = ".($nCurrentTime-$_GET['token_t']));
        }
        return array(true, "");
    }


    function response_error($sMsg="", $nCode=1){
        if ($nCode == 1) {
            exec(" echo \'[".date("YmdHis")."]\tadrapierror\t".addslashes($sMsg)."\t".$_SERVER['REQUEST_URI']."\' >> /app/php_errors.log");
        }
        $this->httpjsonresponse($sMsg, 500, false);

    }

    function httpjsonresponse($sMsg="", $nCode=1, $xData=array()){
        $this->httpresponse(array(
            "code" => $nCode,
            "msg" => $sMsg,
            "data" => $xData,
        ));
    }


    function httpresponse($sMsg='', $nStatus=\Zend\Http\PhpEnvironment\Response::STATUS_CODE_200, $bJsonFlag=true)
    {

        $response = new \Zend\Http\PhpEnvironment\Response();
        $response->setStatusCode($nStatus);
        $response->getHeaders()->addHeaders(array(
                'Access-Control-Allow-Origin' => '*'
            )
        );

        if ($bJsonFlag) {
            $response->getHeaders()->addHeaders(array(
                    'Content-type' => 'application/json'
                )
            );
            $sMsg = json_encode($sMsg);

        }
        $response->setContent($sMsg);

        $response->send();
        exit;
    }

    public function initRedis($host, $port, $db)
    {
        try{
            $oReturn = new \YcheukfCommon\Lib\RedisMq();

            $oReturn->connect($host, $port);
            $oReturn->select($db);
        }catch(\RedisException $e){
            echo "".$e->getMessage()."\n";
            return false;
        }
        return $oReturn;
    }

    static function initPdo($sType='db_master'){
        $aPdoConfig = require '/app/config/autoload/db.local.php';

        $oGlobalPDO =  @new \PDO($aPdoConfig[$sType]['dsn'], $aPdoConfig[$sType]['username'], $aPdoConfig[$sType]['password'], array(\PDO::ATTR_PERSISTENT => true));
        @$oGlobalPDO->query($aPdoConfig[$sType]['driver_options'][\PDO::MYSQL_ATTR_INIT_COMMAND]);
        return $oGlobalPDO;
    }
    function __get($name='')
    {
        switch ($name) {
            case 'pdo':
                return self::initPdo();
            break;
            case 'pdo_slave':
                return self::initPdo('db_slave');
            break;
            case 'redis':
                $aRedisConfig = require '/app/config/autoload/cache.global.php';
                return $this->initRedis($aRedisConfig['cache_config']['params']['servers'][0][0], $aRedisConfig['cache_config']['params']['servers'][0][1], $aRedisConfig['cache_config']['params']['servers'][0][2]);
                break;
            case 'uuidredis':
                $aRedisConfig = require '/app/module/Lur/config/module.config.php';
                return $this->initRedis($aRedisConfig['addtionalredis']['host'], $aRedisConfig['addtionalredis']['port'], $aRedisConfig['addtionalredis']['db']);

            case 'configs':
                $configs = require '/app/module/Lur/config/module.config.php';
                return $configs;

            default:
                break;
        }
    }

}




