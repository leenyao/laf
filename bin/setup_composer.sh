#!/bin/bash
## this is a mfn vendor setup script
currentdir=$(cd "$(dirname "$0")"; pwd)

cd $currentdir/../

if [ ! -f "$currentdir/../composer.phar" ]; then
    wget https://getcomposer.org/installer -O ./composer.phar
fi
php composer.phar -n self-update
php composer.phar -n install

cd $currentdir/../public

php ../composer.phar -n install

cd ../bin