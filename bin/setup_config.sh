#!/bin/bash
## this is a mfn vendor setup script

# projectname first upper
sProjectName=`echo $LAF_project_namespace | sed 's/^\w\|\s\w/\U&/g'`


if [ ! -f "/app/config/autoload/local.php" ]; then 
    rm -f /app/config/autoload/local.php
    cp -f /app/config/autoload/local-sample.php /app/config/autoload/local.php
    sed -ri -e "s#\#webhost_path\##${LAF_webhost_path}#" /app/config/autoload/local.php
    sed -ri -e "s#\#debugFlag\##${LAF_debugFlag}#" /app/config/autoload/local.php
fi

rm -f /app/config/application.config.php
cp -f /app/config/application-sample.config.php /app/config/application.config.php

# sed -ri -e "s#'Demo',##" /app/config/application.config.php
sed -ri -e "s#\#project_namespace\##${sProjectName}#" /app/config/application.config.php

rm -f /app/config/console.config.php
cp -f /app/config/console-sample.config.php /app/config/console.config.php
sed -ri -e "s#\#project_namespace\##${sProjectName}#" /app/config/console.config.php


#########################db config#####################
if [ -z ${LAF_MYSQL_DATABASE} ]; then
    LAF_MYSQL_DATABASE=laf
fi 
if [ -z ${LAF_MYSQL_HOST} ]; then
    LAF_MYSQL_HOST=lafmysql
fi 
if [ -z ${LAF_MYSQL_USER} ]; then
    LAF_MYSQL_USER=root
fi 
if [ -z ${LAF_MYSQL_PASSWORD} ]; then
    LAF_MYSQL_PASSWORD="root123"
fi 
if [ -z ${LAF_MYSQL_PORT} ]; then
    LAF_MYSQL_PORT=3306
fi 

if [ -z ${LAF_MYSQLSLAVE_DATABASE} ]; then
    LAF_MYSQLSLAVE_DATABASE=$LAF_MYSQL_DATABASE
fi
if [ -z ${LAF_MYSQLSLAVE_HOST} ]; then
    LAF_MYSQLSLAVE_HOST=$LAF_MYSQL_HOST
fi 
if [ -z ${LAF_MYSQLSLAVE_USER} ]; then
    LAF_MYSQLSLAVE_USER=$LAF_MYSQL_USER
fi 
if [ -z ${LAF_MYSQLSLAVE_PASSWORD} ]; then
    LAF_MYSQLSLAVE_PASSWORD=$LAF_MYSQL_PASSWORD
fi 
if [ -z ${LAF_MYSQLSLAVE_PORT} ]; then
    LAF_MYSQLSLAVE_PORT=$LAF_MYSQL_PORT
fi 

rm -f /app/config/autoload/db.global.php
cp -f /app/config/autoload/db-sample.php /app/config/autoload/db.global.php
sed -ri -e "s#\#LAF_MYSQL_DATABASE\##${LAF_MYSQL_DATABASE}#" /app/config/autoload/db.global.php
sed -ri -e "s#\#LAF_MYSQL_HOST\##${LAF_MYSQL_HOST}#" /app/config/autoload/db.global.php
sed -ri -e "s#\#LAF_MYSQL_USER\##${LAF_MYSQL_USER}#" /app/config/autoload/db.global.php
sed -ri -e "s#\#LAF_MYSQL_PASSWORD\##${LAF_MYSQL_PASSWORD}#" /app/config/autoload/db.global.php
sed -ri -e "s#\#LAF_MYSQL_PORT\##${LAF_MYSQL_PORT}#" /app/config/autoload/db.global.php

sed -ri -e "s#\#LAF_MYSQLSLAVE_DATABASE\##${LAF_MYSQLSLAVE_DATABASE}#" /app/config/autoload/db.global.php
sed -ri -e "s#\#LAF_MYSQLSLAVE_HOST\##${LAF_MYSQLSLAVE_HOST}#" /app/config/autoload/db.global.php
sed -ri -e "s#\#LAF_MYSQLSLAVE_USER\##${LAF_MYSQLSLAVE_USER}#" /app/config/autoload/db.global.php
sed -ri -e "s#\#LAF_MYSQLSLAVE_PASSWORD\##${LAF_MYSQLSLAVE_PASSWORD}#" /app/config/autoload/db.global.php
sed -ri -e "s#\#LAF_MYSQLSLAVE_PORT\##${LAF_MYSQLSLAVE_PORT}#" /app/config/autoload/db.global.php

#########################redis config#####################

if [ -z ${LAF_REDIS_HOST} ]; then
    LAF_REDIS_HOST=lafredis
fi 
if [ -z ${LAF_REDISPORT} ]; then
    LAF_REDISPORT="6379"
fi 

rm -f /app/config/autoload/cache.global.php
cp -f /app/config/autoload/cache-sample.php /app/config/autoload/cache.global.php
sed -ri -e "s#\#LAF_REDIS_HOST\##${LAF_REDIS_HOST}#" /app/config/autoload/cache.global.php
sed -ri -e "s#\#LAF_REDISPORT\##${LAF_REDISPORT}#" /app/config/autoload/cache.global.php


#########################director config#####################

if [ -z ${LAF_runServiceFlag} ]; then
    LAF_runServiceFlag=1
fi 

if [[ $LAF_runServiceFlag = 0 ]]; then
    rm -f /app/data/runservice.lock
else
    touch /app/data/runservice.lock
fi



mkdir -p /app/data
chmod -cR --quiet 777 /app/data
mkdir -p /app/public/tmpupload
chmod -cR --quiet 777 /app/public/tmpupload

mkdir -p /app/public/module
if [ ! -L "/app/public/module/${sProjectName}" ]; then 
    ln  -s /app/module/${sProjectName}/public /app/public/module/${sProjectName} 
    typeset -l sProjectNameLowwer=$sProjectName
    ln  -s /app/module/${sProjectName}/public /app/public/module/${sProjectNameLowwer} 

    
fi

if [ ! -L "/app/public/js/FCPHPClassCharts" ]; then 
    ln -s /app/vendor/ycheukf/report/src/Report/Lib/FCPHPClassCharts /app/public/js/FCPHPClassCharts
fi






