<?php
return array(
	'view_helpers' => array(
		'invokables' => array(
			//Alert
			'alert' => 'YcheukTwbBundle\View\Helper\TwbBundleAlert',

			//Badge
			'badge' => 'YcheukTwbBundle\View\Helper\TwbBundleBadge',

			//DropDown
			'dropDown' => 'YcheukTwbBundle\View\Helper\TwbBundleDropDown',

			//Form
			'formYcf' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleForm',
			'formYcfSearchbar' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormSearchbar',
			'formYcfButton' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormButton',
			'formYcfSubmitcancel' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormSubmitcancel',
			'formYcfHidden' => 'Zend\Form\View\Helper\FormHidden',
			'formYcfText' => 'Zend\Form\View\Helper\FormText',
			'formYcfCheckbox' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormCheckbox',
			'formYcfCollection' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormCollection',
			'formYcfElement' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormElement',
			'formYcfMultiCheckbox' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormMultiCheckbox',
			'formYcfRadio' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormRadio',
			'formYcfRow' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormRow',
			'formYcfPassword' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormPassword',
			'formYcfDate' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormDate',
			'formYcfDatetime' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormDatetime',
			'formYcfStatic' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormStatic',
			'formYcfLabel' => 'YcheukTwbBundle\View\Helper\TwbBundleLabel',
			'formYcfAutocomplete' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormAutocomplete',
			'formYcfSelectpanel' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormSelectpanel',
			'formYcfSelectHref' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormSelectHref',

			'formYcfTextarea' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormTextarea',
			'formYcfSelect' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormSelect',
			'formYcfImage' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormImage',
			'formYcfDateselect' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormDateSelect',
			'formYcfDatetimeselect' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormDatetTimeSelect',
			'formYcfGroupselect' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormGroupselect',
			'formYcfFileupload' => 'YcheukTwbBundle\Form\View\Helper\TwbBundleFormFileupload',

			'formYcfEditor' => 'YcheukTwbBundle\Form\View\Helper\Editor',
			'editor' => 'YcheukTwbBundle\Form\View\Helper\Editor',
			'formYcfWysiwyg' => 'YcheukTwbBundle\Form\View\Helper\formYcfWysiwyg',
			'formYcfAddablelist' => 'YcheukTwbBundle\Form\View\Helper\formYcfAddablelist',

			'formYcfKolresourcepikcer' => 'YcheukTwbBundle\Form\View\Helper\Kolresourcepikcer',

			//Label
			'label' => 'YcheukTwbBundle\View\Helper\TwbBundleLabel'
		)
	)
);