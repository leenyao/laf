<?php
/**
 * ZfcUserAdmin Configuration
 *
 * If you have a ./config/autoload/ directory set up for your project, you can
 * drop this config file in it and change the values as you wish.
 */
$settings = array(
    

    /**
     * Mapper for ZfcUser
     *
     * Set the mapper to be used here
     * Currently Available mappers
     * 
     * ZfcUserAdmin\Mapper\UserDoctrine
     *
     * By default this is using
     * ZfcUserAdmin\Mapper\UserZendDb
     */
//    'user_mapper' => 'ZfcUserAdmin\Mapper\UserDoctrine',
//    'user_mapper' => 'ZmcBase\Mapper\SystemUsersMapper',
    'user_mapper' => 'ZfcUserAdmin\Mapper\UserZendDb',
);

/**
 * You do not need to edit below this line
 */
return array(
    'zfcuseradmin' => $settings,
    'service_manager' => array(
        'factories' => array(
			'zfcuser_user_mapper' => function ($sm) {//覆盖原有的zfcuser_user_mapper
				/** @var $config \ZfcUserAdmin\Options\ModuleOptions */
				$config = $sm->get('zfcuseradmin_module_options');
				$mapperClass = $config->getUserMapper();
				if (stripos($mapperClass, 'doctrine') !== false) {
					$mapper = new $mapperClass(
						$sm->get('zfcuser_doctrine_em'),
						$sm->get('zfcuser_module_options')
					);
				} else {
					/** @var $zfcUserOptions \ZfcUser\Options\UserServiceOptionsInterface */
					$zfcUserOptions = $sm->get('zfcuser_module_options');

					/** @var $mapper \ZfcUserAdmin\Mapper\UserZendDb */
					$mapper = new $mapperClass();
					$mapper->setDbAdapter($sm->get('zfcuser_zend_db_adapter'));
					$entityClass = $zfcUserOptions->getUserEntityClass();
					$mapper->setEntityPrototype(new $entityClass);
					$mapper->setHydrator(new \ZfcUser\Mapper\UserHydrator());
					$mapper->setTableName($zfcUserOptions->getTableName());
	//			var_dump($zfcUserOptions->getTableName());
				}
				return $mapper;
			}
		),
	)
);
