<?php
return array(
    'apiservice' => array(
        "customer" =>array(
            'token' => array(
                "class"=>"\Application\Restapi\Token", 
                "restfulflag"=>1,  
                'memo'=>'获取用户token',
                "d:entity"=>array(
                    "id" => array(
                        "d:type"=>"string",
                        "d:memo"=>"用户token",
                    ),
                    "user_name" => array(
                        "d:type"=>"string",
                        "d:post-required"=>"true",
                    ),
                    "user_password" => array(
                        "d:type"=>"string",
                        "d:post-required"=>"true",
                    ),
                ),
            ),
        ),
        // itemcategory/1
        "resources" =>array(
            "user" => array("restfulflag"=>1,  "driver"=>"mysql", "table"=>"system_users","record_cache"=>1, 'memo'=>'用户信息'),//\n\
            "oauthuser" => array("driver"=>"mysql", "table"=>"oauth_users", "record_cache"=>1),//\n\
            "oauthclients" => array("driver"=>"mysql", "table"=>"oauth_clients"),//\n\
            "oauthtokens" => array("driver"=>"mysql", "table"=>"oauth_access_tokens"),//\n\
            "metadata" => array("restfulflag"=>1, "driver"=>"mysql", "table"=>"system_metadata", "record_cache"=>1, 'memo'=>'基准信息'),//\n\
            "location_city" => array("driver"=>"mysql", "table"=>"location_city", "record_cache"=>1),//\n\
            "location_district" => array("driver"=>"mysql", "table"=>"location_district", "record_cache"=>1),//\n\
            "location_province" => array("driver"=>"mysql", "table"=>"location_province", "record_cache"=>1),//\n\
            "rbacroleuser" => array("driver"=>"mysql", "table"=>"rbac_role_user"),//\n\
            "rbacrole" => array("driver"=>"mysql", "table"=>"rbac_role"),//\n\
            "rbacpermission" => array("driver"=>"mysql", "table"=>"rbac_permission"),//\n\
            "rbacrrp" => array("driver"=>"mysql", "table"=>"rbac_role_permission"),//\n\
            "suggestion" => array("driver"=>"mysql", "table"=>"b_suggestion", ),//\n\
            "contentblock" => array("driver"=>"mysql", "table"=>"system_contentblock"),//\n\
            "medialib" => array("driver"=>"mysql", "table"=>"system_medialib"),//\n\
            "iostoken" => array("driver"=>"mysql", "table"=>"system_iostoken"),//\n\
            "andriodtoken" => array("driver"=>"mysql", "table"=>"system_andriodtoken"),//\n\
            "userbander" => array("driver"=>"mysql", "table"=>"system_users_bander"),//\n\
            "userlog" => array("driver"=>"mysql", "table"=>"system_userlog"),//\n\
            "emaillist" => array("driver"=>"mysql", "table"=>"system_email_list"),//\n\
            "jobs" => array("driver"=>"mysql", "table"=>"system_jobs"),//\n\
            'md5key' => array("driver"=>"mysql", "table"=>"system_md5key"),//\n\
            "report" => array("driver"=>"mysql", "table"=>"system_report"),//\n\
            "customerlog" => array("driver"=>"mysql", "table"=>"system_customerlog"),//\n\
            "viewrolepermission" => array("driver"=>"mysql", "table"=>"view_role_permission"),//\n\
            "payment" => array("driver"=>"mysql", "table"=>"system_payment"),//\n\
            "relation" => array("driver"=>"mysql", "table"=>"system_relation"),//\n\
            "view_payment" => array("driver"=>"mysql", "table"=>"view_payment"),//\n\
            
            "contact" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"system_contact", 'memo'=>'联系人信息'),//\n\
            "apishell" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"system_apishell", 'memo'=>'通过api传送是shell'),//\n\

            "profilebase" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"system_users", 'memo'=>''),//\n\
            "profilepassword" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"oauth_users", 'memo'=>''),//\n\
            
        ),
    ),
);