<?php

return array(
    "cache_config" => array(
        'enable' => true,
        'type' => 'redis', // memcache | redis
        'keypre' => 'laf_cache',
        'params' =>array(
            'servers' => array(
//                array('localhost', 11211, false),
                array('#LAF_REDIS_HOST#', '#LAF_REDISPORT#', 0),
            ),
        ),
    ),
);