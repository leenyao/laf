<?php

defined('BASE_INDEX_PATH') || define('BASE_INDEX_PATH', (dirname(dirname(__FILE__)))."/public");
return array(
	'crondjob' => array(

		/*
		    为每个脚本创建一个计时器, 当执行时间到达时间戳时才会执行脚本
		    array(
		        '脚本文件名1' => 时间戳,
		        '脚本文件名2' => 时间戳,
		    )
		*/		
		'aGlobalScriptIndex' => array(
		    array(BASE_INDEX_PATH.'/../bin/script/crondjob/Messagequeue.php', 5),//可独立执行, 可不填时间, 则使用全局的时间
		),
		//一次触发所处理的条数
		'nGlobalLimitNum' => 40,
		//定时服务触发的频率, 秒
		'nGlobalFrequency' => 2,
		//触发器表名
		'sGlobalTriggerTalbe' => 'system_trigger_log',
		//触发器表名
		'aGlobalTimer' => array(),
	),
);