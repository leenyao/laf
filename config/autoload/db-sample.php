<?php

return array(
    'db_master' => array(
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=#LAF_MYSQL_DATABASE#;host=#LAF_MYSQL_HOST#;port=#LAF_MYSQL_PORT#',
        'username' => '#LAF_MYSQL_USER#',
        'password' => '#LAF_MYSQL_PASSWORD#',
        'driver_options' => array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\';SET time_zone = \'+8:00\'',
        ),
    ),
    'db_slave' => array(
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=#LAF_MYSQLSLAVE_DATABASE#;host=#LAF_MYSQLSLAVE_HOST#;port=#LAF_MYSQLSLAVE_PORT#',
        'username' => '#LAF_MYSQLSLAVE_USER#',
        'password' => '#LAF_MYSQLSLAVE_PASSWORD#',
        'driver_options' => array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\';SET time_zone = \'+8:00\'',
//            \PDO::ATTR_PERSISTENT => true,
        ),
    ),
);