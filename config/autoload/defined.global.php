<?php

defined('LAF_BASEPATH') || define('LAF_BASEPATH', __DIR__."/../../");


defined('LAF_NAMESPACE') || define('LAF_NAMESPACE', ucfirst(getenv("LAF_project_namespace")));


// 用于混淆ID的混乱码
defined('LAF_HASHSLAT') || define('LAF_HASHSLAT', "LAFSLAT123456789");


// 调用 \YcheukfCommon\Lib\Functions::::getResourceById 所使用的cache 前缀
defined('LAF_CACHE_GETRESOURCEBYID_PRE') || define('LAF_CACHE_GETRESOURCEBYID_PRE', "LAFC_101");

// 调用 \YcheukfCommon\Lib\Functions::::getResourceMetaData 所使用的cache 前缀
defined('LAF_CACHE_GETRESOURCEMETADATA_PRE') || define('LAF_CACHE_GETRESOURCEMETADATA_PRE', "LAFC_102");


// 调用 \YcheukfCommon\Lib\Functions::::getResourceMetaData 所使用的cache 前缀
defined('LAF_CACHE_USERPOLE_PRE') || define('LAF_CACHE_USERPOLE_PRE', "LAFC_103");


// 调用 \YcheukfCommon\Lib\Functions::::getRelationList 所使用的cache 前缀
defined('LAF_CACHE_GETRELATIONLIST_PRE') || define('LAF_CACHE_GETRELATIONLIST_PRE', "LAFC_104");



return array();