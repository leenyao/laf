<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */


$aConfig = array(
    "oauth_client_id" => array(
        'default' => 5000,
        'wechat' => 6000,
    ),
//    //memcache configuration
//    "cache_config" => array(
//        'enable' => true,
//        'type' => 'redis', // memcache | redis
//        'keypre' => 'laf_redis',
//        'params' =>array(
//            'servers' => array(
////                array('localhost', 11211, false),
//                array('laf_redis', 6379, 1),
//            ),
//        ),
//    ),
    
    'cache_dir' =>  './data/cache',
    'tmpupload_dir' =>  './public/tmpupload',
    'tmpwxjssdk_dir' =>  './public/tmpupload/wxjssdk',
    'fonts_dir' =>  './public/fonts',
    'SMSconfig' => array(//短信配置
        'posturl' => 'http://sdk.univetro.com.cn:6200/sdkproxy/sendsms.action?cdkey=7SDK-LHW-0588-ODQNR&password=371502&&phone=[==dt==]&message=[==msg==]',
    ),
    // 'navigation' => require(__DIR__."/nav.autogeneration.php"),
    'service_manager' => array(
        'invokables' => array(
            'YcheukfCommon\Lib\ZfcUser\AuthAdapter' => 'YcheukfCommon\Lib\ZfcUser\AuthAdapter',
        ),
        'factories' => array(
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
            'ycfmq' => function ($sm){
                $aConfig = $sm->get('configuration');
                if($aConfig['cache_config']['type'] == 'redis'){
                    $oMq = new \YcheukfCommon\Lib\RedisMq();
                    $oMq->connect($aConfig['cache_config']['params']['servers'][0][0], $aConfig['cache_config']['params']['servers'][0][1]);                $oMq->select($aConfig['cache_config']['params']['servers'][0][2]);

                    return $oMq;
                }
                die("can not load redis");
            },
            'Zend\Db\Adapter\Adapter' => function ($sm)
            {
                $aConfig = $sm->get('config');
                if(!isset($aConfig['db_master']) || !isset($aConfig['db_slave']))return false;

                $slaverAdapter = new Zend\Db\Adapter\Adapter($aConfig['db_slave']);
                $oMSAdapter = new ZfcBase\Db\Adapter\MasterSlaveAdapter($slaverAdapter, $aConfig['db_master']);

                return $oMSAdapter;
            },
            'Rbac\Doctrine\DBAL\Connection' => function ($sm)
            {
                $aConfig = $sm->get('config');
                $aDsn = \YcheukfCommon\Lib\Functions::getParamFromDSN($aConfig['db_master']['dsn']);

                $config = new \Doctrine\DBAL\Configuration();
                $connectionParams = array(
                    'user' => $aConfig['db_master']['username'],
                    'password' => $aConfig['db_master']['password'],
                    'host' => $aDsn['host'],
                    'dbname' => $aDsn['dbname'],
                    'port' => $aDsn['port'],
                    'driver' => 'pdo_mysql',
                );
                $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);


                return $conn;
            },
            //  'solr.solr_default' => function ($sm){

            //     $options = $sm->get('Configuration');
            //     $options = $options['solr'];

            //     $connParams = $options['connection']['solr_default'];

            //     $httpUri = new \Zend\Uri\Http();

            //     $httpUri->setHost($connParams['host'])
            //         ->setPath($connParams['path'])
            //         ->setPort($connParams['port'])
            //         ->setScheme($connParams['scheme'])
            //         ->setUser($connParams['user'])
            //         ->setPassword($connParams['password']);

            //     $client = new \YcheukfCommon\Lib\SolrClient($httpUri);

            //     $config = $options['configuration']['solr_default'];
            //     $client->setSelectPath($config['select_path'])
            //         ->setUpdatePath($config['update_path'])
            //         ->setResultClass($config['resultClass']);
            //     return $client;
            // }

        ),
    ),
);


return $aConfig;
