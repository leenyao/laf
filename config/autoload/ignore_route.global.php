<?php

return array(
    'ignore_route' => array(
        "api",    //不过滤api请求
        "ichuangyeapi",    //不过滤 aichuangye 请求
        "hxvote",    //不过滤debug请求
        "home",    //不过滤官网普通操作请求
        "search",
        "application",    //不过滤官网普通操作请求
        "oauth",    //不过滤oauth请求
        "demo",    //不过滤DEMO请求
        "debug",    //不过滤debug请求
        "lurdebug",    //不过滤debug请求
        "forgetpwd",    //不过滤debug请求
        "register",    //不过滤debug请求
        "datas",    //不过滤静态数据请求
        "aboutus",    //不过滤静态数据请求
        "dashboard",    //不过滤dashboard请求
        "wxlogin",    //不过滤dashboard请求
        "zfcuser/login",    //不过登陆请求
        "zfcuser/logout",    //不过登出请求
        "download",    //不过登出请求
        "rest-api-docs",    //rest api module
        "rest-api",    //rest api module
        "wxusertoken",    //rest api module
        "weixinapi",    //rest api module
        "hcoffee/default",    //rest api module
        "wechat/",    //rest api module
        "test",
        "error403",
        "error404",
        "moduleconfig"

//        "zfcadmin",    //不过登出请求
    ),
);