<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Zend\Session\SessionManager' => function ($sm) {
                /**
                * 不适用 savehandle, 在php.ini中解决该问题
                */
//                return array();
                if(PHP_SAPI=='apache2handler')//浏览器访问使用session
                    ini_set('session.gc_maxlifetime',60*60*24);//session 过期时间
                else
                    ini_set('session.gc_maxlifetime', 0);//session 过期时间
                $aConfig = $sm->get('config');
                /******** mongodb 配置
                $sMongodbKey = 'db_mongodb';
                $sDsn =     "mongodb://".$aConfig[$sMongodbKey]["username"].":".$aConfig[$sMongodbKey]["password"]."@".$aConfig[$sMongodbKey]["host"].":".$aConfig[$sMongodbKey]["port"]."/".$aConfig[$sMongodbKey]["dbname"];
                $mongo = new \Mongo($sDsn);
                $options = new \Zend\Session\SaveHandler\MongoDBOptions(array(
                    'database'   => $aConfig[$sMongodbKey]['dbname'],
                    'collection' => 'sessions',
                ));
                $saveHandler = new \Zend\Session\SaveHandler\MongoDB($mongo, $options);
                */

                /******** 服务器 配置
                
                $cache = \Zend\Cache\StorageFactory::factory(array(
                    'adapter' => array(
                        'name' => 'redis',
                        'options' => array(
                            'server' => array('host'=>$aConfig['cache_config']['params']['servers'][0][0], 'port'=>$aConfig['cache_config']['params']['servers'][0][1])
                        ),
                    )
                ));
                $saveHandler = new \Zend\Session\SaveHandler\Cache($cache);
//        */
///*
                
                /******** mysql 配置
//                table gateway save handle
                $tableGateway = new \Zend\Db\TableGateway\TableGateway('system_session', $sm->get('Zend\Db\Adapter\Adapter'));
                $saveHandler  = new \Zend\Session\SaveHandler\DbTableGateway($tableGateway, new \Zend\Session\SaveHandler\DbTableGatewayOptions());
//*/

/*
//                /******** memcache 配置
                $cache = \Zend\Cache\StorageFactory::factory(array(
                    'adapter' => array(
                       'name' => 'memcached',
                       'options' => array(
                           'server' => '127.0.0.1',
                       ),
                    )
                ));
                $saveHandler = new \Zend\Session\SaveHandler\Cache($cache);
*/

                $config = new \Zend\Session\Config\StandardConfig();
                $config->setOptions(array(
                    'name' => '1000newsessid',
                    'cache_expire' => 18000,
                    'rememberMeSeconds' => 60*60*24*14,
                ));
                $manager     = new \Zend\Session\SessionManager($config);
                $manager->start();
                return $manager;
            },
        ),
    ),
);