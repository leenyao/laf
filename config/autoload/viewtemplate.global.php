<?php
/**
 * viewtemplate Configuration
 *
 */
return array(
    'viewtemplate' => array(
        'zfcuser/login' => 'admin/user/login',
        '__defaultlayout'  => 'layout/layout',
    ),
    'preg_viewtemplate' => array(
        '/zfcadmin\//i'  => 'layout/admin',
        '/userprofile/i'  => 'layout/admin',
//        '/(zfcadmin\/user_company\/)[\w]+/' => 'layout/company',
    ),
);
