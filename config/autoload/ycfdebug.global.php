<?php
$aCacheConfig = file_exists(__DIR__."/cache.local.php") ? require __DIR__."/cache.local.php" : require __DIR__."/cache.global.php";
$aTmp = array(
	'debugconfig' => array(
//		'allowips' => array("127.0.0.1", "192.168.*", "101.231.93.46", "222.66.4.242", "101.231.104.150", "222.67.209.233"),//allow ips. empty means no forbidden
		'enable' => false,//true=>write cache file
		'adapter' => 'redis',//file|memcache|redis
		'cachepath' => __DIR__.'/../../data/cache/ycheukf.debug.html',
		'ignore_request' => array('.css', '.js', '.ico', '.png', '.gif', '.jpg', '.peng', 'oauth/authorize', 'system/vcode'),
		"memcache_config" => array(
			'jquery_keyname' => 'ycfdebug_jquery_key',
		),
		"redis_config" => array(
			'jquery_keyname' => 'ycfdebug_jquery_key',
		),
	),
);
$aTmp['debugconfig']['memcache_config']['servers'] = $aCacheConfig['cache_config']['params']['servers'];
//var_dump($aTmp['debugconfig']['memcache_config']);
return $aTmp;