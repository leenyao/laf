<?php
/**
 * ZfcRbac Configuration
 *
 * If you have a ./config/autoload/ directory set up for your project, you can
 * drop this config file in it and change the values as you wish.
 */
$settings = array(
    /**
     * The default role that is used if no role is found from the
     * role provider.
     */
    'anonymousRole' => 'guest',

    /**
     * Flag: enable or disable the routing firewall.
     */
    'firewallRoute' => false,


    /**
     * Flag: enable or disable the controller firewall.
     */
    'firewallController' => false,

    /**
     * Set the view template to use on a 403 error.
     */
    'template' => 'error/403',

    /**
     * flag: enable or disable the use of lazy-loading providers.
     */
    'enableLazyProviders' => true,

    'firewalls' => array(
        'ZfcRbac\Firewall\Route' => array(//权限配置
//            array('route' => 'zfcadmin/*', 'roles' => array('superadmin', 'admin', 'user-member')),
//            array('route' => 'zfcuser/login', 'roles' => '*'),
//            array('route' => 'debug', 'roles' => array('guest', 'admin'))
        ),
        'ZfcRbac\Firewall\Controller' => array(
//            array('controller' => '*', 'actions' => '*', 'roles' => 'admin')
        ),
    ),

    'providers' => array(
        'ZfcRbac\Provider\AdjacencyList\Role\DoctrineDbal' => array(
            'connection' => '\Rbac\Doctrine\DBAL\Connection',
            'options' => array(
                'table'         => 'rbac_role',
                'id_column'     => 'id',
                'name_column'   => 'role_name',
//                'join_column'   => 'parent_role_id'
            )
        ),
        'ZfcRbac\Provider\Generic\Permission\DoctrineDbal' => array(
            'connection' => '\Rbac\Doctrine\DBAL\Connection',
            'options' => array(
                'role_table'            => 'rbac_role',
                'role_join_table'       => 'rbac_role_permission',
                'role_id_column'        => 'id',
                'role_name_column'      => 'role_name',
                'role_join_column'      => 'role_id',
                'permission_table'      => 'rbac_permission',
                'permission_id_column'  => 'id',
                'permission_join_column'=> 'perm_id',
                'permission_name_column'=> 'perm_name',
            )
        ),
    ),
//    'providers' => array(
//        'ZfcRbac\Provider\Generic\Role\InMemory' => array(
//            'roles' => array(
//                'admin',
//                'member' => array('admin'),
//                'guest' => array('member'),
//            ),
//        ),
//        'ZfcRbac\Provider\Generic\Permission\InMemory' => array(
//            'permissions' => array(
//                'admin' => array('admin'),
//            )
//        ),
//    ),

    /**
     * Set the identity provider to use. The identity provider must be retrievable from the
     * service locator and must implement \ZfcRbac\Identity\IdentityInterface.
     */
    'identity_provider' => 'standard_identity'
);

$serviceManager = array(
    'factories' => array(
        'ZfcRbac\Service\Rbac' => 'ZfcRbac\Service\RbacFactory',

        'standard_identity' => function ($sm) {
                if (PHP_SAPI === 'cli') return;
                $aRoles = array('guest');

                //通过route来判断该次请求是否需要读取用户相关的用户组
                $sPermission = \YcheukfCommon\Lib\Functions::getPermissionByRoute($sm->get('httprouter')->match($sm->get('request')));
                $bChkFlag = \YcheukfCommon\Lib\Functions::_chkPermissionFilter($sm, $sPermission);
                $bAjax = $sm->get('request')->isXmlHttpRequest();

                $aAllowRoute = array('zfcadmin/home');//一些route, 所有人都能带身份访问
                if(in_array($sPermission, $aAllowRoute)||$bChkFlag || $bAjax){//不过滤的请求 || ajax请求
                    $oAuthService = ($sm->get('zfcuser_auth_service'));
                    $bLogin = ($oAuthService->hasIdentity());
                    if(preg_match("/.*ycfdebug.*/", $sm->get('request')->getRequestUri())){
                        $aRoles = array('guest');
                    }elseif($bLogin){//获取该用户的权限
                        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($sm, $oAuthService->getIdentity());
                    }else{
                        $aRoles = array('guest');
                    }
                }else{
                }
//                \YcheukfCommon\Lib\Functions::debug($aRoles,  "[inline]---[standard_identity]---aRoles");
                $identity = new \ZfcRbac\Identity\StandardIdentity($aRoles);
               return $identity;
        },
    )
);

/**
 * You do not need to edit below this line
 */
return array(
    'zfcrbac' => $settings,
    'service_manager' => $serviceManager,
);
