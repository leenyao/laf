<?php
namespace #NAMESPACE#\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class #CLASSNAME# extends \YcheukfCommon\Lib\Restapi{
    function delete($sResource=null, $sId="") {
        $this->_init($sResource, true);
        $sResourceKey = $sResource;
        $aReturn = $this->doDelete(
            $sResourceKey, //要删除的资源
            array("m1005_id"=>2),  //要更新的资源字段
            $this->sUserId,  //用户id
            $sId //资源id
        );
    }
    /*
    * post/put 调用本函数
    */
    function _postConfig($sResource=null, $sId="", $aPostData) {
        $sApidocsKey = 'order';
        $sResourceKey = $sResource;
        $aPostData = $this->_fmtPostData($this->aConfig['apiservice']['customer'], $aPostData, $sApidocsKey);
//var_dump($aPostData);
        $aDocsConf = array(
            $sApidocsKey => array(
                'resource' => $sResourceKey,
                'select' => array(
                    'dataset' => array(
                        'user_id' => $this->sUserId,
                    ),
                    'where' => array()
                ),
                'child' => array(
                    'items' => array(
                        'resource' => 'hcoffee_order_items',
                        'select' => array(
                            'dataset' => array(
                                'order_id' => '[=fatherid=]',
                            ),
                        ),
                        'delete_dataset' => array('m1006_id' => 2,),
                        'child' => array(
                            'attrs' => array(
                                'resource' => 'hcoffee_order_items_options',
                                 'delete_dataset' => array('status' => 2,),
                                'select' => array(
                                    'dataset' => array(
                                        'order_items_id' => '[=fatherid=]',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf[$sApidocsKey]['select']['where']['id'] = $sId;
        }else{
            $aDocsConf[$sApidocsKey]['select']['dataset']['created_time'] = date('Y-m-d H:i:s');
        }
        return $this->doSave($aPostData, $aDocsConf);
    }
    function get($sResource=null, $sId="") {
        $this->_init($sResource, 1);//第二个参数 1=>验证token
        $aDocsConf = array(
            $sResource => array(//apidocs键值
                'resource' => $sResource,//apidocs键值对应资源名
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    'where' => array()
                ),
//                'child' => array(//子元素
//                    'items' => array(//子元素的apidocs键值
//                        'resource' => 'hcoffee_items',//子元素的对应资源名
//                        'select' => array('where' =>array('m1004_id'=>1, 'category_id'=>'[=field=]id')),
//                        'child' => array(
//                            'attrs' => array(
//                                'resource' => 'view_item_option_1001',
//                                'select' => array('where' =>array('status'=>1, 'resid'=>'[=field=]id')),
//                            ),
//                        ),
//                    ),
//                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf[$sResource]['select']['where']['id'] = $sId;
        }
        return $this->returnGet($aDocsConf, $sId, array('cachekey'=>__FUNCTION__.'_GET_'.$sId, 'expiredtime'=>3600));
    }
    function post($sResource=null, $sId="") {
        $this->doPost($sResource, $sId);
    }
    function put($sResource=null, $sId="") {
        $this->doPut($sResource, $sId);
    }

}