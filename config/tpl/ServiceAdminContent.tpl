<?php

namespace #NAMESPACE#\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class AdminContent extends \Application\Service\AdminContent
{

    public function preSave($aForm, $sSource, $nCid, $nId){
        $sCurrentTime = date("Y-m-d H:i:s");
        $aForm = parent::preSave($aForm, $sSource, $nCid, $nId);
        switch($sSource){
            case 'user':
            break;
        }
        return $aForm;
    }

   public function afterSave($aForm, $sSource, $sSaveId, $nCid=null, $nFid=null){
        parent::afterSave($aForm, $sSource, $sSaveId, $nCid, $nFid);
    }
}
