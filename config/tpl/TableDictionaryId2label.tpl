<?php
namespace #NAMESPACE#\YcheukfReportExt\Report\Dictionary;


/**
 * id转label类
 *
 * 负责将db类中获得id转变成相应的label
 *
 * @author   ycheukf@gmail.com
 * @package  Dictionary
 * @access   public
 */
class Id2label extends \YcheukfReportExt\Report\Dictionary\Id2label
{}
