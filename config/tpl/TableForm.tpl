<?php
namespace #NAMESPACE#\Form;
class #CLASSNAME# extends \Application\Form\Common{

    public function __construct($oController, $nEntityStep=null, $nEntityId=null, $sEntitySource=null, $nEntityCid=null, $nEntityFid=null)
    {
        $this->setController($oController, __CLASS__);
        $this->setActionSource($sEntitySource);
        $this->setActionId($nEntityId);
        $this->setActionCid($nEntityCid);
        $this->setActionFid($nEntityFid);
        $this->setActionStep($nEntityStep);
        var_dump($this->nEntityId, $this->nEntityCid, $this->nEntityFid, $this->nEntityStep);
    	/*
    	
        $this->aFormElement = $this->addMetadataList($this->aFormElement, 7002);

    	 */
        
        $aM7005 = $this->addMetadataList($this->aFormElement, 101);

        $this->aFormElement = array();

        // 增加 metadata为 101的 select list
        $this->aFormElement = $this->addMetadataList($this->aFormElement, 101);

        $this->aFormElement["id"] = array(
                'type' => 'hidden',
                'attributes' => array('type'=>'hidden'),
                'options' => array('tips'=>""),
        );
        if (empty($this->nEntityId)) {// 新增

            $this->aFormElement["m7005_id"] = array(
                    'type' => 'select',
                    'attributes' => array('class'=>'select-tab'),
                    'options' => array(
                        'value_options' => $aM7005,
                    ),
            );

            $this->aFormElement["r1"] = array(
                    'type' => 'hidden',
                    'attributes' => array('type'=>'hidden', "value"=>0),
                    'options' => array('tips'=>""),
            );
        }else{//修改

            //静态数据
            $this->aFormElement["static_m7005_id"] = array(
                    'type' => 'element',
                    'attributes' => array("value"=>$aM7005[$aRb_ghr_orgnizations['m7005_id']]),
                    'options' => array('helpname'=>'static'),

            );

        }

        $this->aFormElement["Submitcancel"] = array(
            'type' => 'button',
            'options' => array(
                'helpname'=>'Submitcancel',
            ),
        );

        /*

        $this->aFormElement["user_id"] = array(//
            'type' => 'select',
            'attributes' => array('class'=>'select-tab'),
            'options' => array(
                'value_options' => $aUserId,
            ),
        );
        $this->aFormElement["start"] = array(
            'type' => 'date',
             'attributes' => array('class'=>'date-picker'),
            'options' => array('table'=>$sTableSource, 'format' => 'Y-m-d',),
        );
        $this->aFormElement["content"] = array(
            'type' => 'textarea',
            'attributes' => array( 'class'=>'summernote','required' => 'required'),
            'options' => array('helpname'=>'wysiwyg'),
        );
        $this->aFormElement["code"] = array(
            'type' => 'text',
            'attributes' => array('type'=>'text'),
            'options' => array('tips'=>""),
        );
        $this->aFormElement["relation___1007"] = array(//1:n 关系数据模式
            'type' => 'select',
            'attributes' => array('class'=>'select-tab', 'multiple'=>1),
            'options' => array(
                'value_options' => $aM1007,
            ),
        );
        $this->aFormElement["in_file"] = array(//上传文件
            'type' => 'file',
            'params' => array('id'=>'in_file'),
            'attributes' => array('data-filetype'=>'csv'),
            'options' => array('helpname'=>'fileupload'),
        );

        */
           
		$this->_fmtFormElements(__CLASS__);
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');

        $sFormHeader = "sFormHeader";
        $this->setOptions(array('formheader'=> $sFormHeader));

    }
	public function _fmtElement4Data($aData=array(), $aFormElement){
		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
		if(isset($aData['id'])){//特殊处理逻辑
        }
        return $aFormElement;
    }
    
    public function getformInputFilter($nId=null, $aForm=array()){
        if (!$this->inputFilter || count($this->inputFilter) < 1) {

            $inputFilter = new \Zend\InputFilter\InputFilter();
            $factory = new \Zend\InputFilter\Factory();
            switch ($this->nEntityStep) {
                default://基础信息

                $aValidatios = array(
                    array(
                        'name' => 'not_empty',
                    ),
                   array(
                       'name' => 'Regex',
                       'options' => array(
                           'pattern' => '/(^\d{4}$)/',
                           'messages' => array(
                               'regexNotMatch' => '您输入的年份不正确',
                           )
                       )
                   ),
                );
                if (is_null($nId)) {//新增时检测是否重名
                    
                    $oVaildator = new \Application\Validator\RecordExists(
                        array(
                            "resource" => "b_ghr_annalexamine",
                            "keyfield" => "s1",
                        )
                    );
                    $oVaildator->setService($this->serviceManager);
                    $aValidatios[] = $oVaildator;

                }
                $inputFilter->add(
                    $factory->createInput(
                        array(
                            'name'     => 's1',
                            'required' => true,
                            'validators' => $aValidatios,
                        )
                    )
                );

                $inputFilter->add(
                    $factory->createInput(
                        array(
                            'name'     => 's2',
                            'required' => true,
                            'validators' => array(
                                array(
                                    'name' => 'not_empty',
                                ),
                            ),
                        )
                    )
                );
                break;
            }

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}