<?php
namespace #NAMESPACE#\YcheukfReportExt\Inputs;
class #CLASSNAME# extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. m101_id为删除状态, 数据库应该有此字段1=>正常,0=>删除
                array(
                    'key' => 'm101_id', //操作符
                    'op' => 'in', //操作符=,in,like
                    'value' => "(1)"//过滤的值
                ),
			),
			'input'=>array(
				'detail'=>array(
					'is_editable' => false,
					'is_viewable' => false,
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								$sSource.'___id',
								array(
                                    'key' => 'm101_id',
                                    'group' => false,
                                ),
								'modified',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
			'custom' => array(
				'subtr' => true,//是否有子行
                'subtr_expend' => false, //子行是否初始站看
				'subtr_function' => function($nId) use ($sSource){//子行内容
                    $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sM);
                    $aSourceData = \Application\Model\Common::getResourceById($this->sM, $sSource, $nId);
                    $sHtml = "<p/>
                    	{$aSourceData['id']}
                    ";
                    return nl2br($sHtml);
                }
            ),
		);
		$aInput = $this->_fmtDetailFilters(
            $sSource, 
            $aInput, 
            $this->getReportParams(), 
            array('m101_id'),  //filterable field
            array('modified'), //fulltext search field
            array('m102_id' => array('op' => 'in', 'value' => "1")) // default filters
        );
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}