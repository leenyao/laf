<?php
namespace #NAMESPACE#\Toolbar;
class #CLASSNAME# extends \Application\Toolbar\Toolbar{
    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
        $aReturn = array();

        //新增按钮
        $aReturn[] = $this->_getAddButton($oController, $sSource, $nId);


        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('状态操作:'),
            ),
        );
        $nTypeTmp = 101;
        $aStatus = \Application\Model\Common::getResourceMetadaList($sm, $nTypeTmp);
        foreach($aStatus as $nStatus=>$nLabel){
            $aReturn[] = $this->_getPublishButton($sm, $oController, $nTypeTmp, $sSource, $nStatus, null, 'm'.$nTypeTmp.'_id');
        }
        $aReturn[] = array(
            'type' => 'pipe',
        );
        //下载
        $aReturn[] = array(
            'type' => 'detail-button-download',
            'cls' => 'download',
            'iconCls' => 'licon-download',
            'position' => 'left',
            'params' => array(
                'request' => array('returnType'=>'csv_all'),
                'label' => $oController->translate('download'),
            ),
        );
        //自定义下载
        $aReturn[] = array(
            'type' => 'customdownload-dialog',
            'cls' => 'customdownload',
            'iconCls' => 'licon-customdownload',
            'position' => 'left',
            'params' => array(
                // 'object_name' => 'object_name',
                'data-tips' => 'data-tips',
                'data-param-name' => 'data-param-name',
                'content-url' => $sm->get('httprouter')->assemble(array('type'=>$sSource, 'cid'=>$nId), array('name'=>'zfcadmin/ajaxlistcustomerfield')),
                'label' => $oController->translate('custom download'),
                'dialog-title' => "请选择需要下载的字段",
            ),
        );

        $aReturn[] = array(
            'type' => 'detail-br',
        );



        // 提供 metadata=101的过滤
        $aReturn[] = $this->getMetaFilter(101);

        $aReturn[] = array(
            'type' => 'detail-searchbar',
            'position' => 'right',
            'params' => array(
                'label' => $oController->translate('搜索'),
                'tips' => 'fill chart plz',

                'placeholder' => '模糊搜索关键字',
                'object_name' => 'q_keyword',
            ),
        );

        $aReturn = $this->_fmtPermission($sm, $aReturn);
        return $aReturn;

    }
}