<?php

namespace YcheukfMigration\Migration;

use YcheukfMigration\Library\AbstractMigration;
use Zend\Db\Metadata\MetadataInterface;

class Version10001 extends AbstractMigration
{
    public static $description = "set up";

    public function up(MetadataInterface $schema)
    {
        $aDbConfig = $this->serviceManager->get('config');
        $aDsn = \YcheukfCommon\Lib\Functions::getParamFromDSN($aDbConfig['db_master']['dsn']);
        $sDbName = $aDsn['dbname']; 
        $sDbHost = $aDsn['host']; 
        $sDbPort = $aDsn['port']; 
        $sDbUsername = $aDbConfig['db_master']['username'];
        $sDbPassword = $aDbConfig['db_master']['password'];
        $aTables = $schema->getTableNames();

        system("mysql -u".$sDbUsername." -p".$sDbPassword." --port=".$sDbPort." -h".$sDbHost."  --default-character-set=utf8 -e \"CREATE DATABASE IF NOT EXISTS ".$sDbName."\"");
        //加载框架
        system("mysql -u".$sDbUsername." -p".$sDbPassword." --port=".$sDbPort." -h".$sDbHost."  --default-character-set=utf8 ".$sDbName." < ".__DIR__."/frameworker.sql");

        //加载iac地域
        system("mysql -u".$sDbUsername." -p".$sDbPassword." --port=".$sDbPort." -h".$sDbHost."  --default-character-set=utf8 ".$sDbName." < ".__DIR__."/location_iac.sql");
        //加载地域
        if(count($aTables)){
            $bRunFlag = true;
            foreach($aTables as $sTable){
                if('location_province' == $sTable || 'location_city' == $sTable || 'location_district' == $sTable){
                    $bRunFlag = false;
                    break;
                }
            }
            if($bRunFlag)system("mysql -u".$sDbUsername." -p".$sDbPassword." -h".$sDbHost."   --port=".$sDbPort." --default-character-set=utf8 ".$sDbName." < ".__DIR__."/location.sql");
        }
    }


    public function down(MetadataInterface $schema)
    {

        //reset mongodb
//        $dm = \YcheukfCommon\Lib\Functions::getMongoDm($this->serviceManager);
//        foreach($dm->getConnection()->selectDatabase($dm->getConfiguration()->getDefaultDB())->listCollections() as $oRow)
//            var_export($oRow->drop());

        //reset mysql
        $aTables = $schema->getTableNames();
        $aSql = array();
        $aSql[] =     "    SET FOREIGN_KEY_CHECKS = 0;";
        if(count($aTables)){
            foreach($aTables as $sTable){
                if(\YcheukfMigration\Library\Migration::MIGRATION_TABLE == $sTable)continue;
                if('location_city' == $sTable)continue;
                if('location_district' == $sTable)continue;
                if('location_province' == $sTable)continue;
                if('b_iacip_list' == $sTable)continue;
                if('debug_cachekeys' == $sTable)continue;
                if('rbac_permission' == $sTable)continue;
                if('system_iostoken' == $sTable)continue;
                if('system_session' == $sTable)continue;
                if('wx_relation' == $sTable)continue;
                $aSql[] =     "drop table IF EXISTS $sTable ;";
            }
        }
        $sSql = join("\n", $aSql);

        $this->addSql($sSql);    
    }
}
