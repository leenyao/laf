ALTER TABLE `system_batch_jobs` CHANGE COLUMN `user_id` `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '本任务所操作的用户', ADD COLUMN `op_user_id` int NOT NULL DEFAULT 0 COMMENT '操作用户的id' AFTER `modified`;


ALTER TABLE `system_userlog` ADD COLUMN `serverinfo` longtext DEFAULT NULL  COMMENT '请求时的服务器信息';


