<?php

namespace YcheukfMigration\Migration;

use YcheukfMigration\Library\AbstractMigration;
use Zend\Db\Metadata\MetadataInterface;

class Version10003 extends AbstractMigration
{
    public static $description = "set up";

    public function up(MetadataInterface $schema)
    {
        $aDbConfig = $this->serviceManager->get('config');
        $aDsn = \YcheukfCommon\Lib\Functions::getParamFromDSN($aDbConfig['db_master']['dsn']);
        $sDbName = $aDsn['dbname']; 
        $sDbHost = $aDsn['host']; 
        $sDbPort = $aDsn['port']; 
        $sDbUsername = $aDbConfig['db_master']['username'];
        $sDbPassword = $aDbConfig['db_master']['password'];
        $aTables = $schema->getTableNames();

        //加载框架
        system("mysql -u".$sDbUsername." -p".$sDbPassword." --port=".$sDbPort." -h".$sDbHost."  --default-character-set=utf8 ".$sDbName." < ".__DIR__."/Version10003.sql");
    }


    public function down(MetadataInterface $schema)
    {
 
    }
}
