/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : l_project

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2015-01-05 16:31:14
*/

SET FOREIGN_KEY_CHECKS=0;





-- ----------------------------
-- Table structure for `b_suggestion`
-- ----------------------------
DROP TABLE IF EXISTS `b_suggestion`;
CREATE TABLE `b_suggestion` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL DEFAULT '',
  `content` longtext NOT NULL COMMENT '描述',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `debug_cachekeys`
-- ----------------------------
CREATE TABLE IF NOT EXISTS `debug_cachekeys` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'aa',
  `doneflag` tinyint(4) DEFAULT '0' COMMENT '是否已经在程序中处理完成',
  `cacheflag` tinyint(4) DEFAULT '0' COMMENT '本语句是否使用缓存',
  `title` text NOT NULL COMMENT '标题',
  `md5id` varchar(255) NOT NULL DEFAULT '',
  `lifetime` bigint(20) NOT NULL DEFAULT '0' COMMENT '生存周期, 秒',
  `resource` varchar(255) DEFAULT NULL,
  `memo` text DEFAULT NULL,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `md5id` (`md5id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='aaa';

-- ----------------------------
-- Table structure for `oauth_access_tokens`
-- ----------------------------
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(255) NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `expires` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`,`user_id`),
  KEY `access_token` (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `oauth_clients`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0',
  `client_secret` varchar(255) NOT NULL DEFAULT '',
  `redirect_uri` varchar(255) NOT NULL,
  `memo` varchar(255) NOT NULL DEFAULT '',
  `client_name` varchar(255) NOT NULL COMMENT '客户端名称',
  PRIMARY KEY (`id`),
  KEY `client_secret` (`client_secret`,`client_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_clients
-- ----------------------------
INSERT INTO `oauth_clients` VALUES ('1', '1000', '098f6bcd4621d373cade4e832627b4f6', '', 'test', '');
INSERT INTO `oauth_clients` VALUES ('2', '2000', 'dcbd438abd93ef7b82ccf4e563db3f03', '', 'appserver', '');
INSERT INTO `oauth_clients` VALUES ('3', '3000', '2239c29d5987238724a34ea5ffcbd0f1', '', 'webserver', '');
INSERT INTO `oauth_clients` VALUES ('4', '4000', 'c209f10d550c2b156f64db6057f843a9', '', 'dbproxy', '');
INSERT INTO `oauth_clients` VALUES ('5', '5000', '1bf738448f9b84b3d8d48369c0015509', '', 'wechat', '');

-- ----------------------------
-- Table structure for `oauth_refresh_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refresh_token` varchar(255) NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `expires` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `refresh_token` (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_refresh_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_users`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_users`;
CREATE TABLE `oauth_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `from_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>common user,2=>qq user,3=>weibo, 4=>公司用户, 5=>名片用户, 6=>wechat',
  `email` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `m154_id` tinyint(1) NOT NULL DEFAULT '3' COMMENT '用户注册的操作系统id',
  `retry_count` int(10) NOT NULL DEFAULT '0' COMMENT '重试次数',
  `last_login_timestamp` DATETIME NOT NULL DEFAULT '2000-01-01 01:01:01',
  `allowlogin_timestamp` DATETIME NOT NULL DEFAULT '2001-01-01 01:01:01' COMMENT '大于本时间的用户可以登录',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- ----------------------------
-- Records of oauth_users
-- ----------------------------
-- admin1234
INSERT INTO `oauth_users` VALUES ('1', 'admin', 'c93ccd78b2076528346216b3b2f701e6', '1', '', '', '2014-06-25 15:31:42', '3', 0, '2000-01-01 01:01:01', '2000-01-01 01:01:01');
-- member123
INSERT INTO `oauth_users` VALUES ('1000', 'member', 'a510166163833c79aa703646f59c04bb', '1', '', '', '2014-06-25 15:31:42', '3', 0, '2000-01-01 01:01:01', '2000-01-01 01:01:01');

-- ----------------------------
-- Table structure for `rbac_permission`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_permission`;
CREATE TABLE `rbac_permission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `perm_name` varchar(255) NOT NULL DEFAULT '',
  `cate_name` varchar(200) NOT NULL DEFAULT '' COMMENT '分类名称',
  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '发布状态. metadata.102',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fid` SMALLINT(1) unsigned zerofill NOT NULL DEFAULT '100',
  `perm_label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `perm_name` (`perm_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rbac_permission
-- ----------------------------
-- lv1 1-100
-- INSERT INTO `rbac_permission` VALUES ('1', '', '', '1', '2013-11-24 15:24:40', '0', 'menu_ads');
-- INSERT INTO `rbac_permission` VALUES ('2', '', '', '1', '2013-11-24 15:19:44', '0', 'menu_help');
INSERT INTO `rbac_permission` VALUES ('98', '', '', '1', '2013-11-24 15:25:28', '0', 'menu_user');
INSERT INTO `rbac_permission` VALUES ('99', '', '', '1', '2013-11-24 15:25:31', '0', 'menu_system');

-- lv2 101-1000
-- INSERT INTO `rbac_permission` VALUES ('101', '', '', '1', '2013-11-24 15:24:40', '1', 'menu_ads');
-- INSERT INTO `rbac_permission` VALUES ('102', '', '', '1', '2013-11-24 15:24:40', '2', 'menu_help');
INSERT INTO `rbac_permission` VALUES ('903', '', '', '1', '2013-11-24 15:24:40', '98', 'menu_user');
INSERT INTO `rbac_permission` VALUES ('904', '', '', '1', '2013-11-24 15:24:40', '99', 'menu_rbacrole');
INSERT INTO `rbac_permission` VALUES ('905', '', '', '1', '2013-11-24 15:24:40', '99', 'menu_cacheadmin');

-- lv3 1001+
-- INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/contentlist/ads', '', '1', '2013-11-24 15:42:19', '101', 'view');
-- INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/contentedit/ads', '', '1', '2013-11-25 13:35:12', '101', 'edit');

-- INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/contentlist/help', '', '1', '2013-11-24 15:42:19', '102', 'view');
-- INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/contentedit/help', '', '1', '2013-11-25 13:35:12', '102', 'edit');

INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/contentlist/user', '', '1', '2013-11-24 15:42:19', '903', 'view');
INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/contentedit/user', '', '1', '2013-11-25 13:35:12', '903', 'edit');

INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/contentlist/rbacrole', '', '1', '2013-11-24 15:42:19', '904', 'view');
INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/contentedit/rbacrole', '', '1', '2013-11-25 13:35:12', '904', 'edit');

INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/contentlist/cacheadmin', '', '1', '2013-11-24 15:42:19', '905', 'view');
INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/clearallcache', '', '1', '2013-11-25 13:35:12', '905', 'edit');

-- ----------------------------
-- Table structure for `rbac_role`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role`;
CREATE TABLE `rbac_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_role_id` int(11) unsigned NOT NULL DEFAULT '0',
  `role_name` varchar(32) NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `parent_role_id` (`parent_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rbac_role
-- ----------------------------
INSERT INTO `rbac_role` VALUES ('1', '0', 'superadmin', '2014-06-25 15:31:15');
INSERT INTO `rbac_role` VALUES ('2', '0', 'admin', '2014-06-25 15:31:15');
INSERT INTO `rbac_role` VALUES ('8', '0', 'user-member', '2014-06-25 15:31:57');
INSERT INTO `rbac_role` VALUES ('100', '0', 'guest', '2014-06-25 15:31:15');

-- ----------------------------
-- Table structure for `rbac_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role_permission`;
CREATE TABLE `rbac_role_permission` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned NOT NULL,
  `perm_id` int(11) unsigned NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `role_id2` (`role_id`,`perm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `system_andriodtoken`
-- ----------------------------
DROP TABLE IF EXISTS `system_andriodtoken`;
CREATE TABLE `system_andriodtoken` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `andriodtoken` varchar(255) NOT NULL COMMENT 'ios token',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `andriodtoken` (`andriodtoken`) USING BTREE,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of system_andriodtoken
-- ----------------------------

-- ----------------------------
-- Table structure for `system_contentblock`
-- ----------------------------
DROP TABLE IF EXISTS `system_contentblock`;
CREATE TABLE `system_contentblock` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '1=>合作伙伴, 2=>联系我们',
  `name` varchar(255) NOT NULL,
  `content` longtext NOT NULL COMMENT '描述',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of system_contentblock
-- ----------------------------

-- ----------------------------
-- Table structure for `system_email_list`
-- ----------------------------
DROP TABLE IF EXISTS `system_email_list`;
CREATE TABLE `system_email_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL COMMENT 'email',
  `m159_id` tinyint(1) NOT NULL DEFAULT '2' COMMENT '该email属于哪一类',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`m159_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;


-- ----------------------------
-- Table structure for `system_iostoken`
-- ----------------------------
DROP TABLE IF EXISTS `system_iostoken`;
CREATE TABLE `system_iostoken` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `iostoken` varchar(255) NOT NULL COMMENT 'ios token',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `m158_id` tinyint(1) NOT NULL DEFAULT '2' COMMENT '该token由哪个app产生, 默认为牵牛招聘(2)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `iostoken` (`iostoken`) USING BTREE,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for `system_jobs`
-- ----------------------------
DROP TABLE IF EXISTS `system_jobs`;
CREATE TABLE `system_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m160_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '哪种类型job',
  `m159_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '哪种用户类型',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>初始化,2=>开始执行,5=>进行中,8=>已完成, 9=>失败',
  `json_param` longtext NOT NULL COMMENT 'json变量:{''mailTitle'':''mail的title'', ''mailBody'':''mail的body''}',
  `memo` longtext DEFAULT NULL COMMENT '摘要',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of system_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for `system_lock`
-- ----------------------------
DROP TABLE IF EXISTS `system_lock`;
CREATE TABLE `system_lock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `count` int(11) NOT NULL COMMENT '操作次数',
  `last` DATETIME NULL DEFAULT NULL COMMENT '上次操作时间',
  `type` tinyint(1) NOT NULL COMMENT '操作类型 1=>登录,2=>发送邮件,3=>发送短信',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户操作锁';

-- ----------------------------
-- Records of system_lock
-- ----------------------------

-- ----------------------------
-- Table structure for `system_medialib`
-- ----------------------------
DROP TABLE IF EXISTS `system_medialib`;
CREATE TABLE `system_medialib` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `memo` text DEFAULT NULL,
  `link` text NOT NULL,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of system_medialib
-- ----------------------------

-- ----------------------------
-- Table structure for `system_metadata`
-- ----------------------------
DROP TABLE IF EXISTS `system_metadata`;
CREATE TABLE `system_metadata` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `md5id` varchar(50) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '资源类型',
  `p_type` int(11) NOT NULL DEFAULT '0' COMMENT '父资源type',
  `p_resid` int(11) NOT NULL DEFAULT '0' COMMENT '父资源id',
  `resid` int(11) NOT NULL DEFAULT '0' COMMENT '资源id',
  `label` varchar(250) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '发布状态. 1=>正常, 2=>删除',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `md5id` (`type`,`md5id`),
  KEY `type` (`type`,`resid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (6, 1, 1);
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (6, 2, 1);
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (6, 1000, 8);
;
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (101, 1, '正常');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (101, 2, '删除');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (102, 1, '正常');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (102, 2, '禁止');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (102, 3, '删除');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (134, 1, '发布');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (134, 2, '审核中');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (134, 3, '暂停发布');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (134, 4, '已过期');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (134, 5, '删除');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (135, 1, '是');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (135, 2, '否');

INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (154, 1, 'Android');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (154, 2, 'IOS');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (154, 3, 'PC');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (154, 4, 'Windows');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (154, 5, 'Wechat');

INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (155, 1, '等待付款');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (155, 2, '转入退款');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (155, 3, '未支付');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (155, 4, '已关闭');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (155, 5, '已撤销');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (155, 6, '用户支付中');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (155, 7, '支付失败');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (155, 8, '支付成功');

INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (156, 1, '微信支付');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (156, 2, '支付宝');


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (158, 1, '初始化');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (158, 5, '进行中');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (158, 10, '成功');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (158, 20, '失败');


-- 用户自定义log分类
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (159, 1, '未分类');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (159, 11, 'LUR更新缓存');

-- ----------------------------
-- Table structure for `system_relation`
-- ----------------------------
DROP TABLE IF EXISTS `system_relation`;
CREATE TABLE `system_relation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '资源类型',
  `fid` int(11) NOT NULL DEFAULT '0' COMMENT 'father id',
  `cid` int(11) NOT NULL DEFAULT '0' COMMENT 'child id',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '发布状态. 1=>正常, 2=>删除',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY type (`type`,`fid`),
  KEY type2 (`type`,`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `system_payment`
-- ----------------------------
DROP TABLE IF EXISTS `system_payment`;
CREATE TABLE `system_payment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '内部订单id',
  `order_no` varchar(200) DEFAULT NULL COMMENT '内部订单no',
  `paymentorder_id` int(11) DEFAULT NULL COMMENT '平台内部订单id',
  `m155_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '付款状态',
  `m156_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '付款平台',
  `memo` text DEFAULT NULL COMMENT '付款信息',
  `pay_config` text DEFAULT NULL COMMENT '付款配置',
  `pay_response` text DEFAULT NULL COMMENT '付款结果',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '发布状态. 1=>正常, 2=>删除',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `order_no` (`order_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `system_report`
-- ----------------------------
DROP TABLE IF EXISTS `system_report`;
CREATE TABLE `system_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `resid` int(11) DEFAULT NULL COMMENT '被举报内容ID',
  `type` tinyint(4) DEFAULT '1' COMMENT '类型. 1=>八卦',
  `memo` text DEFAULT NULL COMMENT '举报原因,内容',
  `status` tinyint(4) DEFAULT '0' COMMENT '管理员处理状态. 0=>未处理；1=>已处理',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_report
-- ----------------------------

-- ----------------------------
-- Table structure for `system_session`
-- ----------------------------
DROP TABLE IF EXISTS `system_session`;
CREATE TABLE `system_session` (
  `id` char(32) NOT NULL DEFAULT '',
  `name` char(32) NOT NULL DEFAULT '',
  `modified` int(11) DEFAULT NULL,
  `lifetime` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of system_session
-- ----------------------------

-- ----------------------------
-- Table structure for `system_trigger_log`
-- ----------------------------
DROP TABLE IF EXISTS `system_trigger_log`;
CREATE TABLE `system_trigger_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(200) NOT NULL COMMENT 'table name',
  `optype` varchar(5) NOT NULL COMMENT '操作类型',
  `metadata_type` bigint(11) NOT NULL DEFAULT '0' COMMENT 'metadata.type',
  `resid` bigint(11) NOT NULL COMMENT 'table.id',
  `syncflag_1` tinyint(3) NOT NULL DEFAULT '0' COMMENT '公司报表同步标识',
  `syncflag_2` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'solr同步标识&下载简历文件更新标识',
  `syncflag_3` tinyint(3) NOT NULL DEFAULT '0' COMMENT '给用户发送email的队列;向bbs同步用户;整理m159用户',
  `syncflag_4` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'ios推送标识',
  `syncflag_5` tinyint(3) NOT NULL DEFAULT '0' COMMENT '待定',
  `syncflag_6` tinyint(3) NOT NULL DEFAULT '0' COMMENT '待定',
  `syncflag_7` tinyint(3) NOT NULL DEFAULT '0' COMMENT '待定',
  `syncflag_8` tinyint(3) NOT NULL DEFAULT '0' COMMENT '待定',
  `memo` longtext COMMENT '',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `crt_timestamp` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `syncflag_4` (`syncflag_4`),
  KEY `syncflag_1` (`syncflag_1`),
  KEY `syncflag_2` (`syncflag_2`),
  KEY `syncflag_3` (`syncflag_3`),
  KEY `syncflag_4_2` (`syncflag_4`),
  KEY `syncflag_5` (`syncflag_5`),
  KEY `resid` (`type`,`resid`,`optype`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `system_userlog`
-- ----------------------------
DROP TABLE IF EXISTS `system_userlog`;
CREATE TABLE `system_userlog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_users.id',
  `user_role` char(255) NOT NULL DEFAULT '',
  `url` char(255) NOT NULL DEFAULT '',
  `urllabel` char(255) NOT NULL DEFAULT '',
  `param` text,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `url` (`url`),
  KEY `urllabel` (`urllabel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `system_users`
-- ----------------------------
DROP TABLE IF EXISTS `system_users`;
CREATE TABLE `system_users` (
  `id` bigint(20) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `role_id` int(10) unsigned NOT NULL DEFAULT '100' COMMENT '角色ID,默认guest',
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `email_tmp` varchar(255) NOT NULL DEFAULT '',
  `mail_verify_no` varchar(255) NOT NULL DEFAULT '' COMMENT 'email验证字符串',
  `email_verified` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1=>已认证, 2=>未认证',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `phone_tmp` varchar(255) NOT NULL DEFAULT '',
  `phone_verify_no` varchar(255) NOT NULL DEFAULT '' COMMENT '手机验证码',
  `phone_verified` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1=>已认证, 2=>未认证',
  `display_name` varchar(50) NOT NULL DEFAULT '',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '用户头像地址',
  `password` varchar(128) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '发布状态. 1=>已发布,2=>未发布,3=>已存档',
  `state` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gender` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>male, 2=>femail',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `credit` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `m154_id` tinyint(1) NOT NULL DEFAULT '3' COMMENT '用户注册的平台id',
  `alipay_acc` varchar(255) NOT NULL DEFAULT '' COMMENT '支付宝账号',
  `birthday` date DEFAULT NULL,
  `joinworkdate` date DEFAULT NULL COMMENT '参加工作的日期',
  `location_city_id` int(11) NOT NULL DEFAULT '0' COMMENT '居住地.location_city.id',
  `memo` text DEFAULT NULL COMMENT '自我描述',
  `jobposition` varchar(255) NOT NULL DEFAULT '' COMMENT '职务名称',
  `jobcompany` varchar(255) NOT NULL DEFAULT '' COMMENT '公司名称',
  `privacy` tinyint(4) NOT NULL DEFAULT '1' COMMENT '隐私状态. 0=>隐藏电话邮箱，1=>显示电话邮箱',
  `agent_id` int(10) NOT NULL DEFAULT '0' COMMENT '代理的用户id',
  `weixin` varchar(255) NOT NULL DEFAULT '' COMMENT '微信号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  KEY `user_login_username` (`username`),
  KEY `user_nicename` (`display_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_users
-- ----------------------------
INSERT INTO system_users (`id`,`user_id`,`role_id`, `username`,`email`,`phone`,`display_name`) VALUES('1', '1', 1, 'admin', 'admin@loclahost.com', '13800138000', 'admin'); 
INSERT INTO system_users (`id`,`user_id`,`role_id`, `username`,`email`,`phone`,`display_name`) VALUES('1000', '1000', 8, 'member', 'member@loclahost.com', '13800138000', 'member'); 
-- ----------------------------
-- Table structure for `system_users_bander`
-- ----------------------------
DROP TABLE IF EXISTS `system_users_bander`;
CREATE TABLE `system_users_bander` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0',
  `bander_user_id` int(10) unsigned DEFAULT '0',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `wx_relation`
-- ----------------------------
DROP TABLE IF EXISTS `wx_relation`;
CREATE TABLE `wx_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` varchar(255) NOT NULL DEFAULT '' COMMENT '父ID',
  `cid` varchar(255) NOT NULL DEFAULT '' COMMENT '子ID',
  `status` tinyint(4) NOT NULL,
  `modified` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `from_type` tinyint(4) NOT NULL COMMENT '来源：1=>公众号，1=>分享链接',
  `re_type` tinyint(4) DEFAULT '1' COMMENT '关系类型：1=>认证，2=>收藏',
  PRIMARY KEY (`id`),
  KEY `fid` (`fid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='微信名片认证关系表';


DROP TABLE IF EXISTS `system_contact`;
CREATE TABLE `system_contact` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '联系人名字',
  `address` text NOT NULL COMMENT '',
  `phone` varchar(255) NOT NULL DEFAULT '' COMMENT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP VIEW IF EXISTS `view_role_permission_p1`;
CREATE VIEW `view_role_permission_p1` AS select `a`.`id` AS `id`,`a`.`role_id` AS `role_id`,`a`.`perm_id` AS `perm_id`,`b`.`perm_name` AS `perm_name` from (`rbac_role_permission` `a` left join `rbac_permission` `b` on((`a`.`perm_id` = `b`.`id`)));


DROP VIEW IF EXISTS `view_role_permission`;
CREATE VIEW `view_role_permission` AS select `c`.`id` AS `id`, `c`.`role_id` AS `role_id`,`c`.`perm_id` AS `perm_id`,`c`.`perm_name` AS `perm_name`,`d`.`role_name` AS `role_name` from (`view_role_permission_p1` `c` left join `rbac_role` `d` on((`c`.`role_id` = `d`.`id`)));

DELIMITER ;

DELIMITER ;
DROP TRIGGER IF EXISTS `ti_system_metadata`;
DELIMITER ;;
CREATE TRIGGER `ti_system_metadata` AFTER INSERT ON `system_metadata` FOR EACH ROW BEGIN
    INSERT INTO system_trigger_log SET type = 'system_metadata', optype = 'i', resId = NEW.id, metadata_type=NEW.type, crt_timestamp = CURRENT_TIMESTAMP();
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `tu_system_metadata`;
DELIMITER ;;
CREATE TRIGGER `tu_system_metadata` AFTER UPDATE ON `system_metadata` FOR EACH ROW BEGIN
    IF 
            OLD.id != NEW.id  or  OLD.type != NEW.type  or  OLD.resid != NEW.resid  or  OLD.label != NEW.label 
    THEN
        INSERT INTO system_trigger_log SET type = 'system_metadata', optype = 'u', resId = NEW.id, metadata_type=NEW.type, crt_timestamp = CURRENT_TIMESTAMP();
    END IF;
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `td_system_metadata`;
DELIMITER ;;
CREATE TRIGGER `td_system_metadata` AFTER DELETE ON `system_metadata` FOR EACH ROW BEGIN
        INSERT INTO system_trigger_log SET type = 'system_metadata', optype = 'd', resId = OLD.id, metadata_type=OLD.type, crt_timestamp = CURRENT_TIMESTAMP();
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `ti_system_users`;
DELIMITER ;;
CREATE TRIGGER `ti_system_users` AFTER INSERT ON `system_users` FOR EACH ROW BEGIN
    INSERT INTO system_trigger_log SET type = 'system_users', optype = 'i', resId = NEW.id, metadata_type=0, crt_timestamp = CURRENT_TIMESTAMP();
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `tu_system_users`;
DELIMITER ;;
CREATE TRIGGER `tu_system_users` AFTER UPDATE ON `system_users` FOR EACH ROW BEGIN
    IF 
            OLD.id != NEW.id  or  OLD.modified != NEW.modified 
    THEN
        INSERT INTO system_trigger_log SET type = 'system_users', optype = 'u', resId = NEW.id, metadata_type=0, crt_timestamp = CURRENT_TIMESTAMP();
    END IF;
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `td_system_users`;
DELIMITER ;;
CREATE TRIGGER `td_system_users` AFTER DELETE ON `system_users` FOR EACH ROW BEGIN
        INSERT INTO system_trigger_log SET type = 'system_users', optype = 'd', resId = OLD.id, metadata_type=0, crt_timestamp = CURRENT_TIMESTAMP();
  END
;;
DELIMITER ;



DROP VIEW IF EXISTS `system_matadata_155` ;
CREATE VIEW `system_matadata_155` AS select `system_metadata`.`id` AS `id`,`system_metadata`.`md5id` AS `md5id`,`system_metadata`.`status` AS `status`,`system_metadata`.`type` AS `type`,`system_metadata`.`p_type` AS `p_type`,`system_metadata`.`p_resid` AS `p_resid`,`system_metadata`.`resid` AS `resid`,`system_metadata`.`label` AS `label`,`system_metadata`.`modified` AS `modified` from `system_metadata` where (`system_metadata`.`type` = 155);

DROP VIEW IF EXISTS `system_matadata_156` ;
CREATE VIEW `system_matadata_156` AS select `system_metadata`.`id` AS `id`,`system_metadata`.`md5id` AS `md5id`,`system_metadata`.`status` AS `status`,`system_metadata`.`type` AS `type`,`system_metadata`.`p_type` AS `p_type`,`system_metadata`.`p_resid` AS `p_resid`,`system_metadata`.`resid` AS `resid`,`system_metadata`.`label` AS `label`,`system_metadata`.`modified` AS `modified` from `system_metadata` where (`system_metadata`.`type` = 156);


DROP TABLE IF EXISTS `system_batch_jobs`;
CREATE TABLE `system_batch_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `m158_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>初始化,2=>开始执行,5=>进行中,8=>已完成, 9=>失败',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '任务的分类',
  `jobname` varchar(200) NOT NULL DEFAULT '' COMMENT '任务名称',
  `in_file` text COMMENT '输入文件',
  `out_file` text  COMMENT '输出文件',
  `out_text` text COMMENT '输出结果',
  `out_json` longtext COMMENT '输出结果,json格式',
  `processed_time` int(8) NOT NULL DEFAULT '0' COMMENT '消耗时间',
  `memo` longtext COMMENT '摘要',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

DROP TABLE IF EXISTS `system_md5key`;
CREATE TABLE `system_md5key` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `md5key` char(64) NOT NULL DEFAULT '',
  `type` int(8) NOT NULL DEFAULT '1' COMMENT '业务数据自100起',
  `key` longtext NOT NULL,
  `data` longtext NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `md5key` (`type`,`md5key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='md5 key 仓库';

DROP TABLE IF EXISTS `system_customerlog`;
CREATE TABLE `system_customerlog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_users.id',
  `m159_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '日志类型',
  `memo` longtext COMMENT '摘要',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `m159_id` (`m159_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '用户自定义日志';


DROP TABLE IF EXISTS `system_apishell`;
CREATE TABLE `system_apishell` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(250) DEFAULT NULL COMMENT '主机ID',
  `data` longtext COMMENT 'json stats data',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `ssh2_tunnel` tinyint(1) DEFAULT '1' COMMENT '1=>建立ssh反向隧道',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `host` (`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='通过API输送给机器的shell';


DROP TABLE IF EXISTS `system_moduleconfig`;
CREATE TABLE `system_moduleconfig` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '',
  `memo` longtext COMMENT '摘要',
  `selected` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='module 的多种config配置';

