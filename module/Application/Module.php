<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
class Module
{
    public function onBootstrap(MvcEvent $event)
    {
        $app = $event->getApplication();
       $em  = $app->getEventManager();
        $sm  = $app->getServiceManager();

        \YcheukfCommon\Lib\Functions::setServiceManager($sm);
//var_dump(get_class($em));
//var_dump($sm->get('ModuleManager')->getModules());
//        $sm->get('ModuleManager')->loadModule('ZfcRbac');
//var_dump($sm->get('ModuleManager')->getModules());

        //绑定全局事件
        $this->_setAttach($app, $sm, $em);

        if (defined('APPLICATION_MODEL') && strtolower(APPLICATION_MODEL) !== "console"){
            //加载helper
            $config = new \YcheukfCommon\HelperConfig();
            $config->configureServiceManager($sm->get('ViewHelperManager'));

             //加载layout
            $em->attach(MvcEvent::EVENT_DISPATCH, array($this, 'selectLayoutBasedOnRoute'), -100);


        }

    }

    protected function _setAttach($app, $sm, $em){
        $aConfig = ($sm->get('config'));
        //触发记录修改
        $em->getSharedManager()->attach("*", 'lf_record_update', function ($e) use ($sm, $aConfig){
                return $sm->get(LAF_NAMESPACE."\Service\Record")->onUpdate($e);
        });
        //触发记录新增
        $em->getSharedManager()->attach("*", 'lf_record_insert', function ($e) use ($sm, $aConfig){
//            echo LAF_NAMESPACE."\Service\Record";
            return $sm->get(LAF_NAMESPACE."\Service\Record")->onInsert($e);
        });
        //触发用户登陆认证后事件
        $em->getSharedManager()->attach("*", 'lf_user_afterauthenticate', function ($e) use ($sm){
            return $sm->get("Application\Service\User")->afterAuthenticate($e);
        });

        if (PHP_SAPI === 'cli')return;
        if (defined('APPLICATION_MODEL') && strtolower(APPLICATION_MODEL) === "console")return;
        //修正zfcUser中的登陆过滤
        $em->getSharedManager()->attach(array('ZfcUser\Form\LoginFilter'), 'init', array('Application\Listener\LoginFilter', 'onLogin'), 200);
        //加载route event以控制用户权限
        $em->attach('route', array('Application\Listener\Route', 'onRoute'), -500);

    }

    /**
     * Select the admin layout based on route name
     *
     * @param  MvcEvent $e
     * @return void
     */
    public function selectLayoutBasedOnRoute(MvcEvent $e)
    {

        $matches    = $e->getRouteMatch();
        $aConfig = $e->getApplication()
                     ->getServiceManager()
                     ->get('config');
        $sMatch = $matches->getMatchedRouteName();
        $sLayoutKey = (isset($aConfig['viewtemplate'][$sMatch])) ? $sMatch : '__defaultlayout';
        $targetLayout = $aConfig['viewtemplate'][$sLayoutKey];
        if(!isset($aConfig['viewtemplate'][$sMatch])){
            foreach($aConfig['preg_viewtemplate'] as $rule=>$tmpLayout){
                preg_match($rule,$sMatch,$result); 
                if($result){
                    $targetLayout = $tmpLayout;
                    break;
                }
            }
        }

        $app    = $e->getParam('application');
        $sm     = $app->getServiceManager();
        $config = $sm->get('config');

        $match      = $e->getRouteMatch();
        $controller = $e->getTarget();
        if (!$match instanceof \Zend\Mvc\Router\RouteMatch
            || $controller->getEvent()->getResult()->terminate()
        ) {
            return;
        }
        $controller->layout($targetLayout);
    }


    public function getConfig()
    {
        $aConfig = require __DIR__ . '/config/module.config.php';
        $sViewDir = defined('DEFAULT_TEMPLATE_VIEWDIR') ? DEFAULT_TEMPLATE_VIEWDIR: 'view';
//        $sViewDir = 'view';
        $aConfig['view_manager'] = array(
            'display_not_found_reason' => true,
            'display_exceptions'       => true,
            'doctype'                  => 'HTML5',
            'not_found_template'       => 'error/404',
            'exception_template'       => 'layout/error',
            'template_map' => array(
                'layout/layout' => __DIR__ . '/'.$sViewDir.'/layout/layout.phtml',
                'layout/api'           => __DIR__ . '/'.$sViewDir.'/layout/api.phtml',
                'layout/error'           => __DIR__ . '/'.$sViewDir.'/layout/error.phtml',
                'layout/blank'           => __DIR__ . '/'.$sViewDir.'/layout/blank.phtml',
                 'debug/layout'           => __DIR__ . '/'.$sViewDir.'/layout/blank.phtml',
                'error/404'               => __DIR__ . '/'.$sViewDir.'/layout/404.phtml',
                'error/403'               => __DIR__ . '/'.$sViewDir.'/layout/403.phtml',
                'layout/admin'           => __DIR__ . '/'.$sViewDir.'/layout/admin.phtml',
                
                'admin/user/login'           => __DIR__ . '/'.$sViewDir.'/layout/adminlogin.phtml',
                'admin/login/layout'           => __DIR__ . '/'.$sViewDir.'/layout/adminlogin.phtml',

                //第三方module的temp
                'zfc-admin/admin/index'  => __DIR__ . '/'.$sViewDir.'/admin/index.phtml',
                'zfc-user/user/login'  => __DIR__ . '/'.$sViewDir.'/application/admin/login.phtml',

                'application/admincontent/edit'  => __DIR__ . '/'.$sViewDir.'/application/admin-content/contentedit.phtml',

            ),
            'template_path_stack' => array(
//                __DIR__ . '/view',
                __DIR__ .'/'.$sViewDir,
            ),
        );
        
//        var_dump($aConfig);
        return $aConfig;
    }

    /**
     * 配置Server
     * @return multitype:multitype:NULL  |\Publish\Form\Login
     */
    public function getServiceConfig()
    {
        $aReturn = array(
            'invokables' => array(
                'Application\Service\Record' => 'Application\Service\Record',
                'Application\Service\Menu' => 'Application\Service\Menu',
                'Application\Service\User' => 'Application\Service\User',
                'Application\Service\Common' => 'Application\Service\Common',
                'lf_admincontent' => 'Application\Service\AdminContent',
            )
        );

        if (defined('APPLICATION_MODEL') && strtolower(APPLICATION_MODEL) !== "console") {
            $aReturn['factories'] = array(
                'MenuUserAdmin' => function($sm) {
                    $oAuthService = ($sm->get('zfcuser_auth_service'));
                    $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($sm, $oAuthService->getIdentity());
                  
                    $o = $sm->get('Application\Service\Menu');
                    // $o->setName('user-admin');
                    $sTmp = $aRoles[0] == "superadmin" ? 'user-admin' : $aRoles[0];
                    $o->setName($sTmp);
                    $o->setUserId($oAuthService->getIdentity());
                    return $o->getNavigation();
                },
            );
        }
        

        return $aReturn;
    }

    public function getAutoloaderConfig()
    {
        return array(
//            'Zend\Loader\ClassMapAutoloader' => array(
//                array(
//                    'YcheukfReport\\Module' => __DIR__ . '/../ycheukf/Report/Module.php',
//                ),
//            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,

                    //加载未进入版本库的第三方类库
                    "YcheukTwbBundle" => __DIR__ . '/../ycheukf/TwbBundle/src/TwbBundle',

                    "YcheukfReportExt" => __DIR__ . '/src/' . __NAMESPACE__.'/YcheukfReportExt',
                    "YcheukfCommon" => __DIR__ . '/../ycheukf/Common/src/Common',
                    "YcheukfCommon\Lib" => __DIR__ . '/../ycheukf/Common/src/Common/Lib',
                    "YcheukfCommon\Helper" => __DIR__ . '/../ycheukf/Common/src/Common/Helper',

                ),

            ),
        );
    }




}
