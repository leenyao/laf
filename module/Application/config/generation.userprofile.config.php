<?php
/**
 * 个人设置
*/
$a = array(

    'name' => 'userprofile',
    'label' => '个人中心',
    'route' => 'zfcadmin/userprofile',
    'params' => array('action' => 'index'),
   'pages' => array(
        array(//摘要
            'visible' => true,
            'label' => 'dashbord',
            'route' => 'zfcadmin/userprofile',
            'params' => array('action' => 'index'),
            
        ),
        array(//改密码
            'visible' => true,
            'label' => 'update_password',
            'route' => 'zfcadmin/userprofile',
            'params' => array('action' => 'updpassword'),

        ),
        array(//改密码
            'visible' => true,
            'label' => 'update_userprofilebase',
            'route' => 'zfcadmin/userprofile',
            'params' => array('action' => 'updbase'),

        ),
    ),
);

return $a;