<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
defined('LAF_MD5KV_DATABACKUP_201') || define('LAF_MD5KV_DATABACKUP_201', 201);

$a = array(

    'generation_userprofile' => require(__DIR__."/generation.userprofile.config.php"),

    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'translator' => array(
        'locale' => 'zh_CN',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\AdminReportController' => 'Application\Controller\AdminReportController',
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Debug' => 'Application\Controller\DebugController',
            'Application\Controller\Userprofile' => 'Application\Controller\UserprofileController',
            'Application\Controller\Wechat' => 'Application\Controller\WechatController',
            'Application\Controller\AdminContentController' => 'Application\Controller\AdminContentController',
            'Application\Controller\DatasController' => 'Application\Controller\DatasController',
            'Application\Controller\Api' => 'YcheukfCommon\Lib\Controller\ApiController',
            'Application\Controller\ImgController' => 'Application\Controller\ImgController',
            'YcheukfCommon\Lib\Controller\OauthController' => 'YcheukfCommon\Lib\Controller\OauthController',
        ),
    ),
);
if (defined('APPLICATION_MODEL') && strtolower(APPLICATION_MODEL) !== "console"){
    $a['router'] = array(
        'routes' => array(
            'lurdebug' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                // 'type'    => 'Segment',
                'options' => array(
                    'route'    => '/lurdebug',
                    'defaults' => array(
                        'controller'    => 'Application\Controller\Debug',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'defaults' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action]',
                            'constraints' => array(
                                'controller' => 'Application\Controller\Debug',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                        ),
                    ),
                ),
            ),
            'error403' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/error/403',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'error403',
                    ),
                ),
            ),

            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),

            'forgetpwd' => array(
                'type' => 'Segment',        
                'options' => array(
                    'route'    => '/forgetpwd',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'forgetpwd',
                    ),
                ),
            ),
            'moduleconfig' => array(
                'type' => 'Segment',        
                'options' => array(
                    'route'    => '/moduleconfig',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'moduleconfig',
                    ),
                ),
            ),

            'wechat' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/wechat',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Application\Controller\Wechat',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'login' => array(
                        'type' => 'Segment',        
                        'options' => array(
                            'route'    => '/login',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'Application\Controller\Wechat',
                                'action'     => 'wxlogin',
                            ),
                        ),
                    ),
                    'token' => array(
                        'type' => 'Segment',        
                        'options' => array(
                            'route'    => '/token',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'Application\Controller\Wechat',
                                'action'     => 'wxusertoken',
                            ),
                        ),
                    ),
                    'api' => array(
                        'type' => 'Segment',        
                        'options' => array(
                            'route'    => '/api',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'Application\Controller\Wechat',
                                'action'     => 'weixinapi',
                            ),
                        ),
                    ),
                    'payconfig' => array(
                        'type' => 'Segment',        
                        'options' => array(
                            'route'    => '/payconfig',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'Application\Controller\Wechat',
                                'action'     => 'payconfig',
                            ),
                        ),
                    ),
                    'paynotify' => array(
                        'type' => 'Segment',        
                        'options' => array(
                            'route'    => '/paynotify',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'Application\Controller\Wechat',
                                'action'     => 'paynotify',
                            ),
                        ),
                    ),
                ),
            ),
            'wxlogin' => array(
                'type' => 'Segment',        
                'options' => array(
                    'route'    => '/wxlogin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Application\Controller\Wechat',
                        'action'     => 'wxlogin',
                    ),
                ),
            ),
            'wxusertoken' => array(
                'type' => 'Segment',        
                'options' => array(
                    'route'    => '/wxusertoken',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Application\Controller\Wechat',
                        'action'     => 'wxusertoken',
                    ),
                ),
            ),
            'weixinapi' => array(
                'type' => 'Segment',        
                'options' => array(
                    'route'    => '/weixinapi',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Application\Controller\Wechat',
                        'action'     => 'weixinapi',
                    ),
                ),
            ),
            'datas' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/datas',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'DatasController',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'id2label' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/id2label/:source/[:q]',
                             'constraints' => array(
                                 'q' => '.*',
                             ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'DatasController',
                                'action'        => 'id2label',
                            ),
                        ),
                    ),
                    'alldata' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/alldata/:source',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'DatasController',
                                'action'        => 'alldata',
                            ),
                        ),
                    ),
                    'getlabel' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/getlabel/:source',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'DatasController',
                                'action'        => 'getlabel',
                            ),
                        ),
                    ),
                ),
            ),
            'storagedocs' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/storagedocs',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Application\Controller\Index',
                        'action'        => 'storagedocs',
                    ),
                ),
            ),
            'apidocs' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/apidocs',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Application\Controller\Index',
                        'action'        => 'apidocs',
                    ),
                ),
            ),
            'api' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Api',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'common' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/common/[:resource]/[:op]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Api',
                                'action'        => 'common',
                            ),
                        ),
                    ),
                    'phpsession' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/phpsession',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Api',
                                'action'        => 'phpsession',
                            ),
                        ),
                    ),
                    'procedures' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/procedures/[:op]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Api',
                                'action'        => 'procedures',
                            ),
                        ),
                    ),
                    'imgupload' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/imgupload',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Api',
                                'action'        => 'imgupload',
                            ),
                        ),
                    ),
                    'imagecropping' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => '/imagecropping',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Api',
                                'action'        => 'imagecropping',
                            ),
                        ),
                    ),
                ),
            ),
            'dashboard' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/dashboard',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'dashboard',
                    ),
                ),
            ),
            'test' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/test',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'test',
                    ),
                ),
            ),
            'zfcadmin' => array(
                'may_terminate' => true,
                'child_routes' => array(
                    'userprofile' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/userprofile[/:action]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\Userprofile',
                                // 'action'     => 'index',
                            ),
                        ),
                    ),
                    'contentedit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit[/:type][/:cid][/:id][/:step]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'contentedit',
                            ),
                        ),
                    ),
                    'contentview' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/view[/:type][/:cid][/:id]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'contentview',
                            ),
                        ),
                    ),
                    'contentlist' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/list[/:type][/:cid][/:fid]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'list',
                            ),
                        ),
                    ),
                    'contentchildlist' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/childlist[/:type][/:cid][/:fid]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'list',
                            ),
                        ),
                    ),
                    'ajaxlist' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/ajaxlist[/:type][/:cid][/:fid]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'ajaxlist',
                            ),
                        ),
                    ),
                    'ajaxlistcustomerfield' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/ajaxlistcustomerfield[/:type][/:cid][/:fid]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'ajaxlistcustomerfield',
                            ),
                        ),
                    ),
                    
                    'contentadd' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/content/add[/:type][/:cid][/:id]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'contentedit',
                            ),
                        ),
                    ),
                    'chgbandeduser' => array(//切换绑定的用户
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/banduser/chg[/:userid]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'chgbandeduser',
                            ),
                        ),
                    ),
                    'ajaxchgbandeduser' => array(//切换绑定的用户
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/ajaxchgbandeduser',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'ajaxchgbandeduser',
                            ),
                        ),
                    ),
                    'cancelbandeduser' => array(//取消绑定的用户
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/banduser/cancel[/:userid]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'cancelbandeduser',
                            ),
                        ),
                    ),
                    'contentsetstatus' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/content/setstatus[/:type][/:status][/:fieldname]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'setstatus',
                            ),
                        ),
                    ),
                    'imagecropping' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/content/imagecropping[/:id]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'imagecropping',
                            ),
                        ),
                    ),
                    'clearcache' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/clearcache[/:cache_key]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'clearcache',
                            ),
                        ),
                    ),
                    'clearallcache' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/clearallcache',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminContentController',
                                'action'     => 'clearallcache',
                            ),
                        ),
                    ),
                    'report' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/report[/:type]',
                            'defaults' => array(
                                'controller' => 'Application\Controller\AdminReportController',
                                'action'     => 'index',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'ajaxlist' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/ajaxlist[/:type]',
                                    'defaults' => array(
                                        'controller' => 'Application\Controller\AdminReportController',
                                        'action'     => 'ajaxlist',
                                    ),
                                ),
                            ),
                            'ajaxchildlist' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/ajaxchildlist[/:date]',
                                    'defaults' => array(
                                        'controller' => 'Application\Controller\AdminReportController',
                                        'action'     => 'ajaxchildlist',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    );
}
return $a;