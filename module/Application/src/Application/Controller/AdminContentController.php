<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\InputFilter\InputFilter;
use Application\Form\FormInputFilter;
use Application\Model\Common;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AdminContentController extends \ZfcAdmin\Controller\AdminController
{
    public function reportAction(){
        echo 'aa';die();
    }
    public function translate($s){
        return $this->getServiceLocator()->get('translator')->translate($s);
    }

    public function indexAction()
    {
        $this->redirect()->toRoute("zfcadmin/dashboard");
//        return new ViewModel();
    }
    public function staticreportAction()
    {
        $sId = $this->getEvent()->getRouteMatch()->getParam('id', null);
        $oFileCache = \YcheukfCommon\Lib\Functions::getCacheObject($this->getServiceLocator(), 'filesystem');
        $sOutput = $oFileCache->getItem($sId);
        if(is_null($sOutput)){
            echo "load cache fail!";
        }else{
            $aJson = json_decode($sOutput, 1);
            echo $aJson['data'];
        }
        exit;
//        $this->redirect()->toRoute("zfcadmin/dashboard");
//        return new ViewModel();
    }
    public function cancelbandeduserAction(){
        $bLogined = $this->zfcUserAuthentication()->hasIdentity();
        if(!$bLogined){
            return $this->redirect()->toRoute("zfcuser/login");
        }
        $sErrorMsg = "";
        $oRequest = $this->getRequest();
        $sUserId = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();
        $aData = \Application\Model\Common::getBandedUser($this->getServiceLocator(), $sUserId);
        $nChgUserid = $this->getEvent()->getRouteMatch()->getParam('userid', null);
        if(count($aData['dataset'])){
            foreach($aData['dataset'] as $row){
                if($row['bander_user_id'] == $nChgUserid){
                    $aParams = array(
                        'op'=> 'delete',
                        'resource'=> 'userbander',
                        'resource_type'=> 'common',
                        'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                        'params' => array(
                            'where' => array('user_id'=>$sUserId, 'bander_user_id'=>$nChgUserid),
                        ),
                    );
                    $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
                }
            }
        }
        
//        return $this->redirect()->toRoute("zfcadmin/chgbandeduser");
        return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);

    }
    
    public function ajaxchgbandeduserAction(){
        $bLogined = $this->zfcUserAuthentication()->hasIdentity();
        if(!$bLogined){
            return $this->redirect()->toRoute("zfcuser/login");
        }
        $sUserId = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();

//        $sUserId = 1;
        $status = 1;
        $sErrorMsg = "";
        $oRequest = $this->getRequest();
        if($oRequest->isPost()){//绑定新用户
            $aPost = $oRequest->getPost();
            if(empty($aPost['identity']) || empty($aPost['credential'])){
                $sErrorMsg = '参数错误';
                $status = 0;
            }else{
                $aParams = array(
                    'op'=> 'get',
                    'resource'=> 'oauthuser',
                    'resource_type'=> 'common',
                    'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                    'params' => array(
                        'select' => array (
                            "where" => array('username'=>$aPost['identity'], 'password'=>md5($aPost['credential'])),
                        ),
                    ),
                );
                $aResult = \Application\Model\Common::getResourceList2($this->getServiceLocator(), $aParams);
                if(count($aResult['dataset'])){
                    foreach($aResult['dataset'] as $row){
                        if($row['id'] == $sUserId)continue;
                        $aParams = array(
                            'op'=> 'delete',
                            'resource'=> 'userbander',
                            'resource_type'=> 'common',
                            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                            'params' => array(
                                'where' => array('user_id'=>$sUserId, 'bander_user_id'=>$row['id']),
                            ),
                        );
                        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
                        $aParams = array(
                            'op'=> 'save',
                            'resource'=> 'userbander',
                            'resource_type'=> 'common',
                            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                            'params' => array('dataset'=>array(array('user_id'=>$sUserId, 'bander_user_id'=>$row['id']))),
                        );
                        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
                    }
                }else{
                    $sErrorMsg = "账号或者密码错误";
                    $status = 0;
                }
            }
        }
        $result = array('status'=>$status,'data'=>array(),'msg'=>$sErrorMsg);
        echo json_encode($result);
        return $this->response;
    }
    
    public function chgbandeduserAction(){
        $bLogined = $this->zfcUserAuthentication()->hasIdentity();
        if(!$bLogined){
            return $this->redirect()->toRoute("zfcuser/login");
        }
        $sUserId = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();

//        $sUserId = 1;
        $sErrorMsg = "";
        $oRequest = $this->getRequest();
        if($oRequest->isPost()){//绑定新用户
            $aPost = $oRequest->getPost();
            if(empty($aPost['identity']) || empty($aPost['credential'])){
                $sErrorMsg = '参数错误';
            }else{
                $aParams = array(
                    'op'=> 'get',
                    'resource'=> 'oauthuser',
                    'resource_type'=> 'common',
                    'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                    'params' => array(
                        'select' => array (
                            "where" => array('username'=>$aPost['identity'], 'password'=>md5($aPost['credential'])),
                        ),
                    ),
                );
                $aResult = \Application\Model\Common::getResourceList2($this->getServiceLocator(), $aParams);
                if(count($aResult['dataset'])){
                    foreach($aResult['dataset'] as $row){
                        if($row['id'] == $sUserId)continue;
                        $aParams = array(
                            'op'=> 'delete',
                            'resource'=> 'userbander',
                            'resource_type'=> 'common',
                            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                            'params' => array(
                                'where' => array('user_id'=>$sUserId, 'bander_user_id'=>$row['id']),
                            ),
                        );
                        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
                        $aParams = array(
                            'op'=> 'save',
                            'resource'=> 'userbander',
                            'resource_type'=> 'common',
                            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                            'params' => array('dataset'=>array(array('user_id'=>$sUserId, 'bander_user_id'=>$row['id']))),
                        );
                        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
                    }
                }else{
                    $sErrorMsg = "账号或者密码错误";
                }
            }
        }
        

        $nChgUserid = $this->getEvent()->getRouteMatch()->getParam('userid', null);
        $sMasterUserId = $this->getServiceLocator()->get('Zend\Session\SessionManager')->getStorage()->userBandMasterId;
//        var_dump($this->getServiceLocator()->get('Zend\Session\SessionManager')->getStorage()->userBandMasterId);
        $sCacheMd5Id = '20002_'.(is_null($sMasterUserId) ? $sUserId : $sMasterUserId);
        \YcheukfCommon\Lib\Functions::delCache($this->getServiceLocator(), $sCacheMd5Id);
        if($sMasterUserId == $sUserId){
            $sMasterUserId = $this->getServiceLocator()->get('Zend\Session\SessionManager')->getStorage()->userBandMasterId = null;
        }
//        var_dump($sMasterUserId);
        $aData = \Application\Model\Common::getBandedUser($this->getServiceLocator(), $sUserId);


        if(!is_null($nChgUserid)){
            if(count($aData['dataset'])){
                foreach($aData['dataset'] as $row){
                    if($row['bander_user_id'] == $nChgUserid){
                        if($sMasterUserId == null){
                            $this->getServiceLocator()->get('Zend\Session\SessionManager')->getStorage()->userBandMasterId = $sUserId;
                        }else{
                        }

                        //清除登陆标识, 登陆
                        $sRedirect = isset($_GET['redirect']) ? $_GET['redirect'] : $this->url('/');
                        return \Application\Model\Common::switchLogin($this, $sRedirect, $row['username'], $row['password']);
                    }
                }
            }
        }
        $aViewParams = array(
            'userdata' => $aData['dataset'],
            'sMasterUserId' => $sMasterUserId,
            'sErrorMsg'=> $sErrorMsg, 
            'from' => (isset($_GET['from'])&&$_GET['from']='background') ? 'background': '',
        );
        return new ViewModel($aViewParams);
    }
     public function confirmmsgAction()
   {
        $oRequest = $this->getRequest();
        $aPost = $oRequest->getPost();
        $aForm = json_decode($aPost['params'], 1);
        $sSource = $this->getEvent()->getRouteMatch()->getParam('type', null);
        $sReturn = "";
        echo \YcheukfCommon\Lib\Functions::fmtApiReturn($sReturn);
        return $this->response;
    }
    public function sendmsgAction()
    {
        try{
            $oRequest = $this->getRequest();
            $sSource = "viewcompaignuser";
            if($oRequest->isPost()){//submit
                $aPost = $oRequest->getPost();
                $aForm = json_decode($aPost['params'], 1);
                $sIdName = $sSource.'___id';
                $sUserId = ($this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId());
//                var_dump($sIdName);
                if(!empty($aForm[$sIdName])){
                    $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator());    
                    foreach($aForm[$sIdName] as $sId){
                    
                        $sTitle = "活动-用户通知-个人消息";
                        $sContent = $aForm['msg'];
                        \Application\Model\Common::sendMsg2UserByMQ($this->getServiceLocator(), $sId, $sTitle, $sContent);
                    }
                    echo \YcheukfCommon\Lib\Functions::fmtApiReturn(1);
                    return $this->response;
                }else{
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2036);
                    return $this->response;
                }
            }
        }catch(\YcheukfCommon\Lib\Exception $e){
            echo $e->getMessage();
            return $this->response;
        }
    }


    public function updatesortAction(){
        $nId = $this->getEvent()->getRouteMatch()->getParam('id', null);
        $nIndex = $this->getEvent()->getRouteMatch()->getParam('index', null);
        $sSource = $this->getEvent()->getRouteMatch()->getParam('type', null);
        if(is_null($nId) || is_null($nIndex) || is_null($sSource) ){
            echo \YcheukfCommon\Lib\Functions::fmtApiReturn("error");
            return $this->response;

        }
        $aData = is_null($nId) ? array() : \Application\Model\Common::getResourceById($this->getServiceLocator(), $sSource, $nId);
        $nIndex = ($aData['sort']+$nIndex)<0 ? 0 : ($aData['sort']+$nIndex);
        $aParams = array(
            'op'=> 'save',
            'resource'=> $sSource,
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
            'params' => array('dataset'=>array(array('sort'=> $nIndex)), 'where'=>array('id'=>$nId)),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
        echo \YcheukfCommon\Lib\Functions::fmtApiReturn("OK");
        return $this->response;
    }
    public function contentviewAction(){
        $sCurrentTime = date("Y-m-d H:i:s");
        $oRequest = $this->getRequest();
        $nId = $this->getEvent()->getRouteMatch()->getParam('id', null);
        $nCid = $this->getEvent()->getRouteMatch()->getParam('cid', 0);
        $sSource = $this->getEvent()->getRouteMatch()->getParam('type', null);

        $nId = \YcheukfCommon\Lib\Functions::decodeLafResouceId($nId, $sSource);

        $aData = is_null($nId) ? array() : \Application\Model\Common::getResourceById($this->getServiceLocator(), $sSource, $nId);
        switch($sSource){
            case 'resume':
                $aSource = array('resume_education', 'resume_jobhistory', 'resume_language', 'resume_projecthistory');
                foreach($aSource as $s){
                    $aParams = array(
                        'op'=> 'get',
                        'resource'=> $s,
                        'resource_type'=> 'common',
                        'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                        'params' => array(
                            'select' => array(
                                'where' => array('resume_id'=>$nId),
                            ),
                        ),
                    );
                    $aTmp = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
                    $aData[$s] = $aTmp['data']['dataset'];
                }
                break;
            case 'post':

                $aParams = array(
                    'op'=> 'get',
                    'resource'=> $sSource,
                    'resource_type'=> 'common',
                    'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                    'params' => array(
                        'select' => array(
                            'where' => array('fid'=>$nId),
                        ),
                    ),
                );
                $aTmp = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
                if($aTmp['data']['count'] > 0)
                    $aData['childrecord'] = $aTmp['data']['dataset'];

                break;
        }
        if (isset($aData['resid'])) {
            $aData['encryptResid'] = \YcheukfCommon\Lib\Functions::encryptBySlat($aData['resid']);
        }
        $aData['encryptId'] = \YcheukfCommon\Lib\Functions::encryptBySlat($aData['id']);


        $aViewParams = array(
            'data' => $aData,
            'nId' => $nId,
            'breadcrums' => array(
                array(
                    'link' => $this->url()->fromRoute('zfcadmin/contentlist', array('type'=>$sSource, 'cid'=>$nCid)),
                    'label' => "menu_".$sSource.(empty($nCid)?"":$nCid),
                ),
                array(
                    'label' => is_null($nId) ? 'not found' : 'view',
                ),
            ),
        );
        unset($aViewParams['data']['id']);
        return new ViewModel($aViewParams);
    }

    //检查资源, 若是视图则不允许存储
    public function checkSourceSaveAble($sSource){
        $bReturn = true;
        $aConfig = ($this->getServiceLocator()->get('config'));
        if(isset($aConfig['apiservice']['resources'][$sSource]['isview']) and $aConfig['apiservice']['resources'][$sSource]['isview']==1)
            $bReturn = false;
        return $bReturn;
    }
    public function _getMetadataType($sSource){
        preg_match("/\d+/i", $sSource, $aMatch);
        return count($aMatch) ? $aMatch[0] : false;
    }

    // nCallType: web|cli|3rdcall
    public function contenteditAction($nId=null,$nCid=null,$nFid=null,$sSource=null, $aPost=null, $nStep=null, $nCallType="web")
    {
        $sCurrentTime = date("Y-m-d H:i:s");

        $oRequest = $this->getRequest();
        $nId = is_null($nId) ? $this->getEvent()->getRouteMatch()->getParam('id', null) : $nId;
        $nCid = is_null($nCid) ? $this->getEvent()->getRouteMatch()->getParam('cid', 0) : $nCid;
        $nFid = is_null($nFid) ? $this->getEvent()->getRouteMatch()->getParam('fid', 0) : $nFid;
        $nStep = is_null($nStep) ? $oRequest->getQuery('step', 1) : $nStep;
        $sSource = is_null($sSource) ? $this->getEvent()->getRouteMatch()->getParam('type', null) : $sSource;

        if (strtolower(PHP_SAPI) == 'cli') {
            $nCallType = "cli";
        }
// var_dump($nId.'#step='.$nStep);

        $nId = \YcheukfCommon\Lib\Functions::decodeLafResouceId($nId, $sSource);

        if (!empty($nId) && !is_null($nId)) {
            $aData = \Application\Model\Common::getResourceById($this->getServiceLocator(), $sSource, $nId);
            if (count($aData) < 1) {
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2089);
            }
        }

        $bAddFlag = false;
        $sm = $this->getServiceLocator();
        $this->validPermissions($sSource);
        if(!is_null($aPost) || $oRequest->isPost()){//提交


            if ($nCallType=="web" && $this->url()->fromRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName()) === "/admin/content/add") {
                $bAddFlag = true;
            }

            $oConnection = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection();

            $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->query('set global transaction isolation level read uncommitted')->execute();
            $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->query('set session transaction isolation level read uncommitted')->execute();

            $oConnection->beginTransaction();
            try{

                $sResponseStatus = $oRequest->getQuery('status', 5000);
                if (is_null($aPost)) {
                    header("Content-Type: application/json; charset=utf-8;");
                } 
                
                $this->_filters4Agent($sm, $sSource, array($nId));

                $aPost = is_null($aPost) ? $oRequest->getPost() : $aPost;
                $aForm = json_decode($aPost['params'], 1);

                 $sFormName = \Application\Model\Common::getFormBySource($this->getServiceLocator(), $sSource);
                 $form = new $sFormName($this, $nStep, $nId, $sSource, $nCid, $nFid);
                 $form->setActionSource($sSource);
                 $form->setActionId($nId);
                 $form->setActionCid($nCid);
                 $form->setActionFid($nFid);
                 $form->setActionStep($nStep);


                 if (method_exists($form,'getformInputFilter')){
                    $form->mybindValues($aForm,$this);

                    foreach ($aForm as $key => $valueForm){
                        if ($valueForm == '[]'){
                                $aForm[$key] = '';
                        }
                        if ($valueForm == '至今'){
                                $aForm[$key] = '2100-01-01';
                        }
                    }

                    $form->setData($aForm);
                    $form->setUseInputFilterDefaults(false);//去掉默认所有过滤
                    $form->setInputFilter($form->getformInputFilter($nId, $aForm));

                    if (!$form->isValid()){
                        //错误信息返回
                        $messages = array();
                        foreach ($form->getMessages() as $fieldname => $message){
                            $option = array(
                                'inputName'    => $fieldname,
                                'msg'        => $sm->get('translator')->translate(current($message)),
                                'index'        => 0,
                            );
                            $messages[] = $option;
                        }
                        // var_dump($messages);

                        throw new \Exception(\YcheukfCommon\Lib\Functions::fmtApiReturn($messages, LAF_APICODE_FAILD_5003, "修改失败"), 1);
                        
                    }

                    if($this->getServiceLocator()->has('lf_admincontent')){
                        $aForm = $this->getServiceLocator()->get('lf_admincontent')->preSave($aForm, $sSource, $nCid, $nId, $nStep);
                    }
                    
                 }
                if (method_exists($form,'valueIsValid')){
                    $msg = $form->valueIsValid($sm,$aForm);
                    if(!empty($msg)){

                        throw new \Exception(\YcheukfCommon\Lib\Functions::fmtApiReturn($msg, LAF_APICODE_FAILD_5003, "修改失败"), 1);
                    }
                }
                $sSource = Common::fmtSourceFormat($sSource);
                $aSourceFields = \YcheukfCommon\Lib\Functions::getResourceColumnsFields($this->getServiceLocator(), $sSource);

                // $aForm = $this->preSave($aForm, $sSource, $nCid, $nId);
                //预处理记录
                if(!method_exists($form,'getformInputFilter')){
                    if($this->getServiceLocator()->has('lf_admincontent')){
                        $aForm = $this->getServiceLocator()->get('lf_admincontent')->preSave($aForm, $sSource, $nCid, $nId, $nStep);
                    }
                }

                //处理主记录
                $bChildRecord = false;
                $aRecord = array('modified'=>$sCurrentTime);
                foreach($aForm as $k=>$v){
                    if(!is_array($v)){
                        if(in_array($k, $aSourceFields)){
                            $aRecord[$k] = $v;
                        }
                    }else
                        $bChildRecord = true;
                }

                // $sSaveId = isset($aRecord['id']) && !empty($aRecord['id']) ? $aRecord['id'] : null;
                $sSaveId = $nId;

                if (isset($aRecord["id"]) && !is_null($nId)) {
                    $aRecord["id"] = $nId;
                }
    //                $aRecord = $this->fmtSaveRecord($this->getServiceLocator(), $aRecord, $sSaveId, $sSource);
                $bSaveAble = $this->checkSourceSaveAble($sSource);

                // 该资源可存储处理 && nStep==1
                // var_dump($nStep);
                
                if($bSaveAble && (is_null($nStep) || $nStep==1 || $nStep==0)){

                    $sSaveId = \YcheukfCommon\Lib\Functions::saveResource($this->getServiceLocator(), $sSource, array($aRecord));

                }
                

                $query = $this->getRequest()->getQuery();
                //后处理记录
                if($this->getServiceLocator()->has('lf_admincontent')){

                    $aForm = $this->getServiceLocator()->get('lf_admincontent')->afterSave($aForm, $sSource, $sSaveId, $nCid, $nFid, $nStep);
                }
                $oConnection->commit();

                //成功后跳转
                
                if($nCallType == 'cli')return $sSaveId;
                if($nCallType == '3rdcall')return \YcheukfCommon\Lib\Functions::fmtApiReturn($sSaveId, 5004);

                if (($query->redirect && !empty($query->redirect))) {
                    $sTmp =  $query->redirect; 
                    $aTmp = json_decode($sTmp, 1);
                    $sRedirectQuery = isset($aTmp['query']) ? "?".$aTmp['query'] : "";
                    if (is_null($aTmp)) {
                        $sRedirect =  $query->redirect; 
                    }else{

                        $sRedirect =  $this->url()->fromRoute($aTmp['route'], array('type'=>$aTmp['type'], 'cid'=>$aTmp['cid'],'id'=>$sSaveId)).$sRedirectQuery;
                    // var_dump($sRedirect);

                    }
                }else{

                    if ($bAddFlag == false) {// 修改强制不跳转
                        echo \YcheukfCommon\Lib\Functions::fmtApiReturn($sSaveId, 5004);
                        return $this->response;

                    }
                    // 提交成功, 不跳转

                    $sRedirect =  $this->url()->fromRoute('zfcadmin/contentedit', array('type'=>$sSource, 'id'=>$sSaveId, 'cid'=>$nCid,'fid'=>$nFid))."?id=".\YcheukfCommon\Lib\Functions::encodeLafResouceId($sSaveId, $sSource); 
                }
                echo \YcheukfCommon\Lib\Functions::fmtApiReturn($sRedirect, $sResponseStatus);
                return $this->response;
            }catch (\Exception $e) {
                $oConnection->rollBack();

                if($nCallType == '3rdcall' ){
                    echo $e->getMessage();
                    return $this->response;
                }elseif ($nCallType == 'cli') {
                    return $e->getMessage();
                }else{
                    $sErrorMessage = $e->getMessage();
                    $aTmp2 = json_decode($sErrorMessage, 1);
                    if (is_array($aTmp2) && isset($aTmp2['status']) && isset($aTmp2['msg'])) {
                        echo $e->getMessage();
                        exit;
                    }else{
                        \YcheukfCommon\Lib\Functions::throwErrorByCode(2091, null, array("[=replacement1=]" => ' '.$e->getMessage()));

                    }
                }
            }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
                $oConnection->rollBack();

                if($nCallType == '3rdcall' ){
                    echo $e->getMessage();
                    return $this->response;
                }elseif ($nCallType == 'cli') {
                    return $e->getMessage();
                }else{
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2033, null, array("[=replacement1=]" => ' '.$e->getMessage()));
                }
            }
        }else{//edit
            $nId = $this->_preEdit($sSource, $nId);
            $sFormName = \Application\Model\Common::getFormBySource($this->getServiceLocator(), $sSource);
            $form = new $sFormName($this, $nStep, $nId, $sSource, $nCid, $nFid);
            $form->setActionSource($sSource);
            $form->setActionId($nId);
            $form->setActionCid($nCid);
            $form->setActionFid($nFid);
            $form->setActionStep($nStep);
            $aData = array();
            $sSource = Common::fmtSourceFormat($sSource);
            $aData = is_null($nId) ? array() : \Application\Model\Common::getResourceById($this->getServiceLocator(), $sSource, $nId);
            if($sSource == 'jobs' && !empty($aData)){
                if($aData['status'] > 1){
                    echo \YcheukfCommon\Lib\Functions::fmtApiReturn($msg, 5004);
                    return $this->response;
                }
                    
                if(!empty($aData['json_param'])){
                    $json = json_decode($aData['json_param'],1);
                    $aData['mailTitle'] = $json['mailTitle'];
                    $aData['mailBody'] = $json['mailBody'];
                }
            }
            if($sSource == 'help' && !empty($aData)){
                if(!empty($aData['picture'])){
                    $pics = explode(',',$aData['picture']);
                    if(!empty($pics[0]))
                        $aData['picture'] = $pics[0];
                    if(!empty($pics[1]))
                        $aData['picture_1'] = $pics[1];
                    if(!empty($pics[2]))
                        $aData['picture_2'] = $pics[2];
                }
            }

            $sActionUrl = $form->getRedirectUrl();
            // var_dump($sActionUrl);
            // exit;
            $form->setAttribute('action', $sActionUrl);
            // $form->setAttribute('action', $this->url()->fromRoute($this->getEvent()->getRouteMatch()->getMatchedRouteName(), array('id'=>$nId, 'cid'=>$nCid, 'type'=>$sSource)));
            $form->mybindValues($aData, $this);

            if(strtolower(PHP_SAPI) == 'cli')return $form->getFormElements();
        
            // var_dump($this->url()->fromRoute());

            try {
                $sLink = $this->url()->fromRoute('zfcadmin/contentlist', array('type'=>$sSource, 'cid'=>$nCid));
            } catch (\Zend\Mvc\Exception\DomainException $e) {
                $sLink = "";
            }
            $aViewParams = array(
                'form' => $form,
                'formTitle' => __FUNCTION__,
                'nId' => $nId,
                'breadcrums' => array(
                    array(
                        'link' => $sLink,
                        'label' => "menu_".$sSource.(empty($nCid)?"":$nCid),
                    ),
                    array(
                        'label' => is_null($nId) ? 'add' : 'edit',
                    ),
                ),
            );
            $oViewModel = new ViewModel($aViewParams);
            $oViewModel->setTemplate("application/admincontent/edit");

            return $oViewModel;
        }
    }


    /**
     * 预处理通过user_id获取id的情况
     * @param unknown $sSource
     */
    public function _preEdit($sSource, $nId){
        $sm = $this->getServiceLocator();

        switch ($sSource){
            case 'usercompanyaccount':
            case 'usercompanycertification':
                $sUserId = ($sm->get('zfcuser_auth_service')->getIdentity()->getId());
                $result = Common::getResourceById($sm, 'company', $sUserId,'user_id');
                $nId = !empty($result['id']) ? $result['id'] : '';
                break;
        }
        return $nId;
    }


//    function fmtSaveRecord($aRecord, $sSaveId, $sSource){
//        switch($sSource){
//            case "system_matadata":
//                if(is_null($sSaveId)){
//                    $nResId = Common::getMetadataMaxResId($sm, $nType)
//                }
//            break;
//        }
//        return $aRecord;
//    }
    /**
     * 权限判断
     * @param string $sSource
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>
     */
    public function validPermissions($sSource){
        $sm = $this->getServiceLocator();
        switch($sSource){
            case 'huntertask':
                //个人悬赏
                $users = Common::_get_api_content($sm, 'user');

                if (!empty($users['data'])){
                    if ($users['data']['phone_verified'] != 1){
                        return $this->redirect()->toRoute('zfcadmin/user_hunter/account');
                    }
                }else {
                    return $this->redirect()->toRoute('zfcadmin/user_hunter/account');
                }
                break;
        }
    }



    public function listAction()
    {
        $sSource = $this->getEvent()->getRouteMatch()->getParam('type', null);
        $nCid = $this->getEvent()->getRouteMatch()->getParam('cid', null);
        $nFid = $this->getEvent()->getRouteMatch()->getParam('fid', null);
        $query = $this->getRequest()->getQuery();
        $ajaxlist_url = $this->url()->fromRoute('zfcadmin/ajaxlist', array('type'=>$sSource, 'cid'=>$nCid, 'fid'=>$nFid))."?".$query->toString();
        $aViewParams = array();
        $aViewParams['detail_toolbar'] = $this->_getDetailToolBar($this->getServiceLocator(), $sSource, $nCid, $nFid);
        $aViewParams['ajaxlist_url'] = $ajaxlist_url;
//        $aViewParams['categories'] = array();
//        $sClassMenu = $this->getSubmenuBySource($sSource);
//        if(class_exists($sClassMenu)){
//            $oMenu = new $sClassMenu($this->getServiceLocator(), $sSource, $nCid);
//            $aViewParams['categories'] = $oMenu->getMenu();
//        var_dump($oMenu->getMenu());
//        }
        return new ViewModel($aViewParams);
    }

    public function _getDetailToolBar($sm, $sSource, $nId, $nFid){
        $aReturn = array();
        $sToolbar = \Application\Model\Common::getToolbarBySource($sm, $sSource);
        if(class_exists($sToolbar)){
            $o = new $sToolbar($this, $sm, $sSource, $nId, $nFid);
            $aReturn = $o->getConfig($this, $sm, $sSource, $nId, $nFid);
            $aReturn = $o->fmtOutputJson($aReturn);
        }
        return $aReturn;

    }

    
    public function ajaxlistcustomerfieldAction(){
        //初始化参数
        $aReportParams = ($this->getRequest()->getPost());
        $aTmp = isset($aReportParams['params']) ? json_decode($aReportParams['params'], 1) : array();
        $sReturnType = isset($aTmp['returnType']) ? $aTmp['returnType'] : 'html';
        $sSource = ($this->params()->fromRoute('type', null));
        $nCid = ($this->params()->fromRoute('cid', null));
        //初始化报表引擎参数
        $sClassInput = \Application\Model\Common::getInputBySource($this->getServiceLocator(), $sSource);
        if(class_exists($sClassInput)){
            $oTmp = new $sClassInput();
            $oTmp->setResource($sSource);
            $oTmp->setCid($nCid);
            $oTmp->setFid($this->params()->fromRoute('fid', null));
            $oTmp->setReturnType($sReturnType);
            $oTmp->setExportChaset("GBK");
            $oTmp->setReportParams($aReportParams);
            $oTmp->setSm($this->getServiceLocator());
            $aInput = $oTmp->getInput();
        }else{
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2007, null, array("[=replacement1=]" => $sClassInput));
        }
        $aTmp3 = array();
        if (isset($aInput['input']['detail']['table'])) {

            
            if (is_array($aInput['input']['detail']['table']) && count($aInput['input']['detail']['table'])) {
                foreach ($aInput['input']['detail']['table'] as $sTable => $aRowTmp1) {
                    if (is_array($aRowTmp1['dimen']) && count($aRowTmp1['dimen'])) {
                        foreach ($aRowTmp1['dimen'] as $aRowTmp2) {
                            $sFieldKey = $aRowTmp2;
                            if (is_array($aRowTmp2)) {
                                $sFieldKey = $aRowTmp2['key'];
                            }

                            if(in_array($sFieldKey, array(
                                $sSource.'__split__m101_id', 
                                $sSource.'__split__m102_id', 
                                $sSource.'___id',
                                'm101_id',
                                'm102_id',
                            ))){
                                continue;
                            }

                            $sChecked = isset($aRowTmp2['group']) && $aRowTmp2['group']==false ? '' : 'checked';
                            $aTmp3[] = '<div class="checkbox">
                                <label>
                                    <input name="customfield" type="checkbox" value="'.$sFieldKey.'" '.$sChecked.'>
                                    '.$this->translate(strtolower($sTable."__split__".$sFieldKey)).'
                                </label>
                            </div>';
                        }
                    }
                    
                }
            }
        }

        $sReturn = \Application\Model\Common::fmtHtmlTable($aTmp3, 4);
        
        echo($sReturn);
        // echo '<input type="text" name="aaa" id="inputAaa" class="form-control" required="required" title=""><input type="text" name="aaa" id="inputAaa" class="form-control" required="required" title="">';
        return $this->response;
    }

    public function _getReportDownloadPath($aInput)
    {
        
        $report_config = \YcheukfReport\Lib\ALYS\ALYSConfig::get();
        return $report_config['cache']['csv']['cacheDir'].$aInput['output']['exportInfo']['fileName'].'.csv';
    }
    public function ajaxlistAction()
    {
        //初始化参数
        $aReportParams = ($this->getRequest()->getPost());
        $aTmp = isset($aReportParams['params']) ? json_decode($aReportParams['params'], 1) : array();
        $sReturnType = isset($aTmp['returnType']) ? $aTmp['returnType'] : 'html';
        $sSource = ($this->params()->fromRoute('type', null));
        $nCid = ($this->params()->fromRoute('cid', null));

        $aCustomfields = array();
        if ($this->getRequest()->isXmlHttpRequest() == false) {
            $aTmp4 = isset($aTmp['customfields']) ? ($aTmp['customfields']) : array();

            
            if (is_array($aTmp4) && count($aTmp4)) {
                foreach ($aTmp4 as $aTmp5) {
                    if ($aTmp5['name'] == 'customfield') {
                        $aCustomfields[] = $aTmp5['value'];
                    }
                }
            }
        }
        // exit;
        
        // var_dump($aReportParams);
        // exit;


        //初始化报表引擎参数
        $sClassInput = \Application\Model\Common::getInputBySource($this->getServiceLocator(), $sSource);
        if(class_exists($sClassInput)){
            $oTmp = new $sClassInput();
            $oTmp->setResource($sSource);
            $oTmp->setCustomfields($aCustomfields);
            $oTmp->setCid($nCid);
            $oTmp->setFid($this->params()->fromRoute('fid', null));
            $oTmp->setReturnType($sReturnType);
            $oTmp->setExportChaset("GBK");
            $oTmp->setReportParams($aReportParams);
            $oTmp->setSm($this->getServiceLocator());
            $aInput = $oTmp->getInputBuilder();
            if (in_array($sReturnType, array('csv_all', 'csv'))) {//下载用文件的形式
                $aInput['output']['exportInfo']['type'] = 'file';
            }
        }else{
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2007, null, array("[=replacement1=]" => $sClassInput));
        }

        //执行报表引擎
//        \YcheukfReport\Lib\ALYS\ALYSConfig::set('oRouteMatch', $this->getEvent()->getRouteMatch());
        $aInput = $this->getServiceLocator()->get('YcheukfReportService')->fmtReportParams($aInput, $aReportParams);
        // var_dump($aInput);
        // echo '<xmp>';
        // print_r($aInput['output']);
        // exit;
        $oReport = $this->getServiceLocator()->get('YcheukfReportService')->loadClass('report.start', $aInput);

        $aRe = $oReport->go();



        $checkboxFlag = 1;

        switch($sReturnType){
            case 'csv':
            case 'csv_all'://下载
                $filename = $this->_getReportDownloadPath($aInput);
                $filename = \YcheukfCommon\Lib\Functions::convertCsv2Xls($filename);
                \YcheukfCommon\Lib\Functions::download($filename);

                break;
            default:
                $aOutput = array(
                    'detail_output' => isset($aRe['detail.output']) ? $aRe['detail.output'] : array(),
                    'checkboxFlag' => $checkboxFlag,
                    'detail_page' => $oReport->getInputPageParams(),
                );

                echo \YcheukfCommon\Lib\Functions::fmtApiReturn($aOutput);
            break;
        }
        return $this->response;

    }
    //删除
    public function deleteAction(){
        try{
            $sSource = $this->getEvent()->getRouteMatch()->getParam('type', null);
            $nStatus = $this->getEvent()->getRouteMatch()->getParam('status', 2);

            $oRequest = $this->getRequest();
            if($oRequest->isPost()){//submit
                $aPost = $oRequest->getPost();
                $aForm = json_decode($aPost['params'], 1);
                $sIdName = $sSource.'___id';
                if(!empty($aForm[$sIdName])){
                    $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator());
                    foreach($aForm[$sIdName] as $sId){
                        $aParams = array(
                            'op'=> 'delete',
                            'resource'=> $sSource,
                            'resource_type'=> 'common',
                            'access_token' => $access_token,
                            'params' => array('where'=>array('id'=>$sId)),
                        );
                        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
                    }
                    echo \YcheukfCommon\Lib\Functions::fmtApiReturn($aResult['data']);
                    return $this->response;
                }else{
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2036);
                    return $this->response;
                }
            }
        }catch(\YcheukfCommon\Lib\Exception $e){
            echo $e->getMessage();
            return $this->response;
        }
    }
    //改变状态
    public function setstatusAction(){
        try{
            $sSource = $oSource = $this->getEvent()->getRouteMatch()->getParam('type', null);
            $nStatus = $this->getEvent()->getRouteMatch()->getParam('status', 2);
            $sFieldname = $this->getEvent()->getRouteMatch()->getParam('fieldname', 'status');

            $oRequest = $this->getRequest();
            if($oRequest->isPost()){//submit
                $aPost = $oRequest->getPost();
//                $aPost = array('params'=>json_encode(array($sSource.'___id'=>array('523eadde832ed0e41b000611', '523eadde832ed0e41b000610'))));
                $aForm = json_decode($aPost['params'], 1);
                $sIdName = 'id';
                if(isset($aForm[$sSource.'___id']))
                    $sIdName = $sSource.'___id';
                if(!empty($aForm[$sIdName])){
                    if(is_string($aForm[$sIdName]))
                        $aForm[$sIdName] = array($aForm[$sIdName]);
                    
                    $sm = $this->getServiceLocator();
                    $this->_filters4Agent($sm, $sSource, $aForm[$sIdName]);//代理须验证
                    
//        var_dump($aForm);
                    $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
                    foreach($aForm[$sIdName] as $sId){
                        $updateStatusFlag = true;
                        $sUserId = ($sm->get('zfcuser_auth_service')->getIdentity()->getId());
                        switch($sSource){
                            case 'investorinfo':
                            case 'finance':
                                //添加约见备注
                                if(isset($aForm['actionlog'])){
                                    $type = array('investorinfo'=>1,'finance'=>2);
                                    $updateStatusFlag = false;
                                    $aParams = array(
                                        'op'=> 'save',
                                        'resource'=> 'investorlog',
                                        'resource_type'=> 'common',
                                        'access_token' => $access_token,
                                        'params' => array('dataset'=>array(array('type'=> $type[$sSource],
                                            'user_id'=> $sUserId,                                                                                'investor_info_id'=> $sId, 
                                                                                'status'=> $nStatus, 
                                                                                'memo'=> $aForm['actionlog']))),
                                    );
                                    $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
                                }
                            break;
                            case 'comment':
                                //添加评论的评论
                                if(isset($aForm['actionlog'])){
                                    $updateStatusFlag = false;
                                    $aFRs = \YcheukfCommon\Lib\Functions::getResourceById($sm, $sSource, $sId);
                                    $aParams = array(
                                        'op'=> 'save',
                                        'resource'=> 'comment',
                                        'resource_type'=> 'common',
                                        'access_token' => $access_token,
                                        'params' => array('dataset'=>array(array('fuid'=> $aFRs['cuid'],
                                                                                'cuid'=> $sUserId, 
                                                                                'postid'=> $aFRs['postid'], 
                                                                                'anonymous'=> $aFRs['anonymous'], 
                                                                                'type' => $aFRs['type'],
                                                                                'content' => $aForm['actionlog']))),
                                    );
                                    $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
                                }
                            break;
                            case 'post':
                                    $sKeyName = \Application\Model\Common::getFieldBySource($sFieldname, $sSource);
                                    $aParams = array(
                                        'op'=> 'save',
                                        'resource'=> $sSource,
                                        'resource_type'=> 'common',
                                        'access_token' => $access_token,
                                        'params' => array('dataset'=>array(array($sKeyName=> $nStatus)), 'where'=>array('fid'=>$sId)),
                                    );
                                    $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
                            break;
                        }
                        if($updateStatusFlag){
                            $sKeyName = \Application\Model\Common::getFieldBySource($sFieldname, $sSource);
                            $aParams = array(
                                'op'=> 'save',
                                'resource'=> $sSource,
                                'resource_type'=> 'common',
                                'access_token' => $access_token,
                                'params' => array('dataset'=>array(array($sKeyName=> $nStatus)), 'where'=>array('id'=>$sId)),
                            );
                            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
                        }
                        

                        if($this->getServiceLocator()->has('lf_admincontent')){
                            $this->getServiceLocator()->get('lf_admincontent')->afterSave($aForm, $sSource, $sId);
                        }
                    }
                    echo \YcheukfCommon\Lib\Functions::fmtApiReturn($aResult['data']);
                    return $this->response;
                }else{
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2036);
                    return $this->response;
                }
            }
        }catch(\YcheukfCommon\Lib\Exception $e){
            echo $e->getMessage();
            return $this->response;
        }
    }

    private function _filters4Agent($sm,$sSource,$sIds){
        $tmp_source = array('task','company','resume','taskaction');
        if(!in_array($sSource, $tmp_source) || ($sSource == 'company' && $sIds == array(null))){
            return false;
        }
        //代理须验证
        $roles = $sm->get('ZfcRbac\Service\Rbac')->getIdentity()->getRoles();
        if($roles[0] == 'user-agent'){
            $user = \Application\Model\Common::_get_api_content($sm, 'user'); 
            if(isset($user['data']['m32_id'])){
                if($sSource == 'company'){
                    $_companyIds = $sIds;
                }else{
                    if($sSource == 'task'){
                        $config = array('source'=>$sSource,'pkfield'=>'id','labelfield'=>'company_id');
                    }elseif($sSource == 'taskaction'){
                        $config = array('source'=>'viewtaskaction','pkfield'=>'id','labelfield'=>'company_id');
                    }
                    $_companyIds = \Application\Model\Common::getResourceList($sm, $config, array('id'=>array('$in'=>$sIds)));
                    $_companyIds = array_unique($_companyIds);
                }
                foreach($_companyIds as $com){
                    if(!in_array($com, $user['data']['m32_id'])){
                        \YcheukfCommon\Lib\Functions::throwErrorByCode(2056);
                    }
                }
            }
        }
    }
    
    public function imagecroppingAction(){
        $cid = $this->getEvent()->getRouteMatch()->getParam('id', null);
        if($cid == 0){
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2030);
            return $this->response;
        }
        $sm = $this->getServiceLocator();
        $aRs = \Application\Model\Common::getResourceById($sm, 'content', $cid);
        $request = $this->getRequest();
	if ($request->isPost()){
            $posts = $request->getPost();

            $dst_w  = isset($posts->dst_w) ? $posts->dst_w : 180;
            $dst_h  = isset($posts->dst_h) ? $posts->dst_h : 160;
            $dst_x  = isset($posts->dst_x) ? $posts->dst_x : 0;
            $dst_y  = isset($posts->dst_y) ? $posts->dst_y : 0;
            $path = isset($posts->path) ? BASE_INDEX_PATH.'/'.$posts->path : '';
            $filename = isset($posts->filename) ? $posts->filename : '';
            if(empty($path) || !file_exists($path.'/'.$filename))
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2029);

            $reImages =  \Application\Model\Common::imagecropping($path,$filename,$dst_x,$dst_y,$dst_w,$dst_h);
            if($reImages){
                $imageSrc = str_replace(BASE_INDEX_PATH.'/', '', $reImages['img120']);
                $request = Common::saveDataById($sm, 'content',array('img1' => $imageSrc),$cid);
                if ($request['status'] == 1){
                    echo \YcheukfCommon\Lib\Functions::fmtApiReturn(1);
                }else{
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2030,'图片保存失败');
                }
            }else{
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2030);
            }
            return $this->response;
        }
        $aViewParams = array(
            'cid' => $cid,
            'img1' => $aRs['img1']
        );
        return new ViewModel($aViewParams);
    }
        
    public function clearallcacheAction()
    {
        \YcheukfCommon\Lib\Functions::flushCache($this->getServiceLocator());
        $this->redirect()->toRoute("zfcadmin/contentlist", array('type' => 'cacheadmin', 'cid' => 0));
    }

    public function clearcacheAction()
    {

        $oRequest = $this->getRequest();
        $sCacheKey = $this->getEvent()->getRouteMatch()->getParam('cache_key', null);
        if($oRequest->isPost()){//submit
            if(!is_null($sCacheKey)){
                \YcheukfCommon\Lib\Functions::delCache($this->getServiceLocator(), $sCacheKey, true);
            }
            $aPost = $oRequest->getPost();
            $aForm = json_decode($aPost['params'], 1);
            if(isset($aForm['cache_key']) && count($aForm['cache_key'])){
                foreach($aForm['cache_key'] as $key){
                    \YcheukfCommon\Lib\Functions::delCache($this->getServiceLocator(), $key);
                }
            }
        }else{
        }
        echo \YcheukfCommon\Lib\Functions::fmtApiReturn("OK");
        return $this->response;
    }

}

