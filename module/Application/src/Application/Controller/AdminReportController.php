<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AdminReportController extends \ZfcAdmin\Controller\AdminController
{
    public function ajaxlistAction()
    {
        $aReportParams = ($this->getRequest()->getPost());
        $sSource = $this->getEvent()->getRouteMatch()->getParam('type', null);

	    switch($sSource){
            case 'vote':
                $sTable = 'vote';
                $aInput = array(
                        'filters' => array( //选传. 默认为空, 过滤条件.
                        ),
                        'input'=>array(
                            'flash'=>array(
                                'type' =>'trend',
                                'typeStyle' => 'line', 
//                                  'dateFeild' => 'publisher_date',
//                                  'orderby' =>'id asc',
                                'table' => array(
                                    $sTable => array(
                                        'dimen' => array(
                                                array(
                                                    'key' => 'modified', //必传. 某个维度的键值, 可直接用表中的字段名
                                                    'group'=>true,
                                                ),
                                        ),
                                        'metric' => array(
                                            'count_id',
                                        ),
                                    ),
                                ),
                            ),
                            'detail'=>array(
                                'type' =>'table',
                                'dateFeild' => 'modified',
                                'orderby' =>'id asc',
                                'table' => array(
                                    $sTable => array(
                                        'dimen' => array(
                                            array(
                                                'key' => 'modified', //必传. 某个维度的键值, 可直接用表中的字段名
                                                'group'=>true,
                                            ),
                                        ),
                                        'metric' => array(
                                            'count_id',
                                        ),
                                    ),
                                ),
                            ),
                        ),
                        'output' => array(
                                'format' => 'html',
                        ),
                    );
                    \YcheukfReport\Lib\ALYS\ALYSConfig::set('dateField', 'modified');
                    $aInput = $this->getServiceLocator()->get('YcheukfReportService')->fmtReportParams($aInput, $aReportParams);
                    $oReport = $this->getServiceLocator()->get('YcheukfReportService')->loadClass('report.start', $aInput);
                    $aReport = $oReport->go();
                    break;
            default:
                $sClassInput = \Application\Model\Common::getInputBySource($this->getServiceLocator(), $sSource);
                if(class_exists($sClassInput)){
                    $oTmp = new $sClassInput();
                    $oTmp->setResource($sSource);
                    $oTmp->setReturnType('html');
                    $oTmp->setReportParams($aReportParams);
                    $oTmp->setSm($this->getServiceLocator());
                    $aInput = $oTmp->getInput();
                }
                $aInput = $this->getServiceLocator()->get('YcheukfReportService')->fmtReportParams($aInput, $aReportParams);
                $oReport = $this->getServiceLocator()->get('YcheukfReportService')->loadClass('report.start', $aInput);
                $aReport = $oReport->go();
            break;

		}

//var_dump($aReport1['flash.output']);
		$aOutput = array('detail_output'=>"", 'detail_page'=>false);
		if(isset($aReport['flash.output']) && !empty($aReport['flash.output']))
			$aOutput['detail_output'] .= $aReport['flash.output'];
		if(isset($aReport['detail.output']) && !empty($aReport['detail.output']))
			$aOutput['detail_output'] .= $aReport['detail.output'];
		if(isset($aInput['input']['detail']))
			$aOutput['detail_page'] = $oReport->getInputPageParams();
		echo \YcheukfCommon\Lib\Functions::fmtApiReturn($aOutput);
        return $this->response;

	}
        
    public function ajaxchildlistAction()
    {
        $aReportParams = ($this->getRequest()->getPost());
        $sSource = $this->getEvent()->getRouteMatch()->getParam('type', null);
        $sDate = $this->getEvent()->getRouteMatch()->getParam('date', null);
        switch ($sSource){
                case 'vote':
                        $sTable = 'vote';
			$aInput = array(
                            'filters' => array(
                                array(
                                    'key' => 'modified', //操作符
                                    'op' => '>=', //操作符=,in,like
                                    'value' => $sDate//过滤的值
                                ),
                                array(
                                    'key' => 'modified', //操作符
                                    'op' => '<', //操作符=,in,like
                                    'value' => date('Y-m-d',  strtotime($sDate)+60*60*24)//过滤的值
                                ),
                            ),
                            'input'=>array(
                                'detail'=>array(
                                    'type' =>'table',
                                    'orderby' =>'id asc',
                                    'table' => array(
                                        $sTable => array(
                                            'dimen' => array(
                                                array(
                                                    'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
                                                    'group'=>true,
                                                 ),
                                                'email',
                                                'keyword',
                                                'modified'
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            'output' => array(
                                    'format' => 'html',
                            ),
                        );
                        $aInput = $this->getServiceLocator()->get('YcheukfReportService')->fmtReportParams($aInput, $aReportParams);
                        $oReport = $this->getServiceLocator()->get('YcheukfReportService')->loadClass('report.start', $aInput);
                        $aReport = $oReport->go();
                        break;
		}


//var_dump($aReport1['flash.output']);
		$aOutput = array('detail_output'=>"", 'detail_page'=>false);
		if(isset($aReport['flash.output']) && !empty($aReport['flash.output']))
			$aOutput['detail_output'] .= $aReport['flash.output'];
		if(isset($aReport['detail.output']) && !empty($aReport['detail.output']))
			$aOutput['detail_output'] .= $aReport['detail.output'];  
		if(isset($aInput['input']['detail']))
			$aOutput['detail_page'] = $oReport->getInputPageParams();
		echo \YcheukfCommon\Lib\Functions::fmtApiReturn($aOutput);
        return $this->response;

	}
        
    public function indexAction()
    {
        $sSource = $this->getEvent()->getRouteMatch()->getParam('type', null);
        $aViewParams = array(
                'ajaxlist_url'=>$this->url()->fromRoute('zfcadmin/report/ajaxlist', array('type'=>$sSource)),
                'detail_toolbar' => $this->_getDetailToolBar($this->getServiceLocator(), $sSource)
        );
        $aViewParams = $this->setPageActive($aViewParams, $sSource);
        $oView = new ViewModel($aViewParams);
        return $oView;

    }   

    public function getListMenu($sSource){
		$aMatch = array('type'=>$sSource);
		$aReturn = array();
		$sLabelPre = 'menu_report_';
//		$aReturn[] = array(
//			'label' => $sLabelPre.'task',
//			'iconCls' => 'licon-report-task',	
//			'route' => 'zfcadmin/report',
//			'params' => array('type'=>'task'),
//		);
//		$aReturn[] = array(
//			'label' => $sLabelPre.'resume',
//			'iconCls' => 'licon-report-resume',	
//			'route' => 'zfcadmin/report',
//			'params' => array('type'=>'resume'),
//		);
//
//		$aReturn[] = array(
//			'label' => $sLabelPre.'company',
//			'iconCls' => 'licon-report-company',	
//			'route' => 'zfcadmin/report',
//			'params' => array('type'=>'company'),
//		);
//
//		foreach($aReturn as $k=>$row){
//			$aReturn[$k]['active'] = $row['params']==$aMatch ? true : false;
//		}

		return $aReturn;
	}

			//设置菜单激活
	public function setPageActive($aViewParams, $sSource){
		$aViewParams['categories'] = $this->getListMenu($sSource);
		// foreach($this->getServiceLocator()->get('navigation')->getPages() as $page){
		// 	if($page->getLabel()=='menu_report')
		// 		$page->setActive();
		// }
		return $aViewParams;
	}

	public function _getDetailToolBar($sm, $sSource){
		$aReturn = array();
        $aReturn[] = array(
            'type' => 'datepicker',
            'position' => 'left',
            'params' => array(
                'defaultDate' => date("Y-m-d", strtotime('-30 day')),
                'name' => 'startDate',
            ),
        );
        $aReturn[] = array(
            'type' => 'datepicker',
            'position' => 'left',
            'params' => array(
                'defaultDate' => date("Y-m-d"),
                'name' => 'endDate',
            ),
        );
		return $aReturn;
	}
}
