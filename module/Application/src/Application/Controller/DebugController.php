<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DebugController extends LafController
{
    public function indexAction()
    {
        $chart = new \Ghunti\HighchartsPHP\Highchart();
        $chart->title = array('text' => 'Monthly Average Temperature', 'x' => -20);
        $chart->series[] = array('name' => 'Tokyo', 'data' => array(7.0, 6.9, 9.5));
        $chart->printScripts();        

        echo '<script>'.$chart->render("chart").'</script>';

        // var_dump(123);
        exit;
    }


    public function excelAction()
    {

        $sFileDirector = (BASE_INDEX_PATH."/tmpupload/exceldown");
        exec("mkdir -p ".$sFileDirector);
        $sFileName = "PHPExcel Test Document";
        $sSheetTitle = "PHPExcel Test Document";
        $sFilePath = $sFileDirector."/".md5($sFileName).".xlsx";

        /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');

        define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        // Create new PHPExcel object
        // echo date('H:i:s') , " Create new PHPExcel object" , EOL;
        $objPHPExcel = new \PHPExcel();

        // Set document properties
        // echo date('H:i:s') , " Set document properties" , EOL;
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     // ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle($sFileName)
                                     ->setSubject($sFileName)
                                     ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                                     ->setKeywords("office PHPExcel php")
                                     ->setCategory("Test result file");


        // Add some data
        // echo date('H:i:s') , " Add some data" , EOL;
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Hello')
                    ->setCellValue('B2', 'world!')
                    ->setCellValue('C1', 'Hello')
                    ->setCellValue('D2', 'world!');

        // Miscellaneous glyphs, UTF-8
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A4', 'Miscellaneous glyphs')
                    ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');

        // Rename worksheet
        // echo date('H:i:s') , " Rename worksheet" , EOL;
        $objPHPExcel->getActiveSheet()->setTitle($sSheetTitle);


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Save Excel 2007 file
        // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
        $callStartTime = microtime(true);

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($sFilePath);
        var_dump($sFilePath);


        exit;
    }
}
