<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends LafController
{
    public function indexAction()
    {
        $this->redirect()->toRoute('zfcuser/login');
    }
    public function moduleconfigAction()
    {

        $aResouceList = $aWhere = array();
        if (isset($_GET['action']) && $_GET['action']=='u') {


            $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_master"); //db_master|db_slave
            $sSql = "update system_moduleconfig set selected=0";
            $oSth = $oPdo->prepare($sSql);
            $oSth->execute(array(
            ));

            $sSql = "update system_moduleconfig set selected=1 where id=:id";
            $oSth = $oPdo->prepare($sSql);
            $oSth->execute(array(
                ':id' => $_GET['id'],
            ));

            $sSelectedFile = BASE_INDEX_PATH."/../data/moduleconfig_selected";
            exec("touch  ".$sSelectedFile);
            exec("echo -e \"".$_GET['id']."\\c\" > ".$sSelectedFile);


            echo "切换成功<hr>";
        }
        $aResouceList['system_moduleconfig'] = \Application\Model\Common::getResourceAllDataKRow($this->serviceManager, "system_moduleconfig", $aWhere);



        return new ViewModel(array(
            "aResouceList" => $aResouceList,
       ));

    }

    
    public function error403Action()
    {
       $oView = new ViewModel(array(
       ));
       $oView->setTemplate("error/403");
       return $oView;
    }
    public function error404Action()
    {
       $oView = new ViewModel(array(
       ));
       $oView->setTemplate("error/404");
       return $oView;
    }


    public function testAction()
    {


        //hash 可混淆ID
        $s = 11;
        var_dump(\YcheukfCommon\Lib\Functions::encodeHex($s));
        var_dump(\YcheukfCommon\Lib\Functions::decodeHex(\YcheukfCommon\Lib\Functions::encodeHex($s)));

        $s = 12;
        var_dump(\YcheukfCommon\Lib\Functions::encodeHex($s));
        var_dump(\YcheukfCommon\Lib\Functions::decodeHex(\YcheukfCommon\Lib\Functions::encodeHex($s)));

        $s = 13;
        var_dump(\YcheukfCommon\Lib\Functions::encodeHex($s));
        var_dump(\YcheukfCommon\Lib\Functions::decodeHex(\YcheukfCommon\Lib\Functions::encodeHex($s)));

        $s = 14;
        var_dump(\YcheukfCommon\Lib\Functions::encodeHex($s));
        var_dump(\YcheukfCommon\Lib\Functions::decodeHex(\YcheukfCommon\Lib\Functions::encodeHex($s)));

        exit;

        // $this->redirect()->toRoute('zfcuser/login');
//        return new ViewModel();
    }

    public function translate($s){
        return $this->getServiceLocator()->get('translator')->translate($s);
    }
    public function apidocsAction(){
//        return $this->getServiceLocator()->get('translator')->translate($s);
        return new ViewModel();
    }
    public function storagedocsAction(){
//        return $this->getServiceLocator()->get('translator')->translate($s);
        return new ViewModel();
    }

    public function dashboardAction()
    {
        $bFlagTmp = $bBreak = false;
        $aRow = array();
        $aConfig = $this->getServiceLocator()->get('config');
        $aRoles = \Application\Model\Common::getUserRoles($this->getServiceLocator(), $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity());
        if(isset($aRoles[0]) && isset($aConfig['dashboardroute_role2action'][$aRoles[0]])){
            return $this->redirect()->toRoute($aConfig['dashboardroute_role2action'][$aRoles[0]][0], (isset($aConfig['dashboardroute_role2action'][$aRoles[0]][1]) ? $aConfig['dashboardroute_role2action'][$aRoles[0]][1] : array()));
        }else{
            $aRoles[0] = 'guest';
            return $this->redirect()->toRoute("zfcuser/logout");
            
            // return $this->redirect()->toRoute($aConfig['dashboardroute_role2action'][$aRoles[0]][0], (isset($aConfig['dashboardroute_role2action'][$aRoles[0]][1]) ? $aConfig['dashboardroute_role2action'][$aRoles[0]][1] : array()));
//            \YcheukfCommon\Lib\Functions::throwErrorByCode(2065, null, array("[=replacement1=]"=>$aRoles[0]));
        }
    }
    /**
     * 密码找回
     * @return \Zend\View\Model\ViewModel
     */
    public function forgetpwdAction(){
        $oRequest = $this->getRequest();
        $message = '';
        if ($oRequest->isPost()){
            $aPost = $oRequest->getPost();
            $user_name = !empty($aPost->user_name) ? $aPost->user_name : '';
            $params = array(
                'user_name' =>     $aPost->user_name
            );
            if (!empty($user_name)){
                if (\Application\Model\Common::checkEmail($user_name)){
                    //输入的是邮箱
                    $params['type'] = 0;
                }elseif (\Application\Model\Common::checkMobilePhone($user_name)){
                    //输入的是手机号
                    $params['type'] = 1;
                }

                $result = \Application\Model\Common::_get_api_content($this->getServiceLocator(), 'forgetpwd',$params);
                if (empty($result['data'])){
                    $message = 'apicode_'.$result['status'];
                }else{
                    $message = '找回信息发送成功';
                }
            }
        }

        $aViewParams = array(
            'message' => $message
        );
        return new ViewModel($aViewParams);
    }
}
