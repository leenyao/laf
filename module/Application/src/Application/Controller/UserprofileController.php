<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class UserprofileController extends \Application\Controller\LafController
{
    public function indexAction()
    {


        if ($this->getServiceLocator()->has(LAF_NAMESPACE."\Service\Common")) {
            $aProfile = $this->getServiceLocator()->get(LAF_NAMESPACE."\Service\Common")->getCurrentProfile();
        }else{
            $aProfile = $this->getServiceLocator()->get("\Application\Service\Common")->getCurrentProfile();

        }





       $oView = new ViewModel(array(
        "aProfile" =>$aProfile,
       ));

       return $oView;
    }


    public function updbaseAction($value='')
    {

        $aUserInfo = $this->getServiceLocator()->get(LAF_NAMESPACE."\Service\Common")->getCurrentUserInfo();

        $aPost = ($this->getRequest()->isPost()) ? $this->getRequest()->getPost() : null;
        $sActionReturn = $this->getServiceLocator()->get(LAF_NAMESPACE."\Service\Common")->callAdminContentEditAction($aUserInfo['id'], "", "", "profilebase", $aPost);

        if (is_string($sActionReturn)) {
          echo $sActionReturn;
          return $this->response;
        }else{
          return $sActionReturn;
        }

    }
    
    public function updpasswordAction($value='')
    {

        $aUserInfo = $this->getServiceLocator()->get(LAF_NAMESPACE."\Service\Common")->getCurrentUserInfo();

        $aPost = ($this->getRequest()->isPost()) ? $this->getRequest()->getPost() : null;
        $sActionReturn = $this->getServiceLocator()->get(LAF_NAMESPACE."\Service\Common")->callAdminContentEditAction($aUserInfo['id'], "", "", "profilepassword", $aPost);

        if (is_string($sActionReturn)) {
          echo $sActionReturn;
          return $this->response;
        }else{
          return $sActionReturn;
        }

    }
}
