<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class WechatController extends AbstractActionController
{

    public function weixinapiAction(){
        $aConfig = $this->getServiceLocator()->get('config');
        $query = $this->getRequest()->getQuery();
        $sType = is_null($query->type) ? $aConfig['weixin']['hcoffee_redirect_url'] : $query->type;

        switch(strtolower($sType)){
            case 'menu':
                $menuService = new \Overtrue\Wechat\Menu($aConfig['weixin']['appid'], $aConfig['weixin']['secret']);
                $menus = array();
                $menuService->delete();
//                var_dump($aConfig['weixin']['custommenu']);
                foreach($aConfig['weixin']['custommenu'] as $k=>$row){
                    switch($row['type']){
                        case 'view':
                            $menus[] = new \Overtrue\Wechat\MenuItem($row['params'][0], $row['params'][1], $row['params'][2]);
                            break;
                        case 'button':
                            $button = new \Overtrue\Wechat\MenuItem($k);
                            $aTmp = array();
                            foreach($row['params'] as $row2){
                                $aTmp[] = new \Overtrue\Wechat\MenuItem($row2[0], $row2[1], $row2[2]);
                            }
                             $button->buttons($aTmp);
                             $menus[] = $button;
                            break;
                        case 'click':
                            $menus[] = new \Overtrue\Wechat\MenuItem($row['params'][0], $row['params'][1], $row['params'][2]);
                            break;
                    }
                }

                try {
                  $menuService->set($menus);// 请求微信服务器
                  echo '设置成功！';
                } catch (\Exception $e) {
                  echo '设置失败：' . $e->getMessage();
                }
                break;
            default:
                $server = new \Overtrue\Wechat\Server($aConfig['weixin']['appid'], $aConfig['weixin']['token'], $aConfig['weixin']['EncodingAESKey']);
//                $server->on('message', function($message) use ($aConfig){
//                    return $aConfig['weixin']['welcometxt'];
//                });
                // 监听指定类型
                $server->on('message', 'text', function($message) use ($aConfig) {
                    $sMsg = (isset($aConfig['weixin']['autoreply'][$message['Content']])) ? $aConfig['weixin']['autoreply'][$message['Content']] : $aConfig['weixin']['autoreply']['DEFAULT'];
                    return \Overtrue\Wechat\Message::make('text')->content($sMsg);
                });

                // 只监听指定类型事件
                $server->on('event', 'click', function($event) use ($aConfig) {
                    $sMsg = (isset($aConfig['weixin']['autoreply'][$event->EventKey])) ? $aConfig['weixin']['autoreply'][$event->EventKey] : $aConfig['weixin']['autoreply']['DEFAULT'];
                    return \Overtrue\Wechat\Message::make('text')->content($sMsg);
                });

                // 只监听指定类型事件
                $server->on('event', 'subscribe', function($event) use ($aConfig) {
                    return \Overtrue\Wechat\Message::make('text')->content($aConfig['weixin']['welcometxt']);
                });


                echo $server->serve();
            break;
        }

        exit;
    }
    public function wxusertokenAction(){
        if (isset($_SESSION['wxusertoken'])) echo "var token = '".$_SESSION['wxusertoken']."';";
        else echo "var token = '';";
        exit;
    }
    public function payconfigAction(){
        if (isset($_SESSION['wxusertoken'])) echo "var token = '".$_SESSION['wxusertoken']."';";
        else echo "var token = '';";
        exit;
    }
    public function wxloginAction(){
        $aConfig = $this->getServiceLocator()->get('config');
        $query = $this->getRequest()->getQuery();

        if(!empty($_SESSION['wxusertoken'])){
            try{
                $bFlag = \YcheukfCommon\Lib\OauthClient::checkAccess($this->getServiceLocator(), $_SESSION['wxusertoken']);
                if(!$bFlag)unset($_SESSION['wxusertoken']);
            }catch(\YcheukfCommon\Lib\Exception $e){
                unset($_SESSION['wxusertoken']);
            }
        }

        $auth = new \Overtrue\Wechat\Auth($aConfig['weixin']['appid'], $aConfig['weixin']['secret']);
        if (empty($_SESSION['wxusertoken'])) {
            //微信登录
            $sRedirectUrl = $aConfig['weixin']['wxlogin_redirect_url'];
            if(!is_null($query->redirect_url)){
                $sRedirectUrl = $sRedirectUrl."?redirect_url=".urlencode($query->redirect_url);
            }
            $user = $auth->authorize($sRedirectUrl); // 返回用户 Bag


            $aParams = array(
                'op'=> 'wxlogin',
                'resource'=> 'user',
                'resource_type'=> 'procedures',
                'access_token' => "",
                'params' => $user->toArray(),
            );
            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
            
            if($aResult['status'] == 1){
                $_SESSION['wxusertoken'] = $aResult['data'];
            }else{
                print_r($aResult);
                exit;
            }
        }
        $redirect_url = is_null($query->redirect_url) ? $aConfig['weixin']['hcoffee_redirect_url'] : $query->redirect_url;
        header('Location:'.$redirect_url.(preg_match("/\?/i", $redirect_url) ? "&":"?")."token=".$_SESSION['wxusertoken']);
        exit;
    }
    public function paynotifyAction(){
        $aConfig = $this->getServiceLocator()->get('config');
        $notify = new \Overtrue\Wechat\Payment\Notify(
            $aConfig['weixin']['appid'],
            $aConfig['weixin']['secret'],
            $aConfig['weixin']['mch_id'],
            $aConfig['weixin']['mch_key']
        );

        $transaction = $notify->verify();
        if (!$transaction) {
            echo $notify->reply('FAIL', 'verify transaction error');
            exit;
        }else{
            $aTrade = $transaction->toArray();
//            $aTrade = array('out_trade_no' => '9b61f71adb114df7644231f4222f52bf', 'return_code'=>'SUCCESS');

//
//          将订单状态设置为 未处理
//
            $aParams = array(
                'op'=> 'save',
                'resource'=> 'hcoffee_order',
                'resource_type'=> 'common',
                'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                'params' => array(
                    'dataset'=>array(array(
                        'm1005_id'=> 2,
                    )),
                    'where' => array('no'=>$aTrade['out_trade_no']),
                ),
            );
            $aResult = \YcheukfCommon\Lib\Functions::myUpdate($this->getServiceLocator(), $aParams, 'function');

            
            if(strtoupper($aTrade['return_code']) == 'SUCCESS'){
                $nM155Id = 8;
            }else{
                $nM155Id = 7;
            }

            $aParams = array(
                'op'=> 'save',
                'resource'=> 'payment',
                'resource_type'=> 'common',
                'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                'params' => array(
                    'dataset' => array (
                        array(
                            'pay_response'=> json_encode($aTrade),
                            'm155_id'=> $nM155Id,
                        ),
                    ),
                    'where' => array('order_no'=>$aTrade['out_trade_no']),
                ),
            );
            $aResult = \YcheukfCommon\Lib\Functions::myUpdate($this->getServiceLocator(), $aParams);

/***********debug用
            $aParams = array(
                'op'=> 'save',
                'resource'=> 'userlog',
                'resource_type'=> 'common',
                'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
                'params' => array('dataset'=>array(array(
                    'user_role'=> "wxpay",
                    'url'=> "",
                    'param'=> json_encode($aTrade),
                ))),
            );
            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceLocator(), $aParams, 'function');
*/
        }

        // var_dump($transaction);

        echo $notify->reply();
        exit;
    }
}