<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Common extends Form
{
	public $aFormElement = array();
	public $sActionSource = null;
	protected $inputFilter;			//过滤器
    protected $aRedirectUrl=array("url"=>"", "query"=>array());			
    public $nEntityId=null;
    public $nEntityCid=null;
    public $nEntityFid=null;
    public $nEntityStep=null;



    function __get($name='')
    {
        switch ($name) {
            case 'serviceManager':
                return $this->getServiceManager();
                break;
            default:
                break;
        }
    }
    

    public function fmtSubmitButton($sSource=null, $cid=0)
    {
        $aReturn = array(
            'type' => 'button',
            'options' => array(
                'helpname'=>'Submitcancel',
            ),

        );

        $aReturn['options']['redirect_goback'] = array(
            "label" => "保存 & 返回列表",
            "urlarray" => array(
                "route"=>"zfcadmin/contentlist", 
                "type"=>$sSource, 
                "cid"=>$cid, 
                "id"=>"=saveid=", 
                "query"=>"",
            ),
        );

        // $aReturn['options']['redirect'] = array(
        //     "label" => "保存 && 继续添加下一个",
        //     "urlarray" => array(
        //         "route"=>"zfcadmin/contentadd", 
        //         "type"=>$sSource, 
        //         "cid"=>0, 
        //         "id"=>"=saveid=", 
        //         "query"=>"",
        //     ),
        // );
        return $aReturn;
    }

    public function setRedirectUrl($sUrl='', $aParams=array())
    {
    	$this->aRedirectUrl = array(
    		"url" => $sUrl,
    		"query" => $aParams,
    	);
    }
    public function getRedirectUrl(){
    	$sReturn = $this->aRedirectUrl['url'];
    	$a = array();
    	if (count($this->aRedirectUrl['query'])) {
    		foreach ($this->aRedirectUrl['query'] as $k => $v) {
    			$a[] = $k."=".$v;
    		}
    	}
    	if (count($a)) {
    		$sReturn .= "?".join("&", $a);
    	}
    	return $sReturn;
    }

	public function _fmtFormElements($sClassName=""){
        if (is_null($this->sActionSource)) {
            $this->sActionSource = lcfirst(substr($sClassName, strrpos($sClassName, "\\")+1));
        }
		$aTmpForm = $this->aFormElement;
		$aReturn = array();



		foreach($aTmpForm as $k=>$aTmp){
            $k = trim($k);
            
			if(!isset($aTmpForm[$k]['options']['label']))
				$aTmpForm[$k]['options']['label'] = preg_match("/__split__/i", $k) ? $k : $this->sActionSource."__split__".$k;
			if(!isset($aTmpForm[$k]['options']['fieldname']))
				$aTmpForm[$k]['options']['fieldname'] = $k;
			if(!isset($aTmpForm[$k]['options']['helpname']))
				$aTmpForm[$k]['options']['helpname'] = $aTmpForm[$k]['type'];
			if(!isset($aTmpForm[$k]['options']['tips'])){
				if(!in_array(strtolower($aTmp['type']), array('hidden')))
					$aTmpForm[$k]['options']['tips'] = $k."_tips";
			}

			if(!isset($aTmpForm[$k]['attributes']['id']) && !isset($aTmpForm[$k]['attributes']['noid']))
				$aTmpForm[$k]['attributes']['id'] = $k.uniqid();

//			if(!isset($aTmpForm[$k]['attributes']['class']))
//				$aTmpForm[$k]['attributes']['class'] = 'col-sm-2 control-label';

			if(!isset($aTmpForm[$k]['attributes']['type']))
				$aTmpForm[$k]['attributes']['type'] = $aTmpForm[$k]['type'];

			if(!isset($aTmpForm[$k]['name']))
				$aTmpForm[$k]['name'] = $k;
			if(!isset($aTmpForm[$k]['type']))
				$aTmpForm[$k]['type'] = 'text';


		}
		$this->aFormElement = $aTmpForm;
	}


	function _fmtData($aData=array(), $oController){
		return $aData;
	}

	public function _addElements(){
		$aReturn = array();
		foreach($this->aFormElement as $k=>$aTmp){
			$oEleTmp = $aTmp;
//            var_dump($oEleTmp);
			$spc = false;    //特殊element状态码
			switch (strtolower($oEleTmp['type'])){
			    case 'norequiredselect':
			        $oEleTmp = new \YcheukTwbBundle\Form\Element\NotRequiredSelect($aTmp['name']);
			        $spc = true;
			        break;
    	        case 'norequiredcheckbox':
    	            $oEleTmp = new \YcheukTwbBundle\Form\Element\NotRequiredCheckbox($aTmp['name']);
    	            $spc = true;
    	            break;
	            case 'norequiredradio':
	                $oEleTmp = new \YcheukTwbBundle\Form\Element\NotRequiredRadio($aTmp['name']);
	                $spc = true;
	                break;
			}
			if ($spc){
    			$oEleTmp->setOptions($aTmp['options']);
    			$oEleTmp->setAttributes($aTmp['attributes']);
			}
            $this->add($oEleTmp);
		}
	}

	function mybindValues($aData=array(), $oController){
		$aData = $this->_fmtData($aData, $oController);
		$this->aFormElement = $this->_fmtElement4Data($aData, $this->aFormElement);
		// var_dump($this->aFormElement);
  //       exit;
		$this->_addElements();
		$this->_fmtElements();
	}

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager($serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
	}

    public function getController()
    {
        return $this->controller;
    }

    public function addMetadataList($aReturn, $nType, $aEnableIds=array())
    {
        $aMetaData = \Application\Model\Common::getResourceMetadaList($this->serviceManager, $nType);

        if (count($aEnableIds)) {
            foreach ($aMetaData as $key => $value) {
                if (!in_array($key, $aEnableIds)) {
                    unset($aMetaData[$key]);
                }
            }
        }
        $aReturn["m".$nType."_id"] = array(
                'type' => 'select',
                'attributes' => array('class'=>'select-tab', "value"=>1),
                'options' => array(
                    'value_options' => $aMetaData,
                ),
        );
        return $aReturn;
    }

    public function setActionSource($sSource)
    {
        $this->sActionSource = $sSource;
    }
    public function setActionId($nId)
    {
        $this->nEntityId = $nId;
    }
    public function setActionCid($nId)
    {
        $this->nEntityCid = $nId;
    }

    public function setActionFid($nId)
    {
        $this->nEntityFid = $nId;
    }
    public function setActionStep($nStep)
    {
        $this->nEntityStep = $nStep;
    }




    public function setController($oController, $sClassName=null)
    {
        $this->controller = $oController;
        $sm = $oController->getServiceLocator();
        $this->setServiceManager($sm);
        // decodeLafResouceId
        return $this;
	}

//	public function _fmtElement4Data($aData=array(), $aFormElement){
//		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
//		if(isset($aFormElement['category_id']['options']['value_options'])){
//			if(count($aFormElement['category_id']['options']['value_options']) < 2){
//				$sId = key($aFormElement['category_id']['options']['value_options']);
//				$aFormElement['category_id'] = array(
//					'type' => 'hidden',
//					'name' => 'category_id',
//					'attributes' => array('type'=> 'hidden', 'value'=>$sId),
//					'options' => array('helpname'=> 'hidden', ),
//				);
//			}
//		}
//		return $aFormElement;
//	}



	public function _fmtElement4Data($aData=array(), $aFormElement){
		$sId = isset($aData['id']) ? $aData['id'] : null;
		foreach($aFormElement as $sCode=>$aTmp){
            $sCode = trim($sCode);
			//绑定第三方关系metadata

			if(($nRelationType = \Application\Model\Common::getMatchsRelationType($sCode)) && !is_null($sId)){
				//获取关系数组
				$aTmp2 = \Application\Model\Common::getResourceRelationList($this->getServiceManager(), $nRelationType, $sId);
				if(isset($aTmp['options']['table']) && $aTmp['options']['table'] == 'user'){//特殊处理user表的user id
					$sId = $aData['user_id'];
				}

				if($aTmp['type'] == 'text' && $aTmp['options']['helpname'] == 'autocomplete'){//autocomplete组件的特殊处理
					$aSourceConfig = array('source'=>$aTmp['options']['resource'], 'pkfield'=>'id', 'labelfield'=>'label');
					if(isset($aTmp2[$sId])){
						$aWhere = array($aSourceConfig['pkfield']=>array('$in'=>$aTmp2[$sId]));
						$aResult = \Application\Model\Common::getLabels4Ajax($this->getServiceManager(), $aSourceConfig, $aWhere);
					}else
						$aResult = array();
					$aFormElement[$sCode]['attributes']['data-value'] = json_encode($aResult);
				}elseif($aTmp['type'] == 'text' && $aTmp['options']['helpname'] == 'selectpanel'){//autocomplete组件的特殊处理
//					$aSourceConfig = array('source'=>$aTmp['options']['resource'], 'pkfield'=>'id', 'labelfield'=>'label');
					$sFunc = $aTmp['options']['resource'];
					$aResult = array();
					if(isset($aTmp2[$sId])){
//						$aWhere = array($aSourceConfig['pkfield']=>array('$in'=>$aTmp2[$sId]));
						$aTmp3 = \Application\Model\Common::$sFunc($this->getServiceManager());
						foreach($aTmp3['data'] as $kTmp=>$aTmp4){
							foreach($aTmp4['children'] as $kTmp2=>$aTmp5){
								if(in_array($kTmp2, $aTmp2[$sId])){
									$aTmp5['level'] = 1;
									$aResult[] = $aTmp5;
								}
							}
						}
					}
					$aFormElement[$sCode]['attributes']['data-selected'] = json_encode($aResult);
				}else{//关联关系

					$aRelationDatas = \Application\Model\Common::getRelationList($this->getServiceManager(),$nRelationType, $sId);
		            $aFormElement[$sCode]['attributes']['value'] = $aRelationDatas;

				}
			}else{
				if(isset($aData[$sCode])){

					if ($sCode == 'id' && preg_match("/\d+/i", $aData[$sCode])) {

						$aData[$sCode] = \YcheukfCommon\Lib\Functions::encodeLafResouceId($aData[$sCode], $this->sActionSource);
					}
					$aFormElement[$sCode]['attributes']['value'] = $aData[$sCode];

					//上传组件的特殊处理
					if($aTmp['type'] == 'file' && $aTmp['options']['helpname'] == 'fileupload'){
						$aFormElement[$sCode]['attributes']['data-value'] = $aData[$sCode];
					}
					//selectpanel组件处理
					if($aTmp['type'] == 'text' && $aTmp['options']['helpname'] == 'selectpanel'){
						$sLevelTmp = 1;
						if($sCode == "jflv3_id")$sLevelTmp = 2;
//						var_dump($aData);
//						var_dump($aData[$sCode]);
						$aFormElement[$sCode]['attributes']['data-selected'] = empty($aData[$sCode]) ? "[]" : json_encode(array(array(
							'value' => $aData[$sCode],
							'label' => $aData[$sCode],
							'level' => $sLevelTmp,
						)));
					}
//				var_dump($aFormElement[$sCode]);
				}else{
					switch($sCode){// 默认值
						case 'task_lifecycle':
								$aFormElement[$sCode]['attributes']['value'] = 4;
							break;
					}
				}
			}
		}

		if(isset($aFormElement['category_id']['options']['value_options'])){
			if(count($aFormElement['category_id']['options']['value_options']) < 2){
				$sId = key($aFormElement['category_id']['options']['value_options']);
				$aFormElement['category_id'] = array(
					'type' => 'hidden',
					'name' => 'category_id',
					'attributes' => array('type'=> 'hidden', 'value'=>$sId),
					'options' => array('helpname'=> 'hidden', ),
				);
			}
		}
//		var_dump($aData);
		return $aFormElement;
	}

	public function _fmtElements(){
		foreach($this->getElements() as $sSet=>$oElement){
			switch($sSet){
				case 'category_id':
					if(count($oElement->getOption('value_options')) < 2){
//						$sCidTmp = (key($oElement->getOption('value_options')));
//						$oElement->setAttribute('style', 'display:none');
//var_dump($oElement);
//						$this->remove($sSet);
//						$this->add( array(
//								'type' => 'hidden',
//								'attributes' => array('name' => 'category_id', 'type'=>'hidden', 'value'=>$sCidTmp),
//								'name' => 'category_id',
//	//							'options' => array('table'=>$sTableSource,'tips'=>""),
//							)
//						);
					}
				break;
			}
			switch(strtolower($oElement->getAttribute('type'))){
				case 'textarea':
					$aOptions = $oElement->getOptions();
					if($oElement->getOption('helpname') == 'editor'){
						$sId = $this->get('id')->getValue();
						$sViewType = (int)$this->get('view_type')->getValue();
						$aOptions['view_type'] = $sViewType;
						switch($sViewType){
							case 4://读出子记录
								$access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceManager());
								$aParams = array(
									'op'=> 'get',
									'resource'=> 'post',
									'resource_type'=> 'common',
									'access_token' => $access_token,
									'params' => array(
										'select' => array (
											"where" => array("fid"=>$sId),
										),
									),
								);
								$aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceManager(), $aParams, 'function');
								if($aResult['data']['count'] > 0){
									foreach($aResult['data']['dataset'] as $row){
										$aTmp = array();
										$aTmp['title'] = $row['title'];
										$aTmp['content'] = $row['content'];
										$aTmp['thumbnails'] = $row['thumbnails'];
										$aOptions['entities'][] = $aTmp;
									}
								}
								break;
							default:
								$aTmp = array(
									'title' => $this->get('title')->getValue(),
									'content' => $this->get('content')->getValue(),
									'thumbnails' => $this->get('thumbnails')->getValue(),
								);
								$aOptions['entities'][0] = $aTmp;
								break;
						}
						$this->get($sSet)->setOptions($aOptions);
					}
				break;
			}
		}
	}
}