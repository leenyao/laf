<?php

namespace Application\Form\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\Exception;

class Customer extends \Zend\Form\View\Helper\FormTextarea
{




    function __get($name='')
    {
        switch ($name) {
            case 'serviceManager':
                return \YcheukfCommon\Lib\Functions::getServiceManager();
                break;
            default:
                break;
        }
    }
    public function translate($value='')
    {
        return $this->translator->translate($value);
    }
    

    /**
     * 1:n 的表单配置
     * 
     * @param  [type] $aParamConfig [配置]
     * @param  [type] $sTableName   [表名]
     * @param  [type] $aData        [数据]
     * @return [type]               [description]
     */
    public function _fmtMutipleFormItems($aParamConfig, $sTableName, $aData)
    {

        $aTrData = array();
        $sHTML = "";
        $sTrHtml = "";
        $aMetadataList = array();


        $sTheader = "<th>序号</th>";
        foreach ($aParamConfig as $key => $aRow1) {
            $sTheader .= "<th>". $this->translate($sTableName. "__split__".$key). "</th>";

            if (preg_match("/^m\d+_id$/i", $key)) {
                $nMetadataId = str_replace("m", "", $key);
                $nMetadataId = str_replace("_id", "", $nMetadataId);
                $aMetadataList[$nMetadataId] = \Application\Model\Common::getResourceMetadaList(\YcheukfCommon\Lib\Functions::getServiceManager(), $nMetadataId);
                $aMetadataList[$nMetadataId][0] = "请选择类型";

            }
        }

        $nTotalLine = 4;
        if (isset($aData[$sTableName]) && isset($aData[$sTableName]['dataset']) && count($aData[$sTableName]['dataset'])>4){
            $nTotalLine = count($aData[$sTableName]['dataset']);
        }

        for ($i=0; $i <= $nTotalLine; $i++) {
            foreach ($aParamConfig as $key => $aRow1) {

                if (isset($aData[$sTableName]) && isset($aData[$sTableName]['dataset']) && isset($aData[$sTableName]['dataset'][$i])) {

                            $aParamConfig[$key]['value'] = $aData[$sTableName]['dataset'][$i][$key];
                }else{
                    $aParamConfig[$key]['value'] = "";
                }
            }


        $nTrIndex = $i+1;
        
        $sTrHtml .= "<tr>";
        $sTrHtml .= '<td> <a class="btn deltr" href="javascrip:void(0)" role="button"><i class="fa fa-name-shape-o-direction icon-remove" title="删除""></i>     <font>'.$nTrIndex.'</font></a></td>';
        foreach ($aParamConfig as $key => $aRow1) {
            $sPlaceHolder = isset($aRow1['placeholder']) ? $aRow1['placeholder'] : "";

            $sClass = '';
            if (isset($aRow1['attributes']) &&is_array($aRow1['attributes']) && count($aRow1['attributes'])) {
                $sClass = $aRow1['attributes']['class'];
            }
            switch ($aRow1['type']) {
                case 'text':
                    $sTrHtml .= '<td><input type="text" name="'.$key.'"  class="form-control '.$sClass.'" placeholder="'.$sPlaceHolder.'" value="'.$aRow1['value'].'"  pattern="" title=""></td></td>';
                    break;
                case 'textarea':
                    $sTrHtml .= '<td><textarea name="'.$key.'"  class="form-control '.$sClass.'" rows="3" placeholder="'.$sPlaceHolder.'">'.$aRow1['value'].'</textarea></td>';
                    break;
                case 'fileupload':
                    $sTrHtml .= '<td><input type="file" name="uploadfile" data-value="'.$aRow1['value'].'" data-filetype="csv" id="'.$key.'" class="form-control  upload-input" data-elname="'.$key.'"></td>';
                    break;
                case 'select-metadata':
                    $nMetadataId = str_replace("m", "", $key);
                    $nMetadataId = str_replace("_id", "", $nMetadataId);
                    if (isset($aMetadataList[$nMetadataId])) {
                        $sMetadataOptions= "";
                        foreach ($aMetadataList[$nMetadataId] as $key2 => $value2) {
                            $sSelectHtml = ($aRow1['value'] == $key2) ? "selected" : "";
                            $sMetadataOptions .= "<option {$sSelectHtml} value=".$key2.">".$value2."</option>";
                        }

                        $sTrHtml .= '<td><select name="'.$key.'" class="form-control '.$sClass.'" >
                                '.$sMetadataOptions.'
                            </select></td>';
                    }
                    break;
                


                default:
                    break;
            }
        }
        $sTrHtml .= "</tr>";

    }

        $sHTML .=<<<___EOF
        <table class="table table-hover formmutiple">
            <thead>
                <tr>{$sTheader}</tr>
            </thead>
            <tbody>
                {$sTrHtml}
            </tbody>
        </table>
        <a class="btn btn-info addnewtr" href="javascrip:void(0)" role="button"><i class="fa fa-add icon-plus"></i> 增加 新的一行记录</a>
        <script type="text/javascript">
            $(".addnewtr").click(function(){
                var oNewTr = $('table.formmutiple tr:last').clone();
                oNewTr.find("td:first font").html(parseInt(oNewTr.find("td:first font").text())+1)
                $('table.formmutiple').append(oNewTr)

            });
            $(".deltr").click(function(){
                $(this).parent("td").parent("tr").remove();  
            })
        </script>
___EOF;


        return $sHTML;
    }
}