<?php

namespace Application\Form;

use Zend\Form\Form;

class IndexForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('album2');
    }

    /**
     * 设置首页form
     */
    public function setHomeForm(){
    	$this->add(
    	    array(
    	        'name' => 'searchFilter',
    	        'attributes' => array('type' => 'hidden','id'=>'searchFilter')
    	    )
    	);

    	$this->add(
    	    array(
    	        'name' => 'keyword',
    	        'attributes' => array('type' => 'text',
    	            'id'=>'keyword',
    	            'class'=>'search-input')
    	    )
    	);
    }
}