<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Metadata extends \Application\Form\Common
{
    public function __construct($oController, $nEntityStep=null, $nEntityId=null, $sEntitySource=null, $nEntityCid=null, $nEntityFid=null)
    {
		$sm = $oController->getServiceLocator();
		$this->setController($oController,__CLASS__);
		$this->setServiceManager($sm);
		$sTableSource = "b_ads";
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('table'=>$sTableSource,'tips'=>""),
			),
			"label" => array( 
				'type' => 'text',
				'options' => array('table'=>$sTableSource),
			),
			// "Submitcancel" => array( 
			// 	'type' => 'button',
			// 	'options' => array(
			// 		'table'=>$sTableSource,
			// 		'helpname'=>'Submitcancel',
			// 	),
			// ),

		);
    $this->aFormElement['Submitcancel'] = $this->fmtSubmitButton('metadata', $nEntityCid);

		$this->_fmtFormElements(__CLASS__);

        // we want to ignore the name passed
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }


}