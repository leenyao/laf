<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
class Profilepassword extends \Application\Form\Common{

    public function __construct($oController)
    {
        $this->setController($oController, __CLASS__);
        // var_dump($this->nEntityId, $this->nEntityCid, $this->nEntityFid);
        /*
        
        $this->aFormElement = $this->addMetadataList($this->aFormElement, 7002);

         */
        
        $this->aFormElement = array();


        $this->aFormElement["oldpassword"] = array(
            'type' => 'text',
            'attributes' => array('type'=>'text', "placeholder"=>"请输入旧密码"),
            'options' => array('tips'=>""),
        );
        $this->aFormElement["newpassword1"] = array(
            'type' => 'text',
            'attributes' => array('type'=>'text', "placeholder"=>"请输入新密码"),
            'options' => array('tips'=>""),
        );
        
        $this->aFormElement["newpassword2"] = array(
            'type' => 'text',
            'attributes' => array('type'=>'text', "placeholder"=>"请输入新密码"),
            'options' => array('tips'=>""),
        );
        
        
        $this->aFormElement["Submitcancel"] = array(
            'type' => 'button',
            'options' => array(
                'helpname'=>'Submitcancel',
            ),
        );
        /*

        $this->aFormElement["user_id"] = array(//
            'type' => 'select',
            'attributes' => array('class'=>'select-tab'),
            'options' => array(
                'value_options' => $aUserId,
            ),
        );
        $this->aFormElement["start"] = array(
            'type' => 'date',
             'attributes' => array('class'=>'date-picker'),
            'options' => array('table'=>$sTableSource, 'format' => 'Y-m-d',),
        );
        $this->aFormElement["content"] = array(
            'type' => 'textarea',
            'attributes' => array( 'class'=>'summernote','required' => 'required'),
            'options' => array('helpname'=>'wysiwyg'),
        );
        $this->aFormElement["relation___1007"] = array(//1:n 关系数据模式
            'type' => 'select',
            'attributes' => array('class'=>'select-tab', 'multiple'=>1),
            'options' => array(
                'value_options' => $aM1007,
            ),
        );
        $this->aFormElement["in_file"] = array(//上传文件
            'type' => 'file',
            'params' => array('id'=>'in_file'),
            'attributes' => array('data-filetype'=>'csv'),
            'options' => array('helpname'=>'fileupload'),
        );

        */
           
        $this->_fmtFormElements(__CLASS__);
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
    public function _fmtElement4Data($aData=array(), $aFormElement){
        $aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
        if(isset($aData['id'])){//特殊处理逻辑
            // var_dump($aData['id']);
        }
        return $aFormElement;
    }


    /**
     * form过滤
     * (non-PHPdoc)
     * @see \Zend\Form\Form::getInputFilter()
     */
    public function getformInputFilter($nId=null, $aForm=array()){
        if (!$this->inputFilter || count($this->inputFilter) < 1) {
            $aRoauthuser = \Application\Model\Common::getResourceById($this->serviceManager, 'oauthuser', $nId);

// var_dump($aForm);

            $inputFilter = new \Zend\InputFilter\InputFilter();
            $factory = new \Zend\InputFilter\Factory();
                        
                        // $filter = new RegisterFilter(
                        //     new NoRecordExistsEdit(array(
                        //         'mapper' => $sm->get('zfcuser_user_mapper'),
                        //         'key' => 'email'
                        //     )),
                        //     new NoRecordExistsEdit(array(
                        //         'mapper' => $sm->get('zfcuser_user_mapper'),
                        //         'key' => 'username'
                        //     )),
                        //     $zfcUserOptions
                        // );
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'oldpassword',
                        'required' => true,
                        'validators' => array(
                            new \Application\Validator\Password($aRoauthuser),
                        ),
                    )
                )
            );
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'newpassword1',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'not_empty',
                            ),
                        ),
                    )
                )
            );
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'newpassword2',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'not_empty',
                            ),
                            new \Application\Validator\NewPassword($aForm),
                        ),
                    )
                )
            );
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}