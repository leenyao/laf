<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Rbacrole extends \Application\Form\Common
{
    public function __construct($oController)
    {
		$sm = $oController->getServiceLocator();
		$this->setController($oController,__CLASS__);
		$this->setServiceManager($sm);
		$aPermissios = \Application\Model\Common::getAllPermission2($sm);
//		var_dump($aPermissios);
		$sTableSource = "b_ads";
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('table'=>$sTableSource,'tips'=>""),
			),
			"role_name" => array( 
				'type' => 'text',
				'options' => array('table'=>$sTableSource),
			),
			"relation___rbacrrp" => array( //用户组
				'type' => 'select',
				'attributes' => array( 'multiple'=>1,'class'=>'select-tab'),
				'options' => array(
					'table'=>$sTableSource,
//                    'value_options' => $aPermissios,
                    'group_value_options' => $aPermissios,
					'helpname'=>'Groupselect',
				),
			),
			"Submitcancel" => array( 
				'type' => 'button',
				'options' => array(
					'table'=>$sTableSource,
					'helpname'=>'Submitcancel',
				),
			),

		);
		$this->_fmtFormElements(__CLASS__);

        // we want to ignore the name passed
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }


	public function _fmtElement4Data($aData=array(), $aFormElement){
		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);

		if(isset($aData['id'])){
			$aResult = \Application\Model\Common::getResourceList($this->getServiceManager(), array('source'=>'rbacrrp', 'pkfield'=>'role_id', 'labelfield'=>'perm_id'), array('role_id'=>$aData['id']), true, 1000);
//			var_dump($aResult);
			if(isset($aResult[$aData['id']]) && count($aResult[$aData['id']])){
//				foreach($aFormElement['relation___rbacrrp']['options']['group_value_options'] as $sId=>$row){
//					if(in_array($sId, $aResult[$aData['id']]))
//						$aFormElement['relation___rbacrrp']['options']['group_value_options'][$sId]['selected'] = true;
//					if(count($row['children'])){
//						foreach($row['children'] as $sId2=>$row2){
//							if(in_array($sId2, $aResult[$aData['id']]))
//								$aFormElement['relation___rbacrrp']['options']['group_value_options'][$sId]['children'][$sId2]['selected'] = true;
//						}
//					}
//				}
//				foreach($aResult[$aData['id']] as $sIdTmp){
//					$aFormElement['relation___rbacrrp']['options']['group_value_options'][] = $sIdTmp;
//				}
				$aFormElement['relation___rbacrrp']['options']['selectd_values'] = $aResult[$aData['id']];
			}
		}
//		var_dump($aFormElement['relation___rbacrrp']['options']);
		return $aFormElement;
	}

}