<?php
namespace Application\Form;
class SystemBatchJobs extends \Application\Form\Common{

    public function __construct($oController, $nEntityStep=null, $nEntityId=null, $sEntitySource=null, $nEntityCid=null, $nEntityFid=null)
    {
        $this->setController($oController, __CLASS__);
        $this->setActionSource($sEntitySource);
        $this->setActionId($nEntityId);
        $this->setActionCid($nEntityCid);
        $this->setActionFid($nEntityFid);
        $this->setActionStep($nEntityStep);
        $this->aFormElement = array(
            "id" => array( 
                'type' => 'hidden',
                'attributes' => array('type'=>'hidden'),
                'options' => array('tips'=>""),
            ),
            "memo" => array( 
                'type' => 'textarea',
                'attributes' => array('type'=>'textarea', 'value'=>''),
                'options' => array('tips'=>""),
            ),
            "out_file" => array( 
                'type' => 'hidden',
                'attributes' => array('type'=>'hidden', 'value'=>''),
                'options' => array('tips'=>""),
            ),
            "in_file" => array( 
                'type' => 'file',
                'params' => array('id'=>'file'),
                'attributes' => array('data-filetype'=>'csv'),
                'options' => array('helpname'=>'fileupload'),
            ),
            /*

            "user_id" => array( //所属项目
                'type' => 'select',
                'attributes' => array('class'=>'select-tab'),
                'options' => array(
                    'value_options' => $aUserId,
                ),
            ),
            "start" => array( 
                'type' => 'date',
                 'attributes' => array('class'=>'date-picker'),
                'options' => array('table'=>$sTableSource, 'format' => 'Y-m-d',),
            ),
            "content" => array( 
                'type' => 'textarea',
                'attributes' => array( 'class'=>'summernote','required' => 'required'),
                'options' => array('helpname'=>'wysiwyg'),
            ),
            "code" => array( 
                'type' => 'text',
                'attributes' => array('type'=>'text'),
                'options' => array('tips'=>""),
            ),
            "relation___1007" => array( //所属项目
                'type' => 'select',
                'attributes' => array('class'=>'select-tab', 'multiple'=>1),
                'options' => array(
                    'value_options' => $aM1007,
                ),
            ),            
            */
            // "Submitcancel" => array( 
            //     'type' => 'button',
            //     'options' => array(
            //         "aftersave_urlarray" => array(
            //             "route"=>"zfcadmin/contentlist", 
            //             "type"=>"system_batch_jobs", 
            //             "cid"=>0, 
            //             "id"=>"0", 
            //         ),

            //         // $aOptions['redirect']['urlarray']
            //         'helpname'=>'Submitcancel',
            //     ),
            // ),

        );
        $this->aFormElement['Submitcancel'] = $this->fmtSubmitButton('system_batch_jobs');

        $this->_fmtFormElements(__CLASS__);
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
    public function _fmtElement4Data($aData=array(), $aFormElement){
        $aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
        if(isset($aData['id'])){
        /*
            $aRelation1007 = \Application\Model\Common::getRelationList($this->getServiceManager(),1007, $aData['id']);
            $aFormElement['relation___1008']['attributes']['value'] = $aRelation1008;
        */
        }
        return $aFormElement;
    }
}