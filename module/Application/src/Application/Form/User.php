<?php

namespace Application\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

use Zend\Form\Form;
use Zend\Form\Element;

class User extends \Application\Form\Common
{
    public function __construct($oController)
    {
		$this->setController($oController, __CLASS__);
		$sTableSource = "user";
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('table'=>$sTableSource,'tips'=>""),
			),
			"user_id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('table'=>$sTableSource,'tips'=>""),
			),
			"username" => array( 
				'type' => 'text',
				'options' => array('table'=>$sTableSource),
			),
			"password" => array( 
				'type' => 'password',
				'attributes' => array('placeholder'=>'在修改用户时, 为空即不修改原密码'),
				'options' => array('table'=>$sTableSource),
			),
			"role_id" => array( //用户组
				'type' => 'select',
				'attributes' => array('multiple'=>0, 'class'=>'select-tab'),
				'options' => array('table'=>$sTableSource,
                                'value_options' => \Application\Model\Common::getAllUserRoles($this->serviceManager),
				),
			),
			"display_name" => array( 
				'type' => 'text',
				'options' => array('table'=>$sTableSource),
			),
			"gender" => array(
				'type' => 'select',
				'attributes' => array( 'class'=>'select-tab'),
				'options' => array('table'=>$sTableSource,
                                'value_options' => array('1'=>'男', '2'=>'女'),
				),
			),
			"email" => array( 
				'type' => 'text',
				'options' => array('table'=>$sTableSource,'tips'=>"user_op_email"),
			),
			"phone" => array( 
				'type' => 'text',
				'options' => array('table'=>$sTableSource),
			),
			"Submitcancel" => array( 
				'type' => 'button',
				'options' => array(
					'table'=>$sTableSource,
					'helpname'=>'Submitcancel',
				),
			),

		);
		$this->_fmtFormElements(__CLASS__);

        // we want to ignore the name passed
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
	public function _fmtElement4Data($aData=array(), $aFormElement){
		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
		$aFormElement['password']['attributes']['value'] = '';
		return $aFormElement;
	}

    /**
     * form过滤
     * (non-PHPdoc)
     * @see \Zend\Form\Form::getInputFilter()
     */
    public function getformInputFilter($nId=null){
        if (!$this->inputFilter || count($this->inputFilter) < 1) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
                        
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'username',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'not_empty',
                            ),
                            array(
                                'name' => 'string_length',
                                'options' => array(
                                    'min' => 6,
                                    'max' => 20
                                ),
                            ),
                        ),
                    )
                )
            );
            if(is_null($nId)){
                $inputFilter->add(
                    $factory->createInput(
                        array(
                            'name'     => 'password',
    //                        'required' => true,
                            'validators' =>array(
                                array(
                                    'name' => 'not_empty',
                                ),
                                array(
                                    'name' => 'string_length',
                                    'options' => array(
                                        'min' => 6,
                                        'max' => 20
                                    ),
                                ),
                            ),
                        )
                    )
                );
            }
//            $inputFilter->add(
//                $factory->createInput(
//                    array(
//                        'name'     => 'phone',
//                        'required' => true,
//                        'validators' => array(
//                            array(
//                                'name' => 'not_empty',
//                            ),
//                            array(
//                                'name' => 'Regex',
//                                'options' => array(
//                                    'pattern' => '/(^1[3-5,8]{1}[0-9]{9}$)|(^([0-9]{3,4}-)?[0-9]{7,8}$)/',
//                                    'messages' => array(
//                                        'regexNotMatch' => '您输入的联系电话格式不正确',
//                                    )
//                                )
//                            )
//                        )
//                    )
//                )
//            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'email',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'not_empty',
                            ),
                            array(
                                'name' => 'string_length',
                                'options' => array(
                                    'max' => 100
                                ),
                            ),
                            array(
                                'name' => 'Regex',
                                'options' => array(
                                    'pattern' => '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/',
                                    'messages' => array(
                                        'regexNotMatch' => '您输入的邮箱格式不对',
                                    )
                                )
                            )
                        )
                    )
                )
            );
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}