<?php

namespace Application\Listener;

use Zend\Mvc\MvcEvent;
use Zend\Http\Request as HttpRequest;
use InvalidArgumentException;

class DebugDump
{
    /**
     * @param MvcEvent $e
     */
    public static function onDump(\Zend\EventManager\Event $e)
    {
		$e = func_get_arg(0);
		$sm = $e->getTarget();
		list($data, $memo, $param, $method) = $e->getParams();
		$dm = \YcheukfCommon\Lib\Functions::getMongoDm($sm);
		$oEntity = new \ZmcBase\MongoDocument\DebugApilog();
		$oEntity->setMemo($memo);
		$oEntity->setMethod($method);
		$oEntity->setModified(time());
		$oEntity->setContent(is_object($data) ? get_class($data) : $data);
		$oEntity->setParam($param);
		$dm->persist($oEntity);
//		$dm->flush();

//				$oQb = $dm->createQueryBuilder('ZmcBase\MongoDocument\DebugApilog');
//				$oQbInsert = $oQb->insert();
//				$bFlag = $oQbInsert->getQuery()->execute();
		if(is_string($_SERVER['REQUEST_URI']) && (//ignore api request
			preg_match("/.*\/api\/.*/i", $_SERVER['REQUEST_URI']))
		)return true;

		return false;
	}
}
