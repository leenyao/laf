<?php

namespace Application\Listener;

use Zend\Mvc\MvcEvent;
use Zend\Http\Request as HttpRequest;
use InvalidArgumentException;

class LoginFilter
{
    /**
     * @param MvcEvent $e
     */
    public static function onLogin()
    {
		$e = func_get_arg(0);
//		echo 123;
		$e->getTarget()->remove('identity');
		$e->getTarget()->remove('credential');
	}
}
