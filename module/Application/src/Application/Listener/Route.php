<?php

namespace Application\Listener;

use Zend\Mvc\MvcEvent;
use Zend\Http\Request as HttpRequest;
use InvalidArgumentException;

class Route
{
    /**
     * @param MvcEvent $e
     */
    public static function onRoute(MvcEvent $e)
    {


        $app = $e->getTarget();
        $sm = $app->getServiceManager();
        $request = $sm->get('request');
        //命令行不验证
        if ($request instanceof consolerequest) {
            return;
        }
        if (in_array($_SERVER['SERVER_NAME'], ['localhost'])) {
            return;
        }
        \YcheukfCommon\Lib\Functions::saveUserLog($sm);
        //ajax不验证
        if ($request->isXmlHttpRequest()) {
            return;
        }
        //通过route获取permission
       $sPermission = \Application\Model\Common::getPermissionByRoute($app->getMvcEvent()->getRouteMatch());
        $oRbacService = $sm->get('ZfcRbac\Service\Rbac');
        $sRole = implode(',', $oRbacService->getIdentity()->getRoles());
        if($sPermission != "debug")
            \YcheukfCommon\Lib\Functions::debug(array("role"=>$sRole, "permission"=>$sPermission), "[inline]---[rbac]---check");


        $userAllow = false;

        if ($sm->get('zfcuser_auth_service')->getIdentity()) {
            $userId = $sm->get('zfcuser_auth_service')->getIdentity()->getId();;
            if ($userId == 1044
                && in_array($sPermission, ['zfcadmin/contentlist/metadata', 'zfcadmin/contentedit/metadata'])
            ) {
                $userAllow = true;
            }
        }

        // \YcheukfCommon\Lib\Functions::onlinedebug('sRole', $sRole);

        //过滤一些Permission
        $bFilter = \Application\Model\Common::_chkPermissionFilter($sm, $sPermission);
        if(!$bFilter)return;

        \YcheukfCommon\Lib\Functions::debug($sPermission, "[inline]---[onroute]---sPermission");

        //检查该请求是否已经被存储
        $bExists = self::_chkPermissionExists($sm, $sPermission);


        if($bExists){
            //读取用户角色
            $aConfig = $sm->get('config');
            $oRbacService = $sm->get('ZfcRbac\Service\Rbac');
            if($oRbacService->hasRole('superadmin') || $oRbacService->isGranted($sPermission) || $userAllow){
                return true;
            }else{
                $sRole = implode(',', $oRbacService->getIdentity()->getRoles());
                \YcheukfCommon\Lib\Functions::debug(array($sRole, $sPermission), "[inline]---[permission]---deny");
                if($sRole == 'guest')
                    $sSmg = "您还没有登陆, 请登陆后再尝试";
                else
                    $sSmg = "当前用户 (".$sRole.") 没有相应权限".($aConfig['debugFlag']?$sPermission:"");


                $e->setError($oRbacService::ERROR_RUNTIME)
                    ->setParam('sRole', $sRole)
                    ->setParam('message', $sSmg);
                $app->getEventManager()->trigger('dispatch.error', $e);
                return;
            }
        }else
            return;
    }


//    public function onRoute(MvcEvent $event)
//    {
//        /* @var $service \BjyAuthorize\Service\Authorize */
//        $service    = $this->serviceLocator->get('BjyAuthorize\Service\Authorize');
//        $match      = $event->getRouteMatch();
//        $routeName  = $match->getMatchedRouteName();
//
//        if ($service->isAllowed('route/' . $routeName)) {
//            return;
//        }
//
//        $event->setError(static::ERROR);
//        $event->setParam('route', $routeName);
//        $event->setParam('identity', $service->getIdentity());
//        $event->setParam('exception', new UnAuthorizedException('You are not authorized to access ' . $routeName));
//
//        /* @var $app \Zend\Mvc\Application */
//        $app = $event->getTarget();
//
//        $app->getEventManager()->trigger(MvcEvent::EVENT_DISPATCH_ERROR, $event);
//    }
    public static function _chkPermissionExists($sm, $sPermission){

        return true;
    }


}
