<?php
namespace Application\Model;

class User implements \ZfcUser\Entity\UserInterface
{
	/**
	* @var int
	**/
	protected $id;
	/**
	* @var int
	**/
	protected $user_id = 0;
	/**
	* @var string
	**/
	protected $username = '';
	/**
	* @var string
	**/
	protected $email = '';
	/**
	* @var string
	**/
	protected $email_tmp = '';
	/**
	* @var string
	**/
	protected $mail_verify_no = '';
	/**
	* @var int
	**/
	protected $email_verified = 2;
	/**
	* @var string
	**/
	protected $phone = '';
	/**
	* @var string
	**/
	protected $phone_tmp = '';
	/**
	* @var string
	**/
	protected $phone_verify_no = '';
	/**
	* @var int
	**/
	protected $phone_verified = 2;
	/**
	* @var string
	**/
	protected $display_name = '';
	/**
	* @var string
	**/
	protected $avatar = '';
	/**
	* @var string
	**/
	protected $password = '';
	/**
	* @var int
	**/
	protected $status = 1;
	/**
	* @var int
	**/
	protected $state = 0;
	/**
	* @var int
	**/
	protected $gender = 1;
	/**
	* @var string
	**/
	protected $user_url = '';
	/**
	* @var string
	**/
	protected $user_registered = '';
	/**
	* @var string
	**/
	protected $user_activation_key = '';
	/**
	* @var int
	**/
	protected $credit = 0;
	/**
	* @var int
	**/
	protected $m157_id = 0;
	/**
	* @var int
	**/
	protected $m104_id = 0;
	/**
	* @var int
	**/
	protected $m112_id = 0;
	/**
	* @var int
	**/
	protected $modified = 0;
	/**
	* @var int
	**/
	protected $m154_id = 3;
	/**
	* @var string
	**/
	protected $alipay_acc = '';
	/**
	* @var string
	**/
	protected $birthday = '';
	/**
	* @var string
	**/
	protected $joinworkdate = '';
	/**
	* @var int
	**/
	protected $location_city_id = 0;
	/**
	* @var int
	**/
	protected $m105_id = 0;
	/**
	* @var string
	**/
	protected $memo = '';
	/**
	* @var string
	**/
	protected $jobposition = '';
	/**
	* @var string
	**/
	protected $jobcompany = '';
	/**
	* @var int
	**/
	protected $privacy = 1;
	/**
	* @var int
	**/
	protected $agent_id = 0;

	/**
	* Gets the id property
	* @return integer the id
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	* Sets the id property
	* @param integer the id to set
	* @return void
	*/
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	* Gets the user_id property
	* @return integer the user_id
	*/
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	* Sets the user_id property
	* @param integer the user_id to set
	* @return void
	*/
	public function setUserId($user_id)
	{
		$this->user_id = $user_id;
	}

	/**
	* Gets the username property
	* @return string the username
	*/
	public function getUsername()
	{
		return $this->username;
	}

	/**
	* Sets the username property
	* @param string the username to set
	* @return void
	*/
	public function setUsername($username)
	{
		$this->username = $username;
	}

	/**
	* Gets the email property
	* @return string the email
	*/
	public function getEmail()
	{
		return $this->email;
	}

	/**
	* Sets the email property
	* @param string the email to set
	* @return void
	*/
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	* Gets the email_tmp property
	* @return string the email_tmp
	*/
	public function getEmailTmp()
	{
		return $this->email_tmp;
	}

	/**
	* Sets the email_tmp property
	* @param string the email_tmp to set
	* @return void
	*/
	public function setEmailTmp($email_tmp)
	{
		$this->email_tmp = $email_tmp;
	}

	/**
	* Gets the mail_verify_no property
	* @return string the mail_verify_no
	*/
	public function getMailVerifyNo()
	{
		return $this->mail_verify_no;
	}

	/**
	* Sets the mail_verify_no property
	* @param string the mail_verify_no to set
	* @return void
	*/
	public function setMailVerifyNo($mail_verify_no)
	{
		$this->mail_verify_no = $mail_verify_no;
	}

	/**
	* Gets the email_verified property
	* @return integer the email_verified
	*/
	public function getEmailVerified()
	{
		return $this->email_verified;
	}

	/**
	* Sets the email_verified property
	* @param integer the email_verified to set
	* @return void
	*/
	public function setEmailVerified($email_verified)
	{
		$this->email_verified = $email_verified;
	}

	/**
	* Gets the phone property
	* @return string the phone
	*/
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	* Sets the phone property
	* @param string the phone to set
	* @return void
	*/
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/**
	* Gets the phone_tmp property
	* @return string the phone_tmp
	*/
	public function getPhoneTmp()
	{
		return $this->phone_tmp;
	}

	/**
	* Sets the phone_tmp property
	* @param string the phone_tmp to set
	* @return void
	*/
	public function setPhoneTmp($phone_tmp)
	{
		$this->phone_tmp = $phone_tmp;
	}

	/**
	* Gets the phone_verify_no property
	* @return string the phone_verify_no
	*/
	public function getPhoneVerifyNo()
	{
		return $this->phone_verify_no;
	}

	/**
	* Sets the phone_verify_no property
	* @param string the phone_verify_no to set
	* @return void
	*/
	public function setPhoneVerifyNo($phone_verify_no)
	{
		$this->phone_verify_no = $phone_verify_no;
	}

	/**
	* Gets the phone_verified property
	* @return integer the phone_verified
	*/
	public function getPhoneVerified()
	{
		return $this->phone_verified;
	}

	/**
	* Sets the phone_verified property
	* @param integer the phone_verified to set
	* @return void
	*/
	public function setPhoneVerified($phone_verified)
	{
		$this->phone_verified = $phone_verified;
	}

	/**
	* Gets the display_name property
	* @return string the display_name
	*/
	public function getDisplayName()
	{
		return $this->display_name;
	}

	/**
	* Sets the display_name property
	* @param string the display_name to set
	* @return void
	*/
	public function setDisplayName($display_name)
	{
		$this->display_name = $display_name;
	}

	/**
	* Gets the avatar property
	* @return string the avatar
	*/
	public function getAvatar()
	{
		return $this->avatar;
	}

	/**
	* Sets the avatar property
	* @param string the avatar to set
	* @return void
	*/
	public function setAvatar($avatar)
	{
		$this->avatar = $avatar;
	}

	/**
	* Gets the password property
	* @return string the password
	*/
	public function getPassword()
	{
		return $this->password;
	}

	/**
	* Sets the password property
	* @param string the password to set
	* @return void
	*/
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	* Gets the status property
	* @return integer the status
	*/
	public function getStatus()
	{
		return $this->status;
	}

	/**
	* Sets the status property
	* @param integer the status to set
	* @return void
	*/
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	* Gets the state property
	* @return integer the state
	*/
	public function getState()
	{
		return $this->state;
	}

	/**
	* Sets the state property
	* @param integer the state to set
	* @return void
	*/
	public function setState($state)
	{
		$this->state = $state;
	}

	/**
	* Gets the gender property
	* @return integer the gender
	*/
	public function getGender()
	{
		return $this->gender;
	}

	/**
	* Sets the gender property
	* @param integer the gender to set
	* @return void
	*/
	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	/**
	* Gets the user_url property
	* @return string the user_url
	*/
	public function getUserUrl()
	{
		return $this->user_url;
	}

	/**
	* Sets the user_url property
	* @param string the user_url to set
	* @return void
	*/
	public function setUserUrl($user_url)
	{
		$this->user_url = $user_url;
	}

	/**
	* Gets the user_registered property
	* @return datetime the user_registered
	*/
	public function getUserRegistered()
	{
		return $this->user_registered;
	}

	/**
	* Sets the user_registered property
	* @param datetime the user_registered to set
	* @return void
	*/
	public function setUserRegistered($user_registered)
	{
		$this->user_registered = $user_registered;
	}

	/**
	* Gets the user_activation_key property
	* @return string the user_activation_key
	*/
	public function getUserActivationKey()
	{
		return $this->user_activation_key;
	}

	/**
	* Sets the user_activation_key property
	* @param string the user_activation_key to set
	* @return void
	*/
	public function setUserActivationKey($user_activation_key)
	{
		$this->user_activation_key = $user_activation_key;
	}

	/**
	* Gets the credit property
	* @return integer the credit
	*/
	public function getCredit()
	{
		return $this->credit;
	}

	/**
	* Sets the credit property
	* @param integer the credit to set
	* @return void
	*/
	public function setCredit($credit)
	{
		$this->credit = $credit;
	}

	/**
	* Gets the m157_id property
	* @return integer the m157_id
	*/
	public function getM157Id()
	{
		return $this->m157_id;
	}

	/**
	* Sets the m157_id property
	* @param integer the m157_id to set
	* @return void
	*/
	public function setM157Id($m157_id)
	{
		$this->m157_id = $m157_id;
	}

	/**
	* Gets the m104_id property
	* @return integer the m104_id
	*/
	public function getM104Id()
	{
		return $this->m104_id;
	}

	/**
	* Sets the m104_id property
	* @param integer the m104_id to set
	* @return void
	*/
	public function setM104Id($m104_id)
	{
		$this->m104_id = $m104_id;
	}

	/**
	* Gets the m112_id property
	* @return integer the m112_id
	*/
	public function getM112Id()
	{
		return $this->m112_id;
	}

	/**
	* Sets the m112_id property
	* @param integer the m112_id to set
	* @return void
	*/
	public function setM112Id($m112_id)
	{
		$this->m112_id = $m112_id;
	}

	/**
	* Gets the modified property
	* @return timestamp the modified
	*/
	public function getModified()
	{
		return $this->modified;
	}

	/**
	* Sets the modified property
	* @param timestamp the modified to set
	* @return void
	*/
	public function setModified($modified)
	{
		$this->modified = $modified;
	}

	/**
	* Gets the m154_id property
	* @return integer the m154_id
	*/
	public function getM154Id()
	{
		return $this->m154_id;
	}

	/**
	* Sets the m154_id property
	* @param integer the m154_id to set
	* @return void
	*/
	public function setM154Id($m154_id)
	{
		$this->m154_id = $m154_id;
	}

	/**
	* Gets the alipay_acc property
	* @return string the alipay_acc
	*/
	public function getAlipayAcc()
	{
		return $this->alipay_acc;
	}

	/**
	* Sets the alipay_acc property
	* @param string the alipay_acc to set
	* @return void
	*/
	public function setAlipayAcc($alipay_acc)
	{
		$this->alipay_acc = $alipay_acc;
	}

	/**
	* Gets the birthday property
	* @return datetime the birthday
	*/
	public function getBirthday()
	{
		return $this->birthday;
	}

	/**
	* Sets the birthday property
	* @param datetime the birthday to set
	* @return void
	*/
	public function setBirthday($birthday)
	{
		$this->birthday = $birthday;
	}

	/**
	* Gets the joinworkdate property
	* @return datetime the joinworkdate
	*/
	public function getJoinworkdate()
	{
		return $this->joinworkdate;
	}

	/**
	* Sets the joinworkdate property
	* @param datetime the joinworkdate to set
	* @return void
	*/
	public function setJoinworkdate($joinworkdate)
	{
		$this->joinworkdate = $joinworkdate;
	}

	/**
	* Gets the location_city_id property
	* @return integer the location_city_id
	*/
	public function getLocationCityId()
	{
		return $this->location_city_id;
	}

	/**
	* Sets the location_city_id property
	* @param integer the location_city_id to set
	* @return void
	*/
	public function setLocationCityId($location_city_id)
	{
		$this->location_city_id = $location_city_id;
	}

	/**
	* Gets the m105_id property
	* @return integer the m105_id
	*/
	public function getM105Id()
	{
		return $this->m105_id;
	}

	/**
	* Sets the m105_id property
	* @param integer the m105_id to set
	* @return void
	*/
	public function setM105Id($m105_id)
	{
		$this->m105_id = $m105_id;
	}

	/**
	* Gets the memo property
	* @return string the memo
	*/
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	* Sets the memo property
	* @param string the memo to set
	* @return void
	*/
	public function setMemo($memo)
	{
		$this->memo = $memo;
	}

	/**
	* Gets the jobposition property
	* @return string the jobposition
	*/
	public function getJobposition()
	{
		return $this->jobposition;
	}

	/**
	* Sets the jobposition property
	* @param string the jobposition to set
	* @return void
	*/
	public function setJobposition($jobposition)
	{
		$this->jobposition = $jobposition;
	}

	/**
	* Gets the jobcompany property
	* @return string the jobcompany
	*/
	public function getJobcompany()
	{
		return $this->jobcompany;
	}

	/**
	* Sets the jobcompany property
	* @param string the jobcompany to set
	* @return void
	*/
	public function setJobcompany($jobcompany)
	{
		$this->jobcompany = $jobcompany;
	}

	/**
	* Gets the privacy property
	* @return integer the privacy
	*/
	public function getPrivacy()
	{
		return $this->privacy;
	}

	/**
	* Sets the privacy property
	* @param integer the privacy to set
	* @return void
	*/
	public function setPrivacy($privacy)
	{
		$this->privacy = $privacy;
	}

	/**
	* Gets the agent_id property
	* @return integer the agent_id
	*/
	public function getAgentId()
	{
		return $this->agent_id;
	}

	/**
	* Sets the agent_id property
	* @param integer the agent_id to set
	* @return void
	*/
	public function setAgentId($agent_id)
	{
		$this->agent_id = $agent_id;
	}

	public function toArray()
	{
		$array = array();
		foreach ($this as $k=>$item) {
			if(empty($item))continue;
			$array[$k] = $item;
		}
		return $array;
	}			
}
