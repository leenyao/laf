<?php

namespace Application\Procedures;


class Checkaccess extends \Application\Procedures\Procedures
{
    /**
     * run get operation
     *
     * @param integer $aParam
     * @return string json string
     */
    public function go($aParam, $bCheckToken) {
		//init
		list($nCode, $aInit) = $this->_init($aParam, $this->sResource, $bCheckToken);
		if($nCode != 1)\YcheukfCommon\Lib\Functions::throwErrorByCode($nCode);

		if(
            !isset($aParam['params']["access_token"]) 
            || empty($aParam['params']["access_token"])
        ){
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2059);
        }

        $aData = $this->_getMysqlRecords('oauthtokens', array('access_token'=>$aParam['params']["access_token"]));
//var_dump($aParam['params']);
//exit;
        if(empty($aData))\YcheukfCommon\Lib\Functions::throwErrorByCode(2074, null, array('[=replacement1=]'=>$aParam['params']["access_token"]));
        return $aData;
    }


}