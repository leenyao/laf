<?php

namespace Application\Procedures;


class Login extends \Application\Procedures\Procedures
{
    /**
     * run get operation
     *
     * @param integer $aParam
     * @return string json string
     */
    public function go($aParam, $bCheckToken=true) {
		//init
		list($nCode, $aInit) = $this->_init($aParam, $this->sResource, $bCheckToken);
		if($nCode != 1)\YcheukfCommon\Lib\Functions::throwErrorByCode($nCode);

		if(
            !isset($aParam['params']["client_id"]) 
            || empty($aParam['params']["client_id"])
//            || !isset($aParam['params']["client_secret"]) 
//            || empty($aParam['params']["client_secret"])
            || !isset($aParam['params']["username"]) 
            || empty($aParam['params']["username"])
            || !isset($aParam['params']["password"]) 
            || empty($aParam['params']["password"])
            
        ){
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2059);
        }
        /*********check oauth client
        if(intval($aParam['params']["client_id"]) != 5000){
            $aClient = $this->_getMysqlRecords('oauthclients', array('client_id'=>$aParam['params']["client_id"]));
            if(empty($aClient))\YcheukfCommon\Lib\Functions::throwErrorByCode(2060);
            if($aClient['client_secret'] != $aParam['params']["client_secret"])\YcheukfCommon\Lib\Functions::throwErrorByCode(2060);
        }
        */
        $aUser = $this->_getMysqlRecords('oauthuser', array('username'=>$aParam['params']["username"]));
        if(empty($aUser))\YcheukfCommon\Lib\Functions::throwErrorByCode(2061);
        if(strtotime($aUser['allowlogin_timestamp']) > time())\YcheukfCommon\Lib\Functions::throwErrorByCode(2063);
        if(($aUser['password']) != md5($aParam['params']["password"]))$this->_recordErrorCounts($aUser);
        $aParams = array(
            'op'=> 'save',
            'resource'=> 'oauthuser',
            'resource_type'=> 'common',
            'access_token' => "",
            'params' => array(
                'dataset' => array (
                    array(
                        'retry_count'=>0, 
                        'last_login_timestamp'=>date("Y-m-d H:i:s"), 
                        'allowlogin_timestamp'=>'1970-01-01 01:01:00'
                    ),
                ),
                'where' => array('id' => $aUser['id']),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
        
        $aParams = array(
            'op'=> 'get',
            'resource'=> 'oauthtokens',
            'resource_type'=> 'common',
            'access_token' => "",
            'params' => array(
                'select' => array (
                    "where" => array(
                        'client_id' => $aParam['params']["client_id"],
                        'user_id' => $aUser["id"],
                    ),
                ),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');

        if(empty($aResult['data']['count'])){
            $sReturn = md5($aParam['params']["client_id"].$aUser["id"].time());
            $aParams = array(
                'op'=> 'save',
                'resource'=> 'oauthtokens',
                'resource_type'=> 'common',
                'access_token' => "",
                'params' => array(
                    'dataset' => array (
                        array(
                            'access_token' => $sReturn, 
                            'expires'=>date("Y-m-d H:i:s", time()+7*24*3600), 
                            'client_id'=>$aParam['params']["client_id"], 
                            'user_id'=>$aUser["id"]
                        ),
                    ),
                ),
            );
            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
        }else{
            if(strtotime($aResult['data']['dataset'][0]['expires']) < time()){
                $aParams = array(
                    'op'=> 'delete',
                    'resource'=> 'oauthtokens',
                    'resource_type'=> 'common',
                    'access_token' => '',
                    'params' => array(
        //                'where' => array('user_id'=>$id),
                        'where' => array(
                            'client_id'=>$aParam['params']["client_id"], 
                            'user_id'=>$aUser["id"]
                        ),
                    ),
                );
                $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2064);
            }
            $sReturn = $aResult['data']['dataset'][0]['access_token'];
        }
        return array('access_token'=>$sReturn, 'user_id'=>$aUser["id"]);
    }

    //记录登陆错误次数
    function _recordErrorCounts($aUser){
        if($aUser['retry_count'] > 4){
            $aParams = array(
                'op'=> 'save',
                'resource'=> 'oauthuser',
                'resource_type'=> 'common',
                'access_token' => "",
                'params' => array(
                    'dataset' => array (
                        array( 'retry_count' => 0, 'allowlogin_timestamp'=>date("Y-m-d H:i:s", time()+60*15)),
                    ),
                    'where' => array('id' => $aUser['id']),
                ),
            );
            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2062);
        }else{
            $aParams = array(
                'op'=> 'save',
                'resource'=> 'oauthuser',
                'resource_type'=> 'common',
                'access_token' => "",
                'params' => array(
                    'dataset' => array (
                        array( '$inc' => array('retry_count' => 1)),
                    ),
                    'where' => array('id' => $aUser['id']),
                ),
            );
            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2061);
        }
    }

}