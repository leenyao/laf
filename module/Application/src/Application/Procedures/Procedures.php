<?php

namespace Application\Procedures;


class Procedures extends \YcheukfCommon\Lib\Api
{


	function chg2Shift8($a){

		if(preg_match('/qq`/', $a)){
			$a = 'QQ网友'.(substr($a, strpos( $a, '`')+1, 5));
		}elseif(preg_match('/weibo`/', $a)){
			$a = '新浪微博网友'.(substr($a, strpos( $a, '`')+1, 5));
		}elseif(preg_match('/\d+/', $a)){
			$a = str_replace(substr($a, 3, 4), "****", $a);
		}elseif(preg_match('/@/', $a)){
			$a = str_replace(substr($a, (strpos( $a, "@")-4<1?1:strpos( $a, "@")-4), (strpos( $a, "@")>4 ? 4 : 2)), "****", $a);
		}
		return $a;
	}
	function _getMysqlRecordsCount($sResource, $aWhere){
		$aResourceClass = $this->getConfigByResource($sResource);
		$oSelect = new \Zend\Db\Sql\Select();
		$oSelect->from($aResourceClass['sTableName']);
		$oSelect->where($this->chg2MySQLWhere($aWhere));
		$stmt = new \Zend\Db\Sql\Sql($this->sm->get('Zend\Db\Adapter\Adapter')->getSlaveAdapter());
		$resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
		$oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($oSelect)->execute());
		return $oResult->count();
	}
	function _getMysqlRecords($sResource, $aWhere, $bMutiple=false, $nLimit=null, $sOrder=null){
		$aResourceClass = $this->getConfigByResource($sResource);
		$oSelect = new \Zend\Db\Sql\Select();
		$oSelect->from($aResourceClass['sTableName']);
		$oSelect->where($this->chg2MySQLWhere($aWhere));
		if(!is_null($nLimit))
			$oSelect->limit($nLimit);
		if(!is_null($sOrder))
			$oSelect->order($sOrder);
		$stmt = new \Zend\Db\Sql\Sql($this->sm->get('Zend\Db\Adapter\Adapter')->getSlaveAdapter());
		$resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
		$oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($oSelect)->execute());
//		var_dump($sResource);
//		var_dump($oSelect);
		$aReturn = array();
		if($oResult->count()){
			while($oResult->valid()){
				$aTmp = $oResult->current();
				if($aTmp){
					$aTmp2 = $aTmp->getArrayCopy();
					if($bMutiple)
						$aReturn[$aTmp2['id']] = $aTmp2;
					else
						$aReturn = $aTmp2;
				}
				$oResult->next();
			}
		}
		else{
			$aReturn = array();
		}

		return $aReturn;
	}


	function _getMongoRecords($sResource, $aWhere, $bMutiple=false, $nLimit=null, $sOrder=null){
		$oMongoDm = \YcheukfCommon\Lib\Functions::getMongoDm($this->sm);

		$aInit = $this->getConfigByResource($sResource);
		$oQbFind = $oMongoDm->createQueryBuilder($aInit['sMongoDocument'])->find();
		$aWhere = $this->chg2MongoWhere($aInit['sMongoDocument'], $aWhere);
		$oQbFind = $oQbFind->setQueryArray($aWhere);
		if(!is_null($sOrder)){
			$aSplit = explode(" ", $sOrder);
			$oQbFind->sort($aSplit[0], (isset($aSplit[1]) ? $aSplit[1] : 'asc'));
		}
		$oCursor = $oQbFind->getQuery()->execute();

		if(!is_null($nLimit))
			$oCursor->limit($nLimit);
		$aReturn = array();
		while($oCursor->hasNext()){
			$oCursor->next();
			$aTmp = $oCursor->current()->toArray();
			if($bMutiple)
				$aReturn[] = $aTmp;
			else
				$aReturn = $aTmp;
		}
		return $aReturn;
	}
}