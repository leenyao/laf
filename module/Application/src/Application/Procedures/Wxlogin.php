<?php

namespace Application\Procedures;


class Wxlogin extends \Application\Procedures\Procedures
{
    /**
     * run get operation
     *
     * @param integer $aParam
     * @return string json string
     */
    public function go($aParam, $bCheckToken) {
		//init
		list($nCode, $aInit) = $this->_init($aParam, $this->sResource, $bCheckToken);
		if($nCode != 1)\YcheukfCommon\Lib\Functions::throwErrorByCode($nCode);
        $sWxInitPsd = 'wxpassword';
        $sWxUserName = "wx_".$aParam['params']["openid"];
        $aConfig = $this->sm->get('config');
        $nOauthClient = $aConfig['oauth_client_id']['wechat'];
        $nOauthScrect = "";

        $aData = $this->_getMysqlRecords('user', array('username'=>$sWxUserName));
        if(empty($aData)){
            $sCurrentTime = date("Y-m-d H:i:s");
            $aTmpRecord = array(
                'username' => $sWxUserName,
                'password' => md5($sWxInitPsd),
                'from_type' => 6,
                'modified' => $sCurrentTime,
            );
            $sOauthUserId = \YcheukfCommon\Lib\Functions::saveResource($this->sm, 'oauthuser', array($aTmpRecord));
            $aTmpRecord2 = array(
                'id' => $sOauthUserId, 
                'user_id' => $sOauthUserId, 
                'username' => $aTmpRecord['username'], 
                'password' => $aTmpRecord['password'], 
                'display_name' => $aParam['params']["nickname"], 
                'avatar' => $aParam['params']["headimgurl"], 
                'gender' => $aParam['params']["openid"], 
                'user_registered' => $sCurrentTime, 
                'm154_id' => 5, 
                'memo' => json_encode($aParam['params']), 
            );
            $sUserId = \YcheukfCommon\Lib\Functions::saveResource($this->sm, 'user', array($aTmpRecord2));
        
        }else{
        }
        $aResult = \YcheukfCommon\Lib\OauthClient::loginByParams(
			$nOauthClient, 
			$nOauthScrect, 
			$sWxUserName, 
			$sWxInitPsd, 
			'password', 
			"",
            null,
			"function",
            $this->sm
		);
        return ($aResult);
    }

}