<?php
namespace Application\Restapi;

class Common extends \YcheukfCommon\Lib\Restapi{
    function delete($sResource=null, $sToken="") {
        $this->_init($sResource, true);
        if(empty($sToken)){
            return $this->responce(500, "id can not be empty");
        }
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sm);
        $aParams = array(
            'op'=> 'save',
            'resource'=> $sResource,
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'dataset' => array (
                    array("status"=>2),
                ),
                'where'=>array('id'=>$sToken),
            ),
        );
        $aReturn = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
        if($aReturn['status'] == 1){
            return $this->responce(200, $aReturn['data']);
        }else{
            return $this->responce(500, json_encode($aReturn));
        }
    }
    function _postConfig($sResource=null, $sToken="", $aPostData) {
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sm);
        $aParams = array(
            'op'=> 'save',
            'resource'=> $sResource,
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'dataset' => array (
                    $aPostData,
                ),
            ),
        );
        if(!empty($sToken)){
            $aParams['params']['where'] = array('id'=>$sToken);
        }
        $aReturn = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
        if($aReturn['status'] == 1){
            return $this->responce(200, $aReturn['data']);
        }else{
            return $this->responce(500, json_encode($aReturn));
        }
    }
    function post($sResource=null, $sToken="") {
        $this->_init($sResource, true);
        $aPostData = json_decode($_POST['data'], 1);
        if(!isset($_POST['data']) || is_null($aPostData)){
            return $this->responce(500, "error param:".$_POST['data']);
        }
        $this->_postConfig($sResource, $sToken, $aPostData);
    }
    function put($sResource=null, $sToken="") {
        if(empty($sToken)){
            return $this->responce(500, "token can not be empty");
        }
        $putfp = fopen('php://input', 'r');
        $putdata = '';
        while($dataTmp = fread($putfp, 1024))
            $putdata .= $dataTmp;
        fclose($putfp);
        if(!empty($putdata)){
            parse_str($putdata);
            if(!isset($data)){
                return $this->responce(500, "can not get the param 'data'");
            }
            $aPostData = json_decode($data);
            if(!is_null($aPostData)){
                $this->_postConfig($sResource, $sToken, $aPostData);
            }else{
                return $this->responce(500, "put data error:".$data);
            }
        }else{
            return $this->responce(500, "put data can not be empty");
        }
    }
    function get($sResource=null, $sToken="") {
        $this->_init($sResource);
//        var_dump($this->sAccessToken);
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sm);
        $aParams = array(
            'op'=> 'get',
            'resource'=> $sResource,
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'select' => array (
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                ),
            ),
        );
        if(!empty($sToken)){
            $aParams['params']['select']['where'] = array('id'=>$sToken);
        }
        if(!empty($this->nPnOrder)){
            $aParams['params']['select']['order'] = $this->nPnOrder;
        }
//        var_dump($aParams);
        $aReturn = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
        if($aReturn['status'] == 1){
            return $this->responce(200, json_encode(!empty($sToken) ? $aReturn['data']['dataset'][0] : $aReturn['data']));
        }else{
            return $this->responce(500, json_encode($aReturn));
        }
    }
}
