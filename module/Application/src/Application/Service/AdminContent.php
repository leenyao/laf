<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class AdminContent implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function preSave($aForm, $sSource, $nCid, $nId){
        $sCurrentTime = date("Y-m-d H:i:s");
        switch($sSource){
            
            case 'system_batch_jobs'://若为用户, 则先增加 oauthuser
                 if (is_null($nId)) {
                    $aForm['op_user_id'] = $this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId();
                 }
                
            break;
            case 'profilebase'://修改密码
                    $sUserId = ($this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId());
                    $aForm['id'] = $sUserId;
                break;
            case 'profilepassword'://修改密码
                    $sUserId = ($this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId());
                    $aForm['id'] = $sUserId;
                    $aForm['password'] = md5($aForm['newpassword1']);
                    // var_dump($aForm);

                    
                    break;
            case 'user'://若为用户, 则先增加 oauthuser
                if(!empty($aForm['username'])){
                    $aUserData = is_null($nId) ? array() : \Application\Model\Common::getResourceById($this->serviceManager, $sSource, $nId);
                    $aTmpRecord = array(
                        'username' => $aForm['username'],
    //                    'password' => md5($aForm['password']),
                        'from_type' => 1,
                        'email' => $aForm['email'],
                        'phone' => $aForm['phone'],
                        'modified' => $sCurrentTime,
                    );
                    if(isset($aForm['password']) && !empty($aForm['password']))
                        $aTmpRecord['password'] = md5($aForm['password']);
                    if(isset($aUserData['user_id']) && !empty($aUserData['user_id']))
                        $aTmpRecord['id'] = $aUserData['user_id'];
    //                var_dump($aTmpRecord);
                    $sOauthUserId = \YcheukfCommon\Lib\Functions::saveResource($this->serviceManager, 'oauthuser', array($aTmpRecord));
                    $aForm['id'] = $aForm['user_id'] = $sOauthUserId;
                    if(!empty($aForm['password']))
                        $aForm['password'] = md5($aForm['password']);
                    else
                        unset($aForm['password']);
                    
                    if(is_null($nId)){
                        $aForm['user_registered'] = $sCurrentTime;
                    }
                    
                }
            break;
            case 'metadata'://若为用户的编辑
                if(!empty($nCid)){
                    $sMaxId = \Application\Model\Common::getMetadataMaxResId($this->serviceManager, $nCid);
                    if(is_null($nId)){
                        $aForm['resid'] = $sMaxId + 1;
                        $aForm['type'] = $nCid;
                    }
                }

                //一些不允许重新覆盖的值
                if (!empty($aForm['label'])) {
                    $nIdTmp = !empty($nId) ? $nId : 0;
                    // var_dump($nIdTmp);
                    $aRmetadata = \Application\Model\Common::getResourceById($this->serviceManager, 'metadata', $aForm['label'], 'label');
                    if (count($aRmetadata)) {


                        if ($aRmetadata['type']==$nCid && $aRmetadata['id'] != $nIdTmp) {// 意图覆盖, 制止

                            if (1026 == $aRmetadata['type']) {
                                $aParams = array(
                                    'op'=> 'save',
                                    'resource'=> 'metadata',
                                    'resource_type'=> 'common',
                                    'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
                                    'params' => array('dataset'=>array(array('status'=> 1)), 'where'=>array('id'=>$nIdTmp)),
                                );
                                \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');
                                
                            }else{
                                \YcheukfCommon\Lib\Functions::throwErrorByCode(5003, null, array("[=replacement1=]" => $aForm['label']." 已经存在"));
                            }

                        }
                    }
                }
            break;
            case 'userbander'://绑定用户
                if(isset($aForm['user_id']) && isset($aForm['bander_user_id'])){
                    $aTmpUserId = json_decode($aForm['user_id'], 1);
                    $aTmpBanderUserId = json_decode($aForm['bander_user_id'], 1);
                    $aForm['user_id'] = $aTmpUserId[0]['value'];
                    $aForm['bander_user_id'] = $aTmpBanderUserId[0]['value'];


                    $aParams = array(
                        'op'=> 'delete',
                        'resource'=> $sSource,
                        'resource_type'=> 'common',
                        'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
                        'params' => array(
                            'where' => array('user_id'=>$aForm['user_id'], 'bander_user_id'=>$aForm['bander_user_id']),
                        ),
                    );
                    $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');
                }
            break;
            case 'help':
                $tmp_pic = array();
                if(!empty($aForm['picture']))
                    $tmp_pic[] = $aForm['picture'];
                if(!empty($aForm['picture_1']))
                    $tmp_pic[] = $aForm['picture_1'];
                if(!empty($aForm['picture_2']))
                    $tmp_pic[] = $aForm['picture_2'];
                if(!empty($tmp_pic))
                    $aForm['picture'] = implode(',', $tmp_pic);
            break;
            case 'ads':
            case 'post':
                if(isset($aForm['timing_value']) && empty($aForm['timing_value']))
                    unset($aForm['timing_value']);
            break;
        }
        return $aForm;
    }
   public function afterSave($aForm, $sSource, $sSaveId, $nCid=null, $nFid=null){
        //处理metadata关系记录
        $aMetadataType = array();
        foreach($aForm as $k=>$v){
            if($k = \Application\Model\Common::getMatchsRelationType($k)){
                //处理第三方关系表

                $aTmp = $v;
                $k = str_replace('relation___', '', $k);
                $aTmp2 = explode("|", $k);
                $sRelationType = @$aTmp2[0]?:'';
                $sFid = @$aTmp2[1]?:'fid';
                $sCid = @$aTmp2[2]?:'cid';
                // var_dump('---',$sRelationType, $sSaveId, $aTmp);
                $sResourceTmp = @$aTmp2[3]?:'relation';
                $aParams = array(
                    'op'=> 'delete',
                    'resource'=> $sResourceTmp,
                    'resource_type'=> 'common',
                    'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
                    'params' => array(
                        "where" => array($sFid=>$sSaveId),
                    ),
                );
                if($sResourceTmp == 'relation'){
                    $aParams['params']["where"]["type"] = $sRelationType;
                }
//                var_dump($aParams);
                $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');


                if(count($aTmp)){
                    foreach($aTmp as $sIdTmp){
                        //处理新增的metadata关系
                        if(!empty($sIdTmp)){
                            $aRecord = array(
                                $sFid=>$sSaveId,
                                $sCid=>$sIdTmp
                            );
                            if($sResourceTmp == 'relation'){
                                $aRecord["type"] = $sRelationType;
                            }
                            $sId = \YcheukfCommon\Lib\Functions::saveResource($this->serviceManager, $sResourceTmp, array($aRecord));
                        }
                    }
                }
            }
        }
        if(strtolower(PHP_SAPI) == 'cli')return true;

        $roles = $this->serviceManager->get('ZfcRbac\Service\Rbac')->getIdentity()->getRoles();
        if($roles[0] == 'user-agent' && $sSource == 'company'){
            $sUserId = ($this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId());
            $all_companies = \Application\Model\Common::getResourceMetadaList($this->serviceManager,32,$sUserId,0,1);
            if(!in_array($sSaveId,$all_companies[$sUserId])){
                \Application\Model\Common::saveMetadata($this->serviceManager, 32, $sUserId, $sSaveId);
            }
        }

    }
}
