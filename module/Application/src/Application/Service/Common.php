<?php
namespace Application\Service;

use Zend\Config;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\RouteStackInterface as Router;
use Zend\Navigation\Exception;
use Zend\Navigation\Navigation;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * Abstract navigation factory
 */
class Common implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {
    }

    
    public function callAdminContentEditAction($nId=null,$nCid=null,$nFid=null,$sSource=null, $aPost=null, $nStep=null){
        $oContentController = $this->serviceManager->get('controllerloader')->get('Application\Controller\AdminContentController');
        $sActionReturn = $oContentController->contenteditAction($nId,$nCid,$nFid,$sSource, $aPost, $nStep, '3rdcall');
        // var_dump($sActionReturn);


        return $sActionReturn;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /** 当前用户信息 */
    public function getCurrentUserInfo()
    {

        $sUserId = $this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId();
        $aRuser = \Application\Model\Common::getResourceById($this->serviceManager, 'user', $sUserId);
        return $aRuser;

    }

    /** 当前用户信息 */
    public function getCurrentProfile()
    {


        $getCurrentUserInfo = $this->getCurrentUserInfo();
        $aRrole = \Application\Model\Common::getResourceById($this->serviceManager, 'rbac_role', $getCurrentUserInfo["role_id"]);

        $aProfile = array(
            "base" => array(
                "id" => $getCurrentUserInfo["id"],
                "username" => $getCurrentUserInfo["username"],
                "display_name" => $getCurrentUserInfo["display_name"],
                "user_registered" => $getCurrentUserInfo["user_registered"],
            ),
            "role" => array(
                "role_id" => $getCurrentUserInfo["role_id"],
                "role_label" => $aRrole["role_name"],
            ),
        );
        return $aProfile;
    }
}
