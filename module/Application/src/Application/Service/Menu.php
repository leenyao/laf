<?php
namespace Application\Service;

use Zend\Config;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\RouteStackInterface as Router;
use Zend\Navigation\Exception;
use Zend\Navigation\Navigation;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * Abstract navigation factory
 */
class Menu implements ServiceManagerAwareInterface
{

    /**
     * @var ServiceLocatorInterface $serviceLocator
     */
    public $serviceLocator;

    /**
     * @var array
     */
    public $pages;

    /**
     * @var array
     */
    public $name='default';

    public $userid='';

    
    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceLocator = $serviceManager;
    }
    /**
     * @return \Zend\Navigation\Navigation
     */
    public function getNavigation()
    {
        $pages = $this->getPages();
        return new Navigation($pages);
    }

    public function setName($sName)
    {
        $this->name = $sName;
    }


    public function setUserId($userid)
    {
        $this->userid = $userid;
    }



    /**
     * @return string
     */
    protected function getName()
    {
        return $this->name;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return array
     * @throws \Zend\Navigation\Exception\InvalidArgumentException
     */
    protected function getPages()
    {
        if (null === $this->pages) {
            $configuration = $this->serviceLocator->get('Config');

            if (!isset($configuration['navigation'])) {
                throw new Exception\InvalidArgumentException(
                    'Could not find navigation configuration key'
                );
            }
            if (!isset($configuration['navigation'][$this->getName()])) {
                throw new Exception\InvalidArgumentException(
                    sprintf(
                        'Failed to find a navigation container by the'
                        .' name "%s"',
                        $this->getName()
                    )
                );
            }

            $application = $this->serviceLocator->get('Application');
            $routeMatch = $application->getMvcEvent()->getRouteMatch();
            $router = $application->getMvcEvent()->getRouter();

            $aSpecialNav = $configuration['navigation'][$this->getName()];
            $aSpecialNav[] = $configuration['generation_userprofile'];
            // var_dump($aSpecialNav);
            // exit;

            $pages = $this->getPagesFromConfig(
                $aSpecialNav
            );
            $pages = $this->getNavRecursion($pages);
//            var_dump($pages);
            $this->pages = $this
            ->injectComponents($pages, $routeMatch, $router);
        }
        return $this->pages;
    }

    /**
     * @param string|\Zend\Config\Config|array $config
     * @return array|null|\Zend\Config\Config
     * @throws \Zend\Navigation\Exception\InvalidArgumentException
     */
    protected function getPagesFromConfig($config = null)
    {
        if (is_string($config)) {
            if (file_exists($config)) {
                $config = Config\Factory::fromFile($config);
            } else {
                throw new Exception\InvalidArgumentException(
                    sprintf(
                        'Config was a string but file "%s" does not exist',
                        $config
                    )
                );
            }
        } elseif ($config instanceof Config\Config) {
            $config = $config->toArray();
        } elseif (!is_array($config)) {
            throw new Exception\InvalidArgumentException(
                'Invalid input, expected array, filename, or Zend\Config object'
            );
        }

        return $config;
    }

    /**
     * @param array $pages
     * @param RouteMatch $routeMatch
     * @param Router $router
     * @return mixed
     */
    protected function injectComponents(
        array $pages,
        RouteMatch $routeMatch = null,
        Router $router = null)
    {
        foreach ($pages as &$page) {
            $hasMvc = isset($page['action']) || isset($page['controller'])
                || isset($page['route']);
            if ($hasMvc) {
                if (!isset($page['routeMatch']) && $routeMatch) {
                    $page['routeMatch'] = $routeMatch;
                }
                if (!isset($page['router'])) {
                    $page['router'] = $router;
                }
            }

            if (isset($page['pages'])) {
                $page['pages'] = $this
                ->injectComponents($page['pages'], $routeMatch, $router);
            }
        }
        return $pages;
    }

    //递归配置permission
    function getNavRecursion($aNav){
    //    $aNav['permission'] = (isset($aNav['permission'])) ? $aNav['permission'] : array();
        if(is_array($aNav) && count($aNav)){
    //        var_dump($aNav);
            foreach($aNav as $sKey=>$aTmp){

                if (is_callable($aTmp)) {
                    $aTmp = $aTmp($this->serviceLocator);
                }

                $aTmp['permission'] = $this->getPermisionFromNav($aTmp);
                if(isset($aTmp['pages']) && count($aTmp['pages'])){
                    $aTmp['pages'] = $this->getNavRecursion($aTmp['pages']);//递归子菜单权限
                    foreach($aTmp['pages'] as $aTmp2){//将子菜单的权限赋给父菜单
                        $aTmp['permission'] = array_merge($aTmp['permission'], $aTmp2['permission']);
                    }
                }
                $aTmp['permission'] = array_filter($aTmp['permission']);
                $aTmp['permission'] = array_unique($aTmp['permission']);
                $aNav[$sKey] = $aTmp;
            }
        }
        return $aNav;
    }

    //根据菜单栏的配置获取权限名
    function getPermisionFromNav($aTmp){
        $aPermision = isset($aTmp['permission']) ? $aTmp['permission'] : array();
        $sTmp = "";
        if(isset($aTmp['route'])){
            $sTmp = $aTmp['route'];
            if(isset($aTmp['params']) && count($aTmp['params']) && in_array($aTmp['route'], array('zfcadmin/contentlist', 'zfcadmin/contentedit', 'zfcadmin/contentadd', 'zfcadmin/report'))){
    //            var_dump($aTmp['params']);
                foreach($aTmp['params'] as $sParam)
                    $sTmp .= "/".$sParam;
            }
            $aPermision[] = \YcheukfCommon\Lib\Functions::replacePermission($sTmp);
        }
        return $aPermision;
    }
}
