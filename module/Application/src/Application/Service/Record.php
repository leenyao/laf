<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Record implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function _getIds($sResource, $aWhere){
        $aIds = array();
        if(!isset($aWhere['id'])){
            $aParams = array(
                'op'=> 'get',
                'resource'=> $sResource,
                'resource_type'=> 'common',
                'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
                'params' => array( 
                    "columns" => "id",
                    'select' => array ( 
                        "where" => $aWhere,
                    ),
                ),
            );
            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');
            if($aResult['data']['count']){
                $aIns = array();
                foreach($aResult['data']['dataset'] as $row){
                    $aIds[] = $row['id'];
                }
            }
        }else{
            if(isset($aWhere['id']['$in']) && is_array($aWhere['id']['$in']))
                $aIds = $aWhere['id']['$in'];
            else
                $aIds = array($aWhere['id']);
        }
        return $aIds;
    }


    /**
     * @param MvcEvent $e
     */
    public function onUpdate(\Zend\EventManager\Event $e){
        list($sResource, $aData, $aWhere) = $e->getParams();
        \YcheukfCommon\Lib\Functions::debug($e->getParams(), "[inline]---[event]---".__FUNCTION__);
        $aIds = $this->_getIds($sResource, $aWhere);


        if(count($aIds)){
            foreach($aIds as $sIdTmp){
                //触发更新数据
                $sFunc = "update_".$sResource;
                if(method_exists($this, $sFunc) ){
                    eval("\$this->".$sFunc."(\$sResource, \$aData, \$sIdTmp);");

                }
                $this->_updCache($sResource, $sIdTmp, $aData);

            }
        }
    }
    public function onInsert(\Zend\EventManager\Event $e)
    {
        list($sResource, $aData, $sId) = $e->getParams();
        \YcheukfCommon\Lib\Functions::debug($e->getParams(), "[inline]---[event]---".__FUNCTION__);

        //触发更新数据
        $sFunc = "insert_".$sResource;
        if(method_exists($this, $sFunc)){
            eval("\$this->".$sFunc."(\$sResource, \$aData, \$sId);");
        }
        $this->_updCache($sResource, $sId, $aData);

    }

    private function _updCache($sResource, $sId, $aData)
    {
        // 删除所有由 \YcheukfCommon\Lib\Functions::::getResourceById 产生的cache
        \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, array(LAF_CACHE_GETRESOURCEBYID_PRE."-".$sResource."-".$sId), true);


        // 删除所有由 \YcheukfCommon\Lib\Functions::::getResourceMetaData 产生的cache
        if ($sResource == "metadata") {
            $aResult = \YcheukfCommon\Lib\Functions::getResourceById($this->serviceManager, "metadata", $sId);//读取用户id与权限id关系
            if (count($aResult)) {
                \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, array(LAF_CACHE_GETRESOURCEMETADATA_PRE."-".$sResource."-".$aResult['type']), true);
            }
        }

        // 删除所有由 \YcheukfCommon\Lib\Functions::::getRelationList 产生的cache
        if ($sResource == "relation") {
            $aResult = \YcheukfCommon\Lib\Functions::getResourceById($this->serviceManager, "relation", $sId);//读取用户id与权限id关系

            if (count($aResult)) {
                \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, array(LAF_CACHE_GETRELATIONLIST_PRE."-".$aResult['type']."-".$aResult['fid']), true);
                \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, array(LAF_CACHE_GETRELATIONLIST_PRE."-".$aResult['type']."-".$aResult['cid']), true);
            }
        }



    }


    public function update_user($sResource, $aData, $sId){
//        var_dump(__CLASS__.":".__FUNCTION__);
    }

}
