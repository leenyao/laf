<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class User implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function afterAuthenticate(\Zend\EventManager\Event $e){
        \YcheukfCommon\Lib\Functions::debug($e->getParams(), "[inline]---[event]---afterAuthenticate");
    }


}
