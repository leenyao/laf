<?php
namespace Application\Toolbar;

class Cacheadmin extends Toolbar{
    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
		$aReturn = array();
	
		//下载
		$aReturn[] = array(
			'type' => 'detail-button-action',
			'cls' => 'publish_clear_cache',
			'confirm' => "清空选中缓存",
			'position' => 'left',
			'iconCls' => 'licon-publish_clear_cache',
			'params' => array(
				'label' => $oController->translate('清空选中缓存'),
				'action-url' => $oController->url()->fromRoute('zfcadmin/clearcache'),
			),
		);
		$aReturn[] = array(
			'type' => 'button-add',
			'position' => 'left',
			'cls' => 'clearallcache',
			'iconCls' => 'licon-clearallcache',
			'params' => array(
				'label' => '清空所有缓存',
				'url' => $oController->url()->fromRoute('zfcadmin/clearallcache'),
			),
			'permission' => array(
//				'zfcadmin/system/clearallcache',
			),
		);
		$aReturn[] = array(
			'type' => 'detail-br',
		);


		$aReturn[] = array(
			'type' => 'detail-searchbar',
			'position' => 'right',
			'params' => array(
				'label' => $oController->translate('搜索'),
				'tips' => 'fill chart plz',
				'object_name' => 'q_keyword',
			),
		);
		$aReturn = $this->_fmtPermission($sm, $aReturn);
		return $aReturn;
	}
}