<?php
namespace Application\Toolbar;
class Rbacrole extends Toolbar{

    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
		$aReturn = array();

		//新增按钮
		$aReturn[] = $this->_getAddButton($oController, $sSource, $nId);


		//下载
		$aReturn[] = array(
			'type' => 'detail-button-download',
			'cls' => 'download',
			'iconCls' => 'licon-download',
			'position' => 'left',
			'params' => array(
				'request' => array('returnType'=>'csv_all'),
				'label' => $oController->translate('download'),
			),
		);

		$aReturn[] = array(
			'type' => 'detail-br',
		);




		$aReturn[] = array(
			'type' => 'detail-searchbar',
			'position' => 'right',
			'params' => array(
				'label' => $oController->translate('搜索'),
				'tips' => 'fill chart plz',
				'object_name' => 'q_keyword',
			),
		);

		$aReturn = $this->_fmtPermission($sm, $aReturn);
		return $aReturn;

	}
	
}