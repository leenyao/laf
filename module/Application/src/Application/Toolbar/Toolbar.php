<?php
namespace Application\Toolbar;

class Toolbar{
    var $sm;
    var $oController;
    var $sSource;
    var $nEntityId;
    var $nFatherId;

    public function __construct($oController, $sm, $sSource, $nEntityId, $nFatherId){
        $this->sm = $sm;
        $this->oController = $oController;
        $this->sSource = $sSource;
        $this->nEntityId = $nEntityId;
        $this->nFatherId = $nFatherId;
    }

    function __get($name='')
    {
        switch ($name) {
            case 'serviceManager':
                return $this->sm;
                break;
            default:
                break;
        }
    }


    public function getMetaFilter($nTypeTmp, $aAllowIds=array(), $bMutiple=false) 
    {
        $aQeury = isset($_GET['q']) ? json_decode($_GET['q'], 1) : array();

        $sLabel = $this->sSource."__split__m".$nTypeTmp."_id";

        $sSelectedValue = isset($aQeury['m'.$nTypeTmp.'_id']) ? $aQeury['m'.$nTypeTmp.'_id'] : 0;

        $aStatus = \Application\Model\Common::getResourceMetadaList($this->serviceManager, $nTypeTmp);

        if (count($aAllowIds)) {
            foreach ($aStatus as $key => $value) {
                if (!in_array($key, $aAllowIds)) {
                    unset($aStatus[$key]);
                }
            }
        }

        $aStatus =  array(0 =>$this->serviceManager->get('translator')->translate($sLabel)."-不过滤") + $aStatus;
        $sObjectName = \Application\Model\Common::getFieldBySource('m'.$nTypeTmp.'_id', $this->sSource);
        return array(
            'type' => 'detail-select-action',
            'cls' => 'filter',
            'position' => 'left',
            'params' => array(
                'bMutiple' => $bMutiple,
                'label' => $this->serviceManager->get('translator')->translate('过滤状态'),
                'object_name' => $sObjectName,
                'options' => $aStatus,
                'value' => $sSelectedValue,
            ),
        );
    }

    
    public function fmtOutputJson($aToolbarConfig){
        $aReturn = array();

        if (is_array($aToolbarConfig) && count($aToolbarConfig)) {
            foreach ($aToolbarConfig as $sKtmp1 => $aRowTmp1) {
                switch ($aRowTmp1["type"]) {
                    case 'detail-select-action':
                        if (isset($aRowTmp1['params']) && isset($aRowTmp1['params']['options'])) {

                            if (is_array($aRowTmp1['params']['options']) && count($aRowTmp1['params']['options'])) {
                                $aNewOptions = array();
                                foreach ($aRowTmp1['params']['options'] as $sKtmp2 => $aRowTmp2) {
                                    $aNewOptions["___REPLACEBLANK___".$sKtmp2] = $aRowTmp2;
                                }
                                $aRowTmp1['params']['options'] = $aNewOptions;

                            }
                        }
                        break;
                    default:
                        break;
                }
                $aToolbarConfig[$sKtmp1] = $aRowTmp1;
            }
        }
        return ($aToolbarConfig);
    }

    public function _getAddButton($oController, $sSource, $nId){
        $aReturn = array(
            'type' => 'button-add',
            'position' => 'left',
            'cls' => 'add',
            'iconCls' => 'licon-add',
            'params' => array(
                'label' => '新增',
                'url-route' => array('zfcadmin/contentadd', array('type'=>$sSource, 'cid'=>$nId)),
            ),
            'permission' => array(),
        );
        return $this->_fmtUrl($oController, $aReturn, "url");
    }

    public function _fmtUrl($oController, $aReturn, $urlKey){
        if(isset($aReturn['params']['url-route']))
            $aReturn['params'][$urlKey] = $oController->url()->fromRoute($aReturn['params']['url-route'][0], $aReturn['params']['url-route'][1]).(isset($aReturn['params']['url-route'][2]) ? $aReturn['params']['url-route'][2]:"");
        return $aReturn;
    }
    public function _getPublishButton($sm, $oController, $nTypeTmp, $sSource, $nStatus, $sPermission=null, $sStatusField='status', $sLabel=null){
        $aMetadata = \Application\Model\Common::getResourceMetadaList($sm, $nTypeTmp);
        $aReturn = array(
            'type' => 'detail-button-action',
            'cls' => 'publish_'.$nTypeTmp."_".$nStatus,
            'confirm' => $sm->get('translator')->translate('set_status_'.$sSource."_".$nTypeTmp."_".$nStatus),
            'position' => 'left',
            'iconCls' => 'licon-publish_'.$nTypeTmp."_".$nStatus,
            'params' => array(
                'label' => $oController->translate(!is_null($sLabel) ? $sLabel : $aMetadata[$nStatus]),
                'url-route' => array('zfcadmin/contentsetstatus', array('type'=>$sSource,'status'=>$nStatus, 'fieldname'=>$sStatusField)),
            ),
            'permission' => array(),
        );
        if(!is_null($sPermission))
            $aReturn['permission'][] = $sPermission;
        return $this->_fmtUrl($oController, $aReturn, "action-url");
    }

    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
        $aReturn = array();

        //新增按钮
        $aReturn[] = $this->_getAddButton($oController, $sSource, $nId);


        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('批量操作:'),
            ),
        );
        $nTypeTmp = 102;
        //发布按钮
        $aReturn[] = $this->_getPublishButton($sm, $oController, $nTypeTmp, $sSource, 1);

        //暂停发布按钮
        $aReturn[] = $this->_getPublishButton($sm, $oController, $nTypeTmp, $sSource, 2);

        //删除按钮
        $aReturn[] = $this->_getPublishButton($sm, $oController, $nTypeTmp, $sSource, 3);


        //下载
        $aReturn[] = array(
            'type' => 'detail-button-download',
            'cls' => 'download',
            'iconCls' => 'licon-download',
            'position' => 'left',
            'params' => array(
                'request' => array('returnType'=>'csv_all'),
                'label' => $oController->translate('download'),
            ),
        );

        $aReturn[] = array(
            'type' => 'detail-br',
        );



        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('状态:'),
            ),
        );
        $aStatus = \Application\Model\Common::getResourceMetadaList($sm, 102);
        $aStatus =  array(0 =>$oController->translate("all")) + $aStatus;
        $sObjectName = \Application\Model\Common::getFieldBySource('status', $sSource);
        $aReturn[] = array(
            'type' => 'detail-select-action',
            'cls' => 'filter',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('过滤状态'),
                'object_name' => $sObjectName,
                'options' => $aStatus,
            ),
        );


        $aReturn[] = array(
            'type' => 'detail-searchbar',
            'position' => 'right',
            'params' => array(
                'label' => $oController->translate('搜索'),
                'tips' => 'fill chart plz',
                'object_name' => 'q_keyword',
            ),
        );

        $aReturn = $this->_fmtPermission($sm, $aReturn);
        return $aReturn;

    }
    function _fmtPermission($sm, $aData){
        $aReturn = array();
        foreach($aData as $row){
            if(isset($row['params']['url-route']) && count($row['params']['url-route'])){
                $sPermission = $row['params']['url-route'][0]."/".join("/", array_values($row['params']['url-route'][1]));
                $row['permission'] = $sPermission;
            }
            if(isset($row['permission']) && count($row['permission'])){
//                var_dump($row['permission']);
                $bFlagTmp = \Application\Model\Common::isPermissionGranted($sm,    $row['permission']);
                if(!$bFlagTmp)continue;
            }
            if(isset($row['confirm']) && preg_match('/[a-zA-Z0-9_]+/i', $row['confirm'])){
                $row['confirm'] = $sm->get('translator')->translate('set_status_confirm');
            }
//            echo $row['confirm'];
            $aReturn[] = $row;
        }
        return $aReturn;
    }
}