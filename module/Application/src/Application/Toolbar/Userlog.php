<?php
namespace Application\Toolbar;
class Userlog extends \Application\Toolbar\Toolbar{
    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
        $aReturn = array();

        //下载
        $aReturn[] = array(
            'type' => 'detail-button-download',
            'cls' => 'download',
            'iconCls' => 'licon-download',
            'position' => 'left',
            'params' => array(
                'request' => array('returnType'=>'csv_all'),
                'label' => $oController->translate('download'),
            ),
        );

        $aReturn[] = array(
            'type' => 'detail-br',
        );



        // 提供 metadata=101的过滤
        // $aReturn[] = $this->getMetaFilter(101);

        $aReturn[] = array(
            'type' => 'detail-searchbar',
            'position' => 'right',
            'params' => array(
                'label' => $oController->translate('搜索'),
                'tips' => 'fill chart plz',

                'placeholder' => '模糊搜索关键字',
                'object_name' => 'q_keyword',
            ),
        );

        $aReturn = $this->_fmtPermission($sm, $aReturn);
        return $aReturn;

    }
}