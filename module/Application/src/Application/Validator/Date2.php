<?php
/**
 * 检查 2010-11-22 格式
 */


namespace Application\Validator;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;


class Date2 extends \Zend\Validator\AbstractValidator
{



    protected $messageTemplates = array(
        "invalid"   => "错误的时间格式,支持 YYYY-mm-dd ",
    );
    /**
     * Returns true if and only if $value validates as a Uri
     *
     * @param  string $value
     * @return bool
     */
    public function isValid($value)
    {

        if (preg_match("/^\d{4}-\d{2}-\d{2}$/i", $value) === false) {
            $this->error("invalid");
            return false;
        }
        if (strtotime($value) === false) {
            $this->error("invalid");
            return false;
        }
        return true;

    }
}
