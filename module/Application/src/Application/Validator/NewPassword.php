<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */


namespace Application\Validator;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;


class NewPassword extends \Zend\Validator\AbstractValidator
{



    protected $messageTemplates = array(
        "passwordwrong"   => "two password is not the same",
    );
    /**
     * Returns true if and only if $value validates as a Uri
     *
     * @param  string $value
     * @return bool
     */
    public function isValid($value)
    {

        if ($this->getOption("newpassword2") === $this->getOption("newpassword1")) {
            // $this->error(self::INVALID);
            return true;
        }
        $this->error("passwordwrong");
        return false;

    }
}
