<?php
/**
 * 检查 2010-11-22 格式
 */


namespace Application\Validator;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;


class NotEmpty extends \Zend\Validator\AbstractValidator
{



    protected $messageTemplates = array(
        "invalid"   => "不能为空",
    );
    /**
     * Returns true if and only if $value validates as a Uri
     *
     * @param  string $value
     * @return bool
     */
    public function isValid($value)
    {
        $value = trim($value);

        if (empty($value)) {
            $this->error("invalid");
            return false;
        }
        return true;

    }
}
