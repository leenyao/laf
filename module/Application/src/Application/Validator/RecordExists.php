<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */


namespace Application\Validator;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;


class RecordExists extends \Zend\Validator\AbstractValidator
{

    var $serviceManager;

    protected $messageTemplates = array(
        "RecordExists"   => "this record is alread exists",
    );

    public function setService($sm)
    {
        $this->serviceManager=$sm;
    }

    /**
     * Returns true if and only if $value validates as a Uri
     *
     * @param  string $value
     * @return bool
     */
    public function isValid($value)
    {
        $aRresouce = \Application\Model\Common::getResourceById($this->serviceManager, $this->getOption("resource"), $value, $this->getOption("keyfield"));

        if ($aRresouce && count($aRresouce)) {
            // $this->error(self::INVALID);
            $this->error("RecordExists");
            return false;
        }
        return true;

    }
}


