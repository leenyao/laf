<?php
namespace Application\YcheukfReportExt\Inputs;

class Cacheadmin extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$oSm = $this->getSm();
		$sReturnType = $this->getReturnType();
//		$aAdvanceData = $this->getAdvanceData();
		$nCid = $this->getCid();
		$verify_status = $this->getFid();
		$cParams = array();
		$aParams = isset($aReportParams['params']) ? json_decode($aReportParams['params'], 1) : array();
		$current_page = isset($aParams['current_page']) ? $aParams['current_page'] : 0;
		$current_page++;
		$items_per_page = isset($aParams['items_per_page']) ? $aParams['items_per_page'] : 10;

		$aConfig = $oSm->get('Config');
		$aAdvanceData = array();
		$aCacheAllowKeys = \YcheukfCommon\Lib\Functions::getAllCacheKeys($oSm);
		$aFilters = isset($aReportParams['params']) ? json_decode($aReportParams['params'], 1) : array();
		$sKeyWord = isset($aFilters['q_keyword']) ? $aFilters['q_keyword'] : "";
		if(!empty($sKeyWord)){//过滤
			$aTmp = array();
			foreach($aCacheAllowKeys as $k=>$row){
				if(preg_match("/".str_replace("/", "\/", $sKeyWord)."/i", $k)){
					$aTmp[$k] = $row;
				}
			}
			$aCacheAllowKeys = $aTmp;
		}

//		var_dump($current_page);
		$nIndex = 0;
		$nStart = ($nIndex-1) * $items_per_page;
		$nEnd = ($nIndex) * $items_per_page;
//			var_dump($aCacheAllowKeys);
		foreach($aCacheAllowKeys as $k=>$row){
			$nIndex++;
			if($nIndex < ($current_page-1)*$items_per_page)continue;
			if(count($aAdvanceData) >= $items_per_page)break; //到达展示数
			$aAdvanceData[] = array(
				'cache_key' => $k,
				'cache_keymemo' => $row,
				'cache_lifetime' => $row,
			);
		}
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件.
			),
			'input'=>array(
				'detail'=>array(
					'page' => array(
//						'items_per_page' => 20,
//						'current_page' => 0,
						'total' => count($aCacheAllowKeys),
					),
					'type' =>'table',
					'selected' => 'pageView',
					'orderby' =>'id asc',
					'advanced' => array(
						'type' => 'datas',
						'data' => array($aAdvanceData),
						'dimen' => array(
							'cache_key',
							'cache_keymemo',
							'cache_lifetime',
						),
						'metric' => array(
						),
					),
				),
			),
			'output' => array(
				'format' => 'html',
			),
			'custom' => array(
				'sDatasource' => $sSource,
			),
		);

		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('status',), array());
		$aInput = $this->_formatOutput($aInput);

		return $aInput;

	}

}