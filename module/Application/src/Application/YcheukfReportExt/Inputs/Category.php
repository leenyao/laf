<?php
namespace Application\YcheukfReportExt\Inputs;

class Category extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$nCid = $this->getCid();
		$sReturnType = $this->getReturnType();


		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
				array(
					'key' => 'purview_type', //操作符
					'op' => 'in', //操作符=,in,like
					'value' => "1,2,3"//过滤的值
				),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								array(
									 'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>true,
								),
								array(
									 'key' => 'title', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td200'),
								),
//										'purview_type',
								array(
									 'key' => 'logo', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td200'),
								),
								array(
									 'key' => 'modified', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array(),
								),
//								'logo',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $sReturnType,
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('status', ), array('title'));
		$aInput = $this->_formatOutput($aInput);



		return $aInput;
		
	}

}