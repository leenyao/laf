<?php
namespace Application\YcheukfReportExt\Inputs;

class Company extends Inputs{
    function getInput(){
        $sSource = $this->getResource();
        $aReportParams = $this->getReportParams();
        $nCid = $this->getCid();
        $oSm = $this->getSm();
        $sReturnType = $this->getReturnType();

                
        $roles = $oSm->get('ZfcRbac\Service\Rbac')->getIdentity()->getRoles();
        $isAgent = false;
        if($roles[0] == 'user-agent'){
            $isAgent = true;
        }

        $sTitle = $sSource.'___split___title';
        $sAddress = $sSource.'___split___address';
        $aInput = array(
            'filters' => array( //选传. 默认为空, 过滤条件. 
                array(
                    'key' => 'hot_flag', //操作符
                    'op' => '=', //操作符=,in,like
                    'value' => 2//过滤的值
                ),
                array(
                    'key' => 'uid', //操作符
                    'op' => '<', //操作符=,in,like
                    'value' => 1//过滤的值
                ),
            ),
            'input'=>array(
                'detail'=>array(
                    'type' =>'table',
                    'orderby' =>'modified desc',
                    'table' => array(
                        $sSource => array(
                            'dimen' => array(
                                array(
                                     'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
                                    'group'=>true,
                                ),
                                array('key' => 'sort',
                                     'group'=> !$isAgent,),
                                array(
                                     'key' => 'm156_id', //必传. 某个维度的键值, 可直接用表中的字段名
                                    'group'=> !$isAgent,
                                ),
                                array(
                                     'key' => 'task_count', //必传. 某个维度的键值, 可直接用表中的字段名
                                ),
                                array('key' => 'status',
                                     'group'=> !$isAgent,),
                                array(
                                     'key' => 'm148_id', //必传. 某个维度的键值, 可直接用表中的字段名
                                ),
                                array(
                                     'key' => 'hot_flag', //必传. 某个维度的键值, 可直接用表中的字段名
                                    'group'=>false,
                                ),
                                array(
                                     'key' => 'uid', //必传. 某个维度的键值, 可直接用表中的字段名
                                    'group'=>false,
                                ),
                                array(
                                     'key' => $sTitle, //必传. 某个维度的键值, 可直接用表中的字段名
                                    'thclass'=>array('td200'),
                                ),
                                array(
                                     'key' => 'user_id', //必传. 某个维度的键值, 可直接用表中的字段名
                                    'group'=>!$isAgent,
                                ),
                                array(
                                     'key' => $sAddress, //必传. 某个维度的键值, 可直接用表中的字段名
                                    'thclass'=>array('td300'),
                                ),
//                                $sTitle, //必传. 某个维度的键值, 可直接用表中的字段名
//                                $sAddress, //必传. 某个维度的键值, 可直接用表中的字段名
//                                'title',
//                                'address',
                                array(
                                     'key' => 'modified', //必传. 某个维度的键值, 可直接用表中的字段名
                                    'thclass'=>array(),
                                ),
//                                'logo',
                            ),
                        ),
                    ),
                ),
            ),
            'output' => array(
                'format' => $sReturnType,
            ),
        );

        if($isAgent){
            $user = \Application\Model\Common::_get_api_content($oSm, 'user'); 
            if(isset($user['data']['m32_id'])){
                $aInput['filters'][] = array(
                    'key' => $sSource.'___id', //操作符
                    'op' => 'in', //操作符=,in,like
                    'value' => implode(',', $user['data']['m32_id'])
                );
            }
        }
        
        $aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('status' , 'm156_id', 'm148_id'), array($sTitle, $sAddress));
        $aInput = $this->_formatOutput($aInput);


        return $aInput;
        
    }

}