<?php
namespace Application\YcheukfReportExt\Inputs;

class Contentblock extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$nCid = $this->getCid();
		$sReturnType = $this->getReturnType();

//var_dump($nCid);
		$aInput = array(
			'filters' => array(
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								array(
									'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>true,
								    'thclass'=>array("td100"),
								),
								array(
									'key' => 'name', //必传. 某个维度的键值, 可直接用表中的字段名
								    'thclass'=>array("td200"),
								)
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $sReturnType,
			),
		);
		//$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('status'), array('title'));

		$aInput = $this->_formatOutput($aInput);
		return $aInput;

	}

}