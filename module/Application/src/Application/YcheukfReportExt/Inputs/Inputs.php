<?php
namespace Application\YcheukfReportExt\Inputs;

class Inputs{
	var $sSource;
	var $nCid;
	var $nFid;
	var $sM;
	var $sReturnType;
        var $sCustomParams;
	var $aReportParams;
	var $aAdvanceData;
	var $ExportChaset=null;
	var $Customfields=null;
	var $splitChar = "__split__";

	public function translate($value='')
	{
		return $this->serviceManager->get('translator')->translate($value);
	}

	public function _fmtDetailFilters($sSource, $aInput, $aReportParams, $aFilterFields=array(), $aFulltextField=array(), $aDefaultFilter=array()){

		if (!in_array($sSource.'___id', $aFilterFields)) {
			$aFilterFields[] = $sSource.'___id';
		}
//		if(count($aFilterFields) < 1)return $aInput;
		$aFilters = isset($aReportParams['params']) ? json_decode($aReportParams['params'], 1) : array();

		// 处理外键
		$oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"); //db_master|db_slave
			

		if (is_array($aFilters) && count($aFilters)) {
			$aFkFilters = array();
		    foreach ($aFilters as $sKtmp1 => $aRowTmp1) {
		    	if (empty($aRowTmp1)) {
		    		continue;
		    	}
		        if (preg_match("/^__fk__\d+$/", $sKtmp1)) {
		        	$sTmp1 = \Application\Model\Common::encrypt($aRowTmp1, "D");
		        	list($sFieldKeyTmp, $sSqlTmp) = explode(";;", $sTmp1);

		        	$oSth = $oPdo->prepare($sSqlTmp);
		        	$oSth->execute();
		        	$aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
		// echo ($sSqlTmp."\n\n");
		// var_dump($aResultTmp);

		        	if (count($aResultTmp)) {
			        	foreach ($aResultTmp as $aRowTmp3) {
			        		$aFkFilters[$sFieldKeyTmp][] = $aRowTmp3['idfield'];
			        	}
		        	}else{
		        		$aFkFilters[$sFieldKeyTmp] = array();
		        	}
		        }
		    }

		    
		    if (is_array($aFkFilters) && count($aFkFilters)) {
		        foreach ($aFkFilters as $sKtmp1 => $aRowTmp1) {
		            $aInput['filters'][] = array(
		            		'key' => $sKtmp1, //操作符
		            		'op' => 'in', //操作符=,in,like
		            		'value' => "(".join(",", $aRowTmp1).")",//过滤的值
		            );
		        }
		    }
		    
		}
			
			// var_dump(($aFilters['b_ghr_members___id']));

		if(count($aFilterFields)){
			foreach($aFilterFields as $sKeyName){
				$sKeyName = \Application\Model\Common::getFieldBySource($sKeyName, $sSource);
			// var_dump($sKeyName);
			// var_dump($aFilters);
				
				if(isset($aFilters[$sKeyName])){//存在过滤项
					$aFilters[$sKeyName] = str_replace("___REPLACEBLANK___", "", $aFilters[$sKeyName]);
					if(!empty($aFilters[$sKeyName])){

						switch($sKeyName){
							default:

							
							if (is_array($aFilters[$sKeyName]) && count($aFilters[$sKeyName])) {
								$aFilters[$sKeyName] = array_filter($aFilters[$sKeyName]);
								if(count($aFilters[$sKeyName])){
									$aInput['filters'][] = array(
											'key' => $sKeyName, //操作符
											'op' => 'in', //操作符=,in,like
											'value' => join(',', $aFilters[$sKeyName]),//过滤的值
									);
								}

							}else{
								$aInput['filters'][] = array(
										'key' => $sKeyName, //操作符
										'op' => '=', //操作符=,in,like
										'value' => $aFilters[$sKeyName],//过滤的值
								);
							}
							break;
						}
					}
				}elseif (isset($aDefaultFilter[$sKeyName])) {//存在默认过滤项

					$aInput['filters'][] = array(
							'key' => $sKeyName, //操作符
							'op' => $aDefaultFilter[$sKeyName]['op'], //操作符=,in,like
							'value' => $aDefaultFilter[$sKeyName]['value'],//过滤的值
					);
				}
			}
		}
		// var_dump($aInput['filters']);

//		var_dump($aFilters['q_keyword']);
		$bFulltextSearchFlag = true;
		if(isset($aFilters['q_keyword']) && !empty($aFilters['q_keyword'])){
			$sQWord = trim($aFilters['q_keyword']);

			// if (preg_match("/^id:\d+$/i", strtolower($sQWord)) || preg_match("/^\d+$/i", strtolower($sQWord))) {
			if (preg_match("/^id:\d+$/i", strtolower($sQWord))) {

				$aInput['filters'][] = array(
						'group'=>'filter',
						'key' => 'id', //操作符
						'op' => '=', //操作符=,in,like
						'value' => str_replace("id:", "", $sQWord),//过滤的值
				);
				$bFulltextSearchFlag = false;

			// }elseif (preg_match("/^id:[0-9a-zA-Z]+/i", ($sQWord)) || preg_match("/^[0-9a-zA-Z]+/i", ($sQWord))) {
			}elseif (preg_match("/^id:[0-9a-zA-Z]+/i", ($sQWord))) {
				$sQWord = str_replace("id:", "", $sQWord);
				$sIdTmp = \YcheukfCommon\Lib\Functions::decodeLafResouceId($sQWord, $sSource);

				if (preg_match("/^\d+$/i", strtolower($sIdTmp))) {

					$aInput['filters'][] = array(
							'group'=>'filter',
							'key' => 'id', //操作符
							'op' => '=', //操作符=,in,like
							'value' => $sIdTmp,//过滤的值
					);
					$bFulltextSearchFlag = false;

				}
			}

			if(!empty($sQWord) && $bFulltextSearchFlag && count($aFulltextField)){
				foreach($aFulltextField as $sKeyName){
					$aInput['filters'][] = array(
							'group'=>'filter',
							'key' => $sKeyName, //操作符
							'op' => 'like', //操作符=,in,like
							'value' => $sQWord,//过滤的值
					);
				}
			}
		}
        $aQeury = isset($_GET['q']) ? json_decode($_GET['q'], 1) : array();
		if (count($aQeury)) {
			foreach ($aQeury as $sKeyTmp => $sValTmp3) {

				$aInput['filters'][] = array(
						'group'=>'filter',
						'key' => $sKeyTmp, //操作符
						'op' => 'in', //操作符=,in,like
						'value' => "($sValTmp3)",//过滤的值
				);
			}
		}
		// var_dump($_GET['q']);
		// var_dump($aInput['filters']);
		return $aInput;
	}

	public function getServiceManager()
	{
		return $this->getSm();
	}

	function __get($name='')
	{
		switch ($name) {
			case 'serviceManager':
				return $this->getSm();
				break;
			default:
				break;
		}
	}

	/**
	 * [getInputProxy description]
	 * @return [type] [description]
	 */
	public function getInputBuilder()
	{
		$aInput = $this->getInput();
		$Customfields = ($this->getCustomfields());
		if (is_array($Customfields) && count($Customfields)) {

			$aTmp3 = array();
			// var_dump($Customfields);
			if (isset($aInput['input']['detail']['table'])) {

			    
			    if (is_array($aInput['input']['detail']['table']) && count($aInput['input']['detail']['table'])) {
			        foreach ($aInput['input']['detail']['table'] as $sTable => $aRowTmp1) {
			            if (is_array($aRowTmp1['dimen']) && count($aRowTmp1['dimen'])) {
			                foreach ($aRowTmp1['dimen'] as $nIndex => $aRowTmp2) {
	                            $sFieldKey = $aRowTmp2;
	                            if (is_array($aRowTmp2)) {
	                                $sFieldKey = $aRowTmp2['key'];
	                            }

			                	if(in_array($sFieldKey, array(
			                	    $sTable.'___id',
			                	))){
			                	    continue;
			                	}

			                	$bGroup = false;
			                	if (in_array($sFieldKey, $Customfields)) {
				                	$bGroup = true;
			                	}
			                	if (is_array($aRowTmp2)) {
			                		$aRowTmp2['group'] = $bGroup;
			                	}
			                	if (is_string($aRowTmp2)) {
			                		$aRowTmp2=array(
			                			'group' => $bGroup, 
			                			'key' => $sFieldKey, 
			                		);
			                	}
			                	$aInput['input']['detail']['table'][$sTable]['dimen'][$nIndex] = $aRowTmp2;

			                }
			            }
			        }
			    }
			}
		}
		// var_dump($aInput['input']['detail']['table']);
		// exit;
		return $aInput;
	}	
	function setCustomfields($Customfields){
		$this->Customfields = $Customfields;
	}
	function getCustomfields(){
		return $this->Customfields;
	}


	function setSm($sM){
		$this->sM = $sM;
	}
	function setResource($sSource){
		$this->sSource = $sSource;
	}
	function setCid($nCid){
		$this->nCid = $nCid;
	}
	function setFid($nFid){
		$this->nFid = $nFid;
	}

	public function getExportChaset()
	{
		if (is_null($this->ExportChaset)) {
			$this->setExportChaset("UTF-8");
		}
	    return $this->ExportChaset;
	}
	
	public function setExportChaset($p)
	{
	    $this->ExportChaset = $p;
	}
	

	
	function setReportParams($aReportParams){
		$this->aReportParams = $aReportParams;
	}

	function setReturnType($sReturnType){
		$this->sReturnType = $sReturnType;
	}
        function setCustomParams($sCustomParams){
		$this->sCustomParams = $sCustomParams;
	}
        function getCustomParams(){
		return $this->sCustomParams;
	}
	function getReturnType(){
		return $this->sReturnType;
	}
	function getResource(){
		return $this->sSource;
	}
	function getSm(){
		return $this->sM;
	}
	function getCid(){
		return $this->nCid;
	}
	function getFid(){
		return $this->nFid;
	}
	function getReportParams(){
		return $this->aReportParams;
	}

	function getAdvanceData(){
		return $this->aAdvanceData;
	}
	function setAdvanceData($s){
		$this->aAdvanceData = $s;
	}

	// //为所有的key都加上source 前缀
	// //
	// function _formatInput($aInput){
	// 	foreach ($aInput['input'] as $key => $a1) {
	// 		if (isset($a1['table']) && is_array($a1['table'])) {
	// 			foreach ($a1['table'] as $sResoure => $a2) {
	// 				if (isset($a2['dimen']) && is_array($a2['dimen'])) {
	// 					foreach ($a1['dimen'] as $k2 => $a3) {
	// 						$aTmp = $a3;
	// 						if (is_string($a3)) {
	// 							$aTmp = array(
	// 								"key" => $a3,
	// 							);
	// 						}

	// 					}
	// 				}

	// 			}
	// 		}
	// 	}
	// }
	function _formatOutput($aInput){
		// $aInput = $this->_formatInput($aInput);

		$aConfig = $this->serviceManager->get('config');

		// YcheukfReport
		switch($this->getReturnType()){
			case 'csv':
			case 'csv_all':
				$sCharset = "UTF-8";
				if (isset($aConfig['YcheukfReport']['export_csv_charset'])) {
					$sCharset = $aConfig['YcheukfReport']['export_csv_charset'];
				}
				$sExportCharset = 
				$aInput['output']['exportInfo'] = array(
					'fileName' => "export_".$this->getResource().'_'.date("Y-m-d"),//生成文件时的文件名，type=file时必须指定
					'type' => 'download',//生成文件时的文件名，type=file时必须指定
					'charset' => strtoupper($sCharset),
				);
			  $aInput['output']['csvSeparator'] = ',';
				break;
			default:
				break;
		}
        if(!isset($aInput['custom']))$aInput['custom'] = array();
        $aInput['custom']['cid'] = $this->getCid();
        $aInput['custom']['source'] = $this->getResource();
		return $aInput;
	}


	function _fmtInput4Solr($oSm, $aReportParams, $sSolrType, $sIdFiled, $aInput){
		$aFilters = isset($aReportParams['params']) ? json_decode($aReportParams['params'], 1) : array();
		if(isset($aFilters['q_keyword']) && !empty($aFilters['q_keyword'])){
			list($nTotal, $aSolrData) = \YcheukfCommon\Lib\Functions::getSolrResult($oSm, "allfield:\"".$aFilters['q_keyword']."\" && solr_type:\"".$sSolrType."\"", 'id,name,solr_type', 100);
			$aIds = array();
			for($i=0; $i<count($aSolrData); $i++){
				$aIds[] = (int)str_replace($aSolrData[$i]['solr_type']."___", "", $aSolrData[$i]['id']);
			}

			if(count($aIds)){
				$aInput['filters'][] = array(
					'key' => $sIdFiled, //操作符
					'op' => 'in', //操作符=,in,like
					'value' => join(",", $aIds)//过滤的值
				);
				$aInput['input']['detail']['orderby'] = "find_in_set(".$sIdFiled.",'".join(",", $aIds)."')";
			};

			unset($aFilters['q_keyword']);
			$aReportParams['params'] = json_encode($aFilters);
		}
		return array($aReportParams, $aInput);
	}
	/**
	 * 获取API分页以及条件参数数组
	 * @param 分页参数 $pageParams
	 * @param 条件 $params
	 * @return array
	 */
	function getPageParamsForApi($pageParams,$params = array()){
		$query = json_decode($pageParams, 1);

		$options = array('count_flag'=>1);
		foreach($query as $sKey => $sVal){
			$options[$sKey] = $sVal;
		}
		$options['page'] = 1;	//当前页数
		$options['count'] = 10;	//每页显示数
		if(isset($options['current_page'])){
			$options['page'] = $options['current_page']+1;
			unset($options['current_page']);
		}
		if(isset($options['items_per_page'])){
		    $options['count'] = $options['items_per_page'];
		    unset($options['items_per_page']);
		}
		if (!empty($params)){
		    foreach ($params as $key => $val){
		    	$options[$key] = $val;
		    }
		}

		return $options;
	}


	/**
	 * 从metadata中获取1:n数据的text信息
	 *
	 * @param int $nFatherId 主记录ID
	 * 
	 */
	public function getRelationText1($nFatherId, $sChildSourceType, $nRelationType, $sDefaultMessage="")
	{
		

        $aRelationIds = \Application\Model\Common::getRelationList($this->sM,$nRelationType, $nFatherId);

        $sReturn = $sDefaultMessage;
        if (count($aRelationIds)) {
        	$aTmp = \Application\Model\Common::getResourceMetadaList($this->sM, $sChildSourceType, $aRelationIds);

            $sReturn = "";
        	foreach ($aTmp as $keyTmp2 => $sValTmp2) {
        		$sReturn .= $keyTmp2."/".$sValTmp2.";";
        	}
        }
        return $sReturn;
	}

	/**
	 * 从自定义表中获取1:n数据的text信息
	 *
	 * @param int $nFatherId 主记录ID
	 * 
	 */
	public function getRelationText2($nFatherId, $sChildSourceType, $nRelationType, $sPkfield="id", $sLabelfield="id", $sDefaultMessage="", $sReturnType="text")
	{
		

        $aRelationIds = \Application\Model\Common::getRelationList($this->sM,$nRelationType, $nFatherId);
        // var_dump($aRelationIds);

        $sReturn = $sDefaultMessage;
        $aReturn = array();
        if (count($aRelationIds)) {
	    	$aReturn = \Application\Model\Common::getResourceList($this->sM, array('source'=>$sChildSourceType, 'pkfield'=>$sPkfield, 'labelfield'=>$sLabelfield), array($sPkfield=>array('$in'=>($aRelationIds))));

            $sReturn = "";
        	foreach ($aReturn as $keyTmp2 => $sValTmp2) {
        		$sReturn .= $keyTmp2."/".$sValTmp2.";";
        	}
        }
        switch ($sReturnType) {
        	case 'text':
		        return $sReturn;
        		break;
        	default:
        		return $aReturn;
        		break;
        }
	}


}