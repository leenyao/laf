<?php
namespace Application\YcheukfReportExt\Inputs;

class Metadata extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$nCid = $this->getCid();
		$oSm = $this->getSm();
		$sReturnType = $this->getReturnType();


		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
				array(
					'key' => 'type', //操作符
					'op' => '=', //操作符=,in,like
					'value' => $nCid//过滤的值
				),
				array(
					'key' => 'status', //操作符
					'op' => '=', //操作符=,in,like
					'value' => 1//过滤的值
				),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								array(
									 'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>true,
								),
								array(
									 'key' => 'resid', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>true,
								),
								array(
									 'key' => 'status', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>false,
//											'show'=>false,
								),
								array(
									 'key' => 'type', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>false,
//											'show'=>false,
								),
//										'type',
//								'resid',
								'label',
								array(
									 'key' => 'modified', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array(),
								),
//								'logo',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $sReturnType,
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('label'), array('label'));
		$aInput = $this->_formatOutput($aInput);


		return $aInput;
		
	}

}