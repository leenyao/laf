<?php
namespace Application\YcheukfReportExt\Inputs;

class Post extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$nCid = $this->getCid();
		$oSm = $this->getSm();
		$sReturnType = $this->getReturnType();


		$aPostCategorys = \Application\Model\Common::getCategoryList($oSm, array('purview_type'=>$nCid));
		$aCids = array_keys($aPostCategorys);
		if($nCid==4){
			$aInput = array(
				'filters' => array( //选传. 默认为空, 过滤条件. 
					array(
						'key' => 'category_id', //操作符
						'op' => 'in', //操作符=,in,like
						'value' => "(".join(",", $aCids).")"//过滤的值
					),
					array(
						'key' => 'view_type', //操作符
						'op' => '<>', //操作符=,<>,in,like
						'value' => 5//过滤的值
					),
				),
				'input'=>array(
					'detail'=>array(
						'type' =>'table',
						'orderby' =>'modified desc',
						'table' => array(
							$sSource => array(
								'dimen' => array(
									array(
										 'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
										'group'=>true,
									),
									array(
										 'key' => 'postname', //必传. 某个维度的键值, 可直接用表中的字段名
										'thclass'=>array('td300'),
									),
	//								'postname',
									'category_id',
//									'view_type',
									'm153_id',
									'modified',
									array(
										 'key' => 'status134', //必传. 某个维度的键值, 可直接用表中的字段名
										'thclass'=>array(),
									),
	//								'logo',
								),
							),
						),
					),
				),
				'output' => array(
					'format' => $sReturnType,
				),
			);
		}else{
			$aInput = array(
				'filters' => array( //选传. 默认为空, 过滤条件. 
					array(
						'key' => 'category_id', //操作符
						'op' => 'in', //操作符=,in,like
						'value' => "(".join(",", $aCids).")"//过滤的值
					),
					array(
						'key' => 'view_type', //操作符
						'op' => '<>', //操作符=,<>,in,like
						'value' => 5//过滤的值
					),
				),
				'input'=>array(
					'detail'=>array(
						'type' =>'table',
						'orderby' =>'modified desc',
						'table' => array(
							$sSource => array(
								'dimen' => array(
									array(
										 'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
										'group'=>true,
									),
									array(
										 'key' => 'postname', //必传. 某个维度的键值, 可直接用表中的字段名
										'thclass'=>array('td300'),
									),
	//								'postname',
									'category_id',
									'view_type',
									'modified',
									array(
										 'key' => 'status134', //必传. 某个维度的键值, 可直接用表中的字段名
										'thclass'=>array(),
									),
	//								'logo',
								),
							),
						),
					),
				),
				'output' => array(
					'format' => $sReturnType,
				),
			);
		}
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('category_id', 'status', 'm153_id'), array('postname'));
		$aInput = $this->_formatOutput($aInput);



		return $aInput;
		
	}

}