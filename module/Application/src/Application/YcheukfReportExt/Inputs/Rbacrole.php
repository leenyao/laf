<?php
namespace Application\YcheukfReportExt\Inputs;

class Rbacrole extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$nCid = $this->getCid();
		$oSm = $this->getSm();
		$sReturnType = $this->getReturnType();


		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
				array(
					'key' => $sSource.'___id', //操作符
					'op' => '<>', //操作符=,in,like
					'value' => 1//过滤的值
				),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								array(
									 'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>true,
								),
								'role_name',
								array(
									 'key' => 'modified', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array(),
								),
//								'logo',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $sReturnType,
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('status', ), array('role_name'));
		$aInput = $this->_formatOutput($aInput);


		return $aInput;
		
	}

}