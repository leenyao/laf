<?php
namespace Application\YcheukfReportExt\Inputs;

class Resume extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$nCid = $this->getCid();
		$nFid = $this->getFid();
		$oSm = $this->getSm();
		$sReturnType = $this->getReturnType();

		$sResumeName = 'resume___split___name';
		$sResumeTitle = 'resume___split___title';
		$sResumeBirthday = 'resume___split___birthday';
		$sResumeJoinworkdate = 'resume___split___joinworkdate';
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
//								'resume___id', //必传. 某个维度的键值, 可直接用表中的字段名
								array(
									 'key' => 'resume___id', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td50'),
								),
								array(
									 'key' => 'user_id', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td100'),
								),
								array(
									 'key' => $sResumeName, //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td100'),
								),
//								$sResumeName, //必传. 某个维度的键值, 可直接用表中的字段名
//								'resume___split___job_tags',
//								'user_id',
								array(
									 'key' => 'm155_id', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td50'),
								),
								array(
									 'key' => 'phone', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td50'),
								),
//								array(
//									 'key' => 'gender', //必传. 某个维度的键值, 可直接用表中的字段名
//									'thclass'=>array('td50'),
//								),
//								array(
//									 'key' => 'm105_id', //必传. 某个维度的键值, 可直接用表中的字段名
//									'thclass'=>array('td100'),
//								),
//								array(
//									 'key' => 'm107_id', //必传. 某个维度的键值, 可直接用表中的字段名
//									'thclass'=>array('td150'),
//								),
								array(
									 'key' => $sResumeTitle, //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td100'),
								),
//								'phone',
//								array(
//									 'key' => $sResumeBirthday, //必传. 某个维度的键值, 可直接用表中的字段名
//									'thclass'=>array('td100'),
//								),
//								array(
//									 'key' => $sResumeJoinworkdate, //必传. 某个维度的键值, 可直接用表中的字段名
//									'thclass'=>array('td100'),
//								),
								array(
									 'key' => 'percent', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td100'),
								),
//								'modified',
//								array(
//									 'key' => 'status', //必传. 某个维度的键值, 可直接用表中的字段名
//									'thclass'=>array(),
//								),
								array(
									 'key' => 'resume_oplogs', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array("td50"),
								),
								array(
									 'key' => 'op_jobfunc_id', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array("td50"),
								),
								array(
									 'key' => 'apply_flag', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>false,
								),
//								'logo',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $sReturnType,
			),
		);
//        $aInput['filters'] = array(
//            array(
//                'key' => 'apply_flag', //操作符
//                'op' => '=', //操作符=,in,like
//                'value' => 1,//过滤的值
//            )
//        );
		if(!is_null($nFid)){
			$aTmp2 = \Application\Model\Common::getResourceList($this->getServiceLocator(), array('source'=>'taskaction', 'pkfield'=>'task_id', 'labelfield'=>'resume_id'), array('task_id'=>$nFid), true);
			$aInput['filters'][] = array(
                'key' => 'resume___id', //操作符
                'op' => 'in', //操作符=,in,like
                'value' => join(',', $aTmp2[$nFid]),//过滤的值
			);
		}

		$sSolrType = 'resume';
		$sIdFiled = 'resume___id';
		list($aReportParams, $aInput) = $this->_fmtInput4Solr($oSm, $aReportParams, $sSolrType, $sIdFiled, $aInput);

		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('status', 'm105_id', 'm155_id', 'm107_id', ), array($sResumeName, $sResumeTitle));
		$aInput = $this->_formatOutput($aInput);


		return $aInput;
		
	}

}