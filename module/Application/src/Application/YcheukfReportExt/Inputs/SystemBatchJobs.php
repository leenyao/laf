<?php
namespace Application\YcheukfReportExt\Inputs;
class SystemBatchJobs extends \Application\YcheukfReportExt\Inputs\Inputs{

    function getInput(){
        $sSource = $this->getResource();

                
        $oIdentity = $this->sM->get('zfcuser_auth_service')->getIdentity();
        $sUserId = $oIdentity->getId();
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($this->sM, $oIdentity);

        $aInput = array(
            'filters' => array( //选传. 默认为空, 过滤条件. 
                // array(
                //     'key' => 'op_user_id', //操作符
                //     'op' => 'in', //操作符=,in,like
                //     'value' => "(0, ".$sUserId.")"//过滤的值
                // ),
            ),
            'input'=>array(
                'detail'=>array(
                    'is_editable' => false,
                    'is_viewable' => false,
                    'type' =>'table',
                    'orderby' =>'modified desc,  id desc',
                    'table' => array(
                        $sSource => array(
                            'dimen' => array(
                                $sSource.'___id',
                                // array(
                                //     'key' => 'op_user_id',
                                //     'group' => false,
                                // ),
                                array(
                                    'key' => 'type',
                                    'group' => false,
                                ),
                                array(
                                    'key' => 'out_text',
                                    'group' => false,
                                ),
                                // array(
                                //     'key' => 'user_id',
                                //     'group' => false,
                                // ),
                                // 'user_id',
                                // 'systembatchjobs___split___user_id',
                                array(
                                    'key' => 'm158_id',
                                    'group' => true,
                                ),
                                'memo',
                                'in_file',
                                // 'out_file',
                                'processed_time',
                                'op_user_id',
                                'modified',
                                // array(
                                //     'key'=>'___subtr___op___id',
                                // ),
                            ),
                        ),
                    ),
                ),
            ),
            'output' => array(
                'format' => $this->getReturnType(),
            ),
            'custom' => array(
                'subtr' => true,
                'subtr_function' => function($nId) use ($sSource){
                    $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sM);
                    $aSourceData = \Application\Model\Common::getResourceById($this->sM, $sSource, $nId);
                    $sOut = nl2br($aSourceData['out_text']);

                    $sHtml = "<div  style='word-break: break-all;max-width: 900px;white-space: normal;'>处理结果:<p/><p/>
                        {$sOut}
                    </div>";
                    return $sHtml;
                }
            ),
        );

        if (in_array("user-member", $aRoles)) {

            $aInput['filters'][] = array(
                'key' => 'op_user_id', //操作符
                'op' => 'in', //操作符=,in,like
                'value' => "(0,".$sUserId.")"//过滤的值
            );
        }
        // 超管账号, 允许看到具体用户
        if (in_array("superadmin", $aRoles)) {
            $aInput['input']['detail']['table'][$sSource]['dimen'][1]['group'] = true;
        }

        $aInput = $this->_fmtDetailFilters(
            $sSource, 
            $aInput, 
            $this->getReportParams(), 
            array('status'),  //filterable field
            array('out_text', 'memo') //fulltext search field
            // array('m157_id' => array('op' => 'in', 'value' => "1")) // default filters
        );
        $aInput = $this->_formatOutput($aInput);

        return $aInput;
        
    }
}