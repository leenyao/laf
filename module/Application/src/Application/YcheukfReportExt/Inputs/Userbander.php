<?php
namespace Application\YcheukfReportExt\Inputs;

class Userbander extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$nCid = $this->getCid();
		$oSm = $this->getSm();
		$sReturnType = $this->getReturnType();


		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
//				array(
//					'key' => 'user_id', //操作符
//					'op' => '>=', //操作符=,in,like
//					'value' => 50000//过滤的值
//				),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								array(
									 'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>true,
									'thclass'=>array('td150'),
								),
								array(
									 'key' => 'user_id', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td100'),
								),
								array(
									 'key' => 'bander_user_id', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td100'),
								),
								'modified',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $sReturnType,
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('status',), array( 'user_id', 'bander_user_id'));
		$aInput = $this->_formatOutput($aInput);


		return $aInput;
		
	}

}