<?php
namespace Application\YcheukfReportExt\Inputs;

class Viewusermedal extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$nCid = $this->getCid();
		$nFid = $this->getFid();
		$oSm = $this->getSm();
		$sReturnType = $this->getReturnType();


		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								array(
									 'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=>true,
									'thclass'=>array('td50'),
								),
								'username',
								'display_name',
								'medal_labels',
								'modified',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $sReturnType,
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('username'), array( 'username', 'display_name','medal_labels'));
//		var_dump($aInput['filters']);
		$aInput = $this->_formatOutput($aInput);
		return $aInput;
		
	}

}