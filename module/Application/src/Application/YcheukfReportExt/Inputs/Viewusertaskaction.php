<?php
/**
	user taskaction
*/
namespace Application\YcheukfReportExt\Inputs;

class Viewusertaskaction extends Inputs{
	function getInput(){
		$sSource = $this->getResource();
		$aReportParams = $this->getReportParams();
		$nCid = $this->getCid();
		$nFid = $this->getFid();
		$oSm = $this->getSm();
		$sReturnType = $this->getReturnType();
		$sUserId = ($oSm->get('zfcuser_auth_service')->getIdentity()->getId());
//var_dump($sUserId);

		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
				array(
					'key' => 'to_user_id', //操作符
					'op' => '=', //操作符=,in,like
					'value' =>$sUserId ,//过滤的值
				),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
//								array(
//									 'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
//									'group'=>true,
//								),
								array(
									 'key' => 'to_user_id', //必传. 某个维度的键值, 可直接用表中的字段名
									'group'=> false,
								),
								array(
									 'key' => 'applyuser_id', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td100'),
								),

								array(
									 'key' => 'modified', //必传. 某个维度的键值, 可直接用表中的字段名
									'thclass'=>array('td100'),
								),
//								'user_id',
//								'resume_view_type',
//								'resume_phone',
//								'resume_mail',
//								'modified',
//								array(
//									 'key' => 'verify_status', //必传. 某个维度的键值, 可直接用表中的字段名
//									'thclass'=>array(),
//								),
//								'logo',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $sReturnType,
			),
		);
		if(!is_null($nFid)){
			$aInput['filters'] = array(
				array(
					'key' => 'task_id', //操作符
					'op' => '=', //操作符=,in,like
					'value' =>$nFid ,//过滤的值
				),
				array(
					'key' => 'apply_type', //操作符
					'op' => 'in', //操作符=,in,like
					'value' => '1,2'//过滤的值
				),		
			);
		}
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('status', 'apply_type'), array('resume_name', 'resume_title', 'resume_mail', 'resume_phone'));
		$aInput = $this->_formatOutput($aInput);


		return $aInput;
		
	}

}