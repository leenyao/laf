<?php
namespace YcheukfReportExt\Report\Dictionary;

use Application\Model\Common;

/**
 * id转label类
 *
 * 负责将db类中获得id转变成相应的label
 *
 * @author   ycheukf@gmail.com
 * @package  Dictionary
 * @access   public
 */
class Id2label extends \YcheukfReport\Lib\ALYS\Report\Dictionary\Id2label
{
    public $DB;
    public $oRouter;
    public $lang = 'CN';
    public $combinationFieldSplitChar = '__combine__';
    public $aDimenFunctions = array();

    public function __construct()
    {
        $this->DB = \YcheukfReport\Lib\ALYS\ALYSFunction::loadClass(
            "Dboperate.Sql"
        );
    }


    function __get($name='')
    {
        switch ($name) {
            case 'serviceManager':
                    $aReportConfig = \YcheukfReport\Lib\ALYS\ALYSConfig::get();
                   return $aReportConfig['smHandle'];
                break;
            default:
                break;
        }
    }

    function _geturl($sRoute="", $aParam=array()){
        return($this->oRouter->assemble($aParam, array('name'=>$sRoute)));
    }


    protected function addDimenTrigger($sDimen='', $fFunction)
    {
        $this->aDimenFunctions[$sDimen] = $fFunction;
    }
    protected function getDimenTrigger()
    {
        return $this->aDimenFunctions;
    }

    /**
     * 将数据表中获取得的相关ID转换成对应的文字

     * @param array aDimenkey2selected 存储需要转换的pName, array('timeslot', 'domainId', ...)
     * @param array aryIdSet 存储需要转换的pName与id序列, array('timeslot'=>array('2012-10-10', '2012-10-12'), 'domainId'=>array(1, 2, 3), ...)
     * @param array aConfig 将要输出的html
     * @return array 处理过后的html
     * @description 将数据表中获取得的相关ID转换成对应的文字
     *
     */
    public function ALYSchgId2Label(
        $aDimenkey2selected = array(),
        $aryIdSet = array(),
        $aConfig = array())
    {
        $aReportConfig = \YcheukfReport\Lib\ALYS\ALYSConfig::get();
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
        if (!defined('__DEBUG__'))
            define('__DEBUG__', $aReportConfig['debug']);
       $this->oRouter = ($aReportConfig['smHandle']->get('httprouter'));
//        exit;
        $oRouteMatch = ($this->oRouter->match($aReportConfig['smHandle']->get('request')));
//        var_dump($oRouteMatch->getParam('type'));
        $aryReturn = array();
        $aryTmp = array();
        if (empty($aryIdSet))
            return $aryReturn;
        $aDimenFunctions = $this->getDimenTrigger();



        $oIdentity = $aReportConfig['smHandle']->get('zfcuser_auth_service')->getIdentity();
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($aReportConfig['smHandle'], $oIdentity);
        // var_dump($aRoles);


        foreach ($aDimenkey2selected as $dimenKey => $sSelected) {
            if (is_int($dimenKey))
                $dimenKey = $sSelected;

            if (empty($aryIdSet[$dimenKey]))
                $aryIdSet[$dimenKey] = array();
            if (preg_match(
                '/' . addslashes($this->combinationFieldSplitChar) . '/',
                $dimenKey
            )) {
                $a4Combin = $this->getCombinationFieldAry($dimenKey);
                $dimenKey = $a4Combin['name'];
            }

// var_dump($aInput['custom']['source']);
// var_dump($dimenKey);
            if (count($aDimenFunctions) && isset($aDimenFunctions[$dimenKey])) {
                $aryReturn[$dimenKey] = $aDimenFunctions[$dimenKey]($aryIdSet[$dimenKey], $aReportConfig['smHandle']);

            }elseif (count($aDimenFunctions) && isset($aDimenFunctions[$aInput['custom']['source']."__split__".$dimenKey])) {
                $aryReturn[$dimenKey] = $aDimenFunctions[$aInput['custom']['source']."__split__".$dimenKey]($aryIdSet[$dimenKey], $aReportConfig['smHandle']);
            }else{
                switch (strtolower($dimenKey)) //$dimenKey 为在数据表中的字段名
                {
                    case 'none':
                    case 'dbconfigop':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][$v] = "<a _val='" . $v
                                . "' class='edit' href='javascript::void()'>"
                                . \YcheukfReport\Lib\ALYS\ALYSLang::_(
                                    'RE_DIMEN_EDIT'
                                ) . "</a> &nbsp; <a _val='" . $v
                                . "' class='upgrade' href='javascript::void()'>"
                                . \YcheukfReport\Lib\ALYS\ALYSLang::_(
                                    'RE_DIMEN_UPGRADE'
                                ) . "</a> &nbsp; <a _val='" . $v
                                . "' class='downgrade' href='javascript::void()'>"
                                . \YcheukfReport\Lib\ALYS\ALYSLang::_(
                                    'RE_DIMEN_DOWNGRADE'
                                ) . "</a>&nbsp; <a _val='" . $v
                                . "' class='config' href='javascript::void()'>"
                                . \YcheukfReport\Lib\ALYS\ALYSLang::_(
                                    'RE_DIMEN_CONFIG'
                                ) . "</a>";
                        }
                        break;
                    case 'gender':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][$v] = $v == 1 ? "男" : "女";
                        }
                        break;
                    case 'gender':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][$v] = $v == 1 ? "男" : "女";
                        }
                        break;

                    case 'op_user_id':
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                    
                        $aryReturn[$dimenKey] = $this->_getDataFromResource(
                                                    $aReportConfig['smHandle'],
                                                    $aTmpFilter,
                                                    'user',
                                                    'id',
                                                    'display_name'
                                                );
                        break;

                    case 'datapath':
                    case 'in_file':
                    case 'out_file':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][$v] = "<a href='". $v . "'>下载</a>";;
                        }
                        break;

                    case 'opration':;
                    case (preg_match("/___subtr___/", $dimenKey) ? $dimenKey : false):
                        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($aReportConfig['smHandle']);
                        foreach ($aryIdSet[$dimenKey] as $sActionIdTmp) {
                            $aryReturn[$dimenKey][$sActionIdTmp] = '<span title="显示隐藏" class="tr-trigger tr-trigger icon-resize-full" ></span>';
                        }
                        break;
                    case 'rbac___thirdrelationid':
                        $aTmpRoleIds = array();
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                        if (count($aTmpFilter)) {
                            $aM6Ids = \Application\Model\Common::getResourceMetadaList(
                                $aReportConfig['smHandle'],
                                6,
                                $aTmpFilter,
                                1,
                                1
                            );
                            foreach ($aTmpFilter as $idTmp) {
                                $aryReturn[$dimenKey][$idTmp] = "";
                                $aTmpM6Ids = isset($aM6Ids[$idTmp]) ? $aM6Ids[$idTmp]
                                    : array();
                                if (count($aTmpM6Ids)) {
                                    $aRoleName = array();
                                    foreach ($aTmpM6Ids as $sRoleId) {
                                        $aRoleTmp = \Application\Model\Common::getResourceById(
                                            $aReportConfig['smHandle'],
                                            'rbacrole',
                                            $sRoleId
                                        );
                                        $aRoleName[] = $aReportConfig['smHandle']
                                        ->get('translator')
                                        ->translate($aRoleTmp['role_name']);
                                    }
                                    $aryReturn[$dimenKey][$idTmp] = join(
                                        ",",
                                        $aRoleName
                                    );
                                }
                            }
                        }
                        break;
                    case 'id':
                    case (preg_match("/___id/", $dimenKey) ? $dimenKey : false):
                        $aDataKey = array();
                        $bEditable = isset($aInput['input']['detail']['is_editable']) ? $aInput['input']['detail']['is_editable'] : true;
                        $bViewable = isset($aInput['input']['detail']['is_viewable']) ? $aInput['input']['detail']['is_viewable'] : true;
                        if(preg_match('/___id/i', $dimenKey))
                            $sSource = str_replace('___id', '', $dimenKey);
                        else
                            $sSource = $aInput['custom']['source'];
                        $nTmpCid = isset($aInput['custom']['cid']) ? $aInput['custom']['cid'] : 0;
                        foreach ($aryIdSet[$dimenKey] as $key => $v) {

                            //混淆ID
                            $sHashV = \YcheukfCommon\Lib\Functions::encodeLafResouceId($v, $sSource);
                            if($bEditable){
                                $aDataKey['edit'] = array(
                                    'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_(
                                        'edit'
                                    ),
                                    'data' => $this->_geturl(
                                        'zfcadmin/contentedit',
                                        array(
                                            'cid' => $nTmpCid,
                                            'id' => $sHashV,
                                            'type' => $sSource
                                        )
                                    ),
                                    'attributes' =>  array('target' => '_blank'),
                                    'type' => 'url',
                                    'permission' => array(
                                        'zfcadmin/contentedit/' . $sSource,
                                    ),
                                );
                            };

                            // links

                            if($bViewable){
                                $aDataKey['view'] = array(
                                    'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_(
                                        'view'
                                    ),
                                    'data' => $this->_geturl(
                                        'zfcadmin/contentview',
                                        array(
                                            'cid' => $nTmpCid,
                                            'id' => $sHashV,
                                            'type' => $sSource
                                        )
                                    ),
                                    'attributes' => array("target" => "_blank",),
                                    'type' => 'url',
                                );
                            }
                            if(strtolower(LAF_NAMESPACE)=="lur" && in_array($dimenKey, array('system_batch_jobs___id', 'adschedule___id'))){
                                

                                $aDataKey['histreport'] = array(
                                    'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_('history report'),
                                    'data' => $this->_geturl('lur/default', array('controller'=>'index', 'action'=>'histreport'))."?id=".$sHashV."&debug=0&sdate=".date("Y-m-d", strtotime("-1 day"))."&idkey=".$dimenKey."&edate=".date("Y-m-d", strtotime("-1 day")),    
                                    'attributes' => array("target" => "_blank",),
                                    'type' => 'url',
                                );
                                $aDataKey['realtimereport'] = array(
                                    'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_('realtime report'),
                                    'data' => $this->_geturl('lur/default', array('controller'=>'index', 'action'=>'realtimereport'))."?debug=1&idkey=".$dimenKey."&id=".$sHashV,    
                                    'attributes' => array("target" => "_blank",),
                                    'type' => 'url',
                                );
                            
                            }
                            if(strtolower(LAF_NAMESPACE)=="lur" && in_array($dimenKey, array('adschedule___id'))){

                                $aDataKey['histadschedule'] = array(
                                    'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_('历史修改记录'),
                                    'data' => $this->_geturl('lur/default', array('controller'=>'index', 'action'=>'histadschedule'))."?id=".$sHashV,    
                                    'attributes' => array("target" => "_blank",),
                                    'type' => 'url',
                                );
                             }   
                            $sDataKey = $this
                            ->_fmtDataCommandByPermission(
                                $aReportConfig['smHandle'],
                                $aDataKey
                            );

                            $sLabelTmp = (in_array($sSource, array("adcustomer"))) ?  $v : $sHashV;

                            if (in_array("superadmin", $aRoles)) {
                                $sLabelTmp = $sLabelTmp."/".$v;
                            }

                            $aryReturn[$dimenKey][$v] = '<span data-command=\''
                                . $sDataKey . '\'>' . $sLabelTmp . '</span>';
                        }
                        break;
    //                case 'task_title':
    //                    $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
    //                    if (count($aTmpFilter))
    //                        $aryReturn[$dimenKey] = $this
    //                        ->_getDataFromResource(
    //                            $aReportConfig['smHandle'],
    //                            $aTmpFilter,
    //                            'task',
    //                            'id',
    //                            'task_title'
    //                        );
    //                    break;
                    case 'cid':
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                        if (count($aTmpFilter))
                            $aryReturn[$dimenKey] = $this
                            ->_getDataFromResource(
                                $aReportConfig['smHandle'],
                                $aTmpFilter,
                                'campaign',
                                'id',
                                'theme'
                            );
                        break;
                    case 'category_id':
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                        if (count($aTmpFilter))
                            $aryReturn[$dimenKey] = $this
                            ->_getDataFromResource(
                                $aReportConfig['smHandle'],
                                $aTmpFilter,
                                'hcoffee_category',
                                'id',
                                'label'
                            );
                        break;
                    case 'user__split__role_id':
                    case 'role_id':
                    case 'userlog__split__user_role':
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                        if (count($aTmpFilter))
                            $aryReturn[$dimenKey] = $this
                            ->_getDataFromResource(
                                $aReportConfig['smHandle'],
                                $aTmpFilter,
                                'rbacrole',
                                'id',
                                'role_name'
                            );
                        break;
                        
                    case 'user_id':
                    case 'taskactionlog_user_id':
                    case 'applyuser_id':
                    case 'bander_user_id':
                    case 'agent_id':
                    case 'op_id':
                    case 'report_uid':
                    case 'finance_user_id':
                    case 'fuid':
                    case 'cuid':
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                        // var_dump($aryIdSet);
                        if (count($aTmpFilter))
                            $aryReturn[$dimenKey] = $this
                            ->_getDataFromResource(
                                $aReportConfig['smHandle'],
                                $aTmpFilter,
                                'user',
                                'user_id',
    //                            'username'
                                'display_name'
                            );
                        break;
                    case 'report_resid':
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                        if (count($aTmpFilter))
                            $aryReturn[$dimenKey] = $this
                            ->_getDataFromResource(
                                $aReportConfig['smHandle'],
                                $aTmpFilter,
                                'help',
                                'id',
                                'content'
                            );
                        if(count($aryReturn[$dimenKey])){
                            foreach($aryReturn[$dimenKey] as $i=>$v){
                                $aryReturn[$dimenKey][$i] = '<a href="'.$this->_geturl('zfcadmin/contentlist',
                                                            array(
                                                                'cid' => 1,
                                                                'fid' => $i,
                                                                'type' => 'help'
                                                            )).'"><div class="resid-text">'.$v.'</div></a>';
                            }
                        }
                        break;
                    case 'content':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][(string) $v] = '<div class="resid-text">'.$v.'</div>';
                        }
                        break;
                    case 'company_id':
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                        if (count($aTmpFilter))
                            $aryReturn[$dimenKey] = $this
                            ->_getDataFromResource(
                                $aReportConfig['smHandle'],
                                $aTmpFilter,
                                'company',
                                'id',
                                'title'
                            );
                        break;
                    case 'dc2':
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                        if (count($aTmpFilter))
                            $aryReturn[$dimenKey] = $this
                            ->_getDataFromResource(
                                $aReportConfig['smHandle'],
                                $aTmpFilter,
                                'adschedule',
                                'id',
                                'code_label'
                            );
                        break;
                    case 'resume___split___attachment':
                        $url = \YcheukfCommon\Lib\Functions::getBaseUrl(
                            $aReportConfig['smHandle']
                        );
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            if (!empty($v)) {
                                $aryReturn[$dimenKey][(string) $v] = '<a href="'
                                    . $v . '" target="_blank"><img src="'
                                    . $url
                                    . '/images/icons/attachment.png" class="attachment"></a>';
                            } else {
                                $aryReturn[$dimenKey][(string) $v] = '无附件';
                            }
                        }
                        break;
                    case 'op_jobfunc_id':
                        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
                        if (count($aTmpFilter))
                            $aryReturn[$dimenKey] = $this
                            ->_getDataFromResource(
                                $aReportConfig['smHandle'],
                                $aTmpFilter,
                                'jobfunctionlv1',
                                'id',
                                'name'
                            );
                        break;
                    case 'pay_flag':
                        $aryReturn[$dimenKey] = $this
                        ->_getDataFromMetaData(
                            143,
                            $aryIdSet,
                            $dimenKey,
                            $aReportConfig['smHandle']
                        );
                        break;
                    case 'status':
                        $aryReturn[$dimenKey] = $this
                        ->_getDataFromMetaData(
                            102,
                            $aryIdSet,
                            $dimenKey,
                            $aReportConfig['smHandle']
                        );
                        break;
                    case 'ads_type_id':
                        $aryReturn[$dimenKey] = $this
                        ->_getDataFromMetaData(
                            123,
                            $aryIdSet,
                            $dimenKey,
                            $aReportConfig['smHandle']
                        );
                        break;
                    case 'task_status':
                        $aryReturn[$dimenKey] = $this
                        ->_getDataFromMetaData(
                            134,
                            $aryIdSet,
                            $dimenKey,
                            $aReportConfig['smHandle']
                        );
                        break;
                    case 'apply_type':
                        $aryReturn[$dimenKey] = $this
                        ->_getDataFromMetaData(
                            137,
                            $aryIdSet,
                            $dimenKey,
                            $aReportConfig['smHandle']
                        );
                        break;
                    case 'percent':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $sClassNameTmp = $v >= 60 ? "resumepercent_green"
                                : "resumepercent_red";
                            $aryReturn[$dimenKey][(string) $v] = "<div class='{$sClassNameTmp}' style='width:{$v}%'>"
                                . $v . "%</div>";
                        }
                        break;
                    case 'publisher_date':
                    case 'publisher_enddate':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][(string) $v] = empty($v)
                                || preg_match("/1970/i", $v) ? 0
                                : date("Y-m-d", strtotime($v));
                        }
                        break;
                    case 'status_custom':
                        $tmp = array('-1'=>'未激活','0'=>'未付款','1'=>'已付款', '2'=>'已付款&&insert' , '3'=>'出错');
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][(string) $v] = $tmp[$v];
                        }
                       break; 
                    case 'anonymous':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][(string) $v] = ($v==0)?'实名':'匿名';
                        }
                        break;
                    case 'picture_count':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][(string) $v] = (empty($v))?0:(count(explode(',', $v)));
                        }
                        break;


                    case 'link':
                    case 'downloadurl':
                        $basePath = \YcheukfCommon\Lib\Functions::getBaseUrl($aReportConfig['smHandle']);
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $sTmpUrl = preg_match('/[http|https]:\/\//i', $v) ? $v : "{$basePath}/{$v}";
                            $aryReturn[$dimenKey][(string) $v] = '<a href="'.$sTmpUrl.'" target="_blank">点击打开</a>';
                        }
                        break;
                    case 'logo':
                    case 'pic_url':
                    case 'picture':
                    case 'avatar':
                    case 'resume_picture':
                    case 'thumbnails':
                    case 'ads___split___pic_url':
                    case 'img1':
                    case 'icon':
                        $sBaseUrl = \YcheukfCommon\Lib\Functions::getImgUploadBaseUrl(
                            $aReportConfig['smHandle']
                        );
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            if(preg_match('/[http|https]:\/\//i', $v)){
                                $sTmpUrl = $v;
                            
                            }else{
                                $sSmallTmpUrl = str_replace(".jpg", ".small.jpg", $v);
                                 $sTmpUrl = file_exists($sSmallTmpUrl) ? "{$sBaseUrl}/{$sSmallTmpUrl}" : "{$sBaseUrl}/{$v}";
                            }
                            $aryReturn[$dimenKey][(string) $v] = "<img style='max-width:200px;max-height:100px;' src='{$sTmpUrl}'>";
                        }
                        break;
                    case 'picture_src':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][(string) $v] = preg_match('/\/img\/gossip\//i', $v) ? '系统':'用户';
                        }
                        break;
                    case 'for_picture_id':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][(string) $v] = '<a href="'.$this->_geturl('zfcadmin/contentadd',array('type'=>'viewgossipkeywordpicture','cid'=>2,'id'=>$v)).'" class="add_word" title="为图加词"></a>';
                        }
                        break;
                    case 'for_keyword_id':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][(string) $v] = '<a href="'.$this->_geturl('zfcadmin/contentadd',array('type'=>'viewgossipkeywordpicture','cid'=>1,'id'=>$v)).'" class="add_img" title="为词加图"></a>';
                        }
                        break;
                    case 'contact_id':
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][(string) $v] = '<a href="'.$this->_geturl('zfcadmin/contentadd',array('type'=>'viewgossipkeywordpicture','cid'=>1,'id'=>$v)).'" class="add_img" title="为词加图"></a>';
                        }
                        break;
                    case 'jobs_status':
                        $tmp = array(1=>'初始化',2=>'开始执行',5=>'进行中',8=>'已完成', 9=>'失败');
                        foreach ($aryIdSet[$dimenKey] as $v) {
                            $aryReturn[$dimenKey][$v] = $tmp[$v];
                            if($v > 1)
                             $aryReturn[$dimenKey][$v] .= "<span class='hidecheckbox'></span>";
                        }
                        break;
                    case (preg_match("/^m\d+_id$/", $dimenKey) ? $dimenKey : false)://metadata_id
                        $nTmpId = str_replace("m", "", $dimenKey);
                        $nTmpId = str_replace("_id", "", $nTmpId);
                        $aryReturn[$dimenKey] = $this
                        ->_getDataFromMetaData(
                            $nTmpId,
                            $aryIdSet,
                            $dimenKey,
                            $aReportConfig['smHandle']
                        );
                        break;
                    default:


                            $oTranstor = $aReportConfig['smHandle']
                            ->get('translator');
                            foreach ($aryIdSet[$dimenKey] as $v) {
                                $aryReturn[$dimenKey][(string) $v] = $oTranstor
                                ->translate($v);
                            }
                        // }

                    break;
                }
            }
        }
        return $aryReturn;
    }

    function getCodeLink($sCampaign, $sLink, $nTmpCid, $sId)
    {
        return array(
            'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_('linkcode'),
            'data' => array(
                array(
                    'target' => \YcheukfReport\Lib\ALYS\ALYSLang::_('新浪微博'),
                    'link' => $sLink
                        . "?aoc_source=qianniu&aoc_medium=sinaweibo&aoc_campaign="
                        . $sCampaign . "&aoc_term=" . $nTmpCid
                        . "&aoc_content=" . $sId
                ),
                array(
                    'target' => \YcheukfReport\Lib\ALYS\ALYSLang::_('腾讯微博'),
                    'link' => $sLink
                        . "?aoc_source=qianniu&aoc_medium=qqweibo&aoc_campaign="
                        . $sCampaign . "&aoc_term=" . $nTmpCid
                        . "&aoc_content=" . $sId
                ),
                array(
                    'target' => \YcheukfReport\Lib\ALYS\ALYSLang::_('微信'),
                    'link' => $sLink
                        . "?aoc_source=qianniu&aoc_medium=weixin&aoc_campaign="
                        . $sCampaign . "&aoc_term=" . $nTmpCid
                        . "&aoc_content=" . $sId
                ),
                array(
                    'target' => \YcheukfReport\Lib\ALYS\ALYSLang::_('其它'),
                    'link' => $sLink
                        . "?aoc_source=qianniu&aoc_medium=other&aoc_campaign="
                        . $sCampaign . "&aoc_term=" . $nTmpCid
                        . "&aoc_content=" . $sId
                ),
            ),
            'type' => 'code-dialog',
        );
    }
    function _getDataFromMetaData($sResource, $aryIdSet, $dimenKey, $oSm)
    {
        $aTmpFilter = array_unique($aryIdSet[$dimenKey]);
        $aTmpLabels = \Application\Model\Common::getResourceMetadaList(
            $oSm,
            $sResource,
            $aTmpFilter
        );
        $aReturn = array();
        if (count($aTmpFilter)) {
            foreach ($aTmpFilter as $idTmp)
                $aReturn[$idTmp] = isset($aTmpLabels[$idTmp]) ? $aTmpLabels[$idTmp]
                    : $idTmp;
        }
        return $aReturn;
    }

    function _fmtDataCommandByPermission($sm, $aDataKey)
    {
        $aReturn = array();
        if (count($aDataKey)) {
            foreach ($aDataKey as $k => $row) {
                if (isset($row['permission']) && count($row['permission'])) {
                    $bFlagTmp = \Application\Model\Common::isPermissionGranted(
                        $sm,
                        $row['permission']
                    );
                    if (!$bFlagTmp)
                        continue;
                }
                if(isset($row['permission']))unset($row['permission']);
                $aReturn[$k] = $row;
            }
        }
        return json_encode($aReturn);
    }
    function _getDataFromResource(
        $oSm,
        $aIds,
        $sSource,
        $sIdField,
        $sLabelField)
    {
        $aReturn = array();
        foreach ($aIds as $sId) {
            $aRow = \Application\Model\Common::getResourceById(
                $oSm,
                $sSource,
                $sId,
                $sIdField
            );
            //            var_dump($aRow);
            if (count($aRow))
                $aReturn[$aRow[$sIdField]] = $sId."/".\YcheukfReport\Lib\ALYS\ALYSLang::_($aRow[$sLabelField]);
            else
                $aReturn[$sId] = empty($sId) ? "" : $sId;
        }
        return $aReturn;
    }
}
