<?php
namespace YcheukfReportExt\Report\Dictionary;

class Metric extends \YcheukfReport\Lib\ALYS\Report\Dictionary\Metric{


	public function __construct(){
		parent::__construct();
	}


	public function ALYSmetricFormat($fieldKey, $fieldValue, $aRowData=array()){
		$sRe = "";
		switch($fieldKey){
			case 'nc2pv' :
			case 'bounceRate':
			case 'visiteBack':
				$sRe = \YcheukfReport\Lib\ALYS\ALYSFormat::percent($fieldValue);
				break;
			case 'avgTimeLength' :
				$sRe = \YcheukfReport\Lib\ALYS\ALYSFormat::second4Time($fieldValue);
				break;
			case 'avgDepth' :
				$sRe = \YcheukfReport\Lib\ALYS\ALYSFormat::round($fieldValue);
				break;
			case 'pvbyvisitor' :
				$sRe = $aRowData['pvbyvisitor']."[".$aRowData['visitor'].']';
				break;
			case 'mex1_mixfield' :
				$sRe = number_format(((int)$aRowData['me2']/(int)$aRowData['me1'])*100, 2).'%';
				break;
			case 'mex2_mixfield' :
				$sRe = number_format(((int)$aRowData['me4']/(int)$aRowData['me3'])*100, 2).'%';
				break;
			case 'mdx1_mixfield' :
				$sRe = number_format(((int)$aRowData['md2']/(int)$aRowData['md1'])*100, 2).'%';
				break;
			case 'mdx2_mixfield' :
				$sRe = number_format(((int)$aRowData['md4']/(int)$aRowData['md3'])*100, 2).'%';
				break;
			default :
				$sRe = $fieldValue;

		}
		return $sRe;
	}

	public function ALYSmetricExpresion($field){
		switch($field){
			case 'bounceRate':
				$sEval='islandPageView/entryPageView';
				break;
			case 'visiteBack':
				$sEval='(visitor-newVisitor)/visitor';
				break;
			case 'avgTimeLength':
				$sEval='sumTimeLength/sumSession';
				break;
			case 'avgDepth':
				$sEval='sumDepth/sumSession';
				break;
			case 'nc2uc':
				$sEval='nc/uc';
				break;
				break;
			case 'nc2pv':
				$sEval='pageView/nc';
				break;
			default:
				$sEval=$field;
		}
		return $sEval;

	}


    //指标到字段名映射
	public function ALYSmetric2Field($field){
		if(preg_match('/_nosum/', $field)){
			$field=str_replace('/_nosum/','',$field)." as ".$field;
			return $field;
		}elseif(preg_match('/_mixfield/', $field)){
			$field="id as ".$field;
			return $field;
		}

		switch($field){
			case 'avgDepth' :
				$field=" sum(sumDepth)/sum(sumSession) as ".$field;
				break;
			case 'modified':
				$field = "modified as ".$field;
				break;

			case 'count_id':
//				$field = "count(id) as ".$field;
				$field = array($field=>array('$count'=>'id'));
				break;
			default :
				$field = array($field=>array('$sum'=>$field));
//				$field=" sum1(".$field.") as ".$field;

		}
		return $field;


	}

	public function ALYStableDefine($table){
		$condition = $this -> getConditionByInput();

		switch($table){
			case 'ABC':
				$table="(select * from StatsWeiboActivePost order by date desc) as ".$table;
				break;
			case 'ABC_total':
				$table="(select * from (select * from StatsWeiboActivePost where ".$condition." order by date desc) as t1 group by mid) as ".$table;
				break;


			default :
				$table=$table;

		}
		return $table;


	}

	public function ALYSdimen2Field($key, $selected,$type='',$table=''){

		if($key == 'timeslot'){
			$dateField = \YcheukfReport\Lib\ALYS\Report::getDateFeildByTable($type,$table);
			switch ($selected){
				case 'hour':
					$sReField = "hour";
					break;
				case 'day':
					$sReField = '[EXPRESSION]DATE_FORMAT('.$dateField.', "%Y-%m")';//需要告知后台这是一个表达式
					break;
				case 'week':
					$sReField = '[EXPRESSION]DATE_FORMAT('.$dateField.', "%x-%v")';
					break;
				case 'month':
					$sReField = '[EXPRESSION]DATE_FORMAT('.$dateField.', "%Y-%m")';
					break;
				case 'quarter':
                        $sReField = "[EXPRESSION]CONCAT(YEAR(`".$dateField."`),'-Q',QUARTER(`".$dateField."`))";
                        break;
				case 'year':
					$sReField = '[EXPRESSION]DATE_FORMAT('.$dateField.', "%Y")';
					break;
				default:
					$sReField = $dateField;
			}
		}elseif($selected=='province'){
			$sReField =  "[EXPRESSION]CONCAT(country, province)";
		}elseif($selected=='city'){
			$sReField =  "[EXPRESSION]CONCAT(country, province, city)";
		}elseif($selected=='channelGroupId'){
			$sReField =  "[EXPRESSION]CONCAT(channelPoolId, '".$this->resourceSplitChar."', {$selected})";
		}elseif($selected=='dbconfigop'
			|| $selected=='currentVersion'
			|| $selected=='newestVersion'
		){
			$sReField =  "customerid";
		}elseif(preg_match('/THIRDRELATIONID/i', $selected)){
			$sReField =  "id";
		}elseif(preg_match('/___id/i', $selected)){
			$sReField =  "id";
		}elseif(preg_match('/___split___/i', $selected)){
			list($sTmp, $sReField) = explode("___split___", $selected);
        }else{
			$sReField = $selected;
		}

		return $sReField;
	}

	public function ALYSgetDimenTypeOf($key){
		$quotes='';
		switch ($key){
			case 'channelPoolId':
			//case 'channelGroupId':
			case 'domainId':
				$quotes="";
				break;
			default :
				$quotes="'";
				break;
		}

		return $quotes;
	}

	public function getConditionByInput(){
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$aCondition = array();
		$dateField = \YcheukfReport\Lib\ALYS\Report::getDateFeildByTable();
		foreach($aInput['date'] as $aDate){
			$aCondition[]="(".$dateField." >= '".$aDate['s']."' and ".$dateField." <= '".$aDate['e']."')";
		}
		//filter
		if(is_array($aInput['filters'])){
			foreach($aInput['filters'] as $condition){
				$aCondition[]=$condition['key'].' '.$condition['op'].' '.$condition['value'];
			}
		}
		$sCondition = 1;
		if(is_array($aCondition)&&!empty($aCondition))$sCondition = implode(' and ',$aCondition);
		return $sCondition;
	}

}