<?php
/**
* CSV的输出的PLUGIN
*
*
* @author   ycheukf@gmail.com
* @package  Plugin
* @access   public
*/
namespace YcheukfReportExt\Report\Plugin;
class Csv extends \YcheukfReport\Lib\ALYS\Report\Plugin\Csv{
	public function __construct(){
		parent::__construct();
	}


	public function getCharset()
	{
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		
		$sReturn = "utf8";
		if (isset($aInput['output']['exportInfo']['charset'])) {
			$sReturn = $aInput['output']['exportInfo']['charset'];
		}
		if (in_array(strtolower($sReturn), array("utf8", "utf-8"))) {
			$sReturn = "UTF-8";
		}
		return $sReturn;
	}


	/**
	 *	负责csv的字符串的组织与输出

	 @return string 处理过后的csv 字符串
	 */

	function ALYSfmtOutputCsv(){
		$aOutput = \YcheukfReport\Lib\ALYS\Report\Start::getOutput();
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();

		// $sCsv = "\xEF\xBB\xBF";
		$sCsv = ($this->getCharset()=="UTF-8") ? "\xEF\xBB\xBF" : "";
		if($aInput['nodateFlag'] == false){
			foreach($aInput['date'] as $aTmp){
				$sCsv .= "#".$aTmp['s']."~".$aTmp['e']."\r\n";
			}
			$sCsv .= "\r\n\r\n";
		}
		if(isset($aOutput['flash.output'])){
			$sCsv .= $aOutput['flash.output'];
			$sCsv .= "\r\n\r\n";
		}
		if(isset($aOutput['total.output'])){
			$sCsv .= $aOutput['total.output'];
			$sCsv .= "\r\n\r\n";
		}
		$sCsv .= isset($aOutput['detail.output']) ? $aOutput['detail.output'] : "";
		// $sCsv .= "\r\n\r\n";

		return $sCsv;
	}



	function ALYSfmtListTitle($arrGroup)
	{
		return $arrGroup;
	}


	function ALYSfmtListData($arrListData)
	{
		return $arrListData;
	}
}
?>