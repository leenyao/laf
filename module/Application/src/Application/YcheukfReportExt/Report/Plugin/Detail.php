<?php
/**
* List的PLUGIN
*
* 负责维度与指标的格式化
*
* @author   ycheukf@gmail.com
* @package  Plugin
* @access   public
*/
namespace YcheukfReportExt\Report\Plugin;
class Detail extends \YcheukfReport\Lib\ALYS\Report\Plugin\Detail{
	public function __construct(){
		parent::__construct();
	}


	/**
	 *	负责转换维度配置至所需要的格式

	 @param array a 当前核心类所处理的数据结构
	 @return array 处理过后的数据结构
	 */
	public function ALYSfmt_dimen($a){
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$sDetailFormat = isset($aInput['custom']['detail_format']) ? $aInput['custom']['detail_format'] : "table";
		$sClassName = "\\".__NAMESPACE__."\\Detail".\YcheukfCommon\Lib\Functions::toCamelCase($sDetailFormat);
		$sFunc = __FUNCTION__;
		$o = new $sClassName();
		return $o->$sFunc($a);
	}

	/**
	 *	明细列表序号背景颜色

	 @return array 背景颜色数组
	 */
	public function ALYSgetListBgColor(){
		return array('ca9d50','bddef9','f8cb00','9dc800','ff9f54','00a8a8','de5c5c','b070b0','78a442','c2ba00','009fde');
	}

	/**
	 *	负责转换指标配置至所需要的格式

	 @param array a 当前核心类所处理的数据结构
	 @return array 处理过后的数据结构
	 */
	public function ALYSfmt_metric($a){
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$sDetailFormat = isset($aInput['custom']['detail_format']) ? $aInput['custom']['detail_format'] : "table";
		$sClassName = "\\".__NAMESPACE__."\\Detail".\YcheukfCommon\Lib\Functions::toCamelCase($sDetailFormat);
		$sFunc = __FUNCTION__;
		$o = new $sClassName();
		return $o->$sFunc($a);
	}



	/**
	 *	明细列表html列表包装的定制
	*@return string HTML字符串
	 */
	public function ALYSfmt_list_table($th,$html){
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$sDetailFormat = isset($aInput['custom']['detail_format']) ? $aInput['custom']['detail_format'] : "table";
		$sClassName = "\\".__NAMESPACE__."\\Detail".\YcheukfCommon\Lib\Functions::toCamelCase($sDetailFormat);
		$sFunc = __FUNCTION__;
		$o = new $sClassName();
		return $o->$sFunc($th,$html);
	}

	/**
	 *	明细列表html列表头的定制
	*@param array groups 列表头数据
	*@return string HTML字符串
	 */
	public function ALYSfmt_list_title($groups,$format='html'){

		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$sDetailFormat = isset($aInput['custom']['detail_format']) ? $aInput['custom']['detail_format'] : "table";
		$sClassName = "\\".__NAMESPACE__."\\Detail".\YcheukfCommon\Lib\Functions::toCamelCase($sDetailFormat);
		$sFunc = __FUNCTION__;
		$o = new $sClassName();
		return $o->$sFunc($groups,$format);
	}


	/**
	 *	明细列表html定制
	*@param array aryListData 列表数据
	*@return array 列表数据
	 */
	public function ALYSfmt_list($aryListData){
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$sDetailFormat = isset($aInput['custom']['detail_format']) ? $aInput['custom']['detail_format'] : "table";
		$sClassName = "\\".__NAMESPACE__."\\Detail".\YcheukfCommon\Lib\Functions::toCamelCase($sDetailFormat);
		$sFunc = __FUNCTION__;
		$o = new $sClassName();
		return $o->$sFunc($aryListData);
	}




}

