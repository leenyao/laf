<?php
/**
* List的PLUGIN
*
* 负责维度与指标的格式化
*
* @author   ycheukf@gmail.com
* @package  Plugin
* @access   public
*/
namespace YcheukfReportExt\Report\Plugin;
class DetailTable extends \YcheukfReport\Lib\ALYS\Report\Plugin\Detail{
	public function __construct(){
		parent::__construct();
	}


	/**
	 *	负责转换维度配置至所需要的格式
	 
	 @param array a 当前核心类所处理的数据结构
	 @return array 处理过后的数据结构
	 */
	public function ALYSfmt_dimen($a){
		$aRe = array("title" => \YcheukfReport\Lib\ALYS\ALYSLang::_('DIMEN'));
		foreach($a as $aTmp){
			$aTmpData = array();
			$aTmpData['pName'] = $aTmp['key'];
			$aTmpData['label'] = \YcheukfReport\Lib\ALYS\ALYSLang::_($aTmp['key']);
			$aTmpData['options'] = array();
			$aTmpData['selected'] = $aTmp['selected'];
			foreach($aTmp['options'] as $key2){
				$aTmpData['options'][$key2] = \YcheukfReport\Lib\ALYS\ALYSLang::_($key2);
			}
			if($aTmp['group'] != false)//不需要group
				$aRe['data'][] = $aTmpData;
		}
		return $aRe;
	}

	/**
	 *	明细列表序号背景颜色

	 @return array 背景颜色数组
	 */
	public function ALYSgetListBgColor(){
		return array('ca9d50','bddef9','f8cb00','9dc800','ff9f54','00a8a8','de5c5c','b070b0','78a442','c2ba00','009fde');
	}

	/**
	 *	负责转换指标配置至所需要的格式

	 @param array a 当前核心类所处理的数据结构
	 @return array 处理过后的数据结构
	 */
	public function ALYSfmt_metric($a){
		$aRe = array("title" => \YcheukfReport\Lib\ALYS\ALYSLang::_('METRIC'));
		foreach($a as $aTmp){
			$aTmpData = array();
			$aTmpData['pName'] = $aTmp['key'];
			$aTmpData['label'] = \YcheukfReport\Lib\ALYS\ALYSLang::_($aTmp['key']);
			$aTmpData['type'] = 'radio';
			$aTmpData['options'] = array('none'=>\YcheukfReport\Lib\ALYS\ALYSLang::_('none'), $aTmp['key'] => \YcheukfReport\Lib\ALYS\ALYSLang::_($aTmp['key']));
			$aTmpData['selected'] = $aTmp['show']==true ? $aTmp['key'] : 'none';
//			if($aTmp['show'] != false)//不需要group
			$aRe['data'][] = $aTmpData;
		}
		return $aRe;
	}



	/**
	 *	明细列表html列表包装的定制
	*@return string HTML字符串
	 */
	public function ALYSfmt_list_table($th,$html){
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$th_pack = '';
		////表头开始////
		if($this->_isPerspective())
		{
			//透视图不加行标 在 ALYSfmt_list_title 里加
			$th_pack .= "\n<thead>";
			$th_pack .= $th;
			$th_pack .= "\n</thead>";
		}
		else
		{
			$th_pack .= "\n<thead>";
			$th_pack .= "\n<tr _listtrtitle>\n";
			$th_pack .= $th;
			$th_pack .= "\n</tr>";
			$th_pack .= "\n</thead>";
		}
		//////表头结束////
		$sContent = "\n<div class='table-responsive report-detail'><table _listtablecss0 >".$th_pack.$html."\n</table></div>";
		return $sContent;
	}

	/**
	 *	明细列表html列表头的定制
	*@param array groups 列表头数据
	*@return string HTML字符串
	 */
	public function ALYSfmt_list_title($groups,$format='html'){

		$bIsPerspective = $this->_isPerspective();

		//
		$aDimens = array();
		$aMetrics = array();
		$aMetricKeys = array();
//var_dump($groups);
//		$html = "<th _listtdcss0 ><div>".\YcheukfReport\Lib\ALYS\ALYSLang::_('NO').('pdf'==$format?'':" <span _listThSpan _listspan0 >".\YcheukfReport\Lib\ALYS\ALYSLang::_('select')."</span>")."</div></th>";
		$html = "";
		$nIndex = 0;
		if(is_array($groups)){
			foreach($groups as $k => $aTmp){
				foreach($aTmp as $key=>$aGroup){

//		var_dump($aGroup);
					$key = $aGroup['key'];


					$aGroup['label'] =  (
						preg_match("/__split__modified/", $aGroup['label'])? 
							"MODIFIEDTIME" : (
							preg_match("/___subtr___op___id/", $aGroup['label'])? 
								"OPERATION" : 
								(
									preg_match("/___id/", $aGroup['label'])? 
									"ID" : 
									$aGroup['label']
								)
							)
					);

					$label = \YcheukfReport\Lib\ALYS\ALYSLang::_($aGroup['label']);
					if('dimen'==$k){
						//$label = \YcheukfReport\Lib\ALYS\ALYSLang::_($aGroup['key']);
						$tipLabel = "title='".\YcheukfReport\Lib\ALYS\ALYSLang::_($aGroup['key']."-tip")."'";
						$aDimens[] = $aGroup['key'];
					}else{
						$tipLabel = '';
						$aMetrics[$aGroup['key']] = $label;
						$aMetricKeys[$aGroup['key']] = $aGroup['key'];
					}
					$aThClass = isset($aGroup['thclass']) ? $aGroup['thclass'] : array('td150');
//var_dump($aThClass);
					$aTipClass = isset($aGroup['tipclass']) && count($aGroup['tipclass']) ? $aGroup['tipclass'] : array('jp_tip');
					$aThClass[] = "_".$k."listspan0";
					if($aGroup['sortAble'])
						$aThClass[] = 'sortable';
					if(isset($aGroup['orderbyClass']) && !empty($aGroup['orderbyClass']))
						$aThClass[] = $aGroup['orderbyClass'];
	//				var_export($aThClass);
					$sThClass = $sTipClass = "";

					//根据不同的输出类型处理样式
					switch($format){
						default:
						case 'html':
							$sTipClass = " class='".join(" ", $aTipClass)."'";
							$sThClass = " class='".join(" ", $aThClass)."'";
							break;
						case 'pdf'://pdf中需要把这些class分别替换
							foreach($aTipClass as $c){
								$sTipClass = " class='".$c."'";
							}
							foreach($aThClass as $c){
								$sThClass = " class='".$c."'";
							}
							break;
					}
//					var_dump($sThClass);
					$sDataPk = $nIndex==0 ? "data-pk=1" : "";
					$html .= "\n<th nowrap  data-key='{$key}' {$sDataPk} {$sThClass}><div>".$label.
						"<span   {$sTipClass} {$tipLabel}>&nbsp;</span></div></th>";
					$nIndex++;
				}
			}
		}

		//拼凑透视表的头
		$return_html = "";
		$sPerspective = '<th _listtdcss0 >&nbsp;</th>';
		if($bIsPerspective)
		{
			$return_html = "\n<tr _listtrtitle>\n";
			$return_html .= $html;
			$return_html .= "\n</tr>";
			if(is_array($aDimens))
			{
				foreach($aDimens as $dv)
				{
					$sPerspective .="\n<th nowrap><div>&nbsp;<span >&nbsp;</span></div></th>";
				}
			}

			$iRowSpan = count($aMetricKeys);
			if(is_array($aMetrics))
			{
				foreach($aMetrics as $mv)
				{
					$sPerspective .="\n<th colspan='".$iRowSpan."'><div>&nbsp;<span >".$mv."</span></div></th>";
				}
			}
			$return_html = '<tr>'.$sPerspective.'</tr>'.$return_html;
		}
		else
		{
			$return_html .= $html;
		}

		return $return_html;
	}


	/**
	 *	明细列表html定制
	*@param array aryListData 列表数据
	*@return array 列表数据
	 */
	public function ALYSfmt_list($aryListData){
		$html = '';
//		var_dump($aryListData);
		//多个时段
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$iCnt = count($aryListData);


		$iMultDataNum = count($aInput['date']);
		$nColSpan = 0;
		for($i=0; $i<$iCnt; $i++){
//			$html .= "\n<tr ".($i%2==0?'_listbodytrcss0':'_listbodytrcss1')." {$strClass}  align='right'>";

			$html2 = "";
			$sExtraLabel = null;
			$sSubtrLabel = null;
			$bDefaultResumeFlag = false;
			$nSubTrValue = false;
			if(isset($aryListData[$i]) && is_array($aryListData[$i]))
			{
				foreach($aryListData[$i] as $key =>$aTmp){
					$defaultAlign = $key=='dimens'?"left":"right";
					if(empty($nColSpan)){//计算col span
						$nColSpan = (isset($aryListData[$i]['dimens']) ? count($aryListData[$i]['dimens']):0) + (isset($aryListData[$i]['metric']) ? count($aryListData[$i]['metric']):0);
					}
					foreach($aTmp as $ii => $item){
//						if($ii==0)continue;
						$sMetric = isset($item['metric']) ? $item['metric'] : "";
						for($iMDN=1;$iMDN<$iMultDataNum;$iMDN++){
							if(!empty($item['label'.$iMDN])){
								$item['label'] .= '<br/>'.$item['label'.$iMDN];
							}
						}
						if($key == 'dimens' && $ii == 0) {
							continue;
						}
						if ($key == 'dimens' && $ii == 1) {
							$nSubTrValue = $item['tdVal'];
						}
						$item['tdKey'] = isset($item['tdKey']) ? $item['tdKey'] : $item['tdVal'];
						$colspan = isset($item['colspan'])?$item['colspan']:1;
						$rowspan = isset($item['rowspan'])?$item['rowspan']:1;
						if($colspan==0)continue;//被前面的td跨列
						$align = isset($item['align'])?$item['align']:$defaultAlign;
						$class = isset($item['className'])?$item['className']:'td150';
						$style = isset($item['style'])?$item['style']:'';
						if(!empty($item['tdClass']))
							$item['label'] = '<a href="javascript:void(0);" class="'.$item['tdClass'].'" data-key="'.$item['tdKey'].'"  val="'.$item['tdVal'].'">'.$item['label'].'</a>';
//						$key ="key='{$sMetric}' val='{$item['tdVal']}'" ;
						$sDataKey ="data-key='".$item['tdKey']."'  val='".htmlspecialchars($item['tdVal'])."'" ;
						$labelLTag = isset($item['labelLTag'])?$item['labelLTag']:'';
						$labelRTag = isset($item['labelRTag'])?$item['labelRTag']:'';
						$label = isset($item['label'])?$labelLTag.$item['label'].$labelRTag:'';

						if($key == 'dimens' && $ii == 1 && isset($aInput['custom']['subtr']) && $aInput['custom']['subtr']){
							
							$bExpend = (isset($aInput['custom']['subtr_expend']) && $aInput['custom']['subtr_expend']==false) ? 'icon-resize-full' : 'icon-resize-small';
							$label = '<span title="展开/收起" class="tr-trigger '.$bExpend.'" ></span>'.$label;
						}
						// 

						$sStyle = empty($style)?"":"style='{$style}'";
						$sAlign = "";
//						$sAlign = empty($align)?"":"align='{$align}'";
						$sColspan = $colspan==1?"":"colspan='{$colspan}'";
						$sRowspan = $rowspan==1?"":"rowspan='{$rowspan}'";

						if(isset($item['tdTitle']))
							$sTitle = "title='".$item['tdTitle']."'";
						else
							$sTitle = !isset($item['rowspan']) ? "title='".(empty($sRowspan)?strip_tags($item['label']):"")."'" : "";

                        $keyDivStyle = '';
                        if($item['tdKey'] == 'gossip_content'){
                            $keyDivStyle = 'style="word-wrap: normal; white-space: normal;"';
                        }

						if(!$bDefaultResumeFlag && $item['tdKey']=='default_resume' && $item['tdVal']==1)$bDefaultResumeFlag = true;
                        $html2 .= "\n<td {$sAlign} {$sColspan} {$sRowspan} _listbodytdcss0 {$sTitle} {$sDataKey} ><div {$keyDivStyle}>{$label}</div></td>";
                        if(preg_match('/___subtr___/i', $item['tdKey']))$sSubtrLabel = $item['label'];
					}
				}
			}
			$aTrclass = array();
			if(isset($aryListData[$i]['dimens'][0]['trClass']))$aTrclass[] = $aryListData[$i]['dimens'][0]['trClass'];
			if($bDefaultResumeFlag)$aTrclass[] = "selectedtr";
			$strClass = empty($aTrclass)?"":"class='".join(" ", $aTrclass)."'";
			$html .= "\n<tbody><tr ".($i%2==0?'_listbodytrcss0':'_listbodytrcss1')." {$strClass}  >";
			$html .= $html2;
			$html .= "\n</tr>";

            //自定义子行
			if(isset($aInput['custom']['subtr']) && $aInput['custom']['subtr']){
				$sExpendStype = isset($aInput['custom']['subtr_expend']) && $aInput['custom']['subtr_expend']==false ? "display:none" : "";
				preg_match_all("/data-subdata=[\"\'](.+?)[\"\']/i", $sSubtrLabel, $aMatch);
                $html .= "\n<tr class='tr-trigger-hidden'><td colspan='".($nColSpan-1)."'  style='".$sExpendStype."'> ";
                $html .= $aInput['custom']['subtr_function']($nSubTrValue);
                $html .= "\n</td></tr></tbody>";
            }else{
            	$html .= "</tbody>";
            }
			//特殊逻辑, 多加一行
//			var_dump($aInput['custom']['sDatasource']);
			if(isset($aInput['custom']['sDatasource']) && in_array($aInput['custom']['sDatasource'], array("viewtaskaction", "huntertaskapply"))){
				preg_match_all("/data-actionlog=\'(.+?)\'/i", $sExtraLabel, $aMatch);
//				echo($sExtraLabel);
				if(isset($aMatch[1][0])){
					$aActionLog = (json_decode($aMatch[1][0], 1));
//				var_dump($aActionLog);


					$sClass1 = $aActionLog['taskaction_array']['verify_status']==1 ? "current" : ($aActionLog['taskaction_array']['verify_status'] > 0 ? 'pass' : "");
					$sClass2 = $aActionLog['taskaction_array']['verify_status']==3 ? "current" : ($aActionLog['taskaction_array']['verify_status'] > 1 ? 'pass' : "");
					$sClass3 = $aActionLog['taskaction_array']['verify_status']==5 ? "current" : ($aActionLog['taskaction_array']['verify_status'] > 3 ? 'pass' : "");
					$sClass4 = $aActionLog['taskaction_array']['verify_status']==7 ? "current" : ($aActionLog['taskaction_array']['verify_status'] > 5 ? 'pass' : "");
					$sClass5 = $aActionLog['taskaction_array']['verify_status']==9 ? "current" : ($aActionLog['taskaction_array']['verify_status'] > 7 ? 'pass' : "");

					$sRecommendUser = (isset($aActionLog['resume_array']['name'])) ? $aActionLog['resume_array']['name'] : "";
					$html .= "\n<tr class='tr-trigger-hidden'><td colspan='".($nColSpan-1)."'  style='display:none'>";
					$html .= <<<OUTPUT
						<div class="detail-box">
							<div class="status-progress-wrapper">
							<ul class="status-progress-bar">
							<li class="{$sClass1}">1.人才推荐</li>
							<li class="{$sClass2}">2.平台审核</li>
							<li class="{$sClass3}">3.企业面试</li>
							<li class="{$sClass4}">4.入职录用</li>
							<li class="{$sClass5}">5.奖金获得</li>
							</ul>
							</div>
							<p>
								<span><strong>提交人:</strong>{$aActionLog['applyuser_array']['username']}</span>
								&nbsp;&nbsp;
								<span><strong>候选人:</strong>{$sRecommendUser}</span>
								&nbsp;&nbsp;
								<span><strong>状态:</strong>{$aActionLog['status_label']}</span>
								&nbsp;&nbsp;
								<span style="float:right;"><strong>时间:</strong>{$aActionLog['modified']}</span>
								<p><strong>备注:</strong>{$aActionLog['memo']}</p>
							</p>

						</div>
OUTPUT;
					$html .= "\n</td></tr>";
				}
			}
		}
		return $html;
	}



	//是否为透视图
	private function _isPerspective()
	{
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$sListType = $aInput['input']['detail']['type'];
		return 'perspective'==$sListType;
	}

}


/*

  "filterSelect":
 [
	{//维度或指标块
		"title":"\u7ef4\u5ea6",//块名称
		"data": //块配置
		[
			{
				"pName":"siteCodeDomain", //变量名
				"label":"\u57df\u540d\u5f52\u5e76\u7b49\u7ea7",
				"options":{"none":"none","siteCodeId":"\u8ddf\u8e2a\u4ee3\u7801\u6807\u8bc6","domainId":"\u4e3b\u57df"},//选项
				"selected":"siteCodeId"
			}


			 ,{"pName":"timeslot","label":"\u65e5\u671f","options":{"none":"none","timeslot":"\u65e5\u671f"},"type":"select","selected":"timeslot"},{"pName":"location","label":"\u5730\u57df","options":{"none":"none","country":"\u56fd\u5bb6","province":"\u56fd\u5bb6-\u7701\u4efd","city":"\u56fd\u5bb6-\u7701\u4efd-\u57ce\u5e02"},"type":"select","selected":"country"}
		]
	},
	*/
?>