<?php
/**
* List的PLUGIN
*
* 负责维度与指标的格式化
*
* @author   ycheukf@gmail.com
* @package  Plugin
* @access   public
*/
namespace YcheukfReportExt\Report\Plugin;
class DetailUl extends \YcheukfReport\Lib\ALYS\Report\Plugin\Detail{
	public function __construct(){
		parent::__construct();
	}


	/**
	 *	负责转换维度配置至所需要的格式

	 @param array a 当前核心类所处理的数据结构
	 @return array 处理过后的数据结构
	 */
	public function ALYSfmt_dimen($a){
		$aRe = array("title" => \YcheukfReport\Lib\ALYS\ALYSLang::_('DIMEN'));
		foreach($a as $aTmp){
			$aTmpData = array();
			$aTmpData['pName'] = $aTmp['key'];
			$aTmpData['label'] = \YcheukfReport\Lib\ALYS\ALYSLang::_($aTmp['key']);
			$aTmpData['options'] = array();
			$aTmpData['selected'] = $aTmp['selected'];
			foreach($aTmp['options'] as $key2){
				$aTmpData['options'][$key2] = \YcheukfReport\Lib\ALYS\ALYSLang::_($key2);
			}
			if($aTmp['group'] != false)//不需要group
				$aRe['data'][] = $aTmpData;
		}
		return $aRe;
	}

	/**
	 *	明细列表序号背景颜色

	 @return array 背景颜色数组
	 */
	public function ALYSgetListBgColor(){
		return array('ca9d50','bddef9','f8cb00','9dc800','ff9f54','00a8a8','de5c5c','b070b0','78a442','c2ba00','009fde');
	}

	/**
	 *	负责转换指标配置至所需要的格式

	 @param array a 当前核心类所处理的数据结构
	 @return array 处理过后的数据结构
	 */
	public function ALYSfmt_metric($a){
		$aRe = array("title" => \YcheukfReport\Lib\ALYS\ALYSLang::_('METRIC'));
		foreach($a as $aTmp){
			$aTmpData = array();
			$aTmpData['pName'] = $aTmp['key'];
			$aTmpData['label'] = \YcheukfReport\Lib\ALYS\ALYSLang::_($aTmp['key']);
			$aTmpData['type'] = 'radio';
			$aTmpData['options'] = array('none'=>\YcheukfReport\Lib\ALYS\ALYSLang::_('none'), $aTmp['key'] => \YcheukfReport\Lib\ALYS\ALYSLang::_($aTmp['key']));
			$aTmpData['selected'] = $aTmp['show']==true ? $aTmp['key'] : 'none';
//			if($aTmp['show'] != false)//不需要group
			$aRe['data'][] = $aTmpData;
		}
		return $aRe;
	}



	/**
	 *	明细列表html列表包装的定制
	*@return string HTML字符串
	 */
	public function ALYSfmt_list_table($th,$html){
		//////表头结束////
		$sContent = "\n<div class='ul-responsive report-detail'><ul class='dataview' >".$th.$html."\n</ul></div>";
		return $sContent;
	}

	/**
	 *	明细列表html列表头的定制
	*@param array groups 列表头数据
	*@return string HTML字符串
	 */
	public function ALYSfmt_list_title($groups,$format='html'){
		$return_html = "";
		return $return_html;
	}


	/**
	 *	明细列表html定制
	*@param array aryListData 列表数据
	*@return array 列表数据
	 */
	public function ALYSfmt_list($aryListData){
		$aUlData = array();
		$html = "";
		for($i=0; $i<count($aryListData); $i++){
			if(isset($aryListData[$i]) && is_array($aryListData[$i]))
			{
				foreach($aryListData[$i] as $key =>$aTmp){
					foreach($aTmp as $ii => $item){
						$aUlData[$i][$item['tdKey']] = $item;
					}
				}
			}
		}
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
		$sDetailSource = isset($aInput['custom']['detail_source']) ? $aInput['custom']['detail_source'] : "";

//		var_dump($aInput['custom']);
		for($i=0; $i<count($aUlData); $i++){

			$aTmp = $this->_getDetailSource($sDetailSource, $aUlData[$i]);
			$sClassName = "";
			extract($aTmp);
                        
                        $sCheckBoxHtml = is_null($sCheckBoxValue) ? "" : "<div class=\"checkbox\"><input type=\"checkbox\" value=\"{$sCheckBoxValue}\"></div>";
                        $sImgHtml = is_null($sImgSrc) ? "" : "<a target=\"_blank\" href=\"{$sImgURL}\" class=\"thumb\">{$sImgSrc}</a>";
                        $sClassName .= is_null($sCheckBoxValue)?"":"has-checkbox ";
                        $sClassName .= is_null($sImgSrc)?"":"has-thumb ";
			//html 模板
			$html .= <<<OUTPUT
				<li data-task-id="{$sULid}" {$sLiAttributes} class="{$sClassName}">
					{$sCheckBoxHtml}
					{$sImgHtml}					
					<h3>{$sHeader}</h3>
					<div>{$sLeftP1}</div>
					<div>{$sLeftP2}</div>
					<div class="ui-li-aside">
                                            <div>{$sRightP1}</div>
                                            <div>{$sRightP2}</div>
                                            <div class="links">{$sOperation}</div>
					</div>
				</li>
OUTPUT;
		}
		return $html;
	}

	function _getDetailSource($sDetailSource, $aRow){
		$aReturn = array();
                $aReportConfig = \YcheukfReport\Lib\ALYS\ALYSConfig::get();
                $oPhpRenderer = ($aReportConfig['smHandle']->get('zendviewrendererphprenderer'));
                $aAppConfig = $aReportConfig['smHandle']->get('config');
//		print_r($aRow);
//		var_dump($sDetailSource);
		switch($sDetailSource){
			//判断当前的ul模板是哪一个
			case "resume":
                                $aReturn['sULid'] = $aRow['resume___id']['tdVal'];
                                $aReturn['sLiAttributes'] = null;
				$aReturn['sImgSrc'] = $aRow['picture']['label'];
				$aReturn['sImgURL'] = $aRow['resume___id']['tdVal'];
				$aReturn['sCheckBoxValue'] = $aRow['resume___id']['tdVal'];
				$aReturn['sHeader'] = $aRow['resume___split___name']['label'];
				$aReturn['sLeftP1'] = $aRow['resume___split___title']['label'];
				$aReturn['sLeftP2'] = $aRow['resume___split___birthday']['label'];
				$aReturn['sRightP1'] = $aRow['phone']['label'];
				$aReturn['sRightP2'] = $aRow['percent']['label'];
				$aReturn['sOperation'] = ($aRow['resume___id']['label']);
			break;
			case "user":
                                $aReturn['sULid'] = $aRow['user___id']['tdVal'];
                                $aReturn['sLiAttributes'] = null;
				$aReturn['sImgSrc'] = $aRow['avatar']['label'];
				$aReturn['sImgURL'] = $aRow['user___id']['tdVal'];
				$aReturn['sCheckBoxValue'] = $aRow['user___id']['tdVal'];
				$aReturn['sHeader'] = $aRow['username']['label'];
				$aReturn['sLeftP1'] = $aRow['display_name']['label'];
				$aReturn['sLeftP2'] = $aRow['user___id']['label'];
				$aReturn['sRightP1'] = $aRow['phone']['label'];
				$aReturn['sRightP2'] = $aRow['email']['label'];
				$aReturn['sOperation'] = ($aRow['user___id']['label']);
			break;
                        case "viewtask":
                                $aReturn['sULid'] = $aRow['usercompany_task__id']['tdVal'];
                                $aReturn['sLiAttributes'] = null;
				$aReturn['sImgSrc'] = null;
				$aReturn['sImgURL'] = null;
				$aReturn['sCheckBoxValue'] = $aRow['usercompany_task__id']['tdVal'];
				$aReturn['sHeader'] = '<a target="_blank" href="'.$oPhpRenderer->url('zfcadmin/user_company/viewposition',array('id'=>$aRow['usercompany_task__id']['tdVal'])).'">'.$aRow['jflv3_label']['label'].'</a>';
				$aReturn['sLeftP1'] = $aRow['task_city_label']['label'].' / '.$aRow['expectation_label']['label']
                                                      .(($aRow['task_bonus']['label']>0)?(' / 赏金<font style="color:rgb(255, 156, 0);">￥'.$aRow['task_bonus']['label'].'</font>'):'');
				$aReturn['sLeftP2'] = '<div class="fd-date">'.$aRow['publisher_date']['label'].'发布</div>';
				$aReturn['sRightP1'] = $aRow['viewtask_uc_resume_count']['label'];
				$aReturn['sRightP2'] = null;
				$aReturn['sOperation'] = '<div data-command=\''.$aRow['viewtask_task_status']['label'].'\'></div>';
			break;
                        case "viewtask_home":
                                $aReturn['sULid'] = $aRow['usercompany_task__id']['tdVal'];
                                $aReturn['sLiAttributes'] = null;
				$aReturn['sImgSrc'] = null;
				$aReturn['sImgURL'] = null;
				$aReturn['sCheckBoxValue'] = null;
				$aReturn['sHeader'] = '<a target="_blank" href="'.$oPhpRenderer->url('zfcadmin/user_company/viewposition',array('id'=>$aRow['usercompany_task__id']['tdVal'])).'">'.$aRow['jflv3_label']['label'].'</a>';
				$aReturn['sLeftP1'] = $aRow['task_city_label']['label'].' / '.$aRow['expectation_label']['label']
                                                      .(($aRow['task_bonus']['label']>0)?(' / 赏金<font style="color:rgb(255, 156, 0);">￥'.$aRow['task_bonus']['label'].'</font>'):'');
				$aReturn['sLeftP2'] = '<div class="fd-date">'.$aRow['publisher_date']['label'].'发布</div>';
				$aReturn['sRightP1'] = $aRow['viewtask_uc_resume_count']['label'];
				$aReturn['sRightP2'] = null;
				$aReturn['sOperation'] = null;
			break;
                        case "viewtaskaction":
                                $aReportConfig = \YcheukfReport\Lib\ALYS\ALYSConfig::get();
                                $user_company = \Application\Model\Common::getUserCompanyResource($aReportConfig['smHandle']);
                                $aReturn['sULid'] = $aRow['viewtaskaction___id']['tdVal'];
                                $aReturn['sLiAttributes'] = 'data-resume-mail="'.$aRow['resume_mail']['label'].'" data-location="'.$user_company['address'].'" data-company-title="'.$user_company['title'].'" data-company-contacter="'.$user_company['contacter'].'" data-company-phone="'.$user_company['phone'].'"';
				$aReturn['sImgSrc'] = empty($aRow['resume_picture']['tdVal'])?('<i class="noimg"></i>'):$aRow['resume_picture']['label'];
				$aReturn['sImgURL'] = $oPhpRenderer->url('resumeinfo',array('resume_id'=>$aRow['resume_id']['tdVal']));
				$aReturn['sCheckBoxValue'] = null;
				$aReturn['sHeader'] = '<i class="mail'.($aRow['UL_viewtaskaction_verify_status']['tdVal']>1?0:1).'"></i>'
                                                    .(!empty($aRow['resume_attachment']['label'])?'<i class="resume-attachment"></i>':'')
                                                    .'<a target="_blank" href="'.$oPhpRenderer->url('resumeinfo',array('resume_id'=>$aRow['resume_id']['tdVal'])).'">'.$aRow['resume_name']['label'].'</a>';
				$aReturn['sLeftP1'] = $aRow['resume_gender']['label'].' / '
                                                      .$aRow['resume_m105_id']['label'].' / '
                                                      .(($aRow['resume_joinworkdate']['label']=='无')?'工作年限 / ':($aRow['resume_joinworkdate']['label'].'年 / '))
                                                      .$aRow['resume_m20_id']['label'].' / '
                                                      .'应聘职位：<span id="jobname-'.$aRow['viewtaskaction___id']['tdVal'].'">'.$aRow['task_title']['label'].'</span>';
				$aReturn['sLeftP2'] = '投递时间：'.$aRow['apply_time']['label'];
				$aReturn['sRightP1'] = $aRow['viewtaskaction___id']['label'];
				$aReturn['sRightP2'] = null;
				$aReturn['sOperation'] = '<div data-command=\''.$aRow['UL_viewtaskaction_verify_status']['label'].'\'></div>';
			break;
			default:
				echo __FILE__.":".__LINE__."  '".$sDetailSource."' is not defined";
				exit;
				break;
		}
		return $aReturn;
	}

}

