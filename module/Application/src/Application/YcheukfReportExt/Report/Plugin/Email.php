<?php
/**
* Email 输出的plugin
* 
* 负责EMAIL输出的扩展
*
* @author   ycheukf@gmail.com
* @package  Plugin
* @access   public
*/
namespace YcheukfReportExt\Report\Plugin;
class Email extends \YcheukfReport\Lib\ALYS\Report\Plugin\Email{
	public function __construct(){
		parent::__construct();
	}


	/**
	 *	负责Email发送

	 @param string staticKey 静态html链接的key
	 @return array 处理过后的email数组
	 @description 处理
	 */
	public function ALYSbefore_email($staticKey){
		$_ALYSconfig = \YcheukfReport\Lib\ALYS\ALYSConfig::get();
		$aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
        $oRouter = ($_ALYSconfig['smHandle']->get('httprouter'));
        $aParam = array('id' => $staticKey);

        $basePath = \YcheukfCommon\Lib\Functions::getBaseUrl($_ALYSconfig['smHandle']);
        //配置静态页面链接
        $sUrl = "http://".$basePath.'/'.$oRouter->assemble($aParam, array('name' => 'staticreport'));


		//配置email信息
		$aEmail = array();
		$mailBody = isset($aInput['custom']['email']['mailBody']) ? $aInput['custom']['email']['mailBody'] : "(本邮件由系统自动发送请勿回复)";
		$aEmail['subject'] = isset($aInput['custom']['email']['subject']) ? $aInput['custom']['email']['subject'] : '牵牛每日注册用户数-'.date('Y-m-d');
		$aEmail['toEmail'] =  isset($aInput['custom']['email']['toEmail']) ? $aInput['custom']['email']['toEmail'] : array('to'=>array('ruzhuo_feng@matrixdigi.com'), 'bcc'=>array("fengruzhuo@163.com"));//逗号分割
		$aEmail['body'] = <<<OUTPUT
			<p>{$mailBody}

			<p>请点击前往(需要登陆)
			<p><a href="{$sUrl}" target="_blank">{$sUrl}</a>
<p><BR><BR></p>
<p><img src="http://www.1000new.com/images/logo_small.png">
<p style="color:gray;">&nbsp;&nbsp;&nbsp;&nbsp;牵牛招聘（<a href="http://www.1000new.com" target="_blank">www.1000new.com</a>）是中国最大最垂直的“圈内人”招聘平台，专注于 广告传媒、游戏、IT互联网</p>

OUTPUT;
			//var_export($aEmail);
		return $aEmail;
	}
}
?>