<?php
function admin_breadcrumbs($oThis){
//    $oContainer = $oThis->ycheukfNavigationMenu()->getContainer4Breadcrum('admin');
//    echo($oThis->ycheukfNavigationBreadcrumbs());
//            echo($oThis->ycheukfNavigationBreadcrumbs()->render($oContainer));
    //                    echo($this->ycheukfNavigationMenu()->renderMenu('default', array('UlClass'=>'nav navbar-nav')));
//    return <<<OUTPUT
//    <div class="navigation" id="navigation">
//        <ol class="breadcrumb">
//            <li><a href="#">首页</a></li>
//            <li class="active">职位管理</li>
//        </ol>
//    </div>
//OUTPUT;
}

function admin_detail_toolbar($oThis, $detail_toolbar){
    $sReturn = "";
    $sReturn .= '<div class="form-inline" style="margin:20px;">';
        foreach($detail_toolbar as $key => $oElement){
            $sType = $oElement->getAttribute('type');
            $sFormFuncName = 'formYcf'.ucfirst(strtolower($sType));
            $sReturn .= '<div class="form-group" data-type="'.$sType.'">';
            $sReturn .= $oThis->$sFormFuncName($oElement);
            $sReturn .= '</div>';
        }
    $sReturn .= "</div>";
    return $sReturn;
}
//<a href='".$oThis->url($row['route'], (isset($row['params'])?$row['params']:array()))."'></a>
function admin_content_categories($oThis){
    $sReturn = "";
    if(!isset($oThis->categories) || count($oThis->categories)<1)return $sReturn;
    $sReturn .= "<ul class='ulsubmenu'>";
//    var_dump($oThis->categories);
    foreach($oThis->categories as $row){
        $sIconSpan = isset($row['iconCls'])?"<span class='licon ".$row['iconCls']."'></span>":"";
        $sLiClass = $row['active'] ? "class='active'":"";
        $sUrl = $oThis->url($row['route'], (isset($row['params'])?$row['params']:array()));
        $sReturn .= "<li data-url='{$sUrl}' {$sLiClass}><div>{$sIconSpan}<span class='label'>".$oThis->translate($row['label'])."</span></div><b class='selected'></b></li>";
    }
    $sReturn .= "</ul>";
    $sReturn .= <<<OUTPUT
        <script type="text/javascript">
        \$('ul.ulsubmenu li').each(function(){\$(this).click(function(){location.href = \$(this).attr('data-url')})});
        </script>
OUTPUT;
    return $sReturn;
}
//table-responsive
//, (isset($row['params'])?$row['params']:array())
function admin_ajaxdetail($ajaxlist_url){
    return <<<OUTPUT
        <div class="report-cantainer"></div>
        <script>
        var url = "{$ajaxlist_url}";
        $('.report-cantainer').dataTable({
            toolbar:oDetailToolBar ,
            source: url
        })
        </script>
OUTPUT;

}


function admin_form($oThis){

    // $oThis->ycheukfHeadscript()->prependFile( $oThis->basePath().'/js/air-datepicker/dist/js/i18n/datepicker.zh.js')->prependFile( $oThis->basePath().'/js/air-datepicker/dist/js/datepicker.min.js');
    // $oThis->ycheukfHeadscript()->prependFile( $oThis->basePath().'/js/air-datepicker/dist/js/datepicker.min.js');

    $oThis->ycheukfHeadscript()->prependFile( $oThis->basePath().'/js/jDate-master/dist/jDate.js');
    $oThis->ycheukfHeadlink()->prependStylesheet( $oThis->basePath().'/js/jDate-master/src/jDate.css');


    $form = $oThis->form;
//    var_dump($form);enctype
    $form->setAttribute('class', 'form-horizontal');
    $form->setAttribute('id', 'admincontent');
    $aNewFormElement = array();
    $sTmp = "";
    foreach($form->getElements() as $sSet=>$oElement){
//    var_dump($oElement);
        $sLabel = $oElement->getOption('label');
        $sTips = $oElement->getOption('tips');
        $sHelpName = $oElement->getOption('helpname');
        $bFullcolumns = $oElement->getOption('fullcolumns');
        $sType = $oElement->getAttribute('type');

        $sFormFuncName = ($sHelpName[0] == "\\") ? $sHelpName : 'formYcf'.ucfirst(strtolower($sHelpName));
        switch(strtolower($sType)){
                case 'button':
                case 'submit':
                case 'hidden':
//                    var_dump($oElement);
//                    echo $sFormFuncName;
                    $sTmp .= $oThis->$sFormFuncName($oElement);
                    break;
//                case 'radio':
//                    $zoneidOptions = $sElement->getValueOptions();
//                    $sTmp .=  $zoneidOptions[$sElement->getValue()];
//                    $sTmp .= $oThis->formradio($sElement);
//                    break;
            default:
                $sTmp .= "<div class='form-group'>";
                $oElement->setAttribute('class', 'form-control '.$oElement->getAttribute('class'));
                if ($bFullcolumns == 1) {
                    $sTmp .= "<div class='col-sm-12'>";
                    // var_dump($sFormFuncName);
                    // var_dump($oThis->$sFormFuncName($oElement));
                    $sTmp .= $oThis->$sFormFuncName($oElement);
    //                $sTmp .= $oThis->formErrors($oElement);
                    $sTmp .= "</div>";
                }else{
                    $sTmp .= $oThis->formYcfLabel($oThis->translate($sLabel), 'col-sm-2 control-label' ,$oElement->getAttribute('required'));
                    $sTmp .= "<div class='col-sm-10'>";
                    // var_dump($sFormFuncName);
                    // var_dump($oThis->$sFormFuncName($oElement));
                    $sTmp .= $oThis->$sFormFuncName($oElement);
    //                $sTmp .= $oThis->formErrors($oElement);
                    $sTmp .= "</div>";

                }

                //tips 暂时隐藏
                // $sTranslateTips = $oThis->translate($sTips);
                // if(!is_null($sTips) && !strpos($sTranslateTips, "_tips"))
                    // $sTmp .= "<div class='tips {$sTips}' style='margin-left:150px;'>".$sTranslateTips."</div>";
                $sTmp .= "</div>";
                break;
        }
    }

    //初始化form表单
    $sFormHeader = ($form->getOption("formheader"));
    $form->prepare();
    $form->removeAttribute('enctype');//不需要自动添加的上传类型
    $sReturn = "";
//    $sReturn .= "<div class='panel panel-default'>";
//        $sReturn .= '<div class="panel-heading">'.$oThis->translate($oThis->formTitle).'</div>';
        $sReturn .= "<div class='panel-body'>";
        $sReturn .= "   <div class='alertbox' style='width: 100%;height: 50px;'></div>";
        // $sReturn .= '<div class="alert alert-warning form-alert"><button type="button" class="close" data-dismiss="alert">&times;</button> <h4>警告!</h4>  <span></span></div>';

            $sReturn .= $oThis->form()->openTag($form);
            
                if (!empty($sFormHeader)) {
                        $sReturn .= "<div class=\"page-header\">
                      <h3>{$sFormHeader}</h3>
                    </div>";
                }
                $sReturn .= "<fieldset>";

                    $sReturn .= $sTmp;
                $sReturn .= "</fieldset>";
            $sReturn .= $oThis->form()->closeTag();
        $sReturn .= "</div>";
//    $sReturn .= "</div>";
    return $sReturn;
};

