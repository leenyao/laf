<?php
namespace Demo;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    public function onBootstrap(MvcEvent $event)
    {
        $app = $event->getApplication();
        $em  = $app->getEventManager();
        $sm  = $app->getServiceManager();


        /**
         * 报表配置
         */
        if (defined('APPLICATION_MODEL') && strtolower(APPLICATION_MODEL) !== "console") {
            $sClass1 = '\\'.LAF_NAMESPACE.'\YcheukfReportExt\\Report\\Dictionary\\Id2label';
            $sClass2 = '\\'.LAF_NAMESPACE.'\YcheukfReportExt\\Report\\Dictionary\\Metric';

            if (class_exists('\YcheukfReport\Lib\ALYS\ALYSFunction')) {
                \YcheukfReport\Lib\ALYS\ALYSFunction::setClassMapper('Id2label', $sClass1);
                \YcheukfReport\Lib\ALYS\ALYSFunction::setClassMapper('Metric', $sClass2);
            }
        }
    }

    /**
     * 配置Server
     * @return multitype:multitype:NULL  |\Publish\Form\Login
     */
    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                //通用service
                __NAMESPACE__.'\Service\Common' => __NAMESPACE__.'\Service\Common',

                //编辑记录触发器
                __NAMESPACE__.'\Service\Record' => __NAMESPACE__.'\Service\Record',
                'lf_admincontent' => __NAMESPACE__.'\Service\AdminContent',
            ),
        );
    }
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    // "YcheukfReportExt" => __DIR__ . '/src/' . __NAMESPACE__.'/YcheukfReportExt',

                ),
            ),
        );
    }
}
