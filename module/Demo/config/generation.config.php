<?php

$sLabelPre = "menu_";
return array(


        'user-member' => array(
            array(
                'name' => 'report',
                'label' => $sLabelPre . 'kol',
                'route' => 'zfcadmin/contentlist',
                'params' => array('type' => 'metadata', 'cid'=>1201),
               'pages' => array(
                    \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1201),
                    \YcheukfCommon\Lib\Functions::genNormalMenuConfig('order'),
                    \YcheukfCommon\Lib\Functions::genNormalMenuConfig('socialcontent'),


                    
                ),
            ),
            // array(
            //     'name' => 'user',
            //     'label' => $sLabelPre . 'user',
            //     'route' => 'zfcadmin/user',
            //     'params' => array('type' => 'user'),
            //    'pages' => array(
            //         \YcheukfCommon\Lib\Functions::genNormalMenuConfig('user'),
                    
            //     ),
            // ),
        ),
        'user-admin' => array(
            array(
                'name' => 'report',
                'label' => $sLabelPre . 'kol',
                'route' => 'zfcadmin/contentlist',
                'params' => array('type' => 'metadata', 'cid'=>1201),
               'pages' => array(
                    \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1201),
                    \YcheukfCommon\Lib\Functions::genNormalMenuConfig('order'),
                    \YcheukfCommon\Lib\Functions::genNormalMenuConfig('socialcontent'),


                    
                ),
            ),
            array(
                'name' => 'user',
                'label' => $sLabelPre . 'user',
                'route' => 'zfcadmin/contentlist',
                'params' => array('type' => 'user', 'cid'=>0),
               'pages' => array(
                    \YcheukfCommon\Lib\Functions::genNormalMenuConfig('user'),
                    
                ),
            ),
        ),
        'listeners' => array()

);