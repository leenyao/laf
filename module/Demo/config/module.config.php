<?php

$sLowerNameSpace = strtolower(LAF_NAMESPACE);

if (file_exists(__DIR__."/defined.php")) {
    require_once(__DIR__."/defined.php");
}
$sLabelPre = "menu_";
return array(

    'navigation' => require(__DIR__."/generation.config.php"),
    'module_permissions' => require(__DIR__."/module_permissions.config.php"),

    'dashboardroute_role2action' => array(
        'guest' => array("home"),
        'user-member' => array("zfcadmin/contentlist", array('type' => 'order', 'cid' => 0)),
        'superadmin' => array("zfcadmin/contentlist", array('type' => 'order', 'cid' => 0)),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => array(
            $sLowerNameSpace.'/layout' => __DIR__ . '/../view/layout/layout.phtml',
        ),
    ),

    'translator' => array(
        'locale' => 'zh_CN',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    //路由与layout映射关系
    'viewtemplate' => array(
        'home'  => $sLowerNameSpace.'/layout',
        'zfcuser/login'  => $sLowerNameSpace.'/layout',
    ),

    "apiservice" => array(
        "customer" =>array(),
        "resources" =>array(
            "reporta" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"b_report_a", 'memo'=>''),//\n\
            "order" => array("driver"=>"mysql", "table"=>"b_kol_order", 'memo'=>''),//\n\
            "order_resource" => array("driver"=>"mysql", "table"=>"b_kol_order_resource", 'memo'=>''),//\n\
            "order_resource_date" => array("driver"=>"mysql", "table"=>"b_kol_order_resource_date", 'memo'=>''),//\n\
            "socialcontent" => array("driver"=>"mysql", "table"=>"b_kol_socialcontent", 'memo'=>''),//\n\
        ),        
    ),
    'router' => array(
        'routes' => array(
            $sLowerNameSpace.'' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/'.$sLowerNameSpace,
                    'defaults' => array(
                        'controller'    => LAF_NAMESPACE.'\Controller\Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => LAF_NAMESPACE.'\Controller',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            LAF_NAMESPACE.'\Controller\Index' => LAF_NAMESPACE.'\Controller\IndexController',
            LAF_NAMESPACE.'\Controller\Demo' => LAF_NAMESPACE.'\Controller\DemoController',
        ),
    ),
);
