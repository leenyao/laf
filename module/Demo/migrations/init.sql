

DROP TABLE IF EXISTS `b_demodata`;
CREATE TABLE `b_demodata` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `memo` text  COMMENT '描述',
  `m101_id` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表数据';


-- trigger
DROP TRIGGER IF EXISTS `ti_b_demodata`;
DELIMITER ;;
CREATE TRIGGER `ti_b_demodata` AFTER INSERT ON `b_demodata` FOR EACH ROW BEGIN
    INSERT INTO system_trigger_log SET type = 'b_demodata', optype = 'i', resId = NEW.id,  crt_timestamp = CURRENT_TIMESTAMP();
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `tu_b_demodata`;
DELIMITER ;;
CREATE TRIGGER `tu_b_demodata` AFTER UPDATE ON `b_demodata` FOR EACH ROW BEGIN
    IF 
            OLD.id != NEW.id  or  OLD.modified != NEW.modified 
    THEN
        INSERT INTO system_trigger_log SET type = 'b_demodata', optype = 'u', resId = NEW.id, memo="some memo",  crt_timestamp = CURRENT_TIMESTAMP();
    END IF;
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `td_b_demodata`;
DELIMITER ;;
CREATE TRIGGER `td_b_demodata` AFTER DELETE ON `b_demodata` FOR EACH ROW BEGIN
        INSERT INTO system_trigger_log SET type = 'b_demodata', optype = 'd', resId = OLD.id,  crt_timestamp = CURRENT_TIMESTAMP();
  END
;;
DELIMITER ;




-- Records of rbac_permission
-- ----------------------------
-- lv1 1-100, 第一级权限分类, 仅归类用
INSERT INTO `rbac_permission`(id, perm_name, status, fid, perm_label) VALUES ('1', '', 1, 0, 'menu_report');
INSERT INTO `rbac_permission`(id, perm_name, status, fid, perm_label) VALUES ('2', '', 1, 0, 'menu_kol');

-- lv2 101-1000, 第二级权限分类, 仅归类用
INSERT INTO `rbac_permission`(id, perm_name, status, fid, perm_label) VALUES ('101', '', '1', 1, 'menu_report');
INSERT INTO `rbac_permission`(id, perm_name, status, fid, perm_label) VALUES ('102', '', '1', 2, 'menu_kol');

-- lv3 1001+, 第三级权限分类, 需要控制的路由
INSERT INTO `rbac_permission` (id,perm_name, status, fid, perm_label) VALUES (1001, 'zfcadmin/report/reporta', '1', '101', 'view');
INSERT INTO `rbac_permission` (id,perm_name, status, fid, perm_label) VALUES (1002, 'zfcadmin/contentlist/order', '1', '102', 'view');
INSERT INTO `rbac_permission` (id,perm_name, status, fid, perm_label) VALUES (1003, 'zfcadmin/contentedit/order', '1', '102', 'edit');
INSERT INTO `rbac_permission` (id,perm_name, status, fid, perm_label) VALUES (1004, 'zfcadmin/contentlist/socialcontent', '1', '102', 'view');
INSERT INTO `rbac_permission` (id,perm_name, status, fid, perm_label) VALUES (1005, 'zfcadmin/contentedit/socialcontent', '1', '102', 'edit');
INSERT INTO `rbac_permission` (id,perm_name, status, fid, perm_label) VALUES (1006, 'zfcadmin/contentlist/socialcontent2', '1', '102', 'view');
INSERT INTO `rbac_permission` (id,perm_name, status, fid, perm_label) VALUES (1007, 'zfcadmin/contentedit/socialcontent2', '1', '102', 'edit');
INSERT INTO `rbac_permission` (id,perm_name, status, fid, perm_label) VALUES (1008, 'zfcadmin/contentlist/metadata', '1', '102', 'view');
INSERT INTO `rbac_permission` (id,perm_name, status, fid, perm_label) VALUES (1009, 'zfcadmin/contentedit/metadata', '1', '102', 'view');

-- 需要 角色与资源的关系
INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (8, 1001);
INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (8, 1002);
INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (8, 1003);
INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (8, 1004);
INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (8, 1005);
INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (8, 1006);
INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (8, 1007);
INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (8, 1008);
INSERT INTO rbac_role_permission(role_id, perm_id) VALUES (8, 1009);



DROP TABLE IF EXISTS `b_report_a`;
CREATE TABLE `b_report_a` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `date` date NOT NULL DEFAULT '1970-01-01',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `d1` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 1',
  `d2` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon label',
  `m1` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m2` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m3` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m101_id` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `crt_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`user_id`, `date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





-- kol sql

DROP TABLE IF EXISTS `b_kol_order`;
CREATE TABLE `b_kol_order` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `d1` varchar(200) NOT NULL DEFAULT '' COMMENT '订单名称',
  `d2` varchar(200) NOT NULL DEFAULT '' COMMENT '客户名称',
  `d3` varchar(200) NOT NULL DEFAULT '' COMMENT '备注',
  `d4` varchar(200) NOT NULL DEFAULT '' COMMENT 'dimon label',
  `d5` varchar(200) NOT NULL DEFAULT '' COMMENT 'dimon label',
  `m1` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '刊例金额',
  `m2` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '内部金额',
  `m3` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m4` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m5` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m101_id` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `crt_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `d1` (`d1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

-- kol sql

DROP TABLE IF EXISTS `b_kol_order_resource`;
CREATE TABLE `b_kol_order_resource` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `order_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户订单ID',
  `d1` varchar(200) NOT NULL DEFAULT '' COMMENT '资源ID',
  `internal_cost` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '内部成本',
  `m101_id` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `crt_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  unique KEY `d1` (`order_id`, `d1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单所选中的资源';

DROP TABLE IF EXISTS `b_kol_order_resource_date`;
CREATE TABLE `b_kol_order_resource_date` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户订单ID',
  `d1` varchar(200) NOT NULL DEFAULT '' COMMENT '资源ID',
  `orderdate` date NOT NULL DEFAULT '1970-01-01',
  `m101_id` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `crt_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  unique KEY `d1` (`order_id`, `d1`, `orderdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单所选中的资源的日期';



DROP TABLE IF EXISTS `b_kol_socialcontent`;
CREATE TABLE `b_kol_socialcontent` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `d1` varchar(200) NOT NULL DEFAULT '' COMMENT '微博/微信 名',
  `d2` text COMMENT '链接',
  `d3` text COMMENT '备注',
  `d4` varchar(200) NOT NULL DEFAULT '' COMMENT '账号简介',
  `d5` varchar(200) NOT NULL DEFAULT '' COMMENT '联系人',
  `d6` varchar(200) NOT NULL DEFAULT '' COMMENT '联系方式',
  `d7` varchar(200) NOT NULL DEFAULT '' COMMENT '别名, 冗余 d1,m1202_id,m1203_id',
  `d8` varchar(200) NOT NULL DEFAULT '' COMMENT '',
  `d9` varchar(200) NOT NULL DEFAULT '' COMMENT '',
  `m1` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '粉丝数',
  `m2` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '成本',
  `m3` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m4` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '平均转发量',
  `m5` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '更新频率',
  `m6` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m7` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m8` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m9` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m10` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m101_id` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `m1203_id` tinyint(1) DEFAULT '1' COMMENT '1=>直发,2=>转发',
  `m1202_id` tinyint(1) DEFAULT '1' COMMENT '资源类型 1=>微博 2=>微信',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `crt_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  unique key `d1` (`d1`, `m1202_id`, `m1203_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源/微信-微博';




DROP VIEW IF EXISTS `system_relation_1201`;
CREATE VIEW `system_relation_1201` AS select `system_relation`.`cid` AS `cid`,`system_metadata`.`label` AS `clabel`,`system_relation`.`fid` AS `fid` from (`system_relation` join `system_metadata` on((`system_relation`.`cid` = `system_metadata`.`resid`))) where ((`system_relation`.`type` = 1009) and (`system_metadata`.`type` = 1009));


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1201, 1,'客户1');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1201, 2,'客户2');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1201, 3,'客户3');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1201, 4,'客户4');

INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1202, 1,'微博');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1202, 2,'微信');

INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1203, 1,'直发');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1203, 2,'转发');



INSERT INTO `b_kol_order` (`d1`, `d2`, `d3`, `m1`,`m2`) VALUES ('订单名称1', '2', '备注信息', '1000', 500);
INSERT INTO `b_kol_order` (`d1`, `d2`, `d3`, `m1`, `m2`) VALUES ('订单名称2', '3', '备注信息', '1000', 500);

-- 资源
-- 微博-直发
INSERT INTO `b_kol_socialcontent` (`user_id`, `d7`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `m1202_id`, `m1`, `m2`, `m3`, `m4`, `m5`, `m1203_id`) VALUES ('1', '芙蓉姐姐', '芙蓉姐姐-微博-直发', 'http://weibo.com/id/19', '备注1', '简介123', '联系人a', '18211112222', 1, '1111', '2222', '3333', '4444', '5555', 1);

INSERT INTO `b_kol_socialcontent` (`user_id`, `d7`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `m1202_id`, `m1`, `m2`, `m3`, `m4`, `m5`, `m1203_id`) VALUES ('2', '马云', '马云-微博-直发', 'http://weibo.com/id/20', '备注2', '简介222', '联系人b', '18211112222', 1, '333', '22', '11', '22', '33', 1);

-- 微信-直发
INSERT INTO `b_kol_socialcontent` (`user_id`, `d7`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `m1202_id`, `m1`, `m2`, `m3`, `m4`, `m5`, `m1203_id`) VALUES ('1', '芙蓉姐姐', '芙蓉姐姐-微信-直发', 'http://weibo.com/id/19', '备注1', '简介123', '联系人a', '18211112222', 2, '1111', '2222', '3333', '4444', '5555', 1);
INSERT INTO `b_kol_socialcontent` (`user_id`, `d7`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `m1202_id`, `m1`, `m2`, `m3`, `m4`, `m5`, `m1203_id`) VALUES ('2', '马云', '马云-微信-直发', 'http://weibo.com/id/20', '备注2', '简介222', '联系人b', '18211112222', 2, '333', '22', '11', '22', '33', 1);
-- 微博-转发
INSERT INTO `b_kol_socialcontent` (`user_id`, `d7`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `m1202_id`, `m1`, `m2`, `m3`, `m4`, `m5`, `m1203_id`) VALUES ('1', '芙蓉姐姐', '芙蓉姐姐-微博-转发', 'http://weibo.com/id/19', '备注1', '简介123', '联系人a', '18211112222', 1, '1111', '2222', '3333', '4444', '5555', 2);
INSERT INTO `b_kol_socialcontent` (`user_id`, `d7`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `m1202_id`, `m1`, `m2`, `m3`, `m4`, `m5`, `m1203_id`) VALUES ('2', '马云', '马云-微博-转发', 'http://weibo.com/id/20', '备注2', '简介222', '联系人b', '18211112222', 1, '333', '22', '11', '22', '33', 2);
-- 微信-转发
INSERT INTO `b_kol_socialcontent` (`user_id`, `d7`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `m1202_id`, `m1`, `m2`, `m3`, `m4`, `m5`, `m1203_id`) VALUES ('1', '芙蓉姐姐', '芙蓉姐姐-微信-转发', 'http://weibo.com/id/19', '备注1', '简介123', '联系人a', '18211112222', 2, '1111', '2222', '3333', '4444', '5555', 2);
INSERT INTO `b_kol_socialcontent` (`user_id`, `d7`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `m1202_id`, `m1`, `m2`, `m3`, `m4`, `m5`, `m1203_id`) VALUES ('2', '马云', '马云-微信-转发', 'http://weibo.com/id/20', '备注2', '简介222', '联系人b', '18211112222', 2, '333', '22', '11', '22', '33', 2);


-- 订单-资源关系
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1101', '1', '1');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1101', '1', '6');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1101', '2', '3');




-- 订单所占用资源
INSERT INTO `b_kol_order_resource` (`order_id`, `d1`, `internal_cost`) VALUES ('1', 1, 333);
INSERT INTO `b_kol_order_resource` (`order_id`, `d1`, `internal_cost`) VALUES ('1', 6, 444);
INSERT INTO `b_kol_order_resource` (`order_id`, `d1`, `internal_cost`) VALUES ('3', 3, 555);
-- INSERT INTO `b_kol_order_resource` (`order_id`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `d7`, `d8`, `d9`, `d10`, `d11`, `m1203_id`) VALUES ('1', 6, '马云-微博-转发', 22, 5000, 1000, 10000, 20000, "宋哲", "13022223333", "800", "1", 2);

-- INSERT INTO `b_kol_order_resource` (`order_id`, `d1`, `d2`, `d3`, `d4`, `d5`, `d6`, `d7`, `d8`, `d9`, `d10`, `d11`, `m1203_id`) VALUES ('2', 3, '芙蓉姐姐-微信-直发', 21, 2000, 1000, 10000, 20000, "刘三姐", "13022223333", "800", "2", 1);



-- 订单资源所占用的日期
INSERT INTO `b_kol_order_resource_date` (`order_id`,`d1`, `orderdate`) values (1, 1, "2017-01-10");
INSERT INTO `b_kol_order_resource_date` (`order_id`,`d1`, `orderdate`) values (1, 1, "2017-01-11");
INSERT INTO `b_kol_order_resource_date` (`order_id`,`d1`, `orderdate`) values (1, 1, "2017-01-13");
INSERT INTO `b_kol_order_resource_date` (`order_id`,`d1`, `orderdate`) values (1, 1, "2017-01-15");

INSERT INTO `b_kol_order_resource_date` (`order_id`,`d1`, `orderdate`) values (1, 6, "2017-01-11");
INSERT INTO `b_kol_order_resource_date` (`order_id`,`d1`, `orderdate`) values (1, 6, "2017-01-13");
INSERT INTO `b_kol_order_resource_date` (`order_id`,`d1`, `orderdate`) values (1, 6, "2017-01-22");

INSERT INTO `b_kol_order_resource_date` (`order_id`,`d1`, `orderdate`) values (2, 3, "2017-01-22");
INSERT INTO `b_kol_order_resource_date` (`order_id`,`d1`, `orderdate`) values (2, 3, "2017-01-23");

