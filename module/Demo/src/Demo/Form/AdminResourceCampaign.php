<?php

namespace Demo\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

use Zend\Form\Form;
use Zend\Form\Element;

class AdminResourceUser extends \Application\Form\Common
{
    public function _getTaskType($sm){
		$aTaskType = \Application\Model\Common::getResourceMetadaList($sm, 110);
		return $aTaskType;
	}

    public function __construct($oController)
    {
		$sm = $oController->getServiceLocator();
		$this->setController($oController,__CLASS__);
		$this->setServiceManager($sm);
		$sTableSource = "compaign";

//		var_dump($aLocations);
		$this->aFormElement = array(
			"id" => array(
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('table'=>$sTableSource,'tips'=>""),
			),
			"theme" => array( 
				'type' => 'text',
				'options' => array('table'=>$sTableSource),
			),
			"start" => array( 
				'type' => 'date',
				 'attributes' => array('class'=>'date-picker'),
				'options' => array('table'=>$sTableSource, 'format' => 'Y-m-d',),
			),
			"end" => array( 
				'type' => 'date',
				 'attributes' => array('class'=>'date-picker'),
				'options' => array('table'=>$sTableSource, 'format' => 'Y-m-d',),
			),
                        "memo" => array(
                            'type' => 'textarea',
                            'attributes' => array('class'=>'summernote'),
                            'options' => array('table'=>$sTableSource, 'helpname'=>'wysiwyg'),
                        ),
			"sort" => array( 
				'type' => 'text',
				'options' => array('table'=>$sTableSource),
			),
		    "Submitcancel" => array(
		        'type' => 'button',
		        'options' => array(
		            'table'=>$sTableSource,
		            'helpname'=>'Submitcancel',
		        ),
		    ),
		);
		$this->_fmtFormElements();

        // we want to ignore the name passed
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }

     /**
     * form过滤
     * (non-PHPdoc)
     * @see \Zend\Form\Form::getInputFilter()
     */
    public function getformInputFilter(){
        if (!$this->inputFilter || count($this->inputFilter) < 1) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
                        
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'theme',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'not_empty',
                            ),
                        ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'start',
                        'required' => true,
                        'validators' =>array(),
                    )
                )
            );
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'end',
                        'required' => true,
                        'validators' =>array(),
                    )
                )
            );
            
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}