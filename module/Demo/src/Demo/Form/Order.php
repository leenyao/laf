<?php
namespace Demo\Form;
class Order extends \Application\Form\Common{

    public function __construct($oController, $nStep=0, $sPkId=0)
    {


		$sm = $oController->getServiceLocator();
		$this->setController($oController,__CLASS__);
		$this->setServiceManager($sm);


    	switch (intval($nStep)) {
    		case 3:	//确认订单, 调整内部成本
              	$aOrderResourceV2 = $sm->get(LAF_NAMESPACE.'\Service\Common')->getOrderProperties($sPkId);

				// var_dump($aOrderResourceV2);

				// $aRelation1103 = \Application\Model\Common::getRelationList($sm,1101, $sPkId);
		    	// $aSocialcontent2 = \Application\Model\Common::getResourceList($this->getServiceManager(), array('source'=>'socialcontent2', 'pkfield'=>'id', 'labelfield'=>'d1'), array("id" => array('$in' => $aRelation1102) ));
				

				$this->aFormElement = array(
					"id" => array( 
						'type' => 'hidden',
						'attributes' => array('type'=>'hidden'),
						'options' => array('tips'=>""),
					),
					"kolorder" => array( 
						'type' => 'text',
						// 'attributes' => array('type'=>'text'),
						'options' => array(
							'tips'=>"",
							'data' => $aOrderResourceV2,
							'step' => $nStep,
							'helpname'=>'kolresourcepikcer',
						),

					),
					"Submitcancel" => array( 
						'type' => 'button',
						'options' => array(
							'helpname'=>'Submitcancel',
							"redirect_goback" => array(//提交后跳转
								"label" => "上一步: 调整排期",
								"urlarray" => array(
									"route"=>"zfcadmin/contentedit", 
									"type"=>"order", 
									"cid"=>0, 
									"id"=>"=saveid=", 
									"query"=>"step=2"
								),
							),
						),
					),

				);
    			break;
    		break;
    		case 2: //调整排期

    			//获取关系资源
				$aRelation1101 = \Application\Model\Common::getRelationList($sm,1101, $sPkId);
		    	$aSocialcontent = \Application\Model\Common::getResourceList($this->getServiceManager(), array('source'=>'socialcontent', 'pkfield'=>'id', 'labelfield'=>'d1'), array("id" => array('$in' => $aRelation1101) ));

				// var_dump($aSocialcontent);

				//订单资源
				// $aOrderResource = \Application\Model\Common::getResourceAllData($this->getServiceManager(), "order_resource", array("order_id"=>$sPkId, "d1"=>array('$in' => array_keys($aSocialcontent))));
				// var_dump(array("order_id"=>$sPkId, "d1"=>array('$in' => array_keys($aSocialcontent))));
				// var_dump($aOrderResource);

				$aOrderResourceV2 = array();
				foreach ($aSocialcontent as $sSocialId => $aRow) {
					// $aOrderResourceV2[$sSocialId]['order'] = $aRow;
					$aOrderResourceV2[$sSocialId]['schedule'] = \Application\Model\Common::getResourceList($this->getServiceManager(), array('source'=>'order_resource_date', 'pkfield'=>'id', 'labelfield'=>'orderdate'), array("order_id"=>$sPkId, "d1"=>$sSocialId) );

					
				}

				foreach ($aSocialcontent as $sSocialId => $sSocialLabel) {
					// if (!isset($aOrderResourceV2[$sSocialId])) {
						$aOrderResourceV2[$sSocialId]['resource'] = \Application\Model\Common::getResourceById($this->getServiceManager(), "socialcontent", $sSocialId);
					// }
				}
				// var_dump($aOrderResourceV2);

				// $aRelation1103 = \Application\Model\Common::getRelationList($sm,1101, $sPkId);
		    	// $aSocialcontent2 = \Application\Model\Common::getResourceList($this->getServiceManager(), array('source'=>'socialcontent2', 'pkfield'=>'id', 'labelfield'=>'d1'), array("id" => array('$in' => $aRelation1102) ));
				

				$this->aFormElement = array(
					"id" => array( 
						'type' => 'hidden',
						'attributes' => array('type'=>'hidden'),
						'options' => array('tips'=>""),
					),
					"kolorder" => array( 
						'type' => 'text',
						// 'attributes' => array('type'=>'text'),
						'options' => array(
							'tips'=>"",
							'data' => $aOrderResourceV2,
							'step' => $nStep,
							'helpname'=>'kolresourcepikcer',
						),

					),
					"Submitcancel" => array( 
						'type' => 'button',
						'options' => array(
							"redirect_goback" => array(//提交后跳转
								"label" => "上一步: 订单编辑",
								"urlarray" => array(
									"route"=>"zfcadmin/contentedit", 
									"type"=>"order", 
									"cid"=>0, 
									"id"=>"=saveid=", 
									"query"=>""
								),
							),
							"redirect" => array(//提交后跳转
								"label" => "下一步: 调整价格",
								"urlarray" => array(
									"route"=>"zfcadmin/contentedit", 
									"type"=>"order", 
									"cid"=>0, 
									"id"=>"=saveid=", 
									"query"=>"step=3"
								),
							),
							'helpname'=>'Submitcancel',
						),
					),

				);
    			break;
    		
    		default:

    			//客户
		    	$aM1201 = \Application\Model\Common::getResourceMetadaList($sm, 1201);

		    	$asocialcontent = \Application\Model\Common::getResourceList($this->getServiceManager(), array('source'=>'socialcontent', 'pkfield'=>'id', 'labelfield'=>'d1'));
				$this->aFormElement = array(
					"id" => array( 
						'type' => 'hidden',
						'attributes' => array('type'=>'hidden'),
						'options' => array('tips'=>""),
					),
					"d1" => array( 
						'type' => 'text',
						'attributes' => array('type'=>'text'),
						'options' => array('tips'=>""),
					),
					"d2" => array( 
						'type' => 'select',
						'attributes' => array('class'=>'select-tab', 'multiple'=>0),
						'options' => array(
		                    'value_options' => $aM1201,
						),
					),
					"relation___1101" => array( 
						'type' => 'select',
						'attributes' => array('class'=>'select-tab', 'multiple'=>1),
						'options' => array(
		                    'value_options' => $asocialcontent,
						),
					),

					"d3" => array( 
						'type' => 'textarea',
						'attributes' => array('type'=>'text'),
						'options' => array('tips'=>""),
					),
		            /*

		            "user_id" => array( //所属项目
						'type' => 'select',
						'attributes' => array('class'=>'select-tab'),
						'options' => array(
		                    'value_options' => $aUserId,
						),
					),
					"start" => array( 
						'type' => 'date',
						 'attributes' => array('class'=>'date-picker'),
						'options' => array('table'=>$sTableSource, 'format' => 'Y-m-d',),
					),
					"content" => array( 
		                'type' => 'textarea',
		                'attributes' => array( 'class'=>'summernote','required' => 'required'),
		                'options' => array('helpname'=>'wysiwyg'),
					),
					"code" => array( 
						'type' => 'text',
						'attributes' => array('type'=>'text'),
						'options' => array('tips'=>""),
					),
		            "relation___1007" => array( //所属项目
						'type' => 'select',
						'attributes' => array('class'=>'select-tab', 'multiple'=>1),
						'options' => array(
		                    'value_options' => $aM1007,
						),
					),    
					"in_file" => array( 
						'type' => 'file',
						'params' => array('id'=>'in_file'),
						'attributes' => array('data-filetype'=>'csv'),
						'options' => array('helpname'=>'fileupload'),
					),        
		            */
					"Submitcancel" => array( 
						'type' => 'button',
						'options' => array(
							"redirect" => array(//提交后跳转
								"label" => "下一步: 设置排期",
								"urlarray" => array(
									"route"=>"zfcadmin/contentedit", 
									"type"=>"order", 
									"cid"=>0, 
									"id"=>"=saveid=", 
									"query"=>"step=2"
								),
							),
							'helpname'=>'Submitcancel',
						),
					),

				);
    			break;
    	}
		// $this->setRedirectUrl('', array("redirect_json"=>json_encode()));

		$this->_fmtFormElements(__CLASS__);
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
	public function _fmtElement4Data($aData=array(), $aFormElement){
		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
        return $aFormElement;
    }
}