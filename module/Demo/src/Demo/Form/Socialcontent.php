<?php
namespace Demo\Form;
class socialcontent extends \Application\Form\Common{

    public function __construct($oController)
    {
		$sm = $oController->getServiceLocator();
		$this->setController($oController,__CLASS__);
		$this->setServiceManager($sm);
    	/*
    	
    	$aM1201 = \Application\Model\Common::getResourceMetadaList($sm, 1201);
    	
    	 */
    	$aM1202 = \Application\Model\Common::getResourceMetadaList($sm, 1202);
    	$aM1203 = \Application\Model\Common::getResourceMetadaList($sm, 1203);
    	
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
			"d7" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"m1202_id" => array( 
				'type' => 'select',
				'attributes' => array('class'=>'select-tab', 'multiple'=>0),
				'options' => array(
                    'value_options' => $aM1202,
				),
			),
			"m1203_id" => array( 
				'type' => 'select',
				'attributes' => array('class'=>'select-tab', 'multiple'=>0),
				'options' => array(
                    'value_options' => $aM1203,
				),
			),
			"d2" => array( 
				'type' => 'textarea',
				'attributes' => array('type'=>'textarea'),
				'options' => array('tips'=>""),
			),
			"d3" => array( 
				'type' => 'textarea',
				'attributes' => array('type'=>'textarea'),
				'options' => array('tips'=>""),
			),
			"d4" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"d5" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"d6" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"m1" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"m2" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"m3" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"m4" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"m5" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
            /*

            "user_id" => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab'),
				'options' => array(
                    'value_options' => $aUserId,
				),
			),
			"start" => array( 
				'type' => 'date',
				 'attributes' => array('class'=>'date-picker'),
				'options' => array('table'=>$sTableSource, 'format' => 'Y-m-d',),
			),
			"content" => array( 
                'type' => 'textarea',
                'attributes' => array( 'class'=>'summernote','required' => 'required'),
                'options' => array('helpname'=>'wysiwyg'),
			),
			"code" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
            "relation___1007" => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab', 'multiple'=>1),
				'options' => array(
                    'value_options' => $aM1007,
				),
			),    
			"in_file" => array( 
				'type' => 'file',
				'params' => array('id'=>'in_file'),
				'attributes' => array('data-filetype'=>'csv'),
				'options' => array('helpname'=>'fileupload'),
			),        
            */
			"Submitcancel" => array( 
				'type' => 'button',
				'options' => array(
					
					'helpname'=>'Submitcancel',
				),
			),

		);
		$this->_fmtFormElements(__CLASS__);
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
	public function _fmtElement4Data($aData=array(), $aFormElement){
		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
		if(isset($aData['id'])){
        /*
			$aRelation1007 = \Application\Model\Common::getRelationList($this->getServiceManager(),1007, $aData['id']);
            $aFormElement['relation___1008']['attributes']['value'] = $aRelation1008;
        */
        }
        return $aFormElement;
    }
}