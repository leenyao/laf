<?php

namespace Demo\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class AdminContent extends \Application\Service\AdminContent
{


    public function preSave($aForm, $sSource, $nCid, $nId){
        $aForm = parent::preSave($aForm, $sSource, $nCid, $nId);


       switch($sSource){
           case 'order'://若为用户, 则先增加 oauthuser
            if (isset($aForm['orderdata_schedule'])) {//判断排期时间是否准确
              if (is_string($aForm['orderdata_schedule'])) {
                $aForm['orderdata_schedule'] = array($aForm['orderdata_schedule']);
              }

              foreach ($aForm['orderdata_schedule'] as $key => $sSchedule) {
                if (!empty($sSchedule)) {
                  $aScheduleDate = $this->_fmtScheduleDate($sSchedule);
                  foreach ($aScheduleDate as $sDateTmp) {
                    if (empty($sDateTmp)) {
                      continue;
                    }
                    if (!preg_match("/\d{4}-\d{2}-\d{2}/i", $sDateTmp) || !\YcheukfCommon\Lib\Functions::isDate($sDateTmp)) {
                      \YcheukfCommon\Lib\Functions::throwErrorByCode(9999, "错误的日期格式:".$sDateTmp);
                    }
                  }
                }
              }
            }
            if (isset($aForm['orderdata_adjustvalue'])) {//调整的价格是否正确

              if (is_string($aForm['orderdata_adjustvalue'])) {
                $aForm['orderdata_adjustvalue'] = array($aForm['orderdata_adjustvalue']);
              }
              foreach ($aForm['orderdata_adjustvalue'] as $sTmp) {
                if (!preg_match("/\d+/", $sTmp)) {
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(9999, "价格需要是数字:".$sTmp);
                }
              }
            }

                        // \YcheukfCommon\Lib\Functions::throwErrorByCode(9999, "错误的日期格式:".$sDateTmp);

            // if (isset($aForm['orderdata_schedule'])) {//判断排期时间是否准确

           break;
       }

        return $aForm;
    }

   public function afterSave($aForm, $sSource, $sSaveId, $nCid=null, $nFid=null){
        parent::afterSave($aForm, $sSource, $sSaveId, $nCid, $nFid);
        switch($sSource){
            case 'socialcontent':
              //冗余资源名称
              $this->serviceManager->get(LAF_NAMESPACE.'\Service\Common')->syncSocialContentAlias($sSaveId);
              
            break;
            case 'order':
              if (isset($aForm['form_step'])) {
                switch (intval($aForm['form_step'])) {
                  case 3:
                    if (isset($aForm['orderdata_id'])) {

                      //兼容.字符串转数组
                      if (!is_array($aForm['orderdata_id'])) {
                        $aForm['orderdata_id'] = array($aForm['orderdata_id']);
                      }

                      if (is_string($aForm['orderdata_adjustvalue'])) {
                        $aForm['orderdata_adjustvalue'] = array($aForm['orderdata_adjustvalue']);
                      } 
                      //循环媒体资源ID
                      foreach ($aForm['orderdata_id'] as $nIndex => $sOrderResouceId) {


                        //设置订单内部金额
                        //
                        $aTmp = \Application\Model\Common::getResourceAllData($this->serviceManager, "order_resource", array("order_id"=>$sSaveId, "d1"=>$sOrderResouceId));

                        if (count($aTmp['dataset'])) {//update
                          foreach ($aTmp['dataset'] as $aRowTmp) {
                            $aParams = array(
                                'op'=> 'save',
                                'resource'=> 'order_resource',
                                'params' => array(
                                    'dataset' => array (
                                        array('internal_cost'=>$aForm['orderdata_adjustvalue'][$nIndex]),
                                    ),
                                    'where' => array('order_id'=>$sSaveId, 'd1'=>$sOrderResouceId),
                                ),
                            );
                            \YcheukfCommon\Lib\Functions::myUpdate($this->serviceManager, $aParams);

                          }
                        }else{//instert
                          $aParams = array(
                              'op'=> 'save',
                              'resource'=> 'order_resource',
                              'params' => array(
                                  'dataset' => array (
                                      array('internal_cost'=>$aForm['orderdata_adjustvalue'][$nIndex], 'order_id'=>$sSaveId, 'd1'=>$sOrderResouceId),
                                  ),
                              ),
                          );
                          \YcheukfCommon\Lib\Functions::myUpdate($this->serviceManager, $aParams);
                        }
                      }
                    }
                    break;
                  
                  case 2:
                  default:
                    if (isset($aForm['orderdata_id'])) {

                      if (!is_array($aForm['orderdata_id'])) {
                        $aForm['orderdata_id'] = array($aForm['orderdata_id']);
                      }
                      if (!is_array($aForm['orderdata_schedule'])) {
                        $aForm['orderdata_schedule'] = array($aForm['orderdata_schedule']);
                      }

                      //循环媒体资源ID
                      foreach ($aForm['orderdata_id'] as $nIndex => $sOrderResouceId) {
                        $aScheduleDate = $this->_fmtScheduleDate($aForm['orderdata_schedule'][$nIndex]);


                        $aParams = array(
                            'op'=> 'delete',
                            'resource'=> 'order_resource_date',
                            'params' => array(
                                'where' => array(
                                  'order_id'=>$aForm['id'],
                                  'd1'=>$sOrderResouceId
                                ),
                            ),
                        );
                        $aResult = \YcheukfCommon\Lib\Functions::myDelete($this->serviceManager, $aParams);

                        foreach ($aScheduleDate   as $sDate) {
                          $aParams = array(
                              'op'=> 'save',
                              'resource'=> 'order_resource_date',
                              'params' => array(
                                  'dataset' => array (
                                      array('order_id'=>$aForm['id'], 'd1'=>$sOrderResouceId, 'orderdate'=>$sDate),
                                  ),
                              ),
                          );
                          $aResult = \YcheukfCommon\Lib\Functions::myUpdate($this->serviceManager, $aParams);

                        }

                      }
                    }
                    break;
                }

              }
              //同步冗余的金额
              $this->serviceManager->get(LAF_NAMESPACE.'\Service\Common')->syncOrderAmount($sSaveId);
            break;
        }
    }

    private function _fmtScheduleDate($sSchedule='')
    {
      $sSchedule = trim($sSchedule);
      $aScheduleDate = array();
      if (!empty($sSchedule)) {
        $sSchedule = preg_replace("/\s+/", " ", $sSchedule);
        $sSchedule = str_replace(" ", ",", $sSchedule);
        $sSchedule = str_replace(";", ",", $sSchedule);
        $sSchedule = str_replace("\.", ",", $sSchedule);
        $aScheduleDate = explode(",", $sSchedule);
        $aScheduleDate = array_unique($aScheduleDate);
        $aScheduleDate = array_filter($aScheduleDate);

      }
      return $aScheduleDate;
    }
}
