<?php

namespace Demo\Service;



use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Common implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * 顶端通栏右边的菜单
     */
    public function getTopMenuRightHtml(){
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($this->serviceManager, $this->serviceManager->get('zfcuser_auth_service')->getIdentity());

        // $sUrl = $this->serviceManager->get('zendviewrendererphprenderer')->url('home');
        return <<<_EOF
        {$aRoles[0]} | 
_EOF;

    }

    public function getOrderProperties($sPkId)
    {

        //获取关系资源
        $aRelation1101 = \Application\Model\Common::getRelationList($this->serviceManager,1101, $sPkId);
        $aSocialcontent = \Application\Model\Common::getResourceList($this->serviceManager, array('source'=>'socialcontent', 'pkfield'=>'id', 'labelfield'=>'d1'), array("id" => array('$in' => $aRelation1101) ));

        // var_dump($aSocialcontent);
        $aOrderResourceV2 = array();

        //订单-资源
        // 内部成本价格
        $aTmp = \Application\Model\Common::getResourceAllData($this->serviceManager, "order_resource", array("order_id"=>$sPkId, "d1"=>array('$in' => array_keys($aSocialcontent))));
        if (count($aTmp['dataset'])) {
            foreach ($aTmp['dataset'] as $aRowTmp) {
                $aOrderResourceV2[$aRowTmp['d1']]['adjustvalue'] = $aRowTmp;
            }
        }

        foreach ($aSocialcontent as $sSocialId => $aRow) {
            // $aOrderResourceV2[$sSocialId]['order'] = $aRow;
            $aOrderResourceV2[$sSocialId]['schedule'] = \Application\Model\Common::getResourceList($this->serviceManager, array('source'=>'order_resource_date', 'pkfield'=>'id', 'labelfield'=>'orderdate'), array("order_id"=>$sPkId, "d1"=>$sSocialId) );

            
        }

        foreach ($aSocialcontent as $sSocialId => $sSocialLabel) {
            // if (!isset($aOrderResourceV2[$sSocialId])) {
                $aOrderResourceV2[$sSocialId]['resource'] = \Application\Model\Common::getResourceById($this->serviceManager, "socialcontent", $sSocialId);
            // }
        }
        return $aOrderResourceV2;

    }

    // 冗余资源名称
    public function syncSocialContentAlias($sSaveId){

         $aData = \Application\Model\Common::getResourceById($this->serviceManager, "socialcontent", $sSaveId);
         $sM1202 = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, 1202, $aData['m1202_id']);
         $sM1203 = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, 1203, $aData['m1203_id']);
         $aParams = array(
             'op'=> 'save',
             'resource'=> 'socialcontent',
             'params' => array(
                 'dataset' => array (
                     array('d1'=>$aData['d7']."-".$sM1202."-".$sM1203),
                 ),
                 'where' => array('id'=>$sSaveId),
             ),
         );
         \YcheukfCommon\Lib\Functions::myUpdate($this->serviceManager, $aParams);
    }
    //同步订单金额
    public function syncOrderAmount($nOrderId)
    {
        //获取订单管理的资源
        $aTmp = \Application\Model\Common::getResourceAllData($this->serviceManager, "order_resource", array("order_id"=>$nOrderId));

        $nOrgAmount = $nInternalAmount = 0;//成本 && 内部成本
        if (count($aTmp['dataset'])) {
          foreach ($aTmp['dataset'] as $aRowTmp) {//循环所有资源

            //获取订单-资源-排期
            $aTmp2 = \Application\Model\Common::getResourceAllData($this->serviceManager, "order_resource_date", array("order_id"=>$nOrderId, "d1"=>$aRowTmp['d1']));
            $nDeliverCount = isset($aTmp2['count']) ? $aTmp2['count'] : 0;//投放天数

            //资源成本
            $aTmp3 = \Application\Model\Common::getResourceById($this->serviceManager, "socialcontent", $aRowTmp['d1']);
            $nOrgCost = $aTmp3['m2'];

            //内部成本
            $nInternalCost = $aRowTmp['internal_cost'];

            $nOrgAmount += $nDeliverCount * $nOrgCost;
            $nInternalAmount += $nDeliverCount * $nInternalCost;

          }


          $aParams = array(
              'op'=> 'save',
              'resource'=> 'order',
              'params' => array(
                  'dataset' => array (
                      array('m1'=>$nOrgAmount, 'm2'=>$nInternalAmount),
                  ),
                  'where' => array('id'=>$nOrderId),
              ),
          );
          \YcheukfCommon\Lib\Functions::myUpdate($this->serviceManager, $aParams);

        }
    }

}