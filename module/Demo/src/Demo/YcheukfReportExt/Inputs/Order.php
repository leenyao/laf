<?php
namespace Demo\YcheukfReportExt\Inputs;
class Order extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
                // array(
                //     'key' => 'm101_id', //操作符
                //     'op' => 'in', //操作符=,in,like
                //     'value' => "(1)"//过滤的值
                // ),
			),
			'input'=>array(
				'detail'=>array(
					'is_editable' => true,
					'is_viewable' => true,
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
                                array(
    								'key'=> $sSource.'___id',
    								// 'links' => array(
    								// 	array(
    								// 		"label" => '调整排期',
    								// 		"url" => 'admin/edit/order/0/<id>?step=2',
    								// 	),
    								// 	array(
    								// 		"label" => '调整刊例价格',
    								// 		"url" => 'admin/edit/order/0/<id>?step=3',
    								// 	),
    								// ),
                                ),
								'd1',
								'd2',
								// 'm1',
								// 'm2',
								array(
                                    'key' => 'm101_id',
                                    // 'group' => false,
                                ),
								'modified',
                                array(
    								'key'=>'___subtr___op___id',
                                ),
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
			'custom' => array(
				'subtr' => true,
				'subtr_function' => function($nId) use ($sSource){
                    $aOrderData = \Application\Model\Common::getResourceById($this->sM, $sSource, $nId);

	              	$aOrderResourceData = $this->sM->get(LAF_NAMESPACE.'\Service\Common')->getOrderProperties($nId);

                    $sSourceHtml = "<table><tr class='tr-trigger-nocheckbox'>";
                    $sSourceHtml .= "<td>资源ID</td>";
                    $sSourceHtml .= "<td>资源名称</td>";
                    $sSourceHtml .= "<td>投放天数</td>";
                    $sSourceHtml .= "<td>成本</td>";
                    $sSourceHtml .= "<td>内部成本</td>";
                    $sSourceHtml .= "</tr>";
                    $nPrice0 = $nPrice1 = 0;
                    foreach ($aOrderResourceData as $sResouceId => $aRow) {
	                    $sSourceHtml .= "<tr class='tr-trigger-nocheckbox'>";

	                    $nTmp1 = (isset($aRow['adjustvalue']) ? $aRow['adjustvalue']['internal_cost'] : $aRow['resource']['m2']);
	                    $sSourceHtml .= "<td>".$sResouceId."</td>";
	                    $sSourceHtml .= "<td>".$aRow['resource']['d1']."</td>";
	                    $sSourceHtml .= "<td>".count($aRow['schedule'])."</td>";
	                    $sSourceHtml .= "<td>".($aRow['resource']['m2'])."</td>";
	                    $sSourceHtml .= "<td>".$nTmp1."</td>";
	                    $sSourceHtml .= "</tr>";
	                    $nPrice0 += count($aRow['schedule']) * $aRow['resource']['m2'];
	                    $nPrice1 += count($aRow['schedule']) * $nTmp1;
                    }
                    $sSourceHtml .= "</table>";



                    // var_dump($s1101);
                    $sOut = nl2br($aOrderData['d3']);
                    $sHtml = "<ul>
                    	<li>刊例金额: {$nPrice0}
                    	<li>内部金额: {$nPrice1}
                    	<li>备注: {$sOut}
                    	<li>资源明细: {$sSourceHtml}
                    ";
                    return $sHtml;
                }
            ),
		);
		$aInput = $this->_fmtDetailFilters(
            $sSource, 
            $aInput, 
            $this->getReportParams(), 
            array('m101_id'),  //filterable field
            array('modified'), //fulltext search field
            array() // default filters
        );
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}