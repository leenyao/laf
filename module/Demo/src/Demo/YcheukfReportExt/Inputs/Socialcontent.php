<?php
namespace Demo\YcheukfReportExt\Inputs;
class socialcontent extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
                // array(
                //     'key' => 'm101_id', //操作符
                //     'op' => 'in', //操作符=,in,like
                //     'value' => "(1)"//过滤的值
                // ),
			),
			'input'=>array(
				'detail'=>array(
					'is_editable' => true,
					'is_viewable' => true,
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								$sSource.'___id',
								'd1',
								'm1203_id',
								'm1202_id',
								'm1',
								'm2',
								// 'm3',
								'm4',
								'm5',
								array(
                                    'key' => 'm101_id',
                                    // 'group' => false,
                                ),
								'modified',
                                array(
    								'key'=>'___subtr___op___id',
                                ),
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
			'custom' => array(
				'subtr' => true,
				'subtr_function' => function($nId) use ($sSource){
                    $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sM);
                    $aSourceData = \Application\Model\Common::getResourceById($this->sM, $sSource, $nId);

                    $sHtml = "<p/>
                    	链接: {$aSourceData['d2']}
                    	账号简介: {$aSourceData['d4']}
                    	联系人: {$aSourceData['d5']}
                    	联系方式: {$aSourceData['d6']}
                    	备注: {$aSourceData['d3']}
                    ";
                    return nl2br($sHtml);
                }
            ),
		);
		$aInput = $this->_fmtDetailFilters(
            $sSource, 
            $aInput, 
            $this->getReportParams(), 
            array('m101_id', 'm1203_id', 'm1202_id'),  //filterable field
            array('d1','d2','d3','d4','d5'), //fulltext search field
            array() // default filters
        );
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}