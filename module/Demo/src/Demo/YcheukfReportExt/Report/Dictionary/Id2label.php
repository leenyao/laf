<?php
namespace Demo\YcheukfReportExt\Report\Dictionary;


/**
 * id转label类
 *
 * 负责将db类中获得id转变成相应的label
 *
 * @author   ycheukf@gmail.com
 * @package  Dictionary
 * @access   public
 */
class Id2label extends \YcheukfReportExt\Report\Dictionary\Id2label
{

	public function ALYSchgId2Label($aDimenkey2selected=array(), $aryIdSet=array(), $aConfig=array()){
		$this->addDimonFunc();
		return parent::ALYSchgId2Label($aDimenkey2selected, $aryIdSet, $aConfig);
	}

	/**
	 * 增加字典
	 */
	private function addDimonFunc()
	{
		$sDimonKey = "d2";
		$this->addDimenTrigger($sDimonKey, function ($aIds, $sm) use ($sDimonKey)
		{
			$aryReturn = array();
			$aryReturn = $this->_getDataFromMetaData(1201, array($sDimonKey=>$aIds), $sDimonKey, $sm);
            return $aryReturn;
		});

		$sDimonKey = "socialcontent__split__d7";
		$this->addDimenTrigger($sDimonKey, function ($aIds, $sm) use ($sDimonKey)
		{
			$aryReturn = array();
			$aryReturn = $this->_getDataFromMetaData(1202, array($sDimonKey=>$aIds), $sDimonKey, $sm);
            return $aryReturn;
		});

		$sDimonKey = "order___id";
		$this->addDimenTrigger($sDimonKey, function ($aIds, $sm) use ($sDimonKey)
		{
			$aryReturn = $aDataKey = array();
			$nTmpCid = 0;

		    $sSource = str_replace('___id', '', $sDimonKey);
			foreach ($aIds as $key => $v) {
		        $aDataKey['edit'] = array(
		            'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_(
		                'edit'
		            ),
		            'data' => $this->_geturl(
		                'zfcadmin/contentedit',
		                array(
		                    'cid' => $nTmpCid,
		                    'id' => $v,
		                    'type' => $sSource
		                )
		            ),
		            'attributes' =>  array('target' => '_blank'),
		            'type' => 'url',
		            'permission' => array(
		                'zfcadmin/contentedit/' . $sSource,
		            ),
		        );

		        $aDataKey['edit_step2'] = array(
		            'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_(
		                '调整排期'
		            ),
		            'data' => $this->_geturl(
		                'zfcadmin/contentedit',
		                array(
		                    'cid' => $nTmpCid,
		                    'id' => $v,
		                    'type' => $sSource
		                )
		            )."?step=2",
		            'attributes' =>  array('target' => '_blank'),
		            'type' => 'url',
		            'permission' => array(
		                'zfcadmin/contentedit/' . $sSource,
		            ),
		        );

		        $aDataKey['edit_step3'] = array(
		            'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_(
		                '调整内部价格'
		            ),
		            'data' => $this->_geturl(
		                'zfcadmin/contentedit',
		                array(
		                    'cid' => $nTmpCid,
		                    'id' => $v,
		                    'type' => $sSource
		                )
		            )."?step=3",
		            'attributes' =>  array('target' => '_blank'),
		            'type' => 'url',
		            'permission' => array(
		                'zfcadmin/contentedit/' . $sSource,
		            ),
		        );
			    // links

		        // $aDataKey['view'] = array(
		        //     'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_(
		        //         'view'
		        //     ),
		        //     'data' => $this->_geturl(
		        //         'zfcadmin/contentview',
		        //         array(
		        //             'cid' => $nTmpCid,
		        //             'id' => $v,
		        //             'type' => $sSource
		        //         )
		        //     ),
		        //     'attributes' => array("target" => "_blank",),
		        //     'type' => 'url',
		        // );

			    $sDataKey = $this->_fmtDataCommandByPermission(
			        $sm,
			        $aDataKey
			    );
			    $aryReturn[$v] = '<span data-command=\''. $sDataKey . '\'>' . $v . '</span>';
			}
            return $aryReturn;
		});

		
	}
}