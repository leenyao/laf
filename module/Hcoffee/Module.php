<?php
namespace Hcoffee;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(\Zend\Mvc\MvcEvent $event)
    {
        $app = $event->getApplication();
        $em  = $app->getEventManager();
        $sm  = $app->getServiceManager();
        //触发记录修改
        $em->getSharedManager()->attach("*", 'lf_record_update', function ($e) use ($sm){
                $sm->get("Hcoffee\Service\Record")->onUpdate($e);
        });
        //触发记录新增
        $em->getSharedManager()->attach("*", 'lf_record_insert', function ($e) use ($sm){
                $sm->get("Hcoffee\Service\Record")->onInsert($e);
        });
    }
//lf_record_insert
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    /**
     * 配置Server
     * @return multitype:multitype:NULL  |\Publish\Form\Login
     */
    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'Hcoffee\Service\Common' => 'Hcoffee\Service\Common',
                'Hcoffee\Service\Record' => 'Hcoffee\Service\Record',
                'lf_admincontent' => 'Hcoffee\Service\AdminContent',
            ),
        );
    }

}
