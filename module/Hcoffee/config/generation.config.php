<?php

$sLabelPre = "menu_";
return array(
    'user-admin' => array(
        array(
            'name' => 'hcoffee_order',
            'label' => $sLabelPre . 'hcoffee_order',
            'route' => 'zfcadmin/contentlist',
            'params' => array('type' => 'hcoffee_order', 'cid' => 0),
            'permission' => array(
            ),
           'pages' => array(
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('hcoffee_order'),
            ),
        ),
        array(
            'name' => 'hcoffee_items',
            'label' => $sLabelPre . 'hcoffee_items',
            'route' => 'zfcadmin/contentlist',
            'params' => array('type' => 'hcoffee_items', 'cid' => 0),
            'permission' => array(
            ),
           'pages' => array(
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('hcoffee_items'),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('hcoffee_category'),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1001),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('view_item_option'),
            ),
        ),
        // array(
        //     'name' => 'activities',
        //     'label' => $sLabelPre . 'activities',
        //     'route' => 'zfcadmin/contentlist',
        //     'params' => array('type' => 'activities', 'cid' => 0),
        //     'permission' => array(
        //     ),
        //    'pages' => array(
        //         \YcheukfCommon\Lib\Functions::genNormalMenuConfig('activities'),
        //     ),
        // ),
    ),
    'listeners' => array()
);