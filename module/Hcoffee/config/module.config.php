<?php
return array(
    //登陆后, 角色所跳转的route
    'dashboardroute_role2action' => array(
        'guest' => array("home"),
        'user-member' => array("zfcadmin/contentlist", array('type' => 'content', 'cid' => 0)),
        'superadmin' => array("zfcadmin/contentlist", array('type' => 'hcoffee_order', 'cid' => 0)),
    ),
    'translator' => array(
        'locale' => 'zh_CN',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'opconfig' => array(//运营配置
        'order_notification' => array(
            'mail_title' => '您有一笔新订单! 请及时处理',
            'mail_body' => "订单编号[1], <br/>\n<br/>\n 顾客 [2] 于 [3] 购买了以下商品 [4] <br/>\n [7] <br/>\n 送到 [5]，付款方式是:[6], 请及时处理。",
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(__DIR__ . '/../view'),
    ),
    'weixin' => array(
        'appid' => 'wx9256997286acab62',
        'secret' => 'f828889d7e0cbd6cb3826b748fa28745',
        'token' => 'd41d8cd98f00b204e9800998ecf8427e',
        'EncodingAESKey' => 'kwPoXEIlmCj8HAIRkE5YxUmDoMQ6HDyvZgbzZAHKvl8',
        'mch_id' => '1260258801',
        'mch_key' => '389073f7170772ce86e00fe56c3df0ae',
        'paymentnotify_url' => 'http://hcoffee.leenyao.com/wechat/paynotify',
        'wxlogin_redirect_url' => 'http://hcoffee.leenyao.com/wxlogin',
        'hcoffee_redirect_url' => 'http://hcoffee.leenyao.com/module/hcoffee/index.html#/auth?state=category.list',
        'welcometxt' => "您好！欢迎关注 Hcoffee!",
        'autoreply' => array(
            'NEWESTPROMOTIONS' => '敬请期待',
            'debug' => 'http://hcoffee.leenyao.com/wxlogin?redirect_url=http://hcoffee.leenyao.com/demo/index/debug',
            'DEFAULT' => 'Hcoffee微信外卖即将上线，敬请期待. 详情请咨询 021-62716097',
        ),
        'custommenu' => array(
            "外卖点单" => array(
                'type'=>'view',
                'params' => array("外卖点单", 'view', 'http://hcoffee.leenyao.com/wxlogin?redirect_url='.urlencode('http://hcoffee.leenyao.com/module/hcoffee/index.html#/auth?state=category.list'))
            ),
            "会员中心" => array(
                'type'=>'button',
                'params' => array(
                    array("我的订单", 'view', 'http://hcoffee.leenyao.com/wxlogin?redirect_url='.urlencode('http://hcoffee.leenyao.com/module/hcoffee/index.html#/auth?state=order.list')),
                    array("我的地址", 'view', 'http://hcoffee.leenyao.com/wxlogin?redirect_url='.urlencode('http://hcoffee.leenyao.com/module/hcoffee/index.html#/auth?state=contact.list')),
                ),
            ),
            "最新活动" => array(
                'type'=>'click',
                'params' => array("最新活动", 'view', 'http://mp.weixin.qq.com/s?__biz=MzI3MzAxNTI2OQ==&mid=207377129&idx=1&sn=ed7901418f950d3cca304f3bc43da693&scene=0#rd')
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Hcoffee\Controller\Index' => 'Hcoffee\Controller\IndexController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'hcoffee' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/hcoffee',
                    'defaults' => array(
                        'controller'    => 'Hcoffee\Controller\Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Hcoffee\Controller',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'navigation' => require(__DIR__."/generation.config.php"),
    "apiservice" => array(
        "metadata-usage" =>array(
            'memo' => 'Hcoffee 1000-1100',
            'dictionary' => array(
                1001 => array(
                    'memo' => "商品属性",
                    'resid' => "自增id",
                    'label' => "属性名称",
                    'p_resid' => "hcoffee_category.id",
                ),
                1002 =>  array(
                    'memo' => "商品属性值选项",
                    'resid' => "自增id",
                    'label' => "属性选项名称",
                    'p_type' => "1001",
                    'p_resid' => "m.1001.resid",
                ),
                1003 =>  array(
                    'memo' => "商品分类状态",
                    'resid' => "自增id",
                    'label' => "商品分类状态, 1=>上架, 2=>下架, 3=>删除",
                ),
                1004 =>  array(
                    'memo' => "商品状态",
                    'resid' => "自增id",
                    'label' => "商品状态, 1=>上架, 2=>下架, 3=>删除",
                ),
                1005 =>  array(
                    'memo' => "订单状态",
                    'resid' => "自增id",
                    'label' => "订单状态, 1=>正常, 2=>删除",
                ),
                1006 =>  array(
                    'memo' => "订单-商品状态",
                    'resid' => "自增id",
                    'label' => "订单状态, 1=>正常, 2=>删除",
                ),
                1007 =>  array(
                    'memo' => "商品-商品属性",
                    'resid' => "hcoffee_items.id",
                    'label' => "m.1001.resid",
                ),
                1008 =>  array(
                    'memo' => "支付方式",
                    'resid' => "自增id",
                    'label' => "1=>货到付款, 2=>微信支付",
                ),
                1009 =>  array(
                    'memo' => "接受下单通知的email列表",
                    'resid' => "自增id",
                    'label' => "email",
                ),
                1010 =>  array(
                    'memo' => "商品属性-金额",
                    'resid' => "自增id",
                    'label' => "属性选项-金额",
                    'p_type' => "1001",
                    'p_resid' => "m.1001.resid",
                ),
            ),
        ),
        "resources" =>array(
            "hcoffee_category" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"hcoffee_category", 'memo'=>'商品分类'),//\n\
            "hcoffee_items" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"hcoffee_items", 'memo'=>'商品信息'),//\n\
            "hcoffee_order" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"hcoffee_order", 'memo'=>'订单'),//\n\
            "hcoffee_order_oplog" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"hcoffee_order_oplog", 'memo'=>'订单备注记录'),//\n\
            "view_hcoffee_order" => array("driver"=>"mysql",  "table"=>"view_hcoffee_order", 'memo'=>'订单'),//\n\
            "hcoffee_order_items" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"hcoffee_order_items", 'memo'=>'订单内商品列表'),//\n\
            "hcoffee_order_items_options" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"hcoffee_order_items_options", 'memo'=>'订单内商品选项'),//\n\
            "view_order_item_option" => array("driver"=>"mysql",  "table"=>"view_order_item_option", 'memo'=>'订单内商品视图'),//\n\
            "activities" => array("driver"=>"mysql",  "table"=>"b_activities", 'memo'=>'推广活动'),//\n\
            "view_order_item" => array("driver"=>"mysql",  "table"=>"view_order_item_option", 'memo'=>'订单内商品视图'),//\n\
            "view_items_options" => array("driver"=>"mysql",  "table"=>"view_items_options", 'memo'=>'商品选项列表'),//\n\
            "system_matadata_1001" => array("driver"=>"mysql",  "table"=>"system_matadata_1001", 'memo'=>''),//\n\
            "system_matadata_1002" => array("driver"=>"mysql",  "table"=>"system_matadata_1002", 'memo'=>''),//\n\
            "system_matadata_1005" => array("driver"=>"mysql",  "table"=>"system_matadata_1005", 'memo'=>''),//\n\
            "view_item_option" => array("driver"=>"mysql",  "table"=>"view_item_option", 'memo'=>''),//\n\
            "view_item_option_1001" => array("driver"=>"mysql",  "table"=>"view_item_option_1001", 'memo'=>''),//\n\
        ),
        "customer" =>array(
            'wxuser' => array(
                "class"=>"\Hcoffee\Restapi\Wxuser", 
                "restfulflag"=>1,  
                'memo'=>'通过微信登录的用户',
                "d:entity"=>array(
                    "id" => array(
                        "d:type"=>"string",
                        "d:memo"=>"用户登陆后的token",
                    ),
                    "openid" => array(
                        "d:type"=>"string",
                        "d:post-required"=>true,
                    ),
                ),
            ),
            'activities' => array(
                "class"=>"\Hcoffee\Restapi\Activities", 
                'memo'=>'推广活动',
                "finished"=>1,  
                "restfulflag"=>1, 
                "d:entity"=>array(
                    "id" => array(
                        "d:type"=>"string",
                    ),
                    "thumbnails" => array(
                        "d:type"=>"string",
                        "d:memo"=>"缩略图",
                        "d:post-required"=>true,
                    ),
                    "postname" => array(
                        "d:type"=>"string",
                        "d:memo"=>"title",
                        "d:post-required"=>true,
                    ),
                    "description" => array(
                        "d:type"=>"string",
                        "d:memo"=>"摘要",
                        "d:post-required"=>true,
                    ),
                    "content" => array(
                        "d:type"=>"string",
                        "d:memo"=>"活动内容",
                        "d:post-required"=>true,
                    ),
                ),
            ),
            'my_contact' => array(
                "class"=>"\Hcoffee\Restapi\Mycontact", 
                'memo'=>'用户联系方式',
                "finished"=>1,  
                "restfulflag"=>1, 
                "d:entity"=>array(
                    "id" => array(
                        "d:type"=>"string",
                    ),
                    "name" => array(
                        "d:type"=>"string",
                        "d:memo"=>"用户名",
                        "d:post-required"=>true,
                    ),
                    "address" => array(
                        "d:type"=>"string",
                        "d:memo"=>"地址",
                        "d:post-required"=>true,
                    ),
                    "phone" => array(
                        "d:type"=>"string",
                        "d:memo"=>"手机",
                        "d:post-required"=>true,
                    ),
                ),
            ),
            'orderpay_cb' => array(
                "class"=>"\Hcoffee\Restapi\Orderpaycb", 
                "restfulflag"=>1,  
                'memo'=>'付款回调, entity参考 orderpay', 
            ),
            'dictionary' => array(
                "class"=>"\Hcoffee\Restapi\Dictionary", 
                "restfulflag"=>1,  
                'memo'=>'枚举字典', 
                "d:entity"=>array(
                    "order_status" => array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"订单状态列表",
                        "d:resource"=>"system_matadata_1005",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                                "d:alias"=>"resid",
                            ),
                            'label' => array(
                                "d:type"=>"string",
                                "d:alias"=>"label",
                            ),
                        ),
                    ),
                    "payments" => array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"付款方式列表",
                        "d:resource"=>"system_matadata_1008",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                                "d:alias"=>"resid",
                            ),
                            'label' => array(
                                "d:type"=>"string",
                                "d:alias"=>"label",
                            ),
                        ),
                    ),
                    "payments_status" => array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"付款状态列表",
                        "d:resource"=>"system_matadata_155",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                                "d:alias"=>"resid",
                            ),
                            'label' => array(
                                "d:type"=>"string",
                                "d:alias"=>"label",
                            ),
                        ),
                    ),
                ),
            ),
            'paymentconfig' => array(
                "class"=>"\Hcoffee\Restapi\Paymentconfig", 
                "restfulflag"=>1,
                "finished"=>1,
                'memo'=>'付款配置',
                "d:entity"=>array(
                    "order_id" => array(
                        "d:type"=>"string",
                        "d:memo"=>"订单id",
                    ),
                    "pay_config" => array(
                        "d:type"=>"string",
                        "d:memo"=>"付款的配置信息. json字符串",
                    ),
                    "payment_id" => array(
                        "d:type"=>"string",
                        "d:memo"=>"付款方式id",
                        "d:alias"=>"m156_id",
                    ),
                    "payment_label" => array(
                        "d:type"=>"string",
                        "d:memo"=>"付款方式label",
                        "d:alias"=>"m156_label",
                    ),
                    "payment_status_id" => array(
                        "d:type"=>"string",
                        "d:memo"=>"付款状态id",
                        "d:alias"=>"m155_id",
                    ),
                    "payment_status_label" => array(
                        "d:type"=>"string",
                        "d:memo"=>"付款状态label",
                        "d:alias"=>"m155_label",
                    ),
                ),
            ),
            'order' => array(
                "class"=>"\Hcoffee\Restapi\Order", 
                "restfulflag"=>1,
                "finished"=>1,
                "query"=>array(
                    'order_status' => array(
                        "d:type"=>"int",
                        "d:memo"=>"订单状态",
                    ),
                    'payment_status' => array(
                        "d:type"=>"int",
                        "d:memo"=>"订单付款状态",
                    ),
                ),
                'memo'=>'订单',
                "d:entity"=>array(
                    "id" => array(
                        "d:type"=>"string",
                        "d:memo"=>"订单id",
                    ),
                    "no" => array(
                        "d:type"=>"string",
                        "d:memo"=>"订单id",
                    ),
                    "user_id" => array(
                        "d:type"=>"int",
                        "d:memo"=>"用户id",
                    ),
                    "contact_id" => array(
                        "d:type"=>"int",
                        "d:post-required"=>true,
                        "d:memo"=>"联系人id",
                    ),
                    "name" => array(
                        "d:type"=>"string",
                        "d:memo"=>"联系人姓名",
                    ),
                    "phone" => array(
                        "d:type"=>"string",
                        "d:memo"=>"联系人手机",
                    ),
                    "address" => array(
                        "d:type"=>"string",
                        "d:memo"=>"联系人地址",
                    ),
                    'memo' => array(
                        "d:type"=>"string",
                        "d:post-required"=>true,
                        "d:memo"=>"顾客备注",
                    ),
                    "payment_id" => array(
                        "d:type"=>"int",
                        "d:post-required"=>true,
                        "d:memo"=>"付款方式",
                        "d:alias"=>"m1008_id",
                    ),
                    "payment_label" => array(
                        "d:type"=>"string",
                        "d:memo"=>"付款方式",
                        "d:alias"=>"m1008_label",
                    ),
                    "payment_status_id" => array(
                        "d:type"=>"int",
                        "d:memo"=>"付款状态",
                        "d:alias"=>"m155_id",
                    ),
                    "payment_status_label" => array(
                        "d:type"=>"string",
                        "d:memo"=>"付款状态",
                        "d:alias"=>"m155_label",
                    ),
                    "payment_config" => array(
                        "d:type"=>"string",
                        "d:memo"=>"付款参数",
                        "d:alias"=>"pay_config",
                    ),
                    "created_time" => array(
                        "d:type"=>"string",
                        "d:memo"=>"下单时间",
                    ),
                    "status" => array(
                        "d:type"=>"int",
                        "d:memo"=>"订单状态.id",
                        "d:alias"=>"m1005_id",
                    ),
                    "status_label" => array(
                        "d:type"=>"string",
                        "d:memo"=>"订单状态.label",
                        "d:alias"=>"m1005_label",
                    ),
                    "amount" => array(
                        "d:type"=>"float",
                        "d:memo"=>"订单金额",
                    ),
                    "items" => array(
                        "d:type"=>"list-entity",
                        "d:post-required"=>true,
                        "d:memo"=>"商品列表",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                            ),
                            'item_id' => array(
                                "d:type"=>"int",
                                "d:post-required"=>true,
                            ),
                            'label' => array(
                                "d:type"=>"string",
                                "d:alias"=>"label",
                            ),
                            'category_id' => array(
                                "d:type"=>"int",
                            ),
                            'category_label' => array(
                                "d:type"=>"string",
                            ),
                            'count' => array(
                                "d:type"=>"int",
                                "d:post-required"=>true,
                                "d:memo"=>"数量",
                            ),
                            'memo' => array(
                                "d:type"=>"string",
//                                "d:post-required"=>true,
                                "d:memo"=>"顾客备注",
                            ),
                            'price' => array(
                                "d:type"=>"float",
                                "d:memo"=>"单价",
                            ),
                            'attrs' => array(
                                "d:type"=>"list-entity",
                                "d:post-required"=>true,
                                "d:memo"=>"属性列表",
                                "d:entity" => array(
                                    "id" => array(
                                        "d:type"=>"int",
                                        "d:post-required"=>true,
                                        "d:alias"=>"m1001_id",
                                    ),
                                    "label" => array(
                                        "d:type"=>"string",
                                        "d:alias"=>"m1001_label",
                                    ),
                                    "selected_id" => array(
                                        "d:type"=>"int",
                                        "d:post-required"=>true,
                                        "d:alias"=>"m1002_id",
                                    ),
                                    "selected_label" => array(
                                        "d:type"=>"string",
                                        "d:alias"=>"m1002_label",
                                    ),
                                    "price" => array(
                                        "d:type"=>"float",
                                        "d:alias"=>"m1010_label",
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            'itemcategory' => array(
                "class"=>"\Hcoffee\Restapi\Itemcategory", 
                "finished"=>1,  
                "restfulflag"=>1,  
                'memo'=>'商品分类',
                "d:entity"=>array(
                    "id" => array(
                        "d:type"=>"string",
                    ),
                    "icon" => array(
                        "d:type"=>"string",
                        "d:urlflag"=>true,
                    ),
                    "is_orderable" => array(
                        "d:type"=>"bool",
                        "d:default"=>"true",
                        "d:memo"=>"是否可下单",
                        "d:alias"=>"m1003_id",
                    ),
                    "label" => array(
                        "d:type"=>"string",
                        "d:memo"=>"分类名称",
                    ),
                    "items"=>array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"商品列表",
                        "d:entity" => array(
                            "id" =>array(
                                "d:type"=>"string",
                            ),
                            "icon" => array(
                                "d:type"=>"string",
                                "d:urlflag"=>true,
                            ),
                            "label" =>array(
                                "d:type"=>"string",
                                "d:memo"=>"商品名称",
                            ),
                            "price" =>array(
                                "d:type"=>"float",
                                "d:memo"=>"价格",
                            ),
                            "attrs" =>array(
                                "d:type"=>"list-entity",
                                "d:memo"=>"商品属性列表",
                                "d:entity"=>array(
                                    "id" =>array(
                                        "d:type"=>"string",
                                        "d:alias"=>"m1001_id",
                                    ),
                                    "label" =>array(
                                        "d:type"=>"string",
                                        "d:alias"=>"m1001_label",
                                    ),
                                    "is_multple" =>array(
                                        "d:type"=>"bool",
                                        "d:memo"=>"true=>多选",
                                    ),
                                    "options" =>array(
                                        "d:type"=>"list-entity",
                                        "d:memo"=>"商品属性值列表",
                                        "d:entity"=>array(
                                            "id" =>array(
                                                "d:type"=>"string",
                                                "d:alias"=>"m1002_id",
                                            ),
                                            "label" =>array(
                                                "d:type"=>"string",
                                                "d:alias"=>"m1002_label",
                                            ),
                                            "price" =>array(
                                                "d:type"=>"float",
                                                "d:alias"=>"m1010_label",
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);