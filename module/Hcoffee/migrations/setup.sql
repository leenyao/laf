
DROP TABLE IF EXISTS `hcoffee_category`;
CREATE TABLE `hcoffee_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(250) NOT NULL DEFAULT '',
  `icon` text,
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `m1003_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

DROP TABLE IF EXISTS `hcoffee_items`;
CREATE TABLE `hcoffee_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'system_users.id',
  `icon` text,
  `price` float(8,2) NOT NULL DEFAULT 0 COMMENT 'item price',
  `label` varchar(250) NOT NULL DEFAULT '',
  `m1004_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

DROP TABLE IF EXISTS `hcoffee_order`;
CREATE TABLE `hcoffee_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no` varchar(250) NOT NULL DEFAULT '',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'system_users.id',
  `contact_id` int(8) unsigned NOT NULL DEFAULT '0' COMMENT 'system_contact.id',
  `m1005_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '',
  `m1008_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '',
  `amount` float(8, 2) NOT NULL DEFAULT '0' COMMENT '金额',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '联系人名字',
  `address` text NOT NULL COMMENT '',
  `phone` varchar(255) NOT NULL DEFAULT '' COMMENT '',
  `memo` text COMMENT '',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  UNIQUE KEY `no` (`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

DROP TABLE IF EXISTS `hcoffee_order_items`;
CREATE TABLE `hcoffee_order_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL COMMENT '`hcoffee_order`.id',
  `item_id` bigint(20) unsigned NOT NULL COMMENT '`hcoffee_items`.id',
  `count` int(8) unsigned NOT NULL DEFAULT '0' COMMENT 'item count',
  `price` float(8,2) NOT NULL DEFAULT 0 COMMENT 'item price',
  `memo` text COMMENT '',
  `amount` float(8,2) NOT NULL DEFAULT '0' COMMENT '',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `m1006_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单-下单的商品';


DROP TABLE IF EXISTS `hcoffee_order_items_options`;
CREATE TABLE `hcoffee_order_items_options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `father_id` bigint(20) unsigned NOT NULL COMMENT '`hcoffee_order_items`.id',
  `m1001_id` bigint(20) unsigned NOT NULL COMMENT '',
  `m1002_id` bigint(20) unsigned NOT NULL COMMENT '',
  `m1010_label` float(8,2) NOT NULL DEFAULT 0 COMMENT '',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>active,2=>delete',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `father_id` (`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单-下单的商品-属性选项';




-- ----------------------------
-- Table structure for `hcoffee_order_oplog`
-- ----------------------------
DROP TABLE IF EXISTS `hcoffee_order_oplog`;
CREATE TABLE `hcoffee_order_oplog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_users.id',
  `order_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'hcoffee_order.id',
  `log` text,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>active,2=>delete',
  `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
   KEY `order_id`(`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP VIEW IF EXISTS `view_payment` ;
CREATE  VIEW `view_payment` AS select `system_matadata_156`.`label` AS `m156_label`,`system_matadata_155`.`label` AS `m155_label`,`system_payment`.`id` AS `id`,`system_payment`.`order_id` AS `order_id`,`system_payment`.`paymentorder_id` AS `paymentorder_id`,`system_payment`.`m155_id` AS `m155_id`,`system_payment`.`m156_id` AS `m156_id`,`system_payment`.`memo` AS `memo`,`system_payment`.`pay_config` AS `pay_config`,`system_payment`.`pay_response` AS `pay_response`,`system_payment`.`status` AS `status`,`system_payment`.`modified` AS `modified`,`hcoffee_order`.`user_id` AS `user_id` from (((`system_payment` left join `system_matadata_155` on((`system_matadata_155`.`resid` = `system_payment`.`m155_id`))) left join `system_matadata_156` on((`system_matadata_156`.`resid` = `system_payment`.`m156_id`))) left join `hcoffee_order` on((`hcoffee_order`.`id` = `system_payment`.`order_id`)));