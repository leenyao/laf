
DROP VIEW if exists `system_matadata_1001`;
CREATE  VIEW `system_matadata_1001` AS select `system_metadata`.`id` AS `id`,`system_metadata`.`md5id` AS `md5id`,`system_metadata`.`status` AS `status`,`system_metadata`.`type` AS `type`,`system_metadata`.`p_type` AS `p_type`,`system_metadata`.`p_resid` AS `p_resid`,`system_metadata`.`resid` AS `resid`,`system_metadata`.`label` AS `label`,`system_metadata`.`modified` AS `modified` from `system_metadata` where (`system_metadata`.`type` = 1001);

DROP VIEW if exists `system_matadata_1002`;
CREATE VIEW `system_matadata_1002` AS select `system_metadata`.`id` AS `id`,`system_metadata`.`md5id` AS `md5id`,`system_metadata`.`status` AS `status`,`system_metadata`.`type` AS `type`,`system_metadata`.`p_type` AS `p_type`,`system_metadata`.`p_resid` AS `p_resid`,`system_metadata`.`resid` AS `resid`,`system_metadata`.`label` AS `label`,`system_metadata`.`modified` AS `modified` from `system_metadata` where (`system_metadata`.`type` = 1002);


DROP VIEW IF EXISTS `system_matadata_1005` ;
CREATE VIEW `system_matadata_1005` AS select `system_metadata`.`id` AS `id`,`system_metadata`.`md5id` AS `md5id`,`system_metadata`.`status` AS `status`,`system_metadata`.`type` AS `type`,`system_metadata`.`p_type` AS `p_type`,`system_metadata`.`p_resid` AS `p_resid`,`system_metadata`.`resid` AS `resid`,`system_metadata`.`label` AS `label`,`system_metadata`.`modified` AS `modified` from `system_metadata` where (`system_metadata`.`type` = 1005);

DROP VIEW IF EXISTS `system_matadata_1008` ;
CREATE VIEW `system_matadata_1008` AS select `system_metadata`.`id` AS `id`,`system_metadata`.`md5id` AS `md5id`,`system_metadata`.`status` AS `status`,`system_metadata`.`type` AS `type`,`system_metadata`.`p_type` AS `p_type`,`system_metadata`.`p_resid` AS `p_resid`,`system_metadata`.`resid` AS `resid`,`system_metadata`.`label` AS `label`,`system_metadata`.`modified` AS `modified` from `system_metadata` where (`system_metadata`.`type` = 1008);



DROP VIEW IF EXISTS `system_matadata_1010` ;
CREATE VIEW `system_matadata_1010` AS select `system_metadata`.`id` AS `id`,`system_metadata`.`md5id` AS `md5id`,`system_metadata`.`status` AS `status`,`system_metadata`.`type` AS `type`,`system_metadata`.`p_type` AS `p_type`,`system_metadata`.`p_resid` AS `p_resid`,`system_metadata`.`resid` AS `resid`,`system_metadata`.`label` AS `label`,`system_metadata`.`modified` AS `modified` from `system_metadata` where (`system_metadata`.`type` = 1010);

DROP VIEW if exists `view_order_item_p1`;
CREATE VIEW `view_order_item_p1` AS select `hcoffee_items`.`icon` AS `icon`,`hcoffee_items`.`label` AS `label`,`hcoffee_items`.`category_id` AS `category_id`,`hcoffee_order_items`.`id` AS `id`,`hcoffee_order_items`.`order_id` AS `order_id`,`hcoffee_order_items`.`item_id` AS `item_id`,`hcoffee_order_items`.`count` AS `count`,`hcoffee_order_items`.`price` AS `price`,`hcoffee_order_items`.`memo` AS `memo`,`hcoffee_order_items`.`amount` AS `amount`,`hcoffee_order_items`.`modified` AS `modified`,`hcoffee_order_items`.`m1006_id` AS `m1006_id` from (`hcoffee_order_items` join `hcoffee_items` on((`hcoffee_items`.`id` = `hcoffee_order_items`.`item_id`)));

DROP VIEW if exists `view_order_item`;
CREATE  VIEW `view_order_item` AS select `view_order_item_p1`.*,hcoffee_category.label AS category_label,hcoffee_category.icon AS category_icon from (`view_order_item_p1` join hcoffee_category on((`view_order_item_p1`.category_id = hcoffee_category.`id`)));



DROP VIEW if exists `view_order_item_option`;
CREATE VIEW `view_order_item_option` AS select `system_matadata_1002`.`label` AS `m1002_label`,`system_matadata_1001`.`label` AS `m1001_label`,`system_matadata_1010`.`label` AS `m1010_label`,`hcoffee_order_items_options`.`id` AS `id`,`hcoffee_order_items_options`.`father_id` AS `father_id`,`hcoffee_order_items_options`.`m1001_id` AS `m1001_id`,`hcoffee_order_items_options`.`m1002_id` AS `m1002_id`,`hcoffee_order_items_options`.`modified` AS `modified` from (((`hcoffee_order_items_options` join `system_matadata_1001` on((`hcoffee_order_items_options`.`m1001_id` = `system_matadata_1001`.`resid`))) join `system_matadata_1002` on((`hcoffee_order_items_options`.`m1002_id` = `system_matadata_1002`.`resid`))) left join `system_matadata_1010` on((`system_matadata_1010`.`resid` = `hcoffee_order_items_options`.`m1002_id`)));

DROP VIEW if exists `system_matadata_1007`;
CREATE VIEW `system_matadata_1007` AS select `system_metadata`.`id` AS `id`,`system_metadata`.`md5id` AS `md5id`,`system_metadata`.`status` AS `status`,`system_metadata`.`type` AS `type`,`system_metadata`.`p_type` AS `p_type`,`system_metadata`.`p_resid` AS `p_resid`,`system_metadata`.`resid` AS `resid`,`system_metadata`.`label` AS `label`,`system_metadata`.`modified` AS `modified` from `system_metadata` where (`system_metadata`.`type` = 1007);

DROP VIEW if exists `view_item_option`;
CREATE VIEW `view_item_option` AS select `system_matadata_1002`.`id` AS `id`,`system_matadata_1002`.`md5id` AS `md5id`,`system_matadata_1002`.`status` AS `status`,`system_matadata_1002`.`type` AS `type`,`system_matadata_1002`.`p_type` AS `p_type`,`system_matadata_1002`.`p_resid` AS `p_resid`,`system_matadata_1002`.`resid` AS `m1002_id`,`system_matadata_1001`.`label` AS `m1001_label`,`system_matadata_1002`.`label` AS `m1002_label`,`system_matadata_1010`.`label` AS `m1010_label`,`system_matadata_1002`.`modified` AS `modified` from ((`system_matadata_1001` left join `system_matadata_1002` on(((`system_matadata_1001`.`type` = `system_matadata_1002`.`p_type`) and (`system_matadata_1002`.`p_resid` = `system_matadata_1001`.`resid`)))) left join `system_matadata_1010` on((`system_matadata_1002`.`resid` = `system_matadata_1010`.`resid`)));


DROP VIEW if exists `view_item_option_1001`;
CREATE VIEW `view_item_option_1001` AS select `system_matadata_1007`.`id` AS `id`,`system_matadata_1007`.`md5id` AS `md5id`,`system_matadata_1007`.`status` AS `status`,`system_matadata_1007`.`type` AS `type`,`system_matadata_1007`.`p_type` AS `p_type`,`system_matadata_1007`.`p_resid` AS `p_resid`,`system_matadata_1007`.`resid` AS `resid`,`system_matadata_1007`.`label` AS `m1001_id`,`system_matadata_1007`.`modified` AS `modified`,`system_metadata`.`label` AS `m1001_label` from (`system_matadata_1007` join `system_metadata` on(((`system_matadata_1007`.`label` = `system_metadata`.`resid`) and (`system_metadata`.`type` = 1001))));


DROP VIEW if exists `view_hcoffee_order_p1`;
CREATE VIEW `view_hcoffee_order_p1` AS select `system_matadata_1005`.`label` AS `m1005_label`,`hcoffee_order`.* from `hcoffee_order` left join `system_matadata_1005` on `hcoffee_order`.`m1005_id` = `system_matadata_1005`.`resid` ;

DROP VIEW if exists `view_hcoffee_order_p2`;
CREATE VIEW `view_hcoffee_order_p2` AS select `system_matadata_1008`.`label` AS `m1008_label`,`view_hcoffee_order_p1`.* from `view_hcoffee_order_p1` left join `system_matadata_1008` on (`view_hcoffee_order_p1`.`m1008_id` = `system_matadata_1008`.`resid` );


DROP VIEW if exists `view_hcoffee_order`;
CREATE 
VIEW `view_hcoffee_order` AS 
SELECT
view_hcoffee_order_p2.m1008_label,
view_hcoffee_order_p2.m1005_label,
view_hcoffee_order_p2.id,
view_hcoffee_order_p2.`no`,
view_hcoffee_order_p2.user_id,
view_hcoffee_order_p2.contact_id,
view_hcoffee_order_p2.m1005_id,
view_hcoffee_order_p2.m1008_id,
view_hcoffee_order_p2.amount,
view_hcoffee_order_p2.`name`,
view_hcoffee_order_p2.address,
view_hcoffee_order_p2.phone,
view_hcoffee_order_p2.memo,
view_hcoffee_order_p2.created_time,
view_hcoffee_order_p2.modified,
view_payment.m155_label,
view_payment.pay_config,
view_payment.m155_id
FROM
view_hcoffee_order_p2
LEFT JOIN view_payment ON view_payment.order_id = view_hcoffee_order_p2.id ;

