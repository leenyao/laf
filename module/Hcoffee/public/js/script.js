$(function() {
	var template = ['<li data-id="{id}" data-vote-count="{vote}">',
		'<a class="avatar-wrap" href="', '{url}', '"><img src="', '{img1}', '"></a>',
		'<p>', '{text2}', '</p>',
		'<h2><a href="', '{url}', '">', '{text1}', '</a></h2>',
		'<button type="button" data-id="{id}" {disabled} class="btn btn-submit btn-vote">投票</button>',
		'<p class="aside">总票数 <span>', '{vote}', '</span></p>',
		'</li>'
	].join('');

	function getHtml(id, url, img1, text1, text2, text3, disabled, vote) {
		return template.replace(/\{url\}/g, url)
			.replace(/\{id\}/g, id)
			.replace(/\{img1\}/g, img1)
			.replace(/\{text1\}/g, text1)
			.replace(/\{text2\}/g, text2)
			.replace(/\{text3\}/g, text3)
			.replace(/\{disabled\}/g, disabled)
			.replace(/\{vote\}/g, vote);
	}

	function customAlert(msg) {
		alert(msg);
	}

	function loadData($target) {
		if (!$target || $target.length === 0) {
			return;
		}
		if ($target.attr('data-is-loading') == 'true') {
			return;
		}
		if ($target.attr('data-is-last') == 'true') {
			return;
		}
		var offset = $target.attr('data-offset') || 0;
		var voteable = $target.attr('data-can-vote');
		var disabled = voteable == "true" ? "" : "disabled";
		$target.attr('data-is-loading', 'true');
		$.getJSON(url, {
			offset: offset
		}, function(res) {
			var html = '';
			$target.attr('data-is-loading', 'false');
			$.each(res.data, function(i, v) {
				html += getHtml(v.id, 'content/' + v.id, $.getBasePath() + v.img1, v.text1, v.text2, v.text3, disabled, v.display_vote);
			});
			$($target.attr('href')).find('ul').append(html);
			$target.attr('data-offset', Number(offset) + res.data.length);
			if (res.data.length < 10) {
				$target.attr('data-is-last', 'true');
			}
		});
	}
	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
		var $this = $(this),
			offset = $(this).attr('data-offset') || 0,
			isLoading = $(this).attr('data-is-loading');
		url = $(this).attr('data-link');
		if (offset === 0) {
			loadData($this);
		}
	});
	$('.nav-tabs > .active > a').trigger('show.bs.tab');
	$(document).on('click', '.btn-vote', function() {
		var $li = $(this).closest('li'),
			id = $li.attr('data-id') || $(this).attr('data-id');
		$.post($.getVotePath() + '/' + id, function(res) {
			switch (Number(res.status)) {
				case 1:
					if ($li && $li.length > 0) {
						var $voteCount = $li.find('p.aside span');
						$voteCount.text(Number($li.attr('data-vote-count')) + 1);
					}
					customAlert('投票成功');
					break;
				case -1:
					location.href = res.data;
					break;
				default:
					customAlert(res.msg);
			}
		}, 'json');
	});
	$(document).on('scroll', function(e) {
		var $target;
		if ($(this).height() - $(this).scrollTop() < $('body').height() + 50) {
			loadData($('.nav-tabs > .active > a'));
		}
	});
	$(".btn-share").on('click', function() {
		$(".share-overlay").fadeIn();
	});
	$(".share-overlay").on('click', function() {
		$(this).hide();
	});
});