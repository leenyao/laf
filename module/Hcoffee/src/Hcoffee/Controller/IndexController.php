<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Hcoffee\Controller;



use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction(){
       return new ViewModel();
    }

    public function getattributeslistAction(){
//        var_dump(__CLASS__); 
        header("Content-Type: application/json; charset=utf-8;");
        $aResult = \YcheukfCommon\Lib\Functions::getMetaDataEntityByPtype($this->getServiceLocator(), 1001, 1002);
        echo json_encode($aResult);
        return $this->response;

    }
}