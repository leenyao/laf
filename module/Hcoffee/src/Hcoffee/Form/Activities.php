<?php
namespace Hcoffee\Form;
class Activities extends \Application\Form\Common{

    public function __construct($oController)
    {
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
			"category_id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden', 'value'=>1),
				'options' => array('tips'=>""),
			),
			"postname" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"thumbnails" => array( 
				'type' => 'file',
				'params' => array('id'=>'thumbnails'),
				'options' => array('helpname'=>'fileupload','tips'=>''),
			),
			"description" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'textarea'),
				'options' => array('tips'=>""),
			),
			"content" => array( 
                'type' => 'textarea',
                'attributes' => array( 'class'=>'summernote','required' => 'required'),
                'options' => array('helpname'=>'wysiwyg'),
			),
			"Submitcancel" => array( 
				'type' => 'button',
				'options' => array(
					
					'helpname'=>'Submitcancel',
				),
			),

		);
		$this->_fmtFormElements();
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
}