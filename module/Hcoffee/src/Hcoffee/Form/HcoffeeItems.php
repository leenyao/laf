<?php
namespace Hcoffee\Form;
class HcoffeeItems extends \Application\Form\Common{
    public function __construct($oController)
    {
		$sm = $oController->getServiceLocator();
		$this->setServiceManager($sm);
        $reloadUrl = $oController->url()->fromRoute('hcoffee/default', array('controller'=>'index', 'action'=>'getattributeslist')); 
        $attrsUrl = $oController->url()->fromRoute("zfcadmin/contentlist", array('type' => 'view_item_option', 'cid'=>0)); 
        $aAttributes = \YcheukfCommon\Lib\Functions::getMetaDataEntityByPtype($sm, 1001, 1002);
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
			"label" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"price" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"category_id" => array( 
				'type' => 'select',
				'attributes' => array('class'=>'select-tab'),
				'options' => array(
                    'value_options' => \Application\Model\Common::getResourceList($sm,array('source'=>'hcoffee_category','pkfield'=>'id','labelfield'=>'label')),
                ),
            ),
			"icon" => array( 
				'type' => 'file',
				'params' => array('id'=>'file'),
				'options' => array('helpname'=>'fileupload','tips'=>''),
			),
			"metadata_1007" => array( 
				'type' => 'text',
                'attributes' => array("data-values" => $aAttributes, 'reloadUrl'=>$reloadUrl, 'attrsUrl'=>$attrsUrl),
                'options' => array('helpname'=>'addablelist'),
			),
			"Submitcancel" => array( 
				'type' => 'button',
				'options' => array(
					'helpname'=>'Submitcancel',
				),
			),

		);
		$this->_fmtFormElements();
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
	public function _fmtElement4Data($aData=array(), $aFormElement){
		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
        if(isset($aData['id'])){
            $aM1007 = \Application\Model\Common::getResourceMetadaList($this->getServiceManager(), 1007, $aData['id'], 1, true);
            if(count($aM1007)){
                $aFormElement['metadata_1007']['attributes']['data-selected'] = json_encode($aM1007[$aData['id']]);
            }
        }
		return $aFormElement;
	}
}