<?php
namespace Hcoffee\Form;
class HcoffeeOrderOplog extends \Application\Form\Common{

    public function __construct($oController)
    {
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
			"Submitcancel" => array( 
				'type' => 'button',
				'options' => array(
					
					'helpname'=>'Submitcancel',
				),
			),

		);
		$this->_fmtFormElements();
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
}