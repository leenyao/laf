<?php
namespace Hcoffee\Form;
class SystemMatadata1001 extends \Application\Form\Common{

    public function __construct($oController)
    {
        $nId = $oController->getEvent()->getRouteMatch()->getParam('id', null);
        $nType = ($oController->_getMetadataType($oController->getEvent()->getRouteMatch()->getParam('type', null)));

		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
			"label" => array( 
				'type' => 'text',
//				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
			"Submitcancel" => array( 
				'type' => 'button',
				'options' => array(
					
					'helpname'=>'Submitcancel',
				),
			),

		);
        if(is_null($nId)){
            $this->aFormElement['type'] = array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden', 'value'=>$nType),
				'options' => array('tips'=>""),
			);
            $this->aFormElement['resid'] = array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden', 'value'=>\YcheukfCommon\Lib\Functions::getMetadataMaxResId($oController->getServiceLocator(), $nType)),
				'options' => array('tips'=>""),
			);
        }
        $this->_fmtFormElements();
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
}