<?php
namespace Hcoffee\Form;
class ViewItemOption extends \Application\Form\Common{

    public function __construct($oController)
    {
		$sm = $oController->getServiceLocator();
        $nId = $oController->getEvent()->getRouteMatch()->getParam('id', null);
        $aList =\Application\Model\Common::getResourceMetadaList($sm, 1001);
//        var_dump($aList);
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
			"p_resid" => array( //用户组
				'type' => 'select',
				'attributes' => array('class'=>'select-tab'),
				'options' => array('value_options' => $aList,
				),
			),
			"label" => array( 
				'type' => 'text',
				'options' => array('tips'=>""),
			),
			"metadata_r3_1010_1002" => array( 
				'type' => 'text',
				'options' => array('tips'=>""),
			),
			"Submitcancel" => array( 
				'type' => 'button',
				'options' => array(
					
					'helpname'=>'Submitcancel',
				),
			),

		);
        if(is_null($nId)){

            $this->aFormElement['resid'] = array(
				'type' => 'hidden',
				'attributes' => array('value'=>(\YcheukfCommon\Lib\Functions::getMetadataMaxResId($oController->getServiceLocator(), 1002)+1)),
			);
            $this->aFormElement['type'] = array( 
				'type' => 'hidden',
				'attributes' => array('value'=>1002),
			);
            $this->aFormElement['p_type'] = array( 
				'type' => 'hidden',
				'attributes' => array('value'=>1001),
			);
        }
		$this->_fmtFormElements();
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
}