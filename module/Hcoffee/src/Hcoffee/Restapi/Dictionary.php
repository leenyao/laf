<?php
namespace Hcoffee\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Dictionary extends \YcheukfCommon\Lib\Restapi{
    function get($sResource=null, $sId="") {
        $this->_init($sResource, false);//第二个参数 1=>验证token

        $aDocsConf = array(
            $sResource => array(//apidocs键值
                'child' => array(//子元素
                ),
            ),
        );
        foreach($this->aConfig['apiservice']['customer'][$sResource]['d:entity'] as $k =>$aTmp){
            $aDocsConf[$sResource]['child'][$k] = array(
                'resource' => $aTmp['d:resource'],
                'select' => array('where'=>array('status'=>1)),
            );
        }
        return $this->returnGet($aDocsConf, $sId);
    }

}