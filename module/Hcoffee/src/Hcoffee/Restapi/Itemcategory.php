<?php
namespace Hcoffee\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Itemcategory extends \YcheukfCommon\Lib\Restapi{
    function get($sResource=null, $sId="") {
        $this->_init($sResource, false);
        $aDocsConf = array(
            'itemcategory' => array(
                'resource' => 'hcoffee_category',
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    'where' => array('m1003_id'=>1)
                ),
                'child' => array(
                    'items' => array(
                        'resource' => 'hcoffee_items',
                        'select' => array('where' =>array('m1004_id'=>1, 'category_id'=>'[=field=]id')),
                        'child' => array(
                            'attrs' => array(
                                'resource' => 'view_item_option_1001',
                                'select' => array('where' =>array('status'=>1, 'resid'=>'[=field=]id')),
                                'child' => array(
                                    'options' => array(
                                        'resource' => 'view_item_option',
                                        'select' => array('where' =>array('status'=>1, 'p_resid'=>'[=field=]id')),

                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf['itemcategory']['select']['where']['id'] = $sId;
        }
//        var_dump($_GET);
//        exit;
        return $this->returnGet($aDocsConf, $sId, array('cachekey'=>'hcoffee_itemcategory-list_'.$sId.json_encode($_GET), 'expiredtime' =>86400));
    }

//    function post($sResource=null, $sId="") {
//        $this->doPost($sResource, $sId);
//    }
//    function put($sResource=null, $sId="") {
//        $this->doPut($sResource, $sId);
//    }

}