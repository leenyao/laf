<?php
namespace Hcoffee\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Mycontact extends \YcheukfCommon\Lib\Restapi{
    function delete($sResource=null, $sId="") {
        $this->_init($sResource, true);
        $aReturn = $this->doDelete(
            'contact', //要删除的资源
            array("status"=>2),  //要更新的资源字段
            $this->sUserId,  //用户id
            $sId //资源id
        );
    }
    /*
    * post/put 调用本函数
    */
    function _postConfig($sResource=null, $sId="", $aPostData) {
        $aPostData = $this->_fmtPostData($this->aConfig['apiservice']['customer'], $aPostData, 'my_contact');
//var_dump($aPostData);
        $aDocsConf = array(
            'my_contact' => array(
                'resource' => 'contact',
                'select' => array(
                    'dataset' => array(
                        'user_id' => $this->sUserId,
                    ),
                    'where' => array()
                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf['my_contact']['select']['where']['id'] = $sId;
        }
        return $this->doSave($aPostData, $aDocsConf);
    }
    function get($sResource=null, $sId="") {
        $this->_init($sResource, true);//第二个参数 1=>验证token
        $aDocsConf = array(
            'my_contact' => array(//apidocs键值
                'resource' => 'contact',//apidocs键值对应资源名
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    'where' => array()
                ),
            ),
        );
        if($this->bCheckToken){
            $aDocsConf['my_contact']['select']['where']['user_id'] = $this->sUserId;
        }
        if(!empty($sId)){
            $aDocsConf['my_contact']['select']['where']['id'] = $sId;
        }
        return $this->returnGet($aDocsConf, $sId);
    }
    function post($sResource=null, $sId="") {
        $this->doPost($sResource, $sId);
    }
    function put($sResource=null, $sId="") {
        $this->doPut($sResource, $sId);
    }   

}