<?php
namespace Hcoffee\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Order extends \YcheukfCommon\Lib\Restapi{
    function delete($sResource=null, $sId="") {
        $this->_init($sResource, true);
        $aReturn = $this->doDelete(
            'hcoffee_order', 
            array("m1005_id"=>2), 
            $this->sUserId, 
            $sId
        );
    }
    function _fmtPostDataMyWay($aPostData) {
        $fOrderAmount = 0;
        foreach($aPostData['items'] as $k1 => $a1){
            $aItemData = \Application\Model\Common::getResourceById($this->sm, 'hcoffee_items', $a1['item_id']);
            $nPrice = floatval($aItemData['price']);
            $aPostData['items'][$k1]['price'] = $nPrice;
            if(isset($a1['attrs']) && count($a1['attrs'])){
                foreach($a1['attrs'] as $k2 => $a2){
                    $sM1010Label = \Application\Model\Common::getResourceMetaDataLabel($this->sm, 1010, $a2['m1002_id']);
                    $nPrice += floatval($sM1010Label);
                    $aPostData['items'][$k1]['attrs'][$k2]['m1010_label'] = floatval($sM1010Label);
                }
            }
            $aPostData['items'][$k1]['amount'] = floatval($nPrice) * intval($a1['count']);
            $fOrderAmount += $aPostData['items'][$k1]['amount'];
        }
        //校对订单金额
        $aPostData['amount'] = $fOrderAmount;

//
//        对不同的支付方式进行不同的初始化处理
//        var_dump($aPostData);
        switch(intval($aPostData['m1008_id'])){
            case 1:
                $aPostData['m1005_id'] = 2;//未处理状态
                break;
            case 2:
            default: //在线支付
                $aPostData['m1005_id'] = 1;//未付款状态
                break;
        }
        
        return $aPostData;
    }    
    function _postConfig($sResource=null, $sId="", $aPostData) {
        $aPostData = $this->_fmtPostData($this->aConfig['apiservice']['customer'], $aPostData, 'order');
        $aPostData = $this->_fmtPostDataMyWay($aPostData);
        $aConcatData = \Application\Model\Common::getResourceById($this->sm, 'contact', $aPostData['contact_id']);

            // foreach begin
        $funTmp = function($aIn){
            if(count($aIn)<1) {
                return $this->responce(500, "you did not select any items");
            }else{                
                if(is_array($aIn) && count($aIn)){
                    foreach($aIn as $k1 => $v1){
                        $aData = \Application\Model\Common::getResourceById($this->sm, 'hcoffee_items', $v1['item_id']);
                        if($aData['m1004_id'] != 1)
                            return $this->responce(500, "sorry, ".$aData['label']." is not for sale");
                    }
                }
            }
            return true;
        };
        // foreach end
        $bCheck = $funTmp($aPostData['items']);
        if($bCheck !== true)return $bCheck;
        

        $aDocsConf = array(
            'order' => array(
                'resource' => 'hcoffee_order',
                'select' => array(
                    'dataset' => array(
                        'user_id' => $this->sUserId,
                        'name' => $aConcatData['name'],
                        'address' => $aConcatData['address'],
                        'phone' => $aConcatData['phone'],
                        'no' => md5(uniqid().time()),
                    ),
                    'where' => array()
                ),
                'child' => array(
                    'items' => array(
                        'resource' => 'hcoffee_order_items',
                        'select' => array(
                            'dataset' => array(
                                'order_id' => '[=fatherid=]',
                            ),
                        ),
                        'delete_dataset' => array('m1006_id' => 2,),
                        'child' => array(
                            'attrs' => array(
                                'resource' => 'hcoffee_order_items_options',
                                 'delete_dataset' => array('status' => 2,),
                                'select' => array(
                                    'dataset' => array(
                                        'father_id' => '[=fatherid=]',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf['order']['select']['where']['id'] = $sId;
        }else{
            $aDocsConf['order']['select']['dataset']['created_time'] = date('Y-m-d H:i:s');
        }
        return $this->doSave($aPostData, $aDocsConf);
    }
    function get($sResource=null, $sId="") {
        $this->_init($sResource, 1);
        $aDocsConf = array(
            'order' => array(
                'resource' => 'view_hcoffee_order',
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    'where' => array()
                ),
                'child' => array(
                    'items' => array(
                        'resource' => 'view_order_item',
                        'select' => array('where' =>array('m1006_id'=>1, 'order_id'=>'[=field=]id')),
                        'child' => array(
                            'attrs' => array(
                                'resource' => 'view_order_item_option',
                                'select' => array('where' =>array('father_id'=>'[=field=]id')),
                            ),
                        ),
                    ),
                ),
            ),
        );
        if($this->bCheckToken){
            $aDocsConf['order']['select']['where']['user_id'] = $this->sUserId;
        }
        if(!empty($sId)){
            $aDocsConf['order']['select']['where']['id'] = $sId;
        }


//
//          处理query 请求
//
        if(isset($_GET['order_status'])){
            $aDocsConf['order']['select']['where']['m1005_id'] = $_GET['order_status'];
        }

        return $this->returnGet($aDocsConf, $sId);
    }
    function post($sResource=null, $sId="") {
        $this->doPost($sResource, $sId);
    }
    function put($sResource=null, $sId="") {
        $this->doPut($sResource, $sId);
    }

}