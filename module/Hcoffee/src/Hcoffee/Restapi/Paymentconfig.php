<?php
namespace Hcoffee\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Paymentconfig extends \YcheukfCommon\Lib\Restapi{
    function get($sResource=null, $sId="") {
        $this->_init($sResource, 1);//第二个参数 1=>验证token
        $this->_wechatpay($sId);
        $aDocsConf = array(
            'paymentconfig' => array(//apidocs键值
                'resource' => 'view_payment',//apidocs键值对应资源名
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    'where' =>array()
                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf['paymentconfig']['select']['where']['order_id'] = $sId;
        }
        return $this->returnGet($aDocsConf, $sId);
    }

    private function _wechatpay($sId){
        $aPaymentData = \YcheukfCommon\Lib\Functions::getResourceById($this->sm, 'system_payment', $sId, 'order_id');
//        var_dump($sId);
//        var_dump($aPaymentData);
        if(count($aPaymentData) && !is_null($aPaymentData['pay_config']))return true;

        $aConfig = $this->sm->get('config');
        $o = new \Hcoffee\Restapi\Order($this->sm);
        $aOrderData = (\YcheukfCommon\Lib\Functions::fmtRestApiOutput($o->get('order', $sId)));
//var_dump($aOrderData['id']);
        try{
                    /**
             * 第 1 步：定义商户
             */
            $business = new \Overtrue\Wechat\Payment\Business(
                $aConfig['weixin']['appid'],
                $aConfig['weixin']['secret'],
                $aConfig['weixin']['mch_id'],
                $aConfig['weixin']['mch_key']
            );
    //        user_id
            $aUserData = \YcheukfCommon\Lib\Functions::getResourceById($this->sm, 'user', $aOrderData['user_id']);
//var_dump($aUserData);
            /**
             * 第 2 步：定义订单
             */
            $order = new \Overtrue\Wechat\Payment\Order();
            $order->body = "Hcoffee订单-".$aOrderData['no'];
            $order->out_trade_no = $aOrderData['no'];
            $order->total_fee = $aOrderData['amount']*100;    // 单位为 “分”, 字符串类型
            $order->openid = str_replace("wx_", "", $aUserData['username']);
            $order->notify_url = $aConfig['weixin']['paymentnotify_url'];

            /**
             * 第 3 步：统一下单
             */
            $unifiedOrder = new \Overtrue\Wechat\Payment\UnifiedOrder($business, $order);

            /**
             * 第 4 步：生成支付配置文件
             */
            $payment = new \Overtrue\Wechat\Payment($unifiedOrder);

            $aPaymentConfig = $payment->getConfig();
        }catch(\Overtrue\Wechat\Payment\Exception $e){
//            var_dump($e->getMessage());
            return $this->responce(500, $e->getMessage());
        }

        $aParams = array(
            'op'=> 'save',
            'resource'=> 'payment',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sm),
            'params' => array(
                'dataset' => array (
                    array(
                        'pay_config'=> $aPaymentConfig,
                    ),
                ),
                'where' => array('order_id'=>$aOrderData['id']),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::myUpdate($this->sm, $aParams);
    }
}