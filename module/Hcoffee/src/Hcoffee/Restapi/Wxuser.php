<?php
namespace Hcoffee\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Wxuser extends \YcheukfCommon\Lib\Restapi{
    function delete($sResource=null, $sId="") {
        $aReturn = $this->doDelete(
            'hcoffee_order', //要删除的资源
            array("m1005_id"=>2),  //要更新的资源字段
            $this->sUserId,  //用户id
            $sId //资源id
        );
    }
    /*
    * post/put 调用本函数
    */
    function _postConfig($sResource=null, $sId="", $aPostData) {
        $aPostData = $this->_fmtPostData($this->aConfig['apiservice']['customer'], $aPostData, 'order');//格式化post参数, 第三个参数为apidocs key
        $aParams = array(
            'op'=> 'save',
            'resource'=> $sResource,
            'resource_type'=> 'common',
            'access_token' => $this->dbAccessToken,
            'params' => array(
                'dataset' => array (
                    $aPostData,
                ),
            ),
        );
        if(!empty($sId)){
            $aParams['params']['where'] = array('id'=>$sId);
        }
        $aReturn = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
        if($aReturn['status'] == 1){
            return $this->responce(200, $aReturn['data']);
        }else{
            return $this->responce(500, json_encode($aReturn));
        }
    }
    function get($sResource=null, $sId="") {
        $this->_init($sResource, 1);//第二个参数 1=>验证token
        $aDocsConf = array(
            'itemcategory' => array(//apidocs键值
                'resource' => 'hcoffee_category',//apidocs键值对应资源名
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    'where' => (!empty($sToken) ? array('id'=>$sToken) : array())
                ),
//                'child' => array(//子元素
//                    'items' => array(//子元素的apidocs键值
//                        'resource' => 'hcoffee_items',//子元素的对应资源名
//                        'select' => array('where' =>array('m1004_id'=>1, 'category_id'=>'[=field=]id')),
//                        'child' => array(
//                            'attrs' => array(
//                                'resource' => 'view_item_option_1001',
//                                'select' => array('where' =>array('status'=>1, 'resid'=>'[=field=]id')),
//                            ),
//                        ),
//                    ),
//                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf['itemcategory']['select']['where']['id'] = $sId;
        }
        return $this->returnGet($aDocsConf, $sId);
    }
    function post($sResource=null, $sId="") {
        $this->doPost($sResource, $sId);
    }
    function put($sResource=null, $sId="") {
        $this->doPut($sResource, $sId);
    }

}