<?php

namespace Hcoffee\Service;



class Record extends \Application\Service\Record{

    public function insert_hcoffee_category($sResource, $aData, $sId){
        $aCacheKeyTmp = array('hcoffee', 'itemcategory-list');
        \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, $aCacheKeyTmp, true);
    }
    public function update_hcoffee_category($sResource, $aData, $sId){
        $aCacheKeyTmp = array('hcoffee', 'itemcategory-list');
        \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, $aCacheKeyTmp, true);
    }
    public function insert_hcoffee_item($sResource, $aData, $sId){
        $aCacheKeyTmp = array('hcoffee', 'itemcategory-list');
        \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, $aCacheKeyTmp, true);
    }
    public function update_hcoffee_items($sResource, $aData, $sId){
        $aCacheKeyTmp = array('hcoffee', 'itemcategory-list');
        \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, $aCacheKeyTmp, true);
    }
    public function insert_view_item_option($sResource, $aData, $sId){
        $aCacheKeyTmp = array('hcoffee', 'itemcategory-list');
        \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, $aCacheKeyTmp, true);
    }
    public function update_metadata($sResource, $aData, $sId){
        $aCacheKeyTmp = array('hcoffee', 'itemcategory-list');
        \YcheukfCommon\Lib\Functions::delCache($this->serviceManager, $aCacheKeyTmp, true);
    }


    public function insert_order($sResource, $aData, $sId){
        $aM1009 = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->serviceManager, 1009);
        $aConfig = $this->serviceManager->get('config');

        $o = new \Hcoffee\Restapi\Order($this->serviceManager);
        $aOrderData = (\YcheukfCommon\Lib\Functions::fmtRestApiOutput($o->get('order', $sId)));
        switch(intval($aOrderData['payment_id'])){
            case 1:
            default://货到付款
                break;
            case 2://微信支付
//                $this->_wechatpay($aOrderData, $aConfig);
                $aParams = array(
                    'op'=> 'save',
                    'resource'=> 'payment',
                    'resource_type'=> 'common',
                    'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
                    'params' => array(
                        'dataset' => array (
                            array(
                                'order_id'=> $aOrderData['id'],
                                'order_no'=> $aOrderData['no'],
                                'm155_id'=> 1,
                                'm156_id'=> 1,
                            ),
                        )
                    ),
                );
                $aResult = \YcheukfCommon\Lib\Functions::myUpdate($this->serviceManager, $aParams);
                break;
        }

         \YcheukfCommon\Lib\Functions::sendEmailByMQ($this->serviceManager, array('to'=>$aM1009),$aConfig['opconfig']['order_notification']['mail_title'], $this->_fmtEmailBody($aOrderData, $sId, $aConfig['opconfig']['order_notification']['mail_body']));
    }
    private function _wechatpay($aData, $aConfig){

        try{
                    /**
             * 第 1 步：定义商户
             */
            $business = new \Overtrue\Wechat\Payment\Business(
                $aConfig['weixin']['appid'],
                $aConfig['weixin']['secret'],
                $aConfig['weixin']['mch_id'],
                $aConfig['weixin']['mch_key']
            );
    //        user_id
            $aUserData = \YcheukfCommon\Lib\Functions::getResourceById($this->serviceManager, 'user', $aData['user_id']);

            /**
             * 第 2 步：定义订单
             */
            $order = new \Overtrue\Wechat\Payment\Order();
            $order->body = $aData['id'];
            $order->out_trade_no = $aData['id'];
            $order->total_fee = $aData['amount']*100;    // 单位为 “分”, 字符串类型
            $order->openid = str_replace("wx_", "", $aUserData['username']);
            $order->notify_url = $aConfig['weixin']['paymentnotify_url'];

            /**
             * 第 3 步：统一下单
             */
            $unifiedOrder = new \Overtrue\Wechat\Payment\UnifiedOrder($business, $order);

            /**
             * 第 4 步：生成支付配置文件
             */
            $payment = new \Overtrue\Wechat\Payment($unifiedOrder);
        }catch(\Overtrue\Wechat\Payment\Exception $e){
        
        }
//                var_dump($payment->getConfig());


        //delete例子
        $aParams = array(
            'op'=> 'delete',
            'resource'=> 'payment',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
            'params' => array(
                'where' => array('order_id'=>$aData['id']),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::myDelete($this->serviceManager, $aParams);
        $aParams = array(
            'op'=> 'save',
            'resource'=> 'payment',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
            'params' => array(
                'dataset' => array (
                    array(
                        'order_id'=> $aData['id'],
                        'paymentorder_id'=> $aData['id'],
                        'm155_id'=> 1,
                        'm156_id'=> 1,
                        'pay_config'=> ($payment->getConfig()),
                    ),
                )
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::myUpdate($this->serviceManager, $aParams);
    }
    private function _fmtEmailBody($aData, $sId, $sMailBody){
        $sItems = "<br/>\n";
        foreach($aData['items'] as $row){
            $sItems .= "<br/>\n ".$row['label']."(id:".$row['id'].")";
            if(count($row['attrs'])){
                $sItems .= ", ";
                foreach($row['attrs'] as $row2){
                    $sItems .= "/".$row2['selected_label'];
                }
            }
        }
        $sItems .= "<br/>\n";
        $aConcat = \YcheukfCommon\Lib\Functions::getResourceById($this->serviceManager, 'contact', $aData['contact_id']);
        $sMailBody = str_replace("[1]", $aData['no'], $sMailBody);
        $sMailBody = str_replace("[2]", $aConcat['name']."(".$aConcat['phone'].")", $sMailBody);
        $sMailBody = str_replace("[3]", $aData['created_time'], $sMailBody);
        $sMailBody = str_replace("[4]", $sItems, $sMailBody);
        $sMailBody = str_replace("[5]", $aConcat['address'], $sMailBody);
        $sMailBody = str_replace("[6]", $aData['payment_label'], $sMailBody);
        $sMailBody = str_replace("[7]", "客户留言:".$aData['memo'], $sMailBody);
        return $sMailBody;
    }
}
