<?php
namespace Hcoffee\Toolbar;
class HcoffeeCategory extends \Application\Toolbar\Toolbar{

    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
        $aReturn = array();

        //新增按钮
        $aReturn[] = $this->_getAddButton($oController, $sSource, $nId);


        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('状态操作:'),
            ),
        );
        $nTypeTmp = 1003;
        $aStatus = \Application\Model\Common::getResourceMetadaList($sm, $nTypeTmp);
//        var_dump($aStatus);
        foreach($aStatus as $nStatus=>$nLabel){
            $aReturn[] = $this->_getPublishButton($sm, $oController, $nTypeTmp, $sSource, $nStatus, null, 'm'.$nTypeTmp.'_id');
        }
        $aReturn[] = array(
            'type' => 'pipe',
        );
        //下载
        $aReturn[] = array(
            'type' => 'detail-button-download',
            'cls' => 'download',
            'iconCls' => 'licon-download',
            'position' => 'left',
            'params' => array(
                'request' => array('returnType'=>'csv_all'),
                'label' => $oController->translate('download'),
            ),
        );

        $aReturn[] = array(
            'type' => 'detail-br',
        );



        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('状态:'),
            ),
        );
        $aStatus =  array(0 =>$oController->translate("all")) + $aStatus;
        unset($aStatus[3]);
        $aReturn[] = array(
            'type' => 'detail-select-action',
            'cls' => 'filter',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('过滤状态'),
                'object_name' => 'm'.$nTypeTmp.'_id',
                'options' => $aStatus,
            ),
        );


        $aReturn[] = array(
            'type' => 'detail-searchbar',
            'position' => 'right',
            'params' => array(
                'label' => $oController->translate('搜索'),
                'tips' => 'fill chart plz',
                'object_name' => 'q_keyword',
            ),
        );

        $aReturn = $this->_fmtPermission($sm, $aReturn);
        return $aReturn;

    }
}