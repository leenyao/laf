<?php
namespace Hcoffee\Toolbar;

class User extends \Application\Toolbar\Toolbar{
    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
		$aReturn = array();

		//新增按钮
//		$aReturn[] = $this->_getAddButton($oController, $sSource, $nId);



		//下载
		$aReturn[] = array(
			'type' => 'detail-button-download',
			'cls' => 'download',
			'iconCls' => 'licon-download',
			'position' => 'left',
			'params' => array(
				'request' => array('returnType'=>'csv_all'),
				'label' => $oController->translate('download'),
			),
		);

		$aReturn[] = array(
			'type' => 'detail-br',
		);



		$aReturn[] = array(
			'type' => 'text',
			'position' => 'left',
			'params' => array(
				'label' => $oController->translate('状态:'),
			),
		);
		$aStatus = \Application\Model\Common::getResourceMetadaList($sm, 102);
		$aStatus =  array(0 =>$oController->translate("all")) + $aStatus;
		$sObjectName = \Application\Model\Common::getFieldBySource('status', $sSource);
		$aReturn[] = array(
			'type' => 'detail-select-action',
			'cls' => 'filter',
			'position' => 'left',
			'params' => array(
				'label' => $oController->translate('过滤状态'),
				'object_name' => $sObjectName,
				'options' => $aStatus,
			),
		);
                
//                $aReturn[] = array(
//			'type' => 'text',
//			'position' => 'left',
//			'params' => array(
//				'label' => $oController->translate('注册类型:'),
//			),
//		);
//		$aStatus = \Application\Model\Common::getResourceMetadaList($sm, 157);
//		$aStatus =  array(0 =>$oController->translate("all")) + $aStatus;
//		$sObjectName = \Application\Model\Common::getFieldBySource('m157_id', $sSource);
//		$aReturn[] = array(
//			'type' => 'detail-select-action',
//			'cls' => 'filter',
//			'position' => 'left',
//			'params' => array(
//				'label' => $oController->translate('过滤注册类型'),
//				'object_name' => $sObjectName,
//				'options' => $aStatus,
//			), 
//		);
//                
//                $aReturn[] = array(
//			'type' => 'text',
//			'position' => 'left',
//			'params' => array(
//				'label' => $oController->translate('注册平台:'),
//			),
//		);
//		$aStatus = \Application\Model\Common::getResourceMetadaList($sm, 154);
//		$aStatus =  array(0 =>$oController->translate("all")) + $aStatus;
//		$sObjectName = \Application\Model\Common::getFieldBySource('m154_id', $sSource);
//		$aReturn[] = array(
//			'type' => 'detail-select-action',
//			'cls' => 'filter',
//			'position' => 'left',
//			'params' => array(
//				'label' => $oController->translate('过滤注册平台'),
//				'object_name' => $sObjectName,
//				'options' => $aStatus,
//			), 
//		);


		$aReturn[] = array(
			'type' => 'detail-searchbar',
			'position' => 'right',
			'params' => array(
				'label' => $oController->translate('搜索'),
				'tips' => 'fill chart plz',
				'object_name' => 'q_keyword',
			),
		);

		$aReturn = $this->_fmtPermission($sm, $aReturn);
		return $aReturn;

	}	
}