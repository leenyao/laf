<?php
namespace Hcoffee\YcheukfReportExt\Inputs;
class HcoffeeItems extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
				array(
					'key' => 'm1004_id', //操作符
					'op' => 'in', //操作符=,in,like
					'value' => "(1, 2)"//过滤的值
				),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								'id',
								'label',
								'icon',
								'price',
								'category_id',
								'm1004_id',
								'modified',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $this->getReportParams(), array('m1004_id', 'category_id'), array('label', 'price'));
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}