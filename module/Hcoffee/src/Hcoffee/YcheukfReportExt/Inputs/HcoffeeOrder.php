<?php
namespace Hcoffee\YcheukfReportExt\Inputs;
class HcoffeeOrder extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
				array(
					'key' => 'm1005_id', //操作符
					'op' => 'in', //操作符=,in,like
					'value' => "(1, 2, 3, 4, 5, 6)"//过滤的值
				),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'is_editable' =>false,
					'table' => array(
						$sSource => array(
							'dimen' => array(
                                array(
    								'key'=>'id',
                                ),
								'no',
								'user_id',
//								'address___split___contact_id',
//								'phone___split___contact_id',
//								'memo',
//								'm155_id',
								'm1005_id',
								'amount',
								'modified',
                                array(
    								'key'=>$sSource.'___subtr___op___id',
                                ),
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
			'custom' => array(
				'subtr' => true,
				'subtr_function' => function($nId){
                    $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sM);
                    $aParams = array(
                        'op' => 'get',
                        'resource' => 'view_order_item',
                        'resource_type' => 'common',
                        'access_token' => $access_token,
                        'params' => array(
                            'select' => array(
                                "where" => array(
                                    'order_id' => $nId,
                                    'm1006_id' => 1
                                ),
                            ),
                        ),
                    );
                    $aResult = \Application\Model\Common::getResourceList2(
                        $this->sM,
                        $aParams
                    );
                    if($aResult['count']){
                        foreach($aResult['dataset'] as $kTmp => $aRowTmp){
                            $aParams = array(
                                'op' => 'get',
                                'resource' => 'view_order_item_option',
                                'resource_type' => 'common',
                                'access_token' => $access_token,
                                'params' => array(
                                    'select' => array(
                                        "where" => array(
                                            'father_id' => $aRowTmp['id']
                                        ),
                                    ),
                                ),
                            );
                            $aResult2 = \Application\Model\Common::getResourceList2(
                                $this->sM,
                                $aParams
                            );
                            $aRowTmp['options'] = $aResult2['dataset'];
                            $aResult['dataset'][$kTmp] = $aRowTmp;
                        }
                    }
                    $aList = ($aResult['dataset']);
                    $sHtml = "";
                    $aTmpORder = \Application\Model\Common::getResourceById($this->sM, 'hcoffee_order', $nId);
                    $s1008Label = \Application\Model\Common::getResourceMetaDataLabel($this->sM, 1008, $aTmpORder['m1008_id']);
                    if(in_array($aTmpORder['m1008_id'], array(2, 3, 4, 5))){
                        $aTmpPayment = \Application\Model\Common::getResourceById($this->sM, 'view_payment', $aTmpORder['id'], 'order_id');
                        $s1008Label = $aTmpPayment["m156_label"]."/".$aTmpPayment["m155_label"];
                    }

                    $aParams = array(
                        'op'=> 'get',
                        'resource'=> 'hcoffee_order_oplog',
                        'resource_type'=> 'common',
                        'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sM),
                        'params' => array(
                            'select' => array (
                                "where" => array('order_id' => $aTmpORder['id'], 'status'=>1),
                                "order" => "id desc",
                            ),
                        ),
                    );
                    $aListData = \YcheukfCommon\Lib\Functions::getResourceList2($this->sM, $aParams);
                    $sOpLog = "";
                    if($aListData['count']){
                        // foreach begin
                        $funTmp = function($aIn){
                            $aReturn = array();
                            $sReturn = "<ul>";
                            if(is_array($aIn) && count($aIn)){
                                foreach($aIn as $k1 => $v1){
                                    $aReturn[$k1] = $v1['log'];
                                    $sReturn .= "<li>".$v1['log']." <font color='#cc99cc'>[".$v1['modified']."]</font>";
                                }
                            }
                            $sReturn .= "</ul>";
                            return $sReturn;
                        };
                        $sOpLog = $funTmp($aListData['dataset']);
                        unset($funTmp);
                        // foreach end

                    }
                    $sHtml = "<ul>
                    <li>付款方式: {$s1008Label}
                    <li>收货人: {$aTmpORder['name']}
                    <li>地址: {$aTmpORder['phone']}
                    <li>电话: {$aTmpORder['address']}
                    <li>留言: {$aTmpORder['memo']}
                    <li>商品列表: ";
                    if(count($aList)){
                        $sHtml .= '<ul>';
                        foreach($aList as $aRow){
                            $sMemo = "";
                            if(isset($aRow['options']) && count($aRow['options'])){
                                foreach($aRow['options'] as $aRow2){
                                    $sMemo .= "/".$aRow2["m1002_label"];
                                }
                               $sMemo = substr($sMemo, 1);
                            }
                            if(!empty($aRow['memo']))$sMemo .= ", [留言:".$aRow["memo"]."]";
                            $sHtml .= <<<OUTPUT
                                <li>
                                    {$aRow['label']}X{$aRow['count']}, 
                                    $sMemo,
                                </li>
OUTPUT;
                        }
                        $sHtml .= '</ul>';
                    }
                    $sUrlTmp = $this->sM->get('httprouter')->assemble(array('type'=>'hcoffee_order_oplog', 'cid'=>$nId), array('name'=>'zfcadmin/ajaxlist'));
                    $sUrlAction = $this->sM->get('httprouter')->assemble(array('type'=>'hcoffee_order_oplog', 'cid'=>$nId), array('name'=>'zfcadmin/contentedit'));
                    $sHtml .= "<li>商家备注:<span class='editable-action-log btn btn-link' data-url='". $sUrlTmp . "' add-action-url='".$sUrlAction."?status=<1></1>' >编辑</span> {$sOpLog}";
                    $sHtml .= '</ul>';
                    return $sHtml;
                },
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $this->getReportParams(), array('m1005_id', ), array());
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}