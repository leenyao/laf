<?php
namespace Hcoffee\YcheukfReportExt\Inputs;
class HcoffeeOrderOplog extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$nCid = $this->getCid();

		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
                array(
                    'key' => 'status', //操作符
                    'op' => 'in', //操作符=,in,like
                    'value' => "(1)"//过滤的值
                ),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
                    'is_editable' => false,
                    'is_viewable' => false,
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								array(
                                    'key' => $sSource.'___id',
                                    'value' => function(){
                                        return 123;
                                    },
                                    
                                ),
								'log',
								array(
                                    'key' => 'order_id',
                                    'group' => false,
                                ),
								array(
                                    'key' => 'status',
                                    'group' => false,
                                ),
								'modified',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
		);
        if($nCid){
            $aInput['filters'][] = array(
                'key' => 'order_id', //操作符
                'op' => '=', //操作符=,in,like
                'value' => $nCid//过滤的值
            );
        }
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $this->getReportParams(), array('status', 'order_id'), array());
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}