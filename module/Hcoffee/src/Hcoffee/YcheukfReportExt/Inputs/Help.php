<?php
namespace Hcoffee\YcheukfReportExt\Inputs;
class Help extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
                array(
                    'key' => 'status', //操作符
                    'op' => 'in', //操作符=,in,like
                    'value' => "(1)"//过滤的值
                ),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								'id',
								array(
                                    'key' => 'status',
                                    'group' => false,
                                ),
								'modified',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $this->getReportParams(), array('status', ), array());
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}