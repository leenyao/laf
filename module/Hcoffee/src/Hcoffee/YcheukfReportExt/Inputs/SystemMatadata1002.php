<?php
namespace Hcoffee\YcheukfReportExt\Inputs;
class SystemMatadata1002 extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								'id',
								'modified',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $this->getReportParams(), array('status', ), array());
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}