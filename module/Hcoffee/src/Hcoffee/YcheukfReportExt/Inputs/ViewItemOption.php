<?php
namespace Hcoffee\YcheukfReportExt\Inputs;
class ViewItemOption extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
                array(
                    'key' => 'status', //操作符
                    'op' => 'in', //操作符=,in,like
                    'value' => "(1)"//过滤的值
                ),
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					'orderby' =>'modified desc,  p_resid desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								'id',
								array(
                                    'key' => 'p_resid',
                                    'group' => false,
                                ),
								array(
                                    'key' => 'status',
                                    'group' => false,
                                ),
								'm1001_label',
								'm1002_label',
								'm1010_label',
								'modified',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
		);
		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $this->getReportParams(), array('p_resid', ), array('m1001_label', 'm1002_label'));
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}