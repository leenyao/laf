<?php
$sJson = '[
        [1200, "服务器错误, 请联系管理员"],
        [1201, "没有该用户"],

        [2000, "传递的参数不是json格式"],
        [2001, "access_token为空"],
        [2002, "access_token 验证失败"],
        [2003, "参数 resource 为空"],
        [2004, "参数 select 验证失败, 不为json"],
        [2005, "参数 select 验证失败, 不正确的键值"],
        [2006, "params 为空"],
        [2007, "没有对应的resource.[=replacement1=]"],
        [2008, "服务端找不到对应的service: [=replacement1=]"],
        [2009, "query 出错.[=replacement1=]"],
        [2010, "缺少dataset参数"],
        [2011, "dataset参数需要为数组"],
        [2012, "dataset不能为空"],
        [2013, "id不能为空"],
        [2014, "oauth 服务器没有正常返回数据"],
        [2015, "未知的数据库类型:[=replacement1=]"],
        [2016, "不存在该方法:[=replacement1=]"],
        [2017, "缺少post参数"],
        [2018, "错误的where条件. 操作符:[=replacement1=]"],
        [2019, "错误的where条件. 比较符:[=replacement1=]"],
        [2020, "错误的where条件. 值应该为数组"],
        [2021, "where 参数为必传, 且为数组"],
        [2022, "无法初始化mongodb. 缺少配置数组. 键值:[=replacement1=]"],
        [2023, "无法读取API资源数组, 请确保配置正确"],
        [2024, "数据库条数不正确"],
        [2025, "错误的column变量.没有此类返回值:[=replacement1=]"],
        [2026, "错误的column变量.没有此类操作符:[=replacement1=]"],
        [2027, "未知的请求参数类型:[=replacement1=]"],
        [2028, "不支持该类型文件的上传:[=replacement1=]"],
        [2029, "缺少上传参数"],
        [2030, "上传失败"],
        [2031, "不支持该查询操作符:[=replacement1=]"],
        [2032, "接受任务的记录不完整:[=replacement1=]"],
        [2033, "事务回滚:[=replacement1=]"],
        [2034, "未知的映射metadata:[=replacement1=]"],
        [2035, "无法取得相应数据"],
        [2036, "没有相应的数据"],
        [2037, "缺少发布周期"],
        [2038, "错误的登录参数:[=replacement1=]"],
        [2039, "登录参数为空"],
        [2040, "sql error:[=replacement1=]"],
        [2041, "oauth client的记录为空"],
        [2042, "oauth 的用户为空"],
        [2043, "API error: result=>[=replacement1=], url=>[=replacement2=], params=>[=replacement3=]"],
        [2044, "参数需要为数组:[=replacement1=]"],
        [2045, "没有该id相应的数据:[=replacement1=]"],
        [2047, "204720472047"],
        [2048, "检查token失败:[=replacement1=]"],
        [2049, "没有session id"],
        [2050, "invalid data, remaining: [=replacement1=]"],
        [2051, "API没有提供该类型操作:[=replacement1=]"],
        [2052, "API返回数据出错:[=replacement1=]"],
        [2053, "该企业用户没有登陆"],
        [2054, "图片尺寸不符. 要求:[=replacement1=], 实际:[=replacement2=], "],
        [2055, "该用户没有登陆"],
        [2056, "无效操作，请检查权限。"],
        [2057, "反复提交失败。"],

        [5001, "修改状态失败"]
    ]';


$aJson = json_decode($sJson, 1);
$aReturn = array();
foreach($aJson as $row){
    $aReturn[$row[0]] = $row[1];
}
return $aReturn;

