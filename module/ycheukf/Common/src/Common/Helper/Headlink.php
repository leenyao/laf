<?php
namespace YcheukfCommon\Helper;


class Headlink extends \Zend\View\Helper\HeadLink
{
    public function append($value)
    {
       if(file_exists(BASE_INDEX_PATH.$value->href)){
            $value->href = $this->getView()->basePath().$value->href."?_v=".filemtime(BASE_INDEX_PATH.$value->href);
        }
        parent::append($value);
    }
    public function prepend($value)
    {
       if(file_exists(BASE_INDEX_PATH.$value->href)){
            $value->href = $this->getView()->basePath().$value->href."?_v=".filemtime(BASE_INDEX_PATH.$value->href);
        }
        parent::prepend($value);
    }
}