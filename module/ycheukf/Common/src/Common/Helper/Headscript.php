<?php
namespace YcheukfCommon\Helper;


class Headscript extends \Zend\View\Helper\HeadScript
{
    public function append($value)
    {
       if(file_exists(BASE_INDEX_PATH.$value->attributes['src'])){
            $value->attributes['src'] = $this->getView()->basePath().$value->attributes['src']."?_v=".filemtime(BASE_INDEX_PATH.$value->attributes['src']);
        }
        parent::append($value);
    }
    public function prepend($value)
    {
       if(file_exists(BASE_INDEX_PATH.$value->attributes['src'])){
            $value->attributes['src'] = $this->getView()->basePath().$value->attributes['src']."?_v=".filemtime(BASE_INDEX_PATH.$value->attributes['src']);
        }
        parent::prepend($value);
    }
}