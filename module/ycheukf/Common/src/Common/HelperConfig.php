<?php

namespace YcheukfCommon;

use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

class HelperConfig implements ConfigInterface
{
    /**
     * @var array Pre-aliased view helpers
     */
    protected $helpers = array(
        'ycheukfNavigationBreadcrumbs' => 'YcheukfCommon\Helper\NavigationBreadcrumbs',
        'ycheukfNavigationMenu'        => 'YcheukfCommon\Helper\NavigationMenu',
        'ycheukfHeadlink'        => 'YcheukfCommon\Helper\Headlink',
        'ycheukfHeadscript'        => 'YcheukfCommon\Helper\Headscript',
//        'ycheukfNavigationSitemap'     => 'YcheukfCommon\Helper\NavigationSitemap',
    );

    /**
     * Configure the provided service manager instance with the configuration
     * in this class.
     *
     * Adds the invokables defined in this class to the SM managing helpers.
     *
     * @param  ServiceManager $serviceManager
     * @return void
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        foreach ($this->helpers as $name => $className) {
            $serviceManager->setFactory($name, function(ServiceLocatorInterface $sm) use ($className) {
                $class = new $className();//				$this->ycheukfNavigationBreadcrumbs()->setTranslator($this->getHelperPluginManager()->getServiceLocator()->get('translator'));
//var_dump($sm->getServiceLocator()->get('translator'));
//				$class->setTranslator($sm->getServiceLocator()->get('translator'));


                return $class;
            });
        }
    }
}