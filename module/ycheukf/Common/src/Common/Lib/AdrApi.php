<?php
namespace YcheukfCommon\Lib;


class AdrApi
{
    static $instants=null;

    

    static function getInitstant(){

        if (is_null(self::$instants)) {
            include_once("/app/module/Lur/public/adrapi/simplerestapi2.inc.php");
            self::$instants = new \SimpleRestApi2();
        }
        return self::$instants;
    }

    static function callfunc($func, $vars=[]){
        // var_dump(func_get_args());
        return call_user_func_array([self::getInitstant(), $func], $vars);
    }

}