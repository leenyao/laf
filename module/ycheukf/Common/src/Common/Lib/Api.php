<?php
namespace YcheukfCommon\Lib;

class Api{
	protected $aApiResources;
	protected $sm;
	protected $sResource;

    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * Set the event manager instance used by this context
     *
     * @param  EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(\Zend\EventManager\EventManagerInterface $events)
    {
        $this->events = $events;
        return $this;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (!$this->events instanceof EventManagerInterface) {
            $identifiers = array(__CLASS__, get_called_class());
            if (isset($this->eventIdentifier)) {
                if ((is_string($this->eventIdentifier))
                    || (is_array($this->eventIdentifier))
                    || ($this->eventIdentifier instanceof Traversable)
                ) {
                    $identifiers = array_unique($identifiers + (array) $this->eventIdentifier);
                } elseif (is_object($this->eventIdentifier)) {
                    $identifiers[] = $this->eventIdentifier;
                }
                // silently ignore invalid eventIdentifier types
            }
            $this->setEventManager(new \Zend\EventManager\EventManager($identifiers));
        }
        return $this->events;
    }

	function __construct($sm, $sResource){
        $aConfig = $sm->get('config');
		if(!isset($aConfig["apiservice"]['resources']) || count($aConfig["apiservice"]['resources'])<1)
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2023);

		foreach($aConfig["apiservice"]['resources'] as $k=>$row){
			if(is_array($row))
				$this->aApiResources[$k] = $row;
			elseif(is_string($row)){
				$this->aApiResources[$k] = array(
					'table' => $row,
					'driver' => 'mysql',
				);
			}
		}
        $oResult = $sm->get('Zend\Db\Adapter\Adapter')->getSlaveAdapter()->query('SHOW tables')->execute();
        if($oResult->count()){
             while($oResult->valid()){
                $aTmp = $oResult->current();
                $oResult->next();
                if($aTmp)
                    $sTmp = current($aTmp);
                    $this->aApiResources[$sTmp] = array(
					'table' => $sTmp,
					'driver' => 'mysql',
				);;
            }
       }

		$this->sm = $sm;
		$this->sResource = $sResource;
	}
	public function getServiceManager(){
		return $this->sm;
	}

    /**
     * run get operation
     *
     * @param integer $aParam
     * @return string json string
     */
    public function go($aParam, $bCheckToken=true) {
		//init
		list($nCode, $aInit) = $this->_init($aParam, $this->sResource, $bCheckToken);
		if($nCode != 1)\YcheukfCommon\Lib\Functions::throwErrorByCode($nCode);


		$sDriver = $this->getResourceDriver($this->sResource);
//		var_dump(get_class($this));
		switch($sDriver){
			case "mysql":
			case "mongo":
				$sFunc = "_launch".ucfirst($sDriver);
				$aReturn = $this->$sFunc($aParam, $aInit);
				break;
			default:
				\YcheukfCommon\Lib\Functions::throwErrorByCode(2015, null, array("[=replacement1=]" => $sDriver));
				break;
		}
		return $aReturn;
    }


	public function getConfigByResource($sResource){
		if(!isset($this->aApiResources[$sResource]))\YcheukfCommon\Lib\Functions::throwErrorByCode(2007, null, array("[=replacement1=]" => $sResource));
		list($nCode, $sTableName) = $this->getTableName($sResource);
		if($nCode != 1)\YcheukfCommon\Lib\Functions::throwErrorByCode($nCode);
		return array(
			'sTableName'=>$sTableName, 
//			'sMongoDocument'=>"\ZmcBase\MongoDocument\\".(\YcheukfCommon\Lib\Functions::toCamelCase($sTableName))
		);
	}
	public function getResourceDriver($sResource){
		if(isset($this->aApiResources[$sResource])){
			return $this->aApiResources[$sResource]['driver'];
		}else
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2007, null, array("[=replacement1=]" => $sResource));
	}

	protected function getTableName($sResource){
//        var_dump($sResource);
		return array(1, \YcheukfCommon\Lib\Functions::fromCamelCase($this->aApiResources[$sResource]['table']));
	}



	static function getTableNameByResource($sm, $sResource){
		$aConfig = $sm->get('config');
		return $aConfig['apiservice']['resources'][$sResource]['table'];
	}

	protected function _init($aParam, $sResource, $bCheckToken=true) {
		//check params
		list($nCode) = $this->_checkParams($aParam, $bCheckToken);
		if($nCode != 1)\YcheukfCommon\Lib\Functions::throwErrorByCode($nCode);

		//get service & tablename
		$aInit= $this->getConfigByResource($sResource);

		return array(1, $aInit);
	}


	protected function _checkParamsSelect($aSelect){
		if(isset($aSelect)){
			if(!is_array(($aSelect)))
				\YcheukfCommon\Lib\Functions::throwErrorByCode(2004);
			foreach($aSelect as $sKey=>$sVal)
				if(!in_array($sKey, array("columns", "where", "order", "group", "limit", "offset", "countonly", )))
					\YcheukfCommon\Lib\Functions::throwErrorByCode(2005);
		}
		return array(1);
	}

	protected function _checkParams($aParam, $bCheckToken){
		if($bCheckToken == false)return array(1);//非外部调用不需要检查token
		if(is_null($aParam))
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2000);
		if(!isset($aParam["access_token"]))
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2001);

        $sCacheMd5Id = "api___access_token_".$aParam["access_token"];
        if((\YcheukfCommon\Lib\Functions::chkDbproxyToken($sCacheMd5Id)) != 1){//在本实例内未经过验证
            $bTokenCheckFlag = \YcheukfCommon\Lib\Functions::getCache($this->sm, $sCacheMd5Id);
            if(is_null($bTokenCheckFlag) || $bTokenCheckFlag==false){
                $bFlag = \YcheukfCommon\Lib\OauthClient::checkAccess($this->sm, $aParam["access_token"]);
                if(!$bFlag){
                    \YcheukfCommon\Lib\Functions::delCache($this->sm, $sCacheMd5Id);
                    \YcheukfCommon\Lib\Functions::throwErrorByCode($bFlag);
                }else{
                    \YcheukfCommon\Lib\Functions::saveCache($this->sm, $sCacheMd5Id, 1, 3600*24*7);
                }
            }
            \YcheukfCommon\Lib\Functions::setDbproxyToken($sCacheMd5Id);
        }
		if(!isset($this->sResource))
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2003);
		return array(1);
	}

	function chg2MySqlWhere($aWhere){
		$oWhere = new \Zend\Db\Sql\Where;
		if(count($aWhere)<1)return $oWhere;
		$aCondition = array('$and', '$or', '$not', '$nor', );
		$aConditionMap = array('$and'=>'and', '$or'=>'or', '$not'=>'or', '$nor'=>'or', );
		if(!in_array(key($aWhere), $aCondition)){
			$aWhere = array('$and'=>array($aWhere));
		}
//		var_dump($aWhere);
//				$oWhere->equalTo($sField, $sV);
		foreach($aWhere as $k1=>$a1){//第一层 逻辑关系
			$oWhere = $oWhere->nest();

			$sOperation = $aConditionMap[$k1];
			foreach($a1 as $k2=>$a2){//第二层 多个关系实体
				foreach($a2 as $k3=>$a3){ //第三层, 字段=>值
					if(is_array($a3)){
						foreach($a3 as $k4=>$a4){
							switch(strtolower($k4)){
								case '$in':
									if(count($a4)){
										$oWhere->in($k3, $a4);
										$oWhere = $oWhere->$sOperation;
									}
								break;
								case '$gt':
									$oWhere->greaterThan($k3, $a4);
									$oWhere = $oWhere->$sOperation;
								break;
								case '$gte':
									$oWhere->greaterThanOrEqualTo($k3, $a4);
									$oWhere = $oWhere->$sOperation;
								break;
								case '$lt':
									$oWhere->lessThan($k3, $a4);
									$oWhere = $oWhere->$sOperation;
								break;
								case '$lte':
									$oWhere->lessThanOrEqualTo($k3, $a4);
									$oWhere = $oWhere->$sOperation;
								break;
								case '$regex':
									$oWhere->like($k3, '%'.$a4.'%');
									$oWhere = $oWhere->$sOperation;
								break;
								case '$ne':
									$oWhere->notEqualTo($k3, $a4);
									$oWhere = $oWhere->$sOperation;
								break;
								default:
									\YcheukfCommon\Lib\Functions::throwErrorByCode(2031, null, array("[=replacement1=]" => $k4));
								break;
							}
						}
					}else//直接等于
						$oWhere->equalTo($k3, is_int($a3)? intval($a3) : $a3);
						$oWhere = $oWhere->$sOperation;
				}
			}
			$oWhere = $oWhere->unnest();
			
		}
//						$oWhere->or->equalTo('a', 'a');
//						$oWhere->or->equalTo('b', 'b');
		return $oWhere;
	}

	function chg2MongoWhere($sDocument, $aWhere){
		$oDocument = new $sDocument();
		if(count($aWhere)<1)return $aWhere;
		$aCondition = array('$and', '$or', '$not', '$nor', );
		if(!in_array(key($aWhere), $aCondition)){
			$aWhere = array('$and'=>array($aWhere));
		}
		foreach($aWhere as $k1=>$a1){
			foreach($a1 as $k2=>$a2){
				foreach($a2 as $k3=>$a3){
					$sFunc = 'getAnnotation'.\YcheukfCommon\Lib\Functions::toCamelCase($k3);
					switch(strtolower($oDocument->$sFunc())){
						case 'date':
							if(is_array($a3)){
								foreach($a3 as $k4=>$a4)
									$aWhere[$k1][$k2][$k3][$k4] = new \MongoDate(strtotime($a4));
							}else
								$aWhere[$k1][$k2][$k3] = new \MongoDate(strtotime($a3));
							break;
						case 'int':
							if(is_array($a3)){
								foreach($a3 as $k4=>$a4){
									if(strtolower($k3)=='id'){
										if(is_array($a4)){
											foreach($a4 as $aTmp2)
												$sTmp[] = new \MongoId($aTmp2);
										}else{
											$sTmp = new \MongoId($a4);
										}
									}else{
										if(is_array($a4)){
											$sTmp = array_map(function($p){return (int)$p;}, $a4);
										}else
											$sTmp = (int)$a4;
									}
									$aWhere[$k1][$k2][$k3][$k4] = $sTmp;
								}
							}else{
								$aWhere[$k1][$k2][$k3] = (strtolower($k3)=='id') ? new \MongoId($a3): (int)$a3;
							}
							break;
						default:
							break;
					}
				}
			}
		}
		return $aWhere;
	}

	protected function _launchMongoSave($aParam, $aInit){
		$oMongoDm = \YcheukfCommon\Lib\Functions::getMongoDm($this->sm);
		$aMongoDocuments = array();
		$aWhere = isset($aParam['params']["where"]) ? $aParam['params']["where"] : array();
		$aWhere = $this->chg2MongoWhere($aInit['sMongoDocument'], $aWhere);

		if(count($aWhere)){//update
			$oMongoDocument = new $aInit['sMongoDocument']();
			foreach($aParam['params']['dataset'] as $row){
				if(count($row) < 1)continue;
				$oQbUpdate = $oMongoDm->createQueryBuilder($aInit['sMongoDocument'])->update()->multiple(true);
				$oQbUpdate = $oQbUpdate->setQueryArray($aWhere);
				foreach($row as $k=>$v){
					if($k == 'id')continue;
					if(is_array($v)){
						switch(strtolower($k)){
							case '$inc':
								$oQbUpdate->field(key($v))->inc((int)current($v));
								break;
							default:
								\YcheukfCommon\Lib\Functions::throwErrorByCode(2027, null, array("[=replacement1=]" => key($v)));
								break;
						}
					}else{
						$sFunc = 'getAnnotation'.\YcheukfCommon\Lib\Functions::toCamelCase($k);
						switch(strtolower($oMongoDocument->$sFunc())){
							default:
							case 'raw':
								$sTmpValue = $v;
								break;
							case 'int':
								$sTmpValue = (int)($v);
								break;
							case 'date':
								$sTmpValue = new \MongoDate(strtotime($v));
								break;
						}
						$oQbUpdate = $oQbUpdate->field($k)->set($sTmpValue);
					}
				}
				$oCursor = $oQbUpdate->getQuery()->execute();
			}
//			exit;
			$this->getEventManager()->trigger('lf_record_update', $this, array($this->sResource, $aParam['params']['dataset'][0], $aParam['params']["where"]),function ($r) {return true;});
			\YcheukfCommon\Lib\Functions::$dm = null;
			return ($oCursor) ? 1 : 0;
		}else{//insert
			$i = 0;
			foreach($aParam['params']['dataset'] as $row){
				$aMongoDocuments[$i] = new $aInit['sMongoDocument']();
				if(count($row) < 1)continue;
				foreach($row as $k=>$v){
					$sFunc = "set".ucfirst(\YcheukfCommon\Lib\Functions::toCamelCase($k));
					if(!method_exists($aMongoDocuments[$i], $sFunc))
						\YcheukfCommon\Lib\Functions::throwErrorByCode(2016, null, array("[=replacement1=]" => $aInit['sMongoDocument']."::".$sFunc));
					$aMongoDocuments[$i]->$sFunc($v);
				}
				$oMongoDm->persist($aMongoDocuments[$i]);
				$i++;
			}
			$oMongoDm->flush();
			foreach($aMongoDocuments as $oTmp)
				$this->getEventManager()->trigger('lf_record_insert', $this, array($this->sResource, $oTmp->toArray(), $oTmp->getId()),function ($r) {return true;});
			$oMongoDm->getConnection()->close();
			\YcheukfCommon\Lib\Functions::$dm = null;
			return $aMongoDocuments[count($aMongoDocuments)-1]->getId();
		}
	}
}
