<?php
namespace YcheukfCommon\Lib\Api;

class Column extends \YcheukfCommon\Lib\Api{
    protected function _launchMysql($aParam, $aInit){
        $sTableName = $aInit['sTableName'];
        $stmt = $this->sm->get('Zend\Db\Adapter\Adapter')->getSlaveAdapter()->query('SHOW COLUMNS FROM '.$sTableName);
        $oResult = $stmt->execute();
        $aReturnTmp = array();
        while($oResult->valid()){
            $aTmp = $oResult->current();
            $aReturnTmp[] = $aTmp;
            $oResult->next();
        }
        $aReturn = array("dataset"=>$aReturnTmp, "count"=>$oResult->count());
        return ($aReturn);
    }


}
