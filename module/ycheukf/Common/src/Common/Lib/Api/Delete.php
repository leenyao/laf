<?php
namespace YcheukfCommon\Lib\Api;

class Delete extends \YcheukfCommon\Lib\Api{

	protected function _checkParams($aParam, $bCheckToken=true){
		parent::_checkParams($aParam, $bCheckToken);
		if(!isset($aParam['params']['where']) || ! is_array($aParam['params']['where']) ){
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2021);
		}
		return array(1);
	}

	protected function _launchMongo($aParam, $aInit){
		$oMongoDm = \YcheukfCommon\Lib\Functions::getMongoDm($this->sm);
		$oQbFind = $oMongoDm->createQueryBuilder($aInit['sMongoDocument'])->find();
		$aWhere = isset($aParam["params"]["where"]) ? $aParam["params"]["where"] : array();
		$aWhere = $this->chg2MongoWhere($aInit['sMongoDocument'], $aWhere);
		$oQbFind = $oQbFind->setQueryArray($aWhere);
		$oCursor = $oQbFind->getQuery()->execute();
		$sId = null;
		while($oCursor->hasNext()){
			$oCursor->next();
			$aTmp = $oCursor->current()->toArray();
			$aQbRemove = $oMongoDm->createQueryBuilder($aInit['sMongoDocument'])->findAndRemove()->multiple(true);
			$aQbRemove = $aQbRemove->setQueryArray($this->chg2MongoWhere($aInit['sMongoDocument'], array('id'=>$aTmp['id'])));
			$oDocument = $aQbRemove->getQuery()->execute();
			$sId = $oDocument->getId();
			$this->getEventManager()->trigger('delete', $this->sm, array($this->sResource, $aWhere, $sId));
		}

		\YcheukfCommon\Lib\Functions::$dm = null;

		return $sId;
	}

	protected function _launchMysql($aParam, $aInit){
		$sTableName = $aInit['sTableName'];
		$aWhere = isset($aParam["params"]["where"]) ? $aParam["params"]["where"] : array();
		$select = new \Zend\Db\Sql\Delete;
		$aBandData = null;
		$select->from($sTableName);
		if(isset($aWhere)){
			$select->where($this->chg2MySQLWhere($aWhere));
		}
		$stmt = new \Zend\Db\Sql\Sql($this->sm->get('Zend\Db\Adapter\Adapter'));
		$resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
		$oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($select)->execute());
		$sId = $oResult->count();
		$this->getEventManager()->trigger('delete', $this->sm, array($this->sResource, $aWhere, $sId));
		return $sId;
	}

}
