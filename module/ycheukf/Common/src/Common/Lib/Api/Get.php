<?php
namespace YcheukfCommon\Lib\Api;

class Get extends \YcheukfCommon\Lib\Api{
    var $splitChar = '__split__';
    protected function _checkParams($aParam, $bCheckToken){
        parent::_checkParams($aParam, $bCheckToken);

        if(!isset($aParam["params"]))
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2006);
        
        if(isset($aParam['params']["select"]))
            list($nCode) = $this->_checkParamsSelect($aParam['params']["select"]);
        if(isset($aParam['params']["select"]['where'])){
            if(! is_array($aParam['params']["select"]['where']) ){
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2021);
            }
        }

        return array(1);
    }

    
    function _saveCacheKey($aParam, $aInit){
        $oService = $this->sm->get('\ZmcBase\Service\DebugCachekeys');
        $row = array();
        $aBandData = null;
        $row['md5id'] = ("dbapi___".$this->sResource."_".\YcheukfCommon\Lib\Functions::chgSelect2CacheKey($aParam["params"]['select']));
        $row['memo'] = (json_encode($aParam["params"]['select']));
        $row['resource'] = ($this->sResource);

        $select = new \Zend\Db\Sql\Select;
        $select->from('debug_cachekeys');
        $select->where(array('md5id'=>$row['md5id']));
        $resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
        $stmt = new \Zend\Db\Sql\Sql($this->sm->get('Zend\Db\Adapter\Adapter'));
        $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($select)->execute());
        if($oResult->count() < 1)
            $oResult = $oService->create($row);
    }


    static function onlinedebug($key, $s=''){
        
        $s = is_array($s) ? print_r($s, 1) : ( is_bool($s) ? intval($s) : $s);
        $s = date("Y-m-d H:i:s")."\t".$key."\t".$s;
        exec("echo '$s'  >> /tmp/lur.onlinedebug.".date('YmdH'));

    }
    /**
    * select data from mysql
    */
    protected function _launchMysql($aParam, $aInit){
//        $this->_saveCacheKey($aParam, $aInit);
//var_dump($aParam);
//var_dump($aInit);
//exit;
        $sTableName = $aInit['sTableName'];
        $aSelect = isset($aParam["params"]["select"]) ? $aParam["params"]["select"] : array();

//        try{
        // self::onlinedebug('PHP_SELF', $_SERVER);
        // self::onlinedebug('aParam', $aParam);


            /**
             * 获取记录条数
             */
            $select = new \Zend\Db\Sql\Select;
            $aBandData = null;
            $select->from($sTableName);
            // if(isset($aSelect["columns"]) && $aSelect["columns"]!="*")
            //     $select->columns(\YcheukfCommon\Lib\Functions::_frmColumns($aSelect["columns"],'mysql'), false);
            $select->columns(\YcheukfCommon\Lib\Functions::_frmColumns(array("rowcount" =>array('$count'=>'*')),'mysql'), false);
                
            // if(isset($aSelect["group"]))
            //     $select->group($aSelect["group"]);

            if(isset($aSelect["where"])){
                $select->where($this->chg2MySQLWhere($aSelect["where"]));
            }
                // \Application\Model\common::onlinedebug('where', $aSelect["where"]);
                // \Application\Model\common::onlinedebug('getSqlString', $select->getSqlString());

            // $stmt = new \Zend\Db\Sql\Sql($this->sm->get('Zend\Db\Adapter\Adapter')->getSlaveAdapter());
            $stmt = new \Zend\Db\Sql\Sql($this->sm->get('Zend\Db\Adapter\Adapter'));
            $resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
            $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($select)->execute());
            $nRowCount = 0;
            if ($oResult->valid()) {
                $aTmp = $oResult->current()->getArrayCopy();
                $nRowCount = $aTmp['rowcount'];
                // var_dump($aSelect);
                // var_dump($aTmp);
                // var_dump($nRowCount);

            }
            $aReturn = array("dataset"=>array(), "count"=>intval($nRowCount));

            // var_dump($aReturn);


            if(isset($aParam["params"]["select"]['countonly']) && $aParam["params"]["select"]['countonly']){
            }else{


                $select = new \Zend\Db\Sql\Select;
                $aBandData = null;
                $select->from($sTableName);
                if(isset($aSelect["columns"]) && $aSelect["columns"]!="*")
                    $select->columns(\YcheukfCommon\Lib\Functions::_frmColumns($aSelect["columns"],'mysql'), false);
                if(isset($aSelect["group"]))
                    $select->group($aSelect["group"]);
                if(isset($aSelect["where"])){
                    $select->where($this->chg2MySQLWhere($aSelect["where"]));
                }
                
                if(isset($aSelect["order"])){
                    if(is_string($aSelect["order"]) && preg_match("/\(/i", $aSelect["order"]))
                        $select->order(new \Zend\Db\Sql\Expression($aSelect["order"]));
                    else
                        $select->order($aSelect["order"]);
                }
                if(isset($aSelect["limit"]))
                    $select->limit($aSelect["limit"]);
                if(isset($aSelect["offset"])){
    //                $nOffset = isset($aSelect["limit"]) ? ($aSelect["offset"]-1)*$aSelect["limit"] : 0;
                    $select->offset($aSelect["offset"]);
                }
                $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($select)->execute());
                while($oResult->valid()){
                    $aTmp = $oResult->current();
                    $oResult->next();
                    if($aTmp)
                        $aReturn["dataset"][] = $aTmp->getArrayCopy();
                }
            }
//            exit;
//        }catch(\Zend\Db\Adapter\Exception\InvalidQueryException $e){
//            \YcheukfCommon\Lib\Functions::throwErrorByCode(2009, null, array("[=replacement1=]" => $e->getMessage()."\n".$e->getPrevious()->getMessage()));
//        }
            // var_dump($aReturn);

        return $aReturn;
    }

    protected function _launchMongo($aParam, $aInit){
//        $this->_saveCacheKey($aParam, $aInit);
        $oMongoDm = \YcheukfCommon\Lib\Functions::getMongoDm($this->sm);
        $oQbFind = $oMongoDm->createQueryBuilder($aInit['sMongoDocument'])->find();
        $aSelect = isset($aParam["params"]["select"]) ? $aParam["params"]["select"] : array();
        $aWhere = isset($aSelect["where"]) ? $aSelect["where"] : array();
//        \YcheukfCommon\Lib\Functions::debug($aWhere, "[inline]---[api]---aWhere");
        $aWhere = $this->chg2MongoWhere($aInit['sMongoDocument'], $aWhere);
//        \YcheukfCommon\Lib\Functions::debug($aWhere, "[inline]---[api]---aWhere");
        $oQbFind = $oQbFind->setQueryArray($aWhere);

        if(isset($aParam["params"]["select"]["columns"]) && count($aParam["params"]["select"]["columns"])){
            foreach($aParam["params"]["select"]["columns"] as $k=>$v){
                if(is_string($v))
                    $oQbFind->select($v);
            }
        }
        if(isset($aSelect["order"])){
            $aSplit = explode(" ", $aSelect["order"]);
            $oQbFind->sort($aSplit[0], (isset($aSplit[1]) ? $aSplit[1] : 'asc'));
        }
        $oCursor = $oQbFind->getQuery()->execute();
        $aReturn = array();
        $aReturn["count"] = $oCursor->count();
        $aReturn["dataset"] = array();
        if(isset($aParam["params"]["select"]['countonly']) && $aParam["params"]["select"]['countonly']){
        }else{
            if(isset($aSelect["offset"])){
    //            $nOffset = isset($aSelect["limit"]) ? ($aSelect["offset"]-1)*$aSelect["limit"] : 0;
                $oCursor->skip($aSelect["offset"]);
            }
            if(isset($aSelect["limit"]))
                $oCursor->limit($aSelect["limit"]);

            while($oCursor->hasNext()){
                $oCursor->next();
                $aTmp = $oCursor->current()->toArray();
                if(isset($aParam["params"]["select"]["columns"]) && $aParam["params"]["select"]["columns"]!="*"){
                    $aTmp2 = array();
                    foreach($aParam["params"]["select"]["columns"] as $k=>$v){
                        $aTmp = $this->_fmtRows($aTmp, $v);

                        if(is_array($v)){
                            foreach($v as $sOp=>$aTmp3){
                                switch(strtolower(trim($sOp))){
                                    case '$concat':
                                        $aTmp4 = array();
                                        foreach($aTmp3 as $sField){
                                            $aTmp4[] = $aTmp[$sField];
                                        }
                                        $aTmp2[$k] = join($this->splitChar, $aTmp4);
                                        break;
                                    case '$count':
                                        break;
                                    default:
                                        \YcheukfCommon\Lib\Functions::throwErrorByCode(2026, null, array("[=replacement1=]"=>$sOp));
                                        break;
                                }
                            }
                        }else{
                            $sKey = is_int($k) ? $v : $k;
                            $aTmp2[$sKey] = is_int($aTmp[$v]) ? $aTmp[$v] : (string)$aTmp[$v];
                        }
                    }
                    $aTmp = $aTmp2;
                }
                $aReturn["dataset"][] = $aTmp;
            }
        }
        return $aReturn;
    }

    protected function _fmtRows($aTmp, $v){
        if(is_string($v)){
            if(is_object($aTmp[$v])){
                switch(get_class($aTmp[$v])){
                    case 'DateTime':
//                        var_dump($aTmp[$v]->format("Y-m-d H:i:s"));
                        $aTmp[$v] = (string)$aTmp[$v]->format("Y-m-d H:i:s");
                        break;
                    default:
                        break;
                }
            }
        }
        return $aTmp;
    }
}
