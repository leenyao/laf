<?php
namespace YcheukfCommon\Lib\Api;

class Phpsession extends \YcheukfCommon\Lib\Api{

    protected function _checkParams($aParam, $bCheckToken=true){
        parent::_checkParams($aParam, $bCheckToken);
        if(!isset($aParam["params"]) || !isset($aParam["params"]['id']))
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2049);


        return array(1);
    }

    /**
     * run get operation
     *
     * @param integer $aParam
     * @return string json string
     */
    public function go($aParam) {
        //init
        $sType = "redis";
        list($nCode, $aInit) = $this->_init($aParam, $this->sResource);
        if($nCode != 1)\YcheukfCommon\Lib\Functions::throwErrorByCode($nCode);
        $sTableName = $aInit['sTableName'];
        $sSessionKey = isset($aParam["params"]['key']) ? $aParam["params"]['key'] : null;

        switch($sType){
            case 'mysql':
                $select = new \Zend\Db\Sql\Select;
                $select->from($sTableName);
                $select->where(array('id'=>$aParam["params"]['id'], 'name'=>'PHPSESSID'));
                $stmt = new \Zend\Db\Sql\Sql($this->sm->get('Zend\Db\Adapter\Adapter')->getSlaveAdapter());
                $resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
                $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($select)->execute());
                $aReturn = $oResult->current();
                $sSession = $aReturn['data'];
        //        $sSession = '__ZF|a:1:{s:20:"_REQUEST_ACCESS_TIME";d:1395850703.286076068878173828125;}initialized|C:23:"Zend\Stdlib\ArrayObject":220:{a:4:{s:7:"storage";a:1:{s:4:"init";i:1;}s:4:"flag";i:2;s:13:"iteratorClass";s:13:"ArrayIterator";s:19:"protectedProperties";a:4:{i:0;s:7:"storage";i:1;s:4:"flag";i:2;s:13:"iteratorClass";i:3;s:19:"protectedProperties";}}}';
        //        $sSession = '__ZF|a:1:{s:20:"_REQUEST_ACCESS_TIME";dbproxy_token|s:40:"13d005725adfa4bbff23954cb5e79d208069a068";d:1395891766.261950969696044921875;}initialized|C:23:"Zend\Stdlib\ArrayObject":127:{a:4:{s:7:"storage";a:1:{s:4:"init";i:1;}s:4:"flag";i:2;s:13:"iteratorClass";s:13:"ArrayIterator";s:19:"protectedProperties";N;}}FlashMessenger|C:23:"Zend\Stdlib\ArrayObject":112:{a:4:{s:7:"storage";a:0:{}s:4:"flag";i:2;s:13:"iteratorClass";s:13:"ArrayIterator";s:19:"protectedProperties";N;}}bTokenCheckFlag|b:1;user_token|N;';

                $aSession = $this->unserialize_php($sSession);

        //        session_decode('__ZF|a:1:{s:20:"_REQUEST_ACCESS_TIME";d:1395755367.886744022369384765625;}initialized|C:23:"Zend\Stdlib\ArrayObject":220:{a:4:{s:7:"storage";a:1:{s:4:"init";i:1;}s:4:"flag";i:2;s:13:"iteratorClass";s:13:"ArrayIterator";s:19:"protectedProperties";a:4:{i:0;s:7:"storage";i:1;s:4:"flag";i:2;s:13:"iteratorClass";i:3;s:19:"protectedProperties";}}}');

                break;
            default:
            case 'redis':
                $sKey = "PHPREDIS_SESSION:".$aParam["params"]['id'];
                $sSession = (\YcheukfCommon\Lib\Functions::getCache($this->sm, $sKey, false));
                $aSession = $this->unserialize_php($sSession);
//                var_dump($aSession);
                break;
        }
        return (!is_null($sSessionKey))  ? (isset($aSession[$sSessionKey]) ? $aSession[$sSessionKey] : "") : $aSession;

    }
    function unserialize_php($session_data) {
        $return_data = array();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            if (!strstr(substr($session_data, $offset), "|")) {
                break;
//                \YcheukfCommon\Lib\Functions::throwErrorByCode(2050, null, array("[=replacement1=]" => substr($session_data, $offset)));
            }
            $pos = strpos($session_data, "|", $offset);
            $num = $pos - $offset;
            $varname = substr($session_data, $offset, $num);
            $offset += $num + 1;
            $data = unserialize(substr($session_data, $offset));
            $return_data[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $return_data;
    }
}
