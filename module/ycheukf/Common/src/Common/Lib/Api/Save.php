<?php
namespace YcheukfCommon\Lib\Api;

class Save extends \YcheukfCommon\Lib\Api{

	protected function _checkParams($aParam, $bCheckToken=true){
		parent::_checkParams($aParam, $bCheckToken);

		if(!isset($aParam["params"]["dataset"]))
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2010);
		if(!is_array($aParam["params"]["dataset"]))
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2011);

		if(count($aParam["params"]["dataset"])<1)
			\YcheukfCommon\Lib\Functions::throwErrorByCode(2012);

		return array(1);
	}
/**
	protected function _launchMongo($aParam, $aInit){
		$oMongoDm = \YcheukfCommon\Lib\Functions::getMongoDm($this->sm);
		$oQueryBuilder = $oMongoDm->createQueryBuilder($aInit['sMongoDocument'])->insert();
		foreach($aParam['params']['dataset'] as $row){
			$oMongoDocument = new $aInit['sMongoDocument']();
			if(count($row) < 1)continue;
			foreach($row as $k=>$v){
				$oQueryBuilder->field(ucfirst(\YcheukfCommon\Lib\Functions::toCamelCase($k)))->set($v);
			}
			$oQueryBuilder->getQuery()->execute();
		}
//		$oMongoDm->flush();
		$oMongoDm->getConnection()->close();
		return 1;
	}
*/
	protected function _launchMongo($aParam, $aInit){
		return $this->_launchMongoSave($aParam, $aInit);
	}

	protected function _launchMysql($aParam, $aInit){

		$oAdapter = $this->sm->get('Zend\Db\Adapter\Adapter');
		$aWhere = isset($aParam['params']["where"]) ? $aParam['params']["where"] : array();
		foreach($aParam['params']['dataset'] as $row){
			if(count($aWhere)){//�޸�
				$oUpdate = new \Zend\Db\Sql\Update();
				$oUpdate->table($aInit['sTableName']);
				if(!is_array($row))
					\YcheukfCommon\Lib\Functions::throwErrorByCode(2044, null, array("[=replacement1=]" => $row));
				$aTmp2 = array();
				foreach($row as $k=>$sAtmp){
					if(in_array($k, array('$inc'))){
						$aTmp2[key($sAtmp)] = new \Zend\Db\Sql\Expression(key($sAtmp).'+1');
					}else{
						$aTmp2[$k] = $sAtmp;
					}
				}
				$oUpdate->set($aTmp2);
				$oUpdate->where($this->chg2MySQLWhere($aWhere));

				$stmt = new \Zend\Db\Sql\Sql($oAdapter);
				$resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
				$oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($oUpdate)->execute());
				$this->getEventManager()->trigger('lf_record_update', $this->sm, array($this->sResource, $row, (isset($aParam['params']["where"]) ? $aParam['params']["where"] : array())),function ($r) {return true;});
				$sId = isset($aParam['params']["where"]['id']) ? $aParam['params']["where"]['id'] : $oResult->count();
			}else{//����
				$oInsert = new \Zend\Db\Sql\Insert();
				$oInsert->into($aInit['sTableName']);
				if(!is_array($row))
					\YcheukfCommon\Lib\Functions::throwErrorByCode(2044, null, array("[=replacement1=]" => $row));
				$oInsert->values($row);
				$stmt = new \Zend\Db\Sql\Sql($oAdapter);
				$resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
				$oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($oInsert)->execute());
				$sId = isset($row['id']) ? $row['id'] : $resultSet->getDataSource()->getGeneratedValue();
//				var_dump($row);
//		\YcheukfCommon\Lib\Functions::debug(array($row,$resultSet->getDataSource()->getGeneratedValue(), $sId), "[inline]---[api]---getGeneratedValue");
//				var_dump($oResult->getDataSource()->getGeneratedValue());
//				exit;
//				$sId = $oResult->getId();
				$this->getEventManager()->trigger('lf_record_insert', $this->sm, array($this->sResource, $row, $sId),function ($r) {return true;});
			}
		}
		return $sId;
	}

}
