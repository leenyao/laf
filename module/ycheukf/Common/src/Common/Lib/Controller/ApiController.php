<?php
namespace YcheukfCommon\Lib\Controller;

use Application\Model\Common;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ApiController extends AbstractActionController
{

    /**
    * load resource class
    */
    protected function _loadResourceClass($type='common', $aRequest=null){
        if(is_null($aRequest)){
            $oRequest = $this->getServiceLocator()->get('request');
            $sOp = $oRequest->getPost('op', null);//operation
            $sResource = $oRequest->getPost('resource', null);//operation
        }else{
            $sOp = $aRequest['op'];
            $sResource = $aRequest['resource'];
        }
        if(is_null($sOp))
            $sOp = $this->params()->fromRoute('op',null);

        if(is_null($sResource))
            $sResource = $this->params()->fromRoute('resource',null);

        if($type=='common')
            $sClassName = '\YcheukfCommon\Lib\Api\\'.ucfirst(strtolower($sOp));//resource class
        elseif($type=='procedures'){
            $sResource = "user";//任意写的一个资源, 用于兼容
            $sClassName = '\Application\Procedures\\'.ucfirst(strtolower($sOp));//resource class
        }elseif($type=='phpsession'){
            $sResource = "phpsession";
            $sClassName = '\YcheukfCommon\Lib\Api\\Phpsession';//resource class
        }
        if(!class_exists($sClassName))
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2051, null, array("[=replacement1=]" => $sClassName));
        return new $sClassName($this->getServiceLocator(), $sResource);
    }

    /**
    * load resource class
    */
    protected function _fmtApiParams($aRequest=null){
        if(is_null($aRequest)){
            $oRequest = $this->getServiceLocator()->get('request');
            $aPost = $oRequest->getPost()->toArray();
        }else
            $aPost = $aRequest;
        if(empty($aPost))\YcheukfCommon\Lib\Functions::throwErrorByCode(2017);
        $aRe = array(
            "access_token" => (isset($aPost['access_token']) ? $aPost['access_token'] : ''),
            "params" => (isset($aPost['params']) ? json_decode($aPost['params'], 1) : array()),
        );
        return $aRe;
    }
    /**
    * common operation
    */
    public function commonAction($aRequest=null, $bReturn=false, $bCheckToken=true)
    {
//        $typeHeader = \Zend\Http\Header\ContentType::fromString('');

//        $this->response->getHeaders()->addHeader($typeHeader);
        try{
            $o = $this->_loadResourceClass('common', $aRequest);
            $aApiParams = $this->_fmtApiParams($aRequest);
            $sReturn = \YcheukfCommon\Lib\Functions::fmtApiReturn($o->go($aApiParams, $bCheckToken));
            unset($o);
            if(!is_null(json_decode($sReturn))){
                $request = $this->getRequest();
//                var_dump(get_class($request));
//                exit;
//                header("Content-Type: application/json; charset=utf-8;");
                return $sReturn;
            }else{
//                echo $sReturn;
            }
//            exit;
        }catch(\Zend\Db\Adapter\Exception\InvalidQueryException $e){
            if($bReturn)
                return $e->getMessage()."\n\n".$e->getPrevious()->getMessage();
            else
                echo $e->getMessage()."\n\n".$e->getPrevious()->getMessage();
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2009, null, array("[=replacement1=]" => $e->getMessage()."\n".$e->getPrevious()->getMessage()));
        }catch(\YcheukfCommon\Lib\Exception $e){
//            var_dump($e->getMessage());
            if($bReturn)
                return $e->getMessage();
//            else
//                echo $e->getMessage();
        }
        return $this->response;
    }

    /**
    * common operation
    */
    public function phpsessionAction($aRequest=null, $bCheckToken=true)
    {
        try{
            $o = $this->_loadResourceClass('phpsession', $aRequest);
            $aApiParams = $this->_fmtApiParams($aRequest);
//            var_dump($aApiParams);
//            session_decode('__ZF|a:1:{s:20:"_REQUEST_ACCESS_TIME";d:1395755367.886744022369384765625;}initialized|C:23:"Zend\Stdlib\ArrayObject":220:{a:4:{s:7:"storage";a:1:{s:4:"init";i:1;}s:4:"flag";i:2;s:13:"iteratorClass";s:13:"ArrayIterator";s:19:"protectedProperties";a:4:{i:0;s:7:"storage";i:1;s:4:"flag";i:2;s:13:"iteratorClass";i:3;s:19:"protectedProperties";}}}');
//
//            var_dump($_SESSION);
            echo \YcheukfCommon\Lib\Functions::fmtApiReturn($o->go($aApiParams, $bCheckToken));
//            exit;
        }catch(\Zend\Db\Adapter\Exception\InvalidQueryException $e){
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2009, null, array("[=replacement1=]" => $e->getMessage()."\n".$e->getPrevious()->getMessage()));
        }catch(\YcheukfCommon\Lib\Exception $e){
            echo $e->getMessage();
        }
        return $this->response;
    }

    /**
    * Procedures operation
    */
    public function proceduresAction($aRequest=null, $bReturn=false, $bCheckToken=true)
    {
//        return '{status:1, data:[]}';
        try{
            $o = $this->_loadResourceClass('procedures', $aRequest);
            $aApiParams = $this->_fmtApiParams($aRequest);
            $sReturn = \YcheukfCommon\Lib\Functions::fmtApiReturn($o->go($aApiParams, $bCheckToken));
//            var_dump($sReturn);
            if($bReturn)
                return $sReturn;
            else
                echo $sReturn;
        }catch(\Zend\Db\Adapter\Exception\InvalidQueryException $e){
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2009, null, array("[=replacement1=]" => $e->getMessage()."\n".$e->getPrevious()->getMessage()));
        }catch(\YcheukfCommon\Lib\Exception $e){
            if($bReturn)
                return $e->getMessage();
            else
                echo $e->getMessage();
        }
        return $this->response;
    }

    public function indexAction(){
        echo \YcheukfCommon\Lib\Functions::fmtApiReturn('list');
        return $this->response;
    }

    public function imguploadAction(){
        header("Access-Control-Allow-Origin: *");
        $typeHeader = \Zend\Http\Header\ContentType::fromString('Content-Type: application/json; charset=utf-8;');
        $this->response->getHeaders()->addHeader($typeHeader);
        try{
            $aConfig = ($this->getServiceLocator()->get('config'));
            $request = $this->getRequest();
            $posts = $request->getPost();

            if(isset($aConfig['imgapi_allow_ips']) && isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'] , $aConfig['imgapi_allow_ips'])){
            }else{//正式环境加验证
                $bAccesChkFlag = true;
               if(isset($posts->sp)){
                    $aM151 = \Application\Model\Common::getResourceMetaData($this->getServiceLocator(), 151, 1);
                    if($posts->sp != $aM151['dataset'][0]['label'])
                        \YcheukfCommon\Lib\Functions::throwErrorByCode(2055);
                    $bAccesChkFlag = false;
                }
                if($bAccesChkFlag){
                    $sToken = $this->getServiceLocator()->get('Zend\Session\SessionManager')->getStorage()->user_token;
                    if(is_null($sToken)){
                        $sToken = isset($posts->access_token) ? $posts->access_token : null;
                        if(is_null($sToken) || \YcheukfCommon\Lib\OauthClient::checkAccess($this->getServiceLocator(), $sToken) == false){
                            \YcheukfCommon\Lib\Functions::throwErrorByCode(2055);
                        }
                    }
                }
             }
            $dst_w  = isset($posts->dst_w) ? $posts->dst_w : 400;
            $dst_h  = isset($posts->dst_h) ? $posts->dst_h : 400;
            $dst_x  = isset($posts->dst_x) ? $posts->dst_x : 0;
            $dst_y  = isset($posts->dst_y) ? $posts->dst_y : 0;
            $aFiles = $request->getFiles('uploadfile', null);
            $aSize = $request->getQuery('size', null);
            $aExt = $request->getQuery('ext', null);
            $sZoom = $request->getQuery('zoom', null);
            if(!is_null($aSize)){
                list($nWidth, $nHeight) = explode(",", $aSize);
                $aPicSize = getimagesize($aFiles['tmp_name']);
                if((int)$nWidth === (int)$aPicSize[0] && (int)$nHeight === (int)$aPicSize[1]){
                }else{
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2054, null, array("[=replacement1=]" => $nWidth."x".$nHeight, "[=replacement2=]" => $aPicSize[0]."x".$aPicSize[1]));
                }
            }
            if(is_null($aFiles))
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2029);
            if(!is_null($aExt)){
                $aPath = pathinfo($aFiles["name"]);
                $sExtension = ".".strtolower($aPath["extension"]);
                $aExts = explode(",", $aExt);
                if(!in_array($sExtension, $aExts))
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2028, null, array("[=replacement1=]" => $aFiles['type']));
            }

            $sPath =  \Application\Model\Common::uploadFile($aFiles, $this->getServiceLocator(),$dst_w,$dst_h,$dst_x,$dst_y);

            if($sPath){
                if(strtolower($sZoom)=='small'){
                    $sPath = str_replace(".jpg", ".small.jpg", $sPath);
                    $sPath = str_replace(".png", ".small.png", $sPath);
                }
                echo \YcheukfCommon\Lib\Functions::fmtApiReturn($sPath);
            }else
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2030);
        }catch(\YcheukfCommon\Lib\Exception $e){
            echo ($e->getMessage());
            exit;
        }
        return $this->response;
    }

    /**
     * 图片裁剪
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function imagecroppingAction(){
        header("Access-Control-Allow-Origin: *");
        $typeHeader = \Zend\Http\Header\ContentType::fromString('Content-Type: application/json; charset=utf-8;');
        $this->response->getHeaders()->addHeader($typeHeader);
        try{
            $request = $this->getRequest();
            $posts = $request->getPost();
            $dst_w  = isset($posts->dst_w) ? $posts->dst_w : 400;
            $dst_h  = isset($posts->dst_h) ? $posts->dst_h : 400;
            $dst_x  = isset($posts->dst_x) ? $posts->dst_x : 0;
            $dst_y  = isset($posts->dst_y) ? $posts->dst_y : 0;
            $path = isset($posts->path) ? BASE_INDEX_PATH.'/'.$posts->path : '';
            $filename = isset($posts->filename) ? $posts->filename : '';
            if(empty($path) || !file_exists($path.'/'.$filename))
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2029);

            $reImages =  \Application\Model\Common::imagecropping($path,$filename,$dst_x,$dst_y,$dst_w,$dst_h);

            if($reImages){
                echo \YcheukfCommon\Lib\Functions::fmtApiReturn(true);
            }else{
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2030);
            }
        }catch(\YcheukfCommon\Lib\Exception $e){
            echo json_decode($e->getMessage());
        }
        return $this->response;
    }
}
