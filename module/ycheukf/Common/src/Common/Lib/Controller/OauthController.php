<?php
namespace YcheukfCommon\Lib\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class OauthController extends AbstractActionController
{

    /**
    * common operation
    */
    public function tokenAction()
    {
        try{
            $oRequest = $this->getServiceLocator()->get('request');
            $aPost = $oRequest->getPost()->toArray();
            $sToken = YcheukfCommon\Lib\OauthClient::getToken($aPost);
            echo \YcheukfCommon\Lib\Functions::fmtApiReturn($sToken);
        }catch(\Zend\Db\Adapter\Exception\InvalidQueryException $e){
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2009, null, array("[=replacement1=]" => $e->getMessage()."\n".$e->getPrevious()->getMessage()));
        }catch(\YcheukfCommon\Lib\Exception $e){
            echo $e->getMessage();
        }
        return $this->response;
    }


    /**
    * common operation
    */
    public function authorizeAction()
    {
        try{
            $sToken = $this->_fmtAuthorizeParams();
            $aData = $this->_getDataByToken($sToken);
            echo \YcheukfCommon\Lib\Functions::fmtApiReturn($aData);
        }catch(\Zend\Db\Adapter\Exception\InvalidQueryException $e){
            $msg = sprintf('%s: "%s"; File: %s; Line #%d', $e->getMessage(), $e->getPrevious()->getMessage(), $e->getFile(), $e->getLine());
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2040);
        }catch(\YcheukfCommon\Lib\Exception $e){
            echo $e->getMessage();
        }
        return $this->response;
    }

    protected function _getDataByToken($sToken){
        $select = new \Zend\Db\Sql\Select;
        $aBandData = null;
//        $oService = $this->getZmcService('oauthtokens');
        $oService = \YcheukfCommon\Lib\Api::getZmcService($this->getServiceLocator(), 'oauthtokens');
        $select->from(\YcheukfCommon\Lib\Api::getTableNameByResource($this->getServiceLocator(),'oauthtokens'));
        $select->where(array('access_token'=>$sToken));
        $stmt = new \Zend\Db\Sql\Sql($oService->getMapper()->getDbSlaveAdapter());
        $resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
        $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($select)->execute($aBandData));

        $aReturn = array();
        if($oResult->valid()){
            $aTmp = $oResult->current();
            if($aTmp)
                $aReturn = $aTmp->getArrayCopy();
        }
        if($oResult->count() < 1)
            \YcheukfCommon\Lib\Functions::throwErrorByCode(1201);
        return $aReturn;
    }


    /**
    * load resource class
    */
    protected function _fmtAuthorizeParams(){
        $oRequest = $this->getServiceLocator()->get('request');
        $aPost = $oRequest->getPost()->toArray();
        if(empty($aPost))\YcheukfCommon\Lib\Functions::throwErrorByCode(2039);
        $sToken = isset($aPost['access_token']) ? $aPost['access_token'] : null;
        if(is_null($sToken))
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2001);
        return $sToken;
    }


}
