<?php
namespace YcheukfCommon\Lib;

class Crondjob{
	var $lastRunTime;
	var $oFrameworker;
	function __construct($oFrameworker){
		$this->lastRunTime = time();
		$this->oFrameworker = $oFrameworker;
		
		//不要使用LAFCACHE
		\YcheukfCommon\Lib\Functions::setUseLafCacheFlag(false);
	}


	function __get($name='')
	{
	    switch ($name) {
	        case 'serviceManager':
	            return $this->oFrameworker->sm;
	            break;
	        default:
	            break;
	    }
	}
	// 

	//获取触发表中的数据句柄
	function _checkTriggerTable($syncFlagField, $aTables, $aMetadataType=0){

		if (is_string($aTables)) {
			$aTables = array($aTables);
		}
		$sSql = " select * from ".$this->oFrameworker->config['crondjob']['sGlobalTriggerTalbe']." where type in (\"".join("\",\"", $aTables)."\") and ".$syncFlagField." = 0  order by id asc  limit 0,".$this->oFrameworker->config['crondjob']['nGlobalLimitNum'];
// echo $sSql."\n";

		//读取trigger表的记录
		$aTriggerId = $aId2TriggerRow = array();
		$oPDOStatement = $this->oFrameworker->queryPdo($sSql);
		if($oPDOStatement && $oPDOStatement->rowCount()){
			foreach($oPDOStatement as $result) {
//				$sWhere = (isset($aGlobalResTable[$result['type']]['where'])||!empty($aGlobalResTable[$result['type']]['where'])) ? " and ".$aGlobalResTable[$result['type']]['where'] : "";

				$sWhere =  "";
			// var_dump($result['type'], $aTables	);

				
				if($result['type'] == 'metadata'){
					 if(in_array($result['metadata_type'],  $aMetadataType)){
					 }else{
						$aTriggerId[] = $result['id'];
					}
				}elseif(in_array($result['type'],  $aTables)){
					$aTriggerId[] = $result['id'];
				}else{	 
				}
				$aId2TriggerRow[$result['id']] = $result;

				if($result['optype'] != 'd'){
					$chkSql = " select id from  " . $result['type'] . "  where id  = " . $result['resid'] . " ".$sWhere;
					$oPDOStatement = $this->oFrameworker->queryPdo($chkSql);
					if(!$oPDOStatement || $oPDOStatement->rowCount()<1){//检查该条触发器是否符合条件, 若满足本条件, 则说明该记录已经被删除
						$aTriggerId[] = $result['id'];
						unset($aId2TriggerRow[$result['id']]);
					}
				}
			 }

		}else{//没有需要处理的记录
			return array(array(), array());
		}

		$aResourceResult = array();
		//找不到记录, 说明还没处理就已经被删除
		if(count($aId2TriggerRow)){
			foreach($aId2TriggerRow as $id=>$row){
				$triSql = " select * from  " . $row['type'] . "  where  id  = " . $row['resid'] . " ";
				$oPDOStatement = $this->oFrameworker->queryPdo($triSql);
				if($oPDOStatement && $oPDOStatement->rowCount() < 1){
					switch($row['optype']){
						case 'i':
						case 'u':
							$aTriggerId[] = $id;
							break;
						case 'd':
							$aResourceResult[$id] = $row;
							break;
					}
				}else{
					$row['resdata'] = $oPDOStatement->fetch(\PDO::FETCH_ASSOC);
					$aResourceResult[$id] = $row;
				}
			}
		}
		return array($aTriggerId, $aResourceResult);
	
	}

	function _updateSyncFlag($aTriggerId, $syncFlagField){
		if (!is_array($aTriggerId)) {
			$aTriggerId = array($aTriggerId);
		}
		if(count($aTriggerId)){
			$sSql = " update ".$this->oFrameworker->config['crondjob']['sGlobalTriggerTalbe']." set ".$syncFlagField."=1 where id in (".join(",", $aTriggerId).")";
			$this->oFrameworker->queryPdo($sSql);
			echoMsg("update skip ids:".$syncFlagField.' in '.join(',', $aTriggerId));
		}
	
	}
    public function isDebug()
    {
        $nParamIndex = 2;
        $is = 0;
        if (isset($_SERVER['argv'][$nParamIndex])) {
            $is = intval($_SERVER['argv'][$nParamIndex]);
            if (!in_array($is, [0,1])) {
                echo ("wrong param[".$nParamIndex."]:".$is."\n");
                exit;
            }
        }else{
            $is = 0;
        }
        return $is;
    }


	public function getLastRunTime()
	{
		return $this->lastRunTime;
	}

	// $nOffsetTime=0
	//更新计时器
	function updateTimer($nOffsetTime=0){
		$this->lastRunTime = time() + $nOffsetTime;
	}
}

