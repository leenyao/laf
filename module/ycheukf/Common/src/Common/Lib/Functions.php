<?php
namespace YcheukfCommon\Lib;


class Functions
{
    static $aErrorCode=null;
    static $dm=null;
    static $oMem=null;
    static $aCacheObject=array();
    static $aStaticVars=array();
    static $aCacheKeys=array();
    static $aDbproxyToken=array();
    static $bUseLafCacheFlag=true;
    static $PDOObejct=null;
    static $ServiceManager=null;



    static function getServiceManager()
    {
        return self::$ServiceManager;
    }

    static function setServiceManager(&$p)
    {
        self::$ServiceManager = $p;
    }

    // 方糖气球信息
    static function sc_send(  $text , $desp = '' , $key = 'SCU73227Tb35fc659ee25e5c55d6551ee354b8a945e0574d79f0ca'  )
    {
            $postdata = http_build_query(
            array(
                'text' => $text,
                'desp' => $desp
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context  = stream_context_create($opts);
        return $result = file_get_contents('https://sc.ftqq.com/'.$key.'.send', false, $context);

    }


    /**
    * 格式化字符串以逗号分隔
    */
    static function fmtSplitString($sString){

        $sString = preg_replace("/\s+/", "", $sString);
        $sString = str_replace("，", ",", $sString);
        $sString = str_replace("；", ",", $sString);
        $sString = str_replace(";", ",", $sString);
        $sString = str_replace("\.", ",", $sString);
        $sReturn = explode(",", $sString);
        $sReturn = array_unique($sReturn);
        $sReturn = array_filter($sReturn);
        return is_array($sReturn) && count($sReturn) ? join(',', $sReturn) : '';
    }

    static function genNormalReportMenuConfig($source, $sLabelPre='menu_'){
        return array(
            'name' => $sLabelPre . $source,
            'label' => $sLabelPre . $source,
            'route' => 'zfcadmin/report',
            'params' => array('type' => $source),
        );
    }

    static function fmtHtmlTable($aData, $nColumn=3, $aTableConfig=array())
    {
        $sReturn = '<table class="table table-bordered table-hover">
            <tbody><tr>';
        $index = 0;
        
        if (is_array($aData) && count($aData)) {
            foreach ($aData as $sKtmp1 => $aRowTmp1) {
                if($index!=0 && $index%$nColumn==0){
                    $sReturn .= "\n</tr>\n\r<tr >";
                }
                $sReturn .= "\n<td>".$aRowTmp1."</td>";
                $index++;
            }
        }
        $iExtraTd = (($index%$nColumn)==0?0:$nColumn-($index%$nColumn));
        if ($iExtraTd>0) {
            for ($i=0; $i < $iExtraTd; $i++) { 
                $sReturn .= "\n<td></td>";
            }
        }

        $sReturn .= '
            </tr></tbody>
        </table>';
        return $sReturn;
    }


    static function count_days($a,$b){

        $zero1=strtotime ($a); //当前时间  ,注意H 是24小时 h是12小时 
        $zero2=strtotime ($b);  //过年时间，不能写2014-1-21 24:00:00  这样不对 
        $guonian=(($zero1-$zero2)/86400); //60s*60min*24h   

        return ($guonian);
    }

    static function shuffle_assoc($list) { 
      if (!is_array($list)) return $list; 

      $keys = array_keys($list); 
      shuffle($keys); 
      $random = array(); 
      foreach ($keys as $key) { 
        $random[$key] = $list[$key]; 
      }
      return $random; 
    } 

    static function getRemoteClientIp() { 
        if (isset($_SERVER['HTTP_X_REAL_IP'])) {
            return $_SERVER['HTTP_X_REAL_IP'];
        }
        if (isset($_SERVER['REMOTE_ADDR'])) {
            return $_SERVER['REMOTE_ADDR'];
        }
        return null;
    }


    static function isIpv4Address($host)
    {
        return preg_match('/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/', $host) === 1;
    }

    static function setUseLafCacheFlag($b=true){
        self::$bUseLafCacheFlag = $b;
    }
    static function getUseLafCacheFlag(){
        return self::$bUseLafCacheFlag;
    }

    static function replaceAdrTag($s){

        $s = preg_replace('/\[adrid=\d+\]/i', '', $s);   
        $s = preg_replace('/\[adrsplitid=\w+\]/i', '', $s);   
        
        return $s;
    }
    // 获取点击率
    static function getClickRateByMeo($s='', $type)
    {
        $return = [
            'pc' => 0.01,
            'phone' => 0.035,
            'pad' => 0.025,
            'ott' => 0,
        ];
        preg_match_all('/点击率:.*PC\s+([0-9\.]+)%.*/i', $s, $matched);
        if (isset($matched[1]) && isset($matched[1][0])) {
            $return['pc'] = $matched[1][0]*0.01;
        }
        
        preg_match_all('/点击率:.*PAD\s+([0-9\.]+)%.*/i', $s, $matched);
        if (isset($matched[1]) && isset($matched[1][0])) {
            $return['pad'] = $matched[1][0]*0.01;
        }
        preg_match_all('/点击率:.*PHONE\s+([0-9\.]+)%.*/i', $s, $matched);
        if (isset($matched[1]) && isset($matched[1][0])) {
            $return['phone'] = $matched[1][0]*0.01;
        }
        preg_match_all('/点击率:.*OTT\s+([0-9\.]+)%.*/i', $s, $matched);
        if (isset($matched[1]) && isset($matched[1][0])) {
            $return['ott'] = $matched[1][0]*0.01;
        }
        return isset($return[strtolower($type)]) ? $return[strtolower($type)] : 0;
    }
    /**
     * 上传图片
     * @param $aFiles 上传图片内容
     * @param $sm      service
     * @param number $dst_w 裁剪后图片宽度
     * @param number $dst_h 裁剪后图片高度
     * @param number $x        截取X坐标
     * @param number $y        截取Y坐标
     * @return Ambigous <string, boolean>
     */
    static function uploadFile($aFiles, $sm){
        $dst_w = 400;$dst_h = 400;$x=0;$y=0;
        $aConfig = $sm->get('config');
         $aPath = pathinfo($aFiles["name"]);
         // var_dump($aPath);
        $sExtension = isset($aPath["extension"]) ? ".".strtolower($aPath["extension"]) : "";
         if(in_array($sExtension, array('.jpg','.png',  '.jpeg', '.apk','.doc','.docx', '.csv', '.xlsx', '.xls', '.pdf'))){
         }else{
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2028, null, array("[=replacement1=]" => $aFiles['type']));
         }

        $sUploadPath = "";
        if($sExtension == '.apk'){
            $sUploadPath = $aConfig['tmpupload_dir']."/apk/";
            $sUploadName = $aFiles["name"];
        }
        else{
            if($aFiles['size'] == 0 || $aFiles['size'] > 10*1024*1024){
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2028,'图片过大，最大支持10M。');
            }
            if(empty($sUploadPath))$sUploadPath = $aConfig['tmpupload_dir']."/";

            $sUploadName = uniqid().$sExtension;
        }

        //为避免某些os权限问题, 去除建目录逻辑
        //
        // if(!file_exists($sUploadPath)){
            // $oldmask = umask(0);
            // chmod('/app/public/tmpupload/2016' ,0777); 
            // mkdir($sUploadPath, 0777, true);
            // umask($oldmask);
            // echo $sUploadPath;
            // exec("mkdir -p ".$sUploadPath);
            // sleep(0.5);
        // }
//        var_dump($sUploadPath.'/'.$sUploadName);
        $bFlag = move_uploaded_file($aFiles["tmp_name"], $sUploadPath.'/'.$sUploadName);
        if ($bFlag && in_array($sExtension, array('.jpg', '.png', '.gif', '.jpeg'))){
            $aTmp = (getimagesize($sUploadPath.'/'.$sUploadName));
            list($nNewWidth, $nNewHeight) = self::getCutZoomwh($aTmp[0], $aTmp[1]);
           self::imageZooming($sUploadPath,$sUploadName,$x,$y,$nNewWidth, $nNewHeight, 80, 'small');
            list($nNewWidth, $nNewHeight) = self::getCutZoomwh($aTmp[0], $aTmp[1], 800);
           self::imageZooming($sUploadPath,$sUploadName,$x,$y,$nNewWidth, $nNewHeight, 95, 'medium');
        }
        return $bFlag ? str_replace("./public/", "/", $sUploadPath).$sUploadName : false;
    }

    static public function isDate( $dateString ) {
        return strtotime( date('Y-m-d', strtotime($dateString)) ) === strtotime( $dateString );
    }

    static public function getCutZoomwh($nWidth, $nHeight, $nStandar=400){
        $nRe1 = $nWidth;
        $nRe2 = $nHeight;
//        $nMax = $nWidth>$nHeight ? $nWidth : $nHeight;
        $nMax = $nWidth;
        $nPercent = 0.75;
        while($nMax*$nPercent > $nStandar){
            $nMax = $nMax*$nPercent;
            $nRe1 = $nRe1*$nPercent;
            $nRe2 = $nRe2*$nPercent;
        }
        return array($nRe1, $nRe2);
    }

    /**
     *图片裁剪和缩放
     * @param String $path 图片路径
     * @param String $fileName 图片名称,带后缀
     * @param number $x 截取X坐标
     * @param number $y 截取Y坐标
     * @param number $dst_w 缩放宽度
     * @param number $dst_w 缩放宽度
     * @param number $dst_h 缩放高度
     * @param number $quality 图片质量
     */
    static public function imageZooming($path,$fileName,$x=0,$y=0,$dst_w=400,$dst_h=400,$quality=90, $sSuffix='small'){
        $aryFile = explode('.', $fileName);
        $image = $path.'/'.$fileName;
        list($src_w,$src_h) = getimagesize($image);
        if(!file_exists($image) || empty($src_w) || empty($src_h) ){
            return false;
        }
        $dst_scale = $dst_h/$dst_w; //目标图像长宽比
        $src_scale = $src_h/$src_w; // 原图长宽比

        if ($src_w <= $dst_w && $src_h <= $dst_h){
            //图片尺寸比目标尺寸小
            $w = intval($src_w);
            $h = intval($src_h);
        }else{
            if($src_scale>=$dst_scale){
                // 原图比目标图过高
                $w = intval($src_w);
                $h = intval($dst_scale*$w);
            }else{
                // 原图比目标图宽,则使用原图
                $h = intval($src_h);
                $w = intval($h/$dst_scale);
            }
        }


         $imagetype = exif_imagetype($image);
         switch ($imagetype){
             case 1:
                 $source = \imagecreatefromgif($image);
                 break;
             case 2:
                 $source = \imagecreatefromjpeg($image);
                 break;
            case 3:
                 $source = \imagecreatefrompng($image);
                 break;

        }

        /*
        switch (strtolower($aryFile[1])){
            case 'jpg':
            case 'jpeg':
                //                imagejpeg($target, $path.'/'.$aryFile[0].'.'.$quality.'.small.jpg',$quality);
                $source = imagecreatefromjpeg($image);
                break;
            case 'png':
                $source = imagecreatefrompng($image);
                break;
            case 'gif':
                $source = imagecreatefromgif($image);
                break;
        }*/

        //裁剪
        //$croped = imagecreatetruecolor($w, $h);
        //imagecopy($croped, $source, 0, 0, $src_x, $src_y, $src_w, $src_h);

        // 缩放
        $scale = $dst_w/$w;
        $target = imagecreatetruecolor($dst_w, $dst_h);
        $final_w = intval($w*$scale);
        $final_h = intval($h*$scale);
        $color=imagecolorallocate($target,255,255,255);
        imagecolortransparent($target,$color);
        imagefill($target,0,0,$color);
        $w -= $x;
        $h -= $y;
        
        imagecopyresampled($target,$source,0,0,$x,$y,$final_w,$final_h,$w,$h);
        // 保存
        switch (strtolower($aryFile[1])){
            case 'jpg':
            case 'jpeg':
//                imagejpeg($target, $path.'/'.$aryFile[0].'.'.$quality.'.small.jpg',$quality);
                imagejpeg($target, $path.'/'.$aryFile[0].".".$sSuffix.'.jpg',$quality);
                break;
            case 'png':
                imagepng($target, $path.'/'.$aryFile[0].".".$sSuffix.'.png');
                break;
            case 'gif':
                imagegif($target, $path.'/'.$aryFile[0].".".$sSuffix.'.gif');
                break;
        }
    }

    static function _frmColumns($aColumns, $returnType, $sSplitChar='__split__'){
        $aReturn = array();
        foreach($aColumns as $k=>$row){
            if(is_string($row))
                $aReturn[$k] = $row;
            else{
                foreach($row as $sOp=>$aTmp){
                    switch(strtolower(trim($sOp))){
                        case '$concat':
                            switch(strtolower(trim($returnType))){
                                case 'mysql':
                                    $tmpAry = array();
//                                var_dump($aTmp);
                                    foreach($aTmp as $field){
                                        $tmpAry[] = "CAST(IF(".$field."<=>null, 0, ".$field.") AS CHAR)";
                                    }
                                    $aReturn[$k] = new \Zend\Db\Sql\Expression("CONCAT_WS('".$sSplitChar."', ".implode(',',$tmpAry).")");
                                    break;
                                case 'mongodb':
                                    break;
                                default:
                                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2025, null, array("[=replacement1=]"=>$returnType));
                                    break;
                            }
                            break;
                        case '$expression':
                            switch(strtolower(trim($returnType))){
                                case 'mysql':
//                                    var_dump($aTmp);
                                    $aReturn[$k] = new \Zend\Db\Sql\Expression($aTmp);
                                    break;
                                case 'mongodb':
                                    break;
                                default:
                                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2025, null, array("[=replacement1=]"=>$returnType));
                                    break;
                            }
                            break;
                        case '$count':
                            switch(strtolower(trim($returnType))){
                                case 'mysql':
                                    $aReturn[$k] = new \Zend\Db\Sql\Expression("count({$aTmp})");
                                    break;
                                case 'mongodb':
                                    break;
                                default:
                                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2025, null, array("[=replacement1=]"=>$returnType));
                                    break;
                            }
                            break;
                        case '$sum':
                            switch(strtolower(trim($returnType))){
                                case 'mysql':
                                    $aReturn[$k] = new \Zend\Db\Sql\Expression("sum({$aTmp})");
                                    break;
                                case 'mongodb':
                                    break;
                                default:
                                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2025, null, array("[=replacement1=]"=>$returnType));
                                    break;
                            }
                            break;
                        default:
                            \YcheukfCommon\Lib\Functions::throwErrorByCode(2026, null, array("[=replacement1=]"=>$sOp));
                            break;
                    }
                }
            }
        }
        return $aReturn;
    }
    static function myUpdate($oSm, $aParam){
        $oAdapter = $oSm->get('Zend\Db\Adapter\Adapter');
        $aWhere = isset($aParam['params']["where"]) ? $aParam['params']["where"] : array();
        $sTableName = self::getTableByAlias($oSm, $aParam['resource'], 'table');
        foreach($aParam['params']['dataset'] as $row){
            if(count($aWhere)){//修改
                $oUpdate = new \Zend\Db\Sql\Update();
                $oUpdate->table($sTableName);
                if(!is_array($row))
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2044, null, array("[=replacement1=]" => $row));
                $aTmp2 = array();
                foreach($row as $k=>$sAtmp){
                    if(in_array($k, array('$inc'))){
                        $aTmp2[key($sAtmp)] = new \Zend\Db\Sql\Expression(key($sAtmp).'+1');
                    }else{
                        $aTmp2[$k] = $sAtmp;
                    }
                }
                $oUpdate->set($aTmp2);
                $oUpdate->where(self::chg2MySQLWhere($aWhere));

                $stmt = new \Zend\Db\Sql\Sql($oAdapter);
                $resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
                $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($oUpdate)->execute());
                $oSm->get('eventmanager')->trigger('lf_record_update', $oSm, array($aParam['resource'], $row, (isset($aParam['params']["where"]) ? $aParam['params']["where"] : array())),function ($r) {return true;});
                $sId = isset($aParam['params']["where"]['id']) ? $aParam['params']["where"]['id'] : $oResult->count();
            }else{//新增
                $oInsert = new \Zend\Db\Sql\Insert();
                $oInsert->into($sTableName);
                if(!is_array($row))
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2044, null, array("[=replacement1=]" => $row));
                $oInsert->values($row);
                $stmt = new \Zend\Db\Sql\Sql($oAdapter);
//                var_dump($oInsert);
                $resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
                $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($oInsert)->execute());
                $sId = isset($row['id']) ? $row['id'] : $resultSet->getDataSource()->getGeneratedValue();
//                var_dump($sId);
                $oSm->get('eventmanager')->trigger('lf_record_insert', $oSm, array($aParam['resource'], $row, $sId),function ($r) {return true;});
            }
        }
        return $sId;
    }

    /**
     * 将一些source映射到其他source
     * @param string $sSource
     * @return string
     */
    static public function fmtSourceFormat($sSource){
        if(preg_match("/view_item_option/i", $sSource))$sSource = "metadata";
        return $sSource;
    }

    static function sendMsg2UserByMQ($sm, $userId, $sTitle, $sContent){
        \YcheukfCommon\Lib\Functions::sendMQ($sm, array($userId, $sTitle, $sContent), 'sendpersonalmsg');
    }
    static function sendMsg2User($sm, $userId, $sTitle, $sContent){
        $sCurrentTime = date("Y-m-d H:i:s");
        $aDataSet = array(
            'category_id' => 5,
            'status' => 1,
            'touser_id' => $userId,
            'title' => $sTitle,
            'postname' => $sTitle,
            'content' => $sContent,
            'fid' => 0,
            'modified' => $sCurrentTime,
        );

        $aParams = array(
            'op'=> 'save',
            'resource'=> 'post',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
            'params' => array(
                'dataset' => array ( $aDataSet),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        return true;
    }
    static public function fmtMetadataTypeMd5Id($sSource, $nId){
        $sCacheMd5Id = ($sSource."_".\YcheukfCommon\Lib\Functions::chgSelect2CacheKey(array("where" => array("type"=>$nId))));
        return $sCacheMd5Id;
    }
    static function myDelete($oSm, $aParam){
        $aWhere = isset($aParam["params"]["where"]) ? $aParam["params"]["where"] : array();
        $sTableName = self::getTableByAlias($oSm, $aParam['resource'], 'table');
        $select = new \Zend\Db\Sql\Delete;
        $aBandData = null;
        $select->from($sTableName);
        if(isset($aWhere)){
            $select->where(self::chg2MySQLWhere($aWhere));
        }
        $stmt = new \Zend\Db\Sql\Sql($oSm->get('Zend\Db\Adapter\Adapter'));
        $resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
        $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($select)->execute());
        $sId = $oResult->count();
        $oSm->get('eventmanager')->trigger('lf_record_delete', $oSm, array($aParam['resource'], $aWhere, $sId),function ($r) {return true;});
        //self::$getEventManager()->trigger('lf_record_delete', $oSm, array($aParam['resource'], $aWhere, $sId));
    }
    static function mySelect($oSm, $aParam){
        $aSelect = isset($aParam["params"]["select"]) ? $aParam["params"]["select"] : array();
        $sTableName = self::getTableByAlias($oSm, $aParam['resource'], 'table');
        $select = new \Zend\Db\Sql\Select;
        $aBandData = null;
        $select->from($sTableName);
        if(isset($aSelect["columns"]) && $aSelect["columns"]!="*")
            $select->columns(self::_frmColumns($aSelect["columns"],'mysql'), false);
        if(isset($aSelect["group"]))
            $select->group($aSelect["group"]);

        if(isset($aSelect)){
            $select->where(self::chg2MySQLWhere($aSelect));
        }
        $stmt = new \Zend\Db\Sql\Sql($oSm->get('Zend\Db\Adapter\Adapter')->getSlaveAdapter());
        $resultSet = new \Zend\Db\ResultSet\HydratingResultSet();
        $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($select)->execute());
        $aReturn = array("dataset"=>array(), "count"=>$oResult->count());

        if(isset($aParam["params"]["select"]['countonly']) && $aParam["params"]["select"]['countonly']){
        }else{
            if(isset($aSelect["order"])){
                if(is_string($aSelect["order"]) && preg_match("/\(/i", $aSelect["order"]))
                    $select->order(new \Zend\Db\Sql\Expression($aSelect["order"]));
                else
                    $select->order($aSelect["order"]);
            }
            if(isset($aSelect["limit"]))
                $select->limit($aSelect["limit"]);
            if(isset($aSelect["offset"])){
//                $nOffset = isset($aSelect["limit"]) ? ($aSelect["offset"]-1)*$aSelect["limit"] : 0;
                $select->offset($aSelect["offset"]);
            }
            $oResult = $resultSet->initialize($stmt->prepareStatementForSqlObject($select)->execute());
            while($oResult->valid()){
                $aTmp = $oResult->current();
                $oResult->next();
                if($aTmp)
                    $aReturn["dataset"][] = $aTmp->getArrayCopy();
            }
        }
        return $aReturn;

    }

    static function chg2MySqlWhere($aWhere){
        $oWhere = new \Zend\Db\Sql\Where;
        if(count($aWhere)<1)return $oWhere;
        $aCondition = array('$and', '$or', '$not', '$nor', );
        $aConditionMap = array('$and'=>'and', '$or'=>'or', '$not'=>'or', '$nor'=>'or', );
        if(!in_array(key($aWhere), $aCondition)){
            $aWhere = array('$and'=>array($aWhere));
        }
        foreach($aWhere as $k1=>$a1){//第一层 逻辑关系
            $sOperation = $aConditionMap[$k1];
            foreach($a1 as $k2=>$a2){//第二层 多个关系实体
                foreach($a2 as $k3=>$a3){ //第三层, 字段=>值
                    if(is_array($a3)){
                        foreach($a3 as $k4=>$a4){
                            switch(strtolower($k4)){
                                case '$in':
                                    if(count($a4)){
                                        $oWhere->in($k3, $a4);
                                        $oWhere = $oWhere->$sOperation;
                                    }
                                break;
                                case '$gt':
                                    $oWhere->greaterThan($k3, $a4);
                                    $oWhere = $oWhere->$sOperation;
                                break;
                                case '$gte':
                                    $oWhere->greaterThanOrEqualTo($k3, $a4);
                                    $oWhere = $oWhere->$sOperation;
                                break;
                                case '$lt':
                                    $oWhere->lessThan($k3, $a4);
                                    $oWhere = $oWhere->$sOperation;
                                break;
                                case '$lte':
                                    $oWhere->lessThanOrEqualTo($k3, $a4);
                                    $oWhere = $oWhere->$sOperation;
                                break;
                                case '$regex':
                                    $oWhere->like($k3, '%'.$a4.'%');
                                    $oWhere = $oWhere->$sOperation;
                                break;
                                case '$ne':
                                    $oWhere->notEqualTo($k3, $a4);
                                    $oWhere = $oWhere->$sOperation;
                                break;
                                default:
                                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2031, null, array("[=replacement1=]" => $k4));
                                break;
                            }
                        }
                    }else//直接等于
                        $oWhere->equalTo($k3, $a3);
                        $oWhere = $oWhere->$sOperation;
                }
            }
        }
//                        $oWhere->or->equalTo('a', 'a');
//                        $oWhere->or->equalTo('b', 'b');
        return $oWhere;
    }
    static function getFieldBySource($sField, $sSource=null){
        $sReturn = $sField;
        return $sReturn;
    }
    static function getTableByAlias($sm, $sSourceName, $key=null){
        $aConfig = $sm->get('config');
        if(!isset($aConfig["apiservice"]['resources']) || count($aConfig["apiservice"]['resources'])<1)
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2023);

        $aApiResources = array();
        if (isset($aConfig["apiservice"]['resources'][$sSourceName])) {
            foreach($aConfig["apiservice"]['resources'] as $k=>$row){
                if(is_array($row))
                    $aApiResources[$k] = $row;
                elseif(is_string($row)){
                    $aApiResources[$k] = array(
                        'table' => $row,
                        'driver' => 'mysql',
                    );
                }
            }
        }else{
            $aApiResources[$sSourceName] = array(
                'table' => $sSourceName,
                'driver' => 'mysql',
            );
            
        }


        return isset($key) ? $aApiResources[$sSourceName][$key] : $aApiResources[$sSourceName];
    }

    static function getFormBySource($sm, $sSource=null){
        $aConfig = $sm->get('config');
        $sClassNameTmp = self::toCamelCase(strtolower($sSource));
        $sModuleClass = '\\'.LAF_NAMESPACE.'\Form\\'.$sClassNameTmp;
        if(class_exists($sModuleClass))return $sModuleClass;
        $sClass = '\Application\Form\\'.$sClassNameTmp;
        if(class_exists($sClass))return $sClass;
        $sDir = BASE_INDEX_PATH."/../module/".LAF_NAMESPACE."/src/".LAF_NAMESPACE."/Form";
        self::generateFileByTpl('TableForm.tpl', $sDir, LAF_NAMESPACE, $sClassNameTmp);
        return $sModuleClass;
    }
    static function getToolbarBySource($sm, $sSource=null){
        $aConfig = $sm->get('config');
        $sClassNameTmp = self::toCamelCase(strtolower($sSource));
        $sModuleClass = '\\'.LAF_NAMESPACE.'\Toolbar\\'.$sClassNameTmp;
        if(class_exists($sModuleClass))return $sModuleClass;
//        $sClass = '\Application\Toolbar\\'.ucfirst(strtolower($sSource));
        $sClass = '\Application\Toolbar\\'.$sClassNameTmp;
        if(class_exists($sClass))return $sClass;
        $sDir = BASE_INDEX_PATH."/../module/".LAF_NAMESPACE."/src/".LAF_NAMESPACE."/Toolbar";
        self::generateFileByTpl('TableToolbar.tpl', $sDir, LAF_NAMESPACE, $sClassNameTmp);
        return $sModuleClass;
//        \YcheukfCommon\Lib\Functions::throwErrorByCode(2077, null, array("[=replacement1=]" => $sModuleClass));
    }
    static function generateModuleService($sm, $sClassName){
//        echo BASE_INDEX_PATH;
        echo $sClassName;
        $aConfig = $sm->get('config');
        $sClassNameTmp = $sClassName;
        $sModuleClass = '\\'.LAF_NAMESPACE.'\Service\\'.$sClassNameTmp;

        if(class_exists($sModuleClass))return $sModuleClass;
        $sDir = BASE_INDEX_PATH."/../module/".LAF_NAMESPACE."/src/".LAF_NAMESPACE."/Service";
        self::generateFileByTpl("Service".$sClassName.".tpl", $sDir, LAF_NAMESPACE, $sClassNameTmp);

        return $sModuleClass;

    }
    static function generateFileByTpl($sTplFile, $sDir, $sNameSpace, $sClassNameTmp){
        $sFileTmp = $sDir."/".$sClassNameTmp.".php";
        if (!file_exists($sFileTmp)) {
            system("mkdir -p ".$sDir);
            $sContents = file_get_contents(BASE_INDEX_PATH."/../config/tpl/".$sTplFile);
            $sContents = str_replace("#NAMESPACE#", $sNameSpace, $sContents);
            $sContents = str_replace("#CLASSNAME#", $sClassNameTmp, $sContents);
            $fw = fopen($sFileTmp, "w");
            fwrite($fw, $sContents);
            fclose($fw);
            system("chmod -c 777 ".$sFileTmp);
        }


    }

    /**
     * 初始化报表目录
     */
    static function initReportDictionary($sm){
        $aConfig = $sm->get('config');

        $sClass1 = '\\'.LAF_NAMESPACE.'\YcheukfReportExt\\Report\\Dictionary\\Id2label';
        $sClass2 = '\\'.LAF_NAMESPACE.'\YcheukfReportExt\\Report\\Dictionary\\Metric';
        if(class_exists($sClass1) && class_exists($sClass2))return true;

        $sDir = BASE_INDEX_PATH."/../module/".LAF_NAMESPACE."/src/".LAF_NAMESPACE."/YcheukfReportExt/Report/Dictionary";
        self::generateFileByTpl("TableDictionaryId2label.tpl", $sDir, LAF_NAMESPACE, 'Id2label');
        self::generateFileByTpl("TableDictionaryMetric.tpl", $sDir, LAF_NAMESPACE, 'Metric');
        \YcheukfReport\Lib\ALYS\ALYSFunction::setClassMapper('Id2label', $sClass1);
        \YcheukfReport\Lib\ALYS\ALYSFunction::setClassMapper('Metric', $sClass2);
    }
    static function getInputBySource($sm, $sSource=null){
        $aConfig = $sm->get('config');
        $sClassNameTmp = self::toCamelCase(strtolower($sSource));
        $sModuleClass = '\\'.LAF_NAMESPACE.'\YcheukfReportExt\Inputs\\'.$sClassNameTmp;

        if(class_exists($sModuleClass))return $sModuleClass;
        $sClass = '\Application\YcheukfReportExt\Inputs\\'.$sClassNameTmp;
        if(class_exists($sClass))return $sClass;
        $sDir = BASE_INDEX_PATH."/../module/".LAF_NAMESPACE."/src/".LAF_NAMESPACE."/YcheukfReportExt/Inputs";
        self::generateFileByTpl("TableInput.tpl", $sDir, LAF_NAMESPACE, $sClassNameTmp);
        return $sModuleClass;

    }
    static function setDbproxyToken($sToken){
        self::$aDbproxyToken[$sToken] = 1;
    }
    static function chkDbproxyToken($sToken){
        return (isset(self::$aDbproxyToken[$sToken])) ? self::$aDbproxyToken[$sToken] : false;
    }

    static function throwErrorByCode($nCode, $sMsg=null, $aReplaceMent=array()){
        if(is_null(self::$aErrorCode))
            self::setErrorCode();
        if(is_null($sMsg) || empty($sMsg))
            $sMsg = isset(self::$aErrorCode[$nCode]) ? self::$aErrorCode[$nCode]: $nCode.":undefined error msg";
        if(count($aReplaceMent)){
            foreach($aReplaceMent as $sKey=>$sValue){
                $sMsg = str_replace($sKey, $sValue, $sMsg);
            }
       }
        $aReturn  = self::fmtApiReturn(null,$nCode, $sMsg);
        throw new \YcheukfCommon\Lib\Exception(is_string($aReturn) ? $aReturn : json_encode($aReturn));
    }
    static function xml2array($contents, $get_attributes = 1, $priority = 'tag')
    {
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);
        if (!$xml_values)
            return; //Hmm...
        $xml_array = array ();
        $parents = array ();
        $opened_tags = array ();
        $arr = array ();
        $current = & $xml_array;
        $repeated_tag_index = array ();
        foreach ($xml_values as $data)
        {
            unset ($attributes, $value);
            extract($data);
            $result = array ();
            $attributes_data = array ();
            if (isset ($value))
            {
                if ($priority == 'tag')
                    $result = $value;
                else
                    $result['value'] = $value;
            }
            if (isset ($attributes) and $get_attributes)
            {
                foreach ($attributes as $attr => $val)
                {
                    if ($priority == 'tag')
                        $attributes_data[$attr] = $val;
                    else
                        $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
                }
            }
            if ($type == "open")
            {
                $parent[$level -1] = & $current;
                if (!is_array($current) or (!in_array($tag, array_keys($current))))
                {
                    $current[$tag] = $result;
                    if ($attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    $current = & $current[$tag];
                }
                else
                {
                    if (isset ($current[$tag][0]))
                    {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level]++;
                    }
                    else
                    {
                        $current[$tag] = array (
                            $current[$tag],
                            $result
                        );
                        $repeated_tag_index[$tag . '_' . $level] = 2;
                        if (isset ($current[$tag . '_attr']))
                        {
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset ($current[$tag . '_attr']);
                        }
                    }
                    $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                    $current = & $current[$tag][$last_item_index];
                }
            }
            elseif ($type == "complete")
            {
                if (!isset ($current[$tag]))
                {
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                }
                else
                {
                    if (isset ($current[$tag][0]) and is_array($current[$tag]))
                    {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        if ($priority == 'tag' and $get_attributes and $attributes_data)
                        {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag . '_' . $level]++;
                    }
                    else
                    {
                        $current[$tag] = array (
                            $current[$tag],
                            $result
                        );
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        if ($priority == 'tag' and $get_attributes)
                        {
                            if (isset ($current[$tag . '_attr']))
                            {
                                $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                unset ($current[$tag . '_attr']);
                            }
                            if ($attributes_data)
                            {
                                $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
                    }
                }
            }
            elseif ($type == 'close')
            {
                $current = & $parent[$level -1];
            }
        }
        return ($xml_array);
    }

    //保存用户的自定义日志
    static function saveCustomerlog($sm, $memo="", $m159_id=1, $user_id=0){
        $aParams = array(
            'op'=> 'save',
            'resource'=> 'customerlog',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
            'params' => array('dataset'=>array(array(
                'user_id'=> $user_id,
                'memo'=> $memo,
                'm159_id'=> $m159_id,
            ))),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
    }

    static function saveUserLog($sm){
        //记录用户操作事件
        // var_dump($sm->get('request')->getUri()->getPath());
        // var_dump($sm->has('zfcuser_auth_service'));
        // exit;
        if($sm->has('zfcuser_auth_service') && $sm->has('ZfcRbac\Service\Rbac')){
            $bLogined = $sm->get('zfcuser_auth_service')->hasIdentity();
            $oRbacService = $sm->get('ZfcRbac\Service\Rbac');
            if($bLogined){
                $oRbacService->getIdentity()->getRoles();
                $sRole = implode(',', $oRbacService->getIdentity()->getRoles());
                $sPath = str_replace($sm->get('request')->getBaseUrl(), "", $sm->get('request')->getUri()->getPath());
                $aIgnorePaths = array(
                    'admin/ajaxlist',
                    'admin/list',
                    'imgupload',
                    'admin/dashboard',
                );
                $bSaveLog = true;
                foreach($aIgnorePaths as $sTmp1){
                      if(preg_match("/.*".str_replace("/", "\/", $sTmp1).".*/i", $sPath)){
                          $bSaveLog = false;
                          break;
                      }
                }
                if($bSaveLog){
                    $sParamString = $sm->get('request')->getPost()->toString();
                    if(!empty($sParamString)){
                        $aParams = array(
                            'op'=> 'save',
                            'resource'=> 'userlog',
                            'resource_type'=> 'common',
                            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
                            'params' => array('dataset'=>array(array(
                                'user_id'=> $sm->get('zfcuser_auth_service')->getIdentity()->getId(),
                                'user_role'=> $sRole,
                                'serverinfo'=> json_encode($_SERVER),
                                'url'=> $sPath,
                                'param'=> urldecode($sParamString),
                            ))),
                        );
                        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
                    }
                }
            }
        }
    }
    public function getCsvData($sFile)
    {
        if (!file_exists($sFile)) {
            throw new \YcheukfCommon\Lib\Exception('file not exists:'.$sFile);
        }

        $sEncoding = \YcheukfCommon\Lib\Functions::getFileEncoding($sFile);
        $handle = fopen($sFile,"r");
        $aReturn = array();
        if ($handle) {
            while (!feof($handle)) {
                $aData = fgetcsv($handle);
                if (!is_array($aData) || count($aData) < 1) {
                    continue;
                }
                if (strtoupper($sEncoding) !== 'UTF-8') {
                    foreach ($aData as $key => $value) {
                        $aData[$key] = iconv($sEncoding, "UTF8", $value);
                    }
                }
                $aReturn[] = $aData;
            }
            fclose($handle);
        }
        return $aReturn;
    }

    static function readCsvFileByUtf8($sFile){

       $data= file_get_contents($sFile);
      if( !empty($data) ){    
        $fileType = mb_detect_encoding($data , array('UTF-8','GB2312', 'GBK','LATIN1','BIG5')) ;   
        if( $fileType != 'UTF-8'){   
          $data = mb_convert_encoding($data ,'utf-8' , $fileType);   
        }   
      }   
      return $data;    

    }

    static function iconv2SpecialCharset($sMessage, $sOutCharset){

        $sEncode = mb_detect_encoding($sMessage , array('UTF-8','GB2312', 'GBK','LATIN1','BIG5'));
        if ($sEncode != $sOutCharset) {
            $sMessage = iconv($sEncode, $sOutCharset, $sMessage);
        }
        return $sMessage;
    }

    static function iconv2Utf8($sMessage){

        $sEncode = mb_detect_encoding($sMessage , array('UTF-8','GB2312', 'GBK','LATIN1','BIG5'));
        if ($sEncode != 'UTF-8') {
            $sMessage = iconv($sEncode, "UTF-8", $sMessage);
        }
        return $sMessage;
    }

    static function getFileEncoding($sFile){

       $data= file_get_contents($sFile);
       $fileType = false;
      if( !empty($data) ){    
        $fileType = mb_detect_encoding($data , array('UTF-8','GB2312', 'GBK','LATIN1','BIG5')) ;   
      }   
      return $fileType;    

    }
    static function getBaseUrl($sm){
        // var_dump($_SERVER['HTTP_HOST']);
        return 'http://'.$_SERVER['HTTP_HOST'];

        $aConfig = $sm->get('config');
//        if($sm->has('zendviewrendererphprenderer')){
//            $sBasePath = ($sm->get('zendviewrendererphprenderer')->basePath());
//            return ($sm->get('zendviewrendererphprenderer')->serverUrl($sBasePath));
//        }else{
            return $aConfig['webhost_path'];
//        }
    }
    static function getImgBaseUrl($sm){
        return self::getBaseUrl($sm);
    }
    static function getImgUploadBaseUrl($sm){
        $aConfig = $sm->get('config');
        return empty($aConfig['img_upload_baseurl']) ? \YcheukfCommon\Lib\Functions::getBaseUrl($sm) : $aConfig['img_upload_baseurl'];
    }
    static function setErrorCode($a=array()){
//        $aModule = require(__DIR__ . '/../../../config/errorcode.config.php');
        $aGlobalOrgin = require(__DIR__ . '/../../../../../../config/autoload/errorcode.config.php');
//        var_dump($aGlobalOrgin);
//        var_dump($aModule + $aGlobalOrgin['errorcode']);
        self::$aErrorCode = $a + $aGlobalOrgin['errorcode'];
    }
    static function debug($data, $memo='None', $aCustomParam=array('datatag'=>'xmp'),$method="a", $cacheFile=null){
        if (PHP_SAPI === 'cli')return;
        if(class_exists('\YcheukfDebug\Model\Debug'))
            \YcheukfDebug\Model\Debug::dump($data, $memo, $aCustomParam, $method, $cacheFile);
    }
    static function fmtApiReturn($data=null,$code=1, $smg="OK"){
        $aReturn = array(
            "status" => $code,
            "msg" => $smg,
            "data" => $data,
        );
//        \YcheukfCommon\Lib\Functions::debug($aReturn, "[inline]---[api]---return");
        return json_encode($aReturn);
    }


    static function getParamFromDSN($sDsn){
        preg_match_all("/dbname=([^;]*)/i", $sDsn, $aMatch);
        $dbname = isset($aMatch[1][0]) ? $aMatch[1][0] : "";
        preg_match_all("/host=([^;]*)/i", $sDsn, $aMatch);
        $host = isset($aMatch[1][0]) ? $aMatch[1][0] : "127.0.0.1";
        preg_match_all("/port=([^;]*)/i", $sDsn, $aMatch);
        $port = isset($aMatch[1][0]) ? $aMatch[1][0] : 3306;
        return array(
            'dbname' => $dbname,
            'host' => $host,
            'port' => $port,
        );
    }
    static function getFileCacheObject(){
    }
    static function getCacheObject($sm, $type=null){
        $aConfig = $sm->get('config');
        $type = !is_null($type) ? $type : $aConfig['cache_config']['type'];
        switch($type){
            case 'redis':
                try{
                    if(!isset(self::$aCacheObject[$type])){

                        $oReturn = new \YcheukfCommon\Lib\RedisMq();

                        $oReturn->connect($aConfig['cache_config']['params']['servers'][0][0], $aConfig['cache_config']['params']['servers'][0][1]);
                        $oReturn->select($aConfig['cache_config']['params']['servers'][0][2]);
                        self::$aCacheObject[$type] = $oReturn;
                    }
                    $oReturn = self::$aCacheObject[$type];

                }catch(\RedisException $e){
                    echo "".$e->getMessage()."\n";
                    return false;
                }
            break;
            case 'filesystem':
                $oReturn   = \Zend\Cache\StorageFactory::factory(array(
                    'adapter' => array(
                        'name' => 'filesystem',
                        'options' => array(
                            'cache_dir' => BASE_INDEX_PATH.'/../data/cache',
                            'dir_permission' => 0777,
                            'file_permission' => 0644,
                        ),
                    ),
                    'plugins' => array(
                        // Don't throw exceptions on cache errors
                        'exception_handler' => array(
                            'throw_exceptions' => false
                        ),
                    )
                ));
            break;
            case 'memcache':
            default:
                if(!isset(self::$aCacheObject[$type])){
                    $memcache = new \Memcache;
//var_dump($aConfig['cache_config']['params']['servers']);
                    foreach($aConfig['cache_config']['params']['servers'] as $aRow){
                        $bFlag = $memcache->addServer($aRow[0], $aRow[1], $aRow[2]);
                    }
                    $oReturn = new \Doctrine\Common\Cache\MemcacheCache();
                    $aTmp = array();
                    $oReturn->setMemcache($memcache);
                    $aTmp[$type] = $oReturn;
                    self::$aCacheObject = $aTmp;
                }else{
                    $oReturn = self::$aCacheObject[$type];
                }
            break;
        }

        return $oReturn;
    }
    static function getMongoDm($sm){

        if(is_null(self::$dm)){
            $aConfig = $sm->get("config");

            $sMongodbKey = "db_mongodb";
            if(!isset($aConfig[$sMongodbKey]))
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2022, null, array("[=replacement1=]" => $sMongodbKey));
            $aTmp = $aConfig[$sMongodbKey];
            unset($aTmp['username']);
            unset($aTmp['password']);
            \YcheukfCommon\Lib\Functions::debug($aTmp, "[inline]---[mongodb]---load");

            \Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver::registerAnnotationClasses();

            $config = new \Doctrine\ODM\MongoDB\Configuration();
            $config->setProxyDir(BASE_INDEX_PATH."/../module/Doctrinedir");
            $config->setProxyNamespace('Proxies');
            $config->setHydratorDir(BASE_INDEX_PATH."/../module/Doctrinedir");
            $config->setHydratorNamespace('Hydrators');
            $config->setDefaultDB($aConfig[$sMongodbKey]["dbname"]);
            $config->setAutoGenerateHydratorClasses(false);
            $config->setMetadataDriverImpl(\Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver::create($aConfig[$sMongodbKey]["zmcbase_document_dir"]));

            $sDsn =     "mongodb://".$aConfig[$sMongodbKey]["username"].":".$aConfig[$sMongodbKey]["password"]."@".$aConfig[$sMongodbKey]["host"].":".$aConfig[$sMongodbKey]["port"]."/".$aConfig[$sMongodbKey]["dbname"];
            $evm = new \Doctrine\Common\EventManager();
            $evm->addEventSubscriber(new \YcheukfCommon\Lib\OdmEventSubscribe());
            $oMongoObject = class_exists('MongoClient') ? new \MongoClient($sDsn) : new \Mongo($sDsn);
            $oConnection = new \Doctrine\MongoDB\Connection($oMongoObject, array(), null, $evm);
            $dm = \Doctrine\ODM\MongoDB\DocumentManager::create($oConnection, $config);
            self::$dm = $dm;
        }
        return self::$dm;
    }

    static function chgArrayValue2String($a){
        if(!is_array($a) || count($a)<1)return @(string)$a;
        foreach($a as $k=>$v){
            $a[$k] = self::chgArrayValue2String($v);
        }
        return $a;
    }
    static function chgSelect2CacheKey($aCondition){
        $aReturn = array();
        $aReturn['columns'] = (isset($aCondition['columns'])) ? json_encode($aCondition['columns']) : "*";
        $aReturn['offset'] = (isset($aCondition['offset'])) ? json_encode($aCondition['offset']) : null;
        $aReturn['limit'] = (isset($aCondition['limit'])) ? json_encode($aCondition['limit']) : null;
        $aReturn['order'] = (isset($aCondition['order'])) ? json_encode($aCondition['order']) : null;
        $aReturn['group'] = (isset($aCondition['group'])) ? json_encode($aCondition['group']) : null;
        $aReturn['where'] = (isset($aCondition['where'])) ? self::chgArrayValue2String($aCondition['where']) : null;
        return md5(json_encode($aReturn));
    }

    static function curl_core_func($sUrl, $sProxyIp="",  $nCurlTimeout=3){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sUrl);
        if(!empty($sProxyIp))
            curl_setopt($ch, CURLOPT_PROXY, $sProxyIp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $nCurlTimeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $nCurlTimeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
        $sContent = curl_exec($ch);
        return $sContent;
    }
    static function callApiByCurl($sUrl, $aParams, $bPost=true){
        $sParam="";
        if(count($aParams)){
            foreach ($aParams as $k=>$v)
            {
                $sTmp = (is_array($v)?json_encode($v) : $v);
                $sParam .= "$k=".urlencode($sTmp)."&";
            }
            $sParam = substr($sParam,0,-1);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, $bPost);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sParam);
        curl_setopt($ch, CURLOPT_URL, $sUrl);
        $result =curl_exec($ch);
        curl_close($ch);
        $aJson = json_decode($result, 1);
        if(is_null($aJson)){
            \YcheukfCommon\Lib\Functions::debug(array($sUrl, $aParams, $result), "[inline]---[callApiByCurl]---error");
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2052, null, array("[=replacement1=]" => $result));
        }
        return $aJson;
    }

    /**
    * Translates a camel case string into a string with underscores (e.g. firstName -&gt; first_name)
    * @param     string $str String in camel case format
    * @return     string $str Translated into underscore format
    */
    static function fromCamelCase($str) {
        $str[0] = strtolower($str[0]);
        return preg_replace_callback('/([A-Z])/', function($c){
            return "_" . strtolower($c[1]);
        }, $str);
    }

    /**
    * Translates a string with underscores into camel case (e.g. first_name -&gt; firstName)
    * @param    string     $str String in underscore format
    * @param    bool     $capitalise_first_char If true, capitalise the first char in $str
    * @return   string     $str translated into camel caps
    */
    static function toCamelCase($str, $capitalise_first_char = true) {
        if($capitalise_first_char) {
            $str[0] = strtoupper($str[0]);
        }
        return preg_replace_callback('/_([a-z0-9])/', function($c){return strtoupper($c[1]);}, $str);
    }

    //弃用.本函数已经被其它函数代替
    static function chgSqlCondition($aCondition, $sReturnType='mysql'){
        $aReturn = array();
        $aMaps = array(
            'logical' => array('$and', '$or', '$not', '$nor', ),
            'comparesion_map' => array(
                '$ne'=>'!=',
                '$regex'=>'like',
                '$option'=>'',
                '$gt'=>'>',
                '$gte'=>'>=',
                '$lt'=>'<',
                '$lte'=>'<=',
                '$in'=>'in',
                '$nin'=>'not in',
            ),
        );
        $sKey = key($aCondition);
        if(!in_array($sKey, $aMaps['logical']))
            $aCondition = array('$and'=>$aCondition);

        //check condition
        foreach($aCondition as $sLogical => $aTmp1){
            if(!in_array($sLogical, $aMaps['logical'])){
                \YcheukfCommon\Lib\Functions::throwErrorByCode(2018);
            }
            foreach($aTmp1 as $sField => $mTmp2){
                if(is_array($mTmp2)){
                    foreach($mTmp2 as $sKey => $aTmp4){
                        if(!in_array($sKey, array_keys($aMaps['comparesion_map']))){
                            \YcheukfCommon\Lib\Functions::throwErrorByCode(2019);
                        }
                    }
                }
            }
        }
        switch(strtolower($sReturnType)){
            case 'mysql':
                $aMysql1 = $aMysql2 = $aMysql3 = array();
                foreach($aCondition as $sLogical => $aTmp1){
                    foreach($aTmp1 as $sField => $mTmp2){
                        if(is_string($mTmp2) || is_int($mTmp2)|| is_float($mTmp2)){
                            $sWhere = ":".md5($sField);
                            $aMysql1[$sLogical][] = $sField."= ".$sWhere;
                            $aMysql2[$sWhere] = $mTmp2;
                        }elseif(is_array($mTmp2)){
                            foreach($mTmp2 as $sKey => $aTmp4){
                                switch(strtolower($sKey)){
                                    case '$in':
                                    case '$nin':
                                        $aTmp3 = array();
                                        if(!is_array($aTmp3)){
                                            \YcheukfCommon\Lib\Functions::throwErrorByCode(2020);
                                        }
                                        foreach($aTmp4 as $i=>$vTmp){
                                            $sWhere = ":".md5($sField.$sKey.$i);
                                            $aMysql2[$sWhere] = $vTmp;
                                            $aTmp3[] = $sWhere;
                                        }
                                        $aMysql1[$sLogical][] = $sField." ".$aMaps['comparesion_map'][$sKey]." (".join(",", $aTmp3).")";
                                        break;
                                    default:
                                        $sWhere = ":".md5($sField.$sKey);
                                        $aMysql1[$sLogical][] = $sField." ".$aMaps['comparesion_map'][$sKey]." ".$sWhere;
                                        $aMysql2[$sWhere] = $aTmp4;
                                        break;
                                }
                            }
                        }else{
                            \YcheukfCommon\Lib\Functions::throwErrorByCode(2027);
                        }
                    }
                }
                foreach($aMysql1 as $sLogical=>$sTmp){
                    switch(strtolower($sLogical)){
                        case '$and':
                            $aMysql3[] = "(". join(" and ", $sTmp).")";
                            break;
                        case '$or':
                            $aMysql3[] = "(". join(" or ", $sTmp).")";
                            break;
                        case '$not':
                        case '$nor':
                            $aMysql3[] = "!(".join(" and ", $sTmp).")";
                            break;
                    }
                }
                $aReturn = array(join(" and ", $aMysql3), $aMysql2);
                break;
            default:
            case 'mongo':
                $aReturn = $aCondition;
                break;
        }
        return $aReturn;
    }
    static function downloadHTML($url){
        $aUrlInfo = (parse_url($url));
        $sBaseUrl = (isset($aUrlInfo['scheme'])?$aUrlInfo['scheme']:"http")."://".$aUrlInfo['host'].(isset($aUrlInfo['port'])?":".$aUrlInfo['port']:"");
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true) ; // 获取数据返回
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true) ; // 在启用 CURLOPT_RETURNTRANSFER 时候将获取数据返回
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        $sContent =curl_exec($ch);
        //preg_match_all('/<script>.*<\/script>/si', $sContent, $aMatches);
//        var_dump($url);
//exit;
        //将icon文件转成base64
        preg_match_all('/<link.*href="([^\"]+)".* rel="shortcut icon".*>/i', $sContent, $aMatches);
        if(count($aMatches[0])){
            foreach($aMatches[1] as $i=>$sUrl){
                $sUrl = preg_match("/http/i", $sUrl) ? $sUrl : $sBaseUrl.$sUrl;
                $sContentTmp = @file_get_contents($sUrl);
                $aImgInfo = @getimagesize($sUrl);
                $sMime = isset($aImgInfo['mime']) ? $aImgInfo['mime'] : "image/jpeg";
                $sContent = str_replace($aMatches[1][$i], "data:".$sMime.";base64,".base64_encode($sContentTmp), $sContent);
            }
        }

        //将css文件转成style
        preg_match_all('/<link.*href="([^\"]+)".*rel="stylesheet.*".*>/i', $sContent, $aMatches);
        if(count($aMatches[0])){
            foreach($aMatches[1] as $i=>$sUrl){
                $sUrl = preg_match("/http/i", $sUrl) ? $sUrl : $sBaseUrl.$sUrl;
                $sContentTmp = @file_get_contents($sUrl);

                $sContent = str_replace($aMatches[0][$i], "\n<style>\n".$sContentTmp."\n</style>\n", $sContent);
                //将css中的图片缓存链接
                preg_match_all('/url\(([^\)]+)/i', $sContent, $aMatches2);
                $aPathinfo = (pathinfo($sUrl));
                if(count($aMatches2[0])){
                    foreach($aMatches2[1] as $ii=>$sUrl2){
                        if(preg_match('/data:image/i', $sUrl2))continue;
                        $sUrl3 = str_replace("'", "", $sUrl2);
                        $sUrl3 = str_replace("\"", "", $sUrl3);
                        $sUrl3 = preg_match('/http/i', $sUrl3) ? $sUrl3 : $aPathinfo['dirname']."/".$sUrl3;
//                        var_dump("@".$sUrl2);
//                        var_dump("#".$sUrl3);
                        $sContentTmp = @file_get_contents($sUrl3);
                        $aImgInfo = @getimagesize($sUrl3);
                        $sMime = isset($aImgInfo['mime']) ? $aImgInfo['mime'] : "image/jpeg";
                        $sContent = str_replace($sUrl2, "data:".$sMime.";base64,".base64_encode($sContentTmp), $sContent);
         
//                        $sContent = str_replace($sUrl2, "'".$sUrl3."'", $sContent);
                    }
                }
            }
        }

        //将js文件转成script
        preg_match_all('/<script.* src="([^\"]+)".*>/i', $sContent, $aMatches);
        if(count($aMatches[0])){
            foreach($aMatches[1] as $i=>$sUrl){
                $sUrl = preg_match("/http/i", $sUrl) ? $sUrl : $sBaseUrl.$sUrl;
                $sContentTmp = @file_get_contents($sUrl);

                $sContent = str_replace($aMatches[0][$i], "\n<script>\n".$sContentTmp."\n</script>\n", $sContent);
            }
        }

        //preg_match_all('/<script>.*<\/script>/smi', $sContent, $aMatches);
        ////var_dump($aMatches);
        //if(count($aMatches[0])){
        //    foreach($aMatches[0] as $i=>$sTmp){
        //        $sContent = str_replace($aMatches[0][$i], "", $sContent);
        //    }
        //}

        //将图片转成base64
        preg_match_all('/<img.*src="([^\"]+)".*>/i', $sContent, $aMatches);
        if(count($aMatches[0])){
            foreach($aMatches[1] as $i=>$sUrl){
//        var_dump($sUrl);
                $sUrl = preg_match("/http/i", $sUrl) ? $sUrl : $sBaseUrl.$sUrl;
//        var_dump($sUrl);
                $sContentTmp = @file_get_contents($sUrl);
                $aImgInfo = @getimagesize($sUrl);
                $sMime = isset($aImgInfo['mime']) ? $aImgInfo['mime'] : "image/jpeg";
                $sContent = str_replace($aMatches[1][$i], "data:".$sMime.";base64,".base64_encode($sContentTmp), $sContent);
            }
        }
//        echo($sContent);
//exit;
        return $sContent;

    }
    static function sendSMS($sm, $sPhone, $sMsg){
        $aConfig = $sm->get('config');
        $sPostUrl = $aConfig['SMSconfig']['posturl'];
        $sPostUrl = str_replace("[==dt==]", $sPhone, $sPostUrl);
        $sPostUrl = str_replace("[==msg==]", urlencode(iconv("UTF-8", "GBK", $sMsg)), $sPostUrl).iconv("UTF-8", "GBK", "【棱耀】");
        file_get_contents($sPostUrl);
        return 1;
    }

    static function sendSMSByAliyun($sm, $phone, $TemplateCode, $aParams=[]){
        $aConfig = $sm->get('config');
        $params = array ();

        // *** 需用户填写部分 ***

        // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
        $accessKeyId = $aConfig['aliyun']['sms_ly']['key'];
        $accessKeySecret = $aConfig['aliyun']['sms_ly']['screct'];

        // fixme 必填: 短信接收号码
        $params["PhoneNumbers"] = $phone;

        // fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
        $params["SignName"] = $aConfig['aliyun']['sms_ly']['SignName'];
        
        // fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
        $params["TemplateCode"] = $TemplateCode;

        // fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
        $params['TemplateParam'] = $aParams;

        // fixme 可选: 设置发送短信流水号
        // $params['OutId'] = "12345";

        // fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
        // $params['SmsUpExtendCode'] = "1234567";

        // var_dump($params);


        // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
        if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
            $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
        }

        try {
            // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
            $helper = new \YcheukfCommon\Lib\Aliyun\SignatureHelper();

            // 此处可能会抛出异常，注意catch
            $content = $helper->request(
                $accessKeyId,
                $accessKeySecret,
                "dysmsapi.aliyuncs.com",
                array_merge($params, array(
                    "RegionId" => "cn-hangzhou",
                    "Action" => "SendSms",
                    "Version" => "2017-05-25",
                ))
                // fixme 选填: 启用https
                // ,true
            );

            // var_dump($content);
            return $content;
            
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }

    /**
    *   下载文件类
    *   @$file 文件路径
    **/
    function download($file,$exportName=null){
        //First, see if the file exists
        if (!is_file($file)) { die("<b>404 File not found!</b>"); }
        //Gather relevent info about file
        $len = filesize($file);
        $filename = basename($file);
        $file_extension = strtolower(substr(strrchr($filename,"."),1));

        if (!is_null($exportName)) {
            $filename = $exportName ;
        }else{
        }

        //This will set the Content-Type to the appropriate setting for the file
        switch( $file_extension ) {
          case "pdf": $ctype="application/pdf"; break;
          case "exe": $ctype="application/octet-stream"; break;
          case "zip": $ctype="application/zip"; break;
          case "doc": $ctype="application/msword"; break;
          case "xls": $ctype="application/vnd.ms-excel"; break;
          case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
          case "gif": $ctype="image/gif"; break;
          case "png": $ctype="image/png"; break;
          case "jpeg":
          case "jpg": $ctype="image/jpg"; break;
          case "mp3": $ctype="audio/mpeg"; break;
          case "wav": $ctype="audio/x-wav"; break;
          case "mpeg":
          case "mpg":
          case "mpe": $ctype="video/mpeg"; break;
          case "mov": $ctype="video/quicktime"; break;
          case "avi": $ctype="video/x-msvideo"; break;

          //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
          case "php":
          case "htm":
          case "html":
          case "txt": //die("<b>Cannot be used for ". $file_extension ." files!</b>"); break;
          default: $ctype="application/force-download";
        }

        ob_end_clean();//清除缓冲区,避免乱码
        //Begin writing headers;
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public"); 
        header("Content-Description: File Transfer");
        
        //Use the switch-generated Content-Type
        header("Content-Type:".$ctype);
        // header('Content-Type:text/html; charset=gb2312');
        
        //文件名转成GBK输出 测试ff12.0 ie8.0 chrome无问题
        $encode = mb_detect_encoding($filename, array('ASCII','UTF-8','GB2312','GBK','BIG5'));
        if('GBK'!==$encode){
            $filename = iconv($encode, "GBK", $filename);
        }
        $header='Content-Disposition: attachment; filename="'.$filename;
        header($header);
        header("Content-Transfer-Encoding:binary");
        header("Content-Length: ".$len);
        @readfile($file);
        exit;
    }

    //send message queue
    static function sendMQ($sm, $sMessage, $sQueue, $type="ycfmq"){
        $aConfig = $sm->get('config');
        $sQueueName = "/".LAF_NAMESPACE."/queue/".$sQueue;
        switch($type){
            case "ActiveMq":
                $con = $sm->get('ActiveMq');
                $con->send($sQueueName, json_encode($sMessage));
            break;
            case "ycfmq":
                $con = $sm->get('ycfmq');
                $bFlag = $con->zAdd($sQueueName, 0, json_encode($sMessage));;
            break;
        }
        return true;
    }
    static function getMQ($sm, $sQueue, $type="ycfmq"){
        
        $aConfig = $sm->get('config');
        $sQueueName = "/".LAF_NAMESPACE."/queue/".$sQueue;
        switch($type){
            case "ActiveMq":
                $con = $sm->get('ActiveMq');
                $con->subscribe($sQueueName, null, true);
                $msg = $con->readFrame();
        //            var_dump($msg);

                if ( $msg != null) {
                    $con->ack($msg);
                    $con->unsubscribe($sQueueName);
                    return json_decode($msg->body, 1);
                } else {
                    $con->unsubscribe($sQueueName);
                    return false;
                }
            break;
            case "ycfmq":
                $con = $sm->get('ycfmq');

                $msg = $con->zRevPop($sQueueName);
                return $msg ? json_decode($msg, 1) : false;
            break;
        }
    }

    //向ios用户发送push消息
    /**
    *   @param object sm servicemanager
    *@param string deviceToken 用户ios token
    *@param string sMessage 向用户发送的消息
    */
    static function sendIosMsg($sm, $deviceToken, $sMessage){
        $aConfig = $sm->get('Config');

        //ck.pem通关密码
        $pass = $aConfig['ios']['notification']['password'];
        $sound = $aConfig['ios']['notification']['sound'];

        //建设的通知有效载荷（即通知包含的一些信息）
        $body = array();
        $body['aps'] = array(
            'alert' => $sMessage,
            'sound' => $aConfig['ios']['notification']['sound']
        );
        $payload = json_encode($body);

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $aConfig['ios']['notification']['pem']);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $aConfig['ios']['notification']['password']);
        $fp = stream_socket_client($aConfig['ios']['notification']['link'], $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
        if (!$fp) {
            return false;
        }
        // send message
        $msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n",strlen($payload)) . $payload;
        fwrite($fp, $msg);
        fclose($fp);
//        var_dump($payload);
        return true;
    }
    static function sendIosMsgByMQ($sm, $deviceToken, $sMessage){
        \YcheukfCommon\Lib\Functions::sendMQ($sm, array($deviceToken, $sMessage), 'sendiosnotification');
    }
    static function sendEmailByMQ($sm, $aTo, $sSubject, $sBody){
//        var_dump(array($aTo, $sSubject, $sBody));
        \YcheukfCommon\Lib\Functions::sendMQ($sm, array($aTo, $sSubject, $sBody), 'sendemail');
    }
    static function sendEmail($sm, $aMails, $sSubject, $sBody){
        $message = new \Zend\Mail\Message();
        $aConfig = $sm->get('config');

        // if (isset($aConfig['debugFlag']) && $aConfig['debugFlag']==1) {
            // return true;
        // }


        if(isset($aConfig['sEmailTitleHeader']) && !empty($aConfig['sEmailTitleHeader'])){
            $sSubject = $aConfig['sEmailTitleHeader'].$sSubject;
        }

//        var_dump($aConfig['mail_smtp']);
        if(is_string($aMails)){
            $aMails = trim($aMails);
            $message->addTo($aMails);
        }else{
            foreach($aMails as $k=>$aTmp){
                if(in_array($k, array('to', 'cc', 'bcc'), true)){
                    if(count($aTmp)){
                        foreach($aTmp as $sMail2){
                            $sMail = trim($sMail2);
                            switch($k){
                                case "to":
                                    $message->addTo($sMail);
                                    break;
                                case "cc":
                                    $message->addCc($sMail);
                                    break;
                                case "bcc":
                                    $message->addBcc($sMail);
                                    break;
                            }
                        }
                    }
                }else{
                    $sMail = trim($aTmp);
                    $message->addTo($sMail);
                }
            }
        }

        $text = new \Zend\Mime\Part("");
        $text->type = "text/plain";

        $html = new \Zend\Mime\Part($sBody);
        $html->type = "text/plain";
        // $html->type = "text/html";

        $oBody = new \Zend\Mime\Message();
        $oBody->setParts(array($text, $html));

        $message->setEncoding("UTF-8")->addFrom($aConfig['mail_smtp_from'][0], $aConfig['mail_smtp_from'][1]) ->setSubject($sSubject)->setBody($oBody);
        try{
            // Setup SMTP transport using LOGIN authentication
            $transport = new \Zend\Mail\Transport\Smtp();
            $options   = new \Zend\Mail\Transport\SmtpOptions($aConfig['mail_smtp']);
            $transport->setOptions($options);
            $bFlag = $transport->send($message);
        }catch (\Exception $e) {
            echo "send email error:".$e->getMessage()."\n";
            return false;
        }
        return true;
    }

    static function getDataFromApi($sm, $aParams, $type='function'){
        $aParams['resource'] = isset($aParams['resource']) ? $aParams['resource'] : 'user';
        $aParams['op'] = isset($aParams['op']) ? $aParams['op'] : 'get';
//        var_dump($aParams["params"]);
        $aParams["params"] = isset($aParams["params"]) && isset($aParams["params"]) ? $aParams["params"] : array();
        $aParams["params"] = is_string($aParams["params"]) ? json_decode($aParams["params"], 1) : $aParams["params"];
        $aParams["params"]['select'] = isset($aParams["params"]) && isset($aParams["params"]['select']) ? $aParams["params"]['select'] : array();
        $aConfig = $sm->get('Config');

        //只有在读的时候才获取缓存
        $bCacheEnable = false;
        $sStaticCacheMd5Id = $sCacheMd5Id = "";
        if(strtolower($aParams['op']) == 'get'){
            $sStaticCacheMd5Id = $sCacheMd5Id = ("dbapi___".$aParams['resource']."_".\YcheukfCommon\Lib\Functions::chgSelect2CacheKey($aParams["params"]['select']));
        }
        $aCacheAllowKeys = \YcheukfCommon\Lib\Functions::getCacheAllowKeys($sm);
        $bCacheEnable = isset($aConfig['cache_config']['enable']) ? $aConfig['cache_config']['enable'] : true;
        $bCacheEnable = $bCacheEnable && isset($sCacheMd5Id) && isset($aCacheAllowKeys[$sCacheMd5Id]) ? true : false;


        if($bCacheEnable){//被允许的缓存键值
//            $nLiftTime = $aCacheAllowKeys[$sStaticCacheMd5Id][0];
//            $nCacheType = isset($aCacheAllowKeys[$sStaticCacheMd5Id][2]) ? $aCacheAllowKeys[$sStaticCacheMd5Id][2] : 0;
//            $aCacheKeyConfig = array($nLiftTime, "", $nCacheType, json_encode($aParams), null, $sStaticCacheMd5Id);
            if($aReturn = \YcheukfCommon\Lib\Functions::getCache($sm, $sCacheMd5Id)){//找到缓存
                return $aReturn;
            }
        }

         $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
        $oApiController = $sm->get('controllerloader')->get('Application\Controller\Api');
        $aPost = array();
        foreach($aParams as $k=>$v){
            $aPost[$k] = (is_array($v) ? json_encode($v) : $v);
        }
//            $sm->get('request')->setPost($oParameters);
        $sResourceType = isset($aParams['resource_type']) ? $aParams['resource_type'] : 'common';
        
                // \Application\Model\common::onlinedebug('sResourceType', $sResourceType);

        $sActoin = strtolower($sResourceType)."Action";
        $sContent = $oApiController->$sActoin($aPost, true, false);
        $aResult = json_decode($sContent, 1);
        if(!is_array($aResult)){
            throw new \YcheukfCommon\Lib\Exception($sContent);
        }

         $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
        \YcheukfCommon\Lib\Functions::debug(array($type, number_format(($nTime2-$nTime1), 4).'s', $sCacheMd5Id, $aParams,$aResult), "[inline]---[api]---getDataFromApi");
        if($aResult['status'] == 1999){
            $aResult['status'] = 1;
            $aResult['data'] = array();
        }

        if($aResult['status'] != 1){
            \YcheukfCommon\Lib\Functions::throwErrorByCode($aResult['status'], $aResult['msg']);
        }
        if($bCacheEnable){//写入缓存
            \YcheukfCommon\Lib\Functions::saveCache($sm, $sCacheMd5Id, $aResult, $aCacheAllowKeys[$sCacheMd5Id][0]);
        }
        return $aResult;
    }
    static function saveCache($sm, $sKey, $sData, $nTime=3600, $bStaticFlag=false){
        $aConfig = $sm->get('config');
        $sCacheType = $aConfig['cache_config']['type'];
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($sm, $sCacheType);
        \YcheukfCommon\Lib\Functions::debug(array(__CLASS__.'::'.__FUNCTION__, $sKey, $sData), "[inline]---[".$sCacheType."]---save");
        switch($sCacheType){
            case "memcache":
                $oCacheObject->getMemcache()->set($sKey, $sData, false, $nTime);
            break;
            case "redis":
                $b = $oCacheObject->set($sKey, json_encode($sData));
                if (is_null($nTime)) {
                }else{
                    $a = $oCacheObject->setTimeout($sKey, $nTime);
                }
            break;
        }
        if($bStaticFlag)self::$aStaticVars[$sKey] = $sData;

    }
    static function flushCache($sm){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($sm, 'redis');
        $oCacheObject->flushDB();
    }
    static function getCache($sm, $sKey, $bJson=true){

        // echo PHP_SAPI;
        // if (PHP_SAPI === 'cli')return false;

        $mStaticVars = self::getStaticVars($sm, $sKey);
        if($mStaticVars !== false)return $mStaticVars;

        $aConfig = $sm->get('config');
        $sCacheType = $aConfig['cache_config']['type'];
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($sm, $sCacheType);
        if (!is_object($oCacheObject)) {
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2090);
        }

        $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
        switch(strtolower($sCacheType)){
            case 'memcache':
                if($aReturn = $oCacheObject->getMemcache()->get($sKey)){//找到缓存
                    $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
                    \YcheukfCommon\Lib\Functions::debug(array(__CLASS__.'::'.__FUNCTION__, number_format(($nTime2-$nTime1), 4).'s', $sKey, $aReturn), "[inline]---[".$sCacheType."]---read");
                }
            break;
            case 'redis':

                if($aReturn = $oCacheObject->get($sKey)){//找到缓存
                    $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
                    \YcheukfCommon\Lib\Functions::debug(array(__CLASS__.'::'.__FUNCTION__, number_format(($nTime2-$nTime1), 4).'s', $sKey, $aReturn), "[inline]---[".$sCacheType."]---read");
                }else{
                    return false;
                }
                $aReturn = ($bJson) ? json_decode($aReturn, 1) : $aReturn;
            break;
        }
        return $aReturn;
    }

    static function getAllCacheKeys($sm){
        $aConfig = $sm->get('config');
        $sCacheType = $aConfig['cache_config']['type'];
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($sm);
        switch(strtolower($sCacheType)){
            case 'memcache':
                $oMemObject = $oCacheObject->getMemcache();
                $aExtendStats = $oMemObject->getExtendedStats();
                $array = array();
                foreach($aExtendStats as $sHost => $aStats){
                    $items = $oMemObject->getExtendedStats('items');
                    $items=$items[$sHost]['items'];
                    foreach($items as $key=>$values)
                    {   
                        $number=$key;       
                        $str=$oMemObject->getExtendedStats ("cachedump", $number,0);       
                        $line=$str[$sHost];
                        if( is_array($line) && count($line)>0)
                        {                   
                             foreach($line as $key=>$value)
                            {                  
                                 $array[$key] = $value[1];                 
                            }
                        }
                   }
                }
                ksort($array);
                return($array);
            break;
            case 'redis':
                $aReturn = $oCacheObject->keys("*");
                $aReturn = array_flip($aReturn);
                ksort($aReturn);
                return ($aReturn);
            break;
        }

    }

    static function delCache($sm, $mMemKey, $bRegex=false){
        $aConfig = $sm->get('Config');
        $sCacheType = $aConfig['cache_config']['type'];
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($sm);
        switch(strtolower($sCacheType)){
            case 'memcache':
                $oMemObject = $oCacheObject->getMemcache();
                if($bRegex){//正则
                    $aExtendStats = $oMemObject->getExtendedStats();
                    foreach($aExtendStats as $sHost => $aStats){
                        $items = $oMemObject->getExtendedStats('items');
                        $items = $items[$sHost]['items'];
                        foreach($items as $key=>$values)
                        {
                            $str=$oMemObject->getExtendedStats ("cachedump", $key, 0);
                            $line=$str[$sHost];
                            if( is_array($line) && count($line)>0)
                            {
                                 foreach($line as $key=>$value)
                                {
                                    if(is_string($mMemKey)){
                                         if(preg_match("/$mMemKey/i", $key)){
                                            \YcheukfCommon\Lib\Functions::debug(array(__CLASS__.'::'.__FUNCTION__, $mMemKey, $key, $bRegex), "[inline]---[".$sCacheType."]---delete");
                                            $oMemObject->delete($key);
                                         }
                                    }elseif(is_array($mMemKey)){
                                        $bDelete = true;
                                        foreach($mMemKey as $sKey){
                                             if(!preg_match("/$sKey/i", $key)){
                                                 $bDelete = false;
                                                 break;
                                             }
                                        }
                                        if($bDelete){
                                            \YcheukfCommon\Lib\Functions::debug(array(__CLASS__.'::'.__FUNCTION__, $mMemKey, $key, $bRegex), "[inline]---[".$sCacheType."]---delete");
                                            $oMemObject->delete($key);
                                        }
                                    }
                                }
                            }
                       }
                    }
                }else{
                    \YcheukfCommon\Lib\Functions::debug(array(__CLASS__.'::'.__FUNCTION__, $mMemKey, $bRegex), "[inline]---[".$sCacheType."]---delete");
                    if(is_string($mMemKey))$oMemObject->delete($mMemKey);
                }
            break;
            case 'redis':
                if($bRegex){//正则
                    if(is_string($mMemKey)){
                        $sTmpKey = "*{$mMemKey}*";
                    }elseif(is_array($mMemKey)){
                        $sTmpKey = "*".join("*", $mMemKey)."*";
                    }
                    $aTmp = $oCacheObject->keys($sTmpKey);
                }else{
                    $aTmp = array($mMemKey);
                }
                \YcheukfCommon\Lib\Functions::debug(array(__CLASS__.'::'.__FUNCTION__, $mMemKey, $aTmp, $bRegex), "[inline]---[memcache]---delete");
                if(count($aTmp)){
                    foreach($aTmp as $sKeyTmp){
                        $oCacheObject->delete($sKeyTmp);
                    }
                }
            break;
        }
        return true;
    }
    static function getMicrotimeFloat()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    static function getSolrResult($sm, $keyword, $sFl="*", $nLimit=30, $nStart=0, $aSort=array())
    {
        $datas = array();
        $client = $sm->get('solr.solr_default');
//        var_dump($client);
        $query = new \SolrClient\Query\Query();
        $query->setQuery(str_replace("/", "", $keyword));
        $query->setRows($nLimit);
        $query->setStart($nStart);
        if(count($aSort)){
            foreach($aSort as $aTmp)$query->addSort($aTmp);
        }
//        addSort
        $aParams = $query->getPostQueryParams();
        $aParams['fl'] = $sFl;
        $query->unserialize(serialize($aParams));
        $aParams = $query->getPostQueryParams();
        $query = $client->query($query);
        $total = $query->getNumFound();
        $datas = $query->getDocuments();
        \YcheukfCommon\Lib\Functions::debug(array($aParams, $total, $datas), "[inline]---[solr]---get");
        return array($total, $datas);
    }

    static function getStaticVars($sm, $sCacheKeyName){
        if(!in_array($sCacheKeyName, array(20003, 'cachekey.autogeneration')))return false;
        if(!empty(self::$aStaticVars) && isset(self::$aStaticVars[$sCacheKeyName]))
            return self::$aStaticVars[$sCacheKeyName];

        $aConfig = $sm->get('config');
        $sCacheType = $aConfig['cache_config']['type'];
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($sm, $sCacheType);
        switch(strtolower($sCacheType)){
            case 'memcache':
                $aReturn = $oCacheObject->getMemcache()->get($sCacheKeyName);
            break;
            case 'redis':
                $aReturn = $oCacheObject->get($sCacheKeyName);
                $aReturn = json_decode($aReturn, 1);
            break;
        }

        $aReturn = is_null($aReturn)||!$aReturn ? array() : $aReturn;
        self::$aStaticVars[$sCacheKeyName] = $aReturn;
        \YcheukfCommon\Lib\Functions::debug(array($sCacheKeyName, $aReturn), "[inline]---[static]---genStaticVars");

        return $aReturn;
    }

    /**
    * 获取所有被允许的缓存键值.
    */
    static function getCacheAllowKeys($sm){
        $sCacheKeyName = "cachekey.autogeneration";
        if(!empty(self::$aStaticVars) && isset(self::$aStaticVars[$sCacheKeyName]))
            return self::$aStaticVars[$sCacheKeyName];

        $fAformat = function ($a){
            foreach($a as $k=>$row){
                $row[0] = isset($row[0]) ? $row[0] : 86400;
                $row[1] = isset($row[1]) ? $row[1] : "";
                $row[2] = isset($row[2]) ? $row[2] : 0;
                $row[3] = isset($row[3]) ? $row[3] : "";
                $a[$k] = $row;
            }
            return $a;
        };
        $aConfig = $sm->get('Config');
        if(!isset($aConfig['cache_config']['statickeys']))return array();
        $a = $aConfig['cache_config']['statickeys'];
        ksort($a);
        $aAllCacheKeys = $fAformat($a);
//        var_dump($aAllCacheKeys);
        self::$aStaticVars[$sCacheKeyName] = $aAllCacheKeys;
        return $aAllCacheKeys;
    }



    /*********************************************************************
    函数名称:encrypt
    函数作用:加密解密字符串
    使用方法:
    加密     :encrypt('str','E','nowamagic');
    解密     :encrypt('被加密过的字符串','D','nowamagic');
    参数说明:
    $string   :需要加密解密的字符串
    $operation:判断是加密还是解密:E:加密   D:解密
    $key      :加密的钥匙(密匙);
    *********************************************************************/
    static public function encrypt($string,$operation,$key='')
    {
        $key=md5($key);
        $key_length=strlen($key);
        $string=$operation=='D'?base64_decode($string):substr(md5($string.$key),0,8).$string;
        $string_length=strlen($string);
        $rndkey=$box=array();
        $result='';
        for($i=0;$i<=255;$i++)
        {
            $rndkey[$i]=ord($key[$i%$key_length]);
            $box[$i]=$i;
        }
        for($j=$i=0;$i<256;$i++)
        {
            $j=($j+$box[$i]+$rndkey[$i])%256;
            $tmp=$box[$i];
            $box[$i]=$box[$j];
            $box[$j]=$tmp;
        }
        for($a=$j=$i=0;$i<$string_length;$i++)
        {
            $a=($a+1)%256;
            $j=($j+$box[$a])%256;
            $tmp=$box[$a];
            $box[$a]=$box[$j];
            $box[$j]=$tmp;
            $result.=chr(ord($string[$i])^($box[($box[$a]+$box[$j])%256]));
        }
        if($operation=='D')
        {
            if(substr($result,0,8)==substr(md5(substr($result,8).$key),0,8))
            {
                return substr($result,8);
            }
            else
            {
                return'';
            }
        }
        else
        {
            return str_replace('=','',base64_encode($result));
        }
    }

    //获取kv模式的全部数据
    static function getResourceAllDataKV($sm, $sSource, $aWhere=array(), $sValueField='id', $sKeyField='id'){
        $aTmp = self::getResourceAllData($sm, $sSource, $aWhere);
        $aReturn = array();
        if(($aTmp['count'] > 0)){
            foreach($aTmp['dataset'] as $row){
                $aTmp2 = array();
                if(is_array($sValueField)){
                    foreach($sValueField as $row2){
                        $aTmp2[] = $row[$row2];
                    }            
                }else{
                    $aTmp2 = array($row[$sValueField]);
                }
                $aReturn[$row[$sKeyField]] = join('/', $aTmp2);
            }
        }
        return $aReturn;
    }
    //获取kv模式的全部数据
    static function getResourceAllDataKRow($sm, $sSource, $aWhere=array(), $sKeyField='id'){
        $aTmp = self::getResourceAllData($sm, $sSource, $aWhere);
        $aReturn = array();
        if(($aTmp['count'] > 0)){
            foreach($aTmp['dataset'] as $row){
                $aReturn[$row[$sKeyField]] = $row;
            }
        }
        return $aReturn;
    }
    static function getResourceAllData($sm, $sSource, $aWhere=array()){
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
       $aParams = array(
            'op'=> 'get',
            'resource'=> $sSource,
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'select' => array (
                ),
            ),
        );
        if(count($aWhere))
            $aParams['params']['select']['where'] = $aWhere;


        return self::getResourceList2($sm, $aParams);
    }
    static function getResourceList($sm, $aConfig, $aWhere=array(), $mutipleFlag=false, $limit=null){
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
        $sSource = $aConfig['source'];
        $sPk = $aConfig['pkfield'];
        $sLabelFiled = $aConfig['labelfield'];
        $aParams = array(
            'op'=> 'get',
            'resource'=> $sSource,
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'select' => array (
                ),
            ),
        );
        if(!is_null($limit))
            $aParams['params']['select']['limit'] = $limit;
        if(count($aWhere))
            $aParams['params']['select']['where'] = $aWhere;
        $aTmp = self::getResourceList2($sm, $aParams);
        $aReturn = array();
        if($aTmp['count']){
            foreach($aTmp['dataset'] as $row){
                if($mutipleFlag)
                    $aReturn[$row[$sPk]][] = $row[$sLabelFiled];
                else
                    $aReturn[$row[$sPk]] = $row[$sLabelFiled];
            }
        }
        return $aReturn;
    }


    //判断当前的route是否需要权限控制
    public static function _chkPermissionFilter($sm, $sChkRoute=null){
        $aConfig = $sm->get('Config');
        $aIgnoreRoute = $aConfig['ignore_route'];
        foreach($aIgnoreRoute as $sRoute){
            if(preg_match("/^".str_replace("/", "\/", $sRoute).".*/i", $sChkRoute))
                return false;
        }
        return true;
    }

    //通过route取得当前访问的route 权限
    static function getPermissionByRoute($match){
        if(is_null($match))return false;
//        return $match->getMatchedRouteName();
//        $match      = $oApp->getMvcEvent()->getRouteMatch();
        $aMatchParams = $match->getParams();
//        var_dump($aMatchParams);
//        var_dump($sm->get('httprouter')->getRoutePluginManager());
        $controller = $aMatchParams['controller'];
        $action     = $aMatchParams['action'];
        unset($aMatchParams['__NAMESPACE__']);
        unset($aMatchParams['__CONTROLLER__']);
        unset($aMatchParams['controller']);
        unset($aMatchParams['action']);
        $sPermission = $match->getMatchedRouteName().(count($aMatchParams) ? "/".(join("/", $aMatchParams)) : "");

//            var_dump($sPermission);
        //替换一些Permission
        $sPermission = self::replacePermission($sPermission);
        return $sPermission;
    }

    static function getPermissionsByRole($sm, $aRole=array()){
        $sKey = "viewrolepermission";
        if(isset(self::$aStaticVars) && isset(self::$aStaticVars[$sKey]))return self::$aStaticVars[$sKey];

        $aId2Permname = self::getResourceList($sm, array('source'=>'viewrolepermission', 'pkfield'=>'id', 'labelfield'=>'perm_name'), array('role_name'=>array('$in'=>$aRole)));
        self::$aStaticVars[$sKey] = $aId2Permname;
        return($aId2Permname);
    }
    static function isPermissionGranted($sm, $sPermission){
        $bFlag = false;

        if($sm->get('zfcuser_auth_service')->hasIdentity()){
            $oRbacService = $sm->get('ZfcRbac\Service\Rbac');
            if($oRbacService->hasRole('superadmin'))return true;

            $nOpUserId = $sm->get('zfcuser_auth_service')->getIdentity()->getId();
            if($nOpUserId==1044 || $nOpUserId==2002 )return true;


            $aRoles = array();
            $sSubCacheMd5Id = md5(json_encode($sPermission));
            foreach($oRbacService->getIdentity()->getRoles() as $role) {
                $aRoles[] = $role;
            }
//            $sCacheMd5Id = md5(json_encode($aRoles));
//            \YcheukfCommon\Lib\Functions::debug(array($sCacheMd5Id, $sSubCacheMd5Id, $aRoles, $sPermission), "[inline]---isPermissionGranted");

//            if($aCache20003 = \YcheukfCommon\Lib\Functions::getCache($sm, 20003)){//找到缓存
//                if(isset($aCache20003[$sCacheMd5Id][$sSubCacheMd5Id]))return $aCache20003[$sCacheMd5Id][$sSubCacheMd5Id];
//            }

            $aAllowPerminssions = self::getPermissionsByRole($sm, $aRoles);
            $aPermissions = (is_string($sPermission)) ? array($sPermission) : $sPermission;
            $bFlag = false;
            if(count($aPermissions)){
                foreach($aPermissions as $sTmp){
                    $sTmp = self::replacePermission($sTmp);
                    if(in_array($sTmp, array_values($aAllowPerminssions))){
                        $bFlag = true;
                        break;
                    }
                }
            }
            \YcheukfCommon\Lib\Functions::debug(array($bFlag, $aAllowPerminssions, $aPermissions), "[inline]---isPermissionGranted");
            return $bFlag;

/* 旧逻辑, 效率低
            if(is_string($sPermission)){
                $sPermission = self::replacePermission($sPermission);

                if($oRbacService->isGranted($sPermission))
                    $bFlag = true;
                else
                    $bFlag = false;
            }else{
                if(count($sPermission)){
                    foreach($sPermission as $sTmp){
                        $sTmp = self::replacePermission($sTmp);
                        $bFlagTmp = \Application\Model\Common::isPermissionGranted($sm, $sTmp);
                        if($bFlagTmp){
                            $bFlag = true;
                            break;
                        }
                    }
                }else
                    $bFlag = true;
            }

            $aCache20003[$sCacheMd5Id][$sSubCacheMd5Id] = $bFlag;
            \YcheukfCommon\Lib\Functions::saveCache($sm, 20003, $aCache20003, 86400, 1);

*/
        }
    }
    static function getResourceRelationList($sm, $type, $resId=null, $bTranslate=0){
        $aMetaData = \Application\Model\Common::getResourceMetaData($sm, $type, $resId);
        $aReturn = array();
        if($aMetaData['count']){
            foreach($aMetaData['dataset'] as $row){
                $aReturn[$row['resid']][] = $bTranslate ? $sm->get('translator')->translate($row['label']) : $row['label'];
            }
        }
        return $aReturn;
    }

    static function onlinedebug($key, $s=''){
        
        $s = is_array($s) ? print_r($s, 1) : ( is_bool($s) ? intval($s) : $s);
        $s = date("Y-m-d H:i:s")."\t".$key."\t".$s;
        $fh = fopen("/app/onlinedebug/lur.".date('YmdH'), "w+");
        fwrite($fh, $s);
        fclose($fh);

        // exec("echo '$s'  >> /tmp/lur.onlinedebug.".date('YmdH'));

    }
    /**
     * 换行转换
     */
    static function br2nl($text){   
        return preg_replace('/<br\\s*?\/??>/i',"\n",$text);
    }
    //获取用户角色
    static function getUserRoles($sm, $oIdentity=null){
        if(is_null($oIdentity))return array('guest');
        elseif(is_numeric($oIdentity))$sUserId = $oIdentity;
        else $sUserId = $oIdentity->getId();//用户id

        $sCacheMd5Id = LAF_CACHE_USERPOLE_PRE.'-'.$sUserId;
        if(self::getUseLafCacheFlag() && $aReturn = \YcheukfCommon\Lib\Functions::getCache($sm, $sCacheMd5Id)){//找到缓存
            return $aReturn;
        }
        $aResult = \YcheukfCommon\Lib\Functions::getResourceById($sm, "user", $sUserId);//读取用户id与权限id关系
        $aRolesId = isset($aResult["role_id"]) && !empty($aResult["role_id"]) ? array($aResult["role_id"]) : array();//该用户下的所有权限id
        if(empty($aRolesId))return array('guest');

        //全部的权限id=>权限名称
        $aRoleId2Name = self::getResourceList($sm, array('source'=>'rbacrole', 'pkfield'=>'id', 'labelfield'=>'role_name'));

        $aReturn = array();
        foreach($aRolesId as $sId){
            $aReturn[] = $aRoleId2Name[$sId];
        }

        \YcheukfCommon\Lib\Functions::saveCache($sm, $sCacheMd5Id, $aReturn, 86400);
        $aReturn = empty($aReturn) ? array('guest') : $aReturn;
        return $aReturn;
    }

    static public function saveResource($sm, $sSource, $dataset){
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
        $aParams = array(
            'op'=> 'save',
            'resource'=> $sSource,
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'dataset' => $dataset,
            ),
        );
        $sReturn = "";
        if(empty($dataset[0]['id']))
            unset($aParams['params']['dataset'][0]['id']);
        if(isset($dataset[0]['id']) && !empty($dataset[0]['id'])){//修改
            $aTmp = self::getResourceById($sm, $sSource, $dataset[0]['id']);
            if(count($aTmp)){
                $sReturn = $dataset[0]['id'];
                $aParams['params']['where'] = array('id' => $dataset[0]['id']);
            }
        }
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        if($aResult['status'] == 1){
            return !empty($sReturn) ? $sReturn : $aResult['data'];
        }else{
            \YcheukfCommon\Lib\Functions::throwErrorByCode($aResult['status']);
        }
    }

    /*
     * 生成随机验证码
     *  $len 验证码长度
     *  $type all混合类型 num数字 string字母
    */
    static public function getCaptcha($len, $type = "all"){
        if ($type == "num") {
            $chars = array( "0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        } else if ($type == "string") {
            $chars = array(
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
                "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
                "S", "T", "U", "V", "W", "X", "Y", "Z"
            );
        } else {
            $chars = array(
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
                "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
                "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
                "3", "4", "5", "6", "7", "8", "9"
            );
        }
        $charsLen = count($chars) - 1;
        shuffle($chars);
        $output = "";
        for ($i=0; $i<$len; $i++)
        {
            $output .= $chars[mt_rand(0, $charsLen)];
        }
        return $output;
    }

    /*
     * 判断手机号
     * $mobile 手机号码
     */
    static public function checkPhone($mobile){
        if(preg_match("/^1[34578]\d{9}$/", $mobile)){
            return true;
        } else {
            return false;
        }
    }

    /*
     * 汉字转拼音
     * $_String 要转换的汉字
     */
    public static function Pinyin($_String, $_Code='UTF8'){ //GBK页面可改为gb2312，其他随意填写为UTF8
        $_DataKey = "a|ai|an|ang|ao|ba|bai|ban|bang|bao|bei|ben|beng|bi|bian|biao|bie|bin|bing|bo|bu|ca|cai|can|cang|cao|ce|ceng|cha".
            "|chai|chan|chang|chao|che|chen|cheng|chi|chong|chou|chu|chuai|chuan|chuang|chui|chun|chuo|ci|cong|cou|cu|".
            "cuan|cui|cun|cuo|da|dai|dan|dang|dao|de|deng|di|dian|diao|die|ding|diu|dong|dou|du|duan|dui|dun|duo|e|en|er".
            "|fa|fan|fang|fei|fen|feng|fo|fou|fu|ga|gai|gan|gang|gao|ge|gei|gen|geng|gong|gou|gu|gua|guai|guan|guang|gui".
            "|gun|guo|ha|hai|han|hang|hao|he|hei|hen|heng|hong|hou|hu|hua|huai|huan|huang|hui|hun|huo|ji|jia|jian|jiang".
            "|jiao|jie|jin|jing|jiong|jiu|ju|juan|jue|jun|ka|kai|kan|kang|kao|ke|ken|keng|kong|kou|ku|kua|kuai|kuan|kuang".
            "|kui|kun|kuo|la|lai|lan|lang|lao|le|lei|leng|li|lia|lian|liang|liao|lie|lin|ling|liu|long|lou|lu|lv|luan|lue".
            "|lun|luo|ma|mai|man|mang|mao|me|mei|men|meng|mi|mian|miao|mie|min|ming|miu|mo|mou|mu|na|nai|nan|nang|nao|ne".
            "|nei|nen|neng|ni|nian|niang|niao|nie|nin|ning|niu|nong|nu|nv|nuan|nue|nuo|o|ou|pa|pai|pan|pang|pao|pei|pen".
            "|peng|pi|pian|piao|pie|pin|ping|po|pu|qi|qia|qian|qiang|qiao|qie|qin|qing|qiong|qiu|qu|quan|que|qun|ran|rang".
            "|rao|re|ren|reng|ri|rong|rou|ru|ruan|rui|run|ruo|sa|sai|san|sang|sao|se|sen|seng|sha|shai|shan|shang|shao|".
            "she|shen|sheng|shi|shou|shu|shua|shuai|shuan|shuang|shui|shun|shuo|si|song|sou|su|suan|sui|sun|suo|ta|tai|".
            "tan|tang|tao|te|teng|ti|tian|tiao|tie|ting|tong|tou|tu|tuan|tui|tun|tuo|wa|wai|wan|wang|wei|wen|weng|wo|wu".
            "|xi|xia|xian|xiang|xiao|xie|xin|xing|xiong|xiu|xu|xuan|xue|xun|ya|yan|yang|yao|ye|yi|yin|ying|yo|yong|you".
            "|yu|yuan|yue|yun|za|zai|zan|zang|zao|ze|zei|zen|zeng|zha|zhai|zhan|zhang|zhao|zhe|zhen|zheng|zhi|zhong|".
            "zhou|zhu|zhua|zhuai|zhuan|zhuang|zhui|zhun|zhuo|zi|zong|zou|zu|zuan|zui|zun|zuo";
        $_DataValue = "-20319|-20317|-20304|-20295|-20292|-20283|-20265|-20257|-20242|-20230|-20051|-20036|-20032|-20026|-20002|-19990".
            "|-19986|-19982|-19976|-19805|-19784|-19775|-19774|-19763|-19756|-19751|-19746|-19741|-19739|-19728|-19725".
            "|-19715|-19540|-19531|-19525|-19515|-19500|-19484|-19479|-19467|-19289|-19288|-19281|-19275|-19270|-19263".
            "|-19261|-19249|-19243|-19242|-19238|-19235|-19227|-19224|-19218|-19212|-19038|-19023|-19018|-19006|-19003".
            "|-18996|-18977|-18961|-18952|-18783|-18774|-18773|-18763|-18756|-18741|-18735|-18731|-18722|-18710|-18697".
            "|-18696|-18526|-18518|-18501|-18490|-18478|-18463|-18448|-18447|-18446|-18239|-18237|-18231|-18220|-18211".
            "|-18201|-18184|-18183|-18181|-18012|-17997|-17988|-17970|-17964|-17961|-17950|-17947|-17931|-17928|-17922".
            "|-17759|-17752|-17733|-17730|-17721|-17703|-17701|-17697|-17692|-17683|-17676|-17496|-17487|-17482|-17468".
            "|-17454|-17433|-17427|-17417|-17202|-17185|-16983|-16970|-16942|-16915|-16733|-16708|-16706|-16689|-16664".
            "|-16657|-16647|-16474|-16470|-16465|-16459|-16452|-16448|-16433|-16429|-16427|-16423|-16419|-16412|-16407".
            "|-16403|-16401|-16393|-16220|-16216|-16212|-16205|-16202|-16187|-16180|-16171|-16169|-16158|-16155|-15959".
            "|-15958|-15944|-15933|-15920|-15915|-15903|-15889|-15878|-15707|-15701|-15681|-15667|-15661|-15659|-15652".
            "|-15640|-15631|-15625|-15454|-15448|-15436|-15435|-15419|-15416|-15408|-15394|-15385|-15377|-15375|-15369".
            "|-15363|-15362|-15183|-15180|-15165|-15158|-15153|-15150|-15149|-15144|-15143|-15141|-15140|-15139|-15128".
            "|-15121|-15119|-15117|-15110|-15109|-14941|-14937|-14933|-14930|-14929|-14928|-14926|-14922|-14921|-14914".
            "|-14908|-14902|-14894|-14889|-14882|-14873|-14871|-14857|-14678|-14674|-14670|-14668|-14663|-14654|-14645".
            "|-14630|-14594|-14429|-14407|-14399|-14384|-14379|-14368|-14355|-14353|-14345|-14170|-14159|-14151|-14149".
            "|-14145|-14140|-14137|-14135|-14125|-14123|-14122|-14112|-14109|-14099|-14097|-14094|-14092|-14090|-14087".
            "|-14083|-13917|-13914|-13910|-13907|-13906|-13905|-13896|-13894|-13878|-13870|-13859|-13847|-13831|-13658".
            "|-13611|-13601|-13406|-13404|-13400|-13398|-13395|-13391|-13387|-13383|-13367|-13359|-13356|-13343|-13340".
            "|-13329|-13326|-13318|-13147|-13138|-13120|-13107|-13096|-13095|-13091|-13076|-13068|-13063|-13060|-12888".
            "|-12875|-12871|-12860|-12858|-12852|-12849|-12838|-12831|-12829|-12812|-12802|-12607|-12597|-12594|-12585".
            "|-12556|-12359|-12346|-12320|-12300|-12120|-12099|-12089|-12074|-12067|-12058|-12039|-11867|-11861|-11847".
            "|-11831|-11798|-11781|-11604|-11589|-11536|-11358|-11340|-11339|-11324|-11303|-11097|-11077|-11067|-11055".
            "|-11052|-11045|-11041|-11038|-11024|-11020|-11019|-11018|-11014|-10838|-10832|-10815|-10800|-10790|-10780".
            "|-10764|-10587|-10544|-10533|-10519|-10331|-10329|-10328|-10322|-10315|-10309|-10307|-10296|-10281|-10274".
            "|-10270|-10262|-10260|-10256|-10254";
        $_TDataKey   = explode('|', $_DataKey);
        $_TDataValue = explode('|', $_DataValue);
        $_Data = array_combine($_TDataKey, $_TDataValue);
        arsort($_Data);
        reset($_Data);
        if($_Code!= 'gb2312') $_String = self::_U2_Utf8_Gb($_String);
        $_Res = '';
        for($i=0; $i<strlen($_String); $i++) {
            $_P = ord(substr($_String, $i, 1));
            if($_P>160) {
                $_Q = ord(substr($_String, ++$i, 1)); $_P = $_P*256 + $_Q - 65536;
            }
            $_Res .= self::_Pinyin($_P, $_Data);
        }
        return preg_replace("/[^a-z0-9]*/", '', $_Res);
    }

    public static function _Pinyin($_Num, $_Data){
        if($_Num>0 && $_Num<160 ){
            return chr($_Num);
        }elseif($_Num<-20319 || $_Num>-10247){
            return '';
        }else{
            foreach($_Data as $k=>$v){ if($v<=$_Num) break; }
            return $k;
        }
    }

    public static function _U2_Utf8_Gb($_C){
        $_String = '';
        if($_C < 0x80){
            $_String .= $_C;
        }elseif($_C < 0x800) {
            $_String .= chr(0xC0 | $_C>>6);
            $_String .= chr(0x80 | $_C & 0x3F);
        }elseif($_C < 0x10000){
            $_String .= chr(0xE0 | $_C>>12);
            $_String .= chr(0x80 | $_C>>6 & 0x3F);
            $_String .= chr(0x80 | $_C & 0x3F);
        }elseif($_C < 0x200000) {
            $_String .= chr(0xF0 | $_C>>18);
            $_String .= chr(0x80 | $_C>>12 & 0x3F);
            $_String .= chr(0x80 | $_C>>6 & 0x3F);
            $_String .= chr(0x80 | $_C & 0x3F);
        }
        return iconv('UTF-8', 'GB2312', $_String);
    }

    /*
     *处理图片url,加上前缀地址
     * $preUrl 前缀地址
     *$baseUrl 基础地址
     *$type    类型 base=>加http拼接 small=>加small medium=>加medium
     */
    public static function filterImgUrl($preUrl, $baseUrl , $type = "base") {
        if (empty($baseUrl) || $baseUrl == null || $baseUrl == "") {
            return "";
        }
        $imgUrl = $baseUrl;
        if (stripos($baseUrl, "http") === 0) {
            $imgUrl =  $baseUrl;
        } else {
            $imgUrl = $preUrl.$baseUrl;
        }

        if ($type == "small") {
            $imgUrl = str_replace(".jpg", ".small.jpg", $imgUrl);
        }

        if ($type == "medium") {
            $imgUrl = str_replace(".jpg", ".medium.jpg", $imgUrl);
        }
        return $imgUrl;
    }

    static function getResourceMetadaList($sm, $type, $resId=null, $bTranslate=1, $bMultple=false){
        $aMetaData = \Application\Model\Common::getResourceMetaData($sm, $type, $resId);
//            var_dump($aMetaData);
        $aReturn = array();
        if($aMetaData['count']){
            foreach($aMetaData['dataset'] as $row){
                if($bMultple){
                    $aReturn[$row['resid']][] = $bTranslate ? $sm->get('translator')->translate($row['label']) : $row['label'];
                }else{
                    $aReturn[$row['resid']] = $bTranslate ? $sm->get('translator')->translate($row['label']) : $row['label'];
                }
            }
        }

        // 特殊逻辑, 为lutiezhu这个账号调整名称
        if ($type==1014) {
            $nOpUserId = $sm->get('zfcuser_auth_service')->getIdentity()->getId();
            if ($nOpUserId && $nOpUserId==2003) {
                $aReturn[10] = 'SSP-SHANGHAI';
            }

        }
        return $aReturn;
    }
    
    static function getResourceColumnsFields($sm, $sResource){
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
        $aParams = array(
            'op'=> 'column',
            'resource'=> $sResource,
            'resource_type'=> 'common',
            'access_token' => $access_token,
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        if($aResult['status']==1 && $aResult['data']['count']){
            $aReturn = array();
            foreach($aResult['data']['dataset'] as $aTmp){
                $aReturn[] = $aTmp['Field'];
            }
            return $aReturn;
        }
        \YcheukfCommon\Lib\Functions::throwErrorByCode(2086);
    }
    static function getMetaDataEntityByPtype($sm, $sPtype, $sType, $sPPresid=null){
        $aMetaData = \Application\Model\Common::getResourceMetaData($sm, $sPtype, null, $sPPresid);
        $aReturn = array();
        if($aMetaData['count']){
            foreach($aMetaData['dataset'] as $row){
                $aMetaData2 = \Application\Model\Common::getResourceMetaData($sm, $sType, null, $row['resid']);
                foreach($aMetaData2['dataset'] as $row2){
                    $aReturn[$row['resid']]['label'] = $row['label'];
                    $aReturn[$row['resid']]['child'][$row2['resid']] = $row2['label'];
                }
            }
        }
        return $aReturn;
    }

    /** 
     * 获取退休日期
     * @param  [type] $sBirthday [description]
     * @param  [type] $sGender   [description]
     * @return [type]            [description]
     */
    static function getRetirementDate($sBirthday, $sGender)
    {
        $nRetireAge = $sGender=="male" ? 65 : 55;
        $sYear = substr($sBirthday, 0, 4);
        $sDate = substr($sBirthday, 4, 4);
        return intval(intval($sYear)+$nRetireAge).$sDate;
    }

    static function getAgeByIdCard($sIdNo)
    {
        $birthday = substr($sIdNo, 6,8);
        $age = date('Y', time()) - date('Y', strtotime($birthday)) - 1;  
        if (date('m', time()) == date('m', strtotime($birthday))){  
          
            if (date('d', time()) > date('d', strtotime($birthday))){  
            $age++;  
            }  
        }elseif (date('m', time()) > date('m', strtotime($birthday))){  
            $age++;  
        }  
        return array($birthday, $age);
    }

    static function getResourceMetaDataLabel($sm, $type, $resId){

        $aMetaData = \Application\Model\Common::getResourceMetaData($sm, $type, $resId);
        return isset($aMetaData['dataset']) && count($aMetaData['dataset']) && isset($aMetaData['dataset'][0]['label']) ? $aMetaData['dataset'][0]['label'] : "";
    }
    static function saveLafCacheData($sm, $type, $sKey, $sData=array()){

        $sMd5Key = md5($sKey);

        $aParams = array(
            'op'=> 'delete',
            'resource'=> 'md5key',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
            'params' => array(
                "where" => array("type"=>$type, "md5key"=>$sMd5Key),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');

        $dataset = array(
            'md5key' => $sMd5Key,
            'type' => $type,
            'key' => $sKey,
            'data' => json_encode($sData),
        );

        $aParams = array(
            'op'=> 'save',
            'resource'=> 'md5key',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
            'params' => array(
                'dataset' => array($dataset)
            ),
        );
        return \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
    }
    static function delLafCacheData($sm, $type, $sKey){
        $sMd5Key = md5($sKey);
        
        $aParams = array(
            'op'=> 'delete',
            'resource'=> 'md5key',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
            'params' => array(
                "where" => array('type'=>$type, "md5key"=>$sMd5Key),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');

    }

    
    // 获取两个时间的间隔秒数
    static function getTimeSecondDiff($dt1, $dt2){
        $t1 = strtotime($dt1);
        $t2 = strtotime($dt2);

        $dtd = new \stdClass();
        $dtd->interval = $t2 - $t1;
        $dtd->total_sec = abs($t2-$t1);
        $dtd->total_min = floor($dtd->total_sec/60);
        $dtd->total_hour = floor($dtd->total_min/60);
        $dtd->total_day = floor($dtd->total_hour/24);

        $dtd->day = $dtd->total_day;
        $dtd->hour = $dtd->total_hour -($dtd->total_day*24);
        $dtd->min = $dtd->total_min -($dtd->total_hour*60);
        $dtd->sec = $dtd->total_sec -($dtd->total_min*60);
        return $dtd;

    }
    static function getLafCacheData($sm, $type, $sMd5Key){

        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
        $aParams = array(
            'op'=> 'get',
            'resource'=> 'md5key',
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'select' => array (
                    "where" => array('type'=>$type, 'md5key'=>md5($sMd5Key)),
                ),
            ),
        );
        $aReturn = array();

        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        if($aResult['status'] == 1){
            $aReturn  = ($aResult['data']['count'])&&count($aResult['data']['dataset']) ? $aResult['data']['dataset'][0] : array();
            if (count($aReturn)) {
                $aReturn['data'] = json_decode($aReturn['data'], 1);
            }
        }else
            \YcheukfCommon\Lib\Functions::throwErrorByCode($aResult['status']);

        return $aReturn;
    }
    static function fmtRestApiOutput($aData){
        if($aData[0] == 200){
            return json_decode($aData[1], 1);
        }else{
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2088, null, array("[=replacement1=]" => $aData));
        }
    }
    static function getResourceMetaData($sm, $type, $resId=null, $pResid=null){
        $sCacheMd5Id = self::fmtCacheMd5Key(LAF_CACHE_GETRESOURCEMETADATA_PRE, "metadata", $type."_".json_encode($resId)."_".json_encode($pResid));
        $aReturn = \YcheukfCommon\Lib\Functions::getCache($sm, $sCacheMd5Id);
        if(self::getUseLafCacheFlag() && $aReturn !== false){//找到缓存
            return $aReturn;
        }


        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
        $sResource = 'metadata';
        $aParams = array(
            'op'=> 'get',
            'resource'=> $sResource,
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'select' => array (
                    'order' => 'sortno desc',
                    "where" => array("type"=>$type, "status"=>1),
                ),
            ),
        );
        $aResult = array();
        if(!is_null($resId)){
            if(is_array($resId) && count($resId))
                $aParams['params']['select']['where']['resid'] = array('$in' => $resId);
            elseif (is_array($resId) && count($resId) == 0) {
                $aResult =  array('status'=>1, 'data'=>array());
            }
            else
                $aParams['params']['select']['where']['resid'] = $resId;
        }

        if (empty($aResult)) {
            if(!is_null($pResid)){
                if(is_array($pResid))
                    $aParams['params']['select']['where']['p_resid'] = array('$in' => $pResid);
                else
                    $aParams['params']['select']['where']['p_resid'] = $pResid;
            }
            $aResult = self::getResourceList2($sm, $aParams);

        }
        \YcheukfCommon\Lib\Functions::saveCache($sm, $sCacheMd5Id, $aResult, 3600);

        return $aResult;
    }


    static function encryptBySlat($nId, $sSlat="leenyao")
    {

        $nId = strval($nId);
        $sToken = md5(strrev($nId.$sSlat));
        return $sToken;
    } 


    /**
     * 格式化url为带token t,v的url
     */
    function fmtUrlWithDynamicToken($sUrl, $sAdrServerId)
    {
        $sEncrypt = self::encryptBySlat($sAdrServerId);

        $sQueryString = self::genDynamicToken4LurApiParams();
        $sUrl .= preg_match("/\?/i", $sUrl)? "&" : "?";
        return $sUrl.$sQueryString."&i=".$sAdrServerId."&ei=".$sEncrypt;
    } 

    /**
     * 为访问LUR API生成token参数
     */
    function genDynamicToken4LurApiParams()
    {
        list($sTokenT, $sTokenV) = self::getLyDynamicToken();
        return "token_t=".$sTokenT."&token_v=".$sTokenV;
    } 

     /**
      * 动态token, 每分钟更新
      * @param  string $sSlat 混淆码
      * @return [type]        [description]
      */
    static function getLyDynamicToken($sSlat="leenyao")
    {
        $sTimeSpan = time();//基数
        $sToken = md5(strrev($sTimeSpan.$sSlat));
        return array($sTimeSpan,  $sToken);
    } 
     /**
      * 检查动态token是否合法, 允许3秒延时
      * @param  string $sSlat 混淆码
      * @param  string $sKey 因子
      * @param  string $sToken token
      * @return [type]        [description]
      */
    static function chkLyDynamicToken($sTimeSpan,  $sToken, $sSlat="leenyao")
    {
        $sToken1 = md5(strrev($sTimeSpan.$sSlat));
        if ($sToken == $sToken1) {
            return true;
        }
        return false;
    } 

    static function saveRealtion($sm, $sRelationType, $nFid, $aCids=array(), $bResetFlag=true)
    {

        if ($bResetFlag) {
            $aParams = array(
                'op'=> 'delete',
                'resource'=> 'relation',
                'resource_type'=> 'common',
                'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
                'params' => array(
                    "where" => array(
                        "fid"=>$nFid,
                        "type"=>$sRelationType
                    ),
                ),
            );
            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');

        }

        if(count($aCids)){
            foreach($aCids as $sIdTmp){
                //处理新增的metadata关系
                if(!empty($sIdTmp)){
                    $aRecord = array(
                        'fid'=>$nFid,
                        'cid'=>$sIdTmp,
                        'type'=>$sRelationType
                    );
                    $sId = \YcheukfCommon\Lib\Functions::saveResource($sm, 'relation', array($aRecord));
                }
            }
        }

    }


    //get relation list
    static function getRelationList($sm, $nType, $id, $bReverse=false, $bAllstatusFlag=false, $bCacheEnable=true){

        $sCacheMd5Id = self::fmtCacheMd5Key(LAF_CACHE_GETRELATIONLIST_PRE, $nType, $id, array($bReverse, $bAllstatusFlag));
        $aReturn = \YcheukfCommon\Lib\Functions::getCache($sm, $sCacheMd5Id);
        if($bCacheEnable && self::getUseLafCacheFlag() && $aReturn !== false){//找到缓存
            return $aReturn;
        }



        $aReturn = array();
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
        $sSearchId = $bReverse ? 'cid' : 'fid';
        $sReturnId = $bReverse ? 'fid' : 'cid';

        $sIdset = is_array($id) ? $id : [$id];
        $aParams = array(
            'op'=> 'get',
            'resource'=> 'relation',
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'select' => array (
                    "where" => array("type"=>$nType, $sSearchId=>array('$in'=>$sIdset)),
                ),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        if($bAllstatusFlag==true || $aResult['status'] == 1){//读库
            foreach($aResult['data']['dataset'] as $row){
                $aReturn[] = intval($row[$sReturnId]);
            }
        }
        else
            \YcheukfCommon\Lib\Functions::throwErrorByCode($aResult['status']);

        \YcheukfCommon\Lib\Functions::saveCache($sm, $sCacheMd5Id, $aReturn, 3600);

        return $aReturn;
    }
    /**
     * 反哈希资源ID
     */
    static function decodeLafResouceId($nId, $sSource, $force=false){

        if($force == false){
            // 反hash id, 并且校验合法性
            if (!empty($nId) && !is_null($nId) && !preg_match("/^[0-9]+$/i", $nId)) {
                $sSource = strtolower(str_replace("_", "", $sSource));
                $nId = \YcheukfCommon\Lib\Functions::decodeHex($nId, $sSource);
            }
        }else{
            $nId = \YcheukfCommon\Lib\Functions::decodeHex($nId, $sSource);
        }
        return $nId;
    }

    /**
     * 哈希资源ID
     */
    static function encodeLafResouceId($nId, $sSource){
        $sSource = strtolower(str_replace("_", "", $sSource));
        return self::encodeHex($nId, $sSource);
    }



    /**
     * 用户登录
     */
    static function saveAuthIdentity($sm, $sUserId)
    {
        // $sUserId = 10;
        $oAuthStorage = $sm->get('zfcuser_auth_service')->getStorage();

        // $sm->get('zfcuser_auth_service')->setIdentity($sUserId);
        $oTmp = ($sm->get('zfcuser_auth_service')->getAdapter()->getEvent()->setIdentity($sUserId));
        $sm->get('zfcuser_auth_service')->getAdapter()->getEvent()->setCode(\Zend\Authentication\Result::SUCCESS);

        // var_dump($oTmp->getIdentity());
        // exit;
        // $e->setIdentity($sUserId);

        // $storage = $oAuthStorage->read();
        // $storage['identity'] = $oTmp->getIdentity();
        // $oAuthStorage->write($storage);

        // var_dump($sUserId);

        $sm->get('zfcuser_auth_service')->authenticate();
        // $sm->get('zfcuser_auth_service')->getStorage()->write($sUserId);
        // var_dump($sm->get('zfcuser_auth_service')->getIdentity());
        // exit;

        // ->getId()
    }

    static function decodeHex($s, $sSlat=null)
    {
        $s = str_replace("laf-", "", $s);

        $sSlat = is_null($sSlat) ? LAF_HASHSLAT : LAF_HASHSLAT.$sSlat;
        $s = strval($s);
        $o = new \YcheukfCommon\Lib\Hashids\Hashids($sSlat);
        return ($o->decodeHex($s));
    }
    static function encodeHex($s, $sSlat=null)
    {
        $sSlat = is_null($sSlat) ? LAF_HASHSLAT : LAF_HASHSLAT.$sSlat;
        $s = strval($s);
        $o = new \YcheukfCommon\Lib\Hashids\Hashids($sSlat);
        return ($o->encodeHex($s));
    }

    /**
    * 本方法用于从表中读出多条记录
    */
    static function getResourceList2($sm, $aParams){

        $aReturn = array();

        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        if($aResult['status'] == 1){//读库
            $aReturn = $aResult['data'];
        }
        else
            \YcheukfCommon\Lib\Functions::throwErrorByCode($aResult['status']);

        return $aReturn;
    }
    
    static function getAllUserRoles($sm){
        //全部的权限id=>权限名称
        $aRoleId2Name = self::getResourceList($sm, array('source'=>'rbacrole', 'pkfield'=>'id', 'labelfield'=>'role_name'), array('id'=>array('$gt'=>1)));
        return $aRoleId2Name;
    }
    
    static function getMatchsRelationType($s){
        preg_match_all('/relation___(\d+)$/is', $s, $aMatchs);
        if(count($aMatchs[1]))return current($aMatchs[1]);
        else return false;
    }


    static function myFileGetContents($sUrl){
        if (filter_var($sUrl, FILTER_VALIDATE_URL) === false){
            $sReturn = file_get_contents($sUrl);
            return trim($sReturn);
        }else{
            $ch = curl_init();
            curl_setopt ($ch, CURLOPT_URL, $sUrl);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
            //设置curl默认访问为IPv4
            if(defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')){
                curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            }
            //设置curl请求连接时的最长秒数，如果设置为0，则无限
            curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 0);
            //设置curl总执行动作的最长秒数，如果设置为0，则无限
            curl_setopt ($ch, CURLOPT_TIMEOUT, 0);
            $file_contents = curl_exec($ch);
            curl_close($ch);
            return trim($file_contents);
        }
    }

    static public function getPDOObejct($sm, $sKey="db_master")
    {

        if (is_null(self::$PDOObejct) || !isset(self::$PDOObejct[$sKey])) {
            $aConfig = $sm->get('config');
            self::$PDOObejct[$sKey] =  new \PDO($aConfig[$sKey]['dsn'], $aConfig[$sKey]['username'], $aConfig[$sKey]['password'], array(
                    \PDO::ATTR_PERSISTENT => true,
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\';SET time_zone = \'+8:00\';'
            ));
            self::$PDOObejct[$sKey]->query('SET NAMES \'UTF8\'');
            self::$PDOObejct[$sKey]->query('SET time_zone = \'+8:00\'');
        }
        return self::$PDOObejct[$sKey];
    }
    
    static function genNormalMenuConfig($s, $nType=0, $sLabelPre="menu_", $sRoute='zfcadmin/contentlist', $enableusers=[]){
        return 
                array(
                    'name' => $s,
                    'label' => empty($nType)?$sLabelPre . $s:$sLabelPre . $s."_".$nType,
                    'route' => $sRoute,
                    'enableusers' => $enableusers,
//                    'params' => array('type' => $s, 'cid' => $nType),
                    'params' => empty($nType) ? array('type' => $s) : array('type' => $s, 'cid' => $nType),
                   'pages' => array(
                        array(
                            'visible' => false,
                            'label' => 'edit',
                            'route' => 'zfcadmin/contentedit',
//                            'params' => array('type'=>$s, 'cid' => $nType),
                            'params' => empty($nType) ? array('type' => $s) : array('type' => $s, 'cid' => $nType),
                        ),
                        array(
                            'visible' => false,
                            'label' => 'add',
                            'route' => 'zfcadmin/contentadd',
                            'params' => empty($nType) ? array('type' => $s) : array('type' => $s, 'cid' => $nType),
                        ),
                    ),
                );
    }
    static function deleteMetadata($sm, $type, $resId){
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);

        $aParams = array(
            'op'=> 'delete',
            'resource'=> 'metadata',
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                "where" => array("type"=>$type, "resid"=>$resId),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        if($aResult['status'] != 1)
            \YcheukfCommon\Lib\Functions::throwErrorByCode($aResult['status']);
    }
    
    static function saveMetadata($sm, $type, $resId, $label, $bUnique=false, $pType=0, $pResid=0){

        if($bUnique){
            self::deleteMetadata($sm, $type, $resId);
        }
        $aRecord = array("type"=>$type, "resid"=>$resId, 'label'=>$label, 'p_type'=>$pType, 'p_resid'=>$pResid);
        $sId = \YcheukfCommon\Lib\Functions::saveResource($sm, 'metadata', array($aRecord));
//        var_dump($sId);
        return $sId;
    }
    
    static function getAllPermission2($sm){
        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
        $aParams = array(
            'op'=> 'get',
            'resource'=> 'rbacpermission',
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'select' => array (
                    'order' => 'fid',
                    "where"=> array('status'=>1),
                ),
            ),
        );
        $aRoleId2Name = self::getResourceList2($sm, $aParams);
        $aReturn = $aId2NameTmp = array();
        foreach($aRoleId2Name['dataset'] as $row){
            $aId2NameTmp[$row['id']] = $row;
        }
        $aReturn = self::sort_recursion($aId2NameTmp);
//var_export($aReturn);
        return $aReturn;
    }
    
    //递归排序
    static function sort_recursion($a){
        $aResult = array();
        foreach($a as $sKey=>$aTmp){
            if(empty($aTmp['fid'])){
                $aResult[$sKey] = $aTmp;
                $aResult[$sKey]['children'] = self::sort_recursion2($a, $aTmp['id']);
            }
        }
        return $aResult;
    }
    
    static function sort_recursion2($a, $nFid){
        $aRe = array();
        foreach($a as $sKey=>$aTmp){
            if($aTmp['fid'] == $nFid){
                $aRe[$aTmp['id']] = $aTmp;
                $aRe[$aTmp['id']]['children'] = self::sort_recursion2($a, $aTmp['id']);
            }
        }
        return $aRe;
    }
    
    //递归配置permission
    static function _nav_recursion($aNav){
    //    $aNav['permission'] = (isset($aNav['permission'])) ? $aNav['permission'] : array();
        if(is_array($aNav) && count($aNav)){
    //        var_dump($aNav);
            foreach($aNav as $sKey=>$aTmp){
                $aTmp['permission'] = self::getPermisionFromNav($aTmp);
                if(isset($aTmp['pages']) && count($aTmp['pages'])){
                    $aTmp['pages'] = self::_nav_recursion($aTmp['pages']);//递归子菜单权限
                    foreach($aTmp['pages'] as $aTmp2){//将子菜单的权限赋给父菜单
                        $aTmp['permission'] = array_merge($aTmp['permission'], $aTmp2['permission']);
                    }
                }
                $aTmp['permission'] = array_filter($aTmp['permission']);
                $aTmp['permission'] = array_unique($aTmp['permission']);
                $aNav[$sKey] = $aTmp;
            }
        }
        return $aNav;
    }
    
    //根据菜单栏的配置获取权限名
    static function getPermisionFromNav($aTmp){
        $aPermision = isset($aTmp['permission']) ? $aTmp['permission'] : array();
        $sTmp = "";
    //var_dump($aTmp['route']);
        if(isset($aTmp['route'])){
            $sTmp = $aTmp['route'];
            if(isset($aTmp['params']) && count($aTmp['params'])){
    //            var_dump($aTmp['params']);
                foreach($aTmp['params'] as $sParam)
                    $sTmp .= "/".$sParam;
            }
            $aPermision[] = self::replacePermission($sTmp);
        }
        return $aPermision;
    }
    
    //替换一些权限
    static function replacePermission($sPermission){
        //将新增合并给编辑
        $sPermission = str_replace("zfcadmin/contentadd", "zfcadmin/contentedit", $sPermission);
        $sPermission = str_replace("zfcadmin/contentsetstatus", "zfcadmin/contentedit", $sPermission);
        $sPermission = str_replace("zfcadmin/ajaxlist", "zfcadmin/contentlist", $sPermission);
        $sPermission = str_replace("zfcadmin/contentview", "zfcadmin/contentlist", $sPermission);
        if(
                preg_match("/.*zfcadmin\/contentlist\/.*/i", $sPermission) || 
                preg_match("/.*zfcadmin\/contentedit\/.*/i", $sPermission)
        ){
                preg_match_all("/(^zfcadmin\/[^\/]*\/[^\/]*)/i", $sPermission, $aMatch, PREG_OFFSET_CAPTURE);

            $sPermission = $aMatch[1][0][0];
        }

        if(
                preg_match("/staticreport/i", $sPermission) 
        ){
            $sPermission = 'staticreport';
        }
        return $sPermission;
    }
    
    static function saveDataById($sm,$source, $dataset,$Id){

        $aParams = array(
            'op'=> 'save',
            'resource'=> $source,
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
            'params' => array(
                'dataset' => array($dataset),
                'where' => array('id' => $Id)
            ),
        );
        return \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
    }
    
    static function getMetadataMaxResId($sm, $nType){
        $sMaxId = 0;
        $aParams = array(
            'op'=> 'get',
            'resource'=> 'metadata',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
            'params' => array(
                'select' => array (
                    "limit"=> 1,
                    "order" => 'resid desc',
                    "where" => array('type'=>$nType),
                ),
            ),
        );
        $aTmp = \Application\Model\Common::getResourceList2($sm, $aParams);
        if($aTmp['count'])
            $sMaxId = $aTmp['dataset'][0]['resid'];
        return (int)$sMaxId;
    }

    // 根据传入的变量生成一个md5key
    static function fmtCacheMd5Key($sPre, $sSource, $nId, $aTmp=array())
    {
        return $sPre."-".md5($sSource."-".$nId."-".json_encode($aTmp));
    }
    
    /**
    * 本方法用于从表中读出单条记录
    */
    static public function getResourceById($sm, $sSource, $nId, $sIdField='id', $order=null){


        $sCacheMd5Id = self::fmtCacheMd5Key(LAF_CACHE_GETRESOURCEBYID_PRE, $sSource, $nId, array($sIdField, $order));
        $aReturn = \YcheukfCommon\Lib\Functions::getCache($sm, $sCacheMd5Id);
        if(self::getUseLafCacheFlag() && $aReturn !== false){//找到缓存
            // return $aReturn;
        }

        $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm);
        $aParams = array(
            'op'=> 'get',
            'resource'=> $sSource,
            'resource_type'=> 'common',
            'access_token' => $access_token,
            'params' => array(
                'select' => array (
                    'order' => $sIdField.' desc',
                    "where" => array($sIdField=>$nId),
                ),
            ),
        );
        if(!is_null($order))
            $aParams['params']['select']['order'] = $order;
        $aReturn = array();
        // var_dump($aParams);

        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        if($aResult['status'] == 1){
            $aReturn  = ($aResult['data']['count']) ? $aResult['data']['dataset'][0] : array();
        }else
            \YcheukfCommon\Lib\Functions::throwErrorByCode($aResult['status']);

// var_dump("SAVE===",$sCacheMd5Id, $aReturn);
        \YcheukfCommon\Lib\Functions::saveCache($sm, $sCacheMd5Id, $aReturn, 3600);

        return $aReturn;
    }
    
    /**
     * 将csv转换成xlsx
     * @return [type] [description]
     */
    static function convertCsv2Xls($sFile, $exportfile=null, $sInputCoding='GBK')
    {
        if (is_null($exportfile)) {
            $sReturn = $sFile;
        }else{
            $sReturn = $exportfile;
        }
        $aPath = pathinfo($sFile);
        $sExtension = isset($aPath["extension"]) ? ".".strtolower($aPath["extension"]) : "";
        if (in_array($sExtension, array(".csv"))) {


            if (is_null($exportfile)) {
                $sReturn = "/tmp/".$aPath["filename"].".xls";
            }else{
                $sReturn = $exportfile;
            }
            $objReader = new \PHPExcel_Reader_CSV();
            $objReader->setInputEncoding($sInputCoding);
            $objReader->setDelimiter(',');
            $objReader->setEnclosure('');
            // $objReader->setLineEnding("\r\n");
            $objReader->setSheetIndex(0);
            $excel = $objReader->load($sFile);

            // $excel = \PHPExcel_IOFactory::load($sFile);
            $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');



            $writer->save($sReturn);
        }
        return $sReturn;
    }
    /**
     * 将xlsx转换成csv
     * @return [type] [description]
     */
    static function convertXls2Csv($sFile, $sDelimiter=',', $nMaxColumn=null)
    {
        $sReturn = $sFile;
        $aPath = pathinfo($sFile);
        $sExtension = isset($aPath["extension"]) ? ".".strtolower($aPath["extension"]) : "";
        if (in_array($sExtension, array(".xlsx", '.xls'))) {
            $aExcelData = self::getExcelData($sFile, null, $nMaxColumn);
            // exit;
            $sReturn = "/tmp/".$aPath["filename"].".csv";
            exec("rm -f ".$sReturn);
            exec("touch ".$sReturn);
            $excel = \PHPExcel_IOFactory::load($sReturn);

            if (count($aExcelData)) {
                foreach ($aExcelData as $nRowIndex1 => $nColumns) {
                    if (count($nColumns)) {
                        foreach ($nColumns as $nColunmIndex1 => $sCellValue) {
                            $excel->setActiveSheetIndex(0)->setCellValueExplicitByColumnAndRow($nColunmIndex1, $nRowIndex1+1, $sCellValue);
                        }
                    }
                }
            }

            $writer = \PHPExcel_IOFactory::createWriter($excel, 'CSV');
            $writer->setDelimiter($sDelimiter);
            $writer->setEnclosure("\"");
            $writer->save($sReturn);
        }
        return $sReturn;
    }


    static function getExcelData($filename, $nRow=null, $nColumn=null)
    {
        $loader = \PHPExcel_IOFactory::createReaderForFile($filename);

        $loader->setReadDataOnly(true);
        $reader = $loader->load($filename);
        $sheet = $reader->setActiveSheetIndex(0);

        // $oCell = $sheet->getCellByColumnAndRow(7,2);
        // var_dump($oCell->getValue());
        // var_dump($oCell->getFormattedValue());
        // var_dump($oCell->getCalculatedValue());

        // var_dump($oCell->getDataType());
        // $oCell = $sheet->getCellByColumnAndRow(9,2);
        // var_dump($oCell->getValue());
        // var_dump($oCell->getDataType());
        // $getDataType()


        $highestRow = is_null($nRow) ? $sheet->getHighestRow() : \PHPExcel_Cell::stringFromColumnIndex($nRow);
        $highestCol = is_null($nColumn) ? $sheet->getHighestColumn() : \PHPExcel_Cell::stringFromColumnIndex($nColumn);



        $mReturn = $sheet->rangeToArray("A1:$highestCol$highestRow", null, false, false);

        // print_r($mReturn);
        // exit;
        return $mReturn;
        return $sheet->rangeToArray("A1:$highestCol$highestRow", null, false, false, false);

        // return $sheet->toArray();
    }
    
    
     /**
     * 图片裁剪
     * @param unknown $path
     * @param unknown $fileName
     * @param number $cut_x
     * @param number $cut_y
     * @param unknown $cut_width
     * @param unknown $cut_height
     * @param number $quality
     * 形状： shape
     * 正方形: square
     * 长方形: rectangle
     */
    static public function imagecropping($path,$fileName,$cut_x=0,$cut_y=0,$dst_w=400,$dst_h=400,$quality=90,$shape='square'){
        $aryFile = explode('.', $fileName);
        $image = $path.'/'.$fileName;
        list($src_w,$src_h) = getimagesize($image); // 获取原图尺寸
        if($aryFile[1] == 'jpeg' || $aryFile[1] == 'jpg'){
            $source = imagecreatefromjpeg($image);
        }elseif($aryFile[1] == 'png'){
            $source = imagecreatefrompng($image);
        }else{
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2028,'图片格式不正确，只能上传jpg、png格式！');
        }
        // 剪裁
        
        $croped = imagecreatetruecolor($dst_w, $dst_h);

        imagecopy($croped,$source,0,0,$cut_x,$cut_y,$dst_w,$dst_h);
        
        if($shape == 'square'){
            // 缩放生成120X120的图
            $img120 = imagecreatetruecolor(120, 120);
            $color=imagecolorallocate($img120,255,255,255);
            imagecolortransparent($img120,$color);
            imagefill($img120,0,0,$color);
            imagecopyresampled($img120,$croped,0,0,0,0,120,120,$dst_w,$dst_h);

            // 缩放生成60X60的图
            $img60 = imagecreatetruecolor(60, 60);
            $color=imagecolorallocate($img60,255,255,255);
            imagecolortransparent($img60,$color);
            imagefill($img60,0,0,$color);
            imagecopyresampled($img60,$croped,0,0,0,0,60,60,$dst_w,$dst_h);
            
            // 保存
            switch (strtolower($aryFile[1])){
                case 'jpg':
                case 'jpeg':
                    imagejpeg($img120, $path.'/'.$aryFile[0].'.120.small.jpg',$quality);
                    imagejpeg($img60, $path.'/'.$aryFile[0].'.60.small.jpg',$quality);
                    return array(
                        'img60' => $path.'/'.$aryFile[0].'.60.small.jpg',
                        'img120' => $path.'/'.$aryFile[0].'.120.small.jpg'
                    );
                break;
                case 'png':
                    imagepng($img120, $path.'/'.$aryFile[0].'.120.small.png');
                    imagepng($img60, $path.'/'.$aryFile[0].'.60.small.png',$quality/10);
                    return array(
                        'img60' => $path.'/'.$aryFile[0].'.60.small.png',
                        'img120' => $path.'/'.$aryFile[0].'.120.small.png'
                    );
                break;
                case 'gif':
                    imagegif($img120, $path.'/'.$aryFile[0].'.120.small.gif');
                    imagegif($img60, $path.'/'.$aryFile[0].'.60.small.gif',$quality);
                    return array(
                        'img60' => $path.'/'.$aryFile[0].'.60.small.jpg',
                        'img120' => $path.'/'.$aryFile[0].'.120.small.jpg'
                    );
                break;
            }
        }else{
            // 缩放生成60X60的图
            $img450 = imagecreatetruecolor(450, 200);
            $color=imagecolorallocate($img450,255,255,255);
            imagecolortransparent($img450,$color);
            imagefill($img450,0,0,$color);
            imagecopyresampled($img450,$croped,0,0,0,0,450,200,$dst_w,$dst_h);
            // 保存
            switch (strtolower($aryFile[1])){
                case 'jpg':
                case 'jpeg':
                    imagejpeg($img450, $path.'/'.$aryFile[0].'.450.small.jpg',$quality);
                    return array(
                        'img450' => $path.'/'.$aryFile[0].'.450.small.jpg'
                    );
                break;
                case 'png':
                    imagepng($img450, $path.'/'.$aryFile[0].'.450.small.png',$quality/10);
                    return array(
                        'img450' => $path.'/'.$aryFile[0].'.450.small.png'
                    );
                break;
                case 'gif':
                    imagegif($img450, $path.'/'.$aryFile[0].'.450.small.gif',$quality);
                    return array(
                        'img450' => $path.'/'.$aryFile[0].'.450.small.jpg'
                    );
                break;
            }
        } 
    }
    
    
    /**
     * 校验邮箱
     * @return boolean
     */
    static public function checkEmail($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }
        return false;
    }
    
    /**
     * 验证手机号
     * @param unknown $mobilephone
     * @return boolean
     */
    static public function checkMobilePhone($mobilephone){
        if(preg_match("/^13[0-9][0-9]{8}$|15[0-9][0-9]{8}$|18[0-9][0-9]{8}$/",$mobilephone)){
            return true;
        }
        return false;
    }
    static function getCategoryList($sm, $aWhere=array()){
        return self::getResourceList($sm, array('source'=>'category', 'pkfield'=>'id', 'labelfield'=>'title'), $aWhere);
    }
    

    static function getJobsId($sMd5Key){
        exec("ps gaux | grep '".$sMd5Key."' | grep -v grep | awk '{print $2}'", $aRunningjobs);
        return count($aRunningjobs) ? $aRunningjobs : false;
    }
}