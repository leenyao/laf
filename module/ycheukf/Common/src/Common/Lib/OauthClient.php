<?php
namespace YcheukfCommon\Lib;


class OauthClient
{


	static $dbproxy_access_token=null;
	/**
	* request url by curl
	*/


	/**
	* check access_token
	*/
	static function checkAccess($sm, $access_token){
		$aConfig = $sm->get("Config");
        $aParams = array(
            'op'=> 'checkaccess',
            'resource'=> 'user',
//            'resource'=> 'task',
            'resource_type'=> 'procedures',
            'access_token' => "",
            'params' => array(
                'access_token' => $access_token,
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        if(($aResult['status'] == 1)){//成功登陆
            return true;
        }else{
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2048);
        }
	}
	static function setDbProxyAccessToken($sToken){
		self::$dbproxy_access_token = $sToken;
    }

    //TBD
	static function getDbProxyAccessToken($sm){
        $sToken = "whatever";
        self::setDbProxyAccessToken($sToken);
		self::$dbproxy_access_token = $sToken;
		return $sToken;

	}
	static function getUserAccessToken($sm){
		return $sm->get('Zend\Session\SessionManager')->getStorage()->user_token;
	}

	static function loginByParams($client_id, $client_secret, $username, $password, $grant_type='password', $login_url=null, $sp=null, $sType="function", $sm=null){
        switch($sType){
            case "function":
                $aParams = array(
                    'op'=> 'login',
                    'resource'=> 'user',
        //            'resource'=> 'task',
                    'resource_type'=> 'procedures',
                    'access_token' => "",
                    'params' => array(
                        'grant_type' => $grant_type,
                        'client_id' => $client_id,
                        'client_secret' => $client_secret,
                        'username' => $username,
                        'password' => $password,
                        'sp' => $sp,
                    ),
                );
        //        var_dump($access_token);
                $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
                if(($aResult['status'] == 1)){//成功登陆
                    return $aResult['data']['access_token'];
                }
                break;
            case "curl":
                $post_data = array();
                $post_data['grant_type'] = $grant_type;
                $post_data['client_id'] = $client_id;
                $post_data['client_secret'] = $client_secret;
                $post_data['username'] = $username;
                $post_data['password'] = $password;
                if(!is_null($sp))
                    $post_data['sp'] = $sp;
                $o="";
                foreach ($post_data as $k=>$v)
                {
                    $o.= "$k=".urlencode($v)."&";
                }
                $post_data=substr($o,0,-1);


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                curl_setopt($ch, CURLOPT_POST, TRUE); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); 
                curl_setopt($ch, CURLOPT_URL, $login_url);
                $result =curl_exec($ch);
                curl_close($ch);
                
                \YcheukfCommon\Lib\Functions::debug(array($login_url, $post_data, $result), __CLASS__."::".__FUNCTION__);
                $aJson = json_decode($result, 1);
                if(is_null($aJson))
                    \YcheukfCommon\Lib\Functions::throwErrorByCode(2014);

        //		self::$access_token =  $aJson['data'];
                if((int)$aJson['status'] == 1)
                    return $aJson['data'];
                else
                    \YcheukfCommon\Lib\Functions::throwErrorByCode($aJson['status'], $aJson['msg']);
            break;
            default:
                break;
        }
	}
	static function getUserIdByToken($sm, $sToken=""){
        
        $a = \YcheukfCommon\Lib\Functions::getResourceById($sm, 'oauthtokens', $sToken, 'access_token');
        return $a['user_id'];
    }

}