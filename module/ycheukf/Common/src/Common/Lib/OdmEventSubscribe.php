<?php

namespace YcheukfCommon\Lib;

class OdmEventSubscribe implements \Doctrine\Common\EventSubscriber
{

    public function preSelectDatabase($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()), "[inline]---[mongodb]---event");
    }
    public function postSelectDatabase($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, get_class($eventArgs->getData())), "[inline]---[mongodb]---event");
    }


    public function preSelectCollection($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()), "[inline]---[mongodb]---event");
    }
    public function postSelectCollection($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, get_class($eventArgs->getData())), "[inline]---[mongodb]---event");
    }

    public function collectionPreInsert($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()), "[inline]---[mongodb]---event");
    }
    public function collectionPostInsert($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()), "[inline]---[mongodb]---event");
    }

    public function collectionPreFind($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()), "[inline]---[mongodb]---event");
    }
    public function collectionPostFind($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()->info()), "[inline]---[mongodb]---event");
    }

    public function collectionPreFindAndRemove($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()), "[inline]---[mongodb]---event");
    }
    public function collectionPostFindAndRemove($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()), "[inline]---[mongodb]---event");
    }


    public function collectionPreBatchInsert($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()), "[inline]---[mongodb]---event");
    }
    public function collectionPostBatchInsert($eventArgs)
    {
		\YcheukfCommon\Lib\Functions::debug(array(__FUNCTION__, $eventArgs->getData()), "[inline]---[mongodb]---event");
    }





	function __call($name,$arguments) {  
//		var_dump($name);
//		var_dump($arguments);
		\YcheukfCommon\Lib\Functions::debug($name, "[inline]---[mongodb]---event");
	}
    public function getSubscribedEvents()
    {
        return array(
			\Doctrine\MongoDB\Events::preFind,
			\Doctrine\MongoDB\Events::postFind,
			\Doctrine\MongoDB\Events::preInsert,
			\Doctrine\MongoDB\Events::postInsert,
			\Doctrine\MongoDB\Events::preUpdate,
			\Doctrine\MongoDB\Events::postUpdate,
			\Doctrine\MongoDB\Events::preRemove,
			\Doctrine\MongoDB\Events::postRemove,
			\Doctrine\MongoDB\Events::preBatchInsert,
			\Doctrine\MongoDB\Events::postBatchInsert,
			\Doctrine\MongoDB\Events::preSave,
			\Doctrine\MongoDB\Events::postSave,
			\Doctrine\MongoDB\Events::preFindOne,
			\Doctrine\MongoDB\Events::postFindOne,
			\Doctrine\MongoDB\Events::preFindAndRemove,
			\Doctrine\MongoDB\Events::postFindAndRemove,
			\Doctrine\MongoDB\Events::preFindAndUpdate,
			\Doctrine\MongoDB\Events::postFindAndUpdate,
			\Doctrine\MongoDB\Events::preGroup,
			\Doctrine\MongoDB\Events::postGroup,
			\Doctrine\MongoDB\Events::preGetDBRef,
			\Doctrine\MongoDB\Events::postGetDBRef,
			\Doctrine\MongoDB\Events::preCreateDBRef,
			\Doctrine\MongoDB\Events::postCreateDBRef,
			\Doctrine\MongoDB\Events::preDistinct,
			\Doctrine\MongoDB\Events::postDistinct,
			\Doctrine\MongoDB\Events::preMapReduce,
			\Doctrine\MongoDB\Events::postMapReduce,
			\Doctrine\MongoDB\Events::preNear,
			\Doctrine\MongoDB\Events::postNear,
			\Doctrine\MongoDB\Events::preCreateCollection,
			\Doctrine\MongoDB\Events::postCreateCollection,
			\Doctrine\MongoDB\Events::preSelectDatabase,
			\Doctrine\MongoDB\Events::postSelectDatabase,
			\Doctrine\MongoDB\Events::preDropDatabase,
			\Doctrine\MongoDB\Events::postDropDatabase,
			\Doctrine\MongoDB\Events::preSelectCollection,
			\Doctrine\MongoDB\Events::postSelectCollection,
			\Doctrine\MongoDB\Events::preDropCollection,
			\Doctrine\MongoDB\Events::postDropCollection,
			\Doctrine\MongoDB\Events::preGetGridFS,
			\Doctrine\MongoDB\Events::postGetGridFS,
			\Doctrine\MongoDB\Events::preConnect,
			\Doctrine\MongoDB\Events::postConnect,
		);
    }
}