<?php
namespace YcheukfCommon\Lib;

class Restapi{
    var $sm;
    var $dbAccessToken;
    var $nPnPage=1;
    var $nPnOrder="";
    var $nPnPerPage=1000;
    var $sAccessToken="";
    var $sUserId="";
    var $aConfig="";
    var $sResource="";
    var $sRunType="";
    var $bCheckToken=true;
    var $eventIdentifier=null;
    var $events=null;
    var $bInitFlag=false;
    var $sAdrServerType=null;
    var $nAdrServerId=null;
    function __construct($sm=null){
        if(is_null($sm)){
            global $oGlobalFramework;
            $this->sm = $oGlobalFramework->sm;
            $this->sRunType = 'curl';
        }else{
            $this->sm = $sm;
            $this->sRunType = 'function';
            $this->bCheckToken = false;
        }
        $this->dbAccessToken = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sm);

        //不要使用LAFCACHE
        \YcheukfCommon\Lib\Functions::setUseLafCacheFlag(false);
    }

    function __get($name='')
    {
        switch ($name) {
            case 'serviceManager':
                return $this->sm;
                break;
            default:
                break;
        }
    }
    
    function responce($nCode, $data){
        if($this->sRunType == 'curl'){
            \ToroHook::fire($nCode, $data);
            exit;
        }else{
            return array($nCode, $data);
        }
    }
    function _init($sResource, $bCheckToken=true, $bCheckAdrId=false){
        if ($this->bInitFlag === true) {
            return;
        }else{
            $this->bInitFlag = true;
        }

        $aRestConfig = array();
        $this->aConfig = $this->sm->get('config');
        $this->sResource = $sResource;
        if(isset($_GET['page']))
            $this->nPnPage = $_GET['page'];
        if(isset($_GET['per_page']))
            $this->nPnPerPage = $_GET['per_page'];
        if(isset($_GET['orderby']))
            $this->nPnOrder = $_GET['orderby'];
        if($this->bCheckToken && $bCheckToken){
            
            if(isset($_GET['token_t']) && isset($_GET['token_v'])){//验证动态密码
                $bDyncLyChkTokenFlag =  isset($this->aConfig['bDyncLyChkTokenFlag']) ? $this->aConfig['bDyncLyChkTokenFlag'] : true;

                if ($bDyncLyChkTokenFlag && ($_GET['token_t']>(time()+120) || $_GET['token_t']<(time()-600))) {//时间和服务器对不上报错
                    return $this->responce(401, " Authorization faild, wrong token_t  ".$_GET['token_t']." vs". time());
                }
                if($bDyncLyChkTokenFlag && !\YcheukfCommon\Lib\Functions::chkLyDynamicToken($_GET['token_t'],  $_GET['token_v'])){
                    return $this->responce(401, "dynamic token Authorization faild");
                }
                // var_dump($bDyncLyChkTokenFlag);

            }elseif($this->sm->get('request')->getHeaders()->has('Authorization')){
                $sAuthorization = $this->sm->get('request')->getHeaders()->get('Authorization')->value;
                if(preg_match("/token /i", $sAuthorization)){
                    if(empty($sAuthorization)){
                        return $this->responce(500, "token can not be empty");
                    }
                    $this->sAccessToken = str_replace("token ", "", $sAuthorization);
                    try{
                        \YcheukfCommon\Lib\OauthClient::checkAccess($this->sm, $this->sAccessToken);
                        $this->sUserId = \YcheukfCommon\Lib\OauthClient::getUserIdByToken($this->sm, $this->sAccessToken);
//                        var_dump($this->sUserId);

                    }catch(\YcheukfCommon\Lib\Exception $e){
                        return $this->responce(401, $e->getMessage());
                    }
                }else{
                    return $this->responce(401, "need Authorization 1");
                }
            }else{
                return $this->responce(401, "need Authorization 2");
            }
        }
        if ($bCheckAdrId) {
            if(!isset($_GET['i']) || !isset($_GET['ei'])){//验证动态密码
                return $this->responce(401, "need i,ei");
            }
            if (\YcheukfCommon\Lib\Functions::encryptBySlat($_GET['i']) !== $_GET['ei']) {
                return $this->responce(401, "unavailable adrid");
            }

            $this->sAdrServerType = $this->sm->get('\Lur\Service\Common')->getAdrServerType($_GET['i']);
            //找不到
            if (is_null($this->sAdrServerType) || $this->sAdrServerType==false) {
                return $this->responce(500, "no such adrserver");
            }
            $this->nAdrServerId = intval($_GET['i']);

            $this->sm->get('\Lur\Service\Addtional')->add2AliveList($this->nAdrServerId);

        }
        if(!isset($this->aConfig['apiservice']['resources'][$sResource]) && !isset($this->aConfig['apiservice']['customer'][$sResource])){
            return $this->responce(404, "no such resource:".$sResource);
        }
        if(!isset($this->aConfig['apiservice']['customer'][$sResource])){
            return $this->responce(403, "permission denied:".$sResource);
        }
    }


    function _fmtPostData($aApidocs, $aData, $sKey){
        $aReturn = array();
        $aApidocs = $aApidocs[$sKey]['d:entity'];
        // print_r($aApidocs);
        foreach($aApidocs as $k1=>$v1){
            if(isset($v1['d:post-required']) && $v1['d:post-required']){
                if(isset($aData[$k1])){
                    $sKeyName = isset($v1['d:alias']) ? $v1['d:alias'] : $k1;
                    if($v1["d:type"] == "list-entity"){
                        $aTmp = array();
                        foreach($aData[$k1] as $aTmp2){
                            $aTmp[] = $this->_fmtPostData($aApidocs, $aTmp2, $k1);
                        }
                        $aReturn[$sKeyName] = $aTmp;
                    }else{
                        $aReturn[$sKeyName] = $aData[$k1];
                    }
                }else{
                    return $this->responce(500, "required key => {$sKey}.{$k1}");
                }
            }
        }
        return $aReturn;
    }
    function doDelete($sResource, $sUpdate, $sUserId, $sId){
        if(empty($sId)){
            return $this->responce(500, "token can not be empty");
        }
        if(empty($sUserId)){
            return $this->responce(500, "userid can not be empty");
        }
        $aParams = array(
            'op'=> 'save',
            'resource'=> $sResource,
            'resource_type'=> 'common',
            'access_token' => $this->dbAccessToken,
            'params' => array(
                'dataset' => array (
                    $sUpdate,
                ),
                'where'=>array('id'=>$sId, 'user_id'=>$sUserId),
            ),
        );
        $aReturn = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
        if($aReturn['status'] == 1){
            return $this->responce(200, $aReturn['data']);
        }else{
            return $this->responce(500, json_encode($aReturn));
        }
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param  EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(\Zend\EventManager\EventManagerInterface $events)
    {
        $this->events = $events;
        return $this;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (!$this->events instanceof EventManagerInterface) {
            $identifiers = array(__CLASS__, get_called_class());
            if (isset($this->eventIdentifier)) {
                if ((is_string($this->eventIdentifier))
                    || (is_array($this->eventIdentifier))
                    || ($this->eventIdentifier instanceof Traversable)
                ) {
                    $identifiers = array_unique($identifiers + (array) $this->eventIdentifier);
                } elseif (is_object($this->eventIdentifier)) {
                    $identifiers[] = $this->eventIdentifier;
                }
                // silently ignore invalid eventIdentifier types
            }
            $this->setEventManager(new \Zend\EventManager\EventManager($identifiers));
        }
        return $this->events;
    }
    function _fmtGetOutput($sDocKey, $aDocConf, $aApiConfigs){

        $sResource = isset($aApiConfigs[$sDocKey]['d:resource']) ? $aApiConfigs[$sDocKey]['d:resource'] : (isset($aDocConf['resource']) ? $aDocConf['resource'] : null);

        if(is_null($sResource)){
            $aReturn = array();
            if(isset($aDocConf['child']) && count($aDocConf['child'])){
                $aRowTmp = array();
                foreach($aDocConf['child'] as $k1 => $a1){
                    list($aRowTmp[$k1], $nTotalTmp) = $this->_fmtGetOutput($k1, $a1, $aApiConfigs[$sDocKey]['d:entity']);
                }
                $aReturn[] = $aRowTmp;
                return array($aReturn, count($aReturn));
            }
        }
        $aParams = array(
            'op'=> 'get',
            'resource'=> $sResource,
            'resource_type'=> 'common',
            'access_token' => $this->dbAccessToken,
            'params' => array(
                'select' => $aDocConf['select'],
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
//        var_dump($aResult);
//        var_dump("#########");
        if($aResult['status'] == 1){
            $aReturn = array();
            foreach($aResult['data']['dataset'] as $aRowTmp){
                $aRowTmp = $this->_fmt4RestApi($aRowTmp, $aApiConfigs[$sDocKey]['d:entity']);
                if(isset($aDocConf['child']) && count($aDocConf['child'])){
                    foreach($aDocConf['child'] as $k1 => $a1){
                        if(isset($a1['select']['where']) && count($a1['select']['where'])){
                            foreach($a1['select']['where'] as $k2 => $v2){
                                if(preg_match("/\[=field=\]/i", $v2)){
                                    $sFieldTmp = str_replace('[=field=]', '', $v2);
                                    $a1['select']['where'][$k2] = $aRowTmp[$sFieldTmp];
                                }
                            }
                        }
                        list($aRowTmp[$k1], $nTotalTmp) = $this->_fmtGetOutput($k1, $a1, $aApiConfigs[$sDocKey]['d:entity']);
                    }
                }
                $aReturn[] = $aRowTmp;
            }
            return array($aReturn, $aResult['data']['count']);
        }else{
            return $this->responce(500, json_encode($aResult));
        }
    }
    protected function __trigger_fmtReturnValue($value='')
    {
        return $value;
    }
    function returnGet($aDocsConf, $sId="", $aCache=array(), $bMd5Flag=0, $bUpdateCacheflag=false){
        $aReturn = array();
        $bCache = false;
        $sCacheTime = "";//缓存生成时间
        if(empty($bUpdateCacheflag) && count($aCache) && $aCacheData = \YcheukfCommon\Lib\Functions::getCache($this->sm, $aCache['cachekey'])){//找到缓存
            $aReturn = $aCacheData[0];
            $nTotal = $aCacheData[1];
            $sCacheTime = isset($aCacheData[2]) ? $aCacheData[2] : "";
            $bCache = true;
            // \YcheukfCommon\Lib\Functions::onlinedebug('restapi-cache-'.key($aDocsConf), $sId);
        }else{
            // \YcheukfCommon\Lib\Functions::onlinedebug('restapi-'.key($aDocsConf), $aCache['cachekey']);
            foreach($aDocsConf as $k1 => $a1){
                list($aReturn, $nTotal) = $this->_fmtGetOutput($k1, $a1, $this->aConfig['apiservice']['customer']);
            }
        }
        $aReturn = $this->__trigger_fmtReturnValue($aReturn);
        if(count($aCache) && $bCache==false){//写缓存
            \YcheukfCommon\Lib\Functions::saveCache($this->sm, $aCache['cachekey'], array($aReturn, $nTotal, (empty($sCacheTime) ? date("YmdHis") : $sCacheTime)), (isset($aCache['expiredtime']) ? $aCache['expiredtime'] : 3600));
        }
        if ($bMd5Flag==1) {
            return $this->responce(200, json_encode(!empty($sId) ? (count($aReturn) ? $aReturn[0] : array()) : array('md5'=>md5(json_encode($aReturn)), 'count'=>$nTotal, 'laf-cacheflag'=>$bCache, 'laf-cachetime'=>$sCacheTime)));
        }else{
            return $this->responce(200, json_encode(!empty($sId) ? (count($aReturn) ? $aReturn[0] : array()) : array('dataset'=>$aReturn, 
                'md5'=>md5(json_encode($aReturn)), 
                'count'=>$nTotal, 
                'laf-cachekey'=>isset($aCache['cachekey']) ? $aCache['cachekey']:"", 
                'laf-cacheflag'=>$bCache, 
                'laf-cachetime'=>$sCacheTime
            )));
        }
    }
    function _savePost($aPostData, $sSafeConfigKey, $aSaveConf, $aDocsConfig, $sFid=null){
        $aDocsConfig = $aDocsConfig[$sSafeConfigKey]['d:entity'];
        $aUpdData = array();
        foreach($aPostData as $k1=>$v1){
            if(isset($aDocsConfig[$k1]) && 'list-entity'==$aDocsConfig[$k1]['d:type']){
            }else{
                $aUpdData[$k1]= $v1;
            }
        }
        if(isset($aSaveConf['select']['dataset']) && count($aSaveConf['select']['dataset'])){
           foreach($aSaveConf['select']['dataset'] as $k1=>$v1){
               $sFieldTmp = $v1;
                if(preg_match("/\[=fatherid=\]/i", $v1)){
                    $sFieldTmp = $sFid;
                }
               $aUpdData[$k1] = $sFieldTmp;
           }
//           var_dump($aUpdData);
        }
        $aParams = array(
            'op'=> 'save',
            'resource'=> $aSaveConf['resource'],
            'resource_type'=> 'common',
            'access_token' => $this->dbAccessToken,
            'params' => array(
                'dataset' => array (
                    $aUpdData,
                ),
            ),
        );
        if(!empty($aSaveConf['select']['where']))
            $aParams['params']['where'] = $aSaveConf['select']['where'];
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
        if($aResult['status'] == 1){
            $nNewId = $aResult['data'];
        }
        foreach($aPostData as $k1=>$v1){
            if(isset($aDocsConfig[$k1]) && 'list-entity'==$aDocsConfig[$k1]['d:type']){
                if(!is_null($nNewId)){//删除子记录再重新插入
                    $aDeleteWhere = array();
                    $aSaveConfChild = $aSaveConf['child'][$k1];
                   foreach($aSaveConfChild['select']['dataset'] as $k2=>$v2){
                        if(preg_match("/\[=fatherid=\]/i", $v2)){
                            $aDeleteWhere[$k2] = $nNewId;
                        }
                   }
                   if(count($aDeleteWhere)){
//                       var_dump($aSaveConfChild['resource']);
//                       var_dump($aDeleteWhere);
//                       var_dump($aSaveConfChild['delete_dataset']);
                        $aParams = array(
                            'op'=> 'save',
                            'resource'=> $aSaveConfChild['resource'],
                            'resource_type'=> 'common',
                            'access_token' => $this->dbAccessToken,
                            'params' => array(
                                'dataset' => array (
                                    $aSaveConfChild['delete_dataset'],
                                ),
                                'where' => $aDeleteWhere,
                            ),
                        );
                        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
                   }
                }
                foreach($v1 as $v2){
                    $this->_savePost($v2, $k1, $aSaveConfChild, $aDocsConfig, $nNewId);
                }
            }
        }
        return $nNewId;
    }
    function doSave($aPostData, $aSaveConf){
        $oConnection = $this->sm->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection();

        $this->sm->get('Zend\Db\Adapter\Adapter')->query('set global transaction isolation level read uncommitted')->execute();
        $this->sm->get('Zend\Db\Adapter\Adapter')->query('set session transaction isolation level read uncommitted')->execute();

        $oConnection->beginTransaction();
        try{
            $sId = null;
            foreach($aSaveConf as $k1 => $a1){
                $sId = $this->_savePost($aPostData, $k1, $a1, $this->aConfig['apiservice']['customer']);
                $this->getEventManager()->trigger('lf_record_insert', $this->sm, array($this->sResource, $a1, $sId),function ($r) {return true;});
            }
            $oConnection->commit();

            if(is_null($sId)){
                return $this->responce(500, "save data error");
            }else
                return $this->get($this->sResource, $sId);
        }catch (Exception $e) {
            $oConnection->rollBack();
            \YcheukfCommon\Lib\Functions::throwErrorByCode(2033, null, array("[=replacement1=]" => $e->getMessage()));
        }
    }
    function _fmt4RestApi($a, $aDescription) {
        $aReturn = array();
        if(count($a)){
            $row2 = array();
            foreach($aDescription as $sKey => $row3){
                $sKeyfield = (isset($row3['d:alias']))?$row3['d:alias'] : $sKey;
                $sValue = isset($a[$sKeyfield]) ? $a[$sKeyfield] : null;
                switch($row3['d:type']){
                    case 'int':    
                        $row2[$sKey] = intval($sValue);
                    break;
                    case 'float':    
                        $row2[$sKey] = floatval($sValue);
                    break;
                    case 'bool':    
                        $row2[$sKey] = ($sValue==1) ? true :false;
                    break;
                    case 'json_string':    
                        $row2[$sKey] = json_decode($sValue, 1);
                    break;
                    default:
                        $row2[$sKey] = $sValue;
                    break;
                }
                if(isset($row3['d:urlflag']) && $row3['d:urlflag'])
                    $row2[$sKey] = \YcheukfCommon\Lib\Functions::getImgBaseUrl($this->sm).$row2[$sKey];
            }
            $aReturn = $row2;
        }
        return $aReturn;
    }
    function doPost($sResource=null, $sId="", $bCheckAdrId=false) {
        $this->_init($sResource, true, $bCheckAdrId);
        $postdata = file_get_contents("php://input"); 
        $postdata = trim($postdata);
        // var_dump($postdata);
        if (!empty($postdata)) {
            $aPostData = json_decode($postdata, 1);
            if(!isset($postdata) || is_null($aPostData)){
                return $this->responce(500, "error param:".$postdata);
            }
            return $this->_postConfig($sResource, $sId, $aPostData);
        }
    }
    function doPut($sResource=null, $sId="", $bCheckAdrId=false) {
        $this->_init($sResource, true, $bCheckAdrId);
        if(empty($sId)){
            return $this->responce(500, "token can not be empty");
        }
        $putfp = fopen('php://input', 'r');
        $putdata = '';
        while($dataTmp = fread($putfp, 1024))
            $putdata .= $dataTmp;
        fclose($putfp);
        if(!empty($putdata)){
            parse_str($putdata);
            $aPostData = json_decode($putdata, 1);
            if(!is_null($aPostData)){
                return $this->_postConfig($sResource, $sId, $aPostData);
            }else{
                return $this->responce(500, "put data error:".$data);
            }
        }else{
            return $this->responce(500, "put data can not be empty");
        }
    }
}
