<?php
namespace YcheukfCommon\Lib;

class SolrClient extends \SolrClient\Client\Client{

    /**
     * Commit changes
     *
     * @param bool $waitFlush Block until index changes are flushed to disk
     * @param bool $waitSearcher Block until a new searcher is opened and registered as the main query searcher, making the changes visible
     */
    public function commit($optimize = false, $waitFlush = true, $waitSearcher = true) {

        $wf = $waitFlush ? 'true' : 'false';
        $ws = $waitSearcher ? 'true' : 'false';
        $op = ($optimize) ? 'true' : 'false';
        $rawPost = '<commit  waitSearcher="' . $ws . '"  />';

        $this->rawPost($rawPost);
    }

    /**
     * Optimize solr
     *
     * @param bool $waitFlush
     * @param bool $waitSearcher
     * @param int $maxSegments
     */
    public function optimize($waitFlush = true, $waitSearcher = true, $maxSegments = 1) {
        $wf = $waitFlush ? 'true' : 'false';
        $ws = $waitSearcher ? 'true' : 'false';

        $rawPost = '<optimize  waitSearcher="' . $ws . '" maxSegments="' . $maxSegments . '" />';

        $this->rawPost($rawPost);
    }
}
