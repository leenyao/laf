<?php
namespace YcheukfCommon\Lib\ZfcUser;

use Ucenter\ucenter;

use Zend\Authentication\Result as AuthenticationResult;
use Zend\Session\Container as SessionContainer;
use ZfcUser\Authentication\Adapter\AdapterChainEvent as AuthEvent;
use Zend\Stdlib\ResponseInterface as Response;
class AuthAdapter extends \ZfcUser\Authentication\Adapter\Db{

    public function authenticate(AuthEvent $e){
//        $e->stopPropagation();//停止事件冒泡
        if ($this->isSatisfied()) {
            $storage = $this->getStorage()->read();
            $e->setIdentity($storage['identity'])
              ->setCode(AuthenticationResult::SUCCESS)
              ->setMessages(array('Authentication successful.'));
            return;
        }

        $identity   = $e->getRequest()->getPost()->get('identity');
        $credential = $e->getRequest()->getPost()->get('credential');
        $sp = $e->getRequest()->getPost()->get('sp', null);
        $credential = $this->preProcessCredential($credential);
        $userObject = NULL;

        try{
            $aConfig = $this->getServiceManager()->get('config');
            $access_token = \YcheukfCommon\Lib\OauthClient::loginByParams(
                $aConfig['oauth_client_id']['default'],
                "",
                $identity, $credential,
                'password',
                "",
                $sp,
                "function",
                $this->getServiceManager()
            );
        }catch(\YcheukfCommon\Lib\Exception $exc){
            $sTmp = $exc->getMessage();
            $e->setCode(AuthenticationResult::FAILURE_CREDENTIAL_INVALID)->setMessages(array($sTmp));
            $this->setSatisfied(false);
            return false;
        }

        $sUserId = 0;

        $this->getServiceManager()->get('Zend\Session\SessionManager')->getStorage()->user_token = $access_token;
        $access_token = \YcheukfCommon\Lib\OauthClient::getUserAccessToken($this->getServiceManager());
        $sUserId = $this->chkUserExists($identity, $this->getServiceManager());
        if(!$sUserId){
            $e->setMessages(array("该用户不存在"));
            $this->setSatisfied(false);
            return false;
        }
        $aUserData =  \Application\Model\Common::getResourceById($this->getServiceManager(), 'user', $sUserId);
        if(isset($aUserData['status']) && $aUserData['status']!=1){
            $e->setMessages(array("该用户已被禁止登陆"));
            $this->setSatisfied(false);
            return false;
        }

        // Success!
        $e->setIdentity($sUserId);
        // Update user's password hash if the cost parameter has changed
//        $this->updateUserPasswordHash($userObject, $credential, $bcrypt);
        $this->setSatisfied(true);
        $storage = $this->getStorage()->read();
        $storage['identity'] = $e->getIdentity();
        $this->getStorage()->write($storage);

        
        $e->setCode(AuthenticationResult::SUCCESS)
          ->setMessages(array('Authentication successful.'));



          $sParamString = "identity=$identity&credential=$credential";
          $aParams = array(
              'op'=> 'save',
              'resource'=> 'userlog',
              'resource_type'=> 'common',
              'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceManager()),
              'params' => array('dataset'=>array(array(
                  'user_id'=> $sUserId,
                  'serverinfo'=> json_encode($_SERVER),
                  'url'=> "/user/login",
                  'param'=> urldecode($sParamString),
              ))),
          );
          $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->getServiceManager(), $aParams, 'function');

        //登陆成功后触发
        if($this->getServiceManager()->has('eventmanager')){
            $this->getServiceManager()->get('eventmanager')->trigger('lf_user_afterauthenticate', $this->getServiceManager(), array($sUserId),function ($r) {return true;});
        }



        //清理登陆的信息
//        $sCacheMd5Id = '20001_'.$sUserId;
//        \YcheukfCommon\Lib\Functions::delCache($this->getServiceManager(), $sCacheMd5Id);
//        $sCacheMd5Id = '20002_'.$sUserId;
//        \YcheukfCommon\Lib\Functions::delCache($this->getServiceManager(), $sCacheMd5Id);
//        $sCacheMd5Id = '20003_1_'.$sUserId;
//        \YcheukfCommon\Lib\Functions::delCache($this->getServiceManager(), $sCacheMd5Id);
//        $sCacheMd5Id = '20003_2_'.$sUserId;
//        \YcheukfCommon\Lib\Functions::delCache($this->getServiceManager(), $sCacheMd5Id);
        return true;
    }

    function chkUserExists($identity, $sm){

        $aParams = array(
            'op'=> 'get',
            'resource'=> 'user',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($sm),
            'params' => array(
                'select' => array (
                    "where" => array("username"=>$identity),
                ),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($sm, $aParams, 'function');
        if($aResult['data']['count'] != 1){
//            $sm->get('Zend\Session\SessionManager')->getStorage()->dbproxy_token = null;
            $sm->get('Zend\Session\SessionManager')->getStorage()->user_token = null;
            return false;
        }
        if( in_array($aResult['status'], array(2302))){    //登陆过期
            var_dump($aResult);
//            header("Location:".$sm->get('zendviewrendererphprenderer')->url('zfcuser/logout')."?redirect=".$sm->get('zendviewrendererphprenderer')->url('home'));
            exit;
        }
        return isset($aResult['data']['dataset'][0]['user_id']) ? $aResult['data']['dataset'][0]['user_id'] : false;
    }
}