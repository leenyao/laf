<?php
namespace YcheukTwbBundle\Form\Element;
use Zend\Form\Element\Select;

/**
 *
 * @author lenovo
 *
 */
class NotRequiredSelect extends Select
{
    /**
     * Provide default input rules for this element
     *
     * Attaches the captcha as a validator.
     *
     * @return array
     */
    public function getInputSpecification()
    {
        $spec = array(
            'name' => $this->getName(),
            'required' => false,
            'validators' => array(
                $this->getValidator()
            )
        );

        return $spec;
    }
}