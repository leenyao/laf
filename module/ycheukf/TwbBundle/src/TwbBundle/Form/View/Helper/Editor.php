<?php
namespace YcheukTwbBundle\Form\View\Helper;
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
use Zend\Form\ElementInterface;
use Zend\Form\Exception;



class Editor extends \Zend\Form\View\Helper\FormTextarea
{


    /**
     * Render a form <textarea> element from the provided $element
     *
     * @param  ElementInterface $element
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {
		$sEntities = ($element->getOption('entities'));
		$sViewType = (int)$element->getOption('view_type');
		$sTabLabels = $element->getOption('tabLabels');
		$sTabLabels = is_null($sTabLabels) ? array('1'=>'纯文本', '3'=>'单图文', '4'=>'多图文') : $sTabLabels;
		if(!isset($sTabLabels[$sViewType])){
			$aTmp = key($sTabLabels);
			$sViewType = $aTmp;
		}
		$sId = 'editor_'.uniqid();
		$aParams = array(
			'render_id' => $sId,
			'view_type' => $sViewType, 	
			'data' => $sEntities, 	
			'tabLabels' => $sTabLabels, 	
		);

		$sJson = json_encode($aParams);
		$sHTML  = <<<OUTPUT
			<div id='{$sId}' class='wysiswyg-editor'></div>
			<script>
			$(function() { /* global MsgContext */
				MsgContext.init({$sJson});
			});
			</script>
OUTPUT;
        return $sHTML;
    }
}
