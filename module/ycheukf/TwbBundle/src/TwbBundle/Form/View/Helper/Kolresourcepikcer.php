<?php

namespace YcheukTwbBundle\Form\View\Helper;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;



class Kolresourcepikcer extends \Zend\Form\View\Helper\FormTextarea
{

	    /**
	     * Render a form <textarea> element from the provided $element
	     *
	     * @param  ElementInterface $element
	     * @throws Exception\DomainException
	     * @return string
	     */
	    public function render(ElementInterface $element)
	    {

	    	$sHTML = "";
    		$aData = $element->getOption("data");
    		$sStep = $element->getOption("step");
	    	$sHTML .= "<input type=hidden name='form_step' value=".$sStep.">";
    		// var_dump($aOptions);
    		switch (intval($sStep)) {
    			case 3:
    				$nIndex = 0;
    				$nPrice1 = 0;
    				$nPrice2 = 0;
		    		foreach ($aData as $key => $aRow) {
		    			$nIndex++;
			    		$nSelectedType = 1; //发放方式
		    			if (isset($aRow['order'])) {
		    				$nSelectedType = $aRow['order']['d11'];
		    			}
		    			$aSchedule = array();
		    			if (isset($aRow['schedule'])) {
		    				asort($aRow['schedule']);
		    				foreach ($aRow['schedule'] as $value) {
		    					$aSchedule[] = date("Y-m-d", strtotime($value));
		    				}
		    			}
		    			$nScheduleCount = count($aSchedule);
		    			$sSchedule = join(",", $aSchedule);
		    			$sSelected1 = $nSelectedType==1 ? "selected" : "";
		    			$sSelected2 = $nSelectedType==2 ? "selected" : "";
		    			$sResourceId = $aRow['resource']['id'];
		    			$sParamName = "orderdata";
		    			$nAdjustvalue = isset($aRow['adjustvalue']) && isset($aRow['adjustvalue']['internal_cost']) ? $aRow['adjustvalue']['internal_cost'] : $aRow['resource']['m2'];
		    			$sHTML .= <<<OUTPUT
		    			<h3>{$nIndex}: {$aRow['resource']['d1']}</h3>
		    			<input type=hidden name="{$sParamName}_id" value="{$sResourceId}">
		    			<h4>资源信息</h4>
		    			成本:{$aRow['resource']['m2']} / 投放天数: {$nScheduleCount} / 粉丝数:{$aRow['resource']['m1']} / 平均转发量:{$aRow['resource']['m4']} / 更新频率:{$aRow['resource']['m5']}  
		    			<h4>内部成本</h4>
		    			<input type=text class="input_adjustvalue" data-v1="{$nScheduleCount}" name="{$sParamName}_adjustvalue" value="{$nAdjustvalue}">

		    			<hr/>
OUTPUT;
	    				$nPrice1 += $aRow['resource']['m2'] * $nScheduleCount;
			    		$nPrice2 += $nAdjustvalue * $nScheduleCount;
		    		}
	    			$sHTML .= <<<OUTPUT
	    			<p>刊例成本: {$nPrice1}
	    			<p>内部成本: <span id="total2">{$nPrice2}</span>
	    			<script>
	    				\$(".input_adjustvalue").change(function(){
	    					var nTotal = 0;
	    					\$(".input_adjustvalue").each(function(){
	    						nTotal += \$(this).data("v1") * \$(this).val()
	    					})
	    					\$("#total2").text(nTotal)
	    				})
	    			</script>
OUTPUT;


    				break;
    			case 2:
    			default:

		    		foreach ($aData as $key => $aRow) {
			    		$nSelectedType = 1; //发放方式
		    			if (isset($aRow['order'])) {
		    				$nSelectedType = $aRow['order']['d11'];
		    			}
		    			$aSchedule = array();
		    			if (isset($aRow['schedule'])) {
		    				asort($aRow['schedule']);
		    				foreach ($aRow['schedule'] as $value) {
		    					// $aSchedule[] = date("Y-m-d", strtotime($value));
		    					$aSchedule[] = $value;
		    				}
		    			}
		    			$sScheduleJson = json_encode($aSchedule);
		    			$sSelected1 = $nSelectedType==1 ? "selected" : "";
		    			$sSelected2 = $nSelectedType==2 ? "selected" : "";
		    			$sResourceId = $aRow['resource']['id'];
		    			$sParamName = "orderdata";
		    			$sUniqid = "dp_".uniqid();

		    			// <input id="{$sUniqid}" data-inline="true" value="{$sSchedule}" type=text class="datepicker-here" data-language="zh" data-multiple-dates="true" name="{$sParamName}_schedule" data-multiple-dates-separator=";" />
		    			$sHTML .= <<<OUTPUT
		    			<h3>{$aRow['resource']['d1']}</h3>
		    			<input type=hidden name="{$sParamName}_id" value="{$sResourceId}">
		    			<p><p>
		    			<h4>排期(点击下面方框进行选择)</h4>
		    			<pre id="{$sUniqid}"></pre>
		    			<input type=hidden id="input_{$sUniqid}" name="{$sParamName}_schedule" value="{$sResourceId}">


		    			<hr/>
		    			<script>
		    			var date = new Date();
		    			var jSelected = []
		    			var jT1 = {$sScheduleJson}
			        	for (var i=0; i < jT1.length; i++) { 
			        		jSelected.push(new Date(jT1[i]))
			        	}
		    			new jDate('{$sUniqid}', {
		    			        date: {
		    			            type: jDate.Multi,
		    			            value: jSelected
		    			        },
		    			        change: function (value) {
		    			        	var a1 = [];
		    			        	for (var i=0; i < value.date.length; i++) { 
		    			        		var od1 = new Date(value.date[i]);

		    			        		var year = od1.getFullYear();
		    			        		var month = '0' + (od1.getMonth()+1);
		    			        		var day = '0' + od1.getDate();
		    			        		a1.push(year+"-"+month.slice(-2)+"-"+day.slice(-2))
		    			        	}
		    			            \$("#{$sUniqid}").html(a1.join("<br/>"));
		    			            \$("#input_{$sUniqid}").val(a1.join("; "));
		    			            console.log('value changed 2', value);
		    			        }
		    			    });
		    			</script>
OUTPUT;
		    		}


				break;
    		}
	        return $sHTML;
	    }
}