<?php
namespace YcheukTwbBundle\Form\View\Helper;
class TwbBundleFormButton extends \Zend\Form\View\Helper\FormButton{

	public function render(\Zend\Form\ElementInterface $oElement){

		$sValue = $oElement->getValue();
		$sTmp = "";
		$sElName = $oElement->getAttribute('data-elname');
		if(is_null($sElName))
			$oElement->setAttribute('data-elname', $oElement->getAttribute('name'));

//		data-elname
		$sButtonLabel = "";
		foreach($oElement->getAttributes() as $k=>$v){
			$sTmp .= $k."='".$v."' ";
			if($k=='name')
				$sButtonLabel = $v;
		}
		$sHTML  = <<<OUTPUT
			<div class="col-sm-offset-2 col-sm-10"><button class="btn btn-default" {$sTmp}>{$sButtonLabel}</button></div>
OUTPUT;
        return $sHTML;	
	}

}