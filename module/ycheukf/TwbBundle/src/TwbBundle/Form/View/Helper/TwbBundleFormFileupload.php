<?php
namespace YcheukTwbBundle\Form\View\Helper;
class TwbBundleFormFileupload extends \Zend\Form\View\Helper\FormFile{
    public function render(\Zend\Form\ElementInterface $oElement){

        $sValue = $oElement->getValue();
        $sTmp = "";
        $sElName = $oElement->getAttribute('data-elname');
        if(is_null($sElName))
            $oElement->setAttribute('data-elname', $oElement->getAttribute('name'));
        $oElement->setAttribute('class', $oElement->getAttribute('class')." upload-input");
        $sFType = $oElement->getAttribute('data-filetype');
        if(is_null($sFType))
            $oElement->setAttribute('data-filetype', 'pic');
        $sPicSize = $oElement->getOption('fileupload-pic-size');

//        $sExtraHtml = "";
//        if(!is_null($sPicSize)){
//            $sExtraHtml .= '<input type="hidden" name="fileupload-pic-size">'
//        }
//        data-elname
        foreach($oElement->getAttributes() as $k=>$v)
            $sTmp .= $k."='".$v."' ";
        $sHTML  = <<<OUTPUT
            <input {$sTmp}>
OUTPUT;
        return $sHTML;
    }

}