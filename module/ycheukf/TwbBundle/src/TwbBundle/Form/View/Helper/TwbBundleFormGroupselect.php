<?php
namespace YcheukTwbBundle\Form\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\Element\Select as SelectElement;
use Zend\Form\Exception;
use Zend\Stdlib\ArrayUtils;

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

class TwbBundleFormGroupselect extends \Zend\Form\View\Helper\FormSelect
{
    /**
     * Render a form <select> element from the provided $element
     *
     * @param  ElementInterface $element
     * @throws Exception\InvalidArgumentException
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {
//			echo 2;
		$oTranslator = $this->getTranslator();
        $aSelectedValues = $element->getOption('selectd_values');
		$aSelectedValues = is_null($aSelectedValues) ? array() : $aSelectedValues;
//		var_dump($aSelectedValues);
        $aValues = $element->getOption('group_value_options');
//		var_dump($aSelectedValues);
        $attributes = $element->getAttributes();
		$aColumns = array();
		foreach($aValues as $row1){
			foreach($row1['children'] as $row2){
				foreach($row2['children'] as $row3){
					if(!in_array($row3['perm_label'], $aColumns))
						$aColumns[] = $row3['perm_label'];
				}
			}
		}
		$sHTML = "";
		$sHTML .= "<div class='table-responsive'><table border=1 class='table table-hover table-bordered table-striped'>";
		$sHTML .= "<tr>";
		$sHTML .= "<td >".$oTranslator->translate("主模块")."</td>";
		$sHTML .= "<td >".$oTranslator->translate("子模块")."</td>";
		foreach($aColumns as $sTmp)
			$sHTML .= "<td >".$oTranslator->translate($sTmp)."</td>";
		$sHTML .= "</tr>";
		foreach($aValues as $row){
//			var_dump($row);
			$sHTML .= "<tr>";
			$sHTML .= "<td rowspan='".(count($row['children']))."'>";
			$sChecked = isset($row['selected']) && $row['selected'] ? "checked" : "";
			$sHTML .= $oTranslator->translate($row['perm_label']);
			$sHTML .= "</td>";
//			$sHTML .= "<td  data-key='".$row['id']."'>";
//			$sChecked = isset($row['selected']) && $row['selected'] ? "checked" : "";
//			$sHTML .= "<input type='checkbox' name='".$attributes['name']."' value='".$row['id']."' {$sChecked}>";
//			$sHTML .= $oTranslator->translate("查看");
//			$sHTML .= "</td>";
//			$sHTML .= "</tr>";
			foreach($row['children'] as $row2){
				$sHTML .= "<td data-key='".$row2['id']."'>";
				$sHTML .= $oTranslator->translate($row2['perm_label']);
				$sHTML .= "</td>";
				foreach($aColumns as $sTmp){
					$sTmpTd = "<td ></td>";
					foreach($row2['children'] as $row3){
//						if($row3['perm_label']=="view" && $row3['perm_label']==$sTmp){
						if($row3['perm_label']==$sTmp){
							$sTmpTd = "<td data-key='".$row3['id']."'>";
							$sChecked = in_array($row3['id'], $aSelectedValues) ? "checked" : "";
							$sTmpTd .= "<input type='checkbox' name='".$attributes['name']."' value='".$row3['id']."'{$sChecked}>";
							$sTmpTd .= "</td>";
							break;
						}
					}
					$sHTML .= $sTmpTd;
				}
				$sHTML .= "</tr>";
			}
			$sHTML .= "</tr>";
		}
		$sHTML .= "</table></div>";
		return $sHTML;
    }
}
