<?php
namespace YcheukTwbBundle\Form\View\Helper;
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

class TwbBundleFormImage extends \Zend\Form\View\Helper\FormImage
{
    public function render(\Zend\Form\ElementInterface $element)
    {
        $src = $element->getAttribute('src');
        if (empty($src)) {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned src; none discovered',
                __METHOD__
            ));
        }
        $aReportConfig = \YcheukfReport\Lib\ALYS\ALYSConfig::get();
        $basePath = \YcheukfCommon\Lib\Functions::getImgBaseUrl($aReportConfig['smHandle']);
        
        $img = '';
        if(is_array($src)){
            foreach($src as $val){
                $s = $val;
                if(!preg_match('/\.small\./',  $val)){
                    $s = str_replace(".jpg", ".small.jpg", $val);
                }
                $img .= '<img src='.$basePath.$s.' style="max-width:200px;max-height:100px;">';
            }
        }else{
            if(!preg_match('/\.small\./',  $src)){
                $src = str_replace(".jpg", ".small.jpg", $src);
            }
            $img = '<img src='.$basePath.$src.' style="max-width:200px;max-height:100px;">';
        }

        $sHTML  = <<<OUTPUT
			<div class="col-sm-10">
				{$img}
			</div>
OUTPUT;
        return $sHTML;
    }
}
