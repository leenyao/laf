<?php
namespace YcheukTwbBundle\Form\View\Helper;
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */


class TwbBundleFormSearchbar extends \Zend\Form\View\Helper\FormTextarea
{
	private static $sFormat = <<<OUTPUT
		<div class="input-group" style="width: 220px;">
			<input type="text" class="form-control" placeholder="%s">
			<span class="input-group-btn">
				<button class="btn btn-default" type="button" data-formaction="%s">
					<span class="glyphicon glyphicon-search"></span>
				</button>
			</span>
		</div>
OUTPUT;
	/**
	 * @see \Zend\Form\View\Helper\FormRadio::render()
	 * @param \Zend\Form\ElementInterface $oElement
	 * @return string
	 */
	public function render(\Zend\Form\ElementInterface $oElement){
		$s1 = $oElement->getOption('placeholder');
		$s2 = $oElement->getOption('data-formaction');
		return sprintf(self::$sFormat,$s1, $s2);
	}
}