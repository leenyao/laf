<?php
namespace YcheukTwbBundle\Form\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\Element\Select as SelectElement;
use Zend\Form\Exception;
use Zend\Stdlib\ArrayUtils;

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

class TwbBundleFormSelectHref extends \Zend\Form\View\Helper\FormSelect
{
    /**
     * Render a form <select> element from the provided $element
     *
     * @param  ElementInterface $element
     * @throws Exception\InvalidArgumentException
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {
        if (!$element instanceof SelectElement) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s requires that the element is of type Zend\Form\Element\Select',
                __METHOD__
            ));
        }

        $name   = $element->getName();
        if (empty($name) && $name !== 0) {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }

        $options = $element->getValueOptions();

        if (($emptyOption = $element->getEmptyOption()) !== null) {
            $options = array('' => $emptyOption) + $options;
        }

        $attributes = $element->getAttributes();
        $value      = $this->validateMultiValue($element->getValue(), $attributes);

        $attributes['name'] = $name;
        if (array_key_exists('multiple', $attributes) && $attributes['multiple']) {
//            $attributes['name'] .= '[]';
//            $attributes['name'] .= $attributes['name'];
        }
        $this->validTagAttributes = $this->validSelectAttributes;

		$oTranslator = $this->getTranslator();
		$aNewOption = array();
		if(count($options)){
			foreach($options as $k=>$v)
				$aNewOption[$k] = $oTranslator->translate(str_replace("\\", "_", $v));
		}
//		var

		$options = $element->getOptions();
        return sprintf(
            '<select %s>%s</select><a href="%s" target="%s">%s</a>',
            $this->createAttributesString($attributes),
            $this->renderOptions($aNewOption, $value),
        	$options['url'],
        	$options['target'],
        	$options['text']
        );
    }

}
