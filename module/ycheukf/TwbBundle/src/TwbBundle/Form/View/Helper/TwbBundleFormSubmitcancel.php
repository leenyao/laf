<?php
namespace YcheukTwbBundle\Form\View\Helper;
class TwbBundleFormSubmitcancel extends \Zend\Form\View\Helper\FormButton{

	public function render(\Zend\Form\ElementInterface $oElement, $buttonContent = null){

//		$sValue = $oElement->getValue();
//		$sTmp = "";
		$sClass = $oElement->getAttribute('class');
//		if(is_null($sElName))
//			$oElement->setAttribute('data-elname', $oElement->getAttribute('name'));
//
////		data-elname
//		$sButtonLabel = "";
//		foreach($oElement->getAttributes() as $k=>$v){
//			$sTmp .= $k."='".$v."' ";
//			if($k=='name')
//				$sButtonLabel = $v;
//		}
    	$aOptions = $oElement->getOptions();
		// var_dump($aOptions);
		$bCacelButtonShow = isset($aOptions['cancelbuttonflag']) ? $aOptions['cancelbuttonflag'] : 1;
                $bContinueButtonShow = isset($aOptions['continuebuttonflag']) ? $aOptions['continuebuttonflag'] : 0;
		$oTranslator = $this->getTranslator();
		$sButtonLabel = $oTranslator->translate("保存");
		$sConfirm = $oTranslator->translate("form_confirm");
		$sButtonLabe2 = $oTranslator->translate("cancel");
        $sButtonLabe3 = $oTranslator->translate("continue2submit");

        $aftersave_urlarray = isset($aOptions['aftersave_urlarray']) ? $aOptions['aftersave_urlarray'] : array();
        // var_dump($aftersave_urlarray);

//		submit then redirect
        $sButtonSubmitRedirect = "";
        if (isset($aOptions['redirect']) && count(($aOptions['redirect']))) {
	        $sButtonLabeTmp = $oTranslator->translate($aOptions['redirect']['label']);

        	$sButtonSubmitRedirect = '<input type="button" class="btn btn-default  btn-primary btn-redirect" _url=\''.urlencode(json_encode($aOptions['redirect']['urlarray'])).'\' value="'.$sButtonLabeTmp.'">';
        }


//		go back
        $sButtonGoBackRedirect = "";
        if (isset($aOptions['redirect_goback']) && count(($aOptions['redirect_goback']))) {
	        $sButtonLabeTmp = $oTranslator->translate($aOptions['redirect_goback']['label']);

        	$sButtonGoBackRedirect = '<input type="button" class="btn btn-default  btn-primary btn-redirect"  type="button" _url=\''.urlencode(json_encode($aOptions['redirect_goback']['urlarray'])).'\' value="'.$sButtonLabeTmp.'">';
        }

        $sHtmlaftersave_urlarray = "";
        if (count($aftersave_urlarray)) {
            $sHtmlaftersave_urlarray = '_url=\''.urlencode(json_encode($aftersave_urlarray)).'\'';

        }


		// $sCancelButton = $bCacelButtonShow ? "<button class='btn btn-default btn-cancel' type='button' onclick='javascript:window.history.go(-1);'>{$sButtonLabe2}</button>" : "";
		$bContinueButtonShow = $bContinueButtonShow ? "<input type=\"hidden\" name=\"continueFlag\" value=\"0\"><button name='continueSubmit' class='btn btn-default btn-submit' type='submit'>{$sButtonLabe3}</button>" : "";
                $sHTML  = <<<OUTPUT
                    <hr/>
			<div class="col-sm-offset-2 col-sm-10">
				{$sButtonGoBackRedirect}
				<button class="btn btn-default btn-submit btn-primary btn-redirect" {$sHtmlaftersave_urlarray} type="button">{$sButtonLabel}</button>
				{$sButtonSubmitRedirect}
				{$bContinueButtonShow}
			</div>
			<script type="text/javascript">
                \$('[name="continueSubmit"]').click(function(){
                    \$('[name="continueFlag"]').val("1");
                });

                var sActionUrl = \$('form#admincontent').attr("action") ?  \$('form#admincontent').attr("action") : '';
				\$('.btn-redirect').click(function(e){
                    var oRedirect = \$(this).attr("_url");

                     if(oRedirect){
                        
                      \$('form#admincontent').attr("action", l.setLocationParam(sActionUrl, "redirect", oRedirect));

                    }
					// \$('form').append("<input type=\"hidden\" name=\"___redirect___\" value=\""+\$(this).attr("_url")+"\">");
					\$('form#admincontent').submit();
				});

				// l.setLocationParam($('form').action, "redirect", $(.btn-redirect).attr("_url"));
                //注释, 不需要确认提交
				// \$('form').submit(function(e){
				// 	if(confirm('{$sConfirm}')){
				// 		return true;
				// 	}else{
				// 		e.stopImmediatePropagation();
				// 		return false;
				// 	}
				// })
			</script>
OUTPUT;
        return $sHTML;
	}

}
