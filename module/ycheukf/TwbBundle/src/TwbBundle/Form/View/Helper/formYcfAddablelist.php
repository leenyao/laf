<?php
namespace YcheukTwbBundle\Form\View\Helper;
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
use Zend\Form\ElementInterface;
use Zend\Form\Exception;



class formYcfAddablelist extends \Zend\Form\View\Helper\FormText
{


    /**
     * Render a form <textarea> element from the provided $element
     *
     * @param  ElementInterface $element
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {
		$sValue = $element->getValue();
		$sElName = $element->getAttribute('name');
        $sAttrs = "";
		foreach($element->getAttributes() as $k=>$v){
            if($k=='type')$v="hidden";
            $v = is_array($v) ? json_encode($v) : $v;
            $sAttrs .= " $k='$v'";
        }
        $reloadUrl = $element->getAttribute('reloadUrl');
        $attrsUrl = $element->getAttribute('attrsUrl');
//        $attrsUrl = $this->url('hcoffee/index/getattributeslist');
//		var_dump($element->getAttributes());
        $sHTML = <<<OUTPUT
            <input {$sAttrs} >
	<script type="text/javascript">
    (function(){
            \$("#{$sElName}").laf_addablelist({
                reloadUrl: '{$reloadUrl}',
                attrsUrl: '{$attrsUrl}'
            });
    }());
	</script>
OUTPUT;
        return $sHTML;
    }
}
