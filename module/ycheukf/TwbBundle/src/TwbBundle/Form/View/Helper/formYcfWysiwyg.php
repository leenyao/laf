<?php
namespace YcheukTwbBundle\Form\View\Helper;
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
use Zend\Form\ElementInterface;
use Zend\Form\Exception;



class formYcfWysiwyg extends \Zend\Form\View\Helper\FormTextarea
{


    /**
     * Render a form <textarea> element from the provided $element
     *
     * @param  ElementInterface $element
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {
		$sValue = $element->getValue();
		$sElName = $element->getAttribute('name');
		
		$sId = "t_".uniqid();
		$sHTML  = <<<OUTPUT
			<textarea id="{$sId}" name="{$sElName}" rows="" cols="">{$sValue}</textarea>
	<script type="text/javascript">
    (function(){
        var sendFile;
        sendFile = function(file, callback) {
            var data;
            data = new FormData();
            data.append("uploadfile", file);
            return \$.ajax({
                url: l.get('imgUploadBaseUrl') + '/api/imgupload?ext=.jpg,.jpeg&zoom=small',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(data) {
                    return callback(data);
                }
            });
        };
		\$("#{$sId}").each(function() {
    		var \$this = \$(this);
    		\$this.summernote({
                lang:'zh-CN',
    			toolbar: [
    				//['style', ['style']], //no style button
    				['fontsize', ['fontsize']],
    				['color', ['color']],
    				['style', ['bold', 'italic', 'underline', 'clear']],
    				['para', ['ul', 'ol', 'paragraph']],
    				['height', ['height']],
                    ['insert', ['picture','link']],
                    ['misc',['codeview']]
    			],
    			height: 300,
                onpaste: function() {
                    var valueField = \$("#{$sId}");
                    setTimeput(function(){
                        valueField.val(\$this.code());
                    },16);
                },
    			onkeyup: function() {
    				var valueField = \$("#{$sId}");
    				valueField.val(\$this.code());
    			},
                onImageUpload: function(files, editor, welEditable) {
                    return sendFile(files[0], function(data) {
                        var url;
                        url = l.get('imgUploadBaseUrl') + data.data;
                        if (Number(data.status) === 1){
                            return editor.insertImage(welEditable, url);
                        }else{
                            return false;
                        } 
                    });
                }
            });
            \$this.code(\$this.val());
        });
    }());
	</script>
OUTPUT;
        return $sHTML;
    }
}
