<?php
namespace YcheukTwbBundle\View\Helper;
class TwbBundleLabel extends \Zend\Form\View\Helper\AbstractHelper{
	/**
	 * @var string
	 */
	private $labelFormat = '<label %s>%s</label>';

	/**
	 * Invoke helper as functor, proxies to {@link render()}.
	 * @param string $sLabelMessage
	 * @param string|array $aLabelAttributes : [optionnal] if string, label class
	 * @return string|\YcheukTwbBundle\View\Helper\TwbBundleAlert
	 */
    public function __invoke($sLabelMessage = null, $aLabelAttributes = 'default' ,$required = FALSE){
        if(!$sLabelMessage)return $this;
        return $this->render($sLabelMessage,$aLabelAttributes,$required);
    }

    /**
     * Retrieve label markup
     * @param string $sLabelMessage
     * @param  string|array $aLabelAttributes : [optionnal] if string, label class
     * @throws \InvalidArgumentException
     * @return string
     */
	public function render($sLabelMessage, $aLabelAttributes = 'label-default',$required = false){
		if(!is_scalar($sLabelMessage))throw new \InvalidArgumentException('Label message expects a scalar value, "'.gettype($sLabelMessage).'" given');
		if(empty($aLabelAttributes))throw new \InvalidArgumentException('Label attributes are empty');
		if(is_string($aLabelAttributes))$aLabelAttributes = array('class' => $aLabelAttributes);
		elseif(!is_array($aLabelAttributes))throw new \InvalidArgumentException('Label attributes expects a string or an array, "'.gettype($aLabelAttributes).'" given');
		elseif(empty($aLabelAttributes['class']))throw new \InvalidArgumentException('Label "class" attribute is empty');
		elseif(!is_string($aLabelAttributes['class']))throw new \InvalidArgumentException('Label "class" attribute expects string, "'.gettype($aLabelAttributes).'" given');

//		if(!preg_match('/(\s|^)label(\s|$)/',$aLabelAttributes['class']))$aLabelAttributes['class'] .= ' label';
		if(null !== ($oTranslator = $this->getTranslator()))$sLabelMessage = $oTranslator->translate($sLabelMessage, $this->getTranslatorTextDomain());
		$labelFormat = $this->_getLableFormat($required);
		return sprintf(
			$labelFormat,
			$this->createAttributesString($aLabelAttributes),
			$sLabelMessage
		);
	}

	/**
	 * 设置label标签
	 * @param bool $required 是否带必填标识
	 * @return string
	 */
	private function _getLableFormat($required = false){
		return $required ? '<label %s><span class="text-danger">*</span>%s</label>' : '<label %s>%s</label>';
	}
}