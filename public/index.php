<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
// xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);

 set_time_limit(0);
chdir(dirname(__DIR__));


///////////////////////check LAF completed/////////////////////////////
if(in_array(PHP_SAPI, array('apache2handler', 'apache', 'apache2filter')) && !file_exists("data/laf_installed")){
    if(file_exists("data/laf_installing")){
        echo "LAF is installing . refresh this page to check the progress";
        echo "<hr>";
        echo "<pre>";
        echo file_get_contents("data/install.log");
        echo "</pre>";
    }else{
        echo "LAF is not installed right";
    }
    exit;
}


// echo system("echo \$LAF_project_namespace");
defined('LAF_NAMESPACE') || define('LAF_NAMESPACE', ucfirst(getenv("LAF_project_namespace")));


defined('BASE_INDEX_PATH') || define('BASE_INDEX_PATH', dirname(__FILE__));
$sHOST = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost');
$sAppModel = "APPLICATION";
defined('APPLICATION_MODEL') || define('APPLICATION_MODEL', $sAppModel);

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
require 'init_autoloader.php';


// Run the application!
$aApplicationConfig = array();
$sAppModel = "APPLICATION";
switch(PHP_SAPI){
	case 'cli':
		$aApplicationConfig = require 'config/console.config.php';
		$sAppModel = "CONSOLE";
	break;
	default:
		switch(true){
//			case preg_match("/\/oauth\//i", $_SERVER['REQUEST_URI']):
//			case preg_match("/\/api\//i", $_SERVER['REQUEST_URI']):
//				$sAppModel = "DBPROXY";
//				$aApplicationConfig = require 'config/dbproxy.config.php';
//			break;
			default:
				$sAppModel = "APPADMIN";
				$aApplicationConfig = require 'config/application.config.php';
			break;
		}
	break;
}
defined('APPLICATION_MODEL') || define('APPLICATION_MODEL', $sAppModel);
//var_dump($sAppModel);
//exit;
Zend\Mvc\Application::init($aApplicationConfig)->run();

/*
$xhprof_data = xhprof_disable();
$XHPROF_ROOT = (__DIR__)."/../../../../../test/xhprof";
include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_lib.php";
include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_runs.php";
$xhprof_runs = new XHProfRuns_Default();
$run_id = $xhprof_runs->save_run($xhprof_data, "xhprof_testing");

//$_SESSION['xhprof_run_id'] = $run_id;
//echo "http://localhost/test/xhprof/xhprof_html/index.php?run={$run_id}&source=xhprof_testing\n";
*/