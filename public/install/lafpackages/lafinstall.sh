#!/bin/bash
set -e

currentdir=$(cd "$(dirname "$0")"; pwd)

bDebug=0
if [[ $1 != "" ]]; then
    bDebug=$1
fi
sCodeDir=$currentdir/lafgovhr
sDockimageDir=$currentdir/dockerimages

for file in $sDockimageDir/*; do
    if test -f $file
    then
        echo "loading docker images:"$file
        docker load -i $file
    fi
done

mkdir -p $sCodeDir && cd $sCodeDir

tar -xzf $currentdir/code.tar.gz ./
sh docker_start.sh