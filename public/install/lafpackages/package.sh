#!/bin/bash
#package LAF GOVHR
set -e

currentdir=$(cd "$(dirname "$0")"; pwd)
cd $currentdir
sSetupDir=/tmp/Lafgovhrsetup
bDebug=0
if [[ $1 != "" ]]; then
    bDebug=$1
fi
echo "
PACKAGING LAF-GOVHR at $sSetupDir
"
mkdir -p $sSetupDir


if [[ $bDebug -eq 0 ]]; then

    echo "downloading installscript"
    wget -q http://lur.mzbrand.com.cn/install/lafpackages/lafinstall.sh -O $sSetupDir/lafinstall.sh

    echo "downloading lafdocker"
    wget -q http://lur.mzbrand.com.cn/install/lafpackages/lafdocker.release.tar.gz -O $sSetupDir/lafdocker.release.tar.gz
    
    echo "downloading laf"
    wget -q http://lur.mzbrand.com.cn/install/lafpackages/laf.release.tar.gz -O $sSetupDir/laf.release.tar.gz

    echo "downloading lafmodulegovhr"
    wget -q http://lur.mzbrand.com.cn/install/lafpackages/lafmodulegovhr.release.tar.gz -O $sSetupDir/lafmodulegovhr.release.tar.gz

    echo "downloading lafvendor"
    wget -q http://lur.mzbrand.com.cn/install/lafpackages/lafvendor.release.tar.gz -O $sSetupDir/lafvendor.release.tar.gz


fi


lafdockerimagesdir=$sSetupDir/dockerimages
mkdir -p $lafdockerimagesdir

lafcodedir=$sSetupDir/codedir
mkdir -p $lafcodedir && rm -fR $lafcodedir/*
cd $lafcodedir
tar -zvxf $sSetupDir/lafdocker.release.tar.gz > /dev/null
cp docker-compose-lib/docker-compose.govhr.yml ./docker-compose.yml

# save docker images
if [[ $bDebug -eq 0 ]]; then
    cd $lafdockerimagesdir
    aDockerImages=$(grep 'image:' $lafcodedir/docker-compose-lib/docker-compose.govhr.yml | awk -F"\""  '{print $2}')
    for i in $aDockerImages; do
        echo "saving docker images:" $i
        # echo $sImageName

        sImageName=$(echo $i | awk -F"/" '{print $NF}')
        docker pull $i
        docker save -o ${sImageName//:/_} $i
    done
fi

mkdir -p $lafcodedir/volume/mysql/data
mkdir -p $lafcodedir/volume/redis/data
mkdir -p $lafcodedir/volume/code

cd $lafcodedir/volume/code
tar -zvxf $sSetupDir/laf.release.tar.gz > /dev/null
tar -zvxf $sSetupDir/lafvendor.release.tar.gz > /dev/null

mkdir -p $lafcodedir/volume/code/module/Govhr && cd $lafcodedir/volume/code/module/Govhr
tar -zvxf $sSetupDir/lafmodulegovhr.release.tar.gz > /dev/null

cd $lafcodedir
tar -czf $sSetupDir/code.tar.gz ./*
rm -fR $lafcodedir


echo "
===DONE===
all the docker images and code are in the dir $sSetupDir

"