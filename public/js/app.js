(function($) { /*global l*/
    //i am new commit
    window.l = {};
    var scope = {};
    l.scope = scope;
    l.setup = function(config) {
        var i;
        if (typeof config === 'object') {
            for (i in config) {
                if (config.hasOwnProperty(i)) {
                    l.scope[i] = config[i];
                }
            }
        }
    };
    l.get = function(paramName) {
        return l.scope[paramName];
    };
    l.ajax = function(url, data) {
        return $.ajax({
            url: url,
            data: {
                params: JSON.stringify(data)
            },
            dataType: 'json',
            type: 'post'
        });
    };
    l.changevcode = function(id){
        var el = document.getElementById(id),src;
        if(el && el.tagName ==='IMG'){
            src = el.src;
            src = src.replace(new RegExp("(&|\\?)tm=[^&]+"),'');
            el.src = src+'?tm='+ new Date().getTime();
        }
    };
    l.dialog = function(config) {
        var remoteUrl = config.remoteUrl,
            autoDestroy = true,
            id = config.id,
            title = config.title || "提示",
            $dialog,
            buttons = config.buttons,
            html = '';
        if (typeof config.autoDestroy !== 'undefined') {
            autoDestroy = config.autoDestroy;
        }
        if (typeof id === 'undefined') {
            id = "modal-" + $.now();
        }
        html = ['<div id="' + id + '" data-remote="' + (remoteUrl ? remoteUrl : "false") + '" class="modal fade">',
            '<div class="modal-dialog">',
            '<div class="modal-content">',
            '<div class="modal-header">',
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
            '<h4 class="modal-title">' + title + '</h4>',
            '</div>',
            '<div class="modal-body">',
            config.body,
            '</div>', (config.footer ? '<div class="modal-footer">' + config.footer + '</div>' : ''),
            '</div>',
            '</div>',
            '</div>'
        ].join('');
        $dialog = $(html).appendTo('body');
        $.each(buttons, function(k, v) {
            var btn = $('<button type="button"></button>'),
                footer = $('.modal-footer', $dialog),
                scope = v.scope || this;
            btn.html(v.text);
            btn.attr(v.attr || {});
            btn.addClass(v.cls);
            if (typeof v.handler === 'function') {
                btn.on('click', function() {
                    v.handler.call(scope, this, $dialog);
                });
            }
            if (footer.length === 0) {
                footer = $('<div class="modal-footer"></div>').appendTo($dialog.find('.modal-content'));
            }
            btn.appendTo(footer);
        });
        if (autoDestroy) {
            $dialog.on('hidden.bs.modal', function() {
                $dialog.remove();
            });
        }
        return $dialog;
    };
    l.setLocationParam = function(query,param,value) {
        var p = new RegExp("(^|)" + param + "=([^&]*)(|$)");
        if(p.test(query)){
            //query = query.replace(p,"$1="+value);
            var firstParam=query.split(param)[0];
            var secondParam=query.split(param)[1];
            if(secondParam.indexOf("&")>-1){
                var lastPraam=secondParam.split("&")[1];
                return  '?'+firstParam+'&'+param+'='+value+'&'+lastPraam;
            }else{
                if(firstParam){
                    return '?'+firstParam+'&'+param+'='+value;
                }else{
                    return '?'+param+'='+value;
                }
            }
        }else{
            if(query == ''){
                return '?'+param+'='+value;
            }else{
                return query+'&'+param+'='+value;
            }
        }
    }
}(jQuery));
$(function() {
    var $forms = $('form');
    $forms.attr('novalidate', 'novalidate');



    function appendParam(params, key, value) {
        var tmp;
        params = params || {};
        if (!key) {
            return params;
        }
        if (typeof params[key] === 'undefined') {
            params[key] = value;
        } else if (!$.isArray(params[key])) {
            tmp = params[key];
            params[key] = [];
            params[key].push(tmp);
            params[key].push(value);
        } else {
            params[key].push(value);
        }
        return params;
    }
    $('input[type="file"].upload-input').each(function() {
        var value = $(this).attr('data-value'),
            bundleField = $(this).data('bundleField'),
            fileType = $(this).attr('data-filetype'),
            preview = $(this).data('preview'),
            wrapper = $(this).data('wrapper');
        if (!wrapper) {
            $(this).wrap('<div class="uploader-wrapper"/>');
            wrapper = $(this).closest(".uploader-wrapper");
            $(this).data('wrapper', wrapper);

        }
        if (!bundleField) {
            $(this).data('bundleField', bundleField = $('<input type="hidden"/>'));
            $(this).after(bundleField);
        }
        bundleField.attr('name', $(this).attr('data-elname'));
        if (value) {
            if (!bundleField) {
                $(this).data('bundleField', bundleField = $('<input type="hidden"/>'));
                $(this).after(bundleField);
            }
            if (fileType === 'pic') {
                if (!preview) {
                    $(this).data('preview', preview = $('<img style="display:none;max-width:500px;max-height:200px;" src="data:,"/>'));
                    $(this).after(preview);
                }
                preview.show();
                if (value.indexOf('http') > -1) {
                    preview.attr('src', value);
                } else {
                    preview.attr('src', l.get('imgUploadBaseUrl') + value);
                }

            } else {
                if (!preview) {
                    $(this).data('preview', preview = $('<a />'));
                    $(this).after(preview);
                }
                if (value.indexOf('http') > -1) {
                    preview.attr('href', value);
                } else {
                    preview.attr('href', l.get('imgUploadBaseUrl') + value);
                }
                preview.text(value);
            }
            bundleField.val(value);
        }
    });
    $('input[type="file"].upload-input').each(function() {
        var params;
        params = $(this).data('params');
        params = params?"?"+params:"";
        $(this).attr('name', 'uploadfile').fileupload({
            dataType: 'json',
            url: l.get('imgUploadBaseUrl') + '/api/imgupload' + params,
            done: function(e, data) {},
            formData: [],
            always: function(e, data) {
                var bundleField = $(this).data('bundleField'),
                    fileType = $(this).attr('data-filetype'),
                    resultUrl = data.result.data + '',
                    preview = $(this).data('preview');
                if (!bundleField) {
                    $(this).data('bundleField', bundleField = $('<input type="hidden"/>'));
                    $(this).after(bundleField);
                }
                if (fileType == 'pic') {
                    if (!preview) {
                        $(this).data('preview', preview = $('<img style="display:none;max-width:500px;max-height:200px;" src="data:,"/>'));
                        $(this).after(preview);
                    }
                    preview.show();
                    if (resultUrl.indexOf('http') > -1) {
                        preview.attr('src', resultUrl);
                    } else {
                        preview.attr('src', l.get('imgUploadBaseUrl') + resultUrl);
                    }
                } else {
                    if (!preview) {
                        $(this).data('preview', preview = $('<a />'));
                        $(this).after(preview);
                    }
                    if (resultUrl.indexOf('http') > -1) {
                        preview.attr('href', resultUrl);
                    } else {
                        preview.attr('href', l.get('imgUploadBaseUrl') + resultUrl);
                    }
                    preview.text(data.result.data);
                }
                bundleField.attr('name', $(this).attr('data-elname'));
                bundleField.val(data.result.data);
                $(this).closest('form').off('submit.fileupload');
            },
            change: function(e, data) {
                $(this).closest('form').on('submit.fileupload', function() {
                    return false;
                });
            }
        });
    });

    function validate(field) {
        var required = field.data('required') || field.attr('required'),
            regexp = field.data('regexp'),
            formGroup = field.closest('.form-group'),
            value = field.val();
        if (required && !value) {
            formGroup.addClass('has-error');
            formGroup.attr('data-original-title', '此为必填项');
            formGroup.tooltip({
                trigger: 'manual',
                placement: 'bottom'
            }).tooltip('show');
            //e.stopPropagation();
            $(document).one('click', function() {
                formGroup.tooltip('destroy');
            });
            return false;
        }
        if (regexp && !new RegExp(regexp).test(value)) {
            formGroup.addClass('has-error');
            formGroup.attr('data-original-title', '输入格式有误');
            formGroup.tooltip({
                trigger: 'manual',
                placement: 'right'
            }).tooltip('show');
            //e.stopPropagation();
            $(document).one('click', function() {
                formGroup.tooltip('destroy');
            });
            return false;
        }
        return true;
    }

    $forms.on('submit', function() {
        var $this = $(this),
            me = this,
            inputs = $('input:not(:disabled)', this),
            selects = $('select:not(:disabled)', this),
            wysiwyg = $('textarea:not(:disabled)', this),
            files = $('input[type="file"]:not(:disabled)', this),
            isValidated = true,
            //textarea = $('textarea:not(:disabled)'),
            data = {};
        $(this).find('.form-group').removeClass('has-error');
        try {
            inputs.each(function() {
                var name = $(this).attr('name'),
                    value = $(this).val(),
                    obj = {};
                if (!name) {
                    return;
                }
                if ($(this).is(':checkbox')) {
                    if (this.checked) {
                        if (!$.isArray(data[name])) {
                            data[name] = [];
                        }
                        data[name].push(this.value);
                    }
                } else if ($(this).is(':radio')) {
                    if (this.checked) {
                        data[name] = this.value;
                    }
                } else {
                    appendParam(data, name, value);
                }

            });
            selects.each(function() {
                var name = $(this).attr('name'),
                    value = $(this).val();
                if (!name) {
                    return;
                }
                appendParam(data, name, value);

            });
            wysiwyg.each(function() {
                var name = $(this).attr('name'),
                    value = $(this).val();
                if (!name) {
                    return;
                }
                appendParam(data, name, value);
            });
            $(inputs).add(selects).add(wysiwyg).each(function() {
                isValidated = isValidated && validate($(this));
            });
            if (!isValidated) {
                return false;
            }
            $.ajax({
                url: $this.attr('action'),
                data: {
                    params: JSON.stringify(data)
                },
                context: $this,
                dataType: 'json',
                type: 'post',
                error: function(res, jqXHR){
                    if (res.responseJSON && res.responseJSON.msg) {
                        $('div.panel-body div.form-alert').remove();
                        var msg = $('<div class="alert alert-warning form-alert"><button type="button" class="close" data-dismiss="alert">&times;</button> <h4>警告!</h4>  <span>'+(res.responseJSON.msg || "未知错误")+'</span></div>').prependTo('div.panel-body');
                    }
                    console.log(res)
                }
            }).done(function(res) {
                var payurl, subject = "";

                if (String(res.status) === '5001') {
                    try {
                        subject = JSON.parse(res.data.jflv3_id)[0].label;
                        if (typeof subject !== "string") {
                            throw "传入的task_title不合法";
                        }
                    } catch (e) {
                        subject = "";
                    }
                    payurl = l.get('baseUrl') + '/script/alipay.php';
                    payurl += '?' + $.param({
                        resid: res.data.resid,
                        total_fee: res.data.task_bonus,
                        body: '商品：牵牛招聘产品',
                        subject: subject
                    });
                    $.alipay({
                        payment: res.data.task_bonus,
                        payurl: payurl
                    });
                }
            }).done(function() {
                var callback = $this.data('callback');
                if (callback && typeof callback.done === 'function') {
                    callback.done.apply(me, arguments);
                }
            });

        } catch (e) {
            
        }
        return false;
    });
    $.widget("ui.autocomplete", $.ui.autocomplete, {
        _renderMenu: function(ul, items) {
            this._super(ul, items);
            if (this.options.hintText) {
                $(ul).prepend("<li class=\"hint-text\">" + this.options.hintText + "</li>");
            }
        }
    });
    $('.autocomplete').each(function() {
        var $this = $(this),
            source, url, value;
        source = $(this).data('source');
        try {
            source = JSON.parse(source);
        } catch (e) {

        }
        if (typeof source === 'string') {
            url = source;
            source = function(request, response) {
                // if($.trim(request.term) === ''){
                //     return false;
                // }
                $.get(url + request.term, function(res) {
                    response(res.data);
                }, 'json');
            };
        }
        try {
            value = $this.data('value') || $this.val();
            if (typeof value === 'object') {
                $this.val(JSON.stringify(value));
            } else {
                $this.val(value);
            }
        } catch (e) {

        }
        $this.tokenfield({
            tokens: value,
            minWidth:500,
            autocomplete: {
                source: source,
                delay: 500,
                hintText: ($(this).data('allowCreate') ? "可以新增" : "不可新增") + "，" +
                    ($(this).data('multiple') ? "可多选" : "不可多选")
            },
            showAutocompleteOnFocus: true,
            allowDuplicates: false,
            createTokensOnBlur: true
        }).on('beforeCreateToken', function(e) {
            var allowCreate = $(this).data('allowCreate'),
                multiple = $(this).data('multiple');
            if (!allowCreate) {
                if (e.token.value === '') {
                    e.token = null;
                }
            }
            if (!multiple) {
                $(this).tokenfield('setTokens', []);
            }

        });
    });

    $('.selectpanel').nestedselect();

    $('.select-tab').tagselect();
    if($.fn.bootstrapDualListbox){
        $('.duallist').bootstrapDualListbox({nonSelectedListLabel: '未选择列表',
            selectedListLabel: '已选择列表',
            filterPlaceHolder: '查找',
            infoText:'共有{0}项',
            infoTextFiltered:'从{1}项中找到{0}项',
            infoTextEmpty:'空列表',
            moveAllLabel:'全选',
            removeAllLabel:'全部取消',
            preserveSelectionOnMove: 'all',
            moveOnSelect: true
        });
    }
    
    //menu
    $('.navbar-submenu').on('click.menu', 'li', function(event) {
        // var _this = $(this),
        //     _parent = _this.parent();
        // _parent.find('ul').slideUp('fast');
        // _this.find('ul:hidden').slideDown('fast');
        // _parent.find('li').removeClass('active');
        // _this.addClass('active');
        // event.stopPropagation();
    });
    //自动打开当前菜单
    $('.navbar-submenu').find('li.active').parent().parent().show();

    //datepicker
    $('.datepicker').each(function() {
        var defaultValue = $(this).data('defaultDate');
        $(this).val(defaultValue);
        $(this).datepicker({
            constrainInput: true,
            dateFormat: 'yy-mm-dd',
            defaultDate: defaultValue,
            changeYear: true,
            yearRange: '-100:+0'
        });
    });
    $('.time-picker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        language: 'zh-CN'
    });

    //ajax
    var ajaxMasker = $('#ajax-masker');
    if (ajaxMasker.length === 0) {
        ajaxMasker = $('<div id="ajax-masker"></div>').appendTo('body');
        ajaxMasker.css({
            position: 'absolute',
            display: 'none',
            top: 0,
            left: 0,
            opacity: 0.4,
            backgroundColor: '#000',
            zIndex: 1050
        });
    }
    $(document).ajaxStart(function() {
        ajaxMasker.css({
            width: $(document).width(),
            height: $(document).height(),
        });
        ajaxMasker.show();
    });
    $(document).ajaxStop(function() {
        ajaxMasker.hide();
    });
    $(document).ajaxComplete(function(e, xhr, settings) {
        var res = {},
            msg, status, target, errorcodes = l.get('apiexceptioncode');
        $('#msg').hide();
        try {
            res = $.parseJSON(xhr.responseText);
        } catch (error) {}
        status = Number(res.status);
        if (xhr.state() === 'rejected') {
            status = 1;
        }
        if(isNaN(status)){
            status = 1;
        }
        switch (status) {
            case 1:
                break;
            case 5000:
                window.location.href = res.data;
                break;
            case 5001:
                break;
            case 5003:
            case 2028:
                target = settings.context || e.target;
                if(target == document){
                    target = 'body';
                }
                msg = $('#msgpanelbody');
                if (msg.length === 0) {
                    msg = $('<div id="msgpanelbody" class="alert alert-warning"><a href="#" class="close" data-dismiss="warning">&times;</a></div>').prependTo('div.panel-body .alertbox');
                    msg.css({
                        // position: 'fixed',
                        // top: 0,
                        width: '100%',
                        // left: 0,
                        zIndex: 1050,
                        cursor: 'pointer'
                    });
                    msg.on('click', function() {
                        $(this).hide();
                    });
                }
                if (msg.is(":visible")) {
                    msg.hide();
                }
                var sErrorMsg = "修改失败"
                if (status==2028)sErrorMsg = "修改失败:上传的文件类型不支持"
                msg.text( sErrorMsg+": "+res.msg);
                msg.show(500);
                if (res.data) {
                    $.each(res.data, function() {
                        var field = $(target).find('[name="' + this.inputName + '"]:eq(' + this.index + ')'),
                            formGroup = field.closest('.form-group');
                        console.log(field)
                        console.log(formGroup)
                            
                        formGroup.addClass('has-error');
                        formGroup.attr('data-original-title', this.msg);
                        formGroup.tooltip({
                            trigger: 'manual',
                            placement: 'bottom',
                            container: target || 'body'
                        }).tooltip('show');
                        //e.stopPropagation();
                        $(document).one('click', function() {
                            formGroup.tooltip('destroy');
                        });
                    });

                }

                break;
            case 5004:

                msg = $('#msgpanelbody');
                if (msg.length === 0) {
                    msg = $('<div id="msgpanelbody" class="alert alert-success"><a href="#" class="close" data-dismiss="success">&times;</a></div>').prependTo('div.panel-body .alertbox');
                    msg.css({
                        // position: 'fixed',
                        // top: 0,
                        width: '100%',
                        // left: 0,
                        zIndex: 1050,
                        cursor: 'pointer'
                    });
                    msg.on('click', function() {
                        $(this).hide();
                    });
                }
                if (msg.is(":visible")) {
                    msg.hide();
                }
                msg.text( "修改成功");
                msg.show(500);
            break;
            default:
                if (errorcodes.length && errorcodes.indexOf(status) > -1) {
                    return;
                }
                msg = $('#msgpanelbody');
                if (msg.length === 0) {
                    msg = $('<div id="msgpanelbody" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert">&times;</a></div>').prependTo('div.panel-body');
                    msg.css({
                        // position: 'fixed',
                        // top: 0,
                        width: '100%',
                        // left: 0,
                        zIndex: 1050,
                        cursor: 'pointer'
                    });
                    msg.on('click', function() {
                        $(this).hide();
                    });

                }
                if (msg.is(":visible")) {
                    msg.hide();
                }
                msg.text(res.msg || "未知错误");
                msg.show(500);
        }
    });

    $(document).on('click.l.dialog', '.action-log', function() {
        var $dialog = $('#modal-log-info'),
            links = $(this).data('url');
        if ($dialog.length < 1) {
            $dialog = $('<div class="modal fade" id="modal-log-info"></div>');
            $dialog.appendTo(document.body);
            $dialog.on('show.bs.modal', function(e) {
                var $this = $(this),
                    link = $this.data('link');
                if (link) {
                    $.post(link, {
                        params: {}
                    }, function(data) {
                        if (Number(data.status) === 1) {
                            $this.html(['<div class="modal-dialog">',
                                '<div class="modal-content">',
                                '<div class="modal-header">',
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                                '<h4 class="modal-title" >备注</h4>',
                                '</div>',
                                '<div class="modal-body">',
                                data.data.detail_output,
                                '</div>', //<!-- modal-body -->
                                '<div class="modal-footer">',
                                '<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>',
                                '</div>',
                                '</div>',
                                '</div>'
                            ].join(''));
                        }
                    }, 'json');
                }
            });
        }
        $dialog.data('link', links);

        $dialog.modal({
            backdrop: true,
            show: false
        });

        $dialog.modal('show');
    });
    /*
        slide menu
    */
    $('.fold').on('click', function() {
        var menu = $(this).closest('.left-menu'),
            right = menu.siblings('.offset-right-panel');
        menu.css({
            'overflow': 'hidden'
        });
        menu.animate({
            width: 'toggle'
        }, 'fast');
        right.animate({
            marginLeft: '0'
        }, 'fast', function() {
            $('.report-cantainer').dataTable('fixDummyScroller');
        });
        $('.open').show();
    });
    $('.open').on('click', function() {
        var menu = $('.left-menu'),
            right = menu.siblings('.offset-right-panel');
        menu.css({
            'overflow': 'hidden'
        });
        menu.animate({
            width: 'toggle'
        }, 'fast');
        right.animate({
            marginLeft: '210'
        }, 'fast', function() {
            $('.report-cantainer').dataTable('fixDummyScroller');
        });
        $('.open').hide();
    });
});
(function() {
    $.datepicker.setDefaults($.datepicker.regional["zh-CN"]);
}());