(function($) {
    var jcrop_api,
        boundx,
        boundy,

        // Grab some information about the preview pane
        $preview = $('#preview-pane'),
        $pcnt = $('#preview-pane .preview-container'),
        $pimg = $('#preview-pane .preview-container img'),

        xsize = $pcnt.width(),
        ysize = $pcnt.height();

    function updatePreview(c) {
        if (parseInt(c.w,10) > 0) {
            var rx = xsize / c.w,
                ry = ysize / c.h;

            $pimg.css({
                width: Math.round(rx * boundx) + 'px',
                height: Math.round(ry * boundy) + 'px',
                marginLeft: '-' + Math.round(rx * c.x) + 'px',
                marginTop: '-' + Math.round(ry * c.y) + 'px'
            });
        }
    }
    $.widget('l.avataruploader', {
        _create: function() {
            var me = this;
            $.l.avataruploader.idGenerator++;
            me.id = this.widgetName + '-' + $.l.avataruploader.generateId();
            $.l.avataruploader.instance[me.id] = this;
            //this.initOptions();

            $('#target').Jcrop({
                onChange: updatePreview,
                onSelect: updatePreview,
                aspectRatio: 1
            }, function() {
                // Use the API to get the real image size
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                // Store the API in the jcrop_api variable
                jcrop_api = this;

                // Move the preview into the jcrop container for css positioning
                $preview.appendTo(jcrop_api.ui.holder);
            });


            this.body = $(this._generateBody());
            this.body.appendTo(this.wrap);
            this._bindEvent();
        },


    });
    $.extend($.l.avataruploader, {
        uuid: 0,
        instance: {},
        idGenerator: 0,
        generateId: function() {
            this.uuid++;
            return this.uuid;
        }
    });
}(jQuery));