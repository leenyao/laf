(function() { /*global l*/

    var DataTable = function(element, options) {
            this.options = $.extend({}, DataTable.DEFAULTS, options);
            this.$element = $(element);
            this._params = {

            };
            this.isDataInitialized = false;
            this.sort_type = 'desc';
            this.sort_by = '';
            this.init('datatable', element, options);
            this.getData();
        },
        old = $.fn.datatable,
        emailregexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    function getModalTemplate(config) {
        var html = ['<div class="modal-dialog">',
            '<div class="modal-content">',
            '<div class="modal-header">',
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
            '<h4 class="modal-title" >${title}</h4>',
            '</div>',
            '<div class="modal-body">',
            '${body}',
            '</div>', //<!-- modal-body -->
            '<div class="modal-footer">',
            '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>',
            '<button data-action="OK" type="button" class="btn btn-primary">确定</button>',
            '</div>',
            '</div>',
            '</div>'
        ].join('');
        return html;
    }
    DataTable.prototype = {
        init: function(type, element, options) {
            var me = this;
            me.$element.on('click.' + type, '.sort', function() {
                me.getData();
            });
            if (options.toolbar) {
                me.$toolbar = $('<div />').insertBefore(me.$element);
                me.$toolbar.toolbar({
                    view: this,
                    items: options.toolbar
                });
            }

            me.$element.on('change.' + type, 'select.page-size', function() {
                me.changePageSize($(this).val());
            });
            me.$element.on('click.' + type, 'input[type="checkbox"]', function() {
                var checkAll = me.$element.find('table tr:eq(0) :checkbox')[0];
                if (checkAll === this) {
                    if (checkAll.checked) {
                        me.$element.find('table tr:not(:eq(0)) :checkbox').prop('checked', true);
                    } else {
                        me.$element.find('table tr:not(:eq(0)) :checkbox').prop('checked', false);
                    }
                }
            });
            me.$element.on('click.' + type, '.sortable', function() {
                var sort_type = $(this).hasClass('orderby_asc') ? 'desc' : 'asc',
                    sort_by = $(this).data('key');
                me._params.sort_type = sort_type;
                me._params.sort_by = sort_by;
                me.getData();
            });
            me.$element.on('click.' + type, '[data-action="modal"]', function() {
                var $dialog = $('#modal'),
                    links = $(this).data('links');
                if ($dialog.length < 1) {
                    $dialog = $('<div class="modal fade" id="modal"></div>');
                    $dialog.appendTo(document.body);
                }
                $dialog.html(['<div class="modal-dialog">',
                    '<div class="modal-content">',
                    '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title" >外链</h4>',
                    '</div>',
                    '<div class="modal-body">',
                    '<div class="">', //<!-- form -->
                    $.map(links, function(v) {
                        var html = '<div class="form-group">';
                        html += '<label for="" class="control-label">{label}</label>';
                        html += '<textarea rows="4" class="form-control">{value}</textarea>';
                        html += '</div>';
                        return html.replace(/\{label\}/, v.target).replace(/\{value\}/, v.link);
                    }).join(''),
                    '</div>', //<!-- form -->
                    '</div>', //<!-- modal-body -->
                    '<div class="modal-footer">',
                    '<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>',
                    '</div>',
                    '</div>',
                    '</div>'
                ].join(''));
                $dialog.modal({
                    backdrop: true
                });
                $dialog.modal('show');
            });
            me.$element.on('click.' + type, '[data-action="ajax"]', function() {
                var data = $(this).data();
                $.ajax({
                    url: data.actionUrl,
                    type: 'post',
                    data: {
                        params: JSON.stringify(data.params)
                    },
                    dataType: 'json'
                }).done(function() {
                    me.getData();
                });
            });
            me.$element.on('click.' + type, '[data-action="forward"]', function() {
                var $dialog = $('#modal-forward'),
                    data = $(this).data(),

                    title = "（简历来自牵牛）" + data.params.title + "-" + data.params.name,
                    content = "您好，应聘产品助理的简历。我已查阅，请您评估一下，若觉合适，我们将安排面试，谢谢！",
                    links = $(this).data('links');
                if ($dialog.length < 1) {
                    $dialog = $('<div class="modal fade" id="modal-forward"></div>');
                    $dialog.appendTo(document.body);
                    $dialog.on('click', '.btn-primary', function() {
                        var params = {},
                            emails, count = 0,
                            isValid = true;
                        params.email = $dialog.find('[name="email"]').val();
                        params.resume_id = $dialog.find('[name="resume_id"]').val();
                        params.subject = $dialog.find('[name="subject"]').val();
                        params.content = $dialog.find('[name="content"]').val();
                        emails = params.email.split(';');
                        $.each(emails, function() {
                            count++;
                            if (count > 3) {
                                isValid = false;
                                return false;
                            }
                            if (!emailregexp.test(this)) {
                                $dialog.find('.help-block').show();
                                isValid = false;
                                return false;
                            }
                        });
                        if (isValid) {
                            $.post($dialog.data('postUrl'), {
                                params: JSON.stringify(params)
                            }).done(function() {
                                $dialog.find('[name="email"]').val('');
                                $dialog.find('[name="resume_id"]').val('');
                                $dialog.find('[name="subject"]').val('');
                                $dialog.find('[name="content"]').val('');
                                alert('发送成功');
                            });
                            $dialog.modal('hide');
                        }
                    });
                    $dialog.html(['<div class="modal-dialog">',
                        '<div class="modal-content">',
                        '<div class="modal-header">',
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                        '<h4 class="modal-title">转发简历</h4>',
                        '</div>',
                        '<div class="modal-body">',
                        '<div class="">', //<!-- form -->
                        '<div class="form-group">',
                        '<input name="resume_id" type="hidden">',
                        '<label for="" class="control-label">收件人</label>',
                        '<input class="form-control" name="email" placeholder="最多可添加3个邮箱,请用&quot;;&quot;分割" type="email">',
                        '<span style="display:none;" class="help-block"><span class="text-danger"><i class="glyphicon glyphicon-remove"></i>邮件格式不正确</span></span>',
                        '</div>',
                        '<div class="form-group">',
                        '<label for="" class="control-label">标题</label>',
                        '<input class="form-control" name="subject" type="text" value="">',
                        '</div>',
                        '<div class="form-group">',
                        '<label for="" class="control-label">正文</label>',
                        '<textarea rows="5" class="form-control" name="content" holderplace="请用\";\"分割" type="email">',
                        '</textarea>',
                        '</div>',
                        '</div>', //<!-- form -->
                        '</div>', //<!-- modal-body -->
                        '<div class="modal-footer">',
                        '<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>',
                        '<button type="button" class="btn btn-primary">确定</button>',
                        '</div>',
                        '</div>',
                        '</div>'
                    ].join(''));
                }
                $dialog.find('[name="resume_id"]').val(data.params.resume_id);
                $dialog.find('[name="subject"]').val(title);
                $dialog.find('[name="content"]').val(content);
                $dialog.find('.help-block').hide();
                $dialog.data('postUrl', data.actionUrl);

                $dialog.modal({
                    backdrop: true
                });
                $dialog.modal('show');
            });
            me.$element.on('click.' + type, 'span.tr-trigger', function() {
                var $trBefore = $(this).closest('tr'),
                    //col = $trBefore.find('td').length,
                    $trAfter = $trBefore.next();
                if ($trAfter.is('.tr-trigger-hidden')) {
                    $trAfter.find('td').toggle();
                }
                if ($trAfter.find('td').is(":visible")) {
                    $(this).removeClass('icon-resize-full');
                    $(this).addClass('icon-resize-small');
                }else{
                    $(this).removeClass('icon-resize-small');
                    $(this).addClass('icon-resize-full');
                }
            });
            me.$element.on('click.'+ type ,'.editable-action-log',function(){
                var $dialog = $('#modal-editable-log'),
                    links = $(this).data('url'),
                    postUrl = $(this).attr('add-action-url');
                if ($dialog.length < 1) {
                    $dialog = $('<div class="modal fade" id="modal-editable-log"></div>');
                    $dialog.appendTo(document.body);
                    $dialog.on('show.bs.modal', function(e) {
                        var $this = $(this),
                            link = $this.data('link');
                        if (link) {
                            $.post(link, {
                                params: {}
                            }, function(data) {
                                if (Number(data.status) === 1) {
                                    $this.html(['<div class="modal-dialog">',
                                        '<div class="modal-content">',
                                        '<div class="modal-header">',
                                            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                                            '<h4 class="modal-title" >备注</h4>',
                                        '</div>',
                                        '<div class="modal-body">',
                                        '<form>',
                                            '<div class="form-group">',
                                                '<label for="" class="control-label">添加备注</label>',
                                                '<textarea name="log" rows="4" class="form-control"></textarea>',
                                            '</div>',
                                            '<div class="form-group">',
                                                '<button type="submit" class="btn btn-primary" >添加</button>',
                                            '</div>',
                                        '</form>',
                                        data.data.detail_output,
                                        '</div>', //<!-- modal-body -->
                                        '<div class="modal-footer">',
                                        '<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>',
                                        '</div>',
                                        '</div>',
                                        '</div>'
                                    ].join(''));
                                    me._generateCommand($this.find('table'));
                                }
                            }, 'json');
                        }
                    });
                    
                    $dialog.on('submit','form', function(e) {
                        e.preventDefault();
                        var postData = {log: $(this).find('[name="log"]').val()};
                        $.ajax({
                            url: $dialog.data('postUrl'),
                            type: 'post',
                            data: {
                                log: postData.log,
                                params: JSON.stringify(postData)
                            },
                            dataType: 'json'
                        }).done(function() {
                            $dialog.triggerHandler('show.bs.modal');
                        });
                        $dialog.data('$dirty',true);
                        $dialog.data('$pristine',false);
                    });
                    $dialog.on('click.' + type, '[data-action="ajax"]', function() {
                        var data = $(this).data();
                        $.ajax({
                            url: data.actionUrl,
                            type: 'post',
                            data: {
                                params: JSON.stringify(data.params)
                            },
                            dataType: 'json'
                        }).done(function() {
                            $dialog.triggerHandler('show.bs.modal');
                        });
                        $dialog.data('$dirty',true);
                        $dialog.data('$pristine',false);
                    });
                    $dialog.on('hide.bs.modal',function(){
                        if($dialog.data('$pristine')){
                            return ;
                        }
                        me.getData();
                        $dialog.data('$dirty',false);
                        $dialog.data('$pristine',true);
                    });
                }
                $dialog.find('.modal-body').html('正在加载...');
                $dialog.data('link', links);
                $dialog.data('postUrl',postUrl);
                $dialog.data('$dirty',false);
                $dialog.data('$pristine',true);
                $dialog.modal({
                    backdrop: true,
                    show: false
                });

                $dialog.modal('show');
            });
        },
        _addCheckbox: function() {
            $('tr>th:nth-child(1)', this.$element).before('<th class="td50"><input type="checkbox"></th>');
            $('tr:not(.tr-trigger-nocheckbox,.tr-trigger-hidden)>td:nth-child(1)', this.$element).before('<td><input type="checkbox"></td>');
            // $('tr:not(.nocheckbox)>td:nth-child(1)', this.$element).before('<td><input type="checkbox"></td>');
            $('.tr-trigger-hidden').prev('tr').find('td:eq(0)').attr('rowspan', 2);
            $('span.hidecheckbox', this.$element).closest('tr').find('td:eq(0)').html('');
        },
        _setPk: function() {
            var pkIndex = $('[data-pk]').index();
            this.pk = {
                keyIndex: pkIndex,
                keyName: this.$element.find('tr:eq(0)>[data-pk]').attr('data-key')
            };
        },
        changePageSize: function(pageSize) {
            var me = this;
            me._params.items_per_page = pageSize;
            me.getData({
                //"items_per_page": pageSize,
                "current_page": 0
                //sort_by: this.sort_by,
                //sort_type: this.sort_type
            });
        },
        fixDummyScroller: function() {
            var $wrap = $(this.$element).find('.table-responsive'),
                $table, $dummyWrap, $dummyDiv;
            if ($wrap.length === 0) {
                return;
            }
            $table = $wrap.find('table');
            $dummyWrap = this.$element.find('.dummy-wrap');
            if ($dummyWrap.length > 0) {
                $dummyWrap.css({
                    display: 'block',
                    overflow: 'auto',
                    width: $wrap.width()
                }).find('div').css({
                    display: 'block',
                    height: 1,
                    overflow: 'auto',
                    width: $table.width()
                });
                return;
            }
            $dummyWrap = $('<div class="dummy-wrap"></div>');
            $dummyWrap.css({
                display: 'block',
                overflow: 'auto',
                width: $wrap.width()
            });
            $dummyDiv = $('<div></div>');
            $dummyDiv.css({
                display: 'block',
                height: 1,
                overflow: 'auto',
                width: $table.width()
            });
            $dummyWrap.append($dummyDiv);
            $dummyWrap.insertBefore($wrap);
            $dummyWrap.scroll(function() {
                $wrap.scrollLeft($dummyWrap.scrollLeft());
            });
            $wrap.scroll(function() {
                $dummyWrap.scrollLeft($wrap.scrollLeft());
            });

        },
        renderHeader: function() {},
        resize: function() {},
        createPager: function(page, pageSize, totalCount) {

            var 
                pageCount = Math.ceil(totalCount / pageSize),
                str = ['<div class="pull-left">',
                        '共'+totalCount+'条记录, '+pageCount+'页, 每页',
                        '<select class="form-control input-sm page-size" style="width:auto;display:inline;margin:20px 0;">',
                        '<option value="10"' + (pageSize === 10 ? ' selected' : '') + '>10</option>',
                        '<option value="20"' + (pageSize === 20 ? ' selected' : '') + '>20</option>',
                        '<option value="50"' + (pageSize === 50 ? ' selected' : '') + '>50</option></select>条记录</div>',

                        '<ul class="pager page page-square pull-right">'
                    ].join(""),
                displayCount = 5,
                halfOfDisplayCount = Math.floor(displayCount / 2),
                displayStart = page - halfOfDisplayCount,
                displayEnd = page + halfOfDisplayCount,
                isCutPrev = displayStart - 1 > 1,
                isCutNext = displayEnd + 1 < pageCount,
                hasFirstPage = displayStart > 1,
                hasLastPage = displayEnd < pageCount,
                pageArray = [],
                i;

            if (displayStart < 1) {
                displayEnd = Math.min(displayEnd - displayStart + 1, displayCount);
                displayStart = 1;

            }
            if (displayEnd >= pageCount) {
                displayStart = Math.max(1, displayStart - displayEnd + pageCount);
                displayEnd = pageCount;

            }
            hasFirstPage = displayStart > 1;
            isCutPrev = displayStart - 1 > 1;
            hasLastPage = displayEnd < pageCount;
            isCutNext = displayEnd + 1 < pageCount;
            //page prev
            if (page > 1) {
                str += '<li data-page="' + (page - 1 - 1) + '"><a href="javascript:void(0);"><b class="left_next"></b></a></li>';
            }
            if (hasFirstPage) {
                str += '<li data-page="0"><a href="javascript:void(0);">1</a></li>';
            }
            if (isCutPrev) {
                str += '<li>...</li>';
            }


            for (i = displayStart; i <= displayEnd; i++) {
                if (i < 1) {
                    continue;
                }
                if (i > pageCount) {
                    break;
                }
                if (i === page) {
                    str += '<li class="active"><a href="javascript:void(0);">' + page + '</a></li>';
                } else {
                    str += '<li data-page="' + (i - 1) + '"><a href="javascript:void(0);">' + i + '</a></li>';
                }
            }
            if (isCutNext) {
                str += '<li>...</li>';
            }
            if (hasLastPage) {
                str += '<li data-page="' + pageCount + '"><a href="javascript:void(0);">' + pageCount + '</a></li>';
            }
            if (page < pageCount) {
                str += '<li data-page="' + page + '"><a href="javascript:void(0);"><b class="right_next"></b></a></li>';
            }
            str += '</ul>';

            return str;
        },
        _generateCommand: function($table){
            $table.find('span').each(function() {
                var $this = $(this),
                    that = this,
                    command = $this.data('command');
                if (command) {
                    var nIndex=0;
                    $this.append('<br/>');
                    $.each(command, function(k, v) {
                        var attributes = '',
                            $target,target,
                            link, data;
                        if (v.attributes) {
                            attributes = $.map(v.attributes, function(v, k) {
                                return k + '="' + v + '"';
                            }).join(' ');
                        }
                        target = v['target-td'];
                        if(target){
                            $target = $(that).closest('tr').find('[data-key="'+target+'"]');
                            if($target.length > 0){
                                if($target.find('.command').length === 0){
                                    $target.append('<div class="command"></div>');
                                }
                                $target = $target.find('.command');
                            }  else{
                                //$target = $(that);
                                return;
                            }
                            
                        } else {
                            $target = $(that);
                        }
                        nIndex++;
                        sBrTag = nIndex%2==0 ? "<br />" : "";
                        switch (v.type) {
                            case 'url':
                                link = '<a href="' + v.data + '" ' + attributes + ' style="    padding-right: 10px;" >' + v.label +sBrTag+ '</a>';
                                $target.append(link);
                                break;
                            case 'code-dialog':
                                link = $('<a data-action="modal" href="javascript:void(0);" ' + attributes + '  style="    padding-right: 10px;">' + v.label + '</a>'+sBrTag);
                                $target.append(link, '&nbsp;', '&nbsp;');
                                link.data('links', v.data);
                                break;
                            case 'action-ajax-reload':
                                link = $('<a data-action="ajax" href="javascript:;" ' + attributes + '>' + v.label + '</a>'+sBrTag);
                                link.data('actionUrl', v['action-url']);
                                link.data('params', v.params);
                                $target.append(link);
                                break;
                            case 'forward-dialog':
                                link = $('<a data-action="forward" href="javascript:;" ' + attributes + '>' + v.label + '</a>'+sBrTag);
                                link.data('actionUrl', v.data);
                                link.data('params', v.params);
                                $target.append(link);
                                break;
                        }

                    });
                }

            });

            // 子命令的收起/展开
            $('.collapsible_command').toggle();
            $('.command-set').click(function(event) {
                if ($(this).hasClass('icon-chevron-down')) {
                    $(this).removeClass('icon-chevron-down')
                    $(this).addClass('icon-chevron-up')
                }else{
                    $(this).removeClass('icon-chevron-up')
                    $(this).addClass('icon-chevron-down')
                }
                $(this).parents('td').find('.collapsible_command').toggle();
            });
        },
        getData: function(param) {

            var me = this,data = {};
            param = param || {};
            param = $.extend({
                current_page: 0
            }, this._params, param);
            me.$toolbar.trigger('viewupdate', param);
            if(!me.isDataInitialized){
                data.initflag = 1;
                me.isDataInitialized = true;
            }
            // console.log(data)

            data.params = JSON.stringify(param);
            $.ajax({
                url: me.options.source,
                type: 'post',
                data: data,
                dataType: 'json'
            }).done(function(res) {

                var page = res.data.detail_page,
                    pager = '',
                    $table;
                if (page) {
                    pager = me.createPager(page.current_page + 1, page.items_per_page, page.total);
                    me.$element.html('<p class="data-count">共' + page.total + '条</p>' + res.data.detail_output + pager);
                } else {
                    me.$element.html(res.data.detail_output);
                }

                $table = $('table', me.$element);
                $table.addClass("table-bordered");
                //$table.addClass("table-striped");
                if (res.data.checkboxFlag) {
                    me._addCheckbox();
                }
                me._setPk();
                me._generateCommand($table);
                me.$element.find('.pager li:not(.active):not(.disabled)').on('click', function() {
                    me.getData({
                        //"items_per_page": '',
                        "current_page": $(this).data('page')
                        //,sort_by: me.sort_by,
                        //sort_type: me.sort_type
                    });
                });
                me.fixDummyScroller();
            });
        },
        getSelection: function() {
            var selection = [],
                index = this.pk.keyIndex;
            this.$element.find('table tr:not(:eq(0)) :checkbox:checked').each(function() {
                selection.push($(this).closest("tr").find('td').eq(index).attr("val"));
            });
            return selection;
        }

    };
    DataTable.DEFAULTS = {
        pageSize: 10,
        renderer: function() {
            return "";
        },
        colunms: []
    };

    $.fn.dataTable = function(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('l.datatable'),
                options = typeof option === 'object' && option;

            if (!data) {
                $this.data('l.datatable', (data = new DataTable(this, options)));
            }
            if (typeof option === 'string') {
                data[option]();
            }
        });
    };
    $.fn.dataTable.noConflict = function() {
        $.fn.dataTable = old;
        return this;
    };
    $.fn.dataTable.Constructor = DataTable;
}());
(function() {

    $.fn.toolbar = function(option) {
        var view = option.view;
        return this.each(function() {
            var element = $(this),
                html = '<div class="row-box clearfix">';
            element.addClass('toolbar');

            $.each(option.items, function() {
                var params = this.params,
                    selectedIndex;

                // 默认选中的值
                if (typeof(params)!="undefined" && typeof(params.object_name)!="undefined" && typeof(params.value)!="undefined") {
                    view._params[params.object_name] = params.value;
                }
                switch (this.type) {
                    case 'button-add':
                        html += ['<a class="btn btn-default btn-add ' + (this.cls || '') + ' pull-' + this.position + '"',
                            ' href="', params.url, '"',
                            this.confirm ? 'data-confirm="' + this.confirm + '" ' : '', '>',
                            '<span class="', this.iconCls || "", '"></span>' + params.label + '</a>'
                        ].join('');
                        break;
                    case 'text':
                        html += '<span class="pull-' + this.position + '">' + params.label + '</span>';
                        break;
                    case 'detail-button-action':
                    case 'ajax-button-action':
                        html += ['<button ',
                            'data-target="' + (params.object_name || '') + '" ',
                            'class="btn btn-default ' + (this.cls || '') + ' btn-action '+this.type+' pull-' + this.position + '" ',
                            this.confirm ? 'data-confirm="' + this.confirm + '" ' : '',
                            params['confirm-url'] ? 'data-confirm-url="' + params['confirm-url'] + '" ' : '',
                            'data-action-url="' + params['action-url'] + '">',
                            '<span class="', this.iconCls || "", '">' + params.label + '</span></button>'
                        ].join('');
                        break;
                    case 'customdownload-dialog':
                        html += ['<button ',
                            // 'data-target="' + (params.object_name || '') + '" ',
                            'class="btn btn-default ' + (this.cls || '') + ' btn-customdownload-dialog pull-' + this.position + '" ',
                            this.confirm ? 'data-confirm="' + this.confirm + '" ' : '',
                            'data-params=\'', JSON.stringify(params), '\' ',
                            // 'data-param-name="', params['data-param-name'], '" ',
                            // 'data-action-url="' + params['action-url'] + '">',
                            '<span class="', this.iconCls || "", '">' + params.label + '</span></button>'
                        ].join('');
                        break;
                    case 'detail-button-dialog-action':
                        html += ['<button ',
                            'data-target="' + (params.object_name || '') + '" ',
                            'class="btn btn-default ' + (this.cls || '') + ' btn-action-dialog pull-' + this.position + '" ',
                            this.confirm ? 'data-confirm="' + this.confirm + '" ' : '',
                            'data-tips=\'', JSON.stringify(params['data-tips']), '\' ',
                            'data-param-name="', params['data-param-name'], '" ',
                            'data-action-url="' + params['action-url'] + '">',
                            '<span class="', this.iconCls || "", '">' + params.label + '</span></button>'
                        ].join('');
                        break;
                    case 'detail-button-dialog-actiontwo':
                        html += ['<button ',
                            'data-target="' + (params.object_name || '') + '" ',
                            'class="btn btn-default ' + (this.cls || '') + ' btn-action-dialogtwo pull-' + this.position + '" ',
                            this.confirm ? 'data-confirm="' + this.confirm + '" ' : '',
                            'data-param-name="', params['data-param-name'], '" ',
                            'data-action-url="' + params['action-url'] + '">',
                            '<span class="', this.iconCls || "", '">' + params.label + '</span></button>'
                        ].join('');
                        break;
                    case 'detail-button-send-msg':
                        html += ['<button ',
                            'data-target="' + (params.object_name || '') + '" ',
                            'class="btn btn-default ' + (this.cls || '') + ' btn-send-msg pull-' + this.position + '" ',
                            this.confirm ? 'data-confirm="' + this.confirm + '" ' : '',
                            'data-param-name="', params['data-param-name'], '" ',
                            'data-action-url="' + params['action-url'] + '">',
                            '<span class="', this.iconCls || "", '">' + params.label + '</span></button>'
                        ].join('');
                        break;
                    case 'detail-br':
                        html += '</div>';
                        html += '<div class="row-box clearfix">';
                        break;
                    case 'detail-hr':
                        html += '</div>';
                        html += '<div class="horizon-row clearfix">';
                        break;
                    case 'detail-select-action':
                    // console.log(params)
                        var sSelectedClass = params.value&&params.value!=0 ? "toolbarseleted" : "";
                        var sMutiple = params.bMutiple ? 'multiple="multiple"' : '';

                        var sTitle = params.bMutiple ? 'title="按住ctrl键多选 (mac是command键)"' : '';
                        html += '<select '+sMutiple+' '+sTitle+' data-target="' + params.object_name + '" style="width:auto;" class="form-control pull-' + this.position + ' '+sSelectedClass+'">';
                        $.each(params.options, function(k, v) {
                            var sOptionKey=k.replace("___REPLACEBLANK___", "");


                    // $(this).removeClass("toolbarseleted");


                            html += '<option value="' + sOptionKey + '" '+(sOptionKey==params.value ? 'selected' : '')+'>' + v + '</option>';
                        });
                        html += '</select>';
                        break;
                    case 'detail-tabfilter-action':
                        html += '<dl data-target="' + params.object_name + '" class="filter ' + (this.cls || '') + ' pull-' + this.position + '">';
                        html += '<dt class="filter-label">' + params.label + '</dt>';
                        selectedIndex = Number(params.selected);
                        if (isNaN(selectedIndex)) {
                            selectedIndex = 0;
                        }
                        $.each(params.options, function(k, v) {
                            if (k === selectedIndex) {
                                html += '<dd class="filter-item active" value="' + k + '">' + v + '</dd>';
                            } else {
                                html += '<dd class="filter-item" value="' + k + '">' + v + '</dd>';
                            }

                        });
                        html += '</dl>';
                        break;
                    case 'detail-searchbar':
                        html += [ //'<form class="">',
                            '<div style="width:200px;" class="input-group pull-' + this.position + '">',
                            '<input type="search" data-target="' + params.object_name + '" placeholder="'+(params.placeholder||"")+'" class="form-control">',
                            '<span class="input-group-btn">',
                            '<button class="btn btn-default btn-search" type="button"><span class="glyphicon glyphicon-search"></span></button>',
                            '</span>',
                            '</div>',
                            '<div class="clearfilter pull-'+ this.position +'" style="padding:10px"><a href="javascript:void(0);">清除过滤条件</a></div>',
                            //'</form>'
                        ].join('');
                        break;
                    case 'datepicker':
                        html += '<input name="' + params.name + '" type="search" style="width:200px;" data-default-date="' + params.defaultDate + '" class="form-control datepicker pull-' + this.position + '">';
                        break;
                    case 'link':
                        html += [
                            '<a href="', params.link, '" ',
                            ((params.attributes) ? 
                            $.map(params.attributes, function(v, k) {
                                return k + '="' + v + '"';
                            }).join(' '):""), ' class="pull-' + this.position + ' toolbar-link">',
                            params.label,
                            '</a>'
                        ].join('');
                        break;
                    case 'detail-button-download':
                        html += [
                            '<button data-type="download" data-href="', params['data-source'] || view.options.source, '" ',
                            'data-request=\'', JSON.stringify(params.request), '\'',
                            ' class="btn btn-default pull-' + this.position + '">',
                            '<span class="', this.iconCls || "", '">' + params.label + '</span>',
                            '</button>'
                        ].join('');
                        break;
                    case 'detail-custom-html':
                        html += [
                            '<div class="custom-item pull-' + this.position + '">',
                            params.content,
                            '</div>'
                        ].join('');
                        break;
                    case 'detail-range-datepicker':
                        html += '<input name="' + params.object_name + '" type="text"  style="width:175px;"' +
                            ' data-target="' + params.object_name + '" data-start-date="' + params.date_selected[0] + '" data-end-date="' + params.date_selected[1] + '"' +
                            ' data-min-date="' + params.date_range[0] + '" data-max-date="' + params.date_range[1] + '"' +
                            ' class="date-range-picker form-control pull-' + this.position + '">';
                        break;
                    case 'pipe':
                        html += '<span class="pipe pull-left">|</span>';
                        break;
                }

            });
            html += '</div>';
            element.append(html);
            element.find('.datepicker').each(function() {
                var defaultValue = $(this).data('defaultDate');
                $(this).val(defaultValue);
                $(this).datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: defaultValue
                });
            });
            element.find('.date-range-picker').each(function() {
                $(this).daterangepicker({
                    ranges: {
                        '今天': [moment(), moment()],
                        '最近3天': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        '最近7天': [moment().subtract('days', 6), moment()],
                        '最近30天': [moment().subtract('days', 29), moment()],
                        '最近一个月': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },

                    locale: {
                        applyLabel: '应用',
                        cancelLabel: '取消',
                        fromLabel: '开始',
                        toLabel: '结束',
                        weekLabel: '周',
                        customRangeLabel: '自定义',
                        daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
                        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月', ],
                        firstDay: 0
                    },
                    format: 'YYYY-MM-DD',
                    minDate: $(this).data('minDate'),
                    maxDate: $(this).data('maxDate'),
                    startDate: $(this).data('startDate'),
                    endDate: $(this).data('endDate')
                }).val($(this).data('startDate') + ' - ' + $(this).data('endDate'));
            });
            element.on('viewupdate', function(e, params) {
                $('[data-type="download"]').each(function() {
                    var requestParam = $.extend({}, params, $(this).data('request'));
                    $(this).data('requestParam', requestParam);
                });
            });
            $('[data-type="download"]', element).on('click', function() {
                var form = $('<form action="' + $(this).data('href') + '" method="post"/>'),
                    input = $('<input type="hidden" name="params"/>'),
                    requestParam = $(this).data('requestParam'),
                    object_name, data = {};
                requestParam = $.extend({}, requestParam, $(this).data('request'));
                object_name = view.pk.keyName;
                data[object_name] = view.getSelection();
                // console.log(requestParam)
                requestParam = $.extend(requestParam, data);
                input.val(JSON.stringify(requestParam));
                //$.post( $(this).data('href'), {params:JSON.stringify(requestParam)});
                form.append(input);
                form.appendTo('body');
                form.submit();
                form.remove();

            });
            element.on('change', '.datepicker', function() {
                element.find('.datepicker').each(function() {
                    view._params[$(this).attr('name')] = $(this).val();
                });
                view.getData();
            });
            element.on('apply.daterangepicker', '.date-range-picker', function(e, picker) {
                var dateRange = [];
                dateRange[0] = picker.startDate.format('YYYY-MM-DD');
                dateRange[1] = picker.endDate.format('YYYY-MM-DD');
                view._params[$(this).attr('name')] = dateRange;
                view.getData();
            });
            element.on('change', 'select', function() {
                view._params[$(this).attr('data-target')] = $(this).val();
                if ($(this).val()==0) {
                    $(this).removeClass("toolbarseleted");
                }else{
                    $(this).addClass("toolbarseleted");
                }
                // 
                view.getData();
            });
            element.on('click', 'dd.filter-item', function() {
                var filter = $(this).closest('.filter');
                filter.find('dd').removeClass('active');
                $(this).addClass('active');
                view._params[filter.attr('data-target')] = $(this).attr('value');
                view.getData();
            });
            element.on('click', 'button.btn-search', function() {
                var $input = $(this).parent().prev('input');
                view._params[$input.attr('data-target')] = $input.val();
                view.getData();
            });
            element.on('keydown', '[type="search"]', function(e) {
                if (e.keyCode === $.ui.keyCode.ENTER || e.keyCode === $.ui.keyCode.NUMPAD_ENTER) {
                    view._params[$(this).attr('data-target')] = $(this).val();
                    view.getData();
                }
            });
            element.on('click', 'div.clearfilter', function(e) {
                window.location.reload()
            });


            element.on('click', '.btn-add', function(e) {
                var confirmable = $(this).data('confirm'),
                    isConfirm = true,
                    dialog,
                    callback;

                if (confirmable) {
                    dialog = l.dialog({
                        body: confirmable,
                        buttons: [{
                            text: '马上认证',
                            cls: 'btn btn-primary',
                            scope: this,
                            handler: function(btn, dialog) {
                                dialog.modal('hide');
                                window.location.href = $(this).attr('href');
                            }
                        }, {
                            text: '暂不认证',
                            cls: 'btn btn-default',
                            attr: {
                                'data-dismiss': 'modal'
                            }
                        }]
                    });
                    dialog.modal('show');
                } else {
                    return;
                }
                e.preventDefault();
                return false;
            });
            element.on('click', 'button.btn-action', function() {
                var object_name = $(this).attr('data-target'),
                    confirmable = $(this).data('confirm'),
                    confirmUrl = $(this).data('confirmUrl'),
                    isConfirm = true,
                    dialog,
                    callback,
                    index = -1;
//                ajax-button-action
                
                if ($("button.btn-action").hasClass('ajax-button-action'))
                {
                }else if (view.getSelection().length < 1) {
                    return false;
                }
                callback = function() {
                    var data = {};
                    if (!object_name) {
                        object_name = view.pk.keyName;
                        data[object_name] = view.getSelection();
                    } else {
                        data[object_name] = [];
                        index = view.$element.find('tr:eq(0)>[data-key="' + object_name + '"]').index();
                        view.$element.find('table tr:not(:eq(0)) :checkbox:checked').each(function() {
                            data[object_name].push($(this).closest("tr").find('td').eq(index).attr("val"));
                        });
                    }
//                    if (data[object_name].length > 0) {
                        $.ajax({
                            url: $(this).attr('data-action-url'),
                            type: 'post',
                            data: {
                                params: JSON.stringify(data)
                            },
                            dataType: 'json'
                        }).done(function() {
                            view.getData();
                        });
//                    }
                };

                if (confirmable) {
                    if (confirmUrl) {
                        dialog = l.dialog({
                            title: confirmable,
                            buttons: [{
                                text: '确定',
                                cls: 'btn btn-primary',
                                scope: this,
                                handler: function(btn, dialog) {
                                    callback.call(this);
                                    dialog.modal('hide');
                                }
                            }, {
                                text: '取消',
                                cls: 'btn btn-default',
                                attr: {
                                    'data-dismiss': 'modal'
                                }
                            }]
                        });
                        dialog.on('shown.bs.modal', function() {
                            var data = {};
                            data[view.pk.keyName] = view.getSelection();
                            l.ajax(confirmUrl, data).done(function(res) {
                                dialog.find('.modal-body').html(res.data);
                            });
                        });
                        dialog.modal('show');
                        isConfirm = false;
                    } else {
                        isConfirm = window.confirm(confirmable);
                    }
                    if (isConfirm) {
                        callback.call(this);
                    }
                } else {
                    callback.call(this);
                }

            });
            element.on('click', '.btn-customdownload-dialog', function() {

                var $dialog = $('#modal-action-dialog'),
                    aParams = ($(this).data('params')),
                    data = {};

                    // console.log(aParams)
                $dialog.remove();
                $dialog = $('<div class="modal fade" id="modal-action-dialog" style="z-index:10001"></div>');


                $dialog.appendTo(document.body);
                $dialog.data('actionUrl', $(this).attr('data-action-url'));
                $dialog.html([
                    '<div class="modal-dialog">',
                    '<div class="modal-content">',
                        '<div class="modal-header">',
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                        '<h4 class="modal-title" >'+(aParams['dialog-title']||'')+'</h4>',
                        '</div>',
                        '<div class="modal-body">',
                            '<div class="form-horizontal">', //<!-- form -->
                                '<div class="form-group"><form>正在加载表格...</form></div>',
                            '</div>', //<!-- form -->
                        '</div>', //<!-- modal-body -->
                        '<div class="modal-footer">',
                            '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>',
                            '<button data-action="OK" type="button" class="btn btn-primary">确定</button>',
                        '</div>',
                    '</div>',
                    '</div>'
                ].join(''));

                // 加载内容
                if (aParams['content-url']) {
                    $.ajax({
                        url: aParams['content-url'],
                        dataType: 'html'
                    })
                    .done(function(res) {
                        $dialog.find('.modal-body .form-group form').html(res);

                        console.log(view.options.source);
                        // console.log($dialog.find('.modal-body .form-group form').serializeArray());
                        //加载点击事件
                        $dialog.on('click', '.btn-primary', function() {
                            $dialog.modal('hide');

                            var form = $('<form action="' + view.options.source + '" method="post"/>'),
                                input = $('<input type="hidden" name="params"/>'),
                                requestParam = $(this).data('requestParam'),
                                object_name, data = {};
                            requestParam = $.extend({}, requestParam, $(this).data('request'), {
                                'customfields':$dialog.find('.modal-body .form-group form').serializeArray(),
                                'returnType': 'csv_all'
                            });
                        console.log(requestParam);

                            object_name = view.pk.keyName;
                            data[object_name] = view.getSelection();
                            // console.log(requestParam)
                            requestParam = $.extend(requestParam, data);
                            input.val(JSON.stringify(requestParam));
                            //$.post( $(this).data('href'), {params:JSON.stringify(requestParam)});
                            form.append(input);
                            form.appendTo('body');
                            form.submit();
                            form.remove();


                        });

                    });
                    
                }



                $dialog.modal({
                    backdrop: true
                });
                $dialog.modal('show');
            });
            element.on('click', '.btn-action-dialogtwo', function() {
                var $dialog = $('#modal-action-dialog'),
                    paramName = $(this).data('paramName'),
                    object_name = $(this).data('target') || view.pk.keyName,
                    index = -1,
                    data = {};
                $dialog.remove();
                if (view.getSelection().length < 1) {
                    return false;
                }
                $dialog = $('<div class="modal fade" id="modal-action-dialog"></div>');
                $dialog.on('click', '.btn-primary', function() {
                    var $select = $dialog.find('select'),
                        selectValue = $select.val();
                    data[object_name] = [];
                    index = view.$element.find('tr:eq(0)>[data-key="' + object_name + '"]').index();
                    view.$element.find('table tr:not(:eq(0)) :checkbox:checked').each(function() {
                        data[object_name].push($(this).closest("tr").find('td').eq(index).attr("val"));
                    });
                    // if (selectValue !== '其它') {
                    //     // data[paramName] = $select.val();
                    // } else {
                        data[paramName] = $dialog.find('textarea').val();
                    // }
                    $dialog.modal('hide');
                    // $.each(view.selection,function(){
                    //     data.push(this[object_name]);
                    // });
                    $.ajax({
                        url: $dialog.data('actionUrl'),
                        type: 'post',
                        data: {
                            params: JSON.stringify(data)
                        },
                        dataType: 'json'
                    }).done(function() {
                        view.getData();
                    });
                });
                $dialog.appendTo(document.body);

                $dialog.data('actionUrl', $(this).attr('data-action-url'));
                $dialog.html(['<div class="modal-dialog">',
                    '<div class="modal-content">',
                    '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title" >评论</h4>',
                    '</div>',
                    '<div class="modal-body">',
                    '<div class="form-horizontal">', //<!-- form -->
                    '<div class="form-group" style="display:none">',
                    '<label for="" class="col-sm-2 control-label">标签:</label>',
                    '<div class="col-sm-10">',
                    // '<select name="' + $(this).data('paramName') + '">',
                    // // $.map($(this).data('tips'), function(v, k) {
                    // //     return '<option value="' + k + '">' + v + '</option>';
                    // // }).join(''),
                    // '</select>',
                    '</div>',
                    '</div>', //<!-- form-grop -->
                    '<div class="form-group">',
                    '<label for="customreason" class="col-sm-2 control-label">内容:</label>',
                    '<div class="col-sm-10">',
                    '<textarea id="customreason" name="' + $(this).data('paramName') + '" class="form-control" placeholder="请填写您要的内容。"></textarea>',
                    '</div>',
                    '</div>', //<!-- form-grop -->
                    '</div>', //<!-- form -->
                    '</div>', //<!-- modal-body -->
                    '<div class="modal-footer">',
                    '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>',
                    '<button data-action="OK" type="button" class="btn btn-primary">确定</button>',
                    '</div>',
                    '</div>',
                    '</div>'
                ].join(''));
                $dialog.find('select').tagselect();
                $dialog.find('select').on('change', function() {
                    if (this.value === "其它") {
                        $dialog.find('.form-group:eq(1)').show();
                    } else {
                        $dialog.find('.form-group:eq(1)').hide();
                    }
                });

                $dialog.modal({
                    backdrop: true
                });
                $dialog.modal('show');
            });
            element.on('click', '.btn-send-msg', function() {
                var $dialog = $('#modal-send-msg'),
                    paramName = $(this).data('paramName'),
                    object_name = $(this).data('target') || view.pk.keyName,
                    index = -1,
                    data = {};
                $dialog.remove();
                $dialog = $('<div class="modal fade" id="modal-send-msg"></div>');
                $dialog.on('click', '.btn-primary', function() {
                    data[object_name] = [];
                    index = view.$element.find('tr:eq(0)>[data-key="' + object_name + '"]').index();
                    view.$element.find('table tr:not(:eq(0)) :checkbox:checked').each(function() {
                        data[object_name].push($(this).closest("tr").find('td').eq(index).attr("val"));
                    });

                    data[paramName] = $dialog.find('textarea').val();
                    $dialog.modal('hide');
                    $.ajax({
                        url: $dialog.data('actionUrl'),
                        type: 'post',
                        data: {
                            params: JSON.stringify(data)
                        },
                        dataType: 'json'
                    }).done(function() {
                        view.getData();
                    });
                });
                $dialog.appendTo(document.body);

                //$dialog.empty();
                $dialog.data('actionUrl', $(this).attr('data-action-url'));
                $dialog.html(['<div class="modal-dialog">',
                    '<div class="modal-content">',
                    '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title" >通知</h4>',
                    '</div>',
                    '<div class="modal-body">',
                    '<div class="form-horizontal">', //<!-- form -->
                    '<div class="form-group" >',
                    '<label for="custommessage" class="col-sm-2 control-label">描述信息:</label>',
                    '<div class="col-sm-10">',
                    '<textarea id="custommessage" class="form-control" placeholder="请输入您要对用户说的话。例如:感谢您参与本次活动。"></textarea>',
                    '</div>',
                    '</div>', //<!-- form-grop -->
                    '</div>', //<!-- form -->
                    '</div>', //<!-- modal-body -->
                    '<div class="modal-footer">',
                    '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>',
                    '<button data-action="OK" type="button" class="btn btn-primary">确定</button>',
                    '</div>',
                    '</div>',
                    '</div>'
                ].join(''));
                $dialog.modal({
                    backdrop: true
                });
                $dialog.modal('show');
            });
        });
    };
}());