(function() { /*global l*/

    var DataTable = function(element, options) {
            this.options = $.extend({}, DataTable.DEFAULTS, options);
            this.$element = $(element);
            this.isDataInitialized = false;
            this._params = {

            };
            this.sort_type = 'desc';
            this.sort_by = '';
            this.init('datatable', element, options);
            this.getData();
        },
        old = $.fn.datatable,
        emailregexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    DataTable.prototype = {
        init: function(type, element, options) {
            var me = this;
            me.$element.on('click.' + type, '.sort', function() {
                me.getData();
            });
            if (options.toolbar) {
                me.$toolbar = $('<div />').insertBefore(me.$element);
                me.$toolbar.toolbar({
                    view: this,
                    items: options.toolbar
                });
            }

            me.$element.on('change.' + type, 'select.page-size', function() {
                me.changePageSize($(this).val());
            });

            me.$element.on('click.' + type, '.sortable', function() {
                var sort_type = $(this).hasClass('orderby_asc') ? 'desc' : 'asc',
                    sort_by = $(this).data('key');
                me._params.sort_type = sort_type;
                me._params.sort_by = sort_by;
                me.getData();
            });
            me.$element.on('click.' + type, '[data-action="inapposite"]', function() {
                var params = $(this).data('params'),
                    $dataTag  = $(this).closest('li'),
                    mailto = $dataTag.data('resumeMail'),
                    paramsJson = $.parseJSON(decodeURI(params)),
                    url  =$(this).data('actionUrl'),
                    form = {
                        title: '拒绝面试',
                        url: url,
                        fields: [{
                            label: '通知平台',
                            type: 'static',
                            name: 'platform',
                            value:'邮箱'
                        },{
                            label: '收件人',
                            type: 'static',
                            value: mailto,
                            name: 'mailto'
                        },{
                            label: '拒绝理由',
                            name: 'memo',
                            type: 'textarea',
                            value: '您的简历已收到，但目前您的工作经历与该职位不是很匹配，因此很抱歉地通知您无法进入面试。'+
                                '希望您能理解及接受我们的面试反馈结果。尽管此次我们暂时未能携手合作，'+
                                '但我们相信：您肯定能找到一个更合适自己发展的平台，不断成长为行业精英，成就自己的梦想。祝职业更上一层楼！',
                            required: true
                        }],
                        primaryHandler: function(btn){
                            btn.attr('disabled','disabled');
                        },
                        callback: function(dialog,status){
                            dialog.find('.btn-primary').removeAttr('disabled');
                            if(status === '1'){
                                me.getData();
                                dialog.modal('hide');
                            }
                        }
                    };
                $.each(paramsJson,function(k,v){
                    form.fields.push({
                        type:'hidden',
                        value:v,
                        name:k
                    });
                });
                $.toggleModalForm(form);
                
            });
            me.$element.on('click.' + type, '[data-action="interview"]', function() {
                var params = $(this).data('params'),
                    paramsJson = $.parseJSON(decodeURI(params)),
                    $dataTag  = $(this).closest('li'),
                    mailto = $dataTag.data('resumeMail'),
                    companyTitle = $dataTag.data('companyTitle'),
                    companyContacter = $dataTag.data('companyContacter'),
                    companyPhone = $dataTag.data('companyPhone'),
                    location = $dataTag.data('location'),
                    positionTitle = $dataTag.find('h3 > a').text(),
                    url  = $(this).data('actionUrl'),
                    form = {
                        title: '通知面试',
                        url: url,
                        fields: [{
                            label: '通知平台',
                            required: true,
                            name: 'platform',
                            type: 'select',
                            minCount: 1,
                            multiple: true,
                            value:['email','app','sms'],
                            data: [{
                                label: '邮箱',
                                value: 'email'
                            }, {
                                label: '牵牛APP',
                                value: 'app'
                            }, {
                                label: '短信',
                                value: 'sms'
                            }]
                        },{
                            label: '收件人',
                            type: 'static',
                            value: mailto,
                            name: 'mailto'
                        }, {
                            label: '标题',
                            name: 'title',
                            required: true,
                            value: companyTitle+':'+positionTitle+'面试通知'
                        }, {
                            label: '面试时间',
                            name: 'when',
                            required: true,
                            type: 'time'
                        }, {
                            label: '面试地点',
                            name: 'where',
                            maxLength: 15,
                            value: location,
                            required: true
                        }, {
                            label: '联系人',
                            name: 'contacts',
                            maxLength: 4,
                            value: companyContacter,
                            required: true
                        }, {
                            label: '联系电话',
                            name: 'phone',
                            value: companyPhone,
                            required: true
                        }, {
                            label: '备注',
                            name: 'remarks',
                            type: 'textarea',
                            value: '请您准时参加面试，如与特殊情况，请提前来电或回复邮件'
                        }],
                        primaryHandler: function(btn){
                            btn.attr('disabled','disabled');
                        },
                        callback: function(dialog,status){
                            dialog.find('.btn-primary').removeAttr('disabled');
                            if(status === '1'){
                                me.getData();
                                dialog.modal('hide');
                            }
                        }
                    };
                $.each(paramsJson,function(k,v){
                    form.fields.push({
                        type:'hidden',
                        value:v,
                        name:k
                    });
                });
                $.toggleModalForm(form);
            });
            me.$element.on('click.' + type, '[data-action="download"]', function() {
                var data = $(this).data(),
                    url = decodeURI(data.actionUrl),
                    params = decodeURI(data.params),
                    form = $('<form action="' + url + '" method="post"/>'),
                    input = $('<input type="hidden" name="params"/>');
                input.val(params);
                //$.post( $(this).data('href'), {params:JSON.stringify(requestParam)});
                form.append(input);
                form.appendTo('body');
                form.submit();
                form.remove();
            });
            me.$element.on('click.' + type, '[data-action="action-ajax-reload"]', function() {
                var data = $(this).data(),
                    params = decodeURI(data.params);
                $.ajax({
                    url: decodeURI(data.actionUrl),
                    type: 'post',
                    data: {
                        params: params
                    },
                    dataType: 'json'
                }).done(function() {
                    me.getData();
                });
            });
            me.$element.on('click.' + type, '.forward', function() {
                var $dialog = $('#modal-forward'),
                    command = $(this).data('command'),
                    task__id = $(this).closest('li').data('taskId'),
                    data = command.forward.data,
                    title = "（简历来自牵牛）" + data.params.title + "-" + data.params.name,
                    content = "您好，应聘" + data.params.title + "的简历。我已查阅，请您评估一下，若觉合适，我们将安排面试，谢谢！";
                if ($dialog.length < 1) {
                    $dialog = $('<div class="modal fade" id="modal-forward"></div>');
                    $dialog.appendTo(document.body);
                    $dialog.on('click', '.btn-primary', function() {
                        var params = {},
                            emails, count = 0,
                            isValid = true;
                        params.email = $dialog.find('[name="email"]').val();
                        params.task_id = $dialog.find('[name="task_id"]').val();
                        params.resume_id = $dialog.find('[name="resume_id"]').val();
                        params.subject = $dialog.find('[name="subject"]').val();
                        params.content = $dialog.find('[name="content"]').val();
                        emails = params.email.split(';');
                        $.each(emails, function() {
                            count++;
                            if (count > 3) {
                                isValid = false;
                                return false;
                            }
                            if (!emailregexp.test(this)) {
                                $dialog.find('.help-block').show();
                                isValid = false;
                                return false;
                            }
                        });
                        if (isValid) {
                            $.post($dialog.data('postUrl'), {
                                params: JSON.stringify(params)
                            }).done(function() {
                                $dialog.find('[name="email"]').val('');
                                $dialog.find('[name="resume_id"]').val('');
                                $dialog.find('[name="subject"]').val('');
                                $dialog.find('[name="content"]').val('');
                                alert('发送成功');
                            });
                            $dialog.modal('hide');
                        }
                    });
                    $dialog.html(['<div class="modal-dialog">',
                        '<div class="modal-content">',
                        '<div class="modal-header">',
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                        '<h4 class="modal-title">转发简历</h4>',
                        '</div>',
                        '<div class="modal-body">',
                        '<div class="">', //<!-- form -->
                        '<div class="form-group">',
                        '<input name="resume_id" type="hidden">',
                        '<input type="hidden" name="task_id">',
                        '<label for="" class="control-label">收件人</label>',
                        '<input class="form-control" name="email" placeholder="最多可添加3个邮箱,请用&quot;;&quot;分割" type="email">',
                        '<span style="display:none;" class="help-block"><span class="text-danger"><i class="glyphicon glyphicon-remove"></i>邮件格式不正确</span></span>',
                        '</div>',
                        '<div class="form-group">',
                        '<label for="" class="control-label">标题</label>',
                        '<input class="form-control" name="subject" type="text" value="">',
                        '</div>',
                        '<div class="form-group">',
                        '<label for="" class="control-label">正文</label>',
                        '<textarea rows="5" class="form-control" name="content" holderplace="请用\";\"分割" type="email">',
                        '</textarea>',
                        '</div>',
                        '</div>', //<!-- form -->
                        '</div>', //<!-- modal-body -->
                        '<div class="modal-footer">',
                        '<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>',
                        '<button type="button" class="btn btn-primary">确定</button>',
                        '</div>',
                        '</div>',
                        '</div>'
                    ].join(''));
                }
                $dialog.find('[name="resume_id"]').val(data.params.resume_id);
                $dialog.find('[name="task_id"]').val(task__id);
                $dialog.find('[name="subject"]').val(title);
                $dialog.find('[name="content"]').val(content);
                $dialog.find('.help-block').hide();
                $dialog.data('postUrl', data.action_url);

                $dialog.modal({
                    backdrop: true
                });
                $dialog.modal('show');
            });
            me.$element.on('click.' + type, 'span.tr-trigger', function() {
                var $trBefore = $(this).closest('tr'),
                    //col = $trBefore.find('td').length,
                    $trAfter = $trBefore.next();
                if ($trAfter.is('.tr-trigger-hidden')) {
                    $trAfter.find('td').toggle();
                }
            });
            me.$element.on('click.' + type, '[data-toggle="dialog"]', function() {
                var target = $(this).data('target'),
                    params = $.parseJSON(decodeURI($(this).data('params'))),
                    dialog = me.dialogs[target];
                console.log(params);
            });
        },
        _addCheckbox: function() {
            $('tr>th:nth-child(1)', this.$element).before('<th class="td50"><input type="checkbox"></th>');
            $('tr:not(.tr-trigger-hidden)>td:nth-child(1)', this.$element).before('<td><input type="checkbox"></td>');
            $('.tr-trigger-hidden').prev('tr').find('td:eq(0)').attr('rowspan', 2);
            $('span.hidecheckbox', this.$element).closest('tr').find('td:eq(0)').html('');
        },
        _setPk: function() {
            var pkIndex = $('[data-pk]').index();
            this.pk = {
                keyIndex: pkIndex,
                keyName: this.$element.find('tr:eq(0)>[data-pk]').attr('data-key')
            };
        },
        changePageSize: function(pageSize) {
            var me = this;
            me._params.items_per_page = pageSize;
            me.getData({
                //"items_per_page": pageSize,
                "current_page": 0
                //sort_by: this.sort_by,
                //sort_type: this.sort_type
            });
        },
        fixDummyScroller: function() {
            var $wrap = $(this.$element).find('.table-responsive'),
                $table, $dummyWrap, $dummyDiv;
            if ($wrap.length === 0) {
                return;
            }
            $table = $wrap.find('table');
            $dummyWrap = this.$element.find('.dummy-wrap');
            if ($dummyWrap.length > 0) {
                $dummyWrap.css({
                    display: 'block',
                    overflow: 'auto',
                    width: $wrap.width()
                }).find('div').css({
                    display: 'block',
                    height: 1,
                    overflow: 'auto',
                    width: $table.width()
                });
                return;
            }
            $dummyWrap = $('<div class="dummy-wrap"></div>');
            $dummyWrap.css({
                display: 'block',
                overflow: 'auto',
                width: $wrap.width()
            });
            $dummyDiv = $('<div></div>');
            $dummyDiv.css({
                display: 'block',
                height: 1,
                overflow: 'auto',
                width: $table.width()
            });
            $dummyWrap.append($dummyDiv);
            $dummyWrap.insertBefore($wrap);
            $dummyWrap.scroll(function() {
                $wrap.scrollLeft($dummyWrap.scrollLeft());
            });
            $wrap.scroll(function() {
                $dummyWrap.scrollLeft($wrap.scrollLeft());
            });

        },
        renderHeader: function() {},
        resize: function() {},
        createPager: function(page, pageSize, totalCount) {
            var str = ['<div class="page"><div class="pull-left">',
                    '每页',
                    '<select class="form-control input-sm page-size" style="width:auto;display:inline;margin:20px 0;">',
                    '<option value="10"' + (pageSize === 10 ? ' selected' : '') + '>10</option>',
                    '<option value="20"' + (pageSize === 20 ? ' selected' : '') + '>20</option>',
                    '<option value="50"' + (pageSize === 50 ? ' selected' : '') + '>50</option></select>个</div>',
                    '<ul class="pager page-square">'
                ].join(""),
                pageCount = Math.ceil(totalCount / pageSize),
                displayCount = 5,
                halfOfDisplayCount = Math.floor(displayCount / 2),
                displayStart = page - halfOfDisplayCount,
                displayEnd = page + halfOfDisplayCount,
                isCutPrev = displayStart - 1 > 1,
                isCutNext = displayEnd + 1 < pageCount,
                hasFirstPage = displayStart > 1,
                hasLastPage = displayEnd < pageCount,
                pageArray = [],
                i;

            if (displayStart < 1) {
                displayEnd = Math.min(displayEnd - displayStart + 1, displayCount);
                displayStart = 1;

            }
            if (displayEnd >= pageCount) {
                displayStart = Math.max(1, displayStart - displayEnd + pageCount);
                displayEnd = pageCount;

            }
            hasFirstPage = displayStart > 1;
            isCutPrev = displayStart - 1 > 1;
            hasLastPage = displayEnd < pageCount;
            isCutNext = displayEnd + 1 < pageCount;
            //page prev
            if (page > 1) {
                str += '<li data-page="' + (page - 1 - 1) + '"><a href="javascript:void(0);"><b class="left_next"></b></a></li>';
            }
            if (hasFirstPage) {
                str += '<li data-page="0"><a href="javascript:void(0);">1</a></li>';
            }
            if (isCutPrev) {
                str += '<li>...</li>';
            }


            for (i = displayStart; i <= displayEnd; i++) {
                if (i < 1) {
                    continue;
                }
                if (i > pageCount) {
                    break;
                }
                if (i === page) {
                    str += '<li class="active"><a href="javascript:void(0);">' + page + '</a></li>';
                } else {
                    str += '<li data-page="' + (i - 1) + '"><a href="javascript:void(0);">' + i + '</a></li>';
                }
            }
            if (isCutNext) {
                str += '<li>...</li>';
            }
            if (hasLastPage) {
                str += '<li data-page="' + pageCount + '"><a href="javascript:void(0);">' + pageCount + '</a></li>';
            }
            if (page < pageCount) {
                str += '<li data-page="' + page + '"><a href="javascript:void(0);"><b class="right_next"></b></a></li>';
            }
            str += '</ul></div>';

            return str;
        },
        getData: function(param) {
            var me = this,data = {};
            param = param || {};
            param = $.extend({
                current_page: 0
            }, this._params, param);
            me.$toolbar.trigger('viewupdate', param);
            if(!me.isDataInitialized){
                data.initflag = 1;
                me.isDataInitialized = true;
            }
            data.params = JSON.stringify(param);
            $.ajax({
                url: me.options.source,
                type: 'post',
                data: data,
                dataType: 'json'
            }).done(function(res) {
                var page = res.data.detail_page,
                    pager = '',
                    $ul;
                if (page) {
                    pager = me.createPager(page.current_page + 1, page.items_per_page, page.total);
                    me.$element.html(res.data.detail_output + pager);
                    me.$toolbar.toolbar('setPage',page.total);
                } else {
                    me.$element.html(res.data.detail_output);
                    //me.$element.html(res.data);
                }

                $ul = $('ul.dataview', me.$element);
                //$table.addClass("table-bordered");
                //$table.addClass("table-striped");
                if (res.data.checkboxFlag) {
                    me._addCheckbox();
                }
                me._setPk();
                $ul.find('.links [data-command]').each(function() {
                    var $this = $(this),
                        url, params,
                        html = "",
                        taskId = $this.closest('li[data-task-id]').data('taskId'),
                        count = 0;
                    command = $this.data('command');
                    if (command) {
                        $.each(command, function(k, v) {
                            var attributes = '',
                                link, data;
                            count++;
                            if (v.attributes) {
                                attributes = $.map(v.attributes, function(v, k) {
                                    return k + '="' + v + '"';
                                }).join(' ');
                            }
                            if (v.data && !$.isEmptyObject(v.data.params)) {
                                params = v.data.params;
                            } else {
                                params = {};
                            }
                            if (v.data && v.data.postIdName) {
                                params[v.data.postIdName] = taskId;
                            }
                            switch (v.type) {
                                case 'text':
                                    html += '<span class="action-text">' + v.label + '</span>';
                                    break;
                                case 'url':
                                    url = v.data.action_url;
                                    params = $.param(params);
                                    url = url.indexOf('?') > -1 ? url + params : url + "?" + params; //no fargment
                                    html += '<a href="' + url + '" ' + attributes + '>' + v.label + '</a>';
                                    break;
                                case 'download':
                                case 'interview':
                                case 'inapposite':
                                case 'action-ajax-reload':
                                    url = encodeURI(v.data.action_url);
                                    params = encodeURI(JSON.stringify(params));
                                    html += '<a data-action="' + v.type + '" data-action-url="' + url + '" data-params="' + params + '" href="javascript:;" ' + attributes + '>' + v.label + '</a>';
                                    break;
                            }
                            html += '<span class="action-pipe">|</span>';
                        });
                        html = html.substring(0, html.lastIndexOf('<span class="action-pipe">|</span>'));
                        $this.append(html);
                    }
                });
                me.$element.find('.pager li:not(.active):not(.disabled)').on('click', function() {
                    me.getData({
                        //"items_per_page": '',
                        "current_page": $(this).data('page')
                        //,sort_by: me.sort_by,
                        //sort_type: me.sort_type
                    });
                });
                me.fixDummyScroller();
            });
        },
        getSelection: function() {
            var selection = [],
                index = this.pk.keyIndex;
            this.$element.find('ul li :checkbox:checked').each(function() {
                selection.push($(this).closest("li").data('taskId'));
            });
            return selection;
        }

    };
    DataTable.DEFAULTS = {
        pageSize: 10,
        renderer: function() {
            return "";
        },
        colunms: []
    };

    $.fn.dataTable = function(option) {
        var superArgs = arguments;
        return this.each(function() {
            var $this = $(this),
                args,
                data = $this.data('l.datatable'),
                options = typeof option === 'object' && option;

            if (!data) {
                $this.data('l.datatable', (data = new DataTable(this, options)));
            }
            if (typeof option === 'string') {
                args = Array.prototype.slice.call(superArgs, 1);
                try {
                    data[option].apply(data, args);
                } catch (e) {
                    throw "no method: " + option;
                }
            }
        });
    };
    $.fn.dataTable.noConflict = function() {
        $.fn.dataTable = old;
        return this;
    };
    $.fn.dataTable.Constructor = DataTable;
}());
(function() {
    function generateButton(cls, iconCls, text, url, custom, pk) {
        var html = "",
            customStr = " ";
        if (!$.isEmptyObject(custom)) {
            customStr = ' data-custom="' + encodeURI(JSON.stringify(custom)) + '"';
        }
        html = ['<a href="javascript:;"', customStr, 'class="btn btn-default btn-batch ', cls, '" data-action-url="', url, '" data-pk="' + pk + '">',
            text,
            '</a>'
        ].join("");
        return html;
    }

    function generateFilter(cls, text, value, checked) {
        var html = "",
            checkCls = ' ';
        if (checked) {
            checkCls = ' checked ';
        }
        html = ['<a href="javascript:;" class="btn btn-filter', checkCls, cls, '" data-value="' + value + '">',
            '<i class="glyphicon glyphicon-ok"></i>', text,
            '</a>'
        ].join("");
        return html;
    }
    $.fn.toolbar = function(option, count) {

        var view = option.view,
            config = option.items;
        if (option === 'setPage') {
            this.each(function() {
                var element = $(this);
                element.find('.total-count').text('共' + count + '条');
            });
        } else {
            this.each(function() {
                var element = $(this),
                    html = '<div class="form-inline">';
                element.addClass('toolbar');
                if (config.canCheckAll) {
                    html += '<div class="checkbox"><label><input id="checkall" type="checkbox">全选</label></div>';
                    html += '<span class="inline-pipe">|</span>';
                }
                $.each(config.batchBtn, function() {
                    var params = this.params,
                        cls = this.cls || " ",
                        iconCls = this.iconCls || " ",
                        selectedIndex;
                    html += generateButton(cls, iconCls, params.label, params.url, params.custom, params.requestIdName);
                });
                html += '<div class="pull-right total-count"></div>';
                html += '<div class="pull-right">';
                $.each(config.filters, function() {
                    var params = this.params,
                        cls = this.cls || " ";
                    html += generateFilter(cls, params.label, params.value, this["default"]);
                });
                html += '</div>';
                html += '</div>';
                element.append(html);
                element.on('click', '.btn-batch', function() {
                    var custom = $(this).data('custom'),
                        url = $(this).data('actionUrl'),
                        selection = view.getSelection(),
                        pkName = $(this).data('pk'),
                        params;
                    if (selection.length < 1) {
                        return false;
                    }
                    try {
                        params = JSON.parse(decodeURI(custom));
                    } catch (e) {
                        params = {};
                    }
                    params[pkName] = selection;
                    $.post(url, {
                        params: JSON.stringify(params)
                    }, function(res) {
                        if (String(res.status) === "1") {
                            alert('成功了^_^!');
                            view.getData();
                        } else {
                            alert('失败了>_<!');
                        }
                    }, "json");
                });
                element.on('click', '.btn-filter', function() {
                    var $filters,
                        filter = [],
                        $this = $(this);
                    $this.toggleClass('checked');
                    $filters = element.find('.btn-filter.checked');
                    $filters.each(function() {
                        filter.push($(this).data('value'));
                    });
                    view._params.verify_status = filter.join(',');
                    view.getData();
                });
                element.on('click', '#checkall', function() {
                    if (this.checked) {
                        view.$element.find('.dataview .checkbox > :checkbox').prop('checked', true);
                    } else {
                        view.$element.find('.dataview .checkbox > :checkbox').prop('checked', false);
                    }
                });
            });
        }
        return this;
    };
}());