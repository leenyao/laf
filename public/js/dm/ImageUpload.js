(function(){
     var ImageUpload = function(element, options) {
        this.type =
            this.options =
            this.enabled =
            this.timeout =
            this.hoverState =
            this.$element = null

        this.init('tooltip', element, options);
    };
    var old = $.fn.tagSelect;

    $.fn.imageUpload = function(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('dm.imageUpload');
            var options = typeof option == 'object' && option;

            if (!data) {
                $this.data('dm.imageUpload', (data = new ImageUpload(this, options)));
            }
            if (typeof option == 'string') {
                data[option]();
            }
        });
    };

    $.fn.imageUpload.Constructor = ImageUpload;
}());