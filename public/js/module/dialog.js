(function($) {/* global l*/
    var defaultConfig = {
        title:[{label:''}],
        btn:[],
        id:'',
        body:'',
        closeButton:true,
        footer:''
    };
    function createModal(config){
        config = $.extend({},defaultConfig,config);
        var html = ['<div id="',config.id,'" class="modal fade">',
            '<div class="modal-dialog">',
            '<div class="modal-content">',
            '<div class="modal-header">',
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
            '<h4 class="modal-title">牵牛网提示</h4>',
            '</div>',
            '<div class="modal-body">',
            config.body,
            '</div>',
            (config.footer?
            '<div class="modal-footer">'+
            config.footer+
            '</div>':""),
            '</div>',
            '</div>',
            '</div>'
        ].join('');
        return html;
    }
    function generateHtml(payment, link) {
        payment = Number(payment);

        link = link || "";
        var serviceFeeRate = 0.15,
            total = payment * (1 + serviceFeeRate),
            serviceFee = payment * serviceFeeRate,
            html;
        serviceFee = serviceFee.toFixed(2);
        total = total.toFixed(2);
        html = ['<div id="modal-alipay" class="modal fade">',
            '<div class="modal-dialog">',
            '<div class="modal-content">',
            '<div class="modal-header">',
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
            '<h4 class="modal-title">牵牛网提示</h4>',
            '</div>',
            '<div class="modal-body">',
            '<p>收款方:上海牵牛信息技术有限公司</p>',
            '<p><strong>商品:牵牛招聘产品</strong><strong class="pull-right">', payment, '元</strong></p>',
            '<p><strong>服务费</strong><span class="text-danger">（费率15%，用于支付第三方手续费）</span><strong class="pull-right">', serviceFee, '元</strong></p>',
            '<p><strong>总计</strong><strong class="pull-right total">', total, '元</strong></p>',
            '<p class="text-warning">温馨提示：每周最多发5次</p>',
            '</div>',
            '<div class="modal-footer">',
            '<p class="text-sm text-left">',
            '<input name="agreement" checked="checked" type="checkbox">我同意<a href="'+l.get('apihost')+'/useragreement'+'">牵牛招聘协议</a></p>',
            '<a data-action="pay" href="', link, '" target="_blank" class="btn btn-primary ">去支付宝付款</a>',
            '<a class="btn btn-default" href="'+l.get('baseUrl')+'/admin/list/huntertask/0'+'">暂缓付款</a>',
            '</div>',
            '</div>',
            '</div>',
            '</div>'
        ].join('');
        return html;
    }
    $.alipay = function(opts) {
        var $modal = $('#alipay');

        if ($modal[0]) {
            $modal.remove();
        }
        $modal = $(generateHtml(opts.payment, opts.payurl)).appendTo('body');
        if($.alipay.timeoutId){
            $('#modal-'+$.alipay.timeoutId).remove();
            clearTimeout($.alipay.timeoutId);
        }
        $modal.on('click','[name="agreement"]',function(){
            if(this.checked){
                $('[data-action="pay"]',$modal).attr('disabled',null);
            }else{
                $('[data-action="pay"]',$modal).attr('disabled','disabled');
            }
        });
        $modal.find('a[data-action="pay"]').on('click',function(){
            $.alipay.timeoutId = setTimeout(function(){
                var body = [
                '<p><h3><strong>请在新打开的支付宝页面完成付款。</strong></h3></p>',
                '<p><a href="">支付遇到问题?</a></p>',
                '<p class="text-center">',
                '<a href="'+l.get('baseUrl')+'/admin/list/huntertask/0'+'" class="btn btn-primary">已完成支付</a>',
                '&nbsp;&nbsp;',
                '<a href="'+l.get('baseUrl')+'/admin/list/hunterbounty/0'+'">查看交易记录</a>',
                '</p>'
                ].join(''),html;
                $modal.modal('hide');
                html = createModal({
                    title:'牵牛网提示',
                    body:body,
                    id:'modal-'+$.alipay.timeoutId
                });
                $(html).appendTo('body').modal('show');
            },1000);
        });
        $modal.modal('show');
    };
}(jQuery));
(function($){
    function fieldHtmlPrefix(required,label){
        var str ="";
        str += '<div class="form-group">';
        str += '<label for="" class="control-label col-sm-2">';
        if (required) {
            str += '<span class="required-symbol text-danger">*</span>';
        }
        str += label;
        str += '</label>';
        str += '<div class="col-sm-10">';
        return str;
    }
    function fieldHtmlSuffix(){
        var str ="";
        str += '</div>';
        str += '</div>';
        return str;
    }
    function generateField(field) {
        var requireStr = field.required ? 'required="true"' : '',
            multipleStr = field.multiple ? 'multiple="true"' : '',
            nameStr = 'name="' + field.name + '"',
            valueStr = '',
            str = '';
        if (field.defaultValue) {
            valueStr = field.defaultValue;
        }
        if (field.value) {
            valueStr = field.value ;
        }
        
        switch (field.type) {
            case 'hidden':
                str += '<input type="hidden" value="{value}" {name}>';
                break;
            case 'static':
                str += fieldHtmlPrefix(field.required, field.label);
                str += '<input type="hidden" value="{value}" {name}>';
                str += '<p class="form-control-static">' + field.value + '</p>';
                str += fieldHtmlSuffix();
                break;
            case 'date':
                str += fieldHtmlPrefix(field.required, field.label);
                str += '<input class="form-control datepicker" type="text" value="{value}" {name} {required}>';
                str += fieldHtmlSuffix();
                break;
            case 'time':
                str += fieldHtmlPrefix(field.required, field.label);
                str += '<input class="form-control time-picker" type="text" value="{value}" {name} {required}>';
                str += fieldHtmlSuffix();
                break;
            case 'select':
                str += fieldHtmlPrefix(field.required, field.label);
                str += '<select class="form-control" {name} {multiple} {required} value="{value}">';
                $.each(field.data, function() {
                    str += '<option value="' + this.value + '">' + this.label + '</option>';
                });
                str += '</select>';
                str += fieldHtmlSuffix();
                break;
            case 'textarea':
                str += fieldHtmlPrefix(field.required, field.label);
                str += '<textarea class="form-control" rows="4" {name} {required}>{value}</textarea>';
                str += fieldHtmlSuffix();
                break;
            case 'text':
            /* falls through */
            default:
                str += fieldHtmlPrefix(field.required, field.label);
                str += '<input class="form-control" type="text" value="{value}" {name} {required}>';
                str += fieldHtmlSuffix();
        }
        
        str = str.replace(/\{value\}/g, valueStr)
            .replace(/\{name\}/g, nameStr)
            .replace(/\{required\}/g, requireStr)
            .replace(/\{multiple\}/g, multipleStr)
            .replace(/\s{2,}/g, ' ');
        return str;
    }

    function getModalTemplate(config) {
        var html = ['<div class="modal-dialog">',
            '<div class="modal-content">',
            '<div class="modal-header">',
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
            '<h4 class="modal-title" >' + config.title + '</h4>',
            '</div>',
            '<div class="modal-body form-horizontal">',
            $.map(config.fields, function(v, k) {
                return generateField(v);
            }).join(''),
            '</div>', //<!-- modal-body -->
            '<div class="modal-footer">',
            '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>',
            '<button data-action="OK" type="button" class="btn btn-primary">确定</button>',
            '</div>',
            '</div>',
            '</div>'
        ].join('');
        return html;
    }
    $.toggleModalForm = function(config){
        var $dialog = $('#modal-form');
        if ($dialog.length < 1) {
            $dialog = $('<div class="modal fade" id="modal-form"></div>');
            $dialog.appendTo(document.body);
            $dialog.on('click', '.btn-primary', function() {
                var params={},handler = $dialog.data('primaryHandler');;
                $.each(config.fields, function(i,v) {
                    params[this.name] = $dialog.find('[name="'+this.name+'"]').val();
                });
                $.ajax({
                    type: "POST",
                    url: $dialog.data('postUrl'),
                    data: {
                        params: JSON.stringify(params)
                    },
                    context: $dialog,
                    dataType: 'json'
                }).done(function(res) {
                    var cb = $dialog.data('callback'),
                        status = String(res.status);
                    if(typeof cb ==='function'){
                        cb($dialog,status);
                    }
                });
                if(typeof handler ==='function'){
                    handler($(this));
                } else {
                    $dialog.modal('hide');
                }
            });
        }
        $dialog.data('callback',config.callback);
        $dialog.data('primaryHandler',config.primaryHandler);
        $dialog.empty();
        $dialog.html(getModalTemplate(config));
        $dialog.modal({
            backdrop: true
        });
        $.each(config.fields, function() {
            var value = this.value || "";
            $dialog.find('[name="'+this.name+'"]').val(this.value||"");
        });
        $dialog.find('select').tagselect();
        $dialog.find('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $dialog.find('.time-picker').datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
            language: 'zh-CN',
            startDate: new Date()
        });
        $dialog.data('postUrl', config.url);
        $dialog.modal('show');
    };
}(jQuery));