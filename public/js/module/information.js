(function($) { /* global Holder,l*/
    var sendFile;

    sendFile = function(file, callback) {
        var data;
        data = new FormData();
        data.append("uploadfile", file);
        return $.ajax({
            url: l.get('imgUploadBaseUrl') + '/api/imgupload?ext=.jpg,.jpeg&zoom=small',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data) {
                return callback(data);
            }
        });
    };

    var me = window.MsgContext = (function() {
        //var options;
        var generateTabs = function() {
            var tabLabels = me.options.tabLabels,
                viewType = me.options.view_type,
                html = '<ul class="nav nav-tabs" id="">';
            $.each(tabLabels, function(k, v) {
                if (String(viewType) === String(k)) {
                    html += '<li data-target="' + k + '" class="active"><a href="#tab-' + k + '" data-toggle="tab">' + v + '</a></li>';
                } else {
                    html += '<li data-target="' + k + '" ><a href="#tab-' + k + '" data-toggle="tab">' + v + '</a></li>';
                }
            });
            html += '</ul>';
            return html;
        },
            generateTabContext = function() {
                var tabLabels = me.options.tabLabels,
                    html = '<div class="tab-content">',
                    viewType = me.options.view_type;
                $.each(tabLabels, function(k) {

                    if (String(viewType) === String(k)) {
                        html += '<div class="tab-pane active" id="tab-' + k + '">';
                    } else {
                        html += '<div class="tab-pane" id="tab-' + k + '">';
                    }
                    html += '<div class="panel panel-default tab-panel">';
                    switch (k) {
                        case '1':
                            html += '<div class="panel-body">';
                            html += '<textarea name="content" rows="20" class="form-control" data-type="text"></textarea>';
                            html += '</div>';
                            break;
                        case '3':
                            html += '<div class="single"></div>';
                            break;
                        case '4':
                            html += '<div class="multiple"></div>';
                            break;
                        default:
                            html += '';
                    }
                    html += '</div>';
                    html += '</div>';

                });
                html += '</div>';
                return html;
            };
        return {
            init: function(options) {

                me.options = options;
                me.$element = $('#' + options.render_id);
                //me.$element = $('#content').parent();
                me.$element.html(generateTabs() + generateTabContext());
                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {

                    var tab = $(e.target).attr('href');
                    tab = $(tab);
                    me.$element.find('.tab-pane').each(function() {
                        if (this === tab[0]) {
                            $('input', this).prop('disabled', false);
                            $('textarea', this).removeAttr('disabled');
                        } else {
                            $('input', this).prop('disabled', true);
                            $('textarea', this).attr('disabled', 'disabled');
                        }
                    });


                    if ($('.cover-default').attr('data-src') === 'data:,') {
                        Holder.run({

                            images: '.cover-default',
                            themes: {
                                "thumbnail": {
                                    background: "#eee",
                                    foreground: "#aaa",
                                    size: 12,
                                    text: "封面图片"
                                }
                            }
                        });
                    }
                });
                me.$element.find('.single').contenteditor({
                    type: 'single',
                });
                me.$element.find('.multiple').contenteditor({
                    type: 'multiple'
                });
                $('.summernote').each(function() {
                    var $this = $(this);
                    $this.summernote({
                        toolbar: [
                            //['style', ['style']], //no style button
                            ['fontsize', ['fontsize']],
                            ['color', ['color']],
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']],
                            ['insert', ['link','picture']] // no insert buttons
                            //['table', ['table']], // no table button
                            //['help', ['help']] / no help button
                        ],
                        locale:'zh-CN',
                        height: 300,
                        onpaste: function(e) {
                            var valueField = $this.data('valueField') || $this;
                            valueField.val($this.code());
                        },
                        onkeyup: function() {
                            var valueField = $this.data('valueField') || $this;
                            valueField.val($this.code());
                        },
                        onImageUpload: function(files, editor, welEditable) {
                            return sendFile(files[0], function(data) {
                                var url;
                                url = l.get('imgUploadBaseUrl') + data.data;
                                if (Number(data.status) === 1){
                                    return editor.insertImage(welEditable, url);
                                }else{
                                    return false;
                                }
                                
                            });
                        }

                    });

                    $this.code($this.val());
                });
                switch (String(me.options.view_type)) {
                    case "1": //text
                        me.$element.find('textarea[data-type="text"]').val(me.options.data ? me.options.data[0].content : "");
                        me.$element.find('textarea[data-type="text"]').code(me.options.data ? me.options.data[0].content : "");
                        break;
                    case "3": //single
                        me.$element.find('.single').contenteditor('setData', me.options.data);
                        break;
                    case "4": //multiple
                        me.$element.find('.multiple').contenteditor('setData', me.options.data);
                        break;
                    default:
                        me.$element.find('textarea[data-type="text"]').val(me.options.data ? me.options.data[0].content : "");
                        break;
                }

                $('[name="view_type"]').val('1');
                if ($.inArray((me.options.view_type).toString(), ['1', '3', '4']) > -1) {
                    $('[name="view_type"]').val(me.options.view_type);
                }

                me.$element.find('ul.nav-tabs').on('click', 'li', function() {
                    var target = $(this).attr('data-target') || "1";
                    $('[name="view_type"]').val(target);
                });
                if ($('ul.nav-tabs>li.active').length === 0) {
                    $('ul.nav-tabs>li[data-target=1]>a').trigger('click');
                }
                me.$element.find('input[type="file"].upload-input').attr('name', 'uploadfile').fileupload({
                    dataType: 'json',
                    url: l.get('imgUploadBaseUrl') + '/api/imgupload',
                    done: function(e, data) {
                        // if (String(data.result.status) !== "1") {
                            
                        // }
                    },
                    always: function(e, data) {
                        var bundleField = $(this).data('bundleField'),
                            fileType = $(this).attr('data-filetype'),
                            resultUrl = data.result.data + '',
                            preview = $(this).data('preview');
                        if (!bundleField) {
                            $(this).data('bundleField', bundleField = $('<input type="hidden">'));
                            $(this).after(bundleField);
                        }
                        if (fileType === 'pic') {
                            if (!preview) {
                                $(this).data('preview', preview = $('<img style="display:none;" width="100" height="100" src="data:,">'));
                                $(this).after(preview);
                            }
                            preview.show();
                            if(resultUrl.indexOf('http') > -1){
                                preview.attr('src', resultUrl);
                            }else{
                                preview.attr('src', l.get('imgUploadBaseUrl') + resultUrl);
                            }
                            
                        }
                        bundleField.attr('name', $(this).attr('data-elname'));
                        bundleField.val(data.result.data);
                        $(this).closest('form').off('submit.fileupload');
                    },
                    change: function() {
                        $(this).closest('form').on('submit.fileupload', function() {
                            return false;
                        });
                    }
                });
                me.$element.find('li.active>a').trigger('shown.bs.tab');
            }

        };

    }());
    me.config = {};
    $.widget('l.contenteditor', {
        options: {
            data: [{
                title: '',
                content: '',
                thumbnails: '',
                description: ''
            }],
            type: 'single'
        },
        _init: function() {

        },
        _create: function() {
            var me = this;
            $.l.contenteditor.idGenerator++;
            me.id = this.widgetName + '-' + $.l.contenteditor.generateId();
            $.l.contenteditor.instance[me.id] = this;
            me._generateBody();
        },
        _generateBody: function() {
            var html = [
                '<div class="col-sm-5">',
                //'<div class="panel panel-default">',
                //'<div class="panel-body">',
                '<div class="thumbnail">',
                //'<h4 data-target="title"></h4>',
                '<div class="preview-wrap">',
                '<img data-target="thumbnail" class="cover-default"  alt="封面图片" src="data:," style="height: 150px; width: 100%; display: block;">',
                '<!-- <p style="line-height:160px;text-align:center;">封面</p> -->',
                '<h5 class="preview-title" data-target="title"></h5>',
                '{action}',
                '</div>',
                '<input type="hidden" name="title">',
                '<input type="hidden" name="thumbnails">',
                '<input type="hidden" name="content">',
                '<input type="hidden" name="description">',
                //'</div>',
                //'</div>',
                '</div>',
                '{lite}',
                '</div>',
                '<div class="col-sm-7">',
                '<div class="form-group">',
                '<label for="', this.id + "-title", '">标题</label>',
                '<input id="', this.id + "-title", '" type="text" class="form-control">',
                '<p class="help-block" data-target="title">请不要超过18个字符</p>',
                '</div>',
                '<div class="form-group">',
                '<label for="', this.id + "-description", '">摘要</label>',
                '<input id="', this.id + "-description", '" type="text" class="form-control">',
                '</div>',
                '<div class="form-group">',
                '<label for="', this.id + "-thumbnail", '">封面</label>',
                '<input id="', this.id + "-thumbnail", '" type="file" class="form-control upload-input" data-filetype="pic" data-elname="thumbnails">',
                '<p class="help-block" data-target="cover">{help}</p>',
                '</div>',
                '<div class="form-group">',
                '<label for="', this.id + "-content", '">内容</label>',
                '<textarea id="', this.id + "-content", '"class="summernote"></textarea>',
                '</div>',
                '</div>'
            ].join(''),
                lite = [
                    '<div class="contentedit-toolbox">',
                    '<button type="button" data-action="add">新增</button>',
                    '</div>'
                ].join(""),
                action = [
                    '<ul class="preview-action">',
                    '<li style="margin-top:50px;"><button type="button" data-action="edit" class="btn btn-link"><span class="glyphicon glyphicon-edit"></span></button></li>',
                    '</ul>'
                ].join('');

            if (this.options.type === 'multiple') {
                html = html.replace(/\{action\}/, action);
                html = html.replace(/\{lite\}/, lite);
                html = html.replace(/\{help\}/, '图片建议尺寸：864X480像素.');
            } else {
                html = html.replace(/\{lite\}/, '');
                html = html.replace(/\{action\}/, '');
                html = html.replace(/\{help\}/, '图片建议尺寸：864X480像素.');
            }
            this.element.html(html);
            this.selectedItem = $('.thumbnail:first', this.element);
            this.element.find('[type="file"].upload-input').data('bundleField', $('[name="thumbnails"]', this.selectedItem));
            this.element.find('[type="file"].upload-input').data('preview', $('img', this.selectedItem));
            this.element.addClass('panel-body');
            this._bindEvents();
            this._onEdit();
        },
        _setValues: function(values) {
            this.$title.val(values.title);
            this.$cover.val(values.cover);
            this.$editor.val(values.content);
        },
        _bindEvents: function() {
            var me = this;
            me.element.on('click.contenteditor', '[data-action="add"]', function() {
                me._onAdd();
            });
            me.element.on('click.contenteditor', '[data-action="edit"]', function() {

                me._setSelectedItem($(this).closest('.thumbnail'));
                me._onEdit();
            });
            me.element.on('click.contenteditor', '[data-action="remove"]', function() {
                if (me.element.find('.thumbnail').length < 3) {
                    alert('不能少于2条信息');
                    return;
                }
                me._onRemove($(this).closest('.thumbnail'));
            });
            me.element.find('#' + this.id + "-title").on('keyup', function() {
                me.selectedItem.find('[name="title"]').val(this.value);
                me.selectedItem.find('[data-target="title"]').html(this.value);
            });
            me.element.find('#' + this.id + "-description").on('keyup', function() {
                me.selectedItem.find('[name="description"]').val(this.value);
                me.selectedItem.find('[data-target="description"]').html(this.value);
            });

        },
        _setSelectedItem: function(selectedItem) {
            var uploader = this.element.find('[type="file"].upload-input');
            this.selectedItem = selectedItem;
            uploader.data('bundleField', $('[name="thumbnails"]', this.selectedItem));
            uploader.data('preview', $('img', this.selectedItem));

        },
        _onEdit: function() {
            var selectedItem = this.selectedItem;
            if (selectedItem.index() > 0) {
                this.element.find('.help-block[data-target="cover"]').text('图片建议尺寸：148X148像素.');
            } else {
                this.element.find('.help-block[data-target="cover"]').text('图片建议尺寸：864X480像素.');
            }


            this.element.find('#' + this.id + "-title").val(selectedItem.find('[name="title"]').val());
            this.element.find('#' + this.id + "-description").val(selectedItem.find('[name="description"]').val());
            this.element.find('.summernote').code(selectedItem.find('[name="content"]').val());
            this.element.find('.summernote').data('valueField', selectedItem.find('[name="content"]'));
        },
        _onRemove: function(item) {
            var selectedItem = item.next('.thumbnail:eq(0)');
            if (item.next('.thumbnail').length === 0) {
                selectedItem = item.prev('.thumbnail:eq(0)');
                //= this.element.find('.thumbnail:first');
            }
            this._setSelectedItem(selectedItem);
            item.remove();
            this._onEdit();
        },
        _onAdd: function() {
            var html = $.l.contenteditor.previewTpl,
                preview = $(html);
            $('.thumbnail:last', me.element).after(preview);
            this._setSelectedItem(preview);
            this._onEdit();
        },
        setData: function(data) {
            var thumbnails, me = this;
            if (!$.isArray(data) || !data.length) {
                return;
            }
            thumbnails = $('.thumbnail', this.element).eq(0);
            thumbnails.find('[data-target="title"]').html(data[0].title);
            thumbnails.find('[data-target="description"]').html(data[0].description);
            thumbnails.find('[data-target="thumbnail"]').attr('src', l.get('baseUrl') + data[0].thumbnails);
            thumbnails.find('[name="title"]').val(data[0].title);
            thumbnails.find('[name="description"]').val(data[0].description);
            thumbnails.find('[name="thumbnails"]').val(data[0].thumbnails);
            thumbnails.find('[name="content"]').val(data[0].content);
            // $(this).find('[data-target="cover"]').attr('src', l.get('baseUrl') + data[i].cover);
            me._setSelectedItem(thumbnails);
            me._onEdit();
            if (this.options.type === 'multiple') {
                $('.thumbnail:nth-child(n+2)', this.element).remove();
                $.each(data, function(i, v) {
                    if (i === 0) {
                        return;
                    }
                    var html = $.l.contenteditor.previewTpl,
                        preview = $(html);
                    preview.find('[name="title"]').val(v.title);
                    preview.find('[name="description"]').val(v.description);
                    preview.find('[name="thumbnails"]').val(v.thumbnails);
                    preview.find('[name="content"]').val(v.content);
                    preview.find('[data-target="title"]').html(data[i].title);
                    //preview.find('[data-target="description"]').html(data[i].description);
                    preview.find('[data-target="thumbnail"]').attr('src', l.get('baseUrl') + data[i].thumbnails);
                    $('.thumbnail:last', me.element).after(preview);
                });
            }

        },
        destroy: function() {
            //clean up
            $.l.contenteditor.instance[this.id] = null;
            delete $.l.contenteditor.instance[this.id];
            $.Widget.prototype.destroy.call(this);
        }
    });
    $.extend($.l.contenteditor, {
        uuid: 0,
        instance: {},
        idGenerator: 0,
        generateId: function() {
            this.uuid++;
            return this.uuid;
        },
        previewTpl: ['<div class="thumbnail">',
            '<div class="preview-wrap">',
            //'<h4 data-target="title"></h4>',
            '<img data-target="thumbnail" class="cover-default"  alt="封面图片" src="" style="height: 50px; width: 50px; display: block;margin-right:0;margin-left:auto;">',
            '<!-- <p style="line-height:160px;text-align:center;">封面</p> -->',
            '<div class="preview-title-sub" data-target="title"></div>',
            '<ul class="preview-action">',
            '<li><button type="button" data-action="edit" class="btn btn-link">',
            '<span class="glyphicon glyphicon-edit"></span>',
            '</button></li>',
            '<li><button type="button" data-action="remove" class="btn btn-link">',
            '<span class="glyphicon glyphicon-remove"></span></button></li>',
            '</ul>',
            '</div>',
            '<input type="hidden" name="title">',
            '<input type="hidden" name="thumbnails">',
            '<input type="hidden" name="content">',
            '<input type="hidden" name="description">',
            '</div>'
        ].join('')
    });
}(jQuery));