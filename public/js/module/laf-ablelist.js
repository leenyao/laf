(function($){
    $.widget('l.laf_addablelist', {
        options:{
            reloadUrl: '/hcoffee/index/getattributeslist',
            attrsUrl: '/admin/list/view_items_options/0',
        },
        _init: function(){

        },
        _create: function(){
            var me = this;
            me.values = me.element.data('values');
            me.selected = me.element.data('selected');
            me.container = me.element.parent();
            me.inputName = me.element[0].name;
            me.element[0].name = '';
            me.btnContainer = $('<div class="btn-toolbar"></div>').appendTo(me.container);
            me.addBtn = $('<button class="btn btn-sm btn-success" type="button">新增</button>').appendTo(me.btnContainer);
            me.reloadBtn = $('<button type="button" class="btn btn-sm btn-default">刷新</button>').appendTo(me.btnContainer);
            me.editBtn = $('<a target="_blank" href="'+l.get('baseUrl')+this.options.attrsUrl+'" class="btn btn-sm btn-primary">编辑／添加列表中属性</a>').appendTo(me.btnContainer);
            me.addBtn.on('click',function(e){
                e.preventDefault();
                $(me._generateRow()).insertBefore(me.btnContainer);
                me._resolveValue();
            });
            me.reloadBtn.on('click',function(e){
                e.preventDefault();
                me._reload();
            });
            if($.isArray(me.selected)){
                $.each(me.selected, function (i,v){
                    $(me._generateRow(v)).insertBefore(me.btnContainer);
                });
            }
            me.container.on('change','select',function(){
                me._refreshAttr($(this));
                me._resolveValue();
            });
            me.container.on('click','button[data-action="remove"]',function(){
                var $row = $(this).closest('.row');
                $row.remove();
                me._resolveValue();
            });
        },
        _resolveValue: function(){
            var value = this.container.find('select').map(function(){
                return $(this).val();
            }).get();
            this.element.val(JSON.stringify(value));
        },
        _reload:function(){
            var me = this;
            $.getJSON(l.get('baseUrl')+this.options.reloadUrl)
                .then(function(res){
                    me._refreshAttrs(res);

                });
        },
        _refreshAttr: function($select){
            var labels = '',
                val = $select.val(),
                attr = this.values[val],
                $row = $select.closest('.row');
            if(attr){
                labels = $.map(attr.child,function(v){
                        return v;
                    }).join(',');
                $row.find('.form-control-static').html(labels);
            }
        },
        _refreshAttrs: function(values){
            var me = this;
            if(values){
                this.values = values;
                this.container.find('select').each(function(i,v){
                    me._refreshAttr($(this));
                });
            }
        },
        _generateRow: function(defaultId,defaultValue){
            var html = [
                '<div class="row">',
                    '<div class="col-sm-4">',
                        '<div class="form-group">',
                            '<label for="" class="control-label col-sm-4">属性名</label>',
                            '<div class="col-sm-8">',
                              '<select class="form-control" name="'+this.inputName+'">',
                                $.map(this.values,function(v,k){
                                    var selected = defaultId+'' === k?' selected':'';
                                    if(!defaultId){
                                        defaultId = k;
                                    }
                                    return '<option'+selected+' value="'+k+'">'+v.label+'</option>';
                                }).join(''),
                              '</select>',
                            '</div>',
                        '</div>',
                    '</div>',
                    '<div class="col-sm-4">',
                        '<div class="form-group">',
                            '<label for="" class="control-label col-sm-4">值</label>',
                            '<div class="col-sm-8">',
                              '<p class="form-control-static">',
                                this.values[defaultId]?$.map(this.values[defaultId].child,function(v){
                                    return v;
                                }).join(','):'',
                              '</p>',
                            '</div>',
                        '</div>',
                    '</div>',
                    '<div class="col-sm-4">',
                        '<button type="button" data-action="remove" class="btn btn-sm btn-warning">删除</button>',
                    '</div>',
                '</div>'
            ].join('');
            return html;
        }

    });
    $.extend($.l.laf_addablelist, {
        uuid: 0,
        instance: {},
        idGenerator: 0,
        generateId: function() {
            this.uuid++;
            return this.uuid;
        }
    });
})(jQuery);
