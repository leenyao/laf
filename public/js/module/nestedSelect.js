(function($) {
    var modalTemplate = [
        '<div class="modal-dialog">',
        '<div class="modal-content">',
        //'<div class="modal-header">',
        //  '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
        //  '<h4 class="modal-title" id="myModalLabel">Modal title</h4>',
        //'</div>',
        '<div class="modal-body">',
        '</div>',
        //'<div class="modal-footer">',
        //  '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>',
        //  '<button type="button" class="btn btn-primary">Save changes</button>',
        //'</div>',
        '</div>', //.modal-content 
        '</div>', //.modal-dialog
    ].join('');
    $.widget('l.nestedselect', {
        options: {
            data: [],
            levelLabel: [],
            selected: [],
            maxCount: Number.POSITIVE_INFINITY
        },
        _init: function() {

        },
        _create: function() {
            var me = this;
            $.l.nestedselect.idGenerator++;
            me.id = this.widgetName + '-' + $.l.nestedselect.generateId();
            $.l.nestedselect.instance[me.id] = this;
            this.initOptions();
            me._createIndex();

            this.modal = $('<div class="modal fade" id="' + me.id + '-panel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>');
            this.modal.hide();
            this.modal.appendTo(document.body);
            this.element.wrap('<div class="form-control tokenfield"></div>');
            this.wrap = this.element.parent();
            this.element.css({
                position: 'absolute',
                left: '-10000px'
            });
            this.fakeInput = $('<input class="token-input ui-autocomplete-input" style="width:100%;"></input>').insertAfter(this.element);
            this.modal.html(modalTemplate);
            this.fakeInput.attr('data-toggle', 'modal');
            this.fakeInput.attr('readonly', true);
            this.fakeInput.attr('placeholder', this.element.attr('placeholder'));
            this.fakeInput.attr("data-target", '#' + me.id + '-panel');
            this.body = this.modal.find('.modal-body');
            this.body.append(me._generateBody());
            this._showChildList();
            this._bindEvent();
            this._initValue();
        },
        _initValue: function() {
            var value = [],
                me = this;
            try {
                value = this.element.data('selected');
            } catch (e) {}
            
                $.each(value, function() {
                    var node = me.cache[this.level + '.' + this.value],
                        label = [];
                    try {
                        $.each(node.path.split('.'), function(k, v) {
                            label.push(me.cache[k + '.' + v].label);
                        });
                        me._addTag(node.path, node.level, node.value, label.join('-'));
                    }catch(e){
                        throw 'Can not find node in the cache. level is "'+this.level+'",value is "'+this.value+'".';
                    }
                    //me._doAdd(node);
                });
            
            me.setValue(value);
        },
        getText: function() {
            return this.fakeInput.val();
        },
        getValue: function() {
            var value = [];
            try {
                value = JSON.parse(this.element.val());
            } catch (e) {}
            return value;
        },
        setValue: function(value) {
            var text = [];
            if (typeof value === 'string'){
                value = [value];
            }
            this.body.find('.ns-item-selected').each(function() {
                var label = $(this).text();
                text.push(label.substring(0, label.length - 1));
            });
            this.fakeInput.val(text.join(','));
            if(value && value.length && value.length >0){
                this.element.val(JSON.stringify(value));
            } else {
                this.element.val('');
            }
            
        },
        _onEdit: function() {

        },
        _onAdd: function(node) {
            var value = this.getValue(),
                isFound = false;
            if (value.length >= this.options.maxCount && this.options.maxCount !==1) {
                alert("只能增加" + this.options.maxCount + "个");
                return false;
            }
            if(this.options.maxCount ===1){
                this._removeAll();
                this._doAdd(node);
                return false;
            }
            $.each(value, function() {
                if (String(this.value) === String(node.value) &&
                    String(this.level) === String(node.level)) {
                    isFound = true;
                    return false;
                }
            });
            if (isFound) {
                return false;
            }
            this._doAdd(node);
        },
        _doAdd: function(node) {
            var me = this,
                value = this.getValue(),
                label = [];

            $.each(node.path.split('.'), function(k, v) {
                label.push(me.cache[k + '.' + v].label);
            });
            value.push({
                value: node.value,
                level: node.level,
                label: node.label
            });

            me._addTag(node.path, node.level, node.value, label.join('-'));
            this.setValue(value);
        },
        _onSelect: function() {},
        _onRemove: function(node) {
            var value = this.getValue(),
                index = -1;
            $.each(value, function(i) {
                if (String(this.value) === String(node.value) &&
                    String(this.level) === String(node.level)) {
                    index = i;
                    return false;
                }
            });
            if (index > -1) {
                value.splice(index, 1);
                this.setValue(value);
            }

        },
        _addTag: function(path, level, value, label) {
            var html = [
                '<li class="ns-item-selected" data-path="' + path + '" data-level="' + level + '" data-value="' + value + '">',
                label, '<span class="ns-item-remove">&times;</span>',
                '</li>'
            ].join('');
            $(html).insertBefore($('.ns-selected-submit', this.body));

        },
        _createModal: function(){
            
        },
        _onChange: function() {},
        _showChildList: function(parentNode) {
            var level, label, list;
            if (parentNode) {
                level = parentNode.level + 1;
                label = this.options.levelLabel[level];
                list = parentNode.children;
            } else {
                level = 0;
                label = this.options.levelLabel[level];
                list = this.rootNode;
            }
            this.body.append(this._getnerateLevel(level, label, list));
        },
        _bindEvent: function() {
            var me = this;
            this.body.on('click.ns', '.ns-item', function() {
                var node = me.nodeIndex[$(this).data('path')],
                    selector = '';
                if (node.isLeaf || node.children.length === 0) {
                    me._onAdd(node);
                } else {
                    // for(i = 0 ;i < node.level + 1; ){
                    //     if(i === 0){
                    //         selector+='.ns-level-'+(++i);
                    //     }else{
                    //          selector+=',.ns-level-'+(++i);
                    //     }
                    // }
                    $('[class|="ns-level"]', me.body).slice(node.level + 1).remove();
                    $(this).siblings('.active').removeClass('active');
                    $(this).addClass('active');
                    me.body.find(selector).remove();
                    me._showChildList(node);
                }
            });
            this.modal.on('hidden.bs.modal', function() {
                me.element.trigger('close.ns', me.getValue());
            });
            this.body.on('click', '.ns-item-remove', function() {
                var path = $(this).closest('li').data('path'),
                    node = me.nodeIndex[path];
                $(this).closest('li').remove();
                me._onRemove(node);
            });
            //this.body.on('click.ns','.ns-item-leaf');
        },
        _remove: function(){

        },
        _removeAll:function(){
            this.body.find('.ns-item-selected').remove();
            this.element.val('');
        },
        _createIndex: function() {
            if (this.nodeIndex) {
                delete this.nodeIndex;
                delete this.rootNode;
            }
            this.nodeIndex = {};
            this.rootNode = [];
            this.cache = {};
            this._addNodeIndex(this.options.data);
        },
        _addNodeIndex: function(array, parentIndex) {
            var me = this,
                affix = parentIndex ? parentIndex + '.' : '';
            if (!array) {
                return;
            }
            $.each(array, function() {

                var value = (this.value||this.id)+"",
                    nodeIndex = affix + value,
                    level = 0;

                level = nodeIndex.split('.').length - 1;
                me.nodeIndex[nodeIndex] = {
                    path: nodeIndex,
                    label: this.label,
                    value: value,
                    level: nodeIndex.split('.').length - 1,
                    children: []
                };
                me.cache[level + '.' + value] = me.nodeIndex[nodeIndex];
                if (level === 0) {
                    me.rootNode.push(value);
                }
                if (parentIndex) {
                    me.nodeIndex[parentIndex].children.push(nodeIndex);
                }
                me._addNodeIndex(this.children, nodeIndex);
            });
        },
        initOptions: function() {
            var x;
            for (x in this.options) {
                if (this.element.data(x)) {

                    this.options[x] = this.element.data(x);
                }
            }

        },
        _generateBody: function() {
            var html = [ //'<div class="ns-body">',
                '<div class="ns-selected-container">',
                '<span class="selected-label">请选择</span>',
                '<ul class="ns-selected-list clearfix">',
                '<li class="ns-selected-submit">',
                '<button type="button" class="btn btn-link btn-tag" data-dismiss="modal">[确定]</button></li>',
                '</ul>', ,
                '</div>',
                //         '<div class="ns-level-0">',
                //             '<div class="ns-title"></div>',
                //             '<ul class="ns-list"></ul>',
                // //<ul class="ns-list"><li class="ns-item"><a></a></li></ul>
                //         '</div>',
                //'</div>'
            ].join('');
            return html;
        },
        _generateList: function(list) {
            var html = [],
                me = this;
            $.each(list, function() {
                var node = me.nodeIndex[this];
                html.push('<li class="ns-item" data-path="' + this + '"><a class="btn btn-link btn-tag" href="javascript:void(0);">' + node.label + '</a></li>');
            });
            return html.join('');
        },
        _getnerateLevel: function(level, label, list) {
            var html = [
                '<div class="ns-level-', level, '">',
                '<div class="ns-title">', label, '</div>',
                '<ul class="ns-list clearfix">',
                this._generateList(list),
                '</ul>',
                '</div>'
            ].join('');
            return html;
        },
        destroy:function(){
            this.modal.remove();
            this.element.css({position:null,left:null});
            this.element.unwrap();
        }
    });
    $.extend($.l.nestedselect, {
        uuid: 0,
        instance: {},
        idGenerator: 0,
        generateId: function() {
            this.uuid++;
            return this.uuid;
        }
    });
}(jQuery));