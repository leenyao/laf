(function($) {
    var modalTemplate = [].join('');
    $.widget('l.tagselect', {
        options: {
            ui:'',
            maxCount:Infinity,
            required: false
        },
        _init: function() {
            var me = this,
                //value = this.element.val(),
                readonly = this.element.is("[readonly]"),
                options = this.element.find('option');
            this.body.empty();

            options.each(function() {
                // var label = this.text,
                //     value = this.value,
                //     isSelected = this.selected;
                if(readonly){
                    this.selected = false;
                }
                me._addTag(this);
            });
        },
        _create: function() {
            var me = this;
            $.l.tagselect.idGenerator++;
            me.id = this.widgetName + '-' + $.l.tagselect.generateId();
            $.l.tagselect.instance[me.id] = this;
            this.initOptions();
            //me._createIndex();
            this.element.wrap('<div class="ts-wrap"></div>');
            this.wrap = $(this.element.parent());
            this.element.hide();
            this.body = $(this._generateBody());
            this.body.appendTo(this.wrap);
            this._bindEvent();
        },
        getValue: function() {
            return this.element.val();
        },
        setValue: function(value) {
            this.element.val(value);
            // this.body.find('[data-valeu="'+value+'"]').addClass('active');
            this._refresh();
        },

        _onSelect: function() {},
        _addTag: function(option) {
            var html = [
                '<li class="ts-item', option.selected ? ' active' : '', '" data-value="' + option.value + '">',
                '<a href="javascript:void(0)" class="">', option.text, '</a>',
                '<b></b>',
                '</li>'
            ].join('');
            return $(html).data('option', option).appendTo(this.body);

        },
        _onChange: function() {},

        _bindEvent: function() {
            var me = this;
            me.body.on('click', '.ts-item', function() {
                var isSelected = $(this).data('option').selected,
                    selectCount = me.body.find('.ts-item.active').length;
                if(me.element.is("[readonly]")){
                    return;
                }
                if(!isSelected && selectCount >= me.options.maxCount){
                    alert("很抱歉，您只能最多选取"+me.options.maxCount+"个");
                    return false;
                }
                if(isSelected && me.options.required && selectCount <=1){
                    return false;
                }
                $(this).data('option').selected = !isSelected;
                me._refresh();
                me.element.trigger('change');
            });
        },
        _refresh: function() {
            $('.ts-item', this.body).each(function() {

                if ($(this).data('option').selected) {
                    $(this).addClass('active');
                    
                } else {
                    $(this).removeClass('active');
                }
            });
        },

        initOptions: function() {
            var x;
            for (x in this.options) {
                if (this.element.data(x)) {
                    this.options[x] = this.element.data(x);
                }
            }
            if(this.element.attr('required')){
                this.options.required = true;
            }

        },
        _generateBody: function() {
            var html = [ //'<div class="ns-body">',
                '<ul class="ts-body">',
                '</ul>'
            ].join('');
            return html;
        },
        destroy: function() {
            this.body.remove();
            this.element.unwrap();
            try{
                delete $.l.tagselect.instance[this.id];
            }catch(e){
                console.log('no instance of $.l.tagselect:'+this.id);
            }
        }

    });
    $.extend($.l.tagselect, {
        uuid: 0,
        instance: {},
        idGenerator: 0,
        generateId: function() {
            this.uuid++;
            return this.uuid;
        }
    });
    
}(jQuery));