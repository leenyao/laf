$(function() {
    $template = $('[l-template]');
    var templateIds = [];
    $template.each(function(){
        var $this = $(this),
            url = $this.attr('l-template-url');
        url && $.get(url,function(result){
            if(String(result.status) === "5004"){

            }
            if(String(result.status) === "1"){
                try{$this.html(result.data);}catch(e){}
            }
            $this.trigger('templateload',$this[0]);
        },'json');
        templateIds.push(this);
    });
    if(templateIds.length === 0){
        $(document).trigger('alltemplateload');
    }
    $(document).on('templateload',function(e,dom){
        var i = $.inArray(dom,templateIds);
        if (i > -1){
            templateIds.splice(i,1);
        }
        if(templateIds.length === 0){
            $(document).trigger('alltemplateload');
        }
    });
    $('.cate-container a, .search-hots a, .filter-item a,.search-location a,.filter a').click(function(){
        var link = $(this).attr('data-link');
        if(link){
            location.href = $(this).attr('data-link');
        }
    });
});
$(document).on('alltemplateload',function(){
    var length = $('.catebox').length;
    $('[data-toggle="tooltip"]').tooltip();
    $('.catebox .section-header').click(function(e){
        var wrap = $(this).closest('.catebox'),
            main = wrap.find('.main'),
            sub = wrap.find('.sub'),
            index = wrap.index();
        if(sub.is(':visible')){
            $(document).trigger('click');
            return;
        }
        $(document).trigger('click');
        main.addClass('active');
        sub.show();
        sub.css('top', index/(length-1)*(-sub.outerHeight()+main.outerHeight()));
        $(document).one('click',function(){
            main.removeClass('active');
            sub.hide();
        });
        e.stopPropagation();
    });
});
(function($){/*global l*/
    if(window.l){return ;}
    window.l = {};
    var scope = {};
    l.scope = scope;
    l.setup = function(config) {
        var i;
        if(typeof config === 'object'){
            for (i in config){
                if(config.hasOwnProperty(i)){
                    l.scope[i] = config[i];
                }
            }
        }
    };
    l.get = function(paramName) {
        return l.scope[paramName];
    };
    l.ajax = function(url, data) {
        return $.ajax({
            url: url,
            data: {
                params: JSON.stringify(data)
            },
            dataType: 'json',
            type: 'post'
        });
    };
    l.changevcode = function(id){
        var el = document.getElementById(id),src;
        if(el && el.tagName ==='IMG'){
            src = el.src;
            src = src.replace(new RegExp("(&|\\?)tm=[^&]+"),'');
            el.src = src+'?tm='+ new Date().getTime();
        }
    };
    l.dialog = function(config){
        var remoteUrl = config.remoteUrl,
            autoDestroy = true,
            id = config.id,
            title = config.title||"提示",
            $dialog,
            buttons = config.buttons,
            html = '';
        if(typeof config.autoDestroy !== 'undefined'){
            autoDestroy = config.autoDestroy;
        }
        if(typeof id === 'undefined'){
            id = "modal-"+$.now();
        }
        html = ['<div id="'+id+'" data-remote="'+(remoteUrl?remoteUrl:"")+'" class="modal fade">',
            '<div class="modal-dialog">',
            '<div class="modal-content">',
            '<div class="modal-header">',
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
            '<h4 class="modal-title">'+title+'</h4>',
            '</div>',
            '<div class="modal-body">',
            config.body,
            '</div>',
            (config.footer?'<div class="modal-footer">'+config.footer+'</div>':''),
            '</div>',
            '</div>',
            '</div>'
        ].join('');
        $dialog = $(html).appendTo('body');
        $.each(buttons,function(k,v){
            var btn = $('<button type="button"></button>'),
                footer = $('.modal-footer',$dialog),
                scope = v.scope || this;
            btn.html(v.text);
            btn.attr(v.attr||{});
            btn.addClass(v.cls);
            if(typeof v.handler === 'function'){
                btn.on('click',function(){
                    v.handler.call(scope,this,$dialog);
                });
            }
            if(footer.length === 0){
                footer = $('<div class="modal-footer"></div>').appendTo($dialog.find('.modal-content'));
            }
            btn.appendTo(footer);
        });
        if(autoDestroy){
            $dialog.on('hidden.bs.modal',function(){
                $dialog.remove();
            });
        }
        return $dialog;
    };

}(jQuery));