$(function() {
    $template = $('[l-template]');
    var templateIds = [];
    $template.each(function(){
        var $this = $(this),
            url = $this.attr('l-template-url');
        url && $.get(url,function(result){
            if(String(result.status) === "5004"){

            }
            if(String(result.status) === "1"){
                try{$this.html(result.data);}catch(e){}
            }
            $this.trigger('templateload',$this[0]);
        },'json');
        templateIds.push(this);
    });
    if(templateIds.length === 0){
        $(document).trigger('alltemplateload');
    }
    $(document).on('templateload',function(e,dom){
        var i = $.inArray(dom,templateIds);
        if (i > -1){
            templateIds.splice(i,1);
        }
        if(templateIds.length === 0){
            $(document).trigger('alltemplateload');
        }
    });
});