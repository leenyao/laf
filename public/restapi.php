<?php
include_once(__DIR__."/restapi/toro.php");
require_once(__DIR__.'/../bin/script/config_inc.php');
//header('Content-type: application/json'); 

$aRestConfig = array();
$aConfig = $oGlobalFramework->sm->get('config');
foreach($aConfig['apiservice']['customer'] as $sTmp => $aTmp){
    $aRestConfig['/('.$sTmp.')/:alpha'] = $aTmp['class'];
    $aRestConfig['/('.$sTmp.')'] = $aTmp['class'];
}
//$aRestConfig["/:alpha/:alpha"] = "\Application\Restapi\Common";
//$aRestConfig["/:alpha"] = "\Application\Restapi\Common";

ToroHook::add("404",  function($aParams=array()) {
    $response = new \Zend\Http\PhpEnvironment\Response();
    $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_404);
    $response->getHeaders()->addHeaders(array(
            'Access-Control-Allow-Origin' => '*'
        )
    );
    $response->setContent(is_string($aParams) ? $aParams : "resource not found");
    $response->send();
    exit;
});
ToroHook::add("200",  function($sMsg='ok') {
    $response = new \Zend\Http\PhpEnvironment\Response();
    $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_200);
    if(is_array($sMsg) || is_array(json_decode($sMsg, 1))){
        $response->getHeaders()->addHeaders(array(
            'Content-type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        ));
    }else{
        $response->getHeaders()->addHeaders(array(
            'Access-Control-Allow-Origin' => '*',
            'Content-type' => 'html/text'
        ));
    }
    $response->setContent($sMsg);
    $response->send();
    exit;
});
ToroHook::add("401",  function($sMsg='Unauthorized') {
    $response = new \Zend\Http\PhpEnvironment\Response();
    $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_401);
    $response->setContent($sMsg);
    $response->getHeaders()->addHeaders(array(
            'Access-Control-Allow-Origin' => '*'
        )
    );
    $response->send();
    exit;
});
ToroHook::add("403",  function($sMsg='Unauthorized') {
    $response = new \Zend\Http\PhpEnvironment\Response();
    $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_403);
    $response->setContent($sMsg);
    $response->getHeaders()->addHeaders(array(
            'Access-Control-Allow-Origin' => '*'
        )
    );
    $response->send();
    exit;
});
ToroHook::add("500",  function($sMsg='service internal error') {
    $response = new \Zend\Http\PhpEnvironment\Response();
    $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_500);
    $response->setContent($sMsg);
    $response->getHeaders()->addHeaders(array(
            'Access-Control-Allow-Origin' => '*'
        )
    );
    $response->send();
    exit;
});
try{
    Toro::serve($aRestConfig);
}catch(\YcheukfCommon\Lib\Exception $e){
    \ToroHook::fire(500, ($e->getMessage()));
}